from logging import Logger
from utilities.validators import Validators
from utilities.util import Util


class CreateQuoteAccessorialValidators(Validators):

    def __init__(self, logger: Logger):
        super().__init__(logger=logger)
        self._log = logger
        self.util = Util(logger)

    def validate_accessorials_based_on_equipment_type_tl(self, equipment_type, accessorial_list):
        _requiredValues = self.util.csv_row_data_to_list_search_by_column_values(
            fileName="tl_accessorials.csv", columnName="equipmentType", searchValue=equipment_type.lower())

        if len(accessorial_list) != len(_requiredValues):
            self._log.error(f"Found Accessorials & SS amount is not the same required. "
                            f"Found amount {len(accessorial_list)}, required amount {len(_requiredValues)}")
        _results = []
        for requiredASS in _requiredValues:
            _results.append(
                self.validate_particular_accessorial_based_on_equipment_type_tl(
                    required_equipment_name=requiredASS["accessorialsincontainer"],
                    accessorial_list=accessorial_list))
        return not any(x for x in _results if x is False)

    def validate_particular_accessorial_based_on_equipment_type_tl(self, required_equipment_name, accessorial_list):
        if any(x for x in accessorial_list if x.name == required_equipment_name):
            return True
        else:
            self._log.error(f"Validation of Accesorial failed, not found {required_equipment_name}.")
            return False
