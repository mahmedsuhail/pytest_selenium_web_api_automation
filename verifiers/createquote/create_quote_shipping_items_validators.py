from logging import Logger

from API.Models.product_commodity import ProductCommodity
from pages.common.models.shipping_item import ShippingItem
from utilities.util import Util
from utilities.validators import Validators


class CreateQuoteShippingItemsValidators(Validators):

    def __init__(self, logger: Logger):
        super().__init__(logger)
        self._log = logger
        self.util = Util(logger)

    def validate_search_commodity_placeholder(self, actual):
        return self.verify_text_match(actual, "Search Commodity")

    def validate_commodity_items_count_more_than_zero(self, commodity_items_list):
        return len(commodity_items_list) > 0

    def validate_commodity_items_names_are_only_with_part_of_value(self, commodity_items_list, value):
        # Validate if list contains only items, that name contains part of 'value'. It's to verify if filtering works

        # if there's an any element that doesn't contain value, it will give true. Method verify if there're only
        # elements that contain value, so need to negate result
        return not any(x for x in commodity_items_list if value.lower() not in x.lower())

    def validate_search_item_from_page_equal_to_expected(self, si_from_page: ShippingItem,
                                                         si_expected: ProductCommodity):
        result1 = self.verify_text_match(si_from_page.commodity_description,
                                         si_expected.model['commodityDescription'])
        result2 = self.verify_text_match(si_from_page.weight,
                                         si_expected.model['weight']['amount'])
        result3 = self.verify_text_match(si_from_page.height,
                                         si_expected.model['dim']['height'])
        result4 = self.verify_text_match(si_from_page.length,
                                         si_expected.model['dim']['length'])
        result5 = self.verify_text_match(si_from_page.width,
                                         si_expected.model['dim']['width'])
        result6 = self.verify_text_match(si_from_page.piece_count,
                                         si_expected.model['pieceCount'])
        result7 = self.verify_text_match(si_from_page.pallet_count,
                                         si_expected.model['handlingUnitCount'])

        return all([result1, result2, result3, result4, result5, result6, result7])
