from utilities.holidays_list import is_holiday


class TestHoliday:

    def test_correct_holiday(self):
        assert is_holiday("05/25/2020")

    def test_not_holiday(self):
        assert is_holiday("05/26/2020") is False
