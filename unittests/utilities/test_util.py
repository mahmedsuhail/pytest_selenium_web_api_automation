import datetime
import logging
import re

import pytest
from assertpy import assert_that

from utilities.util import Util

_logger = logging.getLogger('dummy')


class TestUtilsV2:

    @pytest.mark.unittests
    def test_get_key_value(self):
        dictionary = {
            "sport": "running",
            "event": "New York Marathon",
            "goal": "accomplish"
        }
        assert_that(Util(_logger).get_key_for_value(passed_value="New York Marathon", dictionary=dictionary)) \
            .is_equal_to("event")

    @pytest.mark.unittests
    def test_add_to_dictionary_existing_add_new_value(self):
        dictionary = {
            "sport": "running"
        }
        dictionary1 = Util(_logger).add_to_dictionary(key="event",
                                                      value="New York Marathon",
                                                      passed_dictionary=dictionary)

        assert_that(dictionary).is_equal_to(dictionary1)
        assert_that(dictionary["event"]).is_equal_to("New York Marathon")

    @pytest.mark.unittests
    def test_add_to_dictionary_existing_update_value(self):
        dictionary = {
            "sport": "running"
        }
        dictionary1 = Util(_logger).add_to_dictionary(key="sport", value="cycling",
                                                      passed_dictionary=dictionary)
        assert_that(dictionary).is_equal_to(dictionary1)
        assert_that(dictionary["sport"]).is_equal_to("cycling")

    @pytest.mark.unittests
    def test_add_to_dictionary_not_existing_add_value(self):
        dictionary = Util(_logger).add_to_dictionary(key="sport", value="cycling")

        assert_that(dictionary["sport"]).is_equal_to("cycling")

    @pytest.mark.unittests
    def test_get_random_alpha_numeric_letters_only(self):
        random_string = Util.get_random_alpha_numeric(10)

        assert_that(re.compile(r"^[a-zA-Z]{10}$").match(random_string)).is_true()

    @pytest.mark.unittests
    def test_get_random_alpha_numeric_numbers_only(self):
        random_string = Util.get_random_alpha_numeric(10, characters_type="digits")

        assert_that(re.compile(r"^[0-9]{10}$").match(random_string)).is_true()

    @pytest.mark.unittests
    def test_add_no_days_to_date_saturday(self):
        end_date = Util(_logger).get_date_value(no_of_days=4,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=10, day=20))

        assert_that(end_date).is_equal_to("10/26/2020")

    @pytest.mark.unittests
    def test_add_no_days_to_date_sunday(self):
        end_date = Util(_logger).get_date_value(no_of_days=5,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=10, day=20))

        assert_that(end_date).is_equal_to("10/27/2020")

    @pytest.mark.unittests
    def test_sub_no_days_to_date_saturday(self):
        end_date = Util(_logger).get_date_value(no_of_days=-3,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=10, day=20))

        assert_that(end_date).is_equal_to("10/15/2020")

    @pytest.mark.unittests
    def test_sub_no_days_to_date_sunday(self):
        end_date = Util(_logger).get_date_value(no_of_days=-2,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=10, day=20))

        assert_that(end_date).is_equal_to("10/16/2020")

    @pytest.mark.unittests
    def test_add_no_days_to_date_weekday(self):
        end_date = Util(_logger).get_date_value(no_of_days=7,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=10, day=20))

        assert_that(end_date).is_equal_to("10/29/2020")

    @pytest.mark.unittests
    def test_get_date_based_on_holiday(self):
        end_date = Util(_logger).get_date_based_on_holiday(no_of_days=4,
                                                           date_from=datetime.datetime(year=2020, month=12, day=20))

        assert_that(end_date).is_equal_to("12/28/2020")

    @pytest.mark.unittests
    def test_get_date_on_holiday(self):
        end_date = Util(_logger).get_date_value(no_of_days=4,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=12, day=20))

        assert_that(end_date).is_equal_to("12/24/2020")

    @pytest.mark.unittests
    def test_get_date_on_holiday_current_date_is_workday(self):
        end_date = Util(_logger).get_date_value(no_of_days=0,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=11, day=19))

        assert_that(end_date).is_equal_to("11/19/2020")

    @pytest.mark.unittests
    def test_get_date_on_holiday_current_date_is_holiday(self):
        end_date = Util(_logger).get_date_value(no_of_days=0,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2020, month=11, day=26))

        assert_that(end_date).is_equal_to("11/26/2020")

    @pytest.mark.unittests
    def test_get_date_add_2_working_days_starting_friday(self):
        end_date = Util(_logger).get_date_value(no_of_days=2,
                                                date_format='%m/%d/%Y',
                                                date_from=datetime.datetime(year=2021, month=1, day=22))

        assert_that(end_date).is_equal_to("01/26/2021")

    @pytest.mark.unittests
    def test_get_formatted_date(self):
        expected = "01/13/2021"
        d = datetime.date(day=13, month=1, year=2021)
        v = d.strftime("%m/%d/%Y")
        assert_that(v).is_equal_to(expected)
