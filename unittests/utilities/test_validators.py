import logging

import pytest

from utilities.validators import Validators

_logger = logging.getLogger('dummy')


# TODO: Verifiers unit tests - add remaining tests


class TestValidators:

    @pytest.mark.unittests
    def test_verify_text_contains_positive(self):
        assert Validators(_logger).verify_text_contains(text1="John Doe is a captain", expected_text="a captain")

    @pytest.mark.unittests
    def test_verify_text_contains_case_insensitive_positive(self):
        assert Validators(_logger).verify_text_contains(text1="John Doe is a captain", expected_text="A Captain")

    @pytest.mark.unittests
    def test_verify_text_contains_negative(self):
        assert Validators(_logger).verify_text_contains(text1="John Doe is a captain",
                                                        expected_text="Captains") is False

    @pytest.mark.unittests
    def test_verify_text_match_only_partially_matches_negative(self):
        assert Validators(_logger).verify_text_match(actual_text="John Doe is a captain", expected_text="a captain") \
               is False

    @pytest.mark.unittests
    def test_verify_text_match_positive(self):
        assert Validators(_logger).verify_text_match(actual_text="John Doe is a captain",
                                                     expected_text="john doe is a captain")

    @pytest.mark.unittests
    def test_verify_list_match_positive(self):
        list1 = ["a", "b", "c"]
        list2 = ["a", "b", "c"]
        assert Validators(_logger).verify_list_match(actual_list=list1, expected_list=list2)

    @pytest.mark.unittests
    def test_verify_list_match_negative(self):
        list1 = ["a", "b", "c", "d"]
        list2 = ["a", "b", "c"]
        assert Validators(_logger).verify_list_match(actual_list=list1, expected_list=list2) is False

    @pytest.mark.unittests
    def test_verify_unordered_list_match_positive(self):
        list1 = ["a", "b", "c"]
        list2 = ["c", "a", "b"]
        assert Validators(_logger).verify_unordered_list_match(actual_list=list1, expected_list=list2)

    @pytest.mark.unittests
    def test_verify_list_contains_empty_list_positive(self):
        list1 = ["a", "b", "c"]
        list2 = []
        assert Validators(_logger).verify_list_contains(actual_list=list1, expected_list=list2)

    @pytest.mark.unittests
    def test_verify_list_contains_positive(self):
        list1 = ["a", "b", "c"]
        list2 = ["c", "b"]
        assert Validators(_logger).verify_list_contains(actual_list=list1, expected_list=list2)

    @pytest.mark.unittests
    def test_verify_list_contains_negative(self):
        list1 = ["a", "b", "c"]
        list2 = ["c", "d"]
        assert Validators(_logger).verify_list_contains(actual_list=list1, expected_list=list2) is False

    @pytest.mark.unittests
    def test_verify_list_contains_negative(self):
        list1 = ["a", "b", "c"]
        list2 = ["e"]
        assert Validators(_logger).verify_list_contains(actual_list=list1, expected_list=list2) is False
