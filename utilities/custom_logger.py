import inspect
import logging
from logging import Logger

LOG_FORMAT = '%(asctime)s|%(name)s|%(filename)s:%(lineno)d|%(levelname)s|%(message)s'
DATE_FORMAT = '%m/%d-%H:%M:%S'


# TODO - remove this after further test refactoring
def automation_logger(logLevel=logging.DEBUG):
    # Gets the name of the class / method from where this method is called
    loggerName = inspect.stack()[1][3]
    logger = logging.getLogger(loggerName)
    if not logger.hasHandlers():
        # By default, log all messages
        logger.setLevel(logging.DEBUG)

        fileHandler = logging.FileHandler("logs/automation.log", mode='a')
        fileHandler.setLevel(logLevel)

        formatter = logging.Formatter(LOG_FORMAT, datefmt=DATE_FORMAT)

        fileHandler.setFormatter(formatter)
        logger.addHandler(fileHandler)

    return logger


def test_logger(filename, log_level=logging.DEBUG) -> Logger:
    # Gets the name of the class / method from where this method is called
    logger_name = inspect.stack()[1][3]
    logger = logging.getLogger(name=logger_name)
    # By default, log all messages
    logger.setLevel(logging.DEBUG)

    file_handler = logging.FileHandler(f"logs/{filename}_automation.log", mode='a')
    file_handler.setLevel(log_level)

    formatter = logging.Formatter(LOG_FORMAT, datefmt=DATE_FORMAT)

    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger
