import sys


if __name__ == '__main__':
    """
    script for ordering skipped test list.
    usage: python3 skipped_tests.py <filename>
    """

    pattern = "https://globaltranz.atlassian.net/browse/"

    skipped_tests = {}
    last_key = ""
    file_name = sys.argv[1]

    f = open(file_name, "r")
    lines = f.readlines()
    f.close()

    for line in lines:
        if pattern in line:
            last_key = line
        else:
            value = ""
            if last_key in skipped_tests.keys():
                value = skipped_tests[last_key]
            skipped_tests[last_key] = value + line

    lines.clear()
    for key in sorted(skipped_tests.keys()):
        lines.append(key)
        lines.append(skipped_tests[key])
        print(key)
        print(skipped_tests[key])

    f = open(file_name, "w")
    f.writelines(lines)
    f.close()
