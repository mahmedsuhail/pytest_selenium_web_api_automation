"""
@package utilities

CheckPoint class implementation
It provides functionality to assert the result

Example:
    self.check_point.markFinal("Test Name", result, "Message")
"""
from traceback import print_stack
from inspect import currentframe, getframeinfo
from base.selenium_driver import SeleniumDriver

TEST_FAIL = "FAIL"
TEST_PASS = "PASS"


"""
 WARNING: LEGACY CODE!

 Please do not use in new code, use assertpy library (assert_that, soft_assertion) instead
"""


class ReportTestStatus(SeleniumDriver):

    def __init__(self, driver, log):
        """
        Inits CheckPoint class
        """
        super(ReportTestStatus, self).__init__(driver, log)
        self.resultList = []

    def set_result(self, result, result_message):
        try:
            if result is not None:
                if result:
                    self.resultList.append((TEST_PASS, result_message))
                    self.log.info("### VERIFICATION SUCCESSFUL :: + " + result_message)
                else:
                    self.resultList.append((TEST_FAIL, result_message))
                    self.log.error("### VERIFICATION FAILED :: + " + result_message)
                    self.screen_shot(result_message)
            else:
                self.resultList.append((TEST_FAIL, result_message))
                self.log.error("### VERIFICATION FAILED :: + " + result_message)
                self.screen_shot(result_message)
        except Exception as E:
            self.resultList.append((TEST_FAIL, result_message))
            self.log.error("### Exception Occurred !!!")
            self.screen_shot(result_message)
            self.log.error(f"Exception: {str(E)}")
            print_stack()

    def mark(self, result, result_message):
        """
        Mark the result of the verification point in a test case
        """
        frame_info = getframeinfo(currentframe().f_back)

        result_message = result_message + f"[_filename: {frame_info.filename} _line: {frame_info.lineno}]"
        self.set_result(result, result_message)

    def mark_final(self, test_name, result, result_message):
        """
        Mark the final result of the verification point in a test case
        This needs to be called at least once in a test case
        This should be final test status of the test case
        """
        self.set_result(result, result_message)

        all_failures = self._get_all_failures()

        if len(all_failures) > 0:
            self.log.error(test_name + " ### TEST FAILED")
            failed_steps = 'Failed Steps: \n' + '\n'.join(all_failures)
            self.resultList.clear()
            assert False, failed_steps
        else:
            self.log.info(test_name + " ### TEST SUCCESSFUL")
            self.resultList.clear()
            assert True

    def _get_all_failures(self):

        failure_list = []

        for result in self.resultList:
            if result[0] == TEST_FAIL:
                failure_list.append("ERROR: " + result[1])

        return failure_list
