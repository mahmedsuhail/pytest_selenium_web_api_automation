import datetime

"""------ Holidays list is in %m/%d/%Y format -------"""
HOLIDAYS_LIST = [
    # 2020
    "01/01/2020", "05/25/2020", "07/03/2020",
    "09/07/2020", "11/26/2020", "11/27/2020",
    "12/24/2020", "12/25/2020",
    # 2021
    "01/01/2021"
]

DATE_FORMAT = "%m/%d/%Y"


def is_holiday(date: str) -> bool:
    """
    check if given date is a holiday
    :param date: date in str format MM/DD/YYYY
    :return: True for holiday, False for regular day or weekend
    """
    return date in HOLIDAYS_LIST


def is_holiday_datetime(date: datetime) -> bool:
    return is_holiday(date.strftime(DATE_FORMAT))


def is_weekend_datetime(date: datetime) -> bool:
    w = date.weekday()
    return date.weekday() in [5, 6]
