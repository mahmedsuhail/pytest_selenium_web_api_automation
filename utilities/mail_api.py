from datetime import datetime, timedelta
from logging import Logger
from utilities.util import Util
from mailosaur import MailosaurClient
from mailosaur.models import SearchCriteria
from utilities.read_data import read_json_file


class MailAPI:
    def __init__(self, logger: Logger):
        self.logger = logger
        self.util = Util(logger)
        self.config_file = read_json_file("UTIL/mailosaur_config.json")
        self.client = MailosaurClient(self.config_file["api_key"])
        self.server_id = self.config_file["server_id"]

    def get_email(self, email_address, up_to_minutes_ago=5):
        minutes_ago = datetime.today() - timedelta(minutes=up_to_minutes_ago)

        criteria = SearchCriteria()
        criteria.sent_to = email_address

        return self.client.messages.get(self.server_id, criteria, received_after=minutes_ago)
