"""
@package utilities

Util class implementation
All most commonly used utilities should be implemented in this class

Example:
    name = self.util.getUniqueName()
"""
import calendar
import datetime
import random
import string
from logging import Logger

import pytz

from utilities.holidays_list import is_holiday_datetime
from utilities.holidays_list import is_weekend_datetime
from utilities.read_data import read_csv_data


class Util:
    def __init__(self, logger: Logger):
        if logger is not None:
            self._log = logger

    @staticmethod
    def get_random_alpha_numeric(length: int, characters_type='letters') -> str:
        """
        Get random string of characters

        Parameters:
            length: Length of string, number of characters string should have
            characters_type: Type of characters string should have. Default is letters
            Provide lower/upper/digits for different types
        """
        alpha_num = ''
        if characters_type == 'lower':
            case = string.ascii_lowercase
        elif characters_type == 'upper':
            case = string.ascii_uppercase
        elif characters_type == 'digits':
            case = string.digits
        elif characters_type == 'mix':
            case = string.ascii_letters + string.digits
        else:
            case = string.ascii_letters
        return alpha_num.join(random.choice(case) for _ in range(length))

    def get_unique_name(self, char_count=10) -> str:
        """
        Get a unique name
        """
        return self.get_random_alpha_numeric(char_count, 'lower')

    def get_unique_name_list(self, list_size=5, item_length=None) -> []:
        """
        Get a list of valid email ids

        Parameters:
            list_size: Number of names. Default is 5 names in a list
            item_length: It should be a list containing number of items equal to the listSize
                        This determines the length of the each item in the list -> [1, 2, 3, 4, 5]
        """
        name_list = []
        for i in range(0, list_size):
            name_list.append(self.get_unique_name(item_length[i]))
        return name_list

    def get_formatted_date(self, date_value, input_format="", output_format="") -> str:
        self._log.info("Inside get_formatted_date Method")
        formatted_date = datetime.datetime.strptime(date_value, input_format).date().strftime(output_format)
        self._log.info("formatted_date : " + str(formatted_date))
        return formatted_date

    def get_current_date(self, date_format='%m/%d/%Y', time_zone="") -> str:
        """
        :param time_zone: If want date for particular timezone
        :param date_format: Format in which date need to be returned
        :return: system_date
        """
        try:
            if time_zone == "":
                system_date = datetime.datetime.now().strftime(date_format)
                self._log.info("Full current datetime: " + datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S"))
            else:
                system_date = datetime.datetime.now(pytz.timezone(time_zone)).strftime(date_format)
                self._log.info("Full current datetime: " + datetime.datetime.now(pytz.timezone(time_zone)).strftime(
                    "%m/%d/%Y, %H:%M:%S"))
            self._log.info("currentDate : " + str(system_date))
            return system_date

        except Exception as e:
            self._log.error(
                f"Either Date Format given is invalid or Cannot find Date with the format specified: {str(date_format)}")
            self._log.error(f"Exception: {str(e)}")

    def get_date_based_on_holiday(self, no_of_days: int, date_from=datetime.datetime.now()):
        """
        Under this method we are returning date based on the no of days passed by considering holidays and weekends
        :param no_of_days: no of days need to be passed and no of days can be +ve (for Future date)
        or -ve(for Previous date)
        :param date_from: starting day to count no of days
        :return: date_to_be_selected based on the no of days passed in str format
        """
        self._log.debug("Inside get_date_based_on_holiday method, No of Days: " + str(no_of_days) +
                        " Date From " + str(date_from))
        return self.get_date_value(no_of_days=no_of_days, date_from=date_from, skip_holiday=True)

    def get_date_value(self, no_of_days: int, date_format='%m/%d/%Y',
                       date_from=datetime.datetime.now(),
                       skip_holiday=False) -> str:
        """now
        Under this method, It will return date value based on the no of days passed and date format specified
        :param date_from: starting date to add working days, defaults to now()
        :param no_of_days: no of days
        :param date_format: Format of date to be returned and it's optional
        :param skip_holiday: if end date is on holiday - should we skip to workday or not
        :return:
        """
        try:
            self._log.debug(
                f"Inside get_date_value method, No of Days : {str(no_of_days)}, date_from : {str(date_from)} "
                f"and date Format : {str(date_format)}")
            if type(no_of_days) is str:
                no_of_days = int(no_of_days)

            if type(date_from) is str:
                date_value = datetime.datetime.strptime(date_from, date_format)
            else:
                date_value = date_from
            delta = 1 if no_of_days == 0 else no_of_days / abs(no_of_days)

            for _ in range(abs(no_of_days)):
                date_value += datetime.timedelta(days=delta)
                while is_weekend_datetime(date_value) or (skip_holiday and is_holiday_datetime(date_value)):
                    date_value += datetime.timedelta(days=delta)

            self._log.debug(f"selected date is: {str(date_value)}")
            return date_value.strftime(date_format)

        except Exception as e:
            self._log.error(f"Either Date Format given is invalid or Cannot find Date with the format specified : "
                            f"{str(date_format)}")
            self._log.error(f"Exception : {str(e)}")

    def get_week_day_value(self, passed_date, date_format="%m/%d/%Y"):
        """ Returning Weekday value based on the date passed"""
        try:
            date_to_be_selected = datetime.datetime.strptime(passed_date, date_format)
            weekday_value = calendar.day_abbr[date_to_be_selected.weekday()]
            return weekday_value
        except Exception as e:
            self._log.error(
                "Either Date Format given is invalid or Cannot find weekday with the format specified : " + str(
                    date_format))
            self._log.error("Exception : " + str(e))

    def get_key_for_value(self, passed_value: str, dictionary: dict) -> str:
        self._log.info("Inside get_key_for_value Method")
        try:
            for key, value in dictionary.items():
                if value == passed_value:
                    self._log.info("Return Key Value is : " + str(key))
                    return key
        except Exception as e:
            self._log.error("Exception throw by Method get_key_for_value and Exception is " + str(e))

        return ""

    def add_to_dictionary(self, key: str, value, passed_dictionary=None) -> dict:
        """
        For passed value it will return Dict object
        :param passed_dictionary: whether want to update in existing dictionary or new dict
        :param key: required Key value for dict
        :param value: required value for respective key
        :return: dict object
        """
        self._log.info("Inside add_to_dictonary Method")

        if passed_dictionary is None:
            passed_dictionary = {}

        try:
            passed_dictionary[key] = value
        except Exception as E:
            self._log.error("Exception occurred in add_to_dictonary Method, Exception is : " + str(E))

        return passed_dictionary

    def csv_row_data_to_list_search_by_column_values(self, columnName, searchValue, fileName):
        """
        converting passed csv row data into list. Get rows by searchValue in selected columnName
        :return: row data as List
        """
        try:
            lists = []
            data_obj = read_csv_data(fileName)
            for row in data_obj:
                if row[columnName] == searchValue:
                    lists.append(row)
            return lists
        except Exception as E:
            self._log.error("Error found, Either correct file name or column name not correct")
            self._log.error(E)

    def get_string_between(self, text, start, end):
        rurl_start_index = text.index(start) + len(start)
        rurl_end_index = text.index(end, rurl_start_index + 2)
        return text[rurl_start_index:rurl_end_index]

    def getFormattedDate(self, dateValue, inputFormat="", outputFormat=""):
        self._log.info("Inside getFormattedDate Method")
        formattedDate = datetime.datetime.strptime(dateValue, inputFormat).date().strftime(outputFormat)
        self._log.info("formattedDate : " + str(formattedDate))
        return formattedDate

    def getCurrentDate(self, dateFormat='%m/%d/%Y', timeZone=""):
        """
        :param timeZone: If want date for particular timezone
        :param dateFormat: Format in which date need to be returned
        :return: systemDate
        """
        try:
            if timeZone == "":
                system_date = datetime.datetime.now().strftime(dateFormat)
            else:
                system_date = datetime.datetime.now(pytz.timezone(timeZone)).strftime(dateFormat)
            self._log.info("currentDate : " + str(system_date))
            return system_date

        except Exception as e:
            self._log.error(
                "Either Date Format given is invalid or Cannot find Date with the format specified : " + str(
                    dateFormat))
            self._log.error("Exception : " + str(e))

    def getDateValueWithoutHolidayAndWeekends(self, no_of_days, passedDate="", dateFormat='%m/%d/%Y'):
        """
        Method to return date based on the no of days passed by without considering holidays and weekends
        :param dateFormat:
        :param no_of_days: no of days need to be passed and no of days can be +ve (for Future date) or
        -ve(for Previous date)
        :param passedDate : passedDate from which no. of days added or sub.
        :param : dateFormat : required format of the date in which it returned
        :return: required date based on no. of days passed.
        """
        try:
            self._log.info(
                f"Inside getDateValueWithoutHolidayAndWeekends method, No of Days : {str(no_of_days)} passed "
                f"Date is {str(passedDate)} and date Format : {str(dateFormat)}")
            if type(no_of_days) is str:
                no_of_days = int(no_of_days)
            if passedDate == "":
                date_value = datetime.datetime.now() + datetime.timedelta(days=no_of_days)
            else:
                date_value = datetime.datetime.strptime(passedDate, dateFormat).date() + datetime.timedelta(
                    days=no_of_days)

            required_date = date_value.strftime(dateFormat)
            self._log.info("Required date is : " + str(required_date))
            return required_date

        except Exception as e:
            self._log.error(f"getDateValueWithoutHolidayAndWeekends Method :Either Date Format given is invalid or "
                            f"Cannot find Date with the format specified : {str(dateFormat)} and "
                            f"the exception is {str(e)}")
