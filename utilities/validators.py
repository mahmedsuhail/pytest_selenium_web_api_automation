from logging import Logger


class Validators:

    def __init__(self, logger: Logger):
        self._log = logger

    def verify_text_contains(self, text1: str, expected_text: str) -> bool:
        """
        Verify actual text contains expected text string, case insensitive

        Parameters:
            expected_text: Expected Text
            text1: Actual Text
        """
        self._log.info("Actual Text From Application Web UI --> :: " + text1)
        self._log.info("Expected Text From Application Web UI --> :: " + expected_text)
        if expected_text.lower() in text1.lower():
            self._log.info("### VERIFICATION CONTAINS !!!")
            return True
        else:
            self._log.info("### VERIFICATION DOES NOT CONTAINS !!!")
            return False

    def verify_text_match(self, actual_text: str, expected_text: str) -> bool:
        """
        Verify text match exactly, case insensitive

       Parameters:
            expected_text: Expected Text
            actual_text: Actual Text
        """
        self._log.info("Actual Text From Application Web UI --> :: " + str(actual_text))
        self._log.info("Expected Text From Application Web UI --> :: " + str(expected_text))
        if str(actual_text).lower() == str(expected_text).lower():
            self._log.info("### VERIFICATION MATCHED !!!")
            return True
        else:
            self._log.error("### VERIFICATION NOT MATCHED !!!")
            return False

    def verify_list_match(self, expected_list: list, actual_list: list) -> bool:
        """
        Verify two list matches

        Parameters:
            expected_list: Expected List
            actual_list: Actual List
        """
        return set(expected_list) == set(actual_list)

    def verify_unordered_list_match(self, actual_list: list, expected_list: list) -> bool:
        """
        Verify two unordered list matches

        Parameters:
            expected_list: Expected List
            actual_list: Actual List
        """

        are_equal = sorted(expected_list) == sorted(actual_list)
        if not are_equal:
            self._log.error(
                f"Equipment Length dropdown values do not match expected. Actual:{sorted(actual_list)} Expected:{sorted(expected_list)}")

        return are_equal

    def verify_list_contains(self, expected_list: list, actual_list: list) -> bool:
        """
        Verify actual list contains elements of expected list

        Parameters:
            expected_list: Expected List
            actual_list: Actual List
        """
        length = len(expected_list)
        for i in range(0, length):
            if expected_list[i] not in actual_list:
                return False
        else:
            return True
