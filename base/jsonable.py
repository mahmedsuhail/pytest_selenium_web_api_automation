import json


class JSONAble:
    def __init__(self):
        self.__model = None

    @property
    def model(self):
        if self.__model is None:
            raise NotImplementedError
        return self.__model

    @model.setter
    def model(self, value):
        self.__model = value

    def to_json(self):
        return json.dumps(self.model)
