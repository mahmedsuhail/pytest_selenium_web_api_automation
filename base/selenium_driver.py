import os
import re
import time
from traceback import print_stack
from typing import List

from selenium.common.exceptions import ElementNotSelectableException
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from conftest import config_holder


class SeleniumDriver:

    def __init__(self, driver, log):
        self.log = log
        self.driver = driver
        self.NO_ATTEMPTS = 10
        self.BETWEEN_CLICK_DELAY = 0.5

    def get_base_url(self) -> str:
        return config_holder["baseurl"]

    def screen_shot(self, result_message):
        """
        Takes screenshot of the current open web page
        """
        result_message = result_message.split("_filename:")[0]

        file_name = f"{self.driver.capabilities['browserName'].upper()}_{self.log.name}_{result_message}" \
                    f"_{str(round(time.time()))}.png"
        file_name = re.sub(r'(?u)[^-\w.]', '', file_name)
        screenshot_directory = "../screenshots/"
        relative_file_name = screenshot_directory + file_name
        current_directory = os.path.dirname(__file__)
        destination_file = os.path.join(current_directory, relative_file_name)
        destination_directory = os.path.join(current_directory, screenshot_directory)

        try:
            if not os.path.exists(destination_directory):
                os.makedirs(destination_directory)
            self.driver.save_screenshot(destination_file)
            self.log.info("Screenshot save to directory: " + destination_file)
        except Exception as E:
            self.log.error("### Exception Occurred when taking screenshot")
            self.log.error(str(E))
            print_stack()

    def get_title(self):
        return self.driver.title

    def get_element(self, locator: (By, str), parent=None, repeat=1) -> WebElement:
        element = None
        while repeat > 0:
            try:
                if parent is None:
                    element = self.driver.find_element(*locator)
                else:
                    element = parent.find_element(*locator)
                self.log.info(f"Element found with locator: {locator[1]} and locatorType: {str(locator[0])}")
                repeat = 0
            except Exception as E:
                self.log.info(f"Element not found with locator: {locator[1]} and locatorType: {str(locator[0])}")
                self.log.error(str(E))
                repeat = repeat - 1
                if repeat >= 1:
                    time.sleep(self.BETWEEN_CLICK_DELAY)
        return element

    def wait_for_element(self, locator: (By, str), event="",
                         timeout=10, poll_frequency=0.5, text_value=""):
        _element = None
        try:
            if event == "display":
                _expected_condition = EC.visibility_of_element_located(locator)
            elif event == "notdisplay":
                _expected_condition = EC.invisibility_of_element(locator)
            elif event == "text":
                _expected_condition = EC.text_to_be_present_in_element(locator, text_value)
            elif event == "clickable":
                _expected_condition = EC.element_to_be_clickable(locator)
            else:
                _expected_condition = EC.element_to_be_clickable(locator)

            self.log.info(f"Waiting for maximum :: {timeout} :: seconds for element {event}")
            wait = WebDriverWait(self.driver, timeout=timeout,
                                 poll_frequency=poll_frequency,
                                 ignored_exceptions=[NoSuchElementException,
                                                     ElementNotVisibleException,
                                                     ElementNotSelectableException])
            _element = wait.until(_expected_condition)
            self.log.info(f"Expected condition - element {event} - met")

        except Exception as E:
            self.log.error(f"Error with waiting for {event}, element with locator: {locator[1]}, exception: {E}")
            print_stack()

        return _element

    def wait_for_element_visible(self, locator: (By, str), timeout=10, poll_frequency=0.5):
        return self.wait_for_element(locator=locator, event="displayed", timeout=timeout, poll_frequency=poll_frequency)

    def wait_for_element_not_visible(self, locator: (By, str), timeout=10, poll_frequency=0.5):
        return self.wait_for_element(locator=locator, event="notdisplayed", timeout=timeout,
                                     poll_frequency=poll_frequency)

    def wait_for_element_has_text(self, locator: (By, str), text_value: str, timeout=10, poll_frequency=0.5):
        return self.wait_for_element(
            locator=locator,
            event="text",
            timeout=timeout,
            poll_frequency=poll_frequency,
            text_value=text_value)

    def wait_for_element_clickable(self, locator: (By, str), timeout=10, poll_frequency=0.5):
        return self.wait_for_element(locator=locator, event="clickable", timeout=timeout, poll_frequency=poll_frequency)

    def wait_for_element_list_not_empty(self, locator: (By, str), timeout=10, poll_frequency=0.5):
        wait = WebDriverWait(self.driver, timeout=timeout,
                             poll_frequency=poll_frequency,
                             ignored_exceptions=[NoSuchElementException,
                                                 ElementNotVisibleException,
                                                 ElementNotSelectableException])
        wait.until(lambda driver: len(driver.find_elements(*locator)) > 0)

    def get_element_list(self, locator: (By, str), parent: WebElement = None) -> List[WebElement]:
        """
        NEW METHOD
        Get list of elements
        """
        elements = None
        try:
            if parent is None:
                elements = self.driver.find_elements(*locator)
            else:
                elements = parent.find_elements(*locator)
            self.log.info(f"Element list found with locator: {locator[1]} and  locatorType: {locator[0]}")
        except Exception as E:
            self.log.info(f"Element list not found with locator: {locator[1]} and  locatorType: {locator[0]}")
            self.log.error(str(E))
        return elements

    def element_click(self, element: WebElement, scroll_to=True):
        """
        Click on an element -> MODIFIED
        Either provide element or a combination of locator and locatorType
        """
        try:
            if scroll_to:
                self.scroll_into_view(element)
            element.click()
            self.log.info(f"Clicked on element {element.id}")
        except Exception as E:
            self.log.info("Cannot click on the element Exception found : " + str(E))
            print_stack()

    def js_element_click(self, element: WebElement, scroll_to=True):
        """
        Click on an element -> MODIFIED
        Either provide element or a combination of locator and locatorType
        """
        try:
            if scroll_to:
                self.scroll_into_view(element)
            self.driver.execute_script("arguments[0].click();", element)
            self.log.info(f"JS clicked on element {element.id}")
        except Exception as E:
            self.log.info("Cannot click on the element Exception found : " + str(E))
            print_stack()

    def element_action_click(self, element: WebElement, scroll_to=True):
        """
        Click on an element -> MODIFIED
        Either provide element or a combination of locator and locatorType
        """
        try:
            if scroll_to:
                self.scroll_into_view(element)
            action = ActionChains(self.driver)
            # double click operation and perform
            action.click(element).perform()
            self.log.info(f"Action clicked on element {element.id}")
        except Exception as E:
            self.log.info("Cannot click on the element Exception found : " + str(E))
            print_stack()

    def element_click_by_locator(self, locator: (By, str), scroll_to=True):
        """
        Click on an element -> MODIFIED
        Provide a combination of By and locator
        """
        element = self.get_element(locator)
        self.element_click(element=element, scroll_to=scroll_to)

    def element_double_click(self, locator: (By, str) = None, element=None):
        """
        Double click on an element
        Either provide element or a combination of locator and locatorType
        """
        try:
            if element is None:  # This means if locator is not empty
                element = self.get_element(locator)
            # action chain object creation
            action = ActionChains(self.driver)
            # double click operation and perform
            action.double_click(element).perform()
            self.log.info("Double Clicked on element with locator: " + locator[1] + " locatorType: " + locator[0])
        except Exception as E:
            self.log.error(
                f"Cannot double click on the element with locator: {locator[1]}, locatorType: {locator[0]}\n"
                f"Exception found: {str(E)}")
            print_stack()

    def element_right_click(self, locator: (By, str) = None, element=None):
        """
        Right click on an element
        Either provide element or a combination of locator and locatorType
        """
        try:
            if element is None:
                element = self.get_element(locator)

            action = ActionChains(self.driver)
            action.context_click(element).perform()
            if locator is not None:
                self.log.info("Right clicked on element with locator: " + locator[1] + " locatorType: " + locator[0])
            else:
                self.log.info("Right clicked on element")
        except Exception as E:
            self.log.error(
                f"Cannot right click on the element with locator: {locator[1]}, locatorType: {locator[0]}\n"
                f"Exception found: {str(E)}")
            print_stack()

    def send_keys(self, data, locator: (By, str) = None, element=None, clear=True, scroll_to=True):
        """
        Send keys to an element -> MODIFIED
        Either provide element or a combination of locator and locatorType
        """
        try:
            if element is None:  # This means if locator is not empty
                element = self.get_element(locator)
            if scroll_to:
                self.page_vertical_scroll(element=element, locator=None)
            if clear:
                element.clear()
                key_combination = Keys.CONTROL, "a"
                element.send_keys(key_combination)
                element.send_keys(Keys.DELETE)
                retries = self.NO_ATTEMPTS
                while len(element.text) > 0 and retries > 0:  # additional wait for more complicated elements
                    time.sleep(0.1)
                    element.clear()
                    retries -= 1
            element.send_keys(data)
            if locator:
                self.log.info(
                    f"Sent data on element with locator: {locator[1]} locatorType: {locator[0]} data: {str(data)}")
            else:
                self.log.info(
                    f"Sent data on element with data: {str(data)}")

        except Exception as E:
            self.log.error(
                f"Cannot send data on the element with locator: {locator[1]} "
                f"locatorType: {locator[0]} data: {str(data)}")
            self.log.error(str(E))
            print_stack()

    def send_keys_to_browser(self, data):
        """
        Send keys to a browser
        """
        actions = ActionChains(self.driver)
        actions.send_keys(data)
        actions.perform()

    def get_text(self, locator: (By, str), element=None, info="") -> str:
        """
        NEW METHOD
        Get 'Text' on an element
        Either provide element or a combination of locator and locatorType
        """
        try:
            if locator:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.get_element(locator)
            self.log.debug("Before finding text")
            text = element.text
            self.log.debug("After finding element, size is: " + str(len(text)))
            if len(text) == 0:
                text = element.get_attribute("innerText")
            if len(text) != 0:
                self.log.info("Getting text on element :: " + info)
                self.log.info("The text is :: '" + text + "'")
                text = text.strip()
        except Exception as E:
            self.log.error("Failed to get text on element " + info)
            self.log.error(str(E))
            print_stack()
            text = None
        return text

    def is_element_present(self, locator: (By, str)) -> bool:
        """
        Check if element is present -> MODIFIED
        Either provide element or a combination of locator and locatorType
        """
        try:
            elements = self.get_element_list(locator)
            self.log.info("Element Found")
            # if element is not None:
            if len(elements) > 0:
                self.log.info(
                    f"is_element_present Method, Element present with locator: {locator[1]} locatorType: {locator[0]}")
                return True
            else:
                self.log.info(
                    f"is_element_present Method, Element not present with locator: {locator[1]} "
                    f"locatorType: {locator[0]}")
                return False
        except Exception as E:
            print("isElementPresent Method, Element not found")
            self.log.error(str(E))
            return False

    def is_element_displayed(self, locator: (By, str), element=None) -> bool:
        """
        NEW METHOD
        Check if element is displayed
        Either provide element or a combination of locator and locatorType
        """
        is_displayed = False
        try:
            if element is None:
                element = self.get_element(locator)

            if element is not None:
                is_displayed = element.is_displayed()
                self.log.info(f"Element is displayed with locator: {locator[1]} locatorType: {locator[0]} ")
            else:
                self.log.info(f"Element not displayed with locator: {locator[1]} locatorType: {locator[0]}")
            return is_displayed
        except Exception as E:
            self.log.error(
                f"Passed locator incorrect or updated, passed locator type : {locator[1]} locatorType: {locator[0]}")
            self.log.error(f"Exception: {str(E)}")
            return False

    def is_element_editable(self, locator: (By, str), element=None) -> bool:
        """
        NEW METHOD
        Check if element is editable
        Either provide element or a combination of locator and locatorType
        """
        is_displayed = False
        try:
            if element is None:
                element = self.get_element(locator)

            if element is not None:

                if not (element.is_enabled() and element.is_displayed()):
                    self.log.info(f"Element not editable with locator: {locator[1]} locatorType: {locator[0]}")
                    return False
                self.log.info(f"Element is editable with locator: {locator[1]} locatorType: {locator[0]} ")
                return True
            else:
                self.log.info(f"Element not editable with locator: {locator[1]} locatorType: {locator[0]}")
            return is_displayed
        except Exception as E:
            self.log.error(
                f"Passed locator incorrect or updated, passed locator type : {locator[1]} locatorType: {locator[0]}")
            self.log.error(f"Exception: {str(E)}")
            return False

    def web_scroll(self, direction="up"):
        """
        NEW METHOD
        """
        if direction == "up":
            # Scroll Up
            self.driver.execute_script("window.scrollBy(0, -1000);")

        if direction == "down":
            # Scroll Down
            self.driver.execute_script("window.scrollBy(0, 1000);")

    def clear_field(self, locator: (By, str) = None, element: WebElement = None):
        try:
            if element is None:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.get_element(locator)
                self.log.debug("Before performing clear action")
                element.clear()
        except Exception as E:
            self.log.error("Failed to get element ")
            self.log.error(str(E))
            print_stack()

    def clear_by_select_all_and_delete(self, locator: (By, str) = None, element: WebElement = None, press_escape=True):
        try:
            if element is None:
                element = self.get_element(locator=locator)
            actions = ActionChains(self.driver)
            actions.click(element)
            actions.key_down(Keys.CONTROL)
            actions.send_keys("a")
            actions.key_up(Keys.CONTROL)
            actions.send_keys(Keys.BACK_SPACE)
            if press_escape:
                actions.send_keys(Keys.ESCAPE)
            actions.perform()
        except Exception as E:
            self.log.error(f"Cannot clear Equipment type field\nException: {E}")

    def replace_text(self, text: str, locator: (By, str) = None, element: WebElement = None):
        try:
            if element is None:
                element = self.get_element(locator=locator)
            actions = ActionChains(self.driver)
            actions.click(element)
            actions.key_down(Keys.CONTROL)
            actions.send_keys("a")
            actions.key_up(Keys.CONTROL)
            actions.send_keys(text)
            actions.send_keys(Keys.ESCAPE)
            actions.perform()
        except Exception as E:
            self.log.error(f"Cannot clear Equipment type field\nException: {E}")

    def replace_text_without_escape(self, text: str, locator: (By, str) = None, element: WebElement = None):
        try:
            if element is None:
                element = self.get_element(locator=locator)
            actions = ActionChains(self.driver)
            actions.click(element)
            actions.key_down(Keys.CONTROL)
            actions.send_keys("a")
            actions.key_up(Keys.CONTROL)
            actions.send_keys(text)
            actions.perform()
        except Exception as E:
            self.log.error(f"Cannot clear Equipment type field\nException: {E}")

    def get_attribute(self, locator: (By, str), attribute_type=""):
        try:

            if locator:  # This means if locator is not empty
                element = self.get_element(locator)
                if element is not None:
                    self.log.info(f"Element is displayed with locator: locator[1] locatorType: {locator[0]}")
                    attribute_value = element.get_attribute(attribute_type)
                    self.log.info(f"Attribute value: {attribute_value}")
                    return attribute_value
                else:
                    self.log.info(f"Element is Not displayed with locator: locator[1] locatorType: {locator[0]}")
                    return None

        except Exception as E:
            self.log.info(f"Element not found with locator: locator[1] locatorType: {locator[0]}")
            self.log.error(str(E))
            print_stack()

    def scroll_into_view(self, element):
        try:
            self.log.info("Inside scroll_into_view Method")
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
        except Exception as E:
            self.log.error("Passed Locator incorrect or element get updated in scroll_into_view Method")
            self.log.error(f"Exception: {str(E)}")

    def scroll_by_action(self, element):
        actions = ActionChains(self.driver)
        actions.move_to_element(element)
        actions.perform()

    def grab_and_move_to(self, element_from, element_to):
        actions = ActionChains(self.driver)
        actions.click_and_hold(element_from)
        actions.move_to_element(element_to)
        actions.release()
        actions.perform()

    def grab_and_move_by_offset(self, element_from, xoffset, yoffset):
        actions = ActionChains(self.driver)
        actions.click_and_hold(element_from)
        actions.move_by_offset(xoffset=xoffset, yoffset=yoffset)
        actions.release()
        actions.perform()

    def page_vertical_scroll(self, locator: (By, str) = None, element=None):
        """
        Scroll the Page
        :param locator: locator to find element, if element is not provided
        :param element: if locator is provided, should be None, otherwise overrides locator
        """
        try:
            if element is None:
                element = self.get_element(locator)
            self.log.info("Inside page_vertical_scroll Method")
            self.driver.execute_script("arguments[0].scrollIntoView();", element)
            # self.driver.execute_script("window.scrollTo({top:arguments[0].getBoundingClientRect().top - 40});",
            #                            element)
        except Exception as e:
            self.log.error("Passed Locator incorrect or element get updated in page_vertical_scroll Method")
            self.log.error("Exception : " + str(e))

    def get_url(self) -> str:
        """
        getting the application Url
        :return: Browser Url
        """
        self.log.info("Inside get_url Method")
        try:
            return self.driver.current_url
        except Exception as E:
            self.log.error("Exception occurred in get_url Method and Exception is : " + str(E))

    def navigate_to_url(self, url):
        """
        Navigating to different URL
        :param url: URL have to navigate
        """
        self.log.info("Inside navigate_to_url Method")
        try:
            self.driver.get(url)
        except Exception as E:
            self.log.error("Exception occurred in navigate_to_url Method and Exception is : " + str(E))

    def refresh_page(self):
        """Refreshing current page"""
        self.log.info("Inside refresh_page Method")
        try:
            self.driver.refresh()
        except Exception as E:
            self.log.error("Exception occurred in refresh_page Method and Exception is : " + str(E))

    def is_element_scrollable(self, locator) -> bool:
        try:
            item = self.get_element(locator)
            scroll_height = item.get_attribute("scroll_height")
            offset_height = item.get_attribute("offset_height")
            return int(scroll_height) > int(offset_height)
        except Exception as E:
            self.log.error("Exception occurred in is_element_scrollable Method and Exception is : " + str(E))

    # TODO: check if it is valid only with input element, take care of @value attribute (inputs not always has text)
    # determine if it can be used in combination with Input class
    def get_input_value(self, locator: (By, str), element=None, info="") -> str:
        """
        NEW METHOD
        Get 'Text' on an element
        Either provide element or a combination of locator and locatorType
        """
        try:
            if locator:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.get_element(locator)
            self.log.debug("Before finding text")
            text = element.get_attribute("value")
            self.log.debug("After finding element, size is: " + str(len(text)))
            if len(text) != 0:
                self.log.info("Getting input value on element :: " + info)
                self.log.info("The text is :: '" + text + "'")
                text = text.strip()
        except Exception as E:
            self.log.error("Failed to get input value on element " + info)
            print_stack()
            text = None
            self.log.error(f"Exception: {str(E)}")
        return text

    def get_textbox_value(self, locator, element=None, info=""):
        """
        Get 'Text' on an element
        Either provide element or a combination of locator and locatorType
        """
        try:
            if locator:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.get_element(locator=locator)
            self.log.debug("Before finding text")
            text = element.get_attribute("value")
            self.log.debug("After finding element, size is: " + str(len(text)))
            if len(text) != 0:
                self.log.info("Getting text on element :: " + info)
                self.log.info("The text is :: '" + text + "'")
                text = text.strip()
        except Exception as E:
            self.log.error("Failed to get text on element " + info)
            self.log.error(f"Exception: {str(E)}")
            print_stack()
            text = None
        return text

    def new_tab(self, url):
        """Opening new Tab in Same browser and trigger the passed URL"""
        try:
            self.driver.execute_script("window.open('');")
            self.driver.switch_to.window(self.driver.window_handles[1])
            self.navigate_to_url(url)
        except Exception as e:
            self.log.error(f"Exception occurred in new_tab Method and Exception is : {str(e)}")

    def switch_window(self, index):
        """switching window based on passed index value. Index value should be in int"""
        try:
            self.driver.switch_to.window(self.driver.window_handles[index])
        except Exception as e:
            self.log.error(F"Exception occurred in switch_window Method and Exception is : {str(e)}")

    def select_based_on_text(self, locator: (By, str), select_value, parent: WebElement = None):
        """
        this method select from drop down based on passed Text
        :param locator: Locator (webelement)
        :param parent: parent element to search inside child list, if None, whole document is searched
        :param select_value: The value want to select in Drop down
        :return: selected Name
        """

        try:
            drop_down_list = self.get_element_list(locator=locator, parent=parent)
            for drop_down_index in drop_down_list:
                if (select_value.isdigit() and drop_down_index.text.isdigit() and
                    int(drop_down_index.text) == int(select_value)) or \
                        drop_down_index.text.lower() == select_value.lower():
                    index_name = drop_down_index.text
                    self.log.info("element Text :" + str(index_name))
                    self.driver.execute_script("arguments[0].scrollIntoView();", drop_down_index)
                    drop_down_index.click()
                    return index_name

            self.log.error(
                f"Either provided value: {select_value} was not found or provided locator: {locator[1]} is incorrect")
        except Exception as E:
            self.log.error("Exception occurred in selectBasedOnText Method and Exception is : " + str(E))

    def wait_for_element_not_exist(self, locator: (By, str), timeout=10, poll_frequency=1):
        end_time = time.time() + timeout
        while True:
            elements = self.get_element_list(locator)
            if len(elements) == 0:
                return
            time.sleep(poll_frequency)
            if time.time() > end_time:
                break

        self.log.info(f"Wait for element not exist timeout after: {timeout} seconds. Element still exist")
        raise Exception(f"Wait for element not exist timeout after: {timeout} seconds. Element still exist")

    ##########################################################################
    # ------------ legacy

    def getByType(self, locatorType):
        locatorType = locatorType.lower()
        if locatorType == "id":
            return By.ID
        elif locatorType == "name":
            return By.NAME
        elif locatorType == "xpath":
            return By.XPATH
        elif locatorType == "css":
            return By.CSS_SELECTOR
        elif locatorType == "class":
            return By.CLASS_NAME
        elif locatorType == "link":
            return By.LINK_TEXT
        elif locatorType == "tagname":
            return By.TAG_NAME
        else:
            self.log.info("Locator type " + locatorType +
                          " not correct/supported")
        return False

    def getElement(self, locator, locatorType="id"):
        by_type = self.getByType(locatorType)
        return self.get_element((by_type, locator))

    def getElementList(self, locator, locatorType="id"):
        by_type = self.getByType(locatorType)
        return self.get_element_list((by_type, locator))

    def elementClick(self, locator="", locatorType="id", element=None):
        by_type = self.getByType(locatorType)
        if element is None:
            element = self.get_element(locator=(by_type, locator))
        self.element_click(element=element, scroll_to=False)

    def sendKeys(self, data, locator="", locatorType="id", element=None):
        by_type = self.getByType(locatorType)
        self.send_keys(data=data, locator=(by_type, locator), element=element, clear=True)

    def getText(self, locator="", locatorType="id", element=None, info=""):
        """
        NEW METHOD
        Get 'Text' on an element
        Either provide element or a combination of locator and locatorType
        """
        try:
            if locator:  # This means if locator is not empty
                self.log.debug("In locator condition")
                element = self.getElement(locator, locatorType)
            self.log.debug("Before finding text")
            return self.get_text(locator=None, element=element, info=info)
        except Exception as E:
            self.log.error("Failed to get text on element " + info)
            self.log.error(f"Exception: {str(E)}")
            print_stack()

    def isElementPresent(self, locator="", locatorType="id"):
        """
        Check if element is present -> MODIFIED
        Either provide element or a combination of locator and locatorType
        """
        by_type = self.getByType(locatorType)
        return self.is_element_present(locator=(by_type, locator))

    def isElementDisplayed(self, locator="", locatorType="id", element=None):
        """
        NEW METHOD
        Check if element is displayed
        Either provide element or a combination of locator and locatorType
        """
        by_type = self.getByType(locatorType)
        return self.is_element_displayed(locator=(by_type, locator), element=element)

    def getAttribute(self, locator="", locatorType="id", attributeType=""):
        by_type = self.getByType(locatorType)
        return self.get_attribute(locator=(by_type, locator), attribute_type=attributeType)

    def pageVerticalScroll(self, locator, locatorType):
        """
        Scroll the Page
        :param locator:
        :param locatorType:
        :return:
        """
        by_type = self.getByType(locatorType)
        self.page_vertical_scroll(locator=(by_type, locator))

    def waitForElement(self, locator, locatorType="id", event="", textValue="",
                       timeout=10, pollFrequency=0.5):
        by_type = self.getByType(locatorType)
        return self.wait_for_element(locator=(by_type, locator),
                                     event=event,
                                     poll_frequency=pollFrequency,
                                     timeout=timeout,
                                     text_value=textValue)
