"""
@package base

WebDriver Factory class implementation
It creates a webdriver instance based on browser configurations

Example:
    wdf = WebDriverFactory(browser)
    wdf.getWebDriverInstance()
"""
import os
from pathlib import Path

from msedge.selenium_tools import Edge
from msedge.selenium_tools import EdgeOptions
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions

import utilities.custom_logger as cl

log = cl.automation_logger()


class WebDriverFactory:

    def __init__(self, browser):
        """
        Inits WebDriverFactory class

        Returns:
            None
        """
        self.browser = browser

    """
        Set chrome driver and iexplorer environment based on OS

        chromedriver = "C:/.../chromedriver.exe"
        os.environ["webdriver.chrome.driver"] = chromedriver
        self.driver = webdriver.Chrome(chromedriver)

        PREFERRED: Set the path on the machine where browser will be executed
    """

    def get_web_driver_instance(self):
        """
       Get WebDriver Instance based on the browser configuration

        Returns:
            'WebDriver Instance'
        """
        log.info(f"Browser:{self.browser.lower()}")

        if self.browser.lower() == "edge":
            driver = self.create_edge_driver()
        elif self.browser.lower() == "firefox":
            driver = self.create_firefox_driver()
        elif self.browser.lower() == "chrome":
            driver = self.create_chrome_driver()
        elif self.browser.lower() == "chrome-grid":
            driver = self.create_chrome_driver_grid()

        else:
            raise Exception("Browser is not defined or webdriver unavailable")

        # Setting Driver Implicit Time out for An Element
        driver.implicitly_wait(2)
        # Maximize the window
        driver.maximize_window()
        # print(driver.get_window_size())
        log.info(f"{os.environ.get('PYTEST_CURRENT_TEST')}| Driver window size:{driver.get_window_size()}")
        # Loading browser with App URL
        # driver.get(baseURL)
        return driver

    @staticmethod
    def create_edge_driver() -> webdriver.Edge:
        if "EDGE_DRIVER" in os.environ:
            driver_path = os.environ["EDGE_DRIVER"]
        else:
            driver_path = str(Path(__file__).parent.parent) + "\\" + "msedgedriver.exe"

        log.info(f"driver_path = {driver_path}")

        options = EdgeOptions()
        options.use_chromium = True
        options.add_argument("--disable-backgrounding-occluded-windows")
        driver = Edge(options=options)

        return driver

    @staticmethod
    def create_firefox_driver() -> webdriver.Firefox:
        if "FIREFOX_DRIVER" in os.environ:
            driver_path = os.environ["FIREFOX_DRIVER"]
        else:
            driver_path = str(Path(__file__).parent.parent) + "\\" + "geckodriver.exe"

        log.info(f"driver_path = {driver_path}")
        driver = webdriver.Firefox(executable_path=driver_path,
                                   service_log_path='nul')

        return driver

    @staticmethod
    def create_chrome_driver() -> webdriver.Chrome:
        """
        Creates Chrome driver based on webdriver.chrome.driver system environment. If not set, takes WebDriver binary
        from the project (Windows only)

        Returns: WebDriver instance
        """
        if "CHROME_DRIVER" in os.environ:
            driver_path = os.environ["CHROME_DRIVER"]
        else:
            driver_path = str(Path(__file__).parent.parent) + "\\" + "chromedriver.exe"

        options = ChromeOptions()
        options.add_argument("--disable-backgrounding-occluded-windows")

        log.info(f"driver_path = {driver_path}")
        driver = webdriver.Chrome(executable_path=driver_path, options=options)

        return driver

    @staticmethod
    def create_chrome_driver_grid() -> webdriver.Remote:
        name_to_set = os.environ.get('PYTEST_CURRENT_TEST').split(':')[0]
        if len(name_to_set) > 63:
            name_to_set = name_to_set[-63:]
        grid_url = "http://13.64.107.252:4444/wd/hub"
        capabilities = {
            "browserName": "chrome",
            "name": name_to_set
        }

        log.info(f"grid url = {grid_url}")
        driver = webdriver.Remote(
            command_executor=grid_url,
            desired_capabilities=capabilities)

        return driver
