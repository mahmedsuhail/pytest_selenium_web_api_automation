from utilities.read_data import readconfig
from utilities.util import Util

config_holder = {}


def read_config(environment, browser, log):
    config = readconfig("defaultconfig")
    if environment is None:
        environment = config.get("global", "environment")
    if browser is None:
        browser = config.get("global", "browser")

    url_row = Util(logger=log).csv_row_data_to_list_search_by_column_values(fileName="environment_url.csv",
                                                                            columnName="environment",
                                                                            searchValue=environment)

    config_holder["environment"] = environment
    config_holder["browser"] = browser
    config_holder["baseurl"] = url_row[0]['baseurl']
    config_holder["customer_api_url"] = url_row[0]['customerurl']
    config_holder["identity_url"] = url_row[0]['identityurl']
    config_holder["lookup_api_url"] = url_row[0]['lookupurl']
    config_holder["order_api_url"] = url_row[0]['orderurl']
    config_holder["product_api_url"] = url_row[0]['producturl']

    url_row = Util(logger=log).csv_row_data_to_list_search_by_column_values(fileName="login_details.csv",
                                                                            columnName="environment",
                                                                            searchValue=environment)
    config_holder["login"] = url_row[0]['userName']
    config_holder["password"] = url_row[0]['password']
    config_holder["email"] = url_row[0]['email']
