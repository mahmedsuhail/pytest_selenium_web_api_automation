from API.api_lookup import APILookup


class APILookupTools:

    def __init__(self, api_lookup: APILookup):
        self.api = api_lookup

    def get_shipment_status_filter_by_name(self, name):
        filters = self.api.get_shipment_status_filter_types()
        return [t['id'] for t in filters if t['displayText'] == name][0]
