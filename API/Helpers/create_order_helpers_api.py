import json
from logging import Logger

from API.Models.cancel_tl_order_model import CancelTLOrderModel
from API.Models.create_tl_order_item_model import CreateTLOrderItemModel
from API.Models.create_tl_order_model import CreateTLOrderModel
from API.Models.create_tl_quote_model import CreateTLQuoteModel
from API.Models.tl_order_details_carrier_model import TLOrderDetailsCarrierModel
from API.Models.tl_order_details_model import TLOrderDetailsModel
from API.Types.order_statuses_dict import OrderStatusDict
from API.api_carrier import APICarrier
from API.api_customer import APICustomer
from API.api_customer_tools import APICustomerTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from API.base_gtz_api import BaseGtzAPI


class CreateOrderHelpersAPI:
    def __init__(self, api: BaseGtzAPI, logger: Logger):
        self.api = api
        self.log = logger
        self.order_api = APIOrderAPI(self.api, self.log)
        self.order_tools = APIOrderTools(self.order_api)

    def get_create_order_model(self, customer_search, create_order_data, create_order_item_data):
        customer_tools = APICustomerTools(APICustomer(api=self.api, log=self.log), log=self.log)
        customer = customer_tools.get_master_customer(customer_search)
        order_model = CreateTLOrderModel(customer, create_order_data)
        order_model.add_item_to_order(CreateTLOrderItemModel(create_order_item_data))

        return order_model

    def get_create_tl_quote_model(self, customer_search, create_tl_quote_data):
        customer_tools = APICustomerTools(APICustomer(api=self.api, log=self.log), log=self.log)
        customer = customer_tools.get_master_customer(customer_search)
        create_quote_model = CreateTLQuoteModel(customer, create_tl_quote_data)

        return create_quote_model

    def get_update_order_model_from_get_order_details_response(self, order_model):
        update_order_model = TLOrderDetailsModel(json_model=order_model)

        return update_order_model

    def get_cancel_order_model(self, customer_bk, order_bk, cancel_order_data):
        cancel_order_model = CancelTLOrderModel(customer_bk, order_bk, cancel_order_data=cancel_order_data)

        return cancel_order_model

    def create_order_through_api(self, customer_name: str, create_tl_order_api_data: json,
                                 create_tl_order_item_api_data: json):
        order_model = self.get_create_order_model(
            customer_name,
            create_tl_order_api_data,
            create_order_item_data=create_tl_order_item_api_data)

        # Create order through API
        create_order = self.order_api.create_order(create_order_model=order_model)
        bol_number = self.order_tools.get_created_order_bol_number(create_order)
        order_bk = self.order_tools.get_created_order_bk_number(create_order)
        customer_bk = self.order_tools.get_created_order_bk_customer(create_order)

        return bol_number, order_bk, customer_bk

    def create_quote_through_api(self, customer_name: str, create_tl_quote_api_data: json):
        create_quote_model = self.get_create_tl_quote_model(
            customer_name,
            create_tl_quote_api_data)

        # Create quote through API
        create_quote_response = self.order_api.create_tl_quote(create_quote_model=create_quote_model)
        quote_bk = self.order_tools.get_created_quote_bk_number(create_quote_response)
        sales_rep_email = self.order_tools.get_created_quote_sales_rep_email(create_quote_response)

        return {"quote_bk": quote_bk, "sales_rep_email": sales_rep_email}

    def prepare_order_with_given_status_in_api(self, customer_name: str, create_tl_order_api_data: json,
                                               create_tl_order_item_api_data: json,
                                               tl_order_details_api_carrier_data: json,
                                               status_id: int) -> int:
        # Create order through API
        bol_number, order_bk, customer_bk = self.create_order_through_api(customer_name, create_tl_order_api_data,
                                                                          create_tl_order_item_api_data)

        order_details = self.order_api.get_order_details(order_bk=order_bk)

        # Assign Carrier through API - setting Pending status
        update_order_model = self.get_update_order_model_from_get_order_details_response(
            order_model=order_details)
        update_order_model.add_carrier(carrier_data=TLOrderDetailsCarrierModel(
            carrier_data=tl_order_details_api_carrier_data))
        update_order_model.set_order_status(status_id=OrderStatusDict.order_statuses["PENDING"])

        carrier_api = APICarrier(self.api, self.log)
        update_order = carrier_api.assign_carrier(order_details_data=update_order_model)

        # Update order through API with a given status
        update_order_model = self.get_update_order_model_from_get_order_details_response(
            order_model=order_details)
        update_order_model.set_order_status(status_id=status_id)

        self.order_api.update_order(update_order_model=update_order_model)

        return bol_number
