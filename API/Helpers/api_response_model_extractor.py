import json

from requests import Response


class APIResponseModelExtractor:

    @staticmethod
    def extract_response_model(response: Response):
        try:
            model = json.loads(response.text)['model']
        except KeyError:
            raise Exception(
                f"API call to {response.url} failed. Could not extract model from response. \n Response: {response.text}")

        return model

    @staticmethod
    def extract_response_model_from_json(response: json):
        try:
            model = response['model']
        except KeyError:
            raise Exception(
                f"Could not extract model from response. \n Response: {str(response)}")

        return model
