import json

from API.Models.product_commodity import ProductCommodity
from API.base_gtz_api import BaseGtzAPI
from API.bearer_auth import BearerAuth


class APIProduct:

    def __init__(self, api: BaseGtzAPI):
        self.api = api

    def add_product_to_customer(self, customer_id: int, product_object: ProductCommodity):
        url = f"{self.api.product_api_url}/Customer/{customer_id}/Product"

        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token),
                                           json=product_object.model)
        return json.loads(response.text)

    def get_products_from_customer(self, customer_id: int, search_filter: str = "", page_size: int = 300):
        url = f"{self.api.product_api_url}/Customer/{customer_id}/Product/List"
        url_params = {"customerBK": customer_id, "searchFilter": search_filter, "pageSize": page_size}

        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token), params=url_params)
        return json.loads(response.text)
