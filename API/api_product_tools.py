from API.Models.product_commodity import ProductCommodity
from API.api_product import APIProduct


class APIProductTools:
    # Store here methods that gives business value operating on APIProduct,

    def __init__(self, api_product: APIProduct):
        self.api = api_product

    def check_if_product_exist_or_add_if_not(self, product_name, customer_id,
                                             product_object_if_not_found: ProductCommodity):
        products = self.api.get_products_from_customer(customer_id, product_name)['model']
        filtered = list(filter(lambda x: x['commodity'] == product_name, products))
        if len(filtered) > 0:
            return True
        self.api.add_product_to_customer(customer_id=customer_id, product_object=product_object_if_not_found)
        products = self.api.get_products_from_customer(customer_id, product_name)['model']
        filtered = list(filter(lambda x: x['commodity'] == product_name, products))
        if len(filtered) > 0:
            return True
        else:
            return False
