import json
from logging import Logger

from API.Helpers.api_response_model_extractor import APIResponseModelExtractor
from API.api_logger import APILogger
from API.base_gtz_api import BaseGtzAPI
from API.bearer_auth import BearerAuth


class APILookup:

    def __init__(self, api: BaseGtzAPI, log: Logger):
        self.api = api
        self.api_logger = APILogger(logger=log)

    def get_shipment_status_filter_types(self):
        url = f"{self.api.lookup_api_url}/Lookup/GetShipmentStatusFilterTypes?quoteModeType=3"

        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token))
        self.api_logger.log_api_call(response)

        return APIResponseModelExtractor.extract_response_model(response)

    def get_custom_board_columns(self):
        url = f"{self.api.lookup_api_url}/Lookup/GetCustomBoardColumns"

        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token))
        self.api_logger.log_api_call(response)

        return APIResponseModelExtractor.extract_response_model(response)
