from base.jsonable import JSONAble


class SaveTruckloadBoardCustomColumnSettingModel(JSONAble):
    def __init__(self, display_index: int, header: str, visibility: bool, width: int, sort_direction: str):
        super().__init__()
        visibility_text = ""
        if visibility:
            visibility_text = "Visible"
        else:
            visibility_text = "Collapsed"
        self.model = \
            {"displayIndex": display_index,
             "header": header,
             "visibility": visibility_text,
             "width": width,
             "sortDirection": sort_direction
             }
