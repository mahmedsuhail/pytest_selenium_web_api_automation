from base.jsonable import JSONAble


class SaveTruckloadBoardCustomColumnFilterSettingModel(JSONAble):
    def __init__(self, column_header: str, filter_operation: str, filter_text: str):
        super().__init__()
        self.model = \
            {"columnHeader": column_header,
             "filterOperation": filter_operation,
             "filterText": filter_text,
             "propertyPath": column_header,
             "selectedDistinctObjects": [filter_text]
             }
