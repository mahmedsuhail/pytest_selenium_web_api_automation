from base.jsonable import JSONAble


class TLOrderDetailsAddressModel(JSONAble):
    def __init__(self, address_data):
        super().__init__()
        self.model = address_data
