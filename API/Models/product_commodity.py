class ProductCommodity:
    def __init__(self, commodity_description: str, weight_amount: int,
                 weight_unit: int, nmfc_number: str, freight_class_id: int, handling_unit_type_id: int = 0,
                 stackable: bool = False, dim_length: int = None, dim_width: int = None, dim_height: int = None,
                 handling_unit_count: int = None, dim_unit_id: int = 0, piece_count: int = None,
                 hazmat_flag: bool = False,
                 hazmat_class_id: int = None, hazmat_group_id: int = None, hazmat_prefix_id: int = None,
                 hazmat_code: str = None, hazmat_chemical_name: str = None, hazmat_emergency_number: str = None):
        if hazmat_flag:
            self.model = {"commodityDescription": commodity_description,
                          "commodity": None,
                          "weight": {"amount": weight_amount, "unitId": weight_unit},
                          "pieceCount": piece_count,
                          "handlingUnitTypeId": handling_unit_type_id, "handlingUnitCount": handling_unit_count,
                          "nmfcNumber": nmfc_number,
                          "hazmat": {"flag": hazmat_flag, "classId": hazmat_class_id, "groupId": hazmat_group_id,
                                     "prefixId": hazmat_prefix_id, "code": hazmat_code,
                                     "chemicalName": hazmat_chemical_name,
                                     "emergencyContactNumber": hazmat_emergency_number},
                          "stackable": stackable,
                          "dim": {"length": dim_length, "width": dim_width, "height": dim_height,
                                  "unitId": dim_unit_id},
                          "freightClassId": freight_class_id}

            # {"commodityDescription": "test2pw1", "commodity": null, "weight": {"amount": 3, "unitId": 1},
            #  "pieceCount": 2, "handlingUnitTypeId": 0, "handlingUnitCount": 1, "nmfcNumber": "1245",
            #  "hazmat": {"flag": true, "classId": 10, "groupId": 2, "prefixId": 0, "code": "2EWE",
            #             "chemicalName": "RE123", "emergencyContactNumber": "(232)323-2323"}, "stackable": true,
            #  "dim": {"length": 4, "width": 5, "height": 6, "unitId": 0}, "freightClassId": 55}

        else:
            self.model = {"commodityDescription": commodity_description,
                          "commodity": None,
                          "weight": {"amount": weight_amount, "unitId": weight_unit},
                          "pieceCount": piece_count,
                          "handlingUnitTypeId": handling_unit_type_id, "handlingUnitCount": handling_unit_count,
                          "nmfcNumber": nmfc_number,
                          "stackable": stackable,
                          "dim": {"length": dim_length, "width": dim_width, "height": dim_height,
                                  "unitId": dim_unit_id},
                          "freightClassId": freight_class_id}

            # {"commodityDescription": "gdx", "commodity": null, "weight": {"amount": "", "unitId": 1}, "pieceCount": "",
            #  "handlingUnitTypeId": 0, "handlingUnitCount": "", "nmfcNumber": "123", "stackable": false,
            #  "dim": {"length": "", "width": "", "height": "", "unitId": 0}, "freightClassId": 60}
