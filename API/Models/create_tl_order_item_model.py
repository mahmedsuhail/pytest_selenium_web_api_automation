from base.jsonable import JSONAble


class CreateTLOrderItemModel(JSONAble):
    def __init__(self, item_data):
        super().__init__()
        self.model = item_data
