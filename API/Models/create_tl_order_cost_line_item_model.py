from base.jsonable import JSONAble


class CreateTLOrderCostLineItemModel(JSONAble):
    def __init__(self, charge_name: str, amount: int, accessorial_id: int):
        super().__init__()
        self.model = \
            {"chargeName": charge_name,
             "amount": amount,
             "accessorialId": accessorial_id}
