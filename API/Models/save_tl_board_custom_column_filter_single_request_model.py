from API.Models.save_tl_board_custom_column_filter_setting_model import SaveTruckloadBoardCustomColumnFilterSettingModel
from API.Models.save_tl_board_custom_column_setting_model import SaveTruckloadBoardCustomColumnSettingModel
from base.jsonable import JSONAble


class SaveTruckloadBoardCustomColumnFilterSingleRequestModel(JSONAble):
    def __init__(self, tl_board_id: int):
        super().__init__()
        self.model = {
            "tlBoardId": tl_board_id,
            "customColumnFilterSettings": [],
            "customColumnSettings": []
        }

    def add_column_setting(self, column_setting_model: SaveTruckloadBoardCustomColumnSettingModel):
        self.model['customColumnSettings'].append(column_setting_model.model)

    def add_column_filter_setting(self, column_setting_model: SaveTruckloadBoardCustomColumnFilterSettingModel):
        self.model['customColumnFilterSettings'].append(column_setting_model.model)
