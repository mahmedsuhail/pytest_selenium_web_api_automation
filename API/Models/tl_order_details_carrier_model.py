from base.jsonable import JSONAble


class TLOrderDetailsCarrierModel(JSONAble):
    def __init__(self, carrier_data):
        super().__init__()
        self.model = carrier_data
