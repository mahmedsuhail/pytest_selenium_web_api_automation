from base.jsonable import JSONAble
import datetime
from datetime import timedelta


class CreateTLQuoteModel(JSONAble):
    def __init__(self, customer_data, create_quote_data):
        super().__init__()

        self.model = create_quote_data
        self.set_customer_data_in_quote(customer=customer_data)
        self._update_dates()

    def set_customer_data_in_quote(self, customer):
        self.model['customerBK'] = customer['customerBK']
        self.model['customerInfo']['id'] = customer['customerBK']
        self.model['customerInfo']['name'] = customer['name']
        self.model['customerInfo']['email'] = customer['customerEmail']
        self.model['customerInfo']['phone'] = customer['phoneNumber']
        self.model['salesRepInfo']['id'] = customer['accountSalesRepID']

    def _update_dates(self):
        date = datetime.date.today() + timedelta(days=2)
        self.model['pickupDate'] = date.strftime("%Y-%m-%d")
        self.model['deliveryDate'] = date.strftime("%Y-%m-%d")
