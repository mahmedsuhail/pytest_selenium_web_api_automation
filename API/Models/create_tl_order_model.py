from API.Models.create_tl_order_cost_line_item_model import CreateTLOrderCostLineItemModel
from API.Models.create_tl_order_item_model import CreateTLOrderItemModel
from base.jsonable import JSONAble


class CreateTLOrderModel(JSONAble):
    def __init__(self, customer_data, create_order_data):
        super().__init__()

        self.model = create_order_data

        self.add_customer_data_to_order(customer=customer_data)
        self.add_cost_line_item(CreateTLOrderCostLineItemModel("Base Rate", 0, 1))
        self.add_revenue_line_item(CreateTLOrderCostLineItemModel("Base Rate", 20, 1))

    def add_customer_data_to_order(self, customer):
        self.model['customerBK'] = customer['customerBK']
        self.model['customerInfo']['id'] = customer['customerBK']
        self.model['customerInfo']['name'] = customer['name']
        self.model['customerInfo']['email'] = customer['customerEmail']
        self.model['customerInfo']['phone'] = customer['phoneNumber']
        self.model['salesRepInfo']['id'] = customer['accountSalesRepID']

    def add_item_to_order(self, item_model: CreateTLOrderItemModel):
        self.model['items'].append(item_model.model)

    def add_cost_line_item(self, cost_line_item: CreateTLOrderCostLineItemModel):
        self.model['cost']['lineItems'].append(cost_line_item.model)
        self.model['cost']['total'] += cost_line_item.model['amount']

    def add_revenue_line_item(self, cost_line_item: CreateTLOrderCostLineItemModel):
        self.model['revenue']['lineItems'].append(cost_line_item.model)
        self.model['revenue']['total'] += cost_line_item.model['amount']
