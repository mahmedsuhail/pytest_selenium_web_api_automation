import json

from API.Models.create_tl_order_cost_line_item_model import CreateTLOrderCostLineItemModel
from API.Models.tl_order_details_address_model import TLOrderDetailsAddressModel
from API.Models.tl_order_details_carrier_model import TLOrderDetailsCarrierModel
from base.jsonable import JSONAble


class TLOrderDetailsModel(JSONAble):
    def __init__(self, json_model):
        super().__init__()
        self.model = json_model

    def set_address(self, address_model: TLOrderDetailsAddressModel):
        self.model['address'] = address_model

    def set_order_status(self, status_id):
        self.model['statusId'] = status_id

    def set_target_cost(self, target_cost):
        self.model['cost']['targetCost'] = target_cost

    def set_mode_type(self, mode_type):
        self.model['modeType'] = mode_type

    def set_order_created_by(self, created_by):
        self.model['createdBy'] = created_by

    def set_cargo_value(self, cargo_value):
        self.model['cargoValue'] = cargo_value

    def add_default_cost_line_item(self, cost_line_item: CreateTLOrderCostLineItemModel):
        self.model['cost']['lineItems'] = []
        self.model['cost']['lineItems'].append(cost_line_item.model)
        self.model['cost']['total'] += cost_line_item.model['amount']

    def add_default_revenue_line_item(self, cost_line_item: CreateTLOrderCostLineItemModel):
        self.model['revenue']['lineItems'] = []
        self.model['revenue']['lineItems'].append(cost_line_item.model)
        self.model['revenue']['total'] += cost_line_item.model['amount']

    def add_carrier(self, carrier_data: TLOrderDetailsCarrierModel):
        self.model['carrierBK'] = carrier_data.model['carrierBK']
        self.model['carrierName'] = carrier_data.model['carrierName']
        self.model['carrier'] = carrier_data.model['carrier']

        self.clear_carrier_code()
        self.clear_carrier_servicetype_name()

    def clear_carrier_code(self):
        self.model['carrierCode'] = None

    def clear_carrier_servicetype_name(self):
        self.model['carrierServiceTypeName'] = None

    def add_documents(self, documents: json):
        self.model['documents'] = documents['documents']
