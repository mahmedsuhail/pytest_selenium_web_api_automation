from datetime import datetime
from base.jsonable import JSONAble


class CancelTLOrderModel(JSONAble):
    def __init__(self, customer_bk, order_bk, cancel_order_data):
        super().__init__()
        self.model = cancel_order_data
        self.model['customerBK'] = customer_bk
        self.model['orderBK'] = order_bk
        self.model['truckLoadNoteDetails']['createdDate'] = datetime.utcnow().isoformat() + 'Z'
