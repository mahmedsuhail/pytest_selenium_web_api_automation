import base64
import hashlib
import urllib.parse

from requests_html import HTMLSession

from API.bearer_auth import BearerAuth
from utilities.util import Util


class APILogin:

    def __init__(self, client: HTMLSession, environment, api):
        self.api = api
        self.client_id = "nVb50ZPc0i2BtVSxzbyeTJpfCKEAP1XwycmgSqShux4"
        self.state_id = self.__generate_state_id(96)
        self.code_challenge = self.__generate_code_challenge(self.state_id)
        self.environment = environment

    def login_to_application(self):
        # First call
        url = self.api.identity_url + "/connect/authorize"
        return_url = {"client_id": self.client_id,
                      "redirect_uri": self.api.base_url + "/callback",
                      "response_type": "code",
                      "scope": "openid profile",
                      "state": self.state_id,
                      "code_challenge": self.code_challenge,
                      "code_challenge_method": "S256",
                      "response_mode": "query"}
        url_params = urllib.parse.urlencode(return_url, quote_via=urllib.parse.quote)
        response = self.api.client.request(method="GET", url=url, params=url_params, verify=False)

        self.api.logger.info(
            f"Login to application using API, step 1 - call to url /connect/authorize "
            f"response status code: {response.status_code} ")

        # Second call
        string_content = str(response.content)
        request_verification_token = \
            self.api.util.get_string_between(string_content,
                                             '<input name="__RequestVerificationToken" type="hidden" value="',
                                             '" /><input name="RememberLogin"')
        return_url_from_response = \
            self.api.util.get_string_between(string_content,
                                             '<input type="hidden" id="ReturnUrl" name="ReturnUrl" value="',
                                             '"').replace('amp;', '')

        form_params = {"ReturnUrl": return_url_from_response,
                       "Username": self.api.login,
                       "Password": self.api.password,
                       "button": "login",
                       "__RequestVerificationToken": request_verification_token,
                       "RememberLogin": "false"}
        response = self.api.client.request(method="POST", url=response.html.url, data=form_params, verify=False)

        self.api.logger.info(
            f"Login to application using API, step 2 - call to url: {response.html.url} "
            f"response status code: {response.status_code}")

        # Third call
        url = self.api.identity_url + "/connect/token"
        code = self.api.util.get_string_between(response.url, 'code=', '&')
        form_params = {"client_id": self.client_id,
                       "code": code,
                       "redirect_uri": self.api.base_url + "/callback",
                       "code_verifier": self.state_id,
                       "grant_type": "authorization_code"}
        response_identity = self.api.client.request(method="POST", url=url, data=form_params, verify=False)
        self.api.logger.info(
            f"Login to application using API, step 3 - call to url: /connect/token "
            f"response status code: {response_identity.status_code}")

        return response_identity

    def get_user_info(self):
        url = self.api.identity_url + "/connect/userinfo"
        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token), verify=False)
        self.api.logger.info(
            f"Login to application using API - call to url: /connect/userinfo "
            f"response status code: {response.status_code}")

        return response

    def __generate_state_id(self, length):
        return Util.get_random_alpha_numeric(length, "mix")

    def __generate_code_challenge(self, state_id):
        hashed = hashlib.sha256(state_id.encode('ascii')).digest()
        encoded = base64.urlsafe_b64encode(hashed)
        code_challenge = encoded.decode('ascii')[:-1]
        return code_challenge
