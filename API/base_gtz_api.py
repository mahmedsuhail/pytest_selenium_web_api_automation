import asyncio
import datetime
import json
import time
from logging import Logger

from requests_html import HTMLSession

from API.api_login import APILogin
from base.config_holder import config_holder
from base.webdriverfactory import WebDriverFactory
from utilities.util import Util


class BaseGtzAPI:
    def __init__(self, environment, logger: Logger):
        self.client = HTMLSession()
        self.environment = environment
        self.logger = logger
        self.util = Util(logger)

        self.__get_urls_by_environment()
        self.__get_login_details_by_environment()

        self.identity_response = None
        self.token = None
        self.get_user_info = None
        self.profile = None

        self.api_login = APILogin(self.client, self.environment, self)

    def open_connection(self):
        self.logger.info("Call open_connection")
        self.identity_response = self.api_login.login_to_application()
        self.token = json.loads(self.identity_response.text)['access_token']
        self.get_user_info = self.api_login.get_user_info()
        self.profile = json.loads(self.get_user_info.text)

    def open_logged_in_selenium_driver(self, browser):
        loop = asyncio.get_event_loop()
        loop.run_in_executor(None, self.open_connection)

        wdf = WebDriverFactory(browser)
        self.driver = wdf.get_web_driver_instance()

        if not self.__wait_for_api_login(60):
            self.logger.error("Failed to login using API")
            self.driver.quit()
            raise Exception("Failed to login using API")

        identity_json = json.loads(self.identity_response.text)
        expires_at = int(datetime.datetime.now().timestamp()) + identity_json["expires_in"]
        identity_json["expires_at"] = expires_at
        del identity_json["expires_in"]
        identity_json["profile"] = self.profile

        self.driver.get(self.base_url)
        self.driver.execute_script(
            f"window.localStorage.setItem('oidc.user:{self.identity_url}:{self.api_login.client_id}','" +
            json.dumps(identity_json) + "');")

        return self.driver

    def __wait_for_api_login(self, timeout=60):
        period = 1  # seconds
        must_end = time.time() + timeout
        while time.time() < must_end:
            if self.token is not None and self.profile is not None:
                return True
            time.sleep(period)
        return False

    def __get_urls_by_environment(self):
        self.base_url = config_holder['baseurl']
        self.customer_api_url = config_holder['customer_api_url']
        self.identity_url = config_holder['identity_url']
        self.lookup_api_url = config_holder['lookup_api_url']
        self.order_api_url = config_holder['order_api_url']
        self.product_api_url = config_holder['product_api_url']

    def __get_login_details_by_environment(self):
        self.login = config_holder['login']
        self.password = config_holder['password']
