import json
from logging import Logger

from API.base_gtz_api import BaseGtzAPI
from API.bearer_auth import BearerAuth
from API.api_logger import APILogger


class APICustomer:

    def __init__(self, api: BaseGtzAPI, log: Logger):
        self.api = api
        self.api_logger = APILogger(logger=log)

    def get_list_customers(self, search_value):
        url = f"{self.api.customer_api_url}/Customer/CustomerList"
        params = dict()
        params['searchValue'] = search_value
        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token),
                                           params=params, verify=False)
        self.api_logger.log_api_call(response)
        return json.loads(response.text)
