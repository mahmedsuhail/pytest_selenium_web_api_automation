from API.Helpers.api_response_model_extractor import APIResponseModelExtractor
from API.api_customer import APICustomer


class APICustomerTools:

    def __init__(self, api: APICustomer, log):
        self.api = api
        self.log = log

    def get_master_customer(self, search_value):
        response = self.api.get_list_customers(search_value)
        return [x for x in APIResponseModelExtractor.extract_response_model_from_json(response) if x['isMasterCustomer']][0]
