from datetime import datetime, timedelta

from API.Models.save_tl_board_custom_column_setting_model import SaveTruckloadBoardCustomColumnSettingModel
from API.api_order_api import APIOrderAPI


class APIOrderTools:

    def __init__(self, api: APIOrderAPI):
        self.api = api

    def get_random_order_bol(self, shipment_status_filter_id):
        date_from = datetime.today() - timedelta(days=365)
        date_to = datetime.today() + timedelta(days=30)
        result = self.api.get_tl_order_board(from_date=date_from, to_date=date_to, page_number=1, page_size=25,
                                             shipment_status_filter_type_id=shipment_status_filter_id)[0]
        return result['billOfLading']

    def get_created_order_bol_number(self, create_order_response):
        return create_order_response['orderModel']['bolNumber']

    def get_created_order_bk_number(self, create_order_response):
        return int(create_order_response['orderModel']['orderBK'])

    def get_created_order_bk_customer(self, create_order_response):
        return create_order_response['orderModel']['customerBK']

    def get_created_quote_bk_number(self, create_quote_response):
        return create_quote_response['orderModel']['quoteBK']

    def get_created_quote_sales_rep_email(self, create_quote_response):
        return create_quote_response["orderModel"]["salesRepInfo"]["email"]

    def get_custom_column_setting_to_be_saved(self, custom_column_setting_from_get_boards_model, visibilty_to_change):
        return SaveTruckloadBoardCustomColumnSettingModel(
            display_index=custom_column_setting_from_get_boards_model['displayIndex'],
            header=custom_column_setting_from_get_boards_model['header'],
            visibility=visibilty_to_change,
            width=custom_column_setting_from_get_boards_model['width'],
            sort_direction=custom_column_setting_from_get_boards_model['sortDirection']
        )
