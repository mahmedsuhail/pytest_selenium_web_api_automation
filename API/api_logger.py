from logging import Logger


class APILogger:
    def __init__(self, logger: Logger):
        self.log = logger

    def log_api_call(self, response):
        self.log.info(
            f"API call to: {response.url} "
            f"\n Request: {response.request.method} {response.request.url} {response.request.body} "
            f"\n Response: {response.status_code} {response.text}")
