from API.Helpers.api_response_model_extractor import APIResponseModelExtractor
from API.Models.tl_order_details_model import TLOrderDetailsModel
from API.api_logger import APILogger
from API.base_gtz_api import BaseGtzAPI
from API.bearer_auth import BearerAuth


class APICarrier:

    def __init__(self, api: BaseGtzAPI, log):
        self.api = api
        self.api_logger = APILogger(logger=log)

    def assign_carrier(self, order_details_data: TLOrderDetailsModel):
        url = self.api.order_api_url + "/Carrier/Assign"
        data = order_details_data.model
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)
