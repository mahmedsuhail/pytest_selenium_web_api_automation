import json
from datetime import datetime
from logging import Logger

from API.Helpers.api_response_model_extractor import APIResponseModelExtractor
from API.Models.cancel_tl_order_model import CancelTLOrderModel
from API.Models.create_tl_order_model import CreateTLOrderModel
from API.Models.create_tl_quote_model import CreateTLQuoteModel
from API.Models.save_tl_board_custom_column_filter_single_request_model import \
    SaveTruckloadBoardCustomColumnFilterSingleRequestModel
from API.Models.save_tl_board_custom_request_model import SaveTruckloadBoardCustomRequestModel
from API.Models.tl_order_details_model import TLOrderDetailsModel
from API.api_logger import APILogger
from API.base_gtz_api import BaseGtzAPI
from API.bearer_auth import BearerAuth


class APIOrderAPI:

    def __init__(self, api: BaseGtzAPI, log: Logger):
        self.api = api
        self.api_logger = APILogger(logger=log)

    def get_order_board(self, datetime_from: datetime, datetime_to: datetime, page_number: int, page_size: int):
        url = self.api.order_api_url + "/GetOrderBoard"

        data = {"fromDate": datetime_from.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "toDate": datetime_to.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "pageNumber": page_number, "pageSize": page_size, "searchFilter": [], "shipmentStatusFilterTypeId": 0}
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return json.loads(response.text)

    def get_tl_order_board(self, from_date: datetime, to_date: datetime, page_number: int, page_size: int,
                           shipment_status_filter_type_id: int):
        url = self.api.order_api_url + "/GetTlOrderBoard"
        data = {"fromDate": from_date.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "toDate": to_date.strftime("%Y-%m-%dT%H:%M:%SZ"),
                "pageNumber": page_number, "pageSize": page_size, "searchFilter": [],
                "shipmentStatusFilterTypeId": shipment_status_filter_type_id}
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def create_order(self, create_order_model: CreateTLOrderModel):
        url = self.api.order_api_url + "/Order/CreateOrder"
        data = create_order_model.model
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def create_tl_quote(self, create_quote_model: CreateTLQuoteModel):
        url = self.api.order_api_url + "/Order/CreateTruckLoadQuote"
        data = create_quote_model.model
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def get_order_details(self, order_bk):
        url = self.api.order_api_url + f"/Order/GetOrderDetails?orderBK={order_bk}&modeType=3"
        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token), verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def get_quote_details(self, quote_bk):
        quote_bk = quote_bk - 1000  # we have to subtract 1000 from quoteBK when getting quote
        # details (confirmed with Frontend team)

        url = self.api.order_api_url + f"/Order/GetOrderDetails?modeType=3&quoteBK={quote_bk}"
        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token), verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def update_order(self, update_order_model: TLOrderDetailsModel):
        url = self.api.order_api_url + "/Order/UpdateOrder"
        data = update_order_model.model
        response = self.api.client.request(method="PUT", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def cancel_order(self, cancel_order_model: CancelTLOrderModel):
        url = self.api.order_api_url + "/Order/CancelOrder"
        data = cancel_order_model.model
        response = self.api.client.request(method="PUT", url=url, auth=BearerAuth(self.api.token), json=data,
                                           verify=False)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def get_tl_boards_customization(self):
        url = self.api.order_api_url + "/GetTLBoardsCustomization"
        response = self.api.client.request(method="GET", url=url, auth=BearerAuth(self.api.token))
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def save_truckload_board_customization(self, save_tl_board_custom_model: SaveTruckloadBoardCustomRequestModel):
        url = self.api.order_api_url + "/SaveTruckloadBoardCustomization"
        data = save_tl_board_custom_model.model
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)

    def save_tl_custom_column_filters_single(self,
                                             save_tl_board_custom_model:
                                             SaveTruckloadBoardCustomColumnFilterSingleRequestModel):
        url = self.api.order_api_url + "/SaveTLCustomColumnFiltersSingle"
        data = save_tl_board_custom_model.model
        response = self.api.client.request(method="POST", url=url, auth=BearerAuth(self.api.token), json=data)
        self.api_logger.log_api_call(response)
        return APIResponseModelExtractor.extract_response_model(response)
