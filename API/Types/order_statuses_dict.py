class OrderStatusDict:
    order_statuses = {
        "QUOTED": 3,
        "BOOKED": 6,
        "IN TRANSIT": 7,
        "PAPERWORK": 8,
        "DELIVERED": 17,
        "PENDING": 18,
        "AT SHIPPER": 19,
        "AT CONSIGNEE": 20,
        "PROBLEM": 50,
        "CUSTOMER HOLD": 51,
        "DISPATCH": 60
    }
