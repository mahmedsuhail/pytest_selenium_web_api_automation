#!/bin/sh

grep -r -h -A 1 mark.jira tests/** | sed 's/@pytest.mark.jira("/https:\/\/globaltranz.atlassian.net\/browse\//g' | sed 's/")//g' | sed 's/def //g' | sed 's/(self)://g' | grep -v -- "^--$" > logs/skipped_tests.txt

python3 utilities/skipped_tests.py logs/skipped_tests.txt

