
# UI Automation 

## Project structure

Please refer also to [architecture section](#architecture).

### Modules

#### base

* webdriverfactory - responsible for creation of WebDriver object, according to configuration. 
* selenium_driver - browser operations, controls operations

#### configfiles

#### pages

```
base
  |__ basepage.py - parent class for page objects, common controls present on every page
basictypes - class representaitons of basic controls like checkboxes, text inputs, date time pickers, etc
[subpage_name] - page objects for certain subpages, like create_quote, home, salesorderboard, etc
```

#### SeleniumGrid

#### testData

#### TL and LTL

Test suites for TL and LTL

TODO: Subject to refactor, more sense will make structure:

```
Tests
  - TL
  - LTL
``` 

#### Files in root directory

* webdrivers - to be removed, there is no point to store them in git.
* pytest.ini - configuration for **pytest**
* setup.cfg - configuration for **pycodestyle** and other tools  

## Code style

For transition period the old code can remain as it is. Any new added code should be compliant with the following rules.

### 1. Modules

Each "new style" modules are placed in v2 subdir. Eventually, it is to be moved to main module.

`__init__.py` files - unless any initialisation is done, there is no need to keep these files.

### 2. Code naming

#### 2.1 Locators

Locators should be defined as constants outside of the class, as a pair build of (By, str). This way there is no need to provide locator type to the function.

Example:

```python
ROUTE_DETAILS_INPUT = (By.XPATH, "//*[@id='routeDetails_TL-595_inputField_1']//input")
```

#### 2.2 Methods naming

Methods should be named according to PEP8 specification, using snake_case style.

Each method returning value should have an indication of returned type specified.

```python
def is_checked(self) -> bool:
    return True
```

#### 2.3 Variables

Naming convention should follow snake_case rule.

##### 2.3.1 Private variables 

Private variables should be prefixed with '_' sign - they should not be accesses from outside of the class. 

##### 2.3.2 Public variables

Using of public class variables should be avoided, rather access through a method is prefferred. 

##### 2.3.3 Method parameters 

Method parameters should not be prefixed with '_' and should contain a type hint.

```python
def verify_page_title(self, title_to_verify: str):
```

More info about typing: [https:/docs.python.org - python typing](https://docs.python.org/3/library/typing.html)


## <a name="architecture"></a>Architecture

### Test Generation Layer

Generation of tests bases on Jira test cases. Test generation is done manually. Thus, it is outside of the scope of this document. 

### Test Definition Layer

There are 2 major groups of testcases, either for **Less Than Truckload** and **Truckload**. 

### Test Execution Layer

#### Commandline execution

Pytest command line command:

TODO: add pytest command

For local execution define environment variable to specify webdriver:
* `CHROME_DRIVER=/path/to/chromedriver`  

Sample pytest command:

```
CHROME_DRIVER=c:\webdrivers\chrome\chromedriver.exe pytest 
```

#### Logging and reporting

### Test Adaptation Layer

#### Components
* Selenium Web Driver 
* Selenoid 

#### Selenium Grid

There is a `docker-compose.yml` file provided, please check SeleniumGrid directory. 

To run docker-compose locally:
1. Install docker-compose on your system: [docs.docker.com - install docker compose](https://docs.docker.com/compose/install/)
2. Run docker compose using command: `docker-compose up`
3. Launch test 

TODO: pytest launch with docker-compose