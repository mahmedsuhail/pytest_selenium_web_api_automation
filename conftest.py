import os
import re
import time

import pytest

import utilities.custom_logger as cl
from API.base_gtz_api import BaseGtzAPI
from base.webdriverfactory import WebDriverFactory
from base.config_holder import config_holder
from base.config_holder import read_config

log = cl.automation_logger()


@pytest.fixture()
def setUp():
    log.info("Running method level setUp")

    yield

    log.info("Running method level tearDown")


@pytest.fixture(scope="class")
def oneTimeSetUp(request, browser, environment):
    log.info("Running one time setUp")
    log.info(f"Passed environment : {environment}")
    log.info(f"passed browser : {browser}")
    if browser is None:
        browser = "chrome"
        log.info("Browser value passed as empty so update with default value : " + str(browser))

    if environment is None:
        environment = "sg"
        log.info("Environment value passed as empty so update with default value : " + str(environment))

    wdf = WebDriverFactory(browser)
    driver = wdf.get_web_driver_instance()
    read_config(environment=environment, log=log, browser=browser)

    driver.get(config_holder["baseurl"])
    log.info(f"Running on {config_holder['environment']} Env")

    if request.cls is not None:
        request.cls.driver = driver
        request.cls.url = config_holder["baseurl"]
        request.cls.config_holder = config_holder

    yield driver

    driver.quit()
    log.info(f"{os.environ.get('PYTEST_CURRENT_TEST').split(':')[0]}| Running one time tearDown")


@pytest.fixture(scope="class")
def oneTimeSetUpWithAPI(request, browser, environment):
    log.info("Running one time setUp with API")
    log.info("Passed environment : " + str(environment))
    log.info("passed browser : " + str(browser))
    if browser is None:
        browser = "chrome"
        log.info("Browser value passed as empty so update with default value : " + str(browser))

    if environment is None:
        environment = "qa"
        log.info("Environment value passed as empty so update with default value : " + str(environment))

    read_config(environment=environment, browser=browser, log=log)

    api = BaseGtzAPI(environment, logger=log)
    driver = api.open_logged_in_selenium_driver(browser)

    if request.cls is not None:
        request.cls.driver = driver
        request.cls.api = api
        request.cls.config_holder = config_holder

    yield driver

    driver.quit()
    log.info(f"{os.environ.get('PYTEST_CURRENT_TEST').split(':')[0]}| Running one time tearDown")


@pytest.fixture(scope="class")
def oneTimeSetUp2(request, browser, environment):
    log.info("Running one time manage user setUp")
    read_config(environment=environment, browser=browser, log=log)

    wdf = WebDriverFactory(config_holder["browser"])
    driver = wdf.get_web_driver_instance()

    driver.get(config_holder["identity_url"] + "/usermanagement/listusers")

    if request.cls is not None:
        request.cls.driver = driver
        request.cls.url = config_holder["baseurl"]
        request.cls.config_holder = config_holder

    yield driver

    driver.quit()
    log.info("Running one time manage user tearDown")


def pytest_addoption(parser):
    parser.addoption("--browser")
    parser.addoption("--environment")
    parser.addoption("--osType", help="Type of operating system")


@pytest.fixture(scope="session")
def browser(request):
    return request.config.getoption("--browser")


@pytest.fixture(scope="session")
def environment(request):
    return request.config.getoption("--environment")


@pytest.fixture(scope="session")
def osType(request):
    return request.config.getoption("--osType")


# set up a hook to be able to check if a test has failed
@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"

    setattr(item, "rep_" + rep.when, rep)


# check if a test has failed
@pytest.fixture(scope="function", autouse=True)
def test_failed_check(request, browser, environment):
    yield

    # request.node is an "item" because we use the default
    # "function" scope
    if request.node.rep_setup.failed:
        log.error("setting up a test failed!", request.node.nodeid)
    elif request.node.rep_setup.passed:
        if request.node.rep_call.failed:
            driver = request.cls.driver
            screen_shot(driver=driver, test_class_name=request.node.nodeid, result_message="Assertion failure")


def screen_shot(driver, test_class_name, result_message):
    """
    Takes screenshot of failed test
    """

    test_class_name = test_class_name.split(':')[-1]

    file_name = f"{driver.capabilities['browserName'].upper()}_{test_class_name}_{result_message}" \
                f"_{str(round(time.time()))}.png"
    file_name = re.sub(r'(?u)[^-\w.]', '', file_name)
    screenshot_directory = "screenshots/"
    relative_file_name = screenshot_directory + file_name
    current_directory = os.path.dirname(__file__)
    destination_file = os.path.join(current_directory, relative_file_name)
    destination_directory = os.path.join(current_directory, screenshot_directory)

    try:
        if not os.path.exists(destination_directory):
            os.makedirs(destination_directory)
        driver.save_screenshot(destination_file)
    except Exception as E:
        log.error("### Exception Occurred when taking screenshot")
        log.error(str(E))
