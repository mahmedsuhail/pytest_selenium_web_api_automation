from pages.basepage import BasePage
from pages.home.TMSlogin_page import TMSLoginPage


class TMSLogoutPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.lp = TMSLoginPage(self.driver, log)

    # Locators
    # ----------------------- Logout Page Locators----------------------------------
    _loggedin_usericon = "(//button[contains(@class,'nav-buttonstyle__StyledNavButton')])[1]"
    _logout_button = "//div[contains(@class,'dropdown-nav-buttonstyles__DropdownRow')]"

    # ------------------------------------- Logout Page Common Method-------------------------------------------

    def clickLoggedinUserIcon(self):
        """
            Clicking on the Logged-in user's Icon.
        """
        self.log.info("Click logged-in User icon")
        self.elementClick(self._loggedin_usericon, locatorType="xpath")
        self.waitForElement(self._logout_button, "xpath", "display")

    def clickLogoutButton(self):
        """
            Clicking on the Logout button.
        """
        self.log.info("Click Logout Button")
        self.elementClick(self._logout_button, locatorType="xpath")

    def verifyLogoutSuccessful(self, base_url="https://gtz-wus-identityserver-web-sg.azurewebsites.net/"):
        """
            Validating the Login page after user logged out from the application.
        :return: if title is correct then true else false
        """
        self.log.info("Inside verifyLogoutSuccessful Method")
        self.lp.waitToLoadLoginPage()

        pageTitleResult = self.lp.verifyTitle()
        userNameFieldResult = self.isElementPresent(self.lp._userName_field, locatorType="id")
        loggedinUserIconResult = self.isElementPresent(self._loggedin_usericon, locatorType="xpath")

        if pageTitleResult and userNameFieldResult and not loggedinUserIconResult:
            return True
        else:
            return False

    # ------------------------------------- Logout Page Method----------------------------

    def logout(self):
        """
            Method to execute the Logout functionality.
        """
        self.log.info("Inside Logout Method")
        self.clickLoggedinUserIcon()
        self.clickLogoutButton()
