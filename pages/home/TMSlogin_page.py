import time

from selenium.webdriver.common.by import By

from pages.basepage import BasePage
from utilities.validators import Validators


class TMSLoginPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.driver = driver
        self.log = log

    # Locators
    _userName_field = "Username"
    _password_field = "Password"
    _login_button = "button[value='login']"
    _so_board = "//div[text()='Sales Order Board']"
    # _validation_message = "div[class='danger validation-summary-errors'] li"
    _validation_message = "p[class='danger']"

    def enterUserName(self, userName):
        self.sendKeys(userName, self._userName_field)

    def enterPassword(self, password):
        self.sendKeys(password, self._password_field)

    def clickLoginButton(self):
        self.log.info("Click Login Button")
        self.elementClick(self._login_button, locatorType="css")

    def login(self, userName="", password=""):
        self.log.info("Inside Login Method")
        self.waitToLoadLoginPage()
        self.enterUserName(userName)
        self.enterPassword(password)
        self.clickLoginButton()

    def verifyLoginFailed(self):
        self.log.info("Inside verifyLoginFailed Method")
        result = self.isElementPresent("//li[text()='Invalid username or password']", locatorType="xpath")
        return result

    def verifyTitle(self):
        self.log.info("Inside Verify Title Method")
        return self.verify_page_title("GlobalTranz TMS")

    # method added due to the bug LTL1-3418.Remove the method once bug is fixed.
    def verifyTitleLogout(self):
        self.log.info("Inside Verify Title Logout Method")
        return self.verify_page_title("IdentityServer4")

    def verifyDisplayErrorMessage(self, expectedText):
        self.log.info("Inside verifyDisplayErrorMessage Method")
        actualText = self.getText(self._validation_message, locatorType="css")
        print("Actual Text : " + str(actualText))
        self.log.info("Actual Text : " + actualText)
        result = Validators(logger=self.log).verify_text_match(actualText, expectedText)
        return result

    def clearLoginPassword(self, locator):
        self.clear_field(locator=(By.ID, locator))

    def verifyLoginSuccessful(self):
        self.waitForElement(self._so_board, locatorType="xpath", event="display", timeout=20)
        time.sleep(2)
        result = self.isElementPresent(self._so_board, locatorType="xpath")
        return result

    def waitToLoadLoginPage(self):
        self.log.info("Inside Wait Method")
        self.waitForElement("logo", locatorType="id", timeout=15)
        # self.waitForElement(self._login_button, locatorType="css", event="")
        # "//button[contains(text(),'Login with Azure AD')]"
