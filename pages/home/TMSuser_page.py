from pages.basepage import BasePage
from selenium.webdriver.common.by import By


class TMSUserPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # Locators
    _userName_field = "Username"
    _password_field = "Password"
    _login_button = "button[value='login']"
    _unlock_button = "//button[text()='Unlock']"
    _so_board = "//div[text()='Sales Order Board']"
    _locked_user = '//a[@href="/UserManagement/UnLockUser?UserID=ff047163-7335-493e-84d6-80c5d8820013"]'
    _user_list_header = "//h2[text()='List Users']"

    # _validation_message = "div[class='danger validation-summary-errors'] li"
    _validation_message = "p[class='danger']"
    _header_Text = "List Users"

    def enterUserName(self, userName):
        self.sendKeys(userName, self._userName_field)

    def enterPassword(self, password):
        self.sendKeys(password, self._password_field)

    def clickLoginButton(self):
        self.log.info("Click Login Button")
        self.elementClick(self._login_button, locatorType="css")

    def clickUnlockButton(self):
        try:
            self.log.info("Click Unlock Button")
            self.waitForElement(self._unlock_button, locatorType="xpath", event="display")
            if self.isElementPresent(self._unlock_button, "xpath"):
                self.elementClick(self._unlock_button, locatorType="xpath")
                return True
            else:
                self.log.error("Unlock Button not Display")
                return False
        except Exception as E:
            self.log.error("Exception occurred in clickUnlockButton Method and Exception is : " + str(E))

    def clickLockedUserLink(self, userName):
        try:
            self.log.info("Click Unlock Button")
            self.waitForElement(self._user_list_header, "xpath", event="display")
            self.pageVerticalScroll("//td[contains(text(),'" + userName + "')]", "xpath")
            element = self.isElementPresent("//td[contains(text(),'" + userName + "')]/following-sibling::td[3]/a", "xpath")
            self.log.info("Element Present: " + str(element))
            if element:
                self.elementClick("//td[contains(text(),'" + userName + "')]/following-sibling::td[3]/a", "xpath")
                return True
            else:
                self.log.error("hyperlink not present")
                return False
        except Exception as E:
            self.log.error("Exception Occurred in clickLockedUserLink Method and Exception is : " + str(E))

    def login(self, userName="", password=""):
        self.log.info("Inside Login Method")
        self.waitToLoadLoginPage()
        self.enterUserName(userName)
        self.enterPassword(password)
        self.clickLoginButton()

    def verifyLoginFailed(self):
        self.log.info("Inside verifyLoginFailed Method")
        result = self.isElementPresent("//li[text()='Invalid username or password']", locatorType="xpath")
        return result

    def verifyTitle(self):
        self.log.info("Inside Verify Title Method")
        return self.verify_page_title("GlobalTranz TMS")

    def verifyDisplayErrorMessage(self, expectedText):
        self.log.info("Inside verifyDisplayErrorMessage Method")
        actualText = self.getText(self._validation_message, locatorType="css")
        print("Actual Text : " + str(actualText))
        self.log.info("Actual Text : " + actualText)
        result = self.validators.verify_text_match(actualText, expectedText)
        return result

    def clearLoginPassword(self, locator):
        self.clear_field(locator=(By.ID, locator))

    def verifyLoginSuccessful(self):
        self.waitForElement(self._so_board, locatorType="xpath", event="display")
        result = self.isElementPresent(self._so_board, locatorType="xpath")
        return result

    def waitToLoadLoginPage(self):
        self.log.info("Inside Wait Method")
        self.waitForElement("logo", locatorType="id")
        # self.waitForElement(self._login_button, locatorType="css", event="")
        # "//button[contains(text(),'Login with Azure AD')]"

    def goToNewUrl(self, url):
        self.navigate_to_url(url)

    def unlockUser(self, userName):
        linkStatus = self.clickLockedUserLink(userName)
        unlockButton = self.clickUnlockButton()
        return linkStatus and unlockButton

    def verifyListUserLogin(self):
        """
        verifying the login successful
        :return:
        """
        try:
            self.log.info("Inside verifyListUserLogin Method")
            self.waitForElement("logo", locatorType="id", event="notdisplay")
            getHeaderText = self.getText(self._user_list_header, "xpath")
            if getHeaderText.lower() == self._header_Text.lower():
                return True
            else:
                return False
        except Exception as E:
            self.log.error("Exception occurred in verifyListUserLogin Method and Exception is : " + str(E))
