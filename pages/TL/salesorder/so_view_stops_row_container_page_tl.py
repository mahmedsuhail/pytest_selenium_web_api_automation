from datetime import date

from selenium.webdriver.common.by import By

from pages.TL.salesorder.so_view_stops_calendar_with_hours_page_tl import SOViewStopsCalendarWithHoursModalPageTL
from pages.basepage import BasePage

ACTUAL_DATE_INPUT = (By.ID, "routeDetails_TL-1083_editStops_detailsTab_row_0_actualDateCheckIn")
LATE_REASON = (By.ID, "routeDetails_TL-1083_editStops_detailsTab_row_0_lateReasonSelect")
PICKUP_DATE = (By.XPATH, "//div[input[@id='routeDetails_TL-1083_editStops_detailsTab_row_0_appointmentDate']]")


class SOViewStopsRowContainerPageTL(BasePage):

    def __init__(self, driver, log, row_number):
        super().__init__(driver, log)
        self.row_container_element = self.get_element(
            locator=(By.ID, f"routeDetails_TL-1083_editStops_detailsTab_row_{row_number}_rowContainer"))

    def open_actual_date_form(self):
        self.element_click_by_locator(ACTUAL_DATE_INPUT)
        return SOViewStopsCalendarWithHoursModalPageTL(self.driver, self.log)

    def set_late_reason(self, reason):
        dropdown = self.basic_types_factory.get_dropdown(LATE_REASON)
        dropdown.select_option_by_value(reason)

    def change_pickup_date(self, date_to_set: date):
        calendar = self.basic_types_factory.get_calendar(locator=PICKUP_DATE)
        calendar.set_date_without_escape(date_to_set=date_to_set)
