from selenium.webdriver.common.by import By

from pages.TL.salesorder.so_view_stops_page_tl import SOViewStopsPageTL
from pages.basepage import BasePage

LOADING_ICON = (By.CSS_SELECTOR, ".ag-loading")
SHIPMENT_WARNING_OK_BUTTON = (By.ID, "TL-2373_modal_ok")
ROUTE_DETAILS_SECTION = (By.ID, "routeDetails_TL-1083_RouteDetails_Wrapper")
CANCEL_ORDER_FOOTER_BUTTON = (By.ID, "footer_TL-1806_cancelOrderButton")
COPY_ORDER_FOOTER_BUTTON = (By.ID, "footer_TL-1806_copyOrderButton")
EDIT_ORDER_FOOTER_BUTTON = (By.ID, "footer_TL-1806_editSalesOrderButton")
CANCEL_ORDER_SUBMIT_BUTTON = (By.ID, "cancelOrder_footer_TL-1806_reasonSubmitButton")
CANCEL_ORDER_YES_BUTTON = (By.ID, "cancelOrder_footer_TL-1806_confirmationYesButton")
CANCEL_ORDER_CONFIRMATION_OK_BUTTON = (By.ID, "cancelOrder_footer_TL-1806_result_successOkButton")
ORDER_BOARD_FOOTER_BUTTON = (By.ID, "footer_footer_TL-1806_orderBoardButton")
REMOVE_CARRIER_FOOTER_BUTTON = (By.ID, "footer_TL-1806_removeCarrierButton")
VIEW_STOPS_BUTTON = (By.XPATH, "//button//span[text()='View Stops']")
NOTES_CONTAINER_ADDED_NOTE = (By.ID, "notes_TL-846_container_item0")


class SalesOrderPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def is_page_displayed(self):
        self.wait_for_element_visible(locator=ROUTE_DETAILS_SECTION)
        return self.is_element_displayed(locator=ROUTE_DETAILS_SECTION)

    def open_selected_order_tl(self, bol_id):
        self.driver.get(f"{self.get_base_url()}/tl/sales-order/{bol_id}")

    def wait_for_order_to_be_loaded(self):
        self.wait_for_element_visible(locator=ROUTE_DETAILS_SECTION, timeout=40)

    def dismiss_shipment_warning_if_shows_up(self):
        if self.is_element_present(SHIPMENT_WARNING_OK_BUTTON):
            self.element_click_by_locator(SHIPMENT_WARNING_OK_BUTTON)

    def click_cancel_order(self):
        self.element_click_by_locator(locator=CANCEL_ORDER_FOOTER_BUTTON)

    def click_yes_cancel_order(self):
        self.wait_for_element_clickable(locator=CANCEL_ORDER_YES_BUTTON)
        self.element_click_by_locator(locator=CANCEL_ORDER_YES_BUTTON)

    def click_submit_cancel_order(self):
        self.element_click_by_locator(locator=CANCEL_ORDER_SUBMIT_BUTTON)

    def click_ok_on_cancel_order_success(self):
        self.element_click_by_locator(locator=CANCEL_ORDER_CONFIRMATION_OK_BUTTON)

    def click_order_board_footer_button(self):
        self.element_click_by_locator(locator=ORDER_BOARD_FOOTER_BUTTON)

    def click_remove_carrier_footer_button(self):
        self.element_click_by_locator(locator=REMOVE_CARRIER_FOOTER_BUTTON)

    def click_view_stops_button(self):
        self.element_click_by_locator(locator=VIEW_STOPS_BUTTON)
        return SOViewStopsPageTL(self.driver, self.log)

    def click_copy_order_footer_button(self):
        self.element_click_by_locator(locator=COPY_ORDER_FOOTER_BUTTON)

    def click_edit_order_footer_button(self):
        self.element_click_by_locator(locator=EDIT_ORDER_FOOTER_BUTTON)

    def check_if_any_notes_are_added(self) -> bool:
        return self.is_element_displayed(locator=NOTES_CONTAINER_ADDED_NOTE)


class SalesOrderConfirmationPopup(BasePage):
    YES_BUTTON = (By.XPATH, "//div//button//span[text()='YES']")

    def click_yes(self):
        self.element_click_by_locator(locator=self.YES_BUTTON)
