from selenium.webdriver.common.by import By

from pages.basepage import BasePage

LOADING_ICON = (By.CSS_SELECTOR, ".ag-loading")
EMAIL_RECIPIENT = (By.ID, "")
SEND_EMAIL_OKAY_BUTTON = (By.XPATH, "//button//span[text()='Okay']")
SEND_EMAIL_BUTTON = (By.XPATH, "//button//span[text()='SEND']")
SEND_EMAIL_SUCCESS = (By.XPATH, "//div[text()='Email Sent']")
SELECT_CONTACT_BUTTON = (By.XPATH, "//div[@data-testid='recipient-text-help']")
EMAIL_FIRST_CONTACT_CHECKBOX = (By.XPATH, "//input[@id='LTL1-628_email_contact_list_address_checkbox_0']/..")
ADD_SELECTED_CONTACT_BUTTON = (By.XPATH, "//button/span[text()='Select']")


class SOSendEmailPopupPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_send_email_okay_button(self):
        self.wait_for_element_clickable(locator=SEND_EMAIL_OKAY_BUTTON)
        self.element_click_by_locator(locator=SEND_EMAIL_OKAY_BUTTON)

    def select_first_email_recipient_from_contacts(self):
        self.element_click_by_locator(locator=SELECT_CONTACT_BUTTON)
        self.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=10)
        self.element_click_by_locator(locator=EMAIL_FIRST_CONTACT_CHECKBOX, scroll_to=False)
        self.element_click_by_locator(locator=ADD_SELECTED_CONTACT_BUTTON)

    def click_send_button(self):
        self.log.info("Sending Email...")
        self.wait_for_element_clickable(locator=SEND_EMAIL_BUTTON)
        self.element_click_by_locator(locator=SEND_EMAIL_BUTTON)

    def is_email_sent_message_displayed(self, timeout=10) -> bool:
        self.wait_for_element_visible(locator=SEND_EMAIL_SUCCESS, timeout=timeout)
        return self.is_element_displayed(locator=SEND_EMAIL_SUCCESS)
