from selenium.webdriver.common.by import By

from pages.basepage import BasePage

SELECT_CARRIER_BUTTON = (By.ID, "Fulfillment_TL-921__selectCarrier")
EDIT_CARRIER_BUTTON = (By.ID, "Fulfillment_TL-921_buttonEdit")
SAVE_AND_ASSIGN_BUTTON = (By.ID, "Fulfillment_TL-921_buttonSave&Assign")
DELETE_CARRIER_BUTTON = (By.ID, "SelectCarrier_TL-3850_buttonRemoveCarrier")
SELECT_CARRIER_SEARCH_BOX = (By.CSS_SELECTOR, "#Fulfillment_TL-921_carrier input")
CARRIER_SEARCH_BOX_RESULT_LIST = (
    By.CSS_SELECTOR, "#Fulfillment_TL-921_selectCarrier div[class*='menu'] div[id*='react-select']")
CARRIER_SEARCH_BOX_CLOSE_BUTTON = (By.XPATH, "//div[@id='Fulfillment_TL-921_modal']//span[text()='+']")


class SOCarrierFulfillmentPage(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_select_carrier_button(self):
        self.element_click_by_locator(locator=SELECT_CARRIER_BUTTON)

    def search_and_select_carrier(self, search_string, carrier_name):
        self.wait_for_element(locator=SELECT_CARRIER_SEARCH_BOX)
        self.element_click(
            element=self.get_element(locator=SELECT_CARRIER_SEARCH_BOX))
        self.send_keys(locator=SELECT_CARRIER_SEARCH_BOX, data="")  # workaround, otherwise cannot input text
        self.send_keys_to_browser(data=search_string)
        self.wait_for_carrier_search_result()

        elements = self.get_element_list(locator=CARRIER_SEARCH_BOX_RESULT_LIST)

        for element in elements:
            if element.text.splitlines()[0] == carrier_name:
                self.scroll_into_view(element=element)
                self.element_click(element=element)
                break

    def wait_for_carrier_search_result(self):
        self.wait_for_element(locator=CARRIER_SEARCH_BOX_RESULT_LIST)

    def click_save_and_assign_button(self):
        self.wait_for_element(locator=SAVE_AND_ASSIGN_BUTTON)
        self.element_click_by_locator(locator=SAVE_AND_ASSIGN_BUTTON)

    def click_delete_carrier_button(self):
        self.wait_for_element(locator=DELETE_CARRIER_BUTTON)
        self.element_click_by_locator(locator=DELETE_CARRIER_BUTTON)

    def click_edit_carrier_button(self):
        self.element_click_by_locator(locator=EDIT_CARRIER_BUTTON)

    def click_close_carrier_search_box(self):
        self.element_click_by_locator(locator=CARRIER_SEARCH_BOX_CLOSE_BUTTON)

    def check_if_carrier_is_assigned(self, carrier_name) -> bool:
        locator = (By.XPATH, f"//div[@id='Fulfillment_TL-921_carrierInfo']//div//div//span[text()='{carrier_name}']")
        return self.is_element_displayed(locator=locator)

    def check_if_select_carrier_button_is_visible(self) -> bool:
        return self.is_element_displayed(locator=SELECT_CARRIER_BUTTON)
