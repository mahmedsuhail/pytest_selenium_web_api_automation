from selenium.webdriver.common.by import By

from pages.basepage import BasePage

QUOTE_DOCUMENT = (By.XPATH, "//div[text()='Quote'][contains(@class,'DocumentLabel')]")
BILL_OF_LANDING_DOCUMENT = (By.XPATH, "//div[text()='Bill of Lading'][contains(@class,'DocumentLabel')]")
RATE_CONFIRMATION_DOCUMENT = (By.XPATH, "//div[text()='Rate Confirmation'][contains(@class,'DocumentLabel')]")
SEND_EMAIL_BUTTON = (By.XPATH, "//button//span[text()='Email']")


class SODocumentsPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_on_rate_confirmation_document(self):
        self.element_click_by_locator(locator=RATE_CONFIRMATION_DOCUMENT)

    def is_quote_document_displayed(self):
        return self.is_element_displayed(QUOTE_DOCUMENT)

    def is_bill_of_landing_document_displayed(self):
        return self.is_element_displayed(BILL_OF_LANDING_DOCUMENT)

    def click_send_email_button(self):
        self.element_click_by_locator(locator=SEND_EMAIL_BUTTON)
