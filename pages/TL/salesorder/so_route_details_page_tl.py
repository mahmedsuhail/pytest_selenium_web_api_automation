from selenium.webdriver.common.by import By

from pages.basepage import BasePage

ORIGIN_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Origin_HeaderContent")
ORIGIN_COMPANY_FIELD = (By.XPATH, "(//article[@id='routeDetails_TL-1083_RouteCard_Origin_Company'])[1]")
ORIGIN_CONTACT_NAME_FIELD = (By.XPATH, "(//article[@id='routeDetails_TL-1083_RouteCard_Origin_Company'])[2]")
ORIGIN_PHONE_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Origin_Phone")
ORIGIN_CHECK_IN_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Origin_Check_In")
ORIGIN_CHECKOUT_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Origin_Check_Out")

DESTINATION_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Destination_HeaderContent")
DESTINATION_COMPANY_FIELD = (By.XPATH, "(//article[@id='routeDetails_TL-1083_RouteCard_Destination_Company'])[1]")
DESTINATION_CONTACT_NAME_FIELD = (By.XPATH, "(//article[@id='routeDetails_TL-1083_RouteCard_Destination_Company'])[2]")
DESTINATION_PHONE_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Destination_Phone")
DESTINATION_CHECK_IN_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Destination_Check_In")
DESTINATION_CHECKOUT_FIELD = (By.ID, "routeDetails_TL-1083_RouteCard_Destination_Check_Out")


class SORouteDetailsPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_origin_field(self) -> str:
        return self.get_text(locator=ORIGIN_FIELD)

    def get_origin_company_field(self) -> str:
        return self.get_text(locator=ORIGIN_COMPANY_FIELD)

    def get_origin_contact_name_field(self) -> str:
        return self.get_text(locator=ORIGIN_CONTACT_NAME_FIELD)

    def get_origin_phone_field(self) -> str:
        return self.get_text(locator=ORIGIN_PHONE_FIELD)

    def get_origin_check_in_field(self) -> str:
        return self.get_text(locator=ORIGIN_CHECK_IN_FIELD)

    def get_origin_checkout_field(self) -> str:
        return self.get_text(locator=ORIGIN_CHECKOUT_FIELD)

    def get_destination_field(self) -> str:
        return self.get_text(locator=DESTINATION_FIELD)

    def get_destination_company_field(self) -> str:
        return self.get_text(locator=DESTINATION_COMPANY_FIELD)

    def get_destination_contact_name_field(self) -> str:
        return self.get_text(locator=DESTINATION_CONTACT_NAME_FIELD)

    def get_destination_phone_field(self) -> str:
        return self.get_text(locator=DESTINATION_PHONE_FIELD)

    def get_destination_check_in_field(self) -> str:
        return self.get_text(locator=ORIGIN_CHECK_IN_FIELD)

    def get_destination_checkout_field(self) -> str:
        return self.get_text(locator=ORIGIN_CHECKOUT_FIELD)
