from selenium.webdriver.common.by import By

from pages.basepage import BasePage

TOTAL_SHIPMENT_VALUE = (By.XPATH, "//div[span[text()='Total Shipment Value']]/div")
COMMODITY_DESCRIPTION_VALUE = (By.XPATH, "//div[span[text()='Commodity Description']]/div")
HANDLING_UNIT_COUNT_VALUE = (By.XPATH, "//div[span[text()='Handling Unit Count']]/div")
WEIGHT_VALUE = (By.XPATH, "//div[span[text()='Weight']]/div")
PALLETS_VALUE = (By.XPATH, "//div[span[text()='# Pallets']]/div")
PIECES_VALUE = (By.XPATH, "//div[span[text()='# Pieces']]/div")
CARRIER_INSURANCE_VALUE = (By.XPATH, "//div[span[text()='Carrier Insurance']]/div")


class SOFreightDetailsPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_total_shipment_value(self):
        return self.get_text(TOTAL_SHIPMENT_VALUE)

    def get_commodity_description_value(self):
        return self.get_text(COMMODITY_DESCRIPTION_VALUE)

    def get_handling_unit_count_value(self):
        return self.get_text(HANDLING_UNIT_COUNT_VALUE)

    def get_weight_value(self):
        return self.get_text(WEIGHT_VALUE)

    def get_pallets_value(self):
        return self.get_text(PALLETS_VALUE)

    def get_pieces_value(self):
        return self.get_text(PIECES_VALUE)

    def get_carrier_insurance_value(self):
        return self.get_text(CARRIER_INSURANCE_VALUE)
