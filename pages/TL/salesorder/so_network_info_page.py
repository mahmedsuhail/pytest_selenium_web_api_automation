from selenium.webdriver.common.by import By

from pages.basepage import BasePage

CUSTOMER_NAME_FIELD = (By.ID, "networkInfo_TL-6057_textValue0")
SHIPMENT_TYPE_FIELD = (By.ID, "networkInfo_TL-6057_textValue8")
CREATED_BY_FIELD = (By.ID, "networkInfo_TL-6057_textValue9")
RATING_METHOD_FIELD = (By.ID, "networkInfo_TL-6057_textValue10")
GENERATED_ON_FIELD = (By.ID, "networkInfo_TL-6057_textValue11")
BOOKED_BY_FIELD = (By.ID, "networkInfo_TL-6057_textValue7")


class SONetworkInfoPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def wait_for_booked_by_value(self, expected_value, timeout=3):
        self.wait_for_element_has_text(locator=BOOKED_BY_FIELD, text_value=expected_value, timeout=timeout)

    def get_booked_by_field(self) -> str:
        return self.get_text(locator=BOOKED_BY_FIELD)

    def get_customer_name_field(self) -> str:
        return self.get_text(locator=CUSTOMER_NAME_FIELD)

    def get_shipment_type_field(self) -> str:
        return self.get_text(locator=SHIPMENT_TYPE_FIELD)

    def get_created_by_field(self) -> str:
        return self.get_text(locator=CREATED_BY_FIELD)

    def get_rating_method_field(self) -> str:
        return self.get_text(locator=RATING_METHOD_FIELD)

    def get_generated_on_field(self) -> str:
        return self.get_text(locator=GENERATED_ON_FIELD)
