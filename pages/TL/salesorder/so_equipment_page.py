from selenium.webdriver.common.by import By

from pages.basepage import BasePage

EQUIPMENT_TYPE_FIELD = (By.ID, "equipment_TL-6390_textValue0")
EQUIPMENT_LENGTH_FIELD = (By.ID, "equipment_TL-6390_textValue1")


class SOEquipmentPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_equipment_type_field(self) -> str:
        return self.get_text(locator=EQUIPMENT_TYPE_FIELD)

    def get_equipment_length_field(self) -> str:
        return self.get_text(locator=EQUIPMENT_LENGTH_FIELD)
