from selenium.webdriver.common.by import By

from pages.basepage import BasePage

UNCHANGEABLE_ORDER_STATUS_BUTTON = (By.ID, "statuses_LTL1-1395_statusValue5")
UNCHANGEABLE_ORDER_STATUS_BUTTON_CHILD_ELEMENTS = (By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5']/*")
ORDER_STATUS_BUTTON = (By.ID, "statuses_LTL1-1395_statusValue5_ToggleSelect")
ORDER_STATUS_VALUE = (By.CSS_SELECTOR, "div[id*='statuses_LTL1-1395_statusValue5']")
ORDER_STATUS_DROPDOWN = (By.ID, "statuses_LTL1-1395_statusValue5_PopperContainer")
ORDER_STATUS_DROPDOWN_VALUES = (By.CSS_SELECTOR, "#statuses_LTL1-1395_statusValue5_PopperContainer li")
AVAILABLE_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Available']")
CUSTOMER_HOLD_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Customer Hold']")
BOOKED_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Booked']")
DISPATCH_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Dispatch']")
AT_SHIPPER_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='At Shipper']")
IN_TRANSIT_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='In Transit']")
AT_CONSIGNEE_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='At Consignee']")
PAPERWORK_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Paperwork']")
DELIVERED_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Delivered']")
PROBLEM_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Problem']")
CANCELED_STATUS = (
    By.XPATH, "//div[@id='statuses_LTL1-1395_statusValue5_PopperContainer']//ul//li[text()='Canceled']")
VENDOR_BILL_STATUS = (By.ID, "statuses_LTL1-1395_statusValue3")
INVOICE_STATUS_VALUE = (By.ID, "statuses_LTL1-1395_statusValue2")


class SOStatusesPage(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def check_if_order_status_dropdown_is_visible(self) -> bool:
        return self.is_element_displayed(locator=ORDER_STATUS_BUTTON)

    def wait_for_order_status_change(self, expected_status, timeout=5):
        self.wait_for_element_has_text(locator=ORDER_STATUS_VALUE, text_value=expected_status, timeout=timeout)

    def get_current_order_status(self) -> str:
        return self.get_text(locator=ORDER_STATUS_VALUE)

    def get_order_status_dropdown_options(self) -> []:
        self.open_status_dropdown()

        options = []
        dropdown_options = self.get_element_list(locator=ORDER_STATUS_DROPDOWN_VALUES)
        for option in dropdown_options:
            options.append(option.text)
        return options

    def select_available_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=AVAILABLE_STATUS)

    def select_customer_hold_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=CUSTOMER_HOLD_STATUS)

    def select_booked_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=BOOKED_STATUS)

    def select_dispatch_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=DISPATCH_STATUS)

    def select_at_shipper_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=AT_SHIPPER_STATUS)

    def select_in_transit_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=IN_TRANSIT_STATUS)

    def get_order_status_button_text(self) -> str:
        self.wait_for_element_clickable(locator=UNCHANGEABLE_ORDER_STATUS_BUTTON)
        self.page_vertical_scroll(locator=UNCHANGEABLE_ORDER_STATUS_BUTTON)
        return self.get_text(locator=UNCHANGEABLE_ORDER_STATUS_BUTTON)

    def get_order_status_button_child_elements(self) -> []:
        return self.get_element_list(locator=UNCHANGEABLE_ORDER_STATUS_BUTTON_CHILD_ELEMENTS)

    def open_status_dropdown(self):
        if not self.is_element_displayed(locator=ORDER_STATUS_DROPDOWN):
            self.element_click_by_locator(locator=ORDER_STATUS_BUTTON)

    def click_on_order_status_button(self):
        self.element_click_by_locator(locator=UNCHANGEABLE_ORDER_STATUS_BUTTON)

    def select_at_consignee_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=AT_CONSIGNEE_STATUS)

    def select_paperwork_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=PAPERWORK_STATUS)

    def select_delivered_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=DELIVERED_STATUS)

    def select_problem_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=PROBLEM_STATUS)

    def select_canceled_status(self):
        self.open_status_dropdown()
        self.element_click_by_locator(locator=CANCELED_STATUS)

    def get_vendor_bill_status(self):
        self.get_text(locator=VENDOR_BILL_STATUS)

    def get_invoice_status(self) -> str:
        return self.get_text(locator=INVOICE_STATUS_VALUE)
