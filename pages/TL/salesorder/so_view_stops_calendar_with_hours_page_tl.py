from selenium.webdriver.common.by import By

from pages.basepage import BasePage

DAY_TODAY = (By.XPATH, "//div[contains(@class,'day--today')]")
DAY_AFTER_TODAY = (By.XPATH, "//div[contains(@class,'day--today')]/following-sibling::div")

CHECKIN_DROPDOWN = (By.ID, "routeDetails_TL-1083_editStops_detailsTab_row_0_actualDateCheckIn_select_0")
CHECKOUT_DROPDOWN = (By.ID, "routeDetails_TL-1083_editStops_detailsTab_row_0_actualDateCheckIn_select_1")
SAVE_AND_CLOSE_BUTTON = (By.ID, "routeDetails_TL-1083_editStops_detailsTab_row_0_actualDateCheckIn_button_1")


class SOViewStopsCalendarWithHoursModalPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def set_date_from_current_to_plus_1_day(self):
        self.element_click_by_locator(locator=DAY_TODAY)
        self.element_click_by_locator(locator=DAY_AFTER_TODAY)

    def set_check_in(self, hour_in_15_min_period):
        dropdown = self.basic_types_factory.get_dropdown(CHECKIN_DROPDOWN)
        dropdown.select_option_by_value(hour_in_15_min_period)

    def set_check_out(self, hour_in_15_min_period):
        dropdown = self.basic_types_factory.get_dropdown(CHECKOUT_DROPDOWN)
        dropdown.select_option_by_value(hour_in_15_min_period)

    def click_save_and_close(self):
        self.element_click_by_locator(SAVE_AND_CLOSE_BUTTON)
