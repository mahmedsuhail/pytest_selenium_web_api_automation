from selenium.webdriver.common.by import By

from pages.TL.salesorder.so_view_stops_row_container_page_tl import SOViewStopsRowContainerPageTL
from pages.basepage import BasePage

SAVE_BUTTON = (By.ID, "routeDetails_TL-1083_editStops_saveButton")


class SOViewStopsPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_save(self):
        self.element_click_by_locator(SAVE_BUTTON)

    def get_row_by_number(self, row_number_from_zero):
        return SOViewStopsRowContainerPageTL(self.driver, self.log, row_number_from_zero)
