from selenium.webdriver.common.by import By

from pages.basepage import BasePage

SELECT_REASON_DROPDOWN = (By.XPATH, "//div[text()='Select Reason']")
BREAKDOWN_CANCELLATION_REASON = (By.XPATH, "//div[text()='Breakdown']")
REMOVE_CARRIER_BUTTON = (By.CSS_SELECTOR, "#removeCarrier_footer_TL-1806_confirmationYesButton span")


class SORemoveCarrierPopupPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_select_cancellation_reason(self):
        self.element_click_by_locator(locator=SELECT_REASON_DROPDOWN)

    # TODO - Rework to Dropdown class when automation id will be added
    def select_breakdown_cancellation_reason(self):
        self.element_click_by_locator(BREAKDOWN_CANCELLATION_REASON)

    def click_remove_carrier_button(self):
        self.element_click_by_locator(locator=REMOVE_CARRIER_BUTTON)
