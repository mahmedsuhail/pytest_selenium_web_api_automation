from selenium.webdriver.common.by import By

from pages.basepage import BasePage

SELECT_REASON_DROPDOWN = (By.XPATH, "//div[text()='Select Reason']")
BREAKDOWN_CANCELLATION_REASON = (By.XPATH, "//div[text()='Breakdown']")
REMOVE_CARRIER_BUTTON = (By.CSS_SELECTOR, "#removeCarrier_footer_TL-1806_confirmationYesButton span")
COPY_ORDER_NUMBER_OF_COPIES = (By.ID, "copyOrder_footer_TL-1806_numberOfCopies")
COPY_ORDER_CONFIRM_BUTTON = (By.ID, "copyOrder_footer_TL-1806_confirmationYesButton")
COPY_ORDER_SUCCESS_MODAL = (By.ID, "copyOrder_footer_TL-1806_CopyOrderSuccessModal")
GO_TO_LINK = (By.CSS_SELECTOR, ".copyOrderLink")
GO_TO_BOL_LINK = (By.XPATH, "//a[contains(@href, '/tl/sales-order/')]")


class SOCopySalesOrderPopupPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def copy_order_get_number_of_copies(self):
        return self.get_text(locator=COPY_ORDER_NUMBER_OF_COPIES)

    def click_copy_order_confirm_button(self):
        self.element_click_by_locator(locator=COPY_ORDER_CONFIRM_BUTTON)

    def check_copy_order_success(self, timeout=20) -> bool:
        self.wait_for_element_visible(locator=COPY_ORDER_SUCCESS_MODAL, timeout=timeout)
        return self.is_element_displayed(locator=COPY_ORDER_SUCCESS_MODAL)

    def get_go_to_links(self):
        return self.get_element_list(locator=GO_TO_LINK)

    def click_go_to_bol(self):
        link = self.wait_for_element_clickable(locator=GO_TO_BOL_LINK)
        link.click()
