from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage
from pages.basictypes.dropdown import Dropdown

ADD_CHARGE_BUTTON = (By.ID, "financials_add_charge")
CLOSE_BUTTON = (By.ID, "financials_close")
CONTAINER_ENABLE_BUTTON = (By.ID, "financials_edit")
CONFIRM_AND_CLOSE = (By.ID, "TL-financials_confirm_and_close")
CHARGE_TABLE_HEADER_DESCRIPTION = (By.XPATH, "//div[contains(@class,'ChargeTable') and div[text()='Description']]")
CHARGE_TABLE_HEADER_COST = (By.XPATH, "//div[contains(@class,'ChargeTable') and div[text()='Cost']]")
CHARGE_TABLE_HEADER_REVENUE = (By.XPATH, "//div[contains(@class,'ChargeTable') and div[text()='Revenue']]")
CONTAINER_HEADER_BACKGROUND = (By.XPATH, "//div[div[text()='Financials']]")

ACTUAL_COST_VALUE = (By.ID, "financials_actual_cost")
REVENUE_VALUE = (By.ID, "financials_revenue")
MARGIN_VALUE = (By.ID, "financials_margin")
MARGIN_PERCENTAGE_VALUE = (By.ID, "financials_target_margin")
MAX_BUY_VALUE = (By.ID, "financials_max_buy")
TARGET_COST_VALUE = (By.ID, "financials_target_cost")
PRICE_TO_BEAT_VALUE = (By.ID, "financials_price_to_beat")

CHARGE_TABLE_DESCRIPTION = (By.CSS_SELECTOR, "[id^='financials_charge_select_description']")
CHARGE_TABLE_COST = (By.CSS_SELECTOR, "[id^='financials_change_cost']")
CHARGE_TABLE_RATE = (By.CSS_SELECTOR, "[id^='financials_change_rate']")
CHARGE_TABLE_REVENUE = (By.CSS_SELECTOR, "[id^='financials_change_revenue']")
CHARGE_TABLE_REMOVE_CHARGE = (By.CSS_SELECTOR, "[id^='financials_remove_charge']")
CHARGE_TABLE_TEXT_ONLY = (
    By.XPATH, "//div[contains(@class,'ChargeTablestyle__GridContainer')]/div[contains(@class,'DisplayText')]")


class SOFinancialsPage(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def enable(self):
        self.element_click_by_locator(locator=CONTAINER_ENABLE_BUTTON)

    def click_add_charge(self):
        self.element_click_by_locator(locator=ADD_CHARGE_BUTTON)

    def click_close(self):
        self.element_click_by_locator(locator=CLOSE_BUTTON)

    def click_confirm_and_close(self):
        self.element_click_by_locator(locator=CONFIRM_AND_CLOSE)

    def get_actual_cost(self):
        return self.get_text(locator=ACTUAL_COST_VALUE)

    def get_revenue(self):
        return self.get_text(locator=REVENUE_VALUE)

    def get_margin(self):
        return self.get_text(locator=MARGIN_VALUE)

    def get_margin_percentage(self):
        return self.get_text(locator=MARGIN_PERCENTAGE_VALUE)

    def get_max_buy(self):
        return self.get_input_value(locator=MAX_BUY_VALUE)

    def get_charge_table_rows(self):
        description_elements = self.get_element_list(CHARGE_TABLE_DESCRIPTION)
        cost_elements = self.get_element_list(CHARGE_TABLE_COST)
        rate_elements = self.get_element_list(CHARGE_TABLE_RATE)
        revenue_elements = self.get_element_list(CHARGE_TABLE_REVENUE)
        remove_charge_btn_elements = self.get_element_list(CHARGE_TABLE_REMOVE_CHARGE)
        uom_units_elements = self.get_element_list(CHARGE_TABLE_TEXT_ONLY)

        if not len(description_elements) == len(cost_elements) == len(rate_elements) == len(revenue_elements) == len(
                uom_units_elements) / 2:
            return None
        if not len(description_elements) == len(remove_charge_btn_elements) + 1:
            return None

        tmp = []

        for index, element in enumerate(description_elements):
            if index == 0:
                tmp.append(
                    ChargeRow(driver=self.driver,
                              description_element=description_elements[index],
                              cost_element=cost_elements[index],
                              rate_element=rate_elements[index],
                              revenue_element=revenue_elements[index],
                              remove_btn_element=None,
                              uom_element=uom_units_elements[index * 2],
                              units_element=uom_units_elements[index * 2 + 1],
                              log=self.log))
            else:
                tmp.append(
                    ChargeRow(driver=self.driver,
                              description_element=description_elements[index],
                              cost_element=cost_elements[index],
                              rate_element=rate_elements[index],
                              revenue_element=revenue_elements[index],
                              remove_btn_element=remove_charge_btn_elements[index - 1],
                              uom_element=uom_units_elements[index * 2],
                              units_element=uom_units_elements[index * 2 + 1],
                              log=self.log))
        return tmp

    def get_container_color(self):
        return self.get_element(CONTAINER_HEADER_BACKGROUND).value_of_css_property("background-color")

    def get_price_to_beat(self):
        return self.get_input_value(locator=PRICE_TO_BEAT_VALUE)

    def get_target_cost(self):
        return self.get_input_value(locator=TARGET_COST_VALUE)

    def is_add_charge_button_visible(self):
        return self.is_element_displayed(ADD_CHARGE_BUTTON)

    def is_charge_header_description_visible(self):
        return self.is_element_displayed(CHARGE_TABLE_HEADER_DESCRIPTION)

    def is_charge_header_cost_visible(self):
        return self.is_element_displayed(CHARGE_TABLE_HEADER_COST)

    def is_charge_header_revenue_visible(self):
        return self.is_element_displayed(CHARGE_TABLE_HEADER_REVENUE)

    def is_confirm_close_button_visible(self):
        return self.is_element_displayed(CONFIRM_AND_CLOSE)

    def wait_for_add_charge_button_visible(self):
        self.wait_for_element_visible(locator=ADD_CHARGE_BUTTON)

    def set_max_buy(self, value):
        self.send_keys(locator=MAX_BUY_VALUE, data=value)

    def set_target_cost(self, value):
        self.send_keys(locator=TARGET_COST_VALUE, data=value)

    def set_price_to_beat(self, value):
        self.send_keys(locator=PRICE_TO_BEAT_VALUE, data=value)


class ChargeRow(BasePage):
    def __init__(self, driver,
                 description_element: WebElement,
                 cost_element: WebElement,
                 uom_element: WebElement,
                 units_element: WebElement,
                 rate_element: WebElement,
                 revenue_element: WebElement,
                 remove_btn_element,
                 log):
        super().__init__(driver, log)
        self.description_element = description_element
        self.cost_element = cost_element
        self.uom_element = uom_element
        self.units_element = units_element
        self.rate_element = rate_element
        self.revenue_element = revenue_element
        self.remove_btn_element = remove_btn_element

    def __get_description_element(self) -> Dropdown:
        return self.basic_types_factory.get_dropdown_by_element(self.description_element)

    def clear_filter(self):
        self.__get_description_element().clear_filter()

    def click_remove(self):
        self.element_click(self.get_remove_button())

    def is_description_editable(self):
        main = self.__get_description_element()
        if main.is_dropdown_open():
            return True
        self.element_click(main.main_element)
        return main.is_dropdown_open()

    def is_remove_button_exist(self):
        return self.remove_btn_element is not None

    def get_description(self):
        return self.__get_description_element().get_selected_value()

    def get_all_descriptions(self):
        return self.__get_description_element().get_available_options_values()

    def get_description_color(self):
        return self.__get_description_element().get_placeholder_color()

    def get_remove_button(self):
        return self.remove_btn_element

    def select_description(self, value):
        self.__get_description_element().select_option_by_value(value)

    def select_description_quick(self, value, clear=False):
        self.__get_description_element().select_option_by_value_quick(value, clear)

    def get_all_descriptions_with_search(self, value, clear=False):
        return self.__get_description_element().get_available_options_values_with_search(value, clear)

    def get_cost(self):
        return self.get_input_value(locator=None, element=self.cost_element)

    def set_cost(self, value, clear=True):
        self.send_keys(locator=None, element=self.cost_element, data=value, clear=clear)

    def get_revenue(self):
        return self.get_input_value(locator=None, element=self.revenue_element)

    def set_revenue(self, value, clear=True):
        self.send_keys(locator=None, element=self.revenue_element, data=value, clear=clear)
