from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage


class SalesOrderBoardTableRowPageTL(BasePage):

    def __init__(self, driver, log, row_web_element: WebElement):
        super().__init__(driver, log)
        self.row_element = row_web_element

    def get_cell_value(self, column_id):
        return self.row_element.find_element_by_css_selector(f"div[col-id='{column_id}']").text
