from selenium.webdriver.common.by import By

from pages.TL.salesorderboard.sales_order_board_pending_quote_create_note_page_tl import SalesOrderBoardCreateNotePageTL
from pages.basepage import BasePage

CUSTOMER_NAME = (By.ID, "edi_tender_details_TL-1051_network-info_text1")
SERVICE_TYPE = (By.ID, "edi_tender_details_TL-1051_network-info_text4")
SHIPMENT_TYPE = (By.ID, "edi_tender_details_TL-1051_network-info_text5")
ORIGIN = (By.ID, "edi_tender_details_TL-1051_route-details_text2")
DESTINATION = (By.ID, "edi_tender_details_TL-1051_route-details_text6")
COMMODITY_DESCRIPTION = (By.ID, "edi_tender_details_TL-1051_freight-details_text11")
HANDLING_UNIT_TYPE = (By.ID, "edi_tender_details_TL-1051_freight-details_text12")
HANDLING_UNIT_COUNT = (By.ID, "edi_tender_details_TL-1051_freight-details_text13")
WEIGHT = (By.ID, "edi_tender_details_TL-1051_freight-details_text14")
PALLETS = (By.ID, "edi_tender_details_TL-1051_freight-details_text15")
PIECES = (By.ID, "edi_tender_details_TL-1051_freight-details_text16")
SERVICE = (By.ID, "edi_tender_details_TL-1051_equipment_text1")
EQUIPMENT = (By.ID, "edi_tender_details_TL-1051_equipment_text2")
LENGTH = (By.ID, "edi_tender_details_TL-1051_equipment_text3")
QUOTE_IT_BUTTON = (By.XPATH, "//button//span[text()='Quote it']")
SEND_RATE_BUTTON = (By.XPATH, "//button//span[text()='Send Rate']")
BUILD_LOAD_BUTTON = (By.XPATH, "//button//span[text()='Build load']")
QUOTED_MARKER = (By.XPATH, "//div[text()='Quoted']")
QUOTED_DOCUMENT = (By.XPATH, "//div[contains(@class, 'Document')][text()='Quote']")
ACTUAL_COST = (By.ID, "edi_tender_details_TL-1051_financials_text2")
REVENUE = (By.ID, "edi_tender_details_TL-1051_financials_text1")
MAX_BUY = (By.ID, "edi_tender_details_TL-1051_financials_text3")
TARGET_COST = (By.ID, "edi_tender_details_TL-1051_financials_text4")
NOTES = (By.ID, "edi_tender_details_TL-1051_notes_text13")
SALES_REP = (By.ID, "edi_tender_details_TL-1051_network-info_text3")

REVENUE_INPUT = (By.ID, "financials_TL-7771_revenue")
MARGIN_INPUT = (By.ID, "financials_TL-7771_margin")
MAX_BUY_INPUT = (By.ID, "financials_TL-7771_maxBuy")
TARGET_COST_INPUT = (By.ID, "financials_TL-7771_targetCost")
PRICE_TO_BEAT_INPUT = (By.ID, "financials_TL-7771_priceToBeat")
SELL_RATE_INPUT = (By.ID, "financials_TL-7771_sellRate")
STATUS = (By.ID, "financials_TL-7771_status")

ACCESSORIALS = (By.XPATH, "//div[div[text()='Accessorials']]/following-sibling::div/div")


class SalesOrderBoardQuoteDetailsPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_customer_name(self):
        return self.get_text(locator=CUSTOMER_NAME)

    def get_service_type(self):
        return self.get_text(locator=SERVICE_TYPE)

    def get_shipment_type(self):
        return self.get_text(locator=SHIPMENT_TYPE)

    def get_origin(self):
        return self.get_text(locator=ORIGIN)

    def get_destination(self):
        return self.get_text(locator=DESTINATION)

    def get_commodity_description(self):
        return self.get_text(locator=COMMODITY_DESCRIPTION)

    def get_handling_unit_type(self):
        return self.get_text(locator=HANDLING_UNIT_TYPE)

    def get_handling_unit_count(self):
        return self.get_text(locator=HANDLING_UNIT_COUNT)

    def get_weight(self):
        return self.get_text(locator=WEIGHT)

    def get_pallets(self):
        return self.get_text(locator=PALLETS)

    def get_pieces(self):
        return self.get_text(locator=PIECES)

    def get_service(self):
        return self.get_text(locator=SERVICE)

    def get_equipment(self):
        return self.get_text(locator=EQUIPMENT)

    def get_length(self):
        return self.get_text(locator=LENGTH)

    def get_actual_cost(self):
        return self.get_text(locator=ACTUAL_COST)

    def get_revenue(self):
        return self.get_text(locator=REVENUE)

    def get_max_buy(self):
        return self.get_text(locator=MAX_BUY)

    def get_max_buy_input(self):
        return self.get_input_value(locator=MAX_BUY_INPUT)

    def get_target_cost(self):
        return self.get_text(locator=TARGET_COST)

    def get_target_cost_input(self):
        return self.get_input_value(locator=TARGET_COST_INPUT)

    def wait_for_revenue_change(self, expected_value):
        self.wait_for_element_has_text(locator=REVENUE_INPUT, text_value=expected_value)

    def get_revenue_pending_quote(self):
        return self.get_text(locator=REVENUE_INPUT)

    def get_margin_pending_quote(self):
        return self.get_text(locator=MARGIN_INPUT)

    def get_status(self):
        return self.get_text(locator=STATUS)

    def get_notes_text(self):
        return self.get_text(locator=NOTES)

    def get_accessorials(self):
        elements = self.get_element_list(locator=ACCESSORIALS)
        tmp = []
        for element in elements:
            tmp.append(element.text)
        return tmp

    def set_max_buy(self, value, clear=True):
        self.send_keys(locator=MAX_BUY_INPUT, data=value, clear=clear)

    def is_max_buy_editable(self):
        return self.is_element_editable(locator=MAX_BUY_INPUT)

    def set_target_cost(self, value, clear=True):
        self.send_keys(locator=TARGET_COST_INPUT, data=value, scroll_to=False, clear=clear)
        self.element_click_by_locator(locator=STATUS)

    def is_target_cost_editable(self):
        return self.is_element_editable(locator=TARGET_COST_INPUT)

    def get_price_to_beat(self):
        return self.get_input_value(locator=PRICE_TO_BEAT_INPUT)

    def set_price_to_beat(self, value, clear=True):
        self.send_keys(locator=PRICE_TO_BEAT_INPUT, data=value, clear=clear)

    def is_price_to_beat_editable(self):
        return self.is_element_editable(locator=PRICE_TO_BEAT_INPUT)

    def set_sell_rate(self, value):
        self.send_keys(locator=SELL_RATE_INPUT, data=value)

    def click_quote_it(self):
        self.element_click_by_locator(locator=QUOTE_IT_BUTTON, scroll_to=False)
        return SalesOrderBoardCreateNotePageTL(self.driver, self.log)

    def click_build_load(self):
        self.element_click_by_locator(locator=BUILD_LOAD_BUTTON)

    def click_send_rate_button(self):
        self.element_click_by_locator(locator=SEND_RATE_BUTTON)

    def check_if_quoted_marker_is_visible(self) -> bool:
        return self.is_element_displayed(locator=QUOTED_MARKER)

    def check_if_build_load_button_is_visible(self) -> bool:
        return self.is_element_displayed(locator=BUILD_LOAD_BUTTON)

    def check_if_quote_document_is_visible(self) -> bool:
        return self.is_element_displayed(locator=QUOTED_DOCUMENT)

    def check_if_quote_it_button_is_visible(self):
        return self.is_element_displayed(locator=QUOTE_IT_BUTTON)

    def check_if_status_is_visible(self):
        return self.is_element_displayed(locator=STATUS)

    def check_if_sales_rep_is_visible(self):
        return self.is_element_displayed(locator=SALES_REP)

    def click_on_status(self):
        self.element_click_by_locator(locator=STATUS)
