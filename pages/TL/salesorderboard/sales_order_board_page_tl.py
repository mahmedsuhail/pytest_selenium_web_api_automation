from selenium.webdriver.common.by import By

from pages.TL.salesorderboard.sales_order_board_create_new_board_page_tl import SalesOrderBoardCreateNewBoardPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from pages.basepage import BasePage

TRUCKLOAD_BUTTON = (By.ID, "salesOrderBoard_tl-1810_tl_ServiceTypeLink")
PENDING_QUOTES_TABLE_CONTAINER = (By.ID, "salesOrderBoard_tl-1810_pendingQuotesTable_container")
QUOTE_ID_COLUMN = (By.ID, "salesOrderBoard_TL-2046-TL-2039_quoteNumber")
ROW_DIV = (By.CSS_SELECTOR, "div[row-id]")
LOADING_ICON = (By.CSS_SELECTOR, ".ag-loading")
CANCELED_TAB = (By.CSS_SELECTOR, "li[title='Canceled']")
QUOTED_TAB = (By.CSS_SELECTOR, "li[title='Quoted']")
DELIVERED_TAB = (By.CSS_SELECTOR, "li[title='Delivered']")
AVAILABLE_TAB = (By.CSS_SELECTOR, "li[title='Available']")
PENDING_TAB = (By.CSS_SELECTOR, "li[title='Pending']")
PENDING_QUOTES_TAB = (By.CSS_SELECTOR, "li[title='Pending Quotes']")
AGE_MINUTES_COLUMN_FILTER = (By.XPATH, "//span[text()='Age Minutes']")
BOARDS = (By.CSS_SELECTOR, "div[class*='TabsViewstyles'] li[role='tab']")
TOP_NAV_SEARCH_INPUT = (By.ID, "topNav_TL-2970_search_1")
CLEAR_FILTERS_BUTTON = (By.ID, "TL-5913_clearFiltersButton")
SALES_ORDER_BOARD_CONTAINER = (By.ID, "salesOrderBoard_tl-1810_wrapper")
QUOTE_VIEW_CUSTOMER_NAME = (By.ID, "edi_tender_details_TL-1051_network-info_text1")
QUOTE_PAGE_SPINNER = (By.XPATH, "//div[text()='Loading']")
NO_ROWS_TO_SHOW = (By.XPATH, "//span[contains(.,'No Rows To Show')]")
MANAGE_BOARDS_BUTTON = (By.ID, "TL-1655_ManageBoards")
CREATE_NEW_BOARD_BUTTON = (By.XPATH, "//div[contains(@class,'manage-boardstyles') and span[text()='Create New Board']]")
CONFIRM_BUTTON = (By.XPATH, "//button[contains(.,'Confirm')]")


class SalesOrderBoardPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def is_sales_order_board_displayed(self) -> bool:
        self.wait_for_element_visible(locator=SALES_ORDER_BOARD_CONTAINER)
        return self.is_element_displayed(locator=SALES_ORDER_BOARD_CONTAINER)

    def get_url_sales_order_board(self):
        return "/sales-order-board/tl"

    def open_page_tl(self):
        self.driver.get(f"{self.get_base_url()}/sales-order-board/tl")

    def open_page_ltl(self):
        self.driver.get(f"{self.get_base_url()}/sales-order-board/ltl")

    def open_page_tl_sales_order(self, order_bk):
        self.driver.get(f"{self.get_base_url()}/tl/sales-order/{order_bk}")

    def wait_for_board_rows_to_be_loaded(self, timeout=60, timeout_for_loader=30):
        self.wait_for_element_list_not_empty(ROW_DIV, timeout)

    def clear_filters(self):
        self.wait_for_element_clickable(locator=CLEAR_FILTERS_BUTTON)
        self.element_click_by_locator(locator=CLEAR_FILTERS_BUTTON)

    def is_pending_quotes_tab_displayed(self) -> bool:
        return self.is_element_displayed(locator=PENDING_QUOTES_TABLE_CONTAINER)

    def check_if_created_quote_is_visible_in_table(self, quote_number) -> bool:
        all_quotes = self.get_element_list(locator=QUOTE_ID_COLUMN)

        for quote in all_quotes:
            if quote.text.strip() == quote_number:
                return True

        return False

    def get_table(self):
        return SalesOrderBoardTablePageTL(self.driver, self.log)

    def get_available_boards(self):
        elements = self.get_element_list(BOARDS)
        tmp = []
        for element in elements:
            tmp.append({"name": element.text, "id": int(element.get_attribute("id").replace("react-tabs-", ""))})

        return tmp

    def get_available_boards_names(self):
        elements = self.get_element_list(BOARDS)
        tmp = []
        for element in elements:
            tmp.append(element.text)

        return tmp

    def select_boards(self, table_name, wait_for_load=True):
        self.element_click_by_locator((By.CSS_SELECTOR, f"li[title='{table_name}']"))
        if wait_for_load:
            self.wait_for_board_rows_to_be_loaded(timeout=80, timeout_for_loader=80)

    def open_quote_details(self, quote_number, retry_count=5):
        quote_locator = (By.XPATH, f"//span[@id='{QUOTE_ID_COLUMN[1]}' and contains(text(), '{quote_number}')]")
        self.wait_for_element_clickable(locator=quote_locator)

        while not self.is_element_displayed(locator=QUOTE_VIEW_CUSTOMER_NAME) and retry_count > 0:
            self.element_double_click(locator=quote_locator)
            self.wait_for_page_loader_to_show_and_disappear()
            retry_count = retry_count - 1

    def close_quote_details(self, quote_number):
        quote_locator = (By.XPATH, f"//span[@id='{QUOTE_ID_COLUMN[1]}' and contains(text(), '{quote_number}')]")
        self.element_double_click(quote_locator)

    def open_n_top_quote_details(self, n=1, retry_count=5):
        quote_locator = (By.XPATH, f"(//span[@id='{QUOTE_ID_COLUMN[1]}'])[{n}]")
        self.wait_for_element_clickable(locator=quote_locator)

        quote_id = self.get_text(quote_locator)
        while not self.is_element_displayed(locator=QUOTE_VIEW_CUSTOMER_NAME) and retry_count > 0:
            self.element_double_click(locator=quote_locator)
            self.wait_for_page_loader_to_show_and_disappear()
            retry_count = retry_count - 1
        return quote_id

    def click_on_age_minutes_column_filter(self):
        self.element_click_by_locator(locator=AGE_MINUTES_COLUMN_FILTER)
        self.wait_for_board_rows_to_be_loaded()

    def click_on_quoted_tab(self):
        self.wait_for_element_clickable(locator=QUOTED_TAB)
        self.element_click_by_locator(locator=QUOTED_TAB)

    def click_on_delivered_tab(self):
        self.element_click_by_locator(locator=DELIVERED_TAB)

    def click_on_canceled_tab(self):
        self.element_click_by_locator(locator=CANCELED_TAB)

    def click_on_available_tab(self):
        self.element_click_by_locator(locator=AVAILABLE_TAB)

    def click_on_pending_tab(self):
        self.wait_for_element_clickable(locator=PENDING_TAB)
        self.element_click_by_locator(locator=PENDING_TAB)

    def click_on_pending_quotes(self):
        self.wait_for_element_clickable(locator=PENDING_QUOTES_TAB, timeout=30)
        self.element_click_by_locator(locator=PENDING_QUOTES_TAB)

    def wait_for_page_loader_to_show_and_disappear(self, show_timeout=5, disappear_timeout=10):
        try:
            self.wait_for_element(locator=QUOTE_PAGE_SPINNER, event="display", timeout=show_timeout)
            self.wait_for_element(locator=QUOTE_PAGE_SPINNER, event="notdisplay", timeout=disappear_timeout)

        except Exception as E:
            self.log.error(f"Exception in wait_for_page_loader_to_show_and_disappear Method and Exception is: {str(E)}")

    def click_create_new_board(self):
        self.element_click_by_locator(MANAGE_BOARDS_BUTTON)
        self.element_click_by_locator(CREATE_NEW_BOARD_BUTTON)
        return SalesOrderBoardCreateNewBoardPageTL(self.driver, self.log)

    def delete_custom_board(self, board_name):
        self.element_click_by_locator(MANAGE_BOARDS_BUTTON)
        self.element_click_by_locator(
            locator=(By.XPATH, f"//div[contains(@class,'manage-boardstyles') and span[text()='Delete {board_name}']]"))
        self.element_click_by_locator(CONFIRM_BUTTON)
