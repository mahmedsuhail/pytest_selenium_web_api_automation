from selenium.webdriver.common.by import By

from pages.basepage import BasePage

CANCEL_BUTTON = (By.XPATH, "//button//span[text()='Cancel']")
SEND_RATE_BUTTON = (By.XPATH, "//button//span[text()='Send Rate']")
TEXT_AREA = (By.ID, "financials_TL-2181_quoteItNoteModal_textarea")
ENTER_NOTE_MODAL = (By.ID, "financials_TL-2181_quoteItNoteModal")


class SalesOrderBoardCreateNotePageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_text_from_area(self):
        return self.get_text(locator=TEXT_AREA)

    def set_text_in_area(self, value):
        self.send_keys(locator=TEXT_AREA, data=value)

    def click_cancel_button(self):
        self.element_click_by_locator(locator=CANCEL_BUTTON)

    def click_send_rate_button(self):
        self.element_click_by_locator(locator=SEND_RATE_BUTTON)
        return SalesOrderPendingQuoteEmailDocumentModal(self.driver, self.log)

    def check_if_modal_is_opened(self):
        return self.is_element_displayed(locator=ENTER_NOTE_MODAL)


class SalesOrderPendingQuoteEmailDocumentModal(BasePage):
    DOCUMENT_MODAL = (By.ID, "QuoteIt_TL-9514_email-popup_modal")
    # EMAIL_TO = (By.XPATH, "//div[@id='QuoteIt_TL-9514_email-popup_select_0']//div[contains(@class,'multiValue')]")
    EMAIL_TO = (By.ID, "QuoteIt_TL-9514_email-popup_select_0")
    EMAIL_TO_INPUT = (By.XPATH, "//div[@id='QuoteIt_TL-9514_email-popup_select_0']//input")
    EMAIL_TO_MENU = (By.XPATH, "//div[@id='QuoteIt_TL-9514_email-popup_select_0']//div[contains(@class,'menu')]")
    COPY_EMAIL_TO = (By.ID, "QuoteIt_TL-9514_email-popup_select_1")
    COPY_EMAIL_TO_INPUT = (By.XPATH, "//div[@id='QuoteIt_TL-9514_email-popup_select_1']//input")
    COPY_EMAIL_TO_MENU = (By.XPATH, "//div[@id='QuoteIt_TL-9514_email-popup_select_1']//div[contains(@class,'menu')]")
    SUBJECT = (By.ID, "QuoteIt_TL-9514_email-popup_label_0")
    PDF_CHECKBOX = (By.ID, "QuoteIt_TL-9514_email-popup_checkbox_0")
    HTML_CHECKBOX = (By.ID, "QuoteIt_TL-9514_email-popup_checkbox_1")
    CANCEL_BUTTON = (By.ID, "QuoteIt_TL-9514_email-popup_button_0")
    SEND_BUTTON = (By.ID, "QuoteIt_TL-9514_email-popup_button_1")
    EMAIL_BODY_TEXTAREA = (By.ID, "QuoteIt_TL-9514_email-popup_textarea_0")
    EMAIL_SENT_STATUS = (By.XPATH, "//div[text()='Email Sent']")
    OKAY_BUTTON = (By.XPATH, "//button[@id='QuoteIt_TL-9514_email-popup_button_0' and .='Okay']")

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def is_modal_opened(self):
        return self.is_element_displayed(locator=self.DOCUMENT_MODAL)

    def is_pdf_checked(self):
        return self.get_attribute(locator=self.PDF_CHECKBOX, attribute_type="checked")

    def is_html_checked(self):
        return self.get_attribute(locator=self.HTML_CHECKBOX, attribute_type="checked")

    def get_to_emails(self):
        return self.get_text(locator=self.EMAIL_TO)

    def get_copy_to_emails(self):
        return self.get_text(locator=self.COPY_EMAIL_TO)

    def get_email_subject(self):
        return self.get_text(locator=self.SUBJECT)

    def click_send(self):
        self.element_click_by_locator(self.SEND_BUTTON)

    def click_cancel(self):
        self.element_click_by_locator(self.CANCEL_BUTTON)

    def click_okay(self):
        self.element_click_by_locator(self.OKAY_BUTTON)

    def wait_for_email_sent_status(self):
        self.wait_for_element_visible(self.EMAIL_SENT_STATUS)

    def set_email_body(self, text):
        self.send_keys(locator=self.EMAIL_BODY_TEXTAREA, data=text)

    def set_email_to(self, email_address):
        self.send_keys(locator=self.EMAIL_TO_INPUT, data=email_address, clear=True)
        self.element_click_by_locator(locator=self.EMAIL_TO_MENU)

    def set_email_copy_to(self, email_address):
        self.send_keys(locator=self.COPY_EMAIL_TO_INPUT, data=email_address, clear=True)
        self.element_click_by_locator(locator=self.COPY_EMAIL_TO_MENU)
