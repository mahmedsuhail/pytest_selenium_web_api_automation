import time

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage
from pages.basictypes.checkbox import Checkbox

CHECKBOX_COLUMNS_DROPDOWN_LABELS = (By.CSS_SELECTOR, "div[class*='select-columnsstyles'] label")
POPUP_FILTER_COLUMNS_CONTAINER = (
    By.XPATH,
    "//div[contains(@class,'select-columnsstyles')]//div[contains(@class,'checkbox-liststyles__Container')]")
TOP_NAV_SEARCH_INPUT = (By.ID, "topNav_TL-2970_search_1")


class SalesOrderBoardTableColumnPageTL(BasePage):

    def __init__(self, driver, log, column_header_web_element: WebElement, column_filter_web_element: WebElement):
        super().__init__(driver, log)
        self.column_header_element = column_header_web_element
        self.column_filter_element = column_filter_web_element

    def get_column_name(self):
        return self.column_header_element.get_attribute("innerText")

    def get_column_name_and_position_index(self):
        return {"name": self.column_header_element.get_attribute("innerText"),
                "position": self.column_header_element.get_attribute("aria-colindex")}

    def get_all_available_columns_dropdown(self):
        self.right_click_column_header()

        columns_checkboxes = []
        columns_checkboxes_elements = self.get_element_list(CHECKBOX_COLUMNS_DROPDOWN_LABELS)

        for a_label in columns_checkboxes_elements:
            columns_checkboxes.append(Checkbox(
                name=a_label.get_attribute("innerText"),
                label_web_element=a_label,
                log=self.log,
                driver=self.driver
            ))

        return columns_checkboxes

    def right_click_column_header(self):
        self.scroll_into_view(element=self.column_header_element)
        self.element_right_click(element=self.column_header_element)

    def close_column_popup(self):
        self.element_click_by_locator(TOP_NAV_SEARCH_INPUT)

    def check_all_columns_in_filter_dropdown(self, checkboxes):
        unchecked = [column for column in checkboxes if column.is_checked() is False]
        for element in unchecked:
            element.check()

    def uncheck_all_columns_in_filter_dropdown(self, checkboxes):
        checked = [column for column in checkboxes if column.is_checked() is True]
        for element in checked:
            element.uncheck()

    def is_popup_column_filter_visible(self):
        columns_checkboxes_elements = self.get_element_list(CHECKBOX_COLUMNS_DROPDOWN_LABELS)
        return columns_checkboxes_elements[0].is_displayed()

    def is_popup_column_filter_contain_scroll(self):
        container_element = self.get_element(POPUP_FILTER_COLUMNS_CONTAINER)
        overflow_exist = container_element.value_of_css_property("overflow") == "auto"
        height = container_element.value_of_css_property("height") != "none"
        return overflow_exist and height

    def grab_and_move_to_column(self, to_column):
        self.grab_and_move_to(self.column_header_element, to_column.column_header_element)

    def get_column_width(self):
        return int(self.column_header_element.value_of_css_property("width").replace("px", ""))

    def resize_column_width(self, change_by_px):
        resize_element = self.column_header_element.find_element_by_css_selector("div[ref='eResize']")
        self.grab_and_move_by_offset(resize_element, xoffset=change_by_px, yoffset=0)

    def retry_resize_column_width(self, change_by_px, base_size, timeout=60):
        desired_size = base_size + change_by_px
        end_time = time.time() + timeout
        while True:
            self.resize_column_width(change_by_px)
            if self.get_column_width() == desired_size:
                return
            time.sleep(1)
            if time.time() > end_time:
                break
