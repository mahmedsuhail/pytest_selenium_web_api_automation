from selenium.webdriver.common.by import By

from pages.basepage import BasePage
from pages.basictypes.basic_types_factory import BasicTypesFactory

BOARD_NAME_INPUT = (By.CSS_SELECTOR, "input[name='BoardName']")
SELECT_ALL_LABEL = (By.XPATH, "//label[input[@id='create-board-sidebar-controller']]")
SAVE_AND_CLOSE_BUTTON = (By.XPATH, "//button[contains(.,'SAVE & CLOSE')]")
CONFIRM_BUTTON = (By.XPATH, "//button[contains(.,'Confirm')]")


class SalesOrderBoardCreateNewBoardPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.basic_types_factory = BasicTypesFactory(driver=driver, log=log)

    def set_board_name(self, name):
        self.send_keys(locator=BOARD_NAME_INPUT, data=name)

    def click_select_all_checkbox(self):
        checkbox = self.basic_types_factory.get_checkbox(name="selectAll", locator=SELECT_ALL_LABEL)
        checkbox.check()

    def click_save_and_close(self):
        self.element_click_by_locator(SAVE_AND_CLOSE_BUTTON)
        self.element_click_by_locator(CONFIRM_BUTTON)
