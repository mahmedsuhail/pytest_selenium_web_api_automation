from selenium.webdriver.common.by import By

from pages.TL.salesorderboard.sales_order_board_table_column_page_tl import SalesOrderBoardTableColumnPageTL
from pages.TL.salesorderboard.sales_order_board_table_row_page_tl import SalesOrderBoardTableRowPageTL
from pages.basepage import BasePage

BOL_COLUMN_FILTER_INPUT = (By.ID, "salesOrderBoard_LTL1-1734_customText_billOfLading_textField")
QUOTE_ID_COLUMN_FILTER_INPUT = (By.ID, "salesOrderBoard_LTL1-1734_customText_quoteNumber_textField")
PAGINATION_INPUT = (By.CSS_SELECTOR, "input[id*='salesOrderBoard_tl-1810_pageInput_']")
COLUMN_HEADER = (By.CSS_SELECTOR, "div[role='columnheader'][col-id]")
ROW_DIV = (By.CSS_SELECTOR, "div[row-id]")
LOADING_ICON = (By.CSS_SELECTOR, ".ag-loading")
NO_ROWS_TO_SHOW = (By.XPATH, "//span[contains(.,'No Rows To Show')]")


class SalesOrderBoardTablePageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def set_bol_column_filter(self, value):
        self.wait_for_element_clickable(locator=BOL_COLUMN_FILTER_INPUT)
        self.send_keys(value, BOL_COLUMN_FILTER_INPUT, clear=True)
        self.wait_for_board_loader_not_visible()

    def set_quote_id_column_filter(self, value):
        self.wait_for_element_clickable(locator=QUOTE_ID_COLUMN_FILTER_INPUT)
        self.send_keys(value, QUOTE_ID_COLUMN_FILTER_INPUT)
        self.wait_for_board_loader_not_visible()

    def check_if_order_is_present(self, order_bol_number) -> bool:
        order_locator = (By.ID, f"salesOrderBoard_LTL1-1734_customBolHyperlink_{order_bol_number}_hyperLink")
        return self.is_element_displayed(locator=order_locator)

    def check_if_quote_is_present(self, quote_id) -> bool:
        order_locator = (By.XPATH, f"//div[@role='row' and div[@col-id='quoteNumber' and contains(.,'{quote_id}')]]")
        return self.is_element_displayed(locator=order_locator)

    def open_order(self, order_bol_number):
        order_locator = (By.ID, f"salesOrderBoard_LTL1-1734_customBolHyperlink_{order_bol_number}_hyperLink")
        self.element_click_by_locator(locator=order_locator)

    def set_pagination(self, page_number, wait_for_load=True):
        self.send_keys(data=page_number, locator=PAGINATION_INPUT)
        if wait_for_load:
            self.wait_for_board_rows_to_be_loaded()

    def get_rows(self):
        elements = self.get_element_list(ROW_DIV)
        list = []
        for element in elements:
            list.append(SalesOrderBoardTableRowPageTL(self.driver, self.log, element))
        return list

    def get_row(self, row_index):
        return self.get_rows()[row_index]

    def wait_for_board_rows_to_be_loaded(self, timeout=10):
        self.wait_for_element_list_not_empty(ROW_DIV, 120)

    def wait_for_board_loader_not_visible(self):
        self.wait_for_element_not_exist(LOADING_ICON)

    def wait_for_board_showing_no_rows_to_show(self, timeout=60, timeout_for_loader=30):
        self.wait_for_element_visible(NO_ROWS_TO_SHOW, timeout)

    def scroll_to_div_test(self):
        elements = self.get_element_list((By.XPATH, "//div[@row-index='0']/div[@role='gridcell']"))
        if len(elements) > 0:
            self.scroll_into_view(elements[0])

    def get_columns(self):  # takes very long time
        tmp = []
        elements_column_headers = self.get_element_list(COLUMN_HEADER)
        for index, element_column_header in enumerate(elements_column_headers):
            element_col_index = element_column_header.get_attribute("aria-colindex")
            if element_col_index is None:
                element_ref = element_column_header.find_element_by_css_selector("span[ref='eText'][aria-colindex]")
                element_col_index = element_ref.get_attribute("aria-colindex")
            element_column_filter = self.get_element(locator=(
                By.CSS_SELECTOR, f"div[role='columnheader']:not([col-id])[aria-colindex='{element_col_index}']"))

            tmp.append(
                SalesOrderBoardTableColumnPageTL(self.driver, self.log, element_column_header, element_column_filter))
        return tmp

    def get_columns_sorted(self):  # takes very long time
        tmp = []
        elements_column_headers = self.get_element_list(COLUMN_HEADER)
        for index, element_column_header in enumerate(elements_column_headers):
            element_col_index = element_column_header.get_attribute("aria-colindex")
            if element_col_index is None:
                element_ref = element_column_header.find_element_by_css_selector("span[ref='eText'][aria-colindex]")
                element_col_index = element_ref.get_attribute("aria-colindex")
            element_column_filter = self.get_element(locator=(
                By.CSS_SELECTOR, f"div[role='columnheader']:not([col-id])[aria-colindex='{element_col_index}']"))

            tmp.append({"element": SalesOrderBoardTableColumnPageTL(self.driver, self.log, element_column_header,
                                                                    element_column_filter),
                        "position": element_col_index})

        tmp_sorted = sorted(tmp, key=lambda x: int(x['position']))
        return [item['element'] for item in tmp_sorted]

    def get_columns_names(self, columns_list):
        tmp = []
        for column in columns_list:
            tmp.append(column.get_column_name())
        return tmp

    def get_columns_names_sorted_by_position(self, columns_list):
        tmp = []
        for column in columns_list:
            tmp.append(column.get_column_name_and_position_index())

        tmp_sorted = sorted(tmp, key=lambda x: int(x['position']))
        return [item['name'] for item in tmp_sorted]

    def get_columns_count(self):
        return len(self.get_element_list(COLUMN_HEADER))
