from selenium.webdriver.common.by import By

from pages.basepage import BasePage

CANCEL_BUTTON = (By.XPATH, "//button//span[text()='Cancel']")
SEND_RATE_BUTTON = (By.XPATH, "//button//span[text()='Send Rate']")
TEXT_AREA = (By.ID, "financials_TL-2181_quoteItNoteModal_textarea")
ENTER_NOTE_MODAL = (By.ID, "financials_TL-2181_quoteItNoteModal")


class SalesOrderBoardCreateNotePageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_text_from_area(self):
        return self.get_text(locator=TEXT_AREA)

    def set_text_in_area(self, value):
        self.send_keys(locator=TEXT_AREA, data=value)

    def click_cancel_button(self):
        self.element_click_by_locator(locator=CANCEL_BUTTON)

    def click_send_rate_button(self):
        self.element_click_by_locator(locator=SEND_RATE_BUTTON)

    def check_if_modal_is_opened(self):
        return self.is_element_displayed(locator=ENTER_NOTE_MODAL)
