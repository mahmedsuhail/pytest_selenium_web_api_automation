from selenium.webdriver.common.by import By

from pages.basepage import BasePage
from pages.basictypes.specialinput import SpecialInput

HANDLED_BY_DROPDOWN = (By.ID, "salesOrder_TL-707_insuranceDetails_select")
INSURANCE_PROVIDER_INPUT = (By.ID, "salesOrder_TL-707_insuranceDetails_inputField_1")
INSURANCE_PROVIDER_DIV = (By.XPATH, "//div[input[@id='salesOrder_TL-707_insuranceDetails_inputField_1']]")
ADDITIONAL_INSURANCE_AMOUNT_INPUT = (By.ID, "salesOrder_TL-707_insuranceDetails_inputField_2")
ADDITIONAL_INSURANCE_AMOUNT_DIV = (By.XPATH, "//div[input[@id='salesOrder_TL-707_insuranceDetails_inputField_2']]")
COST_TO_GTZ_OR_CARRIER_DIV = (By.XPATH, "//div[input[@id='salesOrder_TL-707_insuranceDetails_inputField_3']]")
COST_TO_GTZ_OR_CARRIER_INPUT = (By.ID, "salesOrder_TL-707_insuranceDetails_inputField_3")
CHARGE_TO_CUSTOMER_INPUT = (By.ID, "salesOrder_TL-707_insuranceDetails_inputField_4")
SAVE_BUTTON = (By.ID, "salesOrder_TL-707_insuranceDetailsModalButtonSave")
CANCEL_BUTTON = (By.ID, "salesOrder_TL-707_insuranceDetailsModalButtonCancel")
MODAL = (By.ID, "salesOrder_TL-707_insuranceDetails")
CLOSE_MODAL_BUTTON = (By.CSS_SELECTOR, "#salesOrder_TL-707_insuranceDetails div[class*=CloseButton]")

HANDLED_BY_LABEL = (By.CSS_SELECTOR, "label[for='handledBy']")
INSURANCE_PROVIDER_LABEL = (By.CSS_SELECTOR, "label[for='provider']")
ADDITIONAL_INSURANCE_AMOUNT_LABEL = (By.CSS_SELECTOR, "label[for='amount']")
COST_TO_GTZ_OR_CARRIER_LABEL = (By.CSS_SELECTOR, "label[for='carrierCost']")
CHARGE_TO_CUSTOMER_LABEL = (By.CSS_SELECTOR, "label[for='cost']")


class AddInsuranceDetailsPage(BasePage):

    def click_cancel(self):
        self.element_click_by_locator(locator=CANCEL_BUTTON, scroll_to=False)

    def click_save(self):
        self.element_click_by_locator(locator=SAVE_BUTTON, scroll_to=False)

    def get_charge_to_customer(self):
        return self.get_textbox_value(locator=CHARGE_TO_CUSTOMER_INPUT)

    def get_cost_to_gtz_or_carrier(self):
        return self.get_textbox_value(locator=COST_TO_GTZ_OR_CARRIER_INPUT)

    def get_cost_to_gtz_or_carrier_placeholder(self):
        return self.get_attribute(locator=COST_TO_GTZ_OR_CARRIER_INPUT, attribute_type="placeholder")

    def get_cost_to_gtz_or_carrier_border_color(self):
        return self.get_element(locator=COST_TO_GTZ_OR_CARRIER_DIV).value_of_css_property("border-bottom-color")

    def is_modal_visible(self):
        return self.is_element_displayed(locator=MODAL)

    def close_modal(self):
        return self.element_click_by_locator(locator=CLOSE_MODAL_BUTTON)

    def select_handled_by(self, value):
        dropdown = self.__get_handled_by()
        dropdown.select_option_by_value(value)

    def get_handled_by(self) -> str:
        dropdown = self.__get_handled_by()
        return dropdown.get_selected_value()

    def get_handled_by_dropdown_options(self) -> [str]:
        dropdown = self.__get_handled_by()
        return dropdown.get_available_options_values()

    def get_handled_by_placeholder(self):
        dropdown = self.__get_handled_by()
        return dropdown.get_placeholder()

    def get_handled_by_border_color(self):
        dropdown = self.__get_handled_by()
        return dropdown.get_placeholder_color()

    def set_insurance_provider(self, value):
        self.send_keys(data=value, locator=INSURANCE_PROVIDER_INPUT)

    def clear_insurance_provider_field(self):
        self.clear_by_select_all_and_delete(locator=INSURANCE_PROVIDER_INPUT, press_escape=False)

    def get_insurance_provider(self) -> str:
        return self.get_attribute(locator=INSURANCE_PROVIDER_INPUT, attribute_type="value")

    def get_insurance_provider_placeholder(self):
        return self.get_attribute(locator=INSURANCE_PROVIDER_INPUT, attribute_type="placeholder")

    def get_insurance_provider_border_color(self):
        return self.get_element(locator=INSURANCE_PROVIDER_DIV).value_of_css_property("border-bottom-color")

    def set_additional_insurance_amount(self, value):
        self.send_keys(data=value, locator=ADDITIONAL_INSURANCE_AMOUNT_INPUT)

    def clear_additional_insurance_amount_field(self):
        self.clear_by_select_all_and_delete(locator=ADDITIONAL_INSURANCE_AMOUNT_INPUT, press_escape=False)

    def get_additional_insurance_amount(self) -> str:
        return self.get_attribute(locator=ADDITIONAL_INSURANCE_AMOUNT_INPUT, attribute_type="value")

    def get_additional_insurance_amount_placeholder(self):
        return self.get_attribute(locator=ADDITIONAL_INSURANCE_AMOUNT_INPUT, attribute_type="placeholder")

    def get_additional_insurance_amount_border_color(self):
        return self.get_element(locator=ADDITIONAL_INSURANCE_AMOUNT_DIV).value_of_css_property("border-bottom-color")

    def set_cost_to_gtz_or_carrier(self, value):
        self.send_keys(data=value, locator=COST_TO_GTZ_OR_CARRIER_INPUT)

    def clear_cost_to_gtz_or_carrier_field(self):
        self.clear_by_select_all_and_delete(locator=COST_TO_GTZ_OR_CARRIER_INPUT, press_escape=False)

    def set_charge_to_customer(self, value):
        self.send_keys(data=value, locator=CHARGE_TO_CUSTOMER_INPUT)

    def clear_charge_to_customer_field(self):
        self.clear_by_select_all_and_delete(locator=CHARGE_TO_CUSTOMER_INPUT, press_escape=False)

    def get_handled_by_label_text(self):
        return self.get_text(locator=HANDLED_BY_LABEL)

    def get_insurance_provider_label_text(self):
        return self.get_text(locator=INSURANCE_PROVIDER_LABEL)

    def get_additional_insurance_amount_label_text(self):
        return self.get_text(locator=ADDITIONAL_INSURANCE_AMOUNT_LABEL)

    def get_cost_to_gtz_or_carrier_label_text(self):
        return self.get_text(locator=COST_TO_GTZ_OR_CARRIER_LABEL)

    def get_charge_to_customer_label_text(self):
        return self.get_text(locator=CHARGE_TO_CUSTOMER_LABEL)

    def is_save_button_displayed(self):
        return self.is_element_displayed(locator=SAVE_BUTTON)

    def is_cancel_button_displayed(self):
        return self.is_element_displayed(locator=CANCEL_BUTTON)

    def __get_handled_by(self):
        return self.basic_types_factory.get_dropdown(HANDLED_BY_DROPDOWN)
