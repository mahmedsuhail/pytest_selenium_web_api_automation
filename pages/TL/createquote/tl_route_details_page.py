import time
from datetime import date

from selenium.webdriver.common.by import By

from pages.TL.createquote.contactbook.contactbook_destination_page import ContactBookDestinationPage
from pages.TL.createquote.contactbook.contactbook_origin_page import ContactBookOriginPage
from pages.basepage import BasePage

ORIGIN_INPUT = (By.ID, "routeDetails_TL-595_inputField_1")
DESTINATION_INPUT = (By.ID, "routeDetails_TL-595_inputField_2")
APPOINTMENT_REQUIRED_CHECKBOX = (By.XPATH, "//*[contains(text(), 'Appointment Required')]")
APPOINTMENT_CONFIRMED_WITH_LOCATION_CHECKBOX = (
    By.XPATH, "//*[contains(text(), 'Appointment Confirmed With Location')]")
ORIGIN_ADDRESS_DROPBOX = (
    By.XPATH, "//*[@id='routeDetails_TL-595_inputField_1']//div[contains(@class,'-menu')]/div/div")
DESTINATION_ADDRESS_DROPBOX = (By.CSS_SELECTOR, "#routeDetails_TL-595_inputField_2 div[class*='-menu'] div")
ORIGIN_ADDRESS_DROPBOX_FIRST_RESULT = (By.ID, "react-select-11-option-0")
DESTINATION_ADDRESS_DROPBOX_FIRST_RESULT = (By.ID, "react-select-14-option-0")
ORIGIN_LOADING_ELEMENT = (By.XPATH, "//label[text()='*Origin']/following-sibling::div//div[contains(text(),'Loading')]")
ORIGIN_CALENDAR = (By.XPATH, "(//div[contains(@class, 'react-datepicker-wrapper')])[1]")
ORIGIN_OPEN_TIME_PICKER = (By.NAME, "originOpenTime")
ORIGIN_CLOSE_TIME_PICKER = (By.NAME, "originCloseTime")
DESTINATION_CALENDAR = (By.XPATH, "(//div[contains(@class, 'react-datepicker-wrapper')])[2]")
DESTINATION_OPEN_TIME_PICKER = (By.NAME, "destinationOpenTime")
DESTINATION_CLOSE_TIME_PICKER = (By.NAME, "destinationCloseTime")
PICKUP_DATE = (By.XPATH, "//label[@for='pickUpDate']/.. //div[@class='react-datepicker__input-container']//input")
DROP_OFF_DATE = (By.XPATH, "//label[@for='dropOffDate']/.. //div[@class='react-datepicker__input-container']//input")
ORIGIN_OPEN_TIME = (
    By.CSS_SELECTOR, "input[name='originOpenTime']")
ORIGIN_CLOSE_TIME = (
    By.CSS_SELECTOR, "input[name='originCloseTime']")
DESTINATION_OPEN_TIME = (
    By.CSS_SELECTOR, "input[name='destinationOpenTime']")
DESTINATION_CLOSE_TIME = (
    By.CSS_SELECTOR, "input[name='destinationCloseTime']")
APPOINTMENT_REQUIRED_CHECKBOX_CHECKED = (
    By.XPATH, "//*[contains(text(), 'Appointment Required')]/preceding-sibling::div//*[local-name() = 'svg']")
APPOINTMENT_CONFIRMED_CHECKBOX_CHECKED = \
    (By.XPATH,
     "//*[contains(text(), 'Appointment Confirmed With Location')]/preceding-sibling::div//*[local-name() = 'svg']")
ADD_ADDITIONAL_STOP_BUTTON = (By.ID, "TL-2383_add_stops_button")


class TLRouteDetailsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def tl_route_details_visible(self):
        self.wait_for_element(APPOINTMENT_REQUIRED_CHECKBOX)

    def clear_route_details(self):
        self.wait_for_element(APPOINTMENT_REQUIRED_CHECKBOX)
        self.clear_field(locator=APPOINTMENT_REQUIRED_CHECKBOX)

    def search_and_select_origin(self, search_string, expected_result):
        _input = self.basic_types_factory.get_input(locator=ORIGIN_INPUT)
        _input.set_text_value(search_string, select_option=False)

        dropdown = self.basic_types_factory.get_dropdown(locator=ORIGIN_INPUT)
        dropdown.wait_for_dropdown_to_appear()
        dropdown.select_option_by_value(value=expected_result)

    def search_and_select_destination(self, search_string, expected_result):
        _input = self.basic_types_factory.get_input(locator=DESTINATION_INPUT)
        _input.set_text_value(search_string, select_option=False)

        dropdown = self.basic_types_factory.get_dropdown(locator=DESTINATION_INPUT)
        dropdown.wait_for_dropdown_to_appear()
        dropdown.select_option_by_value(value=expected_result)

    def select_origin_by_zip_code_exact_match(self, zip_code):
        _input = self.basic_types_factory.get_input(locator=ORIGIN_INPUT)
        _input.set_text_value(zip_code)

    def select_destination_by_zip_code_exact_match(self, zip_code):
        _input = self.basic_types_factory.get_input(locator=DESTINATION_INPUT)
        _input.set_text_value(zip_code)

    def select_origin_by_name_exact_match(self, name: str):
        contact_book = ContactBookOriginPage(self.driver, self.log)
        contact_book.click_on_icon()
        contact_book.set_search_text(name)
        contact_book.select_first()

    def select_destination_by_name_exact_match(self, name: str):
        contact_book = ContactBookDestinationPage(self.driver, self.log)
        contact_book.click_on_icon()
        contact_book.set_search_text(name)
        contact_book.select_first()

    def get_origin_address_text(self, before_change: str = None) -> str:
        _input = self.basic_types_factory.get_input(locator=ORIGIN_INPUT)
        return self.__get_address_text(address_input=_input, before_change=before_change)

    def get_destination_address_text(self, before_change: str = None) -> str:
        _input = self.basic_types_factory.get_input(locator=DESTINATION_INPUT)
        return self.__get_address_text(address_input=_input, before_change=before_change)

    def get_pickup_date_value(self) -> str:
        return self.get_element(locator=PICKUP_DATE).get_property(name="value")

    def get_dropoff_date_value(self) -> str:
        return self.get_element(locator=DROP_OFF_DATE).get_property(name="value")

    def get_origin_open_time_value(self) -> str:
        return self.get_textbox_value(locator=ORIGIN_OPEN_TIME)

    def get_origin_close_time_value(self) -> str:
        return self.get_textbox_value(locator=ORIGIN_CLOSE_TIME)

    def get_destination_open_time_value(self) -> str:
        return self.get_textbox_value(locator=DESTINATION_OPEN_TIME)

    def get_destination_close_time_value(self) -> str:
        return self.get_textbox_value(locator=DESTINATION_CLOSE_TIME)

    # TODO - right now we do not differentiate between origin & destination appointment required checkboxes.
    #  We should request automation id's
    def get_appointment_required_checked_status(self) -> bool:
        return self.is_element_present(locator=APPOINTMENT_REQUIRED_CHECKBOX_CHECKED)

    def get_appointment_confirmed_checked_status(self) -> bool:
        return self.is_element_present(locator=APPOINTMENT_CONFIRMED_CHECKBOX_CHECKED)

    def clear_origin(self):
        _input = self.basic_types_factory.get_input(locator=ORIGIN_INPUT)
        _input.clear()

    def clear_destination(self):
        _input = self.basic_types_factory.get_input(locator=DESTINATION_INPUT)
        _input.clear()

    def set_origin_pickup_date(self, date_to_set: date):
        calendar = self.basic_types_factory.get_calendar(locator=ORIGIN_CALENDAR)
        calendar.set_date(date_to_set=date_to_set)

    # TODO - REFACTOR
    def set_origin_open_time(self, time_to_set: str):
        time_picker = self.basic_types_factory.get_time_picker(locator=ORIGIN_OPEN_TIME_PICKER)
        time_picker.set_time_value_type_text(time_to_set=time_to_set)

    # TODO - REFACTOR
    def set_origin_close_time(self, time_to_set: str):
        time_picker = self.basic_types_factory.get_time_picker(locator=ORIGIN_CLOSE_TIME_PICKER)
        time_picker.set_time_value_type_text(time_to_set=time_to_set)

    def set_destination_pickup_date(self, date_to_set: date):
        calendar = self.basic_types_factory.get_calendar(locator=DESTINATION_CALENDAR)
        calendar.set_date(date_to_set=date_to_set)

    # TODO - REFACTOR
    def set_destination_open_time(self, time_to_set: str):
        time_picker = self.basic_types_factory.get_time_picker(locator=DESTINATION_OPEN_TIME_PICKER)
        time_picker.set_time_value_type_text(time_to_set=time_to_set)

    # TODO - REFACTOR
    def set_destination_close_time(self, time_to_set: str):
        time_picker = self.basic_types_factory.get_time_picker(locator=DESTINATION_CLOSE_TIME_PICKER)
        time_picker.set_time_value_type_text(time_to_set=time_to_set)

    def get_origin_data(self) -> str:
        _input = self.basic_types_factory.get_input(ORIGIN_INPUT)
        return _input.get_text_value()

    def get_destination_data(self) -> str:
        _input = self.basic_types_factory.get_input(DESTINATION_INPUT)
        return _input.get_text_value()

    def get_origin_data(self) -> str:
        _input = self.basic_types_factory.get_input(ORIGIN_INPUT)
        return _input.get_text_value()

    def get_destination_data(self) -> str:
        _input = self.basic_types_factory.get_input(DESTINATION_INPUT)
        return _input.get_text_value()

    def __get_address_text(self, address_input, before_change):
        text_value = address_input.get_text_value()
        if before_change is not None:
            attempts = 0
            while attempts <= self.NO_ATTEMPTS:
                if before_change != text_value:
                    break
                time.sleep(self.BETWEEN_CLICK_DELAY)
                attempts += 1
                text_value = address_input.get_text_value()

        return text_value

    def click_add_additional_stop_button(self):
        self.element_click_by_locator(locator=ADD_ADDITIONAL_STOP_BUTTON)
