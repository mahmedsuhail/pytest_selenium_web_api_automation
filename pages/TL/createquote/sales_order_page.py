import time

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement

from pages.TL.createquote.sales_order_page_add_insurance_details_page import AddInsuranceDetailsPage
from pages.basepage import BasePage

ADD_SHIPPER_INSURANCE_SECTION = (By.CSS_SELECTOR, "#salesOrder_TL-707 div")
CARRIER_INSURANCE_SECTION_LABEL = (By.CSS_SELECTOR, "#salesOrder_TL-707_carrierInsurance > div:nth-child(1)")
CARRIER_INSURANCE_SECTION_TEXT = (By.CSS_SELECTOR, "#salesOrder_TL-707_carrierInsurance > div:nth-child(2)")
ADD_SHIPPER_INSURANCE_BUTTON = (By.ID, "salesOrder_TL-707_icon_Plus")
EDIT_SHIPPER_INSURANCE_BUTTON = (By.ID, "salesOrder_TL-707_icon_Pencil")
ITEM_TABLE_ROW = (By.CSS_SELECTOR, "div[data-testid='sales-order-item']")
TOTAL_VOLUME = (By.XPATH, "//div[contains(@class,'ShipmentSummaryRow') and div[contains(.,'Total Volume')]]")
TOTAL_DENSITY = (By.XPATH, "//div[contains(@class,'ShipmentSummaryRow') and div[contains(.,'Total Density')]]")
TOTAL_WEIGHT = (By.XPATH, "//div[contains(@class,'ShipmentSummaryRow') and div[contains(.,'Total Weight')]]")
TOTAL_SHIPMENT_VALUE = (By.ID, "salesOrder_TL-707_inputField")
RESTRICTED_LINK = (By.ID, "salesOrder_TL-707_link")
SHIPPER_INSURANCE_REMOVE_BUTTON = (By.ID, "salesOrder_TL-707_icon_Minus")
ITEMS_CONTAINER = (By.ID, "salesOrder_TL-707_items_itemsContainer")


class CreateQuoteSalesOrderPageTL(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_total_volume(self):
        return self.get_text(TOTAL_VOLUME).splitlines()[1]

    def get_total_density(self):
        return self.get_text(TOTAL_DENSITY).splitlines()[1]

    def get_total_weight(self):
        return self.get_text(TOTAL_WEIGHT).splitlines()[1]

    def get_total_shipment_value(self) -> str:
        total_shipment = self.basic_types_factory.get_input(locator=TOTAL_SHIPMENT_VALUE)
        return total_shipment.get_text_value()

    def set_total_shipment_value_and_unfocus(self, value):
        self.send_keys(locator=TOTAL_SHIPMENT_VALUE, data=value)
        self.send_keys_to_browser(data=Keys.TAB)

    def clear_total_shipment_value(self):
        self.basic_types_factory.get_input(locator=TOTAL_SHIPMENT_VALUE).clear()

    def enter_total_shipment_value(self, total_shipment_value):
        _input = self.basic_types_factory.get_input(locator=TOTAL_SHIPMENT_VALUE)
        _input.set_text_value(total_shipment_value)

    def get_sale_item_rows(self):
        elements = self.get_element_list(locator=ITEM_TABLE_ROW)
        wrapped_rows = []
        for x in elements:
            wrapped_rows.append(
                ItemRow(driver=self.driver, base_row_element=x,
                        log=self.log))
        return wrapped_rows

    def click_shipper_insurance_remove_button(self):
        self.element_click_by_locator(SHIPPER_INSURANCE_REMOVE_BUTTON)
        return RemoveInsurancePopUp(self.driver, self.log)

    def click_edit_shipper_insurance_button(self) -> AddInsuranceDetailsPage:
        self.element_click_by_locator(EDIT_SHIPPER_INSURANCE_BUTTON)
        return AddInsuranceDetailsPage(self.driver, self.log)

    def is_shipper_insurance_remove_button_visible(self):
        return self.is_element_displayed(SHIPPER_INSURANCE_REMOVE_BUTTON)

    def wait_for_sale_item_have_count_and_return(self, count, timeout):
        end_time = time.time() + timeout
        while True:
            rows = self.get_sale_item_rows()
            if len(rows) == count:
                return rows
            time.sleep(1)
            if time.time() > end_time:
                break

        self.log.info(f"Wait for Sale item count equal to {count} time out after: {timeout} seconds")
        raise Exception(f"Wait for Sale item count equal to {count} time out after: {timeout} seconds")

    def get_restricted_commodities_link(self):
        """
        Fetching Restricted/Prohibited Commodities Link Name and hyperlink url.
        :return: link text and hyper link url.
        """
        try:
            self.log.debug("Inside get_restricted_commodities_link Method")
            link_text = self.get_text(RESTRICTED_LINK)
            hyper_link_url = self.get_attribute(RESTRICTED_LINK, attribute_type="href")
            return link_text, hyper_link_url
        except Exception as E:
            self.log.errror(f"Passed locator is incorrect or got updated,"
                            f"inside get_restricted_commodities_link Method and Exception is : {str(E)}")

    def add_shipper_interest_insurance_button_is_displayed(self):
        return self.is_element_displayed(locator=ADD_SHIPPER_INSURANCE_BUTTON)

    def edit_shipper_interest_insurance_button_is_displayed(self):
        return self.is_element_displayed(locator=EDIT_SHIPPER_INSURANCE_BUTTON)

    def remove_shipper_interest_insurance_button_is_displayed(self):
        return self.is_element_displayed(locator=SHIPPER_INSURANCE_REMOVE_BUTTON)

    def get_shipper_interest_insurance_section_text(self):
        return self.get_text(locator=ADD_SHIPPER_INSURANCE_SECTION)

    def get_carrier_insurance_section_label(self):
        return self.get_text(locator=CARRIER_INSURANCE_SECTION_LABEL)

    def get_carrier_insurance_section_text(self):
        return self.get_text(locator=CARRIER_INSURANCE_SECTION_TEXT)

    def click_and_open_add_shipper_insurance(self):
        self.element_click_by_locator(ADD_SHIPPER_INSURANCE_BUTTON, False)
        return AddInsuranceDetailsPage(self.driver, self.log)

    def set_total_shipment_value(self, value, clear):
        self.send_keys(data=value, locator=TOTAL_SHIPMENT_VALUE, clear=clear)

    def is_scroll_visible_in_items_list(self):
        element = self.get_element(ITEMS_CONTAINER)
        overflow_exist = element.value_of_css_property("overflow") != "visible"
        max_height = element.value_of_css_property("max-height") != "none"
        return overflow_exist and max_height


class ItemRow(BasePage):
    EDIT_ITEM = "button[data-testid=sales-order-edit-btn]"
    REMOVE_ITEM = "button[data-testid=sales-order-delete-btn]"

    def __init__(self, driver, base_row_element: WebElement, log):
        super().__init__(driver, log)
        self.base_row_element = base_row_element

    def get_commodity_text(self):
        return self.get_text(
            locator=None,
            element=self.base_row_element.find_element_by_css_selector("div[class*='CommodityText']"))

    def click_edit_item(self):
        element = self.base_row_element.find_element_by_css_selector(self.EDIT_ITEM)
        self.element_click(element)

    def click_remove_item(self):
        element = self.base_row_element.find_element_by_css_selector(self.REMOVE_ITEM)
        self.element_click(element)

    def is_edit_item_button_visible(self):
        return len(self.base_row_element.find_elements_by_css_selector(self.EDIT_ITEM)) > 0


class RemoveInsurancePopUp(BasePage):
    MODAL = (By.ID, "salesOrder_TL-707_removeInsuranceDetails")
    NO_BUTTON = (By.ID, "salesOrder_TL-707_removeInsuranceButtonCancel")
    YES_BUTTON = (By.ID, "salesOrder_TL-707_removeInsuranceButtonSave")

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_yes(self):
        self.element_click_by_locator(self.YES_BUTTON)

    def click_no(self):
        self.element_click_by_locator(self.NO_BUTTON)

    def wait_for_popup_visible(self):
        self.wait_for_element_visible(self.MODAL)
