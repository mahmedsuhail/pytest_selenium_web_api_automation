import time

from pages.TL.createquote.contactbook.contactbook_page import ContactBookPage
from pages.basepage import BasePage
from pages.basictypes.basic_types_factory import BasicTypesFactory


class RouteLocationsPage(BasePage):

    def __init__(self, driver, log, locators: {}, contactbook_page: ContactBookPage):
        super().__init__(driver, log)
        self.locators = locators
        self.contactbook_page = contactbook_page
        self.element_factory = BasicTypesFactory(log=log, driver=driver)
        self.elements_loaded = False

    def init_elements(self):
        if not self.elements_loaded:
            self._input_company = self.element_factory.get_input(locator=self.locators["COMPANY_NAME"])
            self._input_contact_name = self.element_factory.get_input(locator=self.locators["CONTACT_NAME"])
            self._input_contact_phone = self.element_factory.get_input(locator=self.locators["CONTACT_PHONE"])
            self._input_contact_email = self.element_factory.get_input(locator=self.locators["CONTACT_EMAIL"])
            self._input_remarks = self.element_factory.get_input(locator=self.locators["REMARKS"])
            self._input_addr1 = self.element_factory.get_input(locator=self.locators["ADDRESS_LINE1"])
            self._input_addr2 = self.element_factory.get_input(locator=self.locators["ADDRESS_LINE2"])
            self._input_city = self.element_factory.get_input(locator=self.locators["ADDRESS_CITY"])
            self._input_zip = self.element_factory.get_input(locator=self.locators["ADDRESS_ZIP"])
            self.elements_loaded = True

    def get_overal_hash(self):
        """
        returns hash of selected values - useful for checking if anything has changed
        """
        return hash(
            self.get_company() +
            self.get_contact_name()
        )

    def get_company(self) -> str:
        self.init_elements()
        return self._input_company.get_text_value()

    def get_contact_name(self) -> str:
        self.init_elements()
        return self._input_contact_name.get_text_value()

    def get_contact_phone(self) -> str:
        self.init_elements()
        return self._input_contact_phone.get_text_value()

    def get_contact_email(self) -> str:
        self.init_elements()
        return self._input_contact_email.get_text_value()

    def get_pickup_remarks(self) -> str:
        self.init_elements()
        return self._input_remarks.get_text_value()

    def get_address(self) -> (str, str, str, str):
        self.init_elements()
        return \
            self._input_addr1.get_text_value(), \
            self._input_addr2.get_text_value(), \
            self._input_city.get_text_value(), \
            self._input_zip.get_text_value()

    def wait_until_changed(self, start_hash):
        attempts = 0
        while attempts <= self.NO_ATTEMPTS:
            current_hash = self.get_overal_hash()
            if current_hash != start_hash:
                break
            time.sleep(self.BETWEEN_CLICK_DELAY)
            attempts += 1

    def type_to_pickup(self, text: str):
        _input = self.element_factory.get_input(locator=self.locators["REMARKS"])
        _input.type_text(text)

    def get_open_time(self):
        _time = self.element_factory.get_time_picker(locator=self.locators["OPEN_TIME"])

    def get_close_time(self):
        _time = self.element_factory.get_time_picker(locator=self.locators["CLOSE_TIME"])

    def type_to_address(self, addr1=None, addr2=None, city=None, zip=None):
        self.init_elements()
        if addr1 is not None:
            self._input_addr1.set_text_value(addr1)

        if addr2 is not None:
            _input_addr2 = self.element_factory.get_input(locator=self.locators["ADDRESS_LINE2"])
            _input_addr2.set_text_value(addr2)

        if city is not None:
            _input_city = self.element_factory.get_input(locator=self.locators["ADDRESS_CITY"])
            _input_city.set_text_value(city)

        if zip is not None:
            _input_zip = self.element_factory.get_input(locator=self.locators["ADDRESS_ZIP"])
            _input_zip.set_text_value(zip)

    def select_by_company(self, company_name: str):
        self.init_elements()
        self.contactbook_page.click_on_icon()
        self.contactbook_page.set_search_text(company_name)
        self.contactbook_page.select_first()

    def enter_company_name(self, company_name: str):
        self.page_vertical_scroll(self.locators["COMPANY_NAME"])
        input_company = self.element_factory.get_input(locator=self.locators["COMPANY_NAME"])
        input_company.set_text_value(text=company_name)

    def enter_contact_name(self, contact_name: str):
        input_company = self.element_factory.get_input(locator=self.locators["CONTACT_NAME"])
        input_company.set_text_value(text=contact_name)

    def enter_contact_phone(self, contact_phone: str):
        input_company = self.element_factory.get_input(locator=self.locators["CONTACT_PHONE"])
        input_company.set_text_value(text=contact_phone)

    def clear_controls(self):
        self.init_elements()
        self._input_company.clear()
        self._input_contact_name.clear()
        self._input_contact_phone.clear()
        self._input_contact_email.clear()
        self._input_remarks.clear()
        self._input_addr1.clear()
        self._input_addr2.clear()
        self._input_city.clear()
        self._input_zip.clear()

    def get_place_holder_value_of_field_company_name_in_rl_container(self) -> str:
        place_holder_value = self.get_attribute(locator=self.locators["COMPANY_NAME"], attribute_type="placeholder")
        return place_holder_value

    def get_place_holder_value_of_field_contact_name_in_rl_container(self) -> str:
        place_holder_value = self.get_attribute(locator=self.locators["CONTACT_NAME"], attribute_type="placeholder")
        return place_holder_value

    def get_place_holder_value_of_field_contact_phone_in_rl_container(self) -> str:
        place_holder_value = self.get_attribute(locator=self.locators["CONTACT_PHONE"], attribute_type="placeholder")
        return place_holder_value

    def get_place_holder_value_of_field_address_line1_in_rl_container(self) -> str:
        place_holder_value = self.get_attribute(locator=self.locators["ADDRESS_LINE1"], attribute_type="placeholder")
        return place_holder_value
