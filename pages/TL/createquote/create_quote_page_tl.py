from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from pages.common.createquote_page import CreateQuotePage

SERVICE_TYPE_FIELD = (By.ID, "createQuote_LTL1-279_select_3")
SERVICE_TYPE_DROP_LIST = (By.XPATH, "//div[@id='createQuote_LTL1-279_select_3'] //div[contains(@class,'-menu')] //div")
SERVICE_FIELD = (By.ID, "createQuote_TL-688_selectService")
SERVICE_FIELD_DROP_LIST = (By.CSS_SELECTOR, "#createQuote_TL-688_selectService div[id*='select']")
EQUIPMENT_TYPE_FIELD = (By.ID, "createQuote_TL-688_EquipmentTypeSection")
EQUIPMENT_TYPE_DROP_LIST = (By.CSS_SELECTOR, "#createQuote_TL-688_EquipmentTypeSection div[id*='select']")
EQUIPMENT_LENGTH_FIELD = (By.ID, "createQuote_TL-688_EquipmentSelect")
EQUIPMENT_LENGTH_DROP_LIST = (By.CSS_SELECTOR, "#createQuote_TL-688_EquipmentSelect div[id*='select']")
SHIPMENT_TYPE_FIELD = (By.ID, "createQuote_TL-688_ShipmentTypeSection")
SHIPMENT_TYPE_DROP_LIST = (By.CSS_SELECTOR, "#createQuote_TL-688_ShipmentTypeSection div[id*='select']")
MIN_TEMP_FIELD = (By.ID, "createQuote_TL-688_EquipmentTextField_5")
MAX_TEMP_FIELD = (By.ID, "createQuote_TL-688_EquipmentTextField_6")
LENGTH_FIELD = (By.ID, "createQuote_TL-688_EquipmentTextField_2")
WIDTH_FIELD = (By.ID, "createQuote_TL-688_EquipmentTextField_3")
HEIGHT_FIELD = (By.ID, "createQuote_TL-688_EquipmentTextField_4")
MIDDLE_SECTION_ENABLED_ELEMENTS_TL = (By.XPATH,
                                      "//*[@id='Fulfillment_TL-715_card' "
                                      "or @id='createQuote_LTL1-279_createQuoteContainer' "
                                      "or @id='TL-5858_financials_financialsContainer']")
COST_CURRENCY_RADIOBUTTON = (By.ID, "createQuote_LTL1-279_select_1")
CUSTOMER_RATES_RADIO = (By.CSS_SELECTOR, "label[for='createQuoteRadios-0']")
COST_ONLY_RADIO = (By.XPATH, "//input[@id='createQuote_LTL1-279_createQuoteRadios-1']/following-sibling::label")
CREATE_QUOTE_FOOTER_BUTTON = (By.ID, "footer_TL-1017_createQuoteButton")
CREATE_ORDER_FOOTER_BUTTON = (By.ID, "footer_TL-1191_createOrderButton")
BUILD_ORDER_FOOTER_BUTTON = (By.ID, "footer_TL_2054_buildOrderButton")
CUSTOMER_SEARCH_BOX = (By.ID, "createQuote_LTL1-279_select_2")
CARRIER_FULFILLMENT_CONTAINER = (By.ID, "Fulfillment_TL-715_card")
FINANCIALS_CONTAINER = (By.ID, "TL-5858_financials_financialsContainer")


class CreateQuotePageTL(CreateQuotePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def open_quote(self, quote_id):
        self.driver.get(f"{self.get_base_url()}/create-quote/tl/quote/{quote_id}")

    def check_if_create_quote_page_is_opened(self) -> bool:
        return self.is_element_displayed(locator=CUSTOMER_SEARCH_BOX)

    def select_equipment_type(self, equipment_type):
        self.scroll_into_view(self.get_element(EQUIPMENT_TYPE_FIELD))
        self.clear_equipment_type_field()

        dropdown = self.basic_types_factory.get_dropdown(locator=EQUIPMENT_TYPE_FIELD)
        dropdown.select_option_by_value(value=equipment_type)

    def select_equipment_length(self, equipment_length):

        dropdown = self.basic_types_factory.get_dropdown(locator=EQUIPMENT_LENGTH_FIELD)
        dropdown.select_option_by_value(value=equipment_length)

    def select_shipment_type(self, shipment_type):
        dropdown = self.basic_types_factory.get_dropdown(locator=SHIPMENT_TYPE_FIELD)
        dropdown.select_option_by_value(value=shipment_type)

    def get_selected_shipment_type(self) -> str:
        dropdown = self.basic_types_factory.get_dropdown(locator=SHIPMENT_TYPE_FIELD)
        return dropdown.get_selected_value()

    def get_selected_equipment_type(self) -> str:
        dropdown = self.basic_types_factory.get_dropdown(locator=EQUIPMENT_TYPE_FIELD)
        return dropdown.get_selected_value()

    def get_selected_equipment_length(self) -> str:
        dropdown = self.basic_types_factory.get_dropdown(locator=EQUIPMENT_LENGTH_FIELD)
        return dropdown.get_selected_value()

    def enter_min_temp(self, value):
        self.send_keys(value, locator=MIN_TEMP_FIELD)
        self.get_element(locator=MIN_TEMP_FIELD).send_keys(Keys.TAB)
        self.log.info(f'Entered Min Temp: {value}')

    def enter_max_temp(self, value):
        self.send_keys(value, locator=MAX_TEMP_FIELD)
        self.get_element(locator=MAX_TEMP_FIELD).send_keys(Keys.TAB)
        self.log.info(f'Entered Max Temp: {value}')

    def enter_length(self, value):
        self.send_keys(value, locator=LENGTH_FIELD)
        self.get_element(locator=LENGTH_FIELD).send_keys(Keys.TAB)
        self.log.info(f'Entered Length: {value}')

    def enter_width(self, value):
        self.send_keys(value, locator=WIDTH_FIELD)
        self.get_element(locator=WIDTH_FIELD).send_keys(Keys.TAB)
        self.log.info(f'Entered Width: {value}')

    def enter_height(self, value):
        self.send_keys(value, locator=HEIGHT_FIELD)
        self.get_element(locator=HEIGHT_FIELD).send_keys(Keys.TAB)
        self.log.info(f'Entered Width: {value}')

    def service_field_is_displayed(self):
        return self.is_element_present(locator=SERVICE_FIELD) and self.is_element_displayed(locator=SERVICE_FIELD)

    def cost_currency_field_is_displayed(self):
        return self.is_element_present(locator=COST_CURRENCY_RADIOBUTTON) and self.is_element_displayed(
            locator=COST_CURRENCY_RADIOBUTTON)

    def equipment_type_field_is_displayed(self):
        return self.is_element_present(locator=EQUIPMENT_TYPE_FIELD) and self.is_element_displayed(
            locator=EQUIPMENT_TYPE_FIELD)

    def verify_equipment_type_field_value(self, expected_value):
        actual_value = self.get_text(locator=EQUIPMENT_TYPE_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_shipment_type_field_value(self, expected_value):
        actual_value = self.get_text(locator=SHIPMENT_TYPE_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_equipment_length_field_value(self, expected_value):
        actual_value = self.get_text(locator=EQUIPMENT_LENGTH_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def equipment_length_field_is_displayed(self):
        return self.is_element_present(locator=EQUIPMENT_LENGTH_FIELD) and self.is_element_displayed(
            locator=EQUIPMENT_LENGTH_FIELD)

    def verify_length_field_is_displayed(self):
        return self.is_element_present(locator=LENGTH_FIELD) and self.is_element_displayed(locator=LENGTH_FIELD)

    def verify_width_field_is_displayed(self):
        return self.is_element_present(locator=WIDTH_FIELD) and self.is_element_displayed(locator=WIDTH_FIELD)

    def verify_height_field_is_displayed(self):
        return self.is_element_present(locator=HEIGHT_FIELD) and self.is_element_displayed(locator=HEIGHT_FIELD)

    def verify_min_temp_field_is_displayed(self):
        return self.is_element_present(locator=MIN_TEMP_FIELD) and self.is_element_displayed(locator=MIN_TEMP_FIELD)

    def verify_max_temp_field_is_displayed(self):
        is_displayed = self.is_element_present(locator=MAX_TEMP_FIELD) and self.is_element_displayed(
            locator=MAX_TEMP_FIELD)

        if not is_displayed:
            self.log.error("Max Temp Field is not displayed")
        return is_displayed

    def verify_min_temp_field_value(self, expected_value):
        actual_value = self.get_textbox_value(locator=MIN_TEMP_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_max_temp_field_value(self, expected_value):
        actual_value = self.get_textbox_value(locator=MAX_TEMP_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_length_value(self, expected_value):
        actual_value = self.get_textbox_value(locator=LENGTH_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_width_value(self, expected_value):
        actual_value = self.get_textbox_value(locator=WIDTH_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_height_value(self, expected_value):
        actual_value = self.get_textbox_value(locator=HEIGHT_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def shipment_type_field_is_displayed(self):
        return self.is_element_present(locator=SHIPMENT_TYPE_FIELD) and self.is_element_displayed(
            locator=SHIPMENT_TYPE_FIELD)

    def customer_rates_radiobutton_is_displayed(self):
        return self.is_element_present(locator=CUSTOMER_RATES_RADIO) and self.is_element_displayed(
            locator=CUSTOMER_RATES_RADIO)

    def cost_radiobutton_is_displayed(self):
        return self.is_element_present(locator=COST_ONLY_RADIO) and self.is_element_displayed(locator=COST_ONLY_RADIO)

    def verify_service_field_value(self, expected_value):
        actual_value = self.get_text(locator=SERVICE_FIELD)
        return self.validators.verify_text_match(actual_value, expected_value)

    def verify_cost_currency_field_value(self, expected_value):
        actual_cost_currency = self.get_text(locator=COST_CURRENCY_RADIOBUTTON)
        return self.validators.verify_text_match(actual_cost_currency, expected_value)

    def verify_service_field_dropdown_values(self, expected_values):
        self.element_click_by_locator(locator=SERVICE_FIELD)
        drop_down_list = self.get_element_list(locator=SERVICE_FIELD_DROP_LIST)
        actual_values = [value.text.lower() for value in drop_down_list]
        expected_values = [value.lower() for value in expected_values]

        return self.validators.verify_unordered_list_match(actual_values, expected_values)

    def get_equipment_type_dropdown_values(self):
        dropdown = self.basic_types_factory.get_dropdown(locator=EQUIPMENT_TYPE_FIELD)
        return dropdown.get_available_options_values()

    def verify_shipment_type_dropdown_values(self, expected_values):
        self.element_click_by_locator(locator=SHIPMENT_TYPE_FIELD)
        drop_down_list = self.get_element_list(locator=SHIPMENT_TYPE_DROP_LIST)
        actual_values = [value.text.lower() for value in drop_down_list]
        expected_values = [value.lower() for value in expected_values]

        return self.validators.verify_unordered_list_match(actual_values, expected_values)

    def verify_equipment_length_dropdown_values(self, expected_values):
        self.element_click_by_locator(locator=EQUIPMENT_LENGTH_FIELD)
        actual_values = [value.text.lower() for value in self.get_element_list(locator=EQUIPMENT_LENGTH_DROP_LIST)]
        self.element_click_by_locator(locator=EQUIPMENT_LENGTH_FIELD)  # closing dropdown
        expected_values = [value.lower() for value in expected_values]

        return self.validators.verify_unordered_list_match(actual_values, expected_values)

    def verify_quote_page_is_enabled_tl(self):
        self.log.debug("Inside verify_quote_page_is_enabled_tl Method")
        enabled_elements_present = self.get_element_list(locator=MIDDLE_SECTION_ENABLED_ELEMENTS_TL)
        if len(enabled_elements_present) == 3:
            self.log.info("verify_quote_page_is_enabled_tl method: Found elements from TL Create Quote")
            return True
        else:
            self.log.info("verify_quote_page_is_enabled_tl method: Element not present, hence quote page is disabled")
            return False

    def clear_equipment_type_field(self):
        try:
            actions = ActionChains(self.driver)
            actions.click(self.get_element(locator=EQUIPMENT_TYPE_FIELD))
            actions.key_down(Keys.CONTROL)
            actions.send_keys("a")
            actions.key_up(Keys.CONTROL)
            actions.send_keys(Keys.BACK_SPACE)
            actions.send_keys(Keys.ESCAPE)
            actions.perform()
        except Exception as E:
            self.log.error(f"Cannot clear Equipment type field\nException: {E}")

    def click_create_quote_footer_button(self):
        self.element_click_by_locator(locator=CREATE_QUOTE_FOOTER_BUTTON)

    def click_create_order_footer_button(self):
        self.element_click_by_locator(locator=CREATE_ORDER_FOOTER_BUTTON)

    def click_build_order_footer_button(self):
        self.element_click_by_locator(locator=BUILD_ORDER_FOOTER_BUTTON)

    def get_create_quote_footer_button_enabled_status(self) -> bool:
        is_enabled = self.get_element(locator=CREATE_QUOTE_FOOTER_BUTTON).get_attribute(name="data-disabled") == "false"
        return is_enabled
