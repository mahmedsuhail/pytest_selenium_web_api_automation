import time
from typing import Optional

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage
from pages.basictypes.specialinput import SpecialInput
# common locators
from pages.basictypes.time_picker import TimePicker

CONTACT_BOOK_WINDOW = (By.ID, "addressBook_LTL-1489_popup")
CONTACT_BOOK_EDIT_COMPANY_NAME = (By.NAME, "companyName")
CONTACT_BOOK_EDIT_CONTACT_NAME = (By.CSS_SELECTOR, "form [name='contactName']")
CONTACT_BOOK_EDIT_STREET1 = (By.NAME, "street1")
CONTACT_BOOK_EDIT_STREET2 = (By.NAME, "street2")
CONTACT_BOOK_EDIT_PHONE = (By.NAME, "phoneNumber")
CONTACT_BOOK_EDIT_EMAIL = (By.NAME, "email")
CONTACT_BOOK_EDIT_ZIP = (By.XPATH, "//label[contains(text(), 'ZIP')]/following-sibling::div")
CONTACT_BOOK_EDIT_STATE = (By.NAME, "state")
CONTACT_BOOK_EDIT_ADDRESS_ICON = (By.XPATH, ".//div[contains(@class, 'AddressEdit')]")
CONTACT_BOOK_EDIT_OPEN_TIME = (By.ID, "select_1")
CONTACT_BOOK_EDIT_CLOSE_TIME = (By.ID, "select_2")

CONTACT_BOOK_ADD_ADDRESS = (By.XPATH, "//div[text()='Add Address']")

CONTACT_BOOK_SAVE_BUTTON = (By.XPATH, ".//button/span[contains(text(),'Save')]")
CONTACT_BOOK_CANCEL_BUTTON = (By.XPATH, ".//button/span[contains(text(),'Cancel')]")
CONTACT_BOOK_CLOSE_BUTTON = (By.XPATH, ".//div[@data-testid='close-button']")

SPINNER = (By.XPATH, "//*[@data-testid='spinner-overlay']")

SAVING_BUTTON = (By.XPATH, "//button[contains(.,'Saving')]")


class ContactBookPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.locators = dict(
            CONTACT_BOOK_WINDOW=CONTACT_BOOK_WINDOW,
            CONTACT_BOOK_ADD_ADDRESS=CONTACT_BOOK_ADD_ADDRESS,
            CONTACT_BOOK_EDIT_COMPANY_NAME=CONTACT_BOOK_EDIT_COMPANY_NAME,
            CONTACT_BOOK_EDIT_CONTACT_NAME=CONTACT_BOOK_EDIT_CONTACT_NAME,
            CONTACT_BOOK_EDIT_STREET1=CONTACT_BOOK_EDIT_STREET1,
            CONTACT_BOOK_EDIT_STREET2=CONTACT_BOOK_EDIT_STREET2,
            CONTACT_BOOK_EDIT_PHONE=CONTACT_BOOK_EDIT_PHONE,
            CONTACT_BOOK_EDIT_EMAIL=CONTACT_BOOK_EDIT_EMAIL,
            CONTACT_BOOK_EDIT_ZIP=CONTACT_BOOK_EDIT_ZIP,
            CONTACT_BOOK_EDIT_STATE=CONTACT_BOOK_EDIT_STATE,
            CONTACT_BOOK_SAVE_BUTTON=CONTACT_BOOK_SAVE_BUTTON,
            CONTACT_BOOK_CANCEL_BUTTON=CONTACT_BOOK_CANCEL_BUTTON,
            CONTACT_BOOK_CLOSE_BUTTON=CONTACT_BOOK_CLOSE_BUTTON,
            CONTACT_BOOK_EDIT_ADDRESS_ICON=CONTACT_BOOK_EDIT_ADDRESS_ICON,
            CONTACT_BOOK_EDIT_OPEN_TIME=CONTACT_BOOK_EDIT_OPEN_TIME,
            CONTACT_BOOK_EDIT_CLOSE_TIME=CONTACT_BOOK_EDIT_CLOSE_TIME,
            SPINNER=SPINNER)
        self.contactbook_window = None

    def click_on_icon(self):
        self.element_click_by_locator(locator=self.locators["CONTACT_BOOK_ICON"])
        self.wait_for_element_visible(locator=self.locators["CONTACT_BOOK_WINDOW"])

    def set_search_text(self, text: str):
        search_input = SpecialInput(locator=self.locators["CONTACT_BOOK_INPUT"], driver=self.driver, log=self.log)
        search_input.set_text_value(text=text)
        self.__wait_for_spinner()

    def is_add_address_visible(self):
        return self.is_element_displayed(self.locators["CONTACT_BOOK_ADD_ADDRESS"])

    def select_first(self):
        attempts = 0
        while attempts <= self.NO_ATTEMPTS:
            try:
                element = self.wait_for_element_clickable(locator=self.locators["CONTACT_BOOK_FIRST_RESULT"])
                if element is not None:
                    element.click()
                    break
                time.sleep(self.BETWEEN_CLICK_DELAY)
                attempts += 1
            except Exception as E:
                time.sleep(self.BETWEEN_CLICK_DELAY)
                self.log.info(f"current time: {time.time()} attempt no: {attempts}\nException: {E}")
                attempts += 1

        self.wait_for_element_not_visible(locator=self.locators["CONTACT_BOOK_WINDOW"])

    def add_address_click(self):
        add_address = self.wait_for_element_clickable(locator=self.locators["CONTACT_BOOK_ADD_ADDRESS"])
        add_address.click()

    def save_button_click(self):
        if self.contactbook_window is None:
            self.contactbook_window = self.get_element(locator=self.locators["CONTACT_BOOK_WINDOW"])
        save = self.get_element(
            locator=self.locators["CONTACT_BOOK_SAVE_BUTTON"],
            parent=self.contactbook_window)
        save.click()

    def wait_until_saving_button_show_and_disappear(self):
        self.wait_for_element_visible(locator=SAVING_BUTTON, timeout=10)
        self.wait_for_element_not_exist(locator=SAVING_BUTTON, timeout=40)

    def cancel_button_click(self):
        if self.contactbook_window is None:
            self.contactbook_window = self.get_element(locator=self.locators["CONTACT_BOOK_WINDOW"])
        cancel = self.get_element(
            locator=self.locators["CONTACT_BOOK_CANCEL_BUTTON"],
            parent=self.contactbook_window)
        cancel.click()

    def is_add_address_form_visible(self):
        save_button = self.is_element_displayed(self.locators["CONTACT_BOOK_SAVE_BUTTON"])
        cancel_button = self.is_element_displayed(self.locators["CONTACT_BOOK_CANCEL_BUTTON"])

        return save_button and cancel_button

    def is_contact_found(self, search_text):
        repeat = self.NO_ATTEMPTS
        while repeat > 0:
            try:
                element = self.wait_for_element_clickable(locator=self.locators["CONTACT_BOOK_FIRST_RESULT"])
                value = element.text
                if search_text in value:
                    return True
                else:
                    repeat = repeat - 1
                    if repeat == 0:
                        return False
            except Exception as E:
                repeat = repeat - 1
                time.sleep(self.BETWEEN_CLICK_DELAY)
        return False

    def is_contact_not_found(self, search_text):
        elements = self.get_element_list(locator=self.locators["CONTACT_BOOK_FIRST_RESULT"])
        if len(elements) == 0 or search_text not in elements[0].text:
            return True
        return False

    def is_results_list_editable(self) -> bool:
        results = self.get_element_list(locator=self.locators["CONTACT_BOOK_RESULTS"])
        editable = True
        for result in results:
            edit_icon_element = self.get_element(locator=self.locators["CONTACT_BOOK_EDIT_ADDRESS_ICON"], parent=result)
            editable = editable and edit_icon_element is not None
        return editable

    def is_add_button_visible(self):
        element = self.wait_for_element_visible(locator=self.locators["CONTACT_BOOK_ADD_BUTTON"])
        return element is not None

    def is_company_name_required(self):
        return self._is_field_required(locator_name="CONTACT_BOOK_EDIT_COMPANY_NAME")

    def is_contact_name_required(self):
        return self._is_field_required(locator_name="CONTACT_BOOK_EDIT_CONTACT_NAME")

    def is_street_address_required(self):
        return self._is_field_required(locator_name="CONTACT_BOOK_EDIT_STREET1")

    def is_phone_number_required(self):
        return self._is_field_required(locator_name="CONTACT_BOOK_EDIT_PHONE")

    def is_zip_required(self):
        return "Required" in self.get_element(locator=self.locators["CONTACT_BOOK_EDIT_ZIP"]).text

    def _is_field_required(self, locator_name: str) -> bool:
        if self.contactbook_window is None:
            self.contactbook_window = self.get_element(locator=self.locators["CONTACT_BOOK_WINDOW"])
        return any(msg in
                   self.get_element(
                       locator=self.locators[locator_name],
                       parent=self.contactbook_window).get_attribute("placeholder")
                   for msg in ['Required', 'Not enough letters'])

    def fill_form(self,
                  company_name: str,
                  contact_name: str,
                  phone_number: str,
                  email: str,
                  street1: str,
                  street2: str,
                  zip_state_city: str):
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_COMPANY_NAME"], value=company_name)
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_CONTACT_NAME"], value=contact_name)
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_PHONE"], value=phone_number)
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_EMAIL"], value=email)
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_STREET1"], value=street1)
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_STREET2"], value=street2)
        self.__set_value(locator=self.locators["CONTACT_BOOK_EDIT_ZIP"], value=zip_state_city)

    def clear_form(self):
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_COMPANY_NAME"], press_escape=False)
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_CONTACT_NAME"], press_escape=False)
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_PHONE"], press_escape=False)
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_EMAIL"], press_escape=False)
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_STREET1"], press_escape=False)
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_STREET2"], press_escape=False)
        self.clear_by_select_all_and_delete(locator=self.locators["CONTACT_BOOK_EDIT_ZIP"], press_escape=False)

    def edit_first_result(self):
        element = self.wait_for_element_clickable(locator=self.locators["CONTACT_BOOK_FIRST_RESULT"])
        edit_button = self.get_element(locator=self.locators["CONTACT_BOOK_EDIT_ADDRESS_ICON"], parent=element)
        edit_button = self.get_element(locator=(By.TAG_NAME, 'svg'), parent=edit_button)
        edit_button.click()

    def close(self):
        if self.contactbook_window is None:
            self.contactbook_window = self.get_element(locator=self.locators["CONTACT_BOOK_WINDOW"])
        element = self.get_element(locator=self.locators["CONTACT_BOOK_CLOSE_BUTTON"], parent=self.contactbook_window)
        element.click()

    def get_visible_contact_book(self) -> Optional[WebElement]:
        elements = self.get_element_list(locator=self.locators["CONTACT_BOOK_WINDOW"])
        for element in elements:
            if element.is_displayed():
                return element
        return None

    def set_main_element(self, main_element: WebElement):
        self.contactbook_window = main_element

    def set_business_hours(self, open_time: str, close_time: str):
        open_time_picker = TimePicker(driver=self.driver, log=self.log,
                                      locator=self.locators["CONTACT_BOOK_EDIT_OPEN_TIME"])
        open_time_picker.set_time_value(open_time)
        close_time_picker = TimePicker(driver=self.driver, log=self.log,
                                       locator=self.locators["CONTACT_BOOK_EDIT_CLOSE_TIME"])
        close_time_picker.set_time_value(close_time)

    def __set_value(self, locator, value):
        if self.contactbook_window is None:
            self.contactbook_window = self.get_element(locator=self.locators["CONTACT_BOOK_WINDOW"])
        element = self.get_element(locator=locator, parent=self.contactbook_window)
        input = SpecialInput(driver=self.driver, log=self.log, element=element)
        if len(value) == 0:
            input.set_text_value(text="", select_option=False)
        else:
            input.set_text_value(value)

    def __wait_for_spinner(self):
        # due to unrealiability of spinner wait, add 2s delay
        # TODO: refactor spinner waits to be more reliable no matter where placed
        self.log.debug(f"__wait_for_spinner start {time.time()}")
        self.wait_for_element_visible(locator=self.locators["SPINNER"])
        self.wait_for_element_not_visible(locator=self.locators["SPINNER"])
        time.sleep(2)
        self.log.debug(f"__wait_for_spinner stop {time.time()}")
