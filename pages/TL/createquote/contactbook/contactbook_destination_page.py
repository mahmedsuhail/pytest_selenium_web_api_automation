from selenium.webdriver.common.by import By

from pages.TL.createquote.contactbook.contactbook_page import ContactBookPage

CONTACT_BOOK_ICON = (By.XPATH, "//*[@id='routeDetails_TL-595_inputField_2']/div/div[2]")

CONTACT_BOOK_INPUT = (By.ID, "addressBook_LTL-1489_inputField_destination")
CONTACT_BOOK_WINDOW = (By.ID, "addressBook_LTL-1489_popup")
CONTACT_BOOK_RESULTS = (By.ID, "addressBook_LTL-1489_sectionResults")
CONTACT_BOOK_FIRST_RESULT = \
    (By.XPATH,
     "//div[contains(.,'Address Book Destination')]//section[@id='addressBook_LTL-1489_sectionResults']/article[1]")


class ContactBookDestinationPage(ContactBookPage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        locators = dict(
            CONTACT_BOOK_ICON=CONTACT_BOOK_ICON,
            CONTACT_BOOK_WINDOW=CONTACT_BOOK_WINDOW,
            CONTACT_BOOK_INPUT=CONTACT_BOOK_INPUT,
            CONTACT_BOOK_RESULTS=CONTACT_BOOK_RESULTS,
            CONTACT_BOOK_FIRST_RESULT=CONTACT_BOOK_FIRST_RESULT
        )
        self.locators.update(locators)
