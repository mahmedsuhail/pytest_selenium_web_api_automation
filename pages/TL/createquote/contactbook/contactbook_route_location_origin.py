from selenium.webdriver.common.by import By

from pages.TL.createquote.contactbook.contactbook_page import ContactBookPage

CONTACT_BOOK_ICON = (By.ID, "locationOrigin_LTL1-601_addressBookIcon")
CONTACT_BOOK_WINDOW = (By.ID, "addressBook_LTL-1489_popup")
CONTACT_BOOK_INPUT = (By.ID, "addressBook_LTL-1489_inputField_origin")
CONTACT_BOOK_RESULTS = (By.ID, "addressBook_LTL-1489_sectionResults")
CONTACT_BOOK_FIRST_RESULT = \
    (By.XPATH,
     "//div[contains(.,'Address Book Origin')]//section[@id='addressBook_LTL-1489_sectionResults']/article[1]")
ORIGIN_INPUT = (By.ID, "routeLocations_TL-847_location_origin_textField_1")
SPINNER = (By.XPATH, "//*[@data-testid='spinner-overlay']")


class ContactBookRouteLocationOriginPage(ContactBookPage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.locators = dict(
            CONTACT_BOOK_ICON=CONTACT_BOOK_ICON,
            CONTACT_BOOK_WINDOW=CONTACT_BOOK_WINDOW,
            CONTACT_BOOK_INPUT=CONTACT_BOOK_INPUT,
            CONTACT_BOOK_RESULTS=CONTACT_BOOK_RESULTS,
            CONTACT_BOOK_FIRST_RESULT=CONTACT_BOOK_FIRST_RESULT,
            SPINNER=SPINNER)
