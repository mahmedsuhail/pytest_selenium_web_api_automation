from typing import List

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage
from pages.basictypes.modal import Modal
from pages.common.models.shipping_item import ShippingItem

SHIPPING_ITEMS_CONTAINER = (By.ID, "shippingItems_TL-682_shippingItemsContainer")
PRODUCT_BOOK_ICON = (By.XPATH, "//div[./input[@id='itemEditor_TL-682_inputField_1']]/div")
COMMODITY_DESCRIPTION = (By.ID, "itemEditor_TL-682_inputField_1")
PIECE_COUNT = (By.ID, "itemEditor_TL-682_inputField_2")
WEIGHT = (By.ID, "itemEditor_TL-682_inputField_3")
WEIGHT_UNITS_DROPDOWN = (By.ID, "itemEditor_TL-682_select_1")
PALLET_COUNT = (By.ID, "itemEditor_TL-682_inputField_5")
LENGTH = (By.ID, "itemEditor_TL-682_inputField_6")
WIDTH = (By.ID, "itemEditor_TL-682_inputField_7")
HEIGHT = (By.ID, "itemEditor_TL-682_inputField_8")
DIM_UNITS_DROPDOWN = (By.ID, "itemEditor_TL-682_select_4")
CLEAR_BUTTON = (By.ID, "shippingItems_TL-682_button_1")
ADD_TO_ORDER_BUTTON = (By.ID, "shippingItems_TL-682_button_2")
STACKABLE_CHECKBOX = (By.XPATH, "//label[input[@name='Stackable']]")
HAZMAT_CHECKBOX = (By.XPATH, "//label[input[@name='Hazmat']]")
HAZMAT_CODE = (By.ID, "itemEditor_TL-682_inputField_9")
HAZMAT_CHEMICAL_NAME = (By.ID, "itemEditor_TL-682_inputField_10")
HAZMAT_EMERGENCY_CONTACT = (By.ID, "itemEditor_TL-682_inputField_11")
HAZMAT_GROUP_DROPDOWN = (By.ID, "itemEditor_TL-682_select_5")
HAZMAT_CLASS_DROPDOWN = (By.ID, "itemEditor_TL-682_select_6")
HAZMAT_PREFIX_DROPDOWN = (By.ID, "itemEditor_TL-682_select_7")
CONTAINER_HEADER_MAIN = (By.XPATH, "//div[contains(@class, 'Header') and ./div[text()='Shipping Items']]")


class CreateQuoteShippingItemsPage(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def open_product_book_popup(self):
        self.element_click_by_locator(PRODUCT_BOOK_ICON, False)
        return CreateQuoteShippingItemsProductBookPopupPage("Product Book", self.driver, self.log)

    def clear_shipping_item_data(self, shipping_item: ShippingItem):
        try:
            if shipping_item.commodity_description:
                self.clear_by_select_all_and_delete(locator=COMMODITY_DESCRIPTION)
            if shipping_item.piece_count:
                self.clear_by_select_all_and_delete(locator=PIECE_COUNT)
            if shipping_item.weight:
                self.clear_by_select_all_and_delete(locator=WEIGHT)
            if shipping_item.weight_units:
                weight_units_dropdown = self.basic_types_factory.get_dropdown(WEIGHT_UNITS_DROPDOWN)
                weight_units_dropdown.clear_filter()
            if shipping_item.pallet_count:
                self.clear_by_select_all_and_delete(locator=PALLET_COUNT)
            if shipping_item.length:
                self.clear_by_select_all_and_delete(locator=LENGTH)
            if shipping_item.width:
                self.clear_by_select_all_and_delete(locator=WIDTH)
            if shipping_item.height:
                self.clear_by_select_all_and_delete(locator=HEIGHT)
            if shipping_item.dim_units:
                weight_units_dropdown = self.basic_types_factory.get_dropdown(DIM_UNITS_DROPDOWN)
                weight_units_dropdown.clear_filter()
            if shipping_item.is_stackable:
                is_stackable = self.basic_types_factory.get_checkbox("Stackable", STACKABLE_CHECKBOX)
                is_stackable.uncheck()
            if shipping_item.is_hazmat:
                is_hazmat = self.basic_types_factory.get_checkbox("Hazmat", HAZMAT_CHECKBOX)
                is_hazmat.uncheck()
            if shipping_item.hazmat_code:
                self.clear_by_select_all_and_delete(locator=HAZMAT_CODE)
            if shipping_item.hazmat_chemical_name:
                self.clear_by_select_all_and_delete(locator=HAZMAT_CHEMICAL_NAME)
            if shipping_item.hazmat_emergency_contact:
                self.clear_by_select_all_and_delete(locator=HAZMAT_EMERGENCY_CONTACT)
            if shipping_item.hazmat_group:
                hazmat_group_dropdown = self.basic_types_factory.get_dropdown(locator=HAZMAT_GROUP_DROPDOWN)
                hazmat_group_dropdown.clear_filter()
            if shipping_item.hazmat_class:
                hazmat_class_dropdown = self.basic_types_factory.get_dropdown(locator=HAZMAT_CLASS_DROPDOWN)
                hazmat_class_dropdown.clear_filter()
            if shipping_item.hazmat_prefix:
                hazmat_prefix_dropdown = self.basic_types_factory.get_dropdown(locator=HAZMAT_PREFIX_DROPDOWN)
                hazmat_prefix_dropdown.clear_filter()

            self.log.info(
                f"Entered shipping item: {shipping_item}")

        except Exception as E:
            self.log.error("Method enter_shipping_item_data : ShippingItems entry in the respective fields got failed.")
            self.log.error("Exception : " + str(E))

    def enter_shipping_item_data(self, shipping_item: ShippingItem, clear=False):
        try:
            if shipping_item.commodity_description:
                self.send_keys(data=shipping_item.commodity_description,
                               locator=COMMODITY_DESCRIPTION)
            if shipping_item.piece_count:
                self.send_keys(data=shipping_item.piece_count, locator=PIECE_COUNT, clear=clear, scroll_to=False)
            if shipping_item.weight:
                self.send_keys(data=shipping_item.weight, locator=WEIGHT, clear=clear, scroll_to=False)
            if shipping_item.weight_units:
                weight_units_dropdown = self.basic_types_factory.get_dropdown(WEIGHT_UNITS_DROPDOWN)
                weight_units_dropdown.select_option_by_value(shipping_item.weight_units)
            if shipping_item.pallet_count:
                self.send_keys(data=shipping_item.pallet_count, locator=PALLET_COUNT, clear=clear, scroll_to=False)
            if shipping_item.length:
                self.send_keys(data=shipping_item.length, locator=LENGTH, clear=clear, scroll_to=False)
            if shipping_item.width:
                self.send_keys(data=shipping_item.width, locator=WIDTH, clear=clear, scroll_to=False)
            if shipping_item.height:
                self.send_keys(data=shipping_item.height, locator=HEIGHT, clear=clear, scroll_to=False)
            if shipping_item.dim_units:
                weight_units_dropdown = self.basic_types_factory.get_dropdown(DIM_UNITS_DROPDOWN)
                weight_units_dropdown.select_option_by_value(shipping_item.dim_units)

            if shipping_item.is_stackable:
                is_stackable = self.basic_types_factory.get_checkbox("Stackable", STACKABLE_CHECKBOX)
                is_stackable.check()

            if shipping_item.is_hazmat:
                is_hazmat = self.basic_types_factory.get_checkbox("Hazmat", HAZMAT_CHECKBOX)
                is_hazmat.check()

            if shipping_item.hazmat_code:
                self.send_keys(data=shipping_item.hazmat_code, locator=HAZMAT_CODE)

            if shipping_item.hazmat_chemical_name:
                self.send_keys(data=shipping_item.hazmat_chemical_name, locator=HAZMAT_CHEMICAL_NAME, clear=False,
                               scroll_to=False)

            if shipping_item.hazmat_emergency_contact:
                self.send_keys(data=shipping_item.hazmat_emergency_contact,
                               locator=HAZMAT_EMERGENCY_CONTACT, clear=False, scroll_to=False)

            if shipping_item.hazmat_group:
                hazmat_group_dropdown = self.basic_types_factory.get_dropdown(locator=HAZMAT_GROUP_DROPDOWN)
                hazmat_group_dropdown.select_option_by_value(shipping_item.hazmat_group)

            if shipping_item.hazmat_class:
                hazmat_class_dropdown = self.basic_types_factory.get_dropdown(locator=HAZMAT_CLASS_DROPDOWN)
                hazmat_class_dropdown.select_option_by_value(shipping_item.hazmat_class)

            if shipping_item.hazmat_prefix:
                hazmat_prefix_dropdown = self.basic_types_factory.get_dropdown(locator=HAZMAT_PREFIX_DROPDOWN)
                hazmat_prefix_dropdown.select_option_by_value(shipping_item.hazmat_prefix)

            self.log.info(
                f"Entered shipping item: {shipping_item}")

        except Exception as E:
            self.log.error("Method enter_shipping_item_data : ShippingItems entry in the respective fields got failed.")
            self.log.error("Exception : " + str(E))

    def get_shipping_item_from_page(self):
        shipping_item = ShippingItem()
        shipping_item.commodity_description = self.get_input_value(locator=COMMODITY_DESCRIPTION)
        shipping_item.piece_count = self.get_input_value(locator=PIECE_COUNT)
        shipping_item.weight = self.get_input_value(locator=WEIGHT)
        shipping_item.weight_units = self.basic_types_factory.get_dropdown(WEIGHT_UNITS_DROPDOWN).get_selected_value()
        shipping_item.pallet_count = self.get_input_value(locator=PALLET_COUNT)
        shipping_item.length = self.get_input_value(locator=LENGTH)
        shipping_item.width = self.get_input_value(locator=WIDTH)
        shipping_item.height = self.get_input_value(locator=HEIGHT)
        shipping_item.dim_units = self.basic_types_factory.get_dropdown(DIM_UNITS_DROPDOWN).get_selected_value()
        shipping_item.is_stackable = self.basic_types_factory.get_checkbox("Stackable", STACKABLE_CHECKBOX).is_checked()
        shipping_item.is_hazmat = self.basic_types_factory.get_checkbox("Hazmat", HAZMAT_CHECKBOX).is_checked()
        if shipping_item.is_hazmat:
            shipping_item.hazmat_group = self.basic_types_factory.get_dropdown(
                HAZMAT_GROUP_DROPDOWN).get_selected_value()
            shipping_item.hazmat_class = self.basic_types_factory.get_dropdown(
                HAZMAT_CLASS_DROPDOWN).get_selected_value()
            shipping_item.hazmat_prefix = self.basic_types_factory.get_dropdown(
                HAZMAT_PREFIX_DROPDOWN).get_selected_value()

            shipping_item.hazmat_code = self.get_input_value(locator=HAZMAT_CODE)
            shipping_item.hazmat_chemical_name = self.get_input_value(locator=HAZMAT_CHEMICAL_NAME)
            shipping_item.hazmat_emergency_contact = self.get_input_value(
                locator=HAZMAT_EMERGENCY_CONTACT)

        self.log.info(f"get_shipping_item_from_page:{shipping_item}")

        return shipping_item

    def get_shipping_item_placeholders_from_page(self):
        shipping_item = ShippingItem()
        shipping_item.commodity_description = self.get_attribute(locator=COMMODITY_DESCRIPTION,
                                                                 attribute_type="placeholder")
        shipping_item.piece_count = self.get_attribute(locator=PIECE_COUNT,
                                                       attribute_type="placeholder")
        shipping_item.weight = self.get_attribute(locator=WEIGHT, attribute_type="placeholder")
        shipping_item.weight_units = self.basic_types_factory.get_dropdown(WEIGHT_UNITS_DROPDOWN).get_placeholder()
        shipping_item.pallet_count = self.get_attribute(locator=PALLET_COUNT,
                                                        attribute_type="placeholder")
        shipping_item.length = self.get_attribute(locator=LENGTH, attribute_type="placeholder")
        shipping_item.width = self.get_attribute(locator=WIDTH, attribute_type="placeholder")
        shipping_item.height = self.get_attribute(locator=HEIGHT, attribute_type="placeholder")
        shipping_item.dim_units = self.basic_types_factory.get_dropdown(DIM_UNITS_DROPDOWN).get_placeholder()
        shipping_item.is_stackable = self.basic_types_factory.get_checkbox("Stackable", STACKABLE_CHECKBOX).is_checked()
        shipping_item.is_hazmat = self.basic_types_factory.get_checkbox("Hazmat", HAZMAT_CHECKBOX).is_checked()
        if shipping_item.is_hazmat:
            shipping_item.hazmat_group = self.basic_types_factory.get_dropdown(HAZMAT_GROUP_DROPDOWN).get_placeholder()
            shipping_item.hazmat_class = self.basic_types_factory.get_dropdown(HAZMAT_CLASS_DROPDOWN).get_placeholder()
            shipping_item.hazmat_prefix = self.basic_types_factory.get_dropdown(HAZMAT_PREFIX_DROPDOWN) \
                .get_placeholder()

            shipping_item.hazmat_code = self.get_attribute(locator=HAZMAT_CODE,
                                                           attribute_type="placeholder")
            shipping_item.hazmat_chemical_name = self.get_attribute(locator=HAZMAT_CHEMICAL_NAME,
                                                                    attribute_type="placeholder")
            shipping_item.hazmat_emergency_contact = self.get_attribute(
                locator=HAZMAT_EMERGENCY_CONTACT, attribute_type="placeholder")
        return shipping_item

    def get_container_header_color(self):
        element = self.get_element(CONTAINER_HEADER_MAIN)
        return element.value_of_css_property(property_name="background-color")

    def click_cancel_button(self):
        self.element_click_by_locator(locator=CLEAR_BUTTON)

    def click_clear_button(self):
        self.element_click_by_locator(locator=CLEAR_BUTTON)

    def click_add_to_order_button(self):
        self.page_vertical_scroll(locator=ADD_TO_ORDER_BUTTON)
        self.element_click_by_locator(locator=ADD_TO_ORDER_BUTTON)

    def is_add_to_order_button_visible(self):
        return self.is_element_displayed(locator=ADD_TO_ORDER_BUTTON)

    def is_page_displayed(self) -> bool:
        return self.is_element_displayed(locator=SHIPPING_ITEMS_CONTAINER)

    def get_hazmat_checkbox_checked_status(self) -> bool:
        return self.basic_types_factory.get_checkbox("Stackable", HAZMAT_CHECKBOX).is_checked()

    def get_stackable_checkbox_checked_status(self) -> bool:
        return self.basic_types_factory.get_checkbox("Hazmat", STACKABLE_CHECKBOX).is_checked()


class CreateQuoteShippingItemsProductBookPopupPage(Modal):
    SEARCH_COMMODITY = (By.ID, "productBook_TL-683_inputField_1")
    COMMODITIES_LIST = (By.CSS_SELECTOR, "div[class*='CommoditiesList']")
    COMMODITY_ITEM = (By.CSS_SELECTOR, "div[class*='CommodityItem']")
    COMMODITY_ITEM_SPINNER = (By.XPATH, "//div[contains(@id,'commodityItem') and text()='...']")

    def __init__(self, name, driver, log):
        super().__init__(name, driver, log)

    def get_search_commodity_input_placeholder(self):
        return self.driver_operations.get_element(self.SEARCH_COMMODITY).get_attribute("placeholder")

    def get_available_commodity_items(self) -> List[WebElement]:
        return self.driver_operations.get_element_list(self.COMMODITY_ITEM)

    def select_item_from_list(self, list: List[WebElement], name):
        self.driver_operations.element_click(next(x for x in list if x.text == name))
        self.driver_operations.wait_for_element(self.COMMODITIES_LIST, "notdisplay")

    def get_available_commodity_items_names(self):
        tmp_items = []
        items = self.get_available_commodity_items()

        for item in items:
            tmp_items.append(item.text)

        return tmp_items

    def search_commodity_input_visibility(self):
        return self.driver_operations.get_element(self.SEARCH_COMMODITY).is_displayed()

    def search_for_commodity(self, value):
        self.driver_operations.send_keys(locator=self.SEARCH_COMMODITY, data=value)
        self.driver_operations.wait_for_element(locator=self.COMMODITY_ITEM_SPINNER, event="display", timeout=2)
        self.driver_operations.wait_for_element(locator=self.COMMODITY_ITEM_SPINNER, event="notdisplay", timeout=15)

    def get_amount_of_visible_commodities(self):
        commodities_list = self.driver_operations.get_element(self.COMMODITIES_LIST)
        max_visible_height = int(commodities_list.location['y']) + int(commodities_list.size['height'])

        items = self.get_available_commodity_items()
        return len(list(x for x in items if x.location['y'] < max_visible_height))

    def is_commodity_items_list_scrollable(self):
        list_element_size = self.driver_operations.get_element(self.COMMODITIES_LIST).size['height']
        elements_list = self.driver_operations.get_element_list(self.COMMODITY_ITEM)
        if len(elements_list) < 1:
            return False
        one_element_size = elements_list[0].size['height']
        return list_element_size / one_element_size > 8
