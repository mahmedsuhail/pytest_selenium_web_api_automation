from selenium.webdriver.common.by import By

from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basepage import BasePage
from pages.TL.createquote.contactbook.contactbook_origin_page import ContactBookOriginPage

ROUTE_STOP_ROWS = (By.XPATH, "//div[@id='TL-2328_routeDetailsModal']/div/div[1]/div")
ADD_STOP_BUTTON = (By.XPATH, "//div[@id='TL-2328_routeDetailsModal']/div/div[2]//node()[contains(text(), 'Add Stop')]")
STOP_TYPE_XPATH_TEXT = "//div[@id='TL-2328_routeDetailsModal']/div/div[2]//*[text()='{{STOP_TYPE}}']"

STOP_ROW_CONTACTBOOK_ICON = (By.XPATH, ".//div[2]/div/div/div[2]/div[@data-testid='action-icon']")


class TLRouteDetailsPopupPage(TLRouteDetailsPage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.rows = None

    def open_stop_address_book(self, index) -> ContactBookOriginPage:
        if self.rows is None:
            self.__create_row_list()
        return self.rows[index].click_on_address_book_icon()

    def add_stop(self, type: str = 'Pick Up'):
        """ adds stop to the route
        Args:
        type -- stop type, possible values: 'Pick Up', 'Drop Off', 'Pick Up & Drop Off'
        """
        button = self.wait_for_element_clickable(locator=ADD_STOP_BUTTON)
        button.click()
        stop_type = self.wait_for_element_clickable(
            locator=(By.XPATH, STOP_TYPE_XPATH_TEXT.replace('{{STOP_TYPE}}', type)))
        stop_type.click()
        self.__create_row_list()

    def __create_row_list(self):
        elements = self.get_element_list(locator=ROUTE_STOP_ROWS)
        self.rows = [RouteDetailsStopRow(self.driver, self.log, x) for x in elements]


class RouteDetailsStopRow(BasePage):

    def __init__(self, driver, log, element):
        super().__init__(driver, log)
        self.row_element = element

    def click_on_address_book_icon(self) -> ContactBookOriginPage:
        button = self.get_element(locator=STOP_ROW_CONTACTBOOK_ICON, parent=self.row_element)
        button.click()
        contact_book = ContactBookOriginPage(self.driver, self.log)
        visible_contact_book = contact_book.get_visible_contact_book()
        contact_book.set_main_element(visible_contact_book)

        return contact_book
