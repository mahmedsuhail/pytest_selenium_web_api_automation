from selenium.webdriver.common.by import By

from pages.TL.createquote.contactbook.contactbook_destination_page import ContactBookDestinationPage
from pages.TL.createquote.routelocations_page import RouteLocationsPage

RL_DESTINATION_COMPANY = (By.ID, "routeLocations_TL-847_location_destination_textField_1")
RL_DESTINATION_CONTACT_NAME = (By.ID, "routeLocations_TL-847_location_destination_textField_2")
RL_DESTINATION_CONTACT_PHONE = (By.ID, "routeLocations_TL-847_location_destination_textField_3")
RL_DESTINATION_CONTACT_EMAIL = (By.ID, "routeLocations_TL-847_location_destination_textField_4")
RL_DESTINATION_PICKUP_REMARKS = (By.ID, "routeLocations_TL-847_location_destination_textField_9")
RL_DESTINATION_ADDR_L1 = (By.ID, "routeLocations_TL-847_location_destination_textField_5")
RL_DESTINATION_ADDR_L2 = (By.ID, "routeLocations_TL-847_location_destination_textField_6")
RL_DESTINATION_CITY = (By.ID, "routeLocations_TL-847_location_destination_textField_7")
RL_DESTINATION_ZIP = (By.ID, "routeLocations_TL-847_location_destination_textField_8")
RL_DESTINATION_OPEN_TIME = (By.ID, "routeLocations_TL-847_location_destination_openTime")
RL_DESTINATION_CLOSE_TIME = (By.ID, "routeLocations_TL-847_location_destination_closeTime")


class RouteLocationsDestinationPage(RouteLocationsPage):

    def __init__(self, driver, log):
        locators = {
            "COMPANY_NAME": RL_DESTINATION_COMPANY,
            "CONTACT_NAME": RL_DESTINATION_CONTACT_NAME,
            "CONTACT_PHONE": RL_DESTINATION_CONTACT_PHONE,
            "CONTACT_EMAIL": RL_DESTINATION_CONTACT_EMAIL,
            "REMARKS": RL_DESTINATION_PICKUP_REMARKS,
            "ADDRESS_LINE1": RL_DESTINATION_ADDR_L1,
            "ADDRESS_LINE2": RL_DESTINATION_ADDR_L2,
            "ADDRESS_CITY": RL_DESTINATION_CITY,
            "ADDRESS_ZIP": RL_DESTINATION_ZIP,
            "OPEN_TIME": RL_DESTINATION_OPEN_TIME,
            "CLOSE_TIME": RL_DESTINATION_CLOSE_TIME
        }
        super().__init__(
            driver=driver,
            log=log,
            locators=locators,
            contactbook_page=ContactBookDestinationPage(driver, log))
