from selenium.webdriver.common.by import By

from pages.basepage import BasePage
from pages.common.builders.note_builder import NoteBuilder
from pages.common.models.note import Note

NOTE_TYPE_DROPDOWN = (By.ID, "notes_TL-710_container_select")
NOTE_TEXT_FIELD = (By.ID, "notes_TL-710_container_textarea")
NOTES_CONTAINER = (By.ID, "notes_TL-710_container_notesContainer")
CLEAR_BUTTON = (By.ID, "notes_TL-710_container_cancelButton")
SAVE_BUTTON = (By.ID, "notes_TL-710_container_saveButton")
SELECT_NOTE_TYPE_ERROR_MSG = (By.XPATH, "//div[ contains(text(),'Please select a note type')]")
ENTER_NOTE_TEXT_ERROR_MSG = (By.XPATH, "//div[ contains(text(),'Please enter note text')]")
ENTER_NOTE_TEXT_AND_TYPE_ERROR_MSG = (By.XPATH,
                                      "//div[ contains(text(),'Please select a note type and enter some text')]")
ADDED_NOTES_CONTAINER = (By.ID, "notes_TL-710_container_notesContainer")
ADDED_NOTE_BASE_ID = (By.ID, "notes_TL-710_container_item")


class TlNotesPage(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_notes_section_visibility(self) -> bool:
        return self.is_element_displayed(locator=NOTE_TEXT_FIELD)

    def get_save_button_visibility(self) -> bool:
        return self.is_element_displayed(locator=SAVE_BUTTON)

    def get_cancel_button_visibility(self) -> bool:
        return self.is_element_displayed(locator=CLEAR_BUTTON)

    def get_note_type_dropdown_value(self) -> str:
        type_dropdown = self.basic_types_factory.get_dropdown(NOTE_TYPE_DROPDOWN)
        return type_dropdown.get_selected_value()

    def get_note_type_dropdown_available_values(self) -> [str]:
        type_dropdown = self.basic_types_factory.get_dropdown(NOTE_TYPE_DROPDOWN)
        return type_dropdown.get_available_options_values()

    def select_note_type(self, value):
        type_dropdown = self.basic_types_factory.get_dropdown(NOTE_TYPE_DROPDOWN)
        return type_dropdown.select_option_by_value(value=value)

    def enter_note_text(self, text):
        self.send_keys(data=text, locator=NOTE_TEXT_FIELD)

    def get_note_textfield_text(self) -> str:
        return self.get_text(locator=NOTE_TEXT_FIELD)

    def click_save_button(self):
        return self.element_click_by_locator(locator=SAVE_BUTTON)

    def click_clear_button(self):
        return self.element_click_by_locator(locator=CLEAR_BUTTON)

    def select_note_type_error_message_is_displayed(self):
        return self.is_element_displayed(locator=SELECT_NOTE_TYPE_ERROR_MSG)

    def enter_note_text_error_message_is_displayed(self):
        return self.is_element_displayed(locator=ENTER_NOTE_TEXT_ERROR_MSG)

    def enter_note_type_and_text_error_message_is_displayed(self):
        return self.is_element_displayed(locator=ENTER_NOTE_TEXT_AND_TYPE_ERROR_MSG)

    def get_added_notes_count(self) -> int:
        added_notes_container = self.get_element(locator=ADDED_NOTES_CONTAINER)
        if added_notes_container is None:
            return 0
        else:
            return len(added_notes_container.find_elements_by_css_selector(f"[id*={ADDED_NOTE_BASE_ID[1]}]"))

    def get_all_notes(self):
        added_notes_container = self.get_element(locator=ADDED_NOTES_CONTAINER)
        note_elements = added_notes_container.find_elements_by_css_selector(f"[id*={ADDED_NOTE_BASE_ID[1]}]")
        notes = []

        for note in note_elements:
            note_data = note.text.splitlines()
            note = NoteBuilder().with_note_type(note_data[0]) \
                .with_username(note_data[1]) \
                .with_note_text(note_data[2]) \
                .with_timestamp(note_data[3]) \
                .build()

            notes.append(note)

        return notes

    def get_added_note_by_text(self, note_text) -> Note:
        notes = self.get_all_notes()

        for note in notes:
            if note.note_text == note_text:
                return note

    def add_note(self, note):
        self.select_note_type(note.note_type)
        self.enter_note_text(note.note_text)
        self.click_save_button()
