from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage
from pages.basictypes.dropdown import Dropdown

ADD_CHARGE_BUTTON = (By.ID, "TL-5858_financials_add_charge")
CANCEL_BUTTON = (By.ID, "TL-5858_financials_cancel")
CONTAINER_ENABLE_BUTTON = (By.ID, "TL-5858_financials_edit")
CHARGE_TABLE_ROW = (By.XPATH, "//div[contains(@id, 'TL-5858_financials_charge_select_description')]")
CHARGE_TABLE_HEADER_DESCRIPTION = (
    By.XPATH, "//div[contains(text(), 'Additional Charges')]/following-sibling::label[text()='Description']")
CHARGE_TABLE_HEADER_COST = (
    By.XPATH, "//div[contains(text(), 'Additional Charges')]/following-sibling::label[text()='Cost']")
CHARGE_TABLE_HEADER_REVENUE = (
    By.XPATH, "//div[contains(text(), 'Additional Charges')]/following-sibling::label[text()='Revenue']")
CONTAINER_HEADER_BACKGROUND = (
    By.XPATH, "//div[div[text()='Financials']]")

ACTUAL_COST_VALUE = (By.ID, "TL-5858_financials_actual_cost")
REVENUE_VALUE = (By.ID, "TL-5858_financials_revenue")
MARGIN_VALUE = (By.ID, "TL-5858_financials_margin")
MARGIN_PERCENTAGE_VALUE = (By.ID, "TL-5858_financials_target_margin")

COST_INPUT = (By.ID, "TL-5858_financials_change_cost_0")
REVENUE_INPUT = (By.ID, "TL-5858_financials_change_revenue_0")
MAX_BUY_INPUT = (By.ID, "TL-5858_financials_max_buy")
TARGET_COST_INPUT = (By.ID, "TL-5858_financials_target_cost")
PRICE_TO_BEAT_INPUT = (By.ID, "TL-5858_financials_price_to_beat")
SAVE_BUTTON = (By.ID, "TL-5858_financials_save")

CHARGE_ROW_DESCRIPTION_ID = "TL-5858_financials_charge_select_description_"
CHARGE_ROW_COST_ID = "TL-5858_financials_change_cost_"
CHARGE_ROW_RATE_ID = "TL-5858_financials_change_rate_"
CHARGE_ROW_REVENUE_ID = "TL-5858_financials_change_revenue_"
CHARGE_ROW_REMOVE_BUTTON_ID = "TL-5858_financials_remove_charge_"


class FinancialsPage(BasePage):
    def __init__(self, driver, log):
        super().__init__(driver, log)

    def enable(self):
        self.element_click_by_locator(locator=CONTAINER_ENABLE_BUTTON)

    def click_add_charge(self):
        self.element_click_by_locator(locator=ADD_CHARGE_BUTTON)

    def click_close(self):
        self.element_click_by_locator(locator=CANCEL_BUTTON)

    def get_actual_cost(self):
        return self.get_text(locator=ACTUAL_COST_VALUE)

    def get_revenue(self):
        return self.get_text(locator=REVENUE_VALUE)

    def get_margin(self):
        return self.get_text(locator=MARGIN_VALUE)

    def get_margin_percentage(self):
        return self.get_text(locator=MARGIN_PERCENTAGE_VALUE)

    def get_max_buy(self):
        return self.get_input_value(locator=MAX_BUY_INPUT)

    def get_charge_table_rows(self):
        elements = self.get_element_list(locator=CHARGE_TABLE_ROW)
        wrapped_rows = []
        index = 0
        for x in elements:
            wrapped_rows.append(
                ChargeRow(driver=self.driver, base_row_element=x, log=self.log, index=index))
            index += 1
        return wrapped_rows

    def get_container_color(self):
        return self.get_element(CONTAINER_HEADER_BACKGROUND).value_of_css_property("background-color")

    def get_price_to_beat(self):
        return self.get_input_value(locator=PRICE_TO_BEAT_INPUT)

    def get_target_cost(self):
        return self.get_input_value(locator=TARGET_COST_INPUT)

    def is_add_charge_button_visible(self):
        return self.is_element_displayed(ADD_CHARGE_BUTTON)

    def is_charge_header_description_visible(self):
        return self.is_element_displayed(CHARGE_TABLE_HEADER_DESCRIPTION)

    def is_charge_header_cost_visible(self):
        return self.is_element_displayed(CHARGE_TABLE_HEADER_COST)

    def is_charge_header_revenue_visible(self):
        return self.is_element_displayed(CHARGE_TABLE_HEADER_REVENUE)

    def is_save_button_visible(self):
        return self.is_element_displayed(SAVE_BUTTON)

    def wait_for_add_charge_button_visible(self):
        self.wait_for_element_visible(locator=ADD_CHARGE_BUTTON)

    def set_cost(self, value):
        self.send_keys(locator=COST_INPUT, data=value)

    def set_revenue(self, value):
        self.send_keys(locator=REVENUE_INPUT, data=value)

    def set_max_buy(self, value):
        self.send_keys(locator=MAX_BUY_INPUT, data=value)

    def set_target_cost(self, value):
        self.send_keys(locator=TARGET_COST_INPUT, data=value)

    def set_price_to_beat(self, value):
        self.send_keys(locator=PRICE_TO_BEAT_INPUT, data=value)

    def click_save(self):
        self.element_click_by_locator(locator=SAVE_BUTTON)


class ChargeRow(BasePage):

    def __init__(self, driver, base_row_element: WebElement, log, index):
        super().__init__(driver, log)
        self.base_row_element = base_row_element
        self.index = index

    def __get_description_element(self) -> Dropdown:
        locator = (By.ID, CHARGE_ROW_DESCRIPTION_ID + str(self.index))
        dropdown_element = self.get_element(locator=locator)
        return self.basic_types_factory.get_dropdown_by_element(dropdown_element)

    def clear_filter(self):
        self.__get_description_element().clear_filter()

    def click_remove(self):
        self.js_element_click(self.get_remove_button())

    def is_description_editable(self):
        main = self.__get_description_element()
        if main.is_dropdown_open():
            return True
        self.element_click(main.main_element)
        return main.is_dropdown_open()

    def is_remove_button_exist(self):
        return len(self.base_row_element.find_elements_by_css_selector("div[class*='IconFontWrapper']")) == 1

    def get_description(self):
        return self.__get_description_element().get_selected_value()

    def get_all_descriptions(self):
        return self.__get_description_element().get_available_options_values()

    def get_description_color(self):
        return self.__get_description_element().get_placeholder_color()

    def get_remove_button(self):
        return self.wait_for_element_visible(locator=(By.ID, CHARGE_ROW_REMOVE_BUTTON_ID + str(self.index)))

    def select_description(self, value):
        self.__get_description_element().select_option_by_value(value)

    def select_description_quick(self, value, clear=False):
        self.__get_description_element().select_option_by_value_quick(value, clear)

    def get_all_descriptions_with_search(self, value, clear=False):
        return self.__get_description_element().get_available_options_values_with_search(value, clear)

    def get_cost(self):
        _input = self.get_element(locator=(By.ID, CHARGE_ROW_COST_ID + str(self.index)))
        return self.get_input_value(locator=None, element=_input)

    def set_cost(self, value, clear=True):
        _input = self.get_element(locator=(By.ID, CHARGE_ROW_COST_ID + str(self.index)))
        self.send_keys(locator=None, element=_input, data=value, clear=clear)

    def get_revenue(self):
        _input = self.get_element(locator=(By.ID, CHARGE_ROW_REVENUE_ID + str(self.index)))
        return self.get_input_value(locator=None, element=_input)

    def set_revenue(self, value, clear=True):
        _input = self.get_element(locator=(By.ID, CHARGE_ROW_REVENUE_ID + str(self.index)))
        self.send_keys(locator=None, element=_input, data=value, clear=clear)
