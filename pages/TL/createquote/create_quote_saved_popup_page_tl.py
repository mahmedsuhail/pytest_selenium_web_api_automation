from selenium.webdriver.common.by import By

from pages.basepage import BasePage

CREATE_QUOTE_SAVED_POPUP = (By.ID, "footer_TL-1017_quoteSavedModal")
GO_TO_PENDING_QUOTES_BOARD_BUTTON = (By.ID, "footer_TL-1017_quoteSavedModalGoToPendingQuoteBoard")
CREATE_NEW_QUOTE_BUTTON = (By.ID, "footer_TL-1017_quoteSavedModalCreateNewQuoteButton")
CREATE_QUOTE_MESSAGE = (By.XPATH, "//div[starts-with(string(), 'Quote # ')] ")
CLOSE_BUTTON = (By.CSS_SELECTOR, "div[class*=CloseButton]")


class CreateQuoteSavedPopupPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def is_popup_displayed(self) -> bool:
        return self.is_element_displayed(locator=CREATE_QUOTE_SAVED_POPUP)

    def is_go_to_pending_quotes_button_displayed(self) -> bool:
        return self.is_element_displayed(locator=GO_TO_PENDING_QUOTES_BOARD_BUTTON)

    def is_create_new_quote_button_displayed(self) -> bool:
        return self.is_element_displayed(locator=CREATE_NEW_QUOTE_BUTTON)

    def is_close_button_visible(self) -> bool:
        return self.is_element_present(locator=CLOSE_BUTTON)

    def get_popup_message(self) -> str:
        return self.get_text(locator=CREATE_QUOTE_MESSAGE).strip()

    def click_go_to_pending_quotes_button(self):
        return self.element_click_by_locator(locator=GO_TO_PENDING_QUOTES_BOARD_BUTTON)

    def click_create_new_quote_button(self):
        return self.element_click_by_locator(locator=CREATE_NEW_QUOTE_BUTTON)
