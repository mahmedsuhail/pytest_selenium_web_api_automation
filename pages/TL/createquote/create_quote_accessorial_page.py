from selenium.webdriver.common.by import By

from pages.common.createquote_page import CreateQuotePage
from pages.basictypes.checkbox import Checkbox

ACC_SECTION = (By.ID, "accessorialNSpecialServices_TL-680_Card-Title")
ACC_CHECKBOX_LABEL = (
    By.CSS_SELECTOR, "#accessorialNSpecialServices_TL-680_Card-Title label[class*='CheckboxContainer']")

ACC_CHECKBOX_CONSIGNEE_HOURS_LABEL = (By.ID, "drop-trailer-consignee-hours")
ACC_CHECKBOX_CONSIGNEE_HOURS_INPUT = (By.ID, "accessorialNSpecialServices_TL-680_input-drop-trailer-consignee-hours")

ACC_CHECKBOX_FIELD_PALLETSEXCHANGE_PIECES_LABEL = (By.ID, "pallets")
ACC_CHECKBOX_FIELD_PALLETSEXCHANGE_PIECES_INPUT = (
    By.ID, "accessorialNSpecialServices_TL-680_input-pallets-piece-count")

ACC_CHECKBOX_FIELD_DROPTRAILERSHIPPER_LENGTH_LABEL = (By.ID, "drop-trailer-shipper-hours")
ACC_CHECKBOX_FIELD_DROPTRAILERSHIPPER_LENGTH_INPUT = (
    By.ID, "accessorialNSpecialServices_TL-680_input-drop-trailer-shipper-hours")

ACC_CHECKBOX_FIELD_TARP_SIZE_LABEL = (By.ID, "tarp-size")
ACC_CHECKBOX_FIELD_TARP_SIZE_BUTTON = (By.ID, "accessorialNSpecialServices_TL-680_select-tarp-size")
ACC_CHECKBOX_FIELD_TARP_SIZE_OPTIONS = (
    By.CSS_SELECTOR, "#accessorialNSpecialServices_TL-680_select-tarp-size [id*=option]")


class CreateQuoteAccessorialPage(CreateQuotePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    mapping_field_under_checkbox_label = {"Pallet Exchange": ACC_CHECKBOX_FIELD_PALLETSEXCHANGE_PIECES_LABEL,
                                          "Drop Trailer Shipper": ACC_CHECKBOX_FIELD_DROPTRAILERSHIPPER_LENGTH_LABEL,
                                          "Drop Trailer Consignee": ACC_CHECKBOX_CONSIGNEE_HOURS_LABEL,
                                          "Tarps": ACC_CHECKBOX_FIELD_TARP_SIZE_LABEL}

    mapping_field_under_checkbox_input = {"Pallet Exchange": ACC_CHECKBOX_FIELD_PALLETSEXCHANGE_PIECES_INPUT,
                                          "Drop Trailer Shipper": ACC_CHECKBOX_FIELD_DROPTRAILERSHIPPER_LENGTH_INPUT,
                                          "Drop Trailer Consignee": ACC_CHECKBOX_CONSIGNEE_HOURS_INPUT}

    tarp_size_expected_fields = {"Not Specified", "Smoke", "4 Foot", "6 Foot", "8 Foot", "Full"}

    def get_field_under_checkbox_label_displayed(self, name):
        selected_locator = self.mapping_field_under_checkbox_label.get(name)
        return self.is_element_displayed(selected_locator)

    def get_field_under_checkbox_input_displayed(self, name):
        selected_locator = self.mapping_field_under_checkbox_input.get(name)
        return self.is_element_displayed(selected_locator)

    def get_field_under_checkbox_input_value(self, name):
        selected_locator = self.mapping_field_under_checkbox_input.get(name)
        return self.get_attribute(selected_locator, attribute_type="value")

    def set_field_under_checkbox_input_value(self, name, value):
        selected_locator = self.mapping_field_under_checkbox_input.get(name)
        self.send_keys(data=value, locator=selected_locator)

    def get_all_accessorials_list_tl(self):
        """Under this method, we are returning accessorials list for TL, in the list of checkboxes"""
        try:
            selected_accessorials_list = []
            accessorial_labels = self.get_element_list(ACC_CHECKBOX_LABEL)

            for a_label in accessorial_labels:
                selected_accessorials_list.append(Checkbox(
                    name=a_label.text,
                    label_web_element=a_label,
                    log=self.log,
                    driver=self.driver
                ))

            self.print_all_found_accessories_tl(selected_accessorials_list)
            return selected_accessorials_list

        except Exception as e:
            self.log.error("Passed locators or locator types  are incorrect for "
                           "get_all_accessorials_list_tl method")
            self.log.error("Exception" + str(e))

    def print_all_found_accessories_tl(self, accessorial_list):
        for ass in accessorial_list:
            self.log.info(f"Found accessory name: {ass.name}")

    def get_tarp_size_field_displayed(self):
        return self.is_element_displayed(ACC_CHECKBOX_FIELD_TARP_SIZE_BUTTON)

    def get_tarp_size_dropdown(self):
        return self.basic_types_factory.get_dropdown(ACC_CHECKBOX_FIELD_TARP_SIZE_BUTTON)

    def validate_field_value(self, expect, actual):
        self.log.info(f"Validate if expect:{expect} is equal to actual:{actual}")
        return expect == actual
