from selenium.webdriver.common.by import By

from pages.TL.createquote.routelocations_page import RouteLocationsPage
from pages.TL.createquote.contactbook.contactbook_route_location_origin import ContactBookRouteLocationOriginPage

RL_ORIGIN_COMPANY = (By.ID, "routeLocations_TL-847_location_origin_textField_1")
RL_ORIGIN_CONTACT_NAME = (By.ID, "routeLocations_TL-847_location_origin_textField_2")
RL_ORIGIN_CONTACT_PHONE = (By.ID, "routeLocations_TL-847_location_origin_textField_3")
RL_ORIGIN_CONTACT_EMAIL = (By.ID, "routeLocations_TL-847_location_origin_textField_4")
RL_ORIGIN_PICKUP_REMARKS = (By.ID, "routeLocations_TL-847_location_origin_textField_9")
RL_ORIGIN_ADDR_L1 = (By.ID, "routeLocations_TL-847_location_origin_textField_5")
RL_ORIGIN_ADDR_L2 = (By.ID, "routeLocations_TL-847_location_origin_textField_6")
RL_ORIGIN_CITY = (By.ID, "routeLocations_TL-847_location_origin_textField_7")
RL_ORIGIN_ZIP = (By.ID, "routeLocations_TL-847_location_origin_textField_8")
RL_ORIGIN_OPEN_TIME = (By.ID, "routeLocations_TL-847_location_origin_openTime")
RL_ORIGIN_CLOSE_TIME = (By.ID, "routeLocations_TL-847_location_origin_closeTime")


class RouteLocationsOriginPage(RouteLocationsPage):

    def __init__(self, driver, log):
        locators = {
            "COMPANY_NAME": RL_ORIGIN_COMPANY,
            "CONTACT_NAME": RL_ORIGIN_CONTACT_NAME,
            "CONTACT_PHONE": RL_ORIGIN_CONTACT_PHONE,
            "CONTACT_EMAIL": RL_ORIGIN_CONTACT_EMAIL,
            "REMARKS": RL_ORIGIN_PICKUP_REMARKS,
            "ADDRESS_LINE1": RL_ORIGIN_ADDR_L1,
            "ADDRESS_LINE2": RL_ORIGIN_ADDR_L2,
            "ADDRESS_CITY": RL_ORIGIN_CITY,
            "ADDRESS_ZIP": RL_ORIGIN_ZIP,
            "OPEN_TIME": RL_ORIGIN_OPEN_TIME,
            "CLOSE_TIME": RL_ORIGIN_CLOSE_TIME
        }
        super().__init__(
            driver=driver,
            log=log,
            locators=locators,
            contactbook_page=ContactBookRouteLocationOriginPage(driver, log))
