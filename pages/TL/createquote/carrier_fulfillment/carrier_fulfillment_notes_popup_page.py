from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from pages.basepage import BasePage

CARRIER_FULFILLMENT_NOTES_POPUP = (By.ID, "Fulfillment_TL-715_carrierNoteModal_container")
OK_BUTTON = (By.ID, "Fulfillment_TL-715_carrierNoteModal_okButton")


class CarrierFulfillmentNotesPopupPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def carrier_fulfillment_notes_popup_is_displayed(self, timeout=0) -> bool:
        self.wait_for_element(locator=CARRIER_FULFILLMENT_NOTES_POPUP, timeout=timeout)
        return self.is_element_present(locator=CARRIER_FULFILLMENT_NOTES_POPUP) and self.is_element_displayed(
            locator=CARRIER_FULFILLMENT_NOTES_POPUP)

    def get_displayed_note_text_value(self) -> str:
        return self._get_currently_displayed_notes_popup().text.splitlines()[1]

    def click_ok_button(self):
        self._get_currently_displayed_notes_popup().find_element_by_id(OK_BUTTON[1]).click()

    def _get_currently_displayed_notes_popup(self) -> WebElement:
        elements = self.get_element_list(locator=CARRIER_FULFILLMENT_NOTES_POPUP)
        # last element is the popup which is actually visible to the user
        return elements[-1]
