from selenium.webdriver.common.by import By

from pages.basepage import BasePage

CARRIER_FULFILLMENT_SECTION = (By.ID, "Fulfillment_TL-715_selectCarrierSection")
CARRIER_FULFILLMENT_DATA_SECTION = (By.ID, "Fulfillment_TL-715_card")
SELECT_CARRIER_BUTTON = (By.ID, "Fulfillment_TL-715__selectCarrier")
SELECT_CARRIER_POPUP_WINDOW = (By.ID, "Fulfillment_TL-715_modal")
SELECT_CARRIER_SEARCH_BOX = (By.ID, "Fulfillment_TL-715_selectCarrier")
SELECT_CARRIER_SEARCH_BOX_INPUT = (By.ID, "#Fulfillment_TL-715_selectCarrier > div > div")
SELECT_CARRIER_POPUP_CLOSE_BUTTON = (By.CSS_SELECTOR, "#Fulfillment_TL-715_modal div[class*='CloseButton'] > span")
EDIT_CARRIER_BUTTON = (By.ID, "Fulfillment_TL-715_buttonEdit")
CARRIER_SEARCH_BOX_RESULT_LIST = (
    By.CSS_SELECTOR, "#Fulfillment_TL-715_selectCarrier div[class*='menu'] div[id*='react-select']")

CARRIER_NAME = (By.ID, "Fulfillment_TL-715_inputField_1")
ERP_CODE = (By.ID, "Fulfillment_TL-715_inputField_2")
TRUCK_NO = (By.ID, "Fulfillment_TL-715_inputField_3")
MC_NO = (By.ID, "Fulfillment_TL-715_inputField_7")
DOT_NO = (By.ID, "Fulfillment_TL-715_inputField_8")
TRAILER_NO = (By.ID, "Fulfillment_TL-715_inputField_9")
DRIVER_NAME = (By.ID, "Fulfillment_TL-715_inputField_10")
DRIVER_EMAIL = (By.ID, "Fulfillment_TL-715_inputField_11")
DRIVER_PHONE = (By.ID, "Fulfillment_TL-715_inputField_12")


class CarrierFulfillmentSectionPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_select_carrier_button(self):
        self.wait_for_element(locator=SELECT_CARRIER_BUTTON)
        element = self.get_element(locator=SELECT_CARRIER_BUTTON)
        self.element_click(
            element=element)

    def click_edit_carrier_button(self):
        element = self.get_element(locator=EDIT_CARRIER_BUTTON)
        self.element_click(
            element=element)

    def click_close_carrier_search_box(self):
        self.element_click(
            element=self.get_element(locator=SELECT_CARRIER_POPUP_CLOSE_BUTTON))

    def search_and_select_carrier(self, search_string, carrier_name):
        self.element_click(
            element=self.get_element(locator=SELECT_CARRIER_SEARCH_BOX))
        self.send_keys_to_browser(data=search_string)
        self.wait_for_carrier_search_result()

        elements = self.get_element_list(locator=CARRIER_SEARCH_BOX_RESULT_LIST)

        for element in elements:
            if element.text.splitlines()[0] == carrier_name:
                self.scroll_into_view(element=element)
                self.element_click(element=element)
                break

    def wait_for_carrier_search_result(self):
        self.wait_for_element(locator=CARRIER_SEARCH_BOX_RESULT_LIST)

    def fulfillment_container_is_displayed(self) -> bool:
        self.wait_for_element(locator=CARRIER_FULFILLMENT_SECTION)
        return self.is_element_present(locator=CARRIER_FULFILLMENT_SECTION) and self.is_element_displayed(
            locator=CARRIER_FULFILLMENT_SECTION)

    def get_carrier_name(self) -> str:
        return self.get_text(locator=CARRIER_NAME)

    def get_erp_code(self) -> str:
        return self.get_text(locator=ERP_CODE)

    def get_truck_no(self) -> str:
        return self.get_text(locator=TRUCK_NO)

    def get_mc_no(self) -> str:
        return self.get_text(locator=MC_NO)

    def get_dot_no(self) -> str:
        return self.get_text(locator=DOT_NO)

    def get_trailer_no(self) -> str:
        return self.get_text(locator=TRAILER_NO)

    def get_driver_name(self) -> str:
        return self.get_text(locator=DRIVER_NAME)

    def get_driver_email(self) -> str:
        return self.get_text(locator=DRIVER_EMAIL)

    def get_driver_phone(self) -> str:
        return self.get_text(locator=DRIVER_PHONE)

    def carrier_search_window_is_displayed(self) -> bool:
        return self.is_element_present(locator=SELECT_CARRIER_POPUP_WINDOW) and self.is_element_displayed(
            locator=SELECT_CARRIER_POPUP_WINDOW)

    def select_carrier_button_is_displayed(self) -> bool:
        return self.is_element_present(locator=SELECT_CARRIER_BUTTON) and self.is_element_displayed(
            locator=SELECT_CARRIER_BUTTON)
