from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.color import Color

from pages.basepage import BasePage
from pages.common.builders.driver_builder import DriverBuilder
from pages.common.models.dispatcher import Dispatcher
from pages.common.models.driver import Driver

CARRIER_FULFILLMENT_POPUP = (By.ID, "Fulfillment_TL-715_modal")
CARRIER_NAME_FIELD = (By.ID, "Fulfillment_TL-715_carrier")
TRAILER_NO = (By.ID, "SelectCarrier_TL-3847_trailerNumber")
TRUCK_NO = (By.ID, "SelectCarrier_TL-3847_truckNumber")
MC_NUMBER = (By.ID, "SelectCarrier_TL-3847_MCNumber")
ERP_CODE = (By.ID, "SelectCarrier_TL-3847_ERPCode")
DOT_NUMBER = (By.ID, "SelectCarrier_TL-3847_DOTNumber")
LEGAL_NAME = (By.ID, "SelectCarrier_TL-3847_legalName")
CITY_STATE_ZIP = (By.ID, "SelectCarrier_TL-3847_cityStateZip")

ALL_DISPATCHERS = (By.XPATH, "//*[contains(@id,'SelectCarrier_TL-3847_dispatcherRow')]")
ALL_DRIVERS = (By.XPATH, "//*[contains(@id,'SelectCarrier_TL-3847_driverRow')]")
DISPATCHER_ROW_BASE = "SelectCarrier_TL-3847_dispatcherRow_"
SAVE_DISPATCHER_BUTTON_BASE = "SelectCarrier_TL-3847_dispatcherButtonSave_"
SAVE_DRIVER_BUTTON_BASE = "SelectCarrier_TL-3847_driverButtonSave_"
EDIT_DISPATCHER_BUTTON_BASE = "SelectCarrier_TL-3847_dispatcherButtonEdit_"
EDIT_DISPATCHER_NAME_INPUT = (By.ID, "SelectCarrier_TL-3847_dispatcherName")
EDIT_DISPATCHER_PHONE_INPUT = (By.ID, "SelectCarrier_TL-3847_dispatcherPhone")
EDIT_DISPATCHER_NAME_INPUT_BASE = "SelectCarrier_TL-3847_dispatcherEditContactName_"
EDIT_DISPATCHER_PHONE_INPUT_BASE = "SelectCarrier_TL-3847_dispatcherEditPhone_"
EDIT_DISPATCHER_EMAIL_INPUT_BASE = "SelectCarrier_TL-3847_dispatcherEditEmail_"
EDIT_DISPATCHER_FAX_INPUT_BASE = "SelectCarrier_TL-3847_dispatcherEditFax_"
EDIT_DRIVER_NAME_INPUT = (By.ID, "SelectCarrier_TL-3847_driverName")
EDIT_DRIVER_PHONE_INPUT = (By.ID, "SelectCarrier_TL-3847_driverPhone")
EDIT_DRIVER_NAME_INPUT_BASE = "SelectCarrier_TL-3847_driverEditDriverName_"
EDIT_DRIVER_PHONE_INPUT_BASE = "SelectCarrier_TL-3847_driverEditPhone_"
EDIT_DRIVER_BUTTON_BASE = "SelectCarrier_TL-3847_driverButtonEdit_"

SELECTED_DISPATCHER_HEX_BACKGROUND_COLOR = '#40a4df'
DRIVER_ROW_BASE = "SelectCarrier_TL-3847_driverRow_"

ADD_CONTACT_BUTTON = (By.ID, "SelectCarrier_TL-3847_addDispatcher")
ADD_DRIVER_BUTTON = (By.ID, "SelectCarrier_TL-3847_addDriver")

REMOVE_CARRIER_BUTTON = (By.ID, "SelectCarrier_TL-3847_buttonRemoveCarrier")
CANCEL_BUTTON = (By.ID, "Fulfillment_TL-715_buttonCancel")
SAVE_AND_ASSIGN_BUTTON = (By.ID, "Fulfillment_TL-715_buttonSave")


class CarrierFulfillmentPopupPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_remove_carrier_button(self):
        element = self.get_element(locator=REMOVE_CARRIER_BUTTON)
        self.element_click(
            element=element)

    def click_cancel_button(self):
        element = self.get_element(locator=CANCEL_BUTTON)
        self.element_click(
            element=element)

    def close_popup_if_present(self):
        element = self.get_element(locator=CANCEL_BUTTON)
        if element is not None:
            self.element_click(
                element=element)

    def click_add_contact_button(self):
        element = self.get_element(locator=ADD_CONTACT_BUTTON)
        self.element_click(
            element=element)

    def click_add_driver_button(self):
        element = self.get_element(locator=ADD_DRIVER_BUTTON)
        self.element_click(
            element=element)

    def click_save_and_assign_button(self):
        element = self.get_element(locator=SAVE_AND_ASSIGN_BUTTON)
        self.element_click(
            element=element)

    def add_driver_button_is_displayed(self) -> bool:
        self.scroll_into_view(element=self.get_element(ADD_DRIVER_BUTTON))
        return self.is_element_present(locator=ADD_DRIVER_BUTTON) and self.is_element_displayed(
            locator=ADD_DRIVER_BUTTON)

    def add_dispatcher_button_is_displayed(self) -> bool:
        self.scroll_into_view(element=self.get_element(ADD_CONTACT_BUTTON))
        return self.is_element_present(locator=ADD_CONTACT_BUTTON) and self.is_element_displayed(
            locator=ADD_CONTACT_BUTTON)

    def save_and_assign_button_is_displayed(self) -> bool:
        self.scroll_into_view(element=self.get_element(SAVE_AND_ASSIGN_BUTTON))
        return self.is_element_present(locator=SAVE_AND_ASSIGN_BUTTON) and self.is_element_displayed(
            locator=SAVE_AND_ASSIGN_BUTTON)

    def get_carrier_name(self) -> str:
        return self.get_text(locator=CARRIER_NAME_FIELD).splitlines()[1]

    def get_trailer_number_value(self) -> str:
        return self._get_textbox_value(TRAILER_NO)

    def enter_trailer_number_value(self, trailer_no):
        css_locator = (By.CSS_SELECTOR, f"#{TRAILER_NO[1]} input")
        element = self.get_element(locator=css_locator)

        self.element_click(element=element)
        self.send_keys(data=trailer_no, locator=css_locator)

    def get_truck_number_value(self) -> str:
        return self._get_textbox_value(TRUCK_NO)

    def get_mc_number_value(self) -> str:
        return self._get_textbox_value(MC_NUMBER)

    def get_erp_code_value(self) -> str:
        return self._get_textbox_value(ERP_CODE)

    def get_dot_number_value(self) -> str:
        return self._get_textbox_value(DOT_NUMBER)

    def get_legal_name_value(self) -> str:
        return self._get_textbox_value(LEGAL_NAME)

    def get_city_state_zip_value(self) -> str:
        return self._get_textbox_value(CITY_STATE_ZIP)

    def is_trailer_number_editable(self) -> bool:
        return self._check_if_textbox_is_editable(TRAILER_NO)

    def is_truck_number_editable(self) -> bool:
        return self._check_if_textbox_is_editable(TRUCK_NO)

    def is_mc_number_editable(self) -> bool:
        return self._check_if_textbox_is_editable(MC_NUMBER)

    def is_erp_code_editable(self) -> bool:
        return self._check_if_textbox_is_editable(ERP_CODE)

    def is_dot_number_editable(self) -> bool:
        return self._check_if_textbox_is_editable(DOT_NUMBER)

    def is_legal_name_editable(self) -> bool:
        return self._check_if_textbox_is_editable(LEGAL_NAME)

    def is_city_state_zip_editable(self) -> bool:
        return self._check_if_textbox_is_editable(CITY_STATE_ZIP)

    def get_dispatcher_name(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index} div:nth-of-type(1) article")
        return self.get_text(locator)

    def get_dispatcher_phone(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index} div:nth-of-type(2) article")
        return self.get_text(locator)

    def get_dispatcher_email(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index} div:nth-of-type(3) article")
        return self.get_text(locator)

    def get_dispatcher_fax(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index} div:nth-of-type(4) article")
        return self.get_text(locator)

    def dispatcher_is_selected(self, index) -> bool:
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index}")
        element = self.get_element(locator=locator)
        background_color = element.value_of_css_property('background-color')
        return Color.from_string(background_color).hex == SELECTED_DISPATCHER_HEX_BACKGROUND_COLOR

    def driver_is_selected(self, index) -> bool:
        locator = (By.CSS_SELECTOR, f"#{DRIVER_ROW_BASE}{index}")
        element = self.get_element(locator=locator)
        background_color = element.value_of_css_property('background-color')
        return Color.from_string(background_color).hex == SELECTED_DISPATCHER_HEX_BACKGROUND_COLOR

    def get_driver_name(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{DRIVER_ROW_BASE}{index} div:nth-of-type(1) article")
        return self.get_text(locator)

    def get_driver_phone(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{DRIVER_ROW_BASE}{index} div:nth-of-type(2) article")
        return self.get_text(locator)

    def _get_textbox_value(self, automation_id_locator) -> str:
        css_locator = (By.CSS_SELECTOR, f"#{automation_id_locator[1]} input")
        return self.get_attribute(css_locator, "value")

    def _check_if_textbox_is_editable(self, automation_id_locator) -> bool:
        css_locator = (By.CSS_SELECTOR, f"#{automation_id_locator[1]} input")
        element = self.get_element(locator=css_locator)

        temp_value = '1234'
        self.element_click(element=element)
        self.send_keys(data=temp_value, locator=css_locator)

        is_editable = self.get_attribute(locator=css_locator, attribute_type="value") == temp_value

        return is_editable

    def select_dispatcher(self, index):
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index}")
        self.page_vertical_scroll(locator=locator)
        self.element_click_by_locator(locator=locator)

    def select_driver(self, index):
        locator = (By.CSS_SELECTOR, f"#{DRIVER_ROW_BASE}{index}")
        self.page_vertical_scroll(locator=locator)
        self.element_click_by_locator(locator=locator)

    def click_save_dispatcher(self, index):
        locator = (By.CSS_SELECTOR, f"#{SAVE_DISPATCHER_BUTTON_BASE}{index} svg")
        self.page_vertical_scroll(locator=locator)
        self.element_click_by_locator(locator=locator)

    def click_save_driver(self, index):
        locator = (By.CSS_SELECTOR, f"#{SAVE_DRIVER_BUTTON_BASE}{index} svg")
        self.page_vertical_scroll(locator=locator)
        self.element_click_by_locator(locator=locator)

    def verify_save_dispatcher_button_is_visible(self, index) -> bool:
        locator = (By.CSS_SELECTOR, f"#{SAVE_DISPATCHER_BUTTON_BASE}{index} svg")
        return self.is_element_displayed(locator=locator)

    def verify_save_driver_button_is_visible(self, index) -> bool:
        locator = (By.CSS_SELECTOR, f"#{SAVE_DRIVER_BUTTON_BASE}{index} svg")
        return self.is_element_displayed(locator=locator)

    def click_edit_dispatcher(self, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_BUTTON_BASE}{index} svg")
        self.page_vertical_scroll(locator=locator)
        self.element_click_by_locator(locator=locator)

    def click_edit_driver(self, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DRIVER_BUTTON_BASE}{index} svg")
        self.page_vertical_scroll(locator=locator)
        self.element_click_by_locator(locator=locator)

    def verify_dispatcher_name_is_required(self) -> bool:
        value = self.get_element(locator=EDIT_DISPATCHER_NAME_INPUT).text
        return value == 'Required'

    def verify_dispatcher_phone_is_required(self) -> bool:
        value = self.get_element(locator=EDIT_DISPATCHER_PHONE_INPUT).get_property("placeholder")
        return value == 'Required'

    def verify_driver_name_is_required(self) -> bool:
        value = self.get_element(locator=EDIT_DRIVER_NAME_INPUT).text
        return value == 'Required'

    def verify_driver_phone_is_required(self) -> bool:
        value = self.get_element(locator=EDIT_DRIVER_PHONE_INPUT).get_property(name="placeholder")
        return value == 'Required'

    def enter_dispatcher_data(self, dispatcher, index):
        self.enter_dispatcher_name(dispatcher.name, index)
        self.enter_dispatcher_phone(dispatcher.phone, index)
        self.enter_dispatcher_email(dispatcher.email, index)
        self.enter_dispatcher_fax(dispatcher.fax, index)

    def enter_dispatcher_name(self, name, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_NAME_INPUT_BASE}{index} input")
        self.send_keys(data=name, locator=locator)
        self.send_keys_to_browser(data=Keys.TAB)

    def enter_dispatcher_phone(self, phone, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_PHONE_INPUT_BASE}{index} input")
        self.send_keys(data=phone, locator=locator)
        self.send_keys_to_browser(data=Keys.TAB)

    def enter_driver_name(self, name, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DRIVER_NAME_INPUT_BASE}{index} input")
        self.send_keys(data=name, locator=locator)
        self.send_keys_to_browser(data=Keys.TAB)

    def enter_driver_phone(self, phone, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DRIVER_PHONE_INPUT_BASE}{index} input")
        self.send_keys(data=phone, locator=locator)
        self.send_keys_to_browser(data=Keys.TAB)

    def enter_dispatcher_email(self, email, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_EMAIL_INPUT_BASE}{index} input")
        self.send_keys(data=email, locator=locator)

    def enter_dispatcher_fax(self, fax, index):
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_FAX_INPUT_BASE}{index} input")
        self.send_keys(data=fax, locator=locator)

    def get_dispatcher_entered_name(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_NAME_INPUT_BASE}{index} input")
        return self.get_element(locator=locator).get_attribute(name='value')

    def get_driver_entered_name(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DRIVER_NAME_INPUT_BASE}{index} input")
        return self.get_element(locator=locator).get_attribute(name='value')

    def get_dispatcher_entered_phone(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_PHONE_INPUT_BASE}{index} input")
        return self.get_element(locator=locator).get_attribute(name='value')

    def get_driver_entered_phone(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DRIVER_PHONE_INPUT_BASE}{index} input")
        return self.get_element(locator=locator).get_attribute(name='value')

    def get_dispatcher_entered_email(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_EMAIL_INPUT_BASE}{index} input")
        return self.get_text(locator=locator)

    def verify_entered_email_is_invalid(self, index) -> bool:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_EMAIL_INPUT_BASE}{index} input")
        return self.get_element(locator=locator).get_attribute('placeholder') == "Invalid email"

    def get_dispatcher_entered_fax(self, index) -> str:
        locator = (By.CSS_SELECTOR, f"#{EDIT_DISPATCHER_FAX_INPUT_BASE}{index} input")
        return self.get_text(locator=locator)

    def get_dispatchers_count(self) -> int:
        count = 0
        dispatchers = self.get_element_list(locator=ALL_DISPATCHERS)

        for dispatcher in dispatchers:
            if dispatcher.text != '':
                count = count + 1

        return count

    def get_drivers_count(self) -> int:
        count = 0
        drivers = self.get_element_list(locator=ALL_DRIVERS)

        for driver in drivers:
            if driver.text != '':
                count = count + 1

        return count

    def get_dispatcher_data(self, index) -> Dispatcher:
        locator = (By.CSS_SELECTOR, f"#{DISPATCHER_ROW_BASE}{index}")
        element = self.get_element(locator=locator)

        text = element.text.splitlines()

        dispatcher = Dispatcher()
        dispatcher.name = text[0]
        dispatcher.phone = text[1]
        dispatcher.email = text[2]
        dispatcher.fax = text[3]

        return dispatcher

    def get_driver_data(self, index) -> Driver:
        locator = (By.CSS_SELECTOR, f"#{DRIVER_ROW_BASE}{index}")
        element = self.get_element(locator=locator)

        text = element.text.splitlines()

        driver = DriverBuilder() \
            .with_name(name=text[0]) \
            .with_phone(phone=text[1]) \
            .build()

        return driver
