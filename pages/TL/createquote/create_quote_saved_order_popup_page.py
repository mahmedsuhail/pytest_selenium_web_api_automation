from selenium.webdriver.common.by import By

from pages.basepage import BasePage

CREATE_ORDER_SAVED_POPUP = (By.ID, "footer_TL-1191_createOrderModalSuccess")
GO_TO_SALES_ORDER_BOARD_BUTTON = (By.ID, "footer_TL-1191_createOrderModalButtonGoToSalesOrderBoard")
GO_TO_BOL = (By.ID, "footer_TL-1191_createOrderModalButtonGoToBol")
CREATE_ORDER_MESSAGE = (By.XPATH, "//div[text()='Order Created!']")
CLOSE_BUTTON = (By.XPATH, "//div[@id='footer_TL-1191_createOrderModalSuccess']//div[contains(@class,'CloseButton')]")


class CreateQuoteSavedOrderPopupPageTL(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def is_popup_displayed(self) -> bool:
        return self.is_element_displayed(locator=CREATE_ORDER_SAVED_POPUP)

    def is_go_to_sales_order_button_displayed(self) -> bool:
        return self.is_element_displayed(locator=GO_TO_SALES_ORDER_BOARD_BUTTON)

    def is_go_to_bol_button_displayed(self) -> bool:
        return self.is_element_displayed(locator=GO_TO_BOL)

    def is_close_button_visible(self) -> bool:
        return self.is_element_present(locator=CLOSE_BUTTON)

    def get_popup_message(self) -> str:
        return self.get_text(locator=CREATE_ORDER_MESSAGE).strip()

    def click_go_to_sales_order_button(self):
        self.wait_for_element_clickable(locator=GO_TO_SALES_ORDER_BOARD_BUTTON)
        return self.element_click_by_locator(locator=GO_TO_SALES_ORDER_BOARD_BUTTON)

    def click_go_to_bol(self):
        self.wait_for_element_clickable(locator=GO_TO_BOL)
        return self.element_click_by_locator(locator=GO_TO_BOL)

    def wait_for_create_order_popup_visible(self):
        self.wait_for_element_visible(locator=CREATE_ORDER_SAVED_POPUP, timeout=120)
