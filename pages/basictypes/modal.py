from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from base.selenium_driver import SeleniumDriver


class Modal:
    CLOSE_BUTTON = "div[class*=CloseButton]"

    def __init__(self, modal_name, driver, log):
        self.modal_name = modal_name
        self.driver_operations = SeleniumDriver(driver, log)
        self.modal_web_element = self.__init_modal_web_element()

    def MAIN_MODAL(self):
        return By.XPATH, f"//div[contains(@class,'TextFieldPopupContainer') and .//div[text()='{self.modal_name}']]"

    def click_close(self):
        self.modal_web_element.find_element_by_css_selector(self.CLOSE_BUTTON).click()

    def is_close_button_visible(self):
        return self.modal_web_element.find_element_by_css_selector(self.CLOSE_BUTTON).is_displayed()

    def __init_modal_web_element(self) -> WebElement:
        return self.driver_operations.get_element(self.MAIN_MODAL())
