from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from base.selenium_driver import SeleniumDriver

POPUP_SPINNER = (By.XPATH, "//div[text()='Loading']")
OPTION_ELEMENT = (By.XPATH, "//div[contains(@id,'-option-0')]")


class SpecialInput:

    def __init__(self, driver, log, locator: (By, str) = None, element: WebElement = None):
        if element is not None:
            self.element = element
        else:
            self.element = SeleniumDriver(driver, log).get_element(locator=locator)
        self.driver = driver
        self.driver_operations = SeleniumDriver(driver, log)

    def get_text_value(self):
        # input tag can contain text either in value tag or in text
        t1 = self.element.get_attribute("value")
        if t1:
            return t1
        return self.element.text

    # TODO - refactor, selection of option from dropdown should be done elsewhere
    def set_text_value(self, text: str, select_option=True):
        if self.element.tag_name == "input":
            self.driver_operations.send_keys(element=self.element, data=text, clear=True)
        else:
            self.driver_operations.element_click(element=self.element)
            # locate embedded input
            _input = self.driver_operations.get_element(locator=(By.TAG_NAME, "input"), parent=self.element)
            self.driver_operations.send_keys(data=text, element=_input, clear=True)

            if len(text) == 0:
                _input.clear()  # additional clear for input with hint

            if select_option:
                self.driver_operations.wait_for_element_clickable((By.XPATH, "//div[contains(@id,'-option-0')]"))
                menuitem = self.driver_operations.get_element(
                    locator=OPTION_ELEMENT,
                    parent=self.element)
                self.driver_operations.element_click(element=menuitem)

    def type_text(self, text: str):
        self.element.send_keys(text)

    def clear(self):
        if self.element.tag_name == "input":
            self.element.clear()
        else:
            _input = self.driver_operations.get_element(locator=(By.TAG_NAME, "input"), parent=self.element)
            self.driver_operations.clear_by_select_all_and_delete(element=_input)
