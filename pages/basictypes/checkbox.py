from selenium.webdriver.common.by import By

CHECKBOX_LOCATOR = (By.TAG_NAME, "svg")


class Checkbox:

    def __init__(self, name, label_web_element, driver, log):
        self.name = name
        self.driver = driver
        self._label_web_element = label_web_element

    def is_checked_old_way(self) -> bool:
        return len(self._label_web_element.find_elements(*CHECKBOX_LOCATOR)) != 0

    def is_checked(self) -> bool:
        return self.driver.execute_script("return arguments[0].querySelectorAll('svg').length >= 1;",
                                          self._label_web_element)

    def check(self, scroll_to=False):
        if not self.is_checked():
            if scroll_to:
                self.driver.execute_script("arguments[0].scrollIntoView();", self._label_web_element)
            self._label_web_element.click()

    def uncheck(self, scroll_to=False):
        if self.is_checked():
            if scroll_to:
                self.driver.execute_script("arguments[0].scrollIntoView();", self._label_web_element)
            self._label_web_element.click()
