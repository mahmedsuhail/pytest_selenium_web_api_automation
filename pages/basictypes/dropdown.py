from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement

from base.selenium_driver import SeleniumDriver

DROPDOWN_OPTIONS = (By.CSS_SELECTOR, "[id*=option]")


class Dropdown:

    def __init__(self, driver, main_dropdown_element: WebElement, log):
        self.driver = driver
        self.driver_operations = SeleniumDriver(driver=driver, log=log)
        self.main_element = main_dropdown_element
        self.input_element = self.__get_input_element()

    def clear_filter(self):
        try:
            actions = ActionChains(self.driver)
            actions.click(self.input_element)
            actions.key_down(Keys.CONTROL)
            actions.send_keys("a")
            actions.key_up(Keys.CONTROL)
            actions.send_keys(Keys.BACK_SPACE)
            actions.send_keys(Keys.ESCAPE)
            actions.perform()
        except Exception as E:
            self.driver_operations.log.error(f"Cannot clear dropdown filter field, {str(E)}")

    def is_dropdown_open(self):
        return len(self.main_element.find_elements_by_css_selector("[id*=option]")) > 0

    def wait_for_dropdown_to_appear(self, timeout=10):
        self.driver_operations.wait_for_element_visible(locator=DROPDOWN_OPTIONS, timeout=timeout)

    def get_placeholder(self):
        placeholder = self.main_element.find_elements_by_css_selector("div[class*='placeholder']")
        if len(placeholder) > 0:
            return placeholder[0].text
        return ''

    def get_placeholder_color(self):
        placeholder = self.main_element.find_elements_by_css_selector("div[class*='placeholder']")
        if len(placeholder) > 0:
            return placeholder[0].value_of_css_property("color")
        return ''

    def get_selected_value(self):
        return self.main_element.text

    def get_available_options_values(self) -> [str]:
        self.__open_if_closed__()
        self.options = self.main_element.find_elements_by_css_selector("[id*=option]")
        options_values = []
        for element in self.options:
            options_values.append(element.text)

        self.__close_if_opened__()

        return options_values

    def get_available_options_values_with_search(self, value, clear=False) -> [str]:
        if clear:
            self.clear_filter()
        self.input_element.send_keys(value)
        option = self.main_element.find_elements_by_css_selector("[id*=option]")
        options_values = []
        for element in option:
            options_values.append(element.text)

        return options_values

    def select_option_by_value(self, value):
        self.__open_if_closed__()
        # options = self.main_element.find_elements_by_css_selector("[id*=option]")
        # due to structure changes, look for options in the whole document (there should be only one option list
        options = self.driver_operations.get_element_list(locator=(By.CSS_SELECTOR, "[id*=option]"))
        option = next((x for x in options if x.text == value), None)
        self.driver_operations.element_click(element=option, scroll_to=False)

    def select_option_by_value_quick(self, value, clear=False):
        if clear:
            self.clear_filter()
        self.input_element.send_keys(value)
        option = self.driver_operations.get_element(locator=(By.CSS_SELECTOR, "[id*=option]"))
        self.driver_operations.element_click(element=option, scroll_to=False)

    def __open_if_closed__(self):
        if not self.is_dropdown_open():
            self.driver_operations.element_click(element=self.main_element, scroll_to=False)

    def __close_if_opened__(self):
        if self.is_dropdown_open():
            self.driver_operations.element_click(element=self.main_element, scroll_to=False)

    def __get_input_element(self):
        return self.main_element.find_element_by_tag_name("input")
