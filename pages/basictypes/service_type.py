import enum


class ServiceType(enum.Enum):
    LTL = 1
    TL = 2
