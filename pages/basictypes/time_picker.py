from selenium.webdriver.common.by import By

from base.selenium_driver import SeleniumDriver

MENU_ELEMENT = (By.XPATH, "//div[contains(@id, '-option-')]")


class TimePicker:

    def __init__(self, driver, log, locator: (By, str)):
        self.locator = locator
        self.driver_operations = SeleniumDriver(driver, log)
        self.element = self.driver_operations.get_element(self.locator)

    def get_time_value_str(self):
        return self.driver_operations.get_text(self.locator)

    def set_time_value_type_text(self, time_to_set: str):
        self.driver_operations.send_keys(data=time_to_set, element=self.element, clear=True)

    def set_time_value(self, time_to_set: str):
        self.driver_operations.page_vertical_scroll(element=self.element)
        self.driver_operations.element_click(element=self.element)

        self.driver_operations.wait_for_element_visible(locator=MENU_ELEMENT)

        self.driver_operations.select_based_on_text(locator=MENU_ELEMENT, select_value=time_to_set, parent=self.element)
