from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from base.selenium_driver import SeleniumDriver
from pages.basictypes.checkbox import Checkbox
from pages.basictypes.dropdown import Dropdown
from pages.basictypes.specialinput import SpecialInput
from pages.basictypes.time_picker import TimePicker
from pages.basictypes.calendarcontrol import CalendarControl


class BasicTypesFactory:

    def __init__(self, driver, log):
        self.log = log
        self.driver = driver
        self.driver_operations = SeleniumDriver(driver=driver, log=log)

    def get_dropdown(self, locator) -> Dropdown:
        return self.get_dropdown_by_element(element=self.driver_operations.get_element(locator))

    def get_dropdown_by_element(self, element: WebElement) -> Dropdown:
        """Get typical dropdown as object"""
        element_dropdown = Dropdown(
            driver=self.driver,
            main_dropdown_element=element,
            log=self.log)

        return element_dropdown

    def get_checkbox(self, name, locator):
        """Get typical dropdown as object"""
        element_checkbox = Checkbox(
            name=name,
            label_web_element=self.driver_operations.get_element(locator),
            log=self.log,
            driver=self.driver)

        return element_checkbox

    def get_input(self, locator: (By, str)) -> SpecialInput:
        element_input = SpecialInput(
            driver=self.driver,
            log=self.log,
            locator=locator
        )

        return element_input

    def get_time_picker(self, locator: (By, str)) -> TimePicker:
        time_picker = TimePicker(
            driver=self.driver,
            log=self.log,
            locator=locator
        )

        return time_picker

    def get_calendar(self, locator: (By, str)) -> CalendarControl:
        calendar = CalendarControl(
            driver=self.driver,
            locator=locator,
            log=self.log
        )

        return calendar
