from datetime import date

from selenium.webdriver.common.by import By

from base.selenium_driver import SeleniumDriver

DATE_FORMAT_STRING = "%m/%d/%Y"


class CalendarControl:

    def __init__(self, driver, log, locator=(By, str)):
        self.log = log
        self.driver = driver
        self.locator = locator
        self.driver_operations = SeleniumDriver(driver, log)
        self.element = self.driver_operations.get_element(locator=self.locator)

    def click_on_icon(self):
        self.driver_operations.element_click(element=self.element)

    def set_date(self, date_to_set: date):
        _input_element = self.driver_operations.get_element(locator=(By.TAG_NAME, "input"), parent=self.element)
        self.driver_operations.replace_text(text=date_to_set.strftime(DATE_FORMAT_STRING), element=_input_element)

    def set_date_without_escape(self, date_to_set: date):
        _input_element = self.driver_operations.get_element(locator=(By.TAG_NAME, "input"), parent=self.element)
        self.driver_operations.replace_text_without_escape(text=date_to_set.strftime(DATE_FORMAT_STRING),
                                                           element=_input_element)
