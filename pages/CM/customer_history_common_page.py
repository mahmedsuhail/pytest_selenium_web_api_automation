from selenium.webdriver.common.by import By

from pages.basepage import BasePage

customer_account_info_view_history_button = (By.XPATH, "//div[contains(@class,'informationstyle__ButtonWrapper')]"
                                                       "//span[text()='View History']")
customer_history_tab_account_history = (By.XPATH, "//li[@title='ACCOUNT HISTORY']")
customer_history_tab_shipment_history = (By.XPATH, "//li[@title='SHIPMENT HISTORY']")
customer_history_tab_quotes_not_booked = (By.XPATH, "//li[@title='QUOTES NOT BOOKED']")
customer_history_tab_account_history_filter_tabs = (By.XPATH, "//div[contains(@class,'ag-header-cell-label')]")


class CustomerHistoryCommonPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_view_history_button_in_acc_info(self):
        self.element_click_by_locator(locator=customer_account_info_view_history_button)

    def view_history_button_is_displayed(self):
        return self.is_element_present(locator=customer_account_info_view_history_button) and \
            self.is_element_displayed(locator=customer_account_info_view_history_button)

    def default_history_board_validations(self):
        common_flag = True
        common_flag = common_flag and self.validators.verify_text_match(
            self.get_attribute(customer_history_tab_account_history, "aria-selected"), "true")
        common_flag = common_flag and self.validators.verify_text_match(
            self.get_attribute(customer_history_tab_shipment_history, "aria-selected"), "false")
        common_flag = common_flag and self.validators.verify_text_match(
            self.get_attribute(customer_history_tab_quotes_not_booked, "aria-selected"), "false")
        self.log.info("Common Flag Status for filter Tab" + str(common_flag))

        account_history_filters_list = ["Section", "Field Name", "Changed By", "Changed Via Application", "Change Date",
                                        "Old Value", "New Value", "Action"]
        account_history_filter_tabs = self.get_element_list(customer_history_tab_account_history_filter_tabs)
        self.log.info("Length of Filters list : " + str(len(account_history_filter_tabs)))
        for i in range(0, len(account_history_filter_tabs)):
            self.driver.execute_script("arguments[0].scrollIntoView();", account_history_filter_tabs[i])
            if account_history_filters_list[i].strip().lower() == account_history_filter_tabs[i].text.lower():
                common_flag = common_flag and True
            else:
                self.log.error(
                    "Account History Filters are not matching : " + str(account_history_filter_tabs[i].text))
                common_flag = common_flag and False

        self.log.info("Final Common Flag Status " + str(common_flag))
        return common_flag
