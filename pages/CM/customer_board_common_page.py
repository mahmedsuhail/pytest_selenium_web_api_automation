from selenium.webdriver.common.by import By

from pages.basepage import BasePage

customer_management_icon = (By.CSS_SELECTOR, "a[title='Customer Management']")
cm_board_header = (By.XPATH, "//h1[text()='Customer Management']")
time_frame_dropdown = (By.CSS_SELECTOR, "div[id = header_sm-126_timeframeFilter_timeframePresetDropdown]")
select_time_frame = (By.XPATH, "//div[@id = 'header_sm-126_timeframeFilter_timeframePresetDropdown']//div[contains("
                               "@class, '-menu')]//div//div")
customer_board_loader = (By.XPATH, "//span[text()='Loading']")
customer_view_button = "//button[@id='customersBoard_sm-126_grid_"
first_view_button_load = (By.XPATH, "(//button[contains(@id,'customersBoard_sm-126_grid')])[1]")
customer_board_headers = (By.XPATH, "//span[@ref='eText']")


class CustomerBoardCommonPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def click_customer_board_icon(self):
        """Clicking Customer Management Icon"""
        try:
            self.log.info("Inside click_customer_board_icon Method")
            self.wait_for_element(customer_management_icon, "display")
            self.element_click(self.get_element(customer_management_icon))
            self.wait_for_element(cm_board_header, "display")
            if self.is_element_displayed(cm_board_header):
                return True
            else:
                return False
        except Exception as e:
            self.log.error("Exception occurred in click_customer_board_icon Method and Exception is : " + str(e))

    def get_customer_link_attribute_value(self, attributeName):
        try:
            self.log.info("Inside get_customer_link_attribute_value Method")
            return self.get_attribute(customer_management_icon, attributeName)
        except Exception as e:
            self.log.error("Exception occurred in Method and Exception is : " + str(e))

    def select_timeframe(self, timeframe):
        """Select Time frame"""
        try:
            self.element_click(self.get_element(time_frame_dropdown))
            self.select_based_on_text(select_time_frame, timeframe)
            selected_timeframe = self.get_text(time_frame_dropdown)
            self.log.info("selected_timeframe : " + str(selected_timeframe))
            if selected_timeframe.lower() == timeframe.lower():
                return True
            else:
                self.log.debug("Selected time frame is not same as passed")
                return False
        except Exception as e:
            self.log.error("Exception occurred in select_timeframe Method and Exception is : " + str(e))

    def wait_for_cutomer_board_display(self):
        self.wait_for_element_visible(customer_board_loader, timeout=15)
        self.wait_for_element_not_visible(customer_board_loader, timeout=20)

    def click_customer_view_button(self, customerid=""):
        """Clicking Customer view button based on the searched customer id """
        try:
            if customerid != "":
                view_button = (By.XPATH, customer_view_button + customerid + "_customerBk']")
            else:
                # Clicking the first index
                view_button = first_view_button_load

            self.wait_for_element_visible(first_view_button_load)
            self.log.info("View button -- > " + str(view_button))
            self.element_click(self.get_element(view_button))

        except Exception as e:
            self.log.error("Exception occurred in click_customer_view_button and Exception is : " + str(e))

    def verify_customer_board_display(self):
        """Validate customer board is displayed or not and return True if displayed else False"""
        try:
            self.log.info("Inside verify_customer_board_display method")
            if self.is_element_displayed(cm_board_header):
                return True
            else:
                return False
        except Exception as e:
            self.log.error("Exception occurred in verify_customer_board_display and Exception is : " + str(e))

    def verify_customer_board_columns(self):
        try:
            self.log.info("Inside verify_customer_board_columns method")
            _customer_board_headers_list = self.csvRowDataToList("cust_columns_headers", "defaultlist.csv")
            headers_list = self.get_element_list(customer_board_headers)
            self.log.info("Length of headers list : " + str(len(headers_list)))
            commonflag = True
            for i in range(0, len(headers_list)):
                self.driver.execute_script("arguments[0].scrollIntoView();", headers_list[i])
                if _customer_board_headers_list[i].strip().lower() == headers_list[i].text.lower():
                    commonflag = commonflag and True
                else:
                    self.log.error("Customer Board Column Headers are not matching : " + str(headers_list[i].text))
                    commonflag = commonflag and False

            return commonflag
        except Exception as e:
            self.log.error("Exception occurred in verify_customer_board_columns and Exception is : " + str(e))
