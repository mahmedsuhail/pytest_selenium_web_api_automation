from selenium.webdriver.common.by import By

from pages.basepage import BasePage

customer_profile_loader = (By.XPATH, "//div[text()='Loading']")
customer_board_button = (By.XPATH, "//span[text()='Customer Board']")
customer_account_info_labels = (By.XPATH, "//div[contains(@class,'informationstyle__InfoWrapper')]//label")
customer_account_info_view_history_button = (By.XPATH, "//div[contains(@class,'informationstyle__ButtonWrapper')]"
                                                       "//span[text()='View History']")
customer_profile_view_or_manage_users_button = (By.XPATH, "//span[text()='View/Manage Users']")
customer_user_mmt_add_user_button = (By.ID, "header_sm-281_addUserButton")
customer_user_mmt_board_filter = (By.XPATH, "//div[contains(@class,'ag-header-cell-label')]")


class CustomerProfileCommonPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def wait_for_cutomer_profile_display(self):
        try:
            self.wait_for_element(customer_profile_loader, event="display")
            self.wait_for_element(customer_profile_loader, event="notdisplay", timeout=15)
            self.wait_for_element_visible(customer_board_button)
            if self.is_element_displayed(customer_board_button):
                self.log.info("Yes! Customer profile is displayed")
                return True
            else:
                self.log.debug("Customer Profile is not displayed")
                return False
        except Exception as e:
            self.log.error(
                "Exception occurred in wait_for_cutomer_profile_display Method and Exception is : " + str(e))

    def verify_account_information_labels(self):
        try:
            self.log.info("Inside verify_account_information_labels method")
            account_information_list = ["Company Name", "City/State Zip", "Company Legal Name",
                                        "Contact Name", "Billing Address", "Contact Email", "Phone"]
            account_info_labels = self.get_element_list(customer_account_info_labels)
            self.log.info("Length of headers list : " + str(len(account_info_labels)))
            commonflag = True
            for i in range(0, len(account_info_labels)):
                self.driver.execute_script("arguments[0].scrollIntoView();", account_info_labels[i])
                if account_information_list[i].strip().lower() == account_info_labels[i].text.lower():
                    commonflag = commonflag and True
                else:
                    self.log.error(
                        "Account Information headers are not matching : " + str(account_info_labels[i].text))
                    commonflag = commonflag and False

            return commonflag
        except Exception as e:
            self.log.error(
                "Exception occurred in verify_account_information_labels Method and Exception is : " + str(e))

    def click_customer_board_button_from_profile(self):
        try:
            self.log.info("Inside click_customer_board_button_from_profile")
            self.element_click(self.get_element(customer_board_button))

        except Exception as e:
            self.log.error(
                "Exception occurred in click_customer_board_button_from_profile Method and Exception is : " + str(e))

    def click_view_or_manage_users_button_from_customer_profile(self):
        try:
            self.log.info("Inside click_customer_board_button_from_profile")
            self.page_vertical_scroll(locator=customer_profile_view_or_manage_users_button)
            self.element_click(self.get_element(customer_profile_view_or_manage_users_button))
        except Exception as e:
            self.log.error(
                "Exception occurred in click_view_or_manage_users_button_from_customer_profile Method and "
                "Exception is : " + str(e))

    def add_user_button_is_displayed(self):
        return self.is_element_present(locator=customer_user_mmt_add_user_button) and \
            self.is_element_displayed(locator=customer_user_mmt_add_user_button)

    def default_customer_user_management_page_validations(self):
        common_flag = True
        common_flag = common_flag and self.add_user_button_is_displayed
        user_management_filter_list = ["View Details", "Customer ID", "Contact Name", "GTZ Ship Username",
                                       "GTZ Ship Email Address"]
        user_management_filter = self.get_element_list(customer_user_mmt_board_filter)
        self.log.info("Length of Filters list : " + str(len(user_management_filter)))
        for i in range(0, len(user_management_filter)):
            self.driver.execute_script("arguments[0].scrollIntoView();", user_management_filter[i])
            if user_management_filter_list[i].strip().lower() == user_management_filter[i].text.lower():
                common_flag = common_flag and True
            else:
                self.log.error(
                    "User Management Filters are not matching : " + str(user_management_filter[i].text))
                common_flag = common_flag and False

        self.log.info("Final Common Flag Status " + str(common_flag))
        return common_flag
