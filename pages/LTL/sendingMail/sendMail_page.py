from pages.basepage import BasePage


class SendMailPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.driver = driver
        self.log = log

    # ----------------------Locators--------------------------------------------------------
    _user_name = "username"
    _password = "password"
    _button_login = "//button[text()='Log in']"
    _server_name = "api Tests"
    _checkBox = "(//input[@type='checkbox'])["
    _delete_button = "//button[@data-qa='trash']"
    _back_button = "button[data-qa='back']"
    _mail_loader = "//div [@delay='0']"
    _mail_subject = "//div[@data-evergreen-table-body='true']//div/div[2] //a[@class='link-cell']/span[1]"
    _mail_refresh = "//button[@data-qa='refresh']"
    _mail_attachment_view = "//span[text()='Attachments']"
    _mail_attach_doc_link = "QuoteRequest.pdf"
    _mail_delete_confirmation_popup = "//div[@role='dialog' and @data-state='entered']"
    _mail_confirmation_delete_button = "//button[text()='Delete Messages']"

    _no_email_view = "//button[text()='Create a sample email']"

    # _user_name_value = "yogabhaskar.p@globaltranz.com"
    # _password_value = "Tejesh@4091"
    # -------------------------------------------Methods--------------------------------------------------

    def openMailPage(self, url="https://mailosaur.com/app/login"):
        """
        OPen Mail Page
        :param url: pass the url have to open
        """
        try:
            self.log.info("Inside openMailPage Method")
            self.new_tab(url)
        except Exception as e:
            self.log.error("Exception occurred in openMailPage Method and Exception is : " + str(e))

    def loginMailServer(self, _user_name_value="yogabhaskar.p@globaltranz.com", _password_value="Tejesh@4091"):
        """Login to the Mail Sure Application"""
        try:
            self.log.info("Inside loginMailServer Method")
            self.sendKeys(_user_name_value, self._user_name, "name")
            self.sendKeys(_password_value, self._password, "name")
            self.elementClick(self._button_login, "xpath")
            self.waitForElement(self._server_name, "link")
            self.select_server()
        except Exception as e:
            self.log.error("Exception occurred in loginMailServer Method and Exception is : " + str(e))

    def selectSeverAndDeleteMail(self, checkBoxCount):
        """Selecting server link in Mail Sure Page"""
        try:
            self.log.info("Inside selectSeverAndDeleteMail Method")
            if self.isElementDisplayed(self._server_name, "link"):
                self.select_server()
            if not self.isElementPresent(self._no_email_view, "xpath"):
                self.waitForElement(self._mail_loader, "xpath", "notdisplay")
                self.selectCheckBox(index=checkBoxCount)
                self.deleteMail()
            else:
                self.log.info("No email Present")

        except Exception as e:
            self.log.error("Exception occurred in selectSeverLink Method and Exception is : " + str(e))

    def select_server(self):
        try:
            self.log.info("Inside select_server Method")
            self.elementClick(self._server_name, "link")
            self.waitForElement(self._delete_button, "xpath", "display")
        except Exception as e:
            self.log.error("Exception occurred in select_server Method and Exception is : " + str(e))

    def selectCheckBox(self, index="all"):
        """
        Selecting checkBox based on passed argument
        """
        try:
            self.log.info("Inside selectCheckBox Method")
            if index.lower() == "all":
                self.elementClick(self._checkBox + str(1) + "]/following-sibling::div", "xpath")
            else:
                index = int(index) + 1
                self.elementClick(self._checkBox + str(index) + "]/following-sibling::div", "xpath")
        except Exception as e:
            self.log.error("Exception occurred in selectCheckBox Method and Exception is : " + str(e))

    def deleteMail(self):
        """deleting the mail"""
        try:
            self.log.info("Inside deleteMail Method")
            self.elementClick(self._delete_button, "xpath")
            self.waitForElement(self._mail_delete_confirmation_popup, "xpath", "display")
            self.elementClick(self._mail_confirmation_delete_button, "xpath")
            self.waitForElement(self._mail_delete_confirmation_popup, "xpath", "notdisplay")
            self.waitForElement(self._mail_loader, "xpath", "notdisplay")
        except Exception as e:
            self.log.error("Exception occurred in deleteMail Method and Exception is : " + str(e))

    def searchAndSelectEmail(self, searchText):
        """search and Select Email Method"""
        try:
            self.log.info("Inside searchAndSelectEmail Method")
            returnText = self.selectBasedOnText(self._mail_subject, "xpath", searchText)
            if returnText == searchText:
                self.log.debug("Text Found and clicked")
                return True
            else:
                self.log.debug("Text not found")
                return False
        except Exception as e:
            self.log.error("Exception occurred in searchAndSelectEmail Method and Exception is : " + str(e))

    def verifyRateQuoteAttachment(self):
        """Verify Attachment in Email"""
        try:
            self.log.info("Inside verifyRateQuoteAttachment Method")
            self.waitForElement(self._mail_attachment_view, "xpath", event="display")
            self.pageVerticalScroll(self._mail_attachment_view, "xpath")
            self.elementClick(self._mail_attachment_view, "xpath")
            getAttachedValue = self.isElementDisplayed(self._mail_attach_doc_link, "link")
            self.log.info("Getting Value : " + str(getAttachedValue))

            if getAttachedValue:
                self.log.info("Attachment Found")
                return True
            else:
                self.log.error("Attachment not found")
                return False
        except Exception as e:
            self.log.error("Exception occurred in verifyRateQuoteAttachment Method and Exception is : " + str(e))

    def emailPageRefresh(self):
        try:
            self.log.info("Inside emailPageRefresh Method")
            if self.isElementPresent(self._no_email_view, "xpath"):
                self.refresh_page()
                self.waitForElement(self._mail_loader, "xpath", "display", timeout=15)
                self.waitForElement(self._mail_loader, "xpath", "notdisplay")
                if self.isElementPresent(self._no_email_view, "xpath"):
                    self.refresh_page()
                    self.waitForElement(self._mail_loader, "xpath", "display", timeout=15)
                    self.waitForElement(self._mail_loader, "xpath", "notdisplay")
                elif self.isElementPresent(self._back_button, "css"):
                    self.elementClick(self._back_button, "css")
                    self.elementClick(self._mail_refresh, "xpath")
                    self.waitForElement(self._mail_loader, "xpath", "display")
                    self.waitForElement(self._mail_loader, "xpath", "notdisplay")
                else:
                    self.elementClick(self._mail_refresh, "xpath")
                    self.waitForElement(self._mail_loader, "xpath", "display")
                    self.waitForElement(self._mail_loader, "xpath", "notdisplay")
            else:
                if self.isElementPresent(self._mail_refresh, "xpath"):
                    self.elementClick(self._mail_refresh, "xpath")
                    self.waitForElement(self._mail_loader, "xpath", "display")
                    self.waitForElement(self._mail_loader, "xpath", "notdisplay")
                elif self.isElementPresent(self._back_button, "css"):
                    self.elementClick(self._back_button, "css")
                    self.elementClick(self._mail_refresh, "xpath")
                    self.waitForElement(self._mail_loader, "xpath", "display")
                    self.waitForElement(self._mail_loader, "xpath", "notdisplay")
        except Exception as e:
            self.log.error("Exception occurred in emailPageRefresh Method and exception is : " + str(e))
