import ast
import string

from pages.basepage import BasePage


class SalesorderFinancialDetailsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # ------- Locators --------------
    _financial_section = "div[id='financials_LTL1-2192_cardBody'] div[id*='financials_LTL1-2192_']:nth-child"
    _financial_section_rows = "div[id='financials_LTL1-2192_cardBody'] div[id*='financials_LTL1-2192_']"
    _shippingitems = "div[id*='financials_LTL1-2192_lineItemsRow"
    _cardbody = "//div[@id='financials_LTL1-2192_cardBody']"
    _totalcost = "financials_LTL1-1396_totalCost"
    _totalrevenue = "financials_LTL1-1396_totalRevenue"
    _totalweight = "financials_LTL1-1396_totalWeight"
    itemindex = "1"
    itemdescindex = "2"
    costindex = "3"
    revenueindex = "4"
    nmfcindex = "5"
    classindex = "6"
    weightindex = "7"
    unittypeindex = "8"
    unitsindex = "9"
    pcsindex = "10"
    lengthindex = "11"
    widthindex = "12"
    heightindex = "13"
    hazmathstatusindex = "14"
    prefixindex = "15"
    hazmathcodeindex = "16"
    packaginggroupindex = "17"
    hazmathclassindex = "18"

    _financialLineItemParent = "financials_LTL1-2192_lineItemsRow"
    _financialEditButton = "financials_LTL1-2192_buttonBeginEdit"
    _bolStatus = "statuses_LTL1-1395_statusValue5"
    _cancelFinancialDetailsButton = "financials_LTL1-2192_buttonCancelEdits"
    _saveFinancialDetailsButton = "financials_LTL1-2192_buttonSaveEdits"
    _modalPopup = "//div[contains(@id, 'financials_LTL1-1600_modal')]"
    _modalConfirmButton = "//div[contains(@id, 'financials_LTL1-1600_modal')]//button"
    _cancelConfirmButton = "//div[contains(@id, 'financials_LTL1-1600_modal')]//button[2]"
    # _warningModalPopup = "financials_LTL1-1600_modalBadMargin"
    # _warningConfirmButton = "financials_LTL1-1600_modalBadMarginButtonOk"
    # _successModalPopup = "financials_LTL1-1600_modalSuccess"
    # _successConfirmButton = "financials_LTL1-1600_modalSuccessButtonOk"
    # _cancelModalPopup = "financials_LTL1-1600_modalUnsaved"
    # _cancelConfirmButton = "financials_LTL1-1600_modalButtonConfirm"
    _DisplayedMsg = "//div[contains(@id, 'financials_LTL1-1600_modal')]//div[contains(@class,'TextContainer')]"
    # _successMsg = "//div[@id='financials_LTL1-1600_modalSuccess']//div[contains(@class,'TextContainer')]"
    # _cancelMsg = "//div[@id='financials_LTL1-1600_modalUnsaved']//div[contains(@class,'TextContainer')]"
    expectedWarningMsg = "Current Revenue cannot be accepted as it is resulting in negative margin for the shipment."
    expectedSuccessMsg = "Changes saved successfully."
    # expectedCancelMsg = "Changes have been made, do you wish to abandon these changes?"
    expectedCancelMsg = "Are you sure you want to start over? All current information will be cleared."

    def validateCarrierBreakup(self, carrierbreakuplist):
        self.log.info("Inside validateCarrierBreakup method")
        try:
            if type(carrierbreakuplist) is list:
                self.log.info("Provided breakup already in list")
            else:
                carrierbreakuplist = ast.literal_eval(carrierbreakuplist)

            passedcarrieritems = carrierbreakuplist[0]
            passedcostlist = carrierbreakuplist[1]
            passedrevenuelist = carrierbreakuplist[2]

            totalrows = self.getElementList(self._financial_section_rows, "css")
            self.log.info("Total rows : " + str(len(totalrows)))

            carrieritemslist = []
            itemscostlist = []
            itemsrevenuelist = []
            totalcost = 0
            totalrevenue = 0

            for i in range(0, len(totalrows)):
                carrieritem = self.getText(
                    self._financial_section + "(" + str(int(i + 1)) + ")>div:nth-child(" + self.itemindex + ")", "css")

                itemcost = self.getText(
                    self._financial_section + "(" + str(int(i + 1)) + ")>div:nth-child(" + self.costindex + ")",
                    "css").replace("(", "-").replace(")", "").replace("$", "")

                itemrevenue = self.getText(
                    self._financial_section + "(" + str(int(i + 1)) + ")>div:nth-child(" + self.revenueindex + ")",
                    "css").replace("(", "-").replace(")", "").replace("$", "")

                if carrieritem.lower() == "shipping service" and itemcost != "0.00" and itemrevenue != "0.00":
                    itemscostlist.append(itemcost)
                    itemsrevenuelist.append(itemrevenue)
                    carrieritemslist.append(carrieritem.lower())
                elif carrieritem.lower() != "shipping service":
                    if itemcost == "0":
                        itemcost = "0.00"
                    elif itemrevenue == "0":
                        itemrevenue = "0.00"
                    itemscostlist.append(itemcost)
                    itemsrevenuelist.append(itemrevenue)
                    carrieritemslist.append(carrieritem.lower())

                totalcost = float(totalcost) + float(itemcost)
                totalrevenue = float(totalrevenue) + float(itemrevenue)

            self.log.info("Total cost : " + str(totalcost))
            self.log.info("Total revenue  : " + str(totalrevenue))

            for i in range(0, len(passedcarrieritems)):
                if passedcarrieritems[i].lower() == "initial cost":
                    passedcarrieritems[i] = "shipping service"
                elif passedcarrieritems[i].lower() == "tradeshow delivery":
                    passedcarrieritems[i] = "tradeshow fee"
                elif passedcarrieritems[i].lower() == "notify prior to arrival":
                    passedcarrieritems[i] = "notification prior to arrival"
                else:
                    passedcarrieritems[i] = passedcarrieritems[i].lower()

            commonflag = False
            setflag = False

            # Validating total cost calculated with displayed total cost in application
            if float(round(totalcost, 2)) == float(self.getText(self._totalcost, "id").replace("$", "")):
                self.log.info("Total cost from application validation result : True")
                commonflag = True
            else:
                self.log.error("Total cost from application validation result : False, Expected cost : " + str(
                    float(round(totalcost, 2))) + "Actual : " + str(
                    float(self.getText(self._totalcost, "id").replace("$", ""))))
                commonflag = False

            # Validating total revenue calculated with displayed total revenue in application
            if float(round(totalrevenue, 2)) == float(self.getText(self._totalrevenue, "id").replace("$", "")):
                self.log.info("Total revenue from application validation result : True")
                commonflag = True
            else:
                self.log.error("Total revenue from application validation result : False, Expected Revenue : " + str(
                    float(round(totalrevenue, 2))) + "Actual : " + str(
                    float(self.getText(self._totalrevenue, "id").replace("$", ""))))
                commonflag = False

            # Preparing dictionary for items - revenue from application
            actualcarrieritems_revenue_dict = dict(zip(carrieritemslist, itemsrevenuelist))
            # Preparing dictionary for items - revenue from csv
            expectedcarrieritems_revenue_dict = dict(zip(passedcarrieritems, passedrevenuelist))

            # Preparing dictionary for items - cost from application
            actualcarrieritems_cost_dict = dict(zip(carrieritemslist, itemscostlist))
            # Preparing dictionary for items - cost from csv
            expectedcarrieritems_cost_dict = dict(zip(passedcarrieritems, passedcostlist))

            # Removing Fuel Surcharge from cost list in order to compare cost list from csv and application
            actualcarrieritems_cost_dict.pop('fuel surcharge')
            expectedcarrieritems_cost_dict.pop('fuel surcharge')

            self.log.info("Revenue dict from application :" + str(actualcarrieritems_revenue_dict))
            self.log.info("Revenue dict from csv :" + str(expectedcarrieritems_revenue_dict))

            self.log.info("Cost dict from application excluding fule surcharge : " + str(actualcarrieritems_cost_dict))
            self.log.info("Cost dict from csv excluding fule surcharge : " + str(expectedcarrieritems_cost_dict))

            if actualcarrieritems_revenue_dict == expectedcarrieritems_revenue_dict:
                setflag = True
            else:
                self.log.error("Revenue Dictonaries from application and csv are not equal : False Expected : " + str(
                    expectedcarrieritems_revenue_dict) + " Actual : " + str(actualcarrieritems_revenue_dict))
                setflag = False

            if actualcarrieritems_cost_dict == expectedcarrieritems_cost_dict:
                setflag = True
            else:
                self.log.error("Cost Dictonaries from application and csv are not equal : False Expected : " + str(
                    expectedcarrieritems_cost_dict) + " Actual : " + str(actualcarrieritems_cost_dict))
                setflag = False

            if commonflag and setflag:
                return True
            else:
                self.log.error("Inside else : False" + str(commonflag) + str(setflag))
                return False
        except Exception as e:
            self.log.error("Exception occurred in validateCarrierBreakup Method and exception is : " + str(e))

    def validateTotalValue(self, passedValue, field=""):
        self.log.info("Inside validateTotalValue method")
        try:

            # Validating passed total revenue from csv with displayed total revenue in application
            if field.lower() == "revenue":
                if round(float(passedValue), 2) == float(self.getText(self._totalrevenue, "id").replace("$", "")):
                    commonflag = True
                else:
                    self.log.error("Compared revenue value with passed revenue and total revenue from application : "
                                   "False, Expected passed Revenue : " + str(round(float(passedValue), 2)) + "Actual : " +
                                   str(float(self.getText(self._totalrevenue, "id").replace("$", ""))))
                    commonflag = False
            elif field.lower() == "cost":
                if round(float(passedValue), 2) == float(self.getText(self._totalcost, "id").replace("$", "")):
                    commonflag = True
                else:
                    self.log.error("Compared cost value with passed cost and total cost from application : "
                                   "False, Expected passed Cost : " + str(round(float(passedValue), 2)) + "Actual : " +
                                   str(float(self.getText(self._totalcost, "id").replace("$", ""))))
                    commonflag = False

            else:
                self.log.error("Either field name or locators used are incorrect")
                commonflag = False

            return commonflag

        except Exception as e:
            self.log.error("Exception occurred in validateTotalValue Method and exception is : " + str(e))

    def validateShippingItems(self, commodity, nmfc, className, weight, weightUnits, unitType,
                              unitCount, pieces, length, width, height, dim, hazmatStatus="",
                              prefix="", hazmatCode="", hazmatGroup="", hazmatClass=""):
        """Under this method, we are validating shipping items in SO Page under Financial section and returning True
        if passed values and actual values are matching """
        self.log.info("Inside validateShippingItems method")
        try:
            _shippingitems_rowno = self.getAttribute(self._cardbody + "//div[text()='" + commodity + "']/parent::div",
                                                     "xpath", "id")
            self.log.info(str(_shippingitems_rowno) + " row items")

            setflag = True
            # length = int(float(length))
            # width = int(float(width))
            # height = int(float(height))
            # weight = int(float(weight))
            # unitCount = int(float(unitCount))
            # pieces = int(float(pieces))
            length = length.replace(".", "")
            width = width.replace(".", "")
            height = height.replace(".", "")
            # weight = weight.replace(".", "")
            # unitCount = unitCount.replace(".", "")
            # pieces = pieces.replace(".", "")
            weight = round(float(weight))
            unitCount = round(float(unitCount))
            pieces = round(float(pieces))

            if unitType.lower() == "pallets(40x48)":
                length = "40"
                width = "48"
            if weightUnits.lower() == "kg":
                weight = round(int(float(weight)) * 2.20462)
            if dim.lower() == "feet":
                length = int(float(length)) * 12
                width = int(float(width)) * 12
                height = int(float(height)) * 12

            _getdescription = self.getText(
                "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.itemdescindex + ")", "css")
            setflag = setflag and self.validators.verify_text_match(commodity.lower(), _getdescription.lower())
            self.log.debug("setflag after validating description " + str(setflag))

            _getNmfc = self.getText("div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.nmfcindex + ")",
                                    "css")
            setflag = setflag and self.validators.verify_text_match(nmfc.replace(".", ""), _getNmfc)
            self.log.debug("setflag after validating NMFC " + str(setflag))

            _getClassValue = self.getText(
                "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.classindex + ")",
                "css")
            setflag = setflag and self.validators.verify_text_match(className, _getClassValue)
            self.log.debug("setflag after validating Class value " + str(setflag))

            _getWeightValue = self.getText(
                "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.weightindex + ")",
                "css")

            setflag = setflag and self.validators.verify_text_match(str(weight), _getWeightValue)
            self.log.debug("setflag after validating Weight Value " + str(setflag))

            _getUnitType = self.getText(
                "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.unittypeindex + ")",
                "css")
            setflag = setflag and self.validators.verify_text_match(unitType.lower(), _getUnitType.lower())
            self.log.debug("setflag after validating Unit Type " + str(setflag))

            _getUnitCount = self.getText(
                "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.unitsindex + ")",
                "css")

            setflag = setflag and self.validators.verify_text_match(unitCount, _getUnitCount)
            self.log.debug("setflag after validating unitCount " + str(setflag))

            _getPieces = self.getText("div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.pcsindex + ")",
                                      "css")

            setflag = setflag and self.validators.verify_text_match(pieces, _getPieces)
            self.log.debug("setflag after validating Peices " + str(setflag))

            _getLength = self.getText("div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.lengthindex + ")",
                                      "css")

            setflag = setflag and self.validators.verify_text_match(str(length), _getLength)
            self.log.debug("setflag after validating Length " + str(setflag))

            _getWidth = self.getText("div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.widthindex + ")",
                                     "css")

            setflag = setflag and self.validators.verify_text_match(str(width), _getWidth)
            self.log.debug("setflag after validating Width " + str(setflag))

            _getHeight = self.getText("div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.heightindex + ")",
                                      "css")
            setflag = setflag and self.validators.verify_text_match(str(height), _getHeight)
            self.log.debug("setflag after validating Height " + str(setflag))

            _hazmathstatuslist = self.getElementList(
                "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.hazmathstatusindex + ")>label>div>*",
                "css")
            if hazmatStatus.lower() == "yes" and len(_hazmathstatuslist) > 0:
                _getHazmathStatus = "yes"
            else:
                _getHazmathStatus = "no"

            setflag = setflag and self.validators.verify_text_match(hazmatStatus.lower(), _getHazmathStatus.lower())
            self.log.debug("setflag after validating Hazmath Status " + str(setflag))

            if hazmatStatus.lower() == "yes":
                _getPrefix = self.getText(
                    "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.prefixindex + ")",
                    "css")
                setflag = setflag and self.validators.verify_text_match(prefix, _getPrefix)
                self.log.debug("setflag after validating Prefix " + str(setflag))

                _getHazmathCode = self.getText(
                    "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.hazmathcodeindex + ")",
                    "css")
                setflag = setflag and self.validators.verify_text_match(hazmatCode, _getHazmathCode)
                self.log.debug("setflag after validating Hazmath code " + str(setflag))

                _getPackingGroup = self.getText(
                    "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.packaginggroupindex + ")",
                    "css")
                setflag = setflag and self.validators.verify_text_match(hazmatGroup, _getPackingGroup)
                self.log.debug("setflag after validating Hazmath/packaging Group " + str(setflag))

                _getHazmathClass = self.getText(
                    "div[id='" + _shippingitems_rowno + "']>div:nth-child(" + self.hazmathclassindex + ")",
                    "css")
                setflag = setflag and self.validators.verify_text_match(hazmatClass.lower(), _getHazmathClass.lower())
                self.log.debug("setflag after validating HazmathClass " + str(setflag))

            self.log.info(
                "Final flag value after validating shipping items in validateShippingItems method: " + str(setflag))
            return setflag

        except Exception as e:
            self.log.error("Exception occurred in validateShippingItems Method and exception is : " + str(e))

    # Edit Financial Section
    def checkBolStatusAndClickEditButton(self):
        self.log.info("Inside checkBolStatusAndClickEditButton method")
        try:
            statusresult = self.getText(self._bolStatus, "id")
            if statusresult.lower() == "pending" and self.isElementDisplayed(self._financialEditButton, "id"):
                self.pageVerticalScroll(self._financialEditButton, "id")
                self.elementClick(self._financialEditButton, "id")
                res = True
            else:
                res = False
            return res
        except Exception as E:
            self.log.error("Exception occurred in checkBolStatusAndClickEditButton Method and exception is : " + str(E))
            return False

    def defaultFinancialSectionValidation(self):
        self.log.info("Inside defaultFinancialSectionValidation method")

        try:
            validatedWarningMsg = False
            validatedSuccessMsg = False

            validatedCancelMsg = self.validateFinancialSectionOnCancelChanges("shipping service", "cost", "1000")
            self.log.info("validatedCancelRes : " + str(validatedCancelMsg))
            self.elementClick(self._financialEditButton, "id")
            totalcost = self.getTotalValue("cost")
            totalrev = self.getTotalValue("rev")
            diffvalue = round(totalrev - totalcost)
            self.log.info("diffvalue : " + str(diffvalue))
            # Locator preparation for child id in order to pass data
            childid = self._financialLineItemParent + str(0) + "inputcost"
            self.log.info("Child id  : " + str(childid))
            valueBeforeEditing = self.getAttribute(childid, "id", "value")
            self.log.info("valueBeforeEditing : " + str(valueBeforeEditing) + " ,and difference Value : " + str(diffvalue))
            value = diffvalue + float(valueBeforeEditing)
            self.log.info("Calculated Considered Value : " + str(value))
            self.log.info("For Warning Message validation passed Value is : " + str(round(value + 1)))
            editedResult = self.editFinancialSection("shipping service", "cost", round(value + 1))
            if editedResult:
                self.elementClick(self._saveFinancialDetailsButton, "id")
                # self.waitForElement(self._warningModalPopup, "id", timeout=15)
                self.waitForElement(self._modalPopup, "xpath", timeout=15)
                if self.isElementDisplayed(self._modalPopup, "xpath"):
                    warningmsg = self.getText(self._DisplayedMsg, "xpath")
                    validatedWarningMsg = self.validators.verify_text_match(warningmsg, self.expectedWarningMsg)
                    self.log.info("validatedWarningMsg : " + str(validatedWarningMsg))
                    # self.elementClick(self._warningConfirmButton, "id")
                    self.elementClick(self._modalConfirmButton, "xpath")
                else:
                    self.log.error("Warning Popup didn't appeared")
                    self.elementClick(self._modalConfirmButton, "xpath")
            else:
                self.log.error("Cannot find passed line item")
                return False

            self.log.info("For Success Msg validation passed Value is : " + str(round(value - 1)))
            # Bug - Refreshing page in order to get proper response
            self.refresh_page()
            self.waitForElement(self._financialEditButton, "id", event="display", timeout=20)
            self.elementClick(self._financialEditButton, "id")
            editedResult1 = self.editFinancialSection("shipping service", "cost", round(value - 1))
            if editedResult1:
                self.elementClick(self._saveFinancialDetailsButton, "id")
                # self.waitForElement(self._successModalPopup, "id", timeout=15)
                self.waitForElement(self._modalPopup, "xpath", timeout=15)
                if self.isElementDisplayed(self._modalPopup, "xpath"):
                    successmsg = self.getText(self._DisplayedMsg, "xpath")
                    validatedSuccessMsg = self.validators.verify_text_match(successmsg, self.expectedSuccessMsg)
                    self.log.info("validatedsuccessmsg : ", str(validatedSuccessMsg))
                    # self.elementClick(self._successConfirmButton, "id")
                    self.elementClick(self._modalConfirmButton, "xpath")

                else:
                    self.log.error("Success Popup didn't appeared")
                    self.elementClick(self._modalConfirmButton, "xpath")
            else:
                self.log.error("Cannot find passed line item")
                return False
            self.log.debug("validatedCancelMsg, Expected : True, Actual : " + str(validatedCancelMsg) +
                           " validatedWarningMsg, Expected : True, Actual : " + str(validatedWarningMsg) +
                           " validatedSuccessMsg, Expected : True, Actual : " + str(validatedSuccessMsg))
            if validatedCancelMsg and validatedWarningMsg and validatedSuccessMsg:
                return True
            else:
                self.log.error("validatedCancelMsg, Expected : True, Actual : " + str(validatedCancelMsg) +
                               " validatedWarningMsg, Expected : True, Actual : " + str(validatedWarningMsg) +
                               " validatedSuccessMsg, Expected : True, Actual : " + str(validatedSuccessMsg))
                return False

        except Exception as E:
            self.log.error(
                "Exception occurred in defaultFinancialSectionValidation Method and exception is : " + str(E))
            return False

    def validateTotalResult(self, field):
        self.log.info("Inside validateTotalResult method")
        try:
            totalrows = self.getElementList(self._financial_section_rows, "css")
            self.log.info("Total rows : " + str(len(totalrows)))
            totalcost = 0
            totalrevenue = 0

            for i in range(0, len(totalrows)):
                itemcost = self.getText(
                    self._financial_section + "(" + str(int(i + 1)) + ")>div:nth-child(" + self.costindex + ")",
                    "css").replace("(", "-").replace(")", "").replace("$", "")

                itemrevenue = self.getText(
                    self._financial_section + "(" + str(int(i + 1)) + ")>div:nth-child(" + self.revenueindex + ")",
                    "css").replace("(", "-").replace(")", "").replace("$", "")

                totalcost = float(totalcost) + float(itemcost)
                totalrevenue = float(totalrevenue) + float(itemrevenue)

            self.log.info("Total cost : " + str(totalcost))
            self.log.info("Total revenue : " + str(totalrevenue))

            if field.lower() == "cost":
                if float(round(totalcost, 2)) == self.getTotalValue(field):
                    self.log.info("Total cost calculated is True")
                    return True
                else:
                    self.log.error("Total cost from application validation result : False, Expected cost : " + str(
                        float(round(totalcost, 2))) + "Actual : " + str(self.getTotalValue(field)))
                    return False
            elif field.lower() == "rev":
                if float(round(totalrevenue, 2)) == self.getTotalValue(field):
                    self.log.info("Total revenue from application validation result : True")
                    return True
                else:
                    self.log.error(
                        "Total revenue from application validation result : False, Expected Revenue : " + str(
                            float(round(totalrevenue, 2))) + "Actual : " + str(self.getTotalValue(field)))
                    return False
        except Exception as E:
            self.log.error("Exception occurred in validateTotalResult Method and exception is : " + str(E))
            return False

    def editFinancialSection(self, lineitem, field, value, rownum=0):
        self.log.info("Inside EditFinancialSection method")
        try:

            totalcost = self.getTotalValue("cost")
            totalrev = self.getTotalValue("rev")
            diffvalue = round(totalrev - totalcost)
            self.log.info("Before Editing Value - totalcost : " + str(totalcost))
            self.log.info("Before Editing value  - totalrev : " + str(totalrev))
            self.log.info("Before Editing Value - diffvalue : " + str(diffvalue))
            if lineitem.lower() == "shipping service":
                parentid = self._financialLineItemParent + str(rownum)
                self.log.info("Parent id : " + str(parentid))
            else:
                lineitem = string.capwords(lineitem)
                parentid = self.getAttribute("//div[text()='" + lineitem + "']/parent::div", "xpath", "id")

            self.log.info("Parent id : " + str(parentid))
            if parentid is not None:

                childid = ""
                if field.lower() == "cost":
                    childid = parentid + "input" + field.lower()
                elif field.lower() == "rev":
                    index = "rev"
                    childid = parentid + "input" + index
                valueBeforeEditing = self.getAttribute(childid, "id", "value")
                self.log.info("valueBeforeEditing : " + str(valueBeforeEditing))
                self.sendKeys(value, childid, "id")
                valueAfterEditing = self.getAttribute(childid, "id", "value")
                self.log.info("valueAfterEditing : " + str(valueAfterEditing))
                totalcost = self.getTotalValue("cost")
                totalrev = self.getTotalValue("rev")
                diffvalue = round(totalrev - totalcost)
                self.log.info("After Editing Value - totalcost : " + str(totalcost))
                self.log.info("After Editing value  - totalrev : " + str(totalrev))
                self.log.info("After Editing Value - diffvalue : " + str(diffvalue))
                return True
            else:
                self.log.error("Cannot find passed line item " + str(lineitem))
                return False
        except Exception as E:
            self.log.error("Exception occurred in editFinancialSection Method and exception is : " + str(E))
            return False

    def validateFinancialEditSectionOnSavingChanges(self, lineitem, field, value, rowno=0):
        self.log.info("Inside validateFinancialEditSectionOnSavingChanges method" + " lineitem : " +
                      str(lineitem) + " field : " + str(field))
        try:
            validatedStatusResult = self.checkBolStatusAndClickEditButton()

            if validatedStatusResult:
                editedValueRes = self.editFinancialSection(lineitem, field, value, rowno)
                if editedValueRes:
                    self.elementClick(self._saveFinancialDetailsButton, "id")
                    # self.waitForElement(self._successModalPopup, "id", timeout=25)
                    self.waitForElement(self._modalPopup, "xpath", timeout=25)
                    if self.isElementDisplayed(self._modalPopup, "xpath"):
                        successMsg = self.getText(self._DisplayedMsg, "xpath")
                        validatedsuccessmsg = self.validators.verify_text_match(successMsg, self.expectedSuccessMsg)
                        # self.elementClick(self._successConfirmButton, "id")
                        self.elementClick(self._modalConfirmButton, "xpath")
                        self.refresh_page()
                        self.waitForElement(self._financialEditButton, "id", event="display", timeout=20)
                        validateTotalRes = self.validateTotalResult(field)
                        self.log.info("validateTotalRes : " + str(validateTotalRes))

                        if validatedsuccessmsg and validateTotalRes:
                            return True
                        else:
                            self.log.error(f"Either success message in not displayed as expected or total result on "
                                           f"saving changes is not matching {str(validatedsuccessmsg)} "
                                           f"{str(validateTotalRes)}")
                            return False
                    else:
                        self.log.error("Success model didn't appear on clicking save button")
                        return False
                else:
                    self.log.error("Cannot find passed line item " + str(lineitem))
                    return False
            else:
                self.log.error("BOL is not in pending status, hence details cannot be editable")
                return False

        except Exception as E:
            self.log.error("Exception occurred in validateFinancialEditSectionOnSavingChanges Method and exception is "
                           ": " + str(E))
            return False

    def validateFinancialSectionOnCancelChanges(self, lineitem, field, value, rowno=0):
        self.log.info("Inside validateFinancialSectionOnCancelChanges method")
        try:
            validatedStatusResult = self.checkBolStatusAndClickEditButton()
            if validatedStatusResult:
                totalValueBeforeCancel = 0
                totalValueAfterCancel = 0
                if field.lower() == "rev":
                    totalValueBeforeCancel = self.getTotalValue(field)
                elif field.lower() == "cost":
                    totalValueBeforeCancel = self.getTotalValue(field)
                else:
                    self.log.error("passed index is invalid")

                editedRes = self.editFinancialSection(lineitem, field, value, rowno)
                if editedRes:
                    self.elementClick(self._cancelFinancialDetailsButton, "id")

                    if self.isElementDisplayed(self._modalPopup, "xpath"):
                        CancelPopupMsg = self.getText(self._DisplayedMsg, "xpath")
                        validateCancelmsg = self.validators.verify_text_match(CancelPopupMsg, self.expectedCancelMsg)
                        self.log.info("validateCancelmsg : " + str(validateCancelmsg))

                        self.elementClick(self._cancelConfirmButton, "xpath")

                        if field.lower() == "rev":
                            totalValueAfterCancel = self.getTotalValue(field)
                        elif field.lower() == "cost":
                            totalValueAfterCancel = self.getTotalValue(field)

                        self.log.info("totalValueBeforeCancel : " + str(totalValueBeforeCancel))
                        self.log.info("totalValueAfterCancel : " + str(totalValueAfterCancel))
                        if validateCancelmsg and totalValueBeforeCancel == totalValueAfterCancel:
                            return True
                        else:
                            self.log.error("Total value is not matching on canceling edited values, Expected : " + str(
                                totalValueBeforeCancel) + " Actual Value : " + str(totalValueAfterCancel))
                            return False
                    else:
                        self.log.error("Cancel confirmation popup didn't appear on clicking cancel button")
                        return False
                else:
                    self.log.error("Cannot find passed line item " + str(lineitem))
                    return False
            else:
                self.log.error("BOL is not in pending status, hence details cannot be editable")
                return False
        except Exception as E:
            self.log.error("Either passed locators or locator types passed are incorrect in "
                           "validateFinancialSectionOnCancelChanges method : " + str(E))
            return False

    def getTotalValue(self, field=""):
        self.log.info("Inside getTotal " + str(field) + " value")
        try:
            if field.lower() == "cost":
                return float(self.getText(self._totalcost, "id").replace("(", "-").replace(")", "").replace("$", ""))
            elif field.lower() == "rev":
                return float(self.getText(self._totalrevenue, "id").replace("(", "-").replace(")", "").replace("$", ""))
        except Exception as E:
            self.log.error("Exception occurred in validateFinancialEditSectionOnSavingChanges Method and exception is "
                           ": " + str(E))
            return False

    def getLineItemsBreakupValue(self, carrierbreakuplist, lineItemName, fieldName):
        """
        Getting Line items Breakup Value
        :return: Value if Exists
        """
        try:
            self.log.info("Inside getLineItemsBreakupValue Method and Line Items Name : " + str(lineItemName) +
                          " , Field Name : " + str(fieldName))
            # carrierbreakuplist = ast.literal_eval(carrierBreakupList)
            passedcarrieritems = carrierbreakuplist[0]
            passedcostlist = carrierbreakuplist[1]
            passedrevenuelist = carrierbreakuplist[2]
            flag = False
            indexValue = 0
            for index in range(len(passedcarrieritems)):
                # self.log.info(passedcarrieritems[index])
                if passedcarrieritems[index].lower() == lineItemName.lower():
                    indexValue = index
                    flag = True
                    break
            if flag:
                if fieldName.lower() == "cost":
                    self.log.info("Cost Value : " + str(passedcostlist[indexValue]))
                    return passedcostlist[indexValue]

                if fieldName.lower() == "revenue":
                    self.log.info("Revenue Value : " + str(passedrevenuelist[indexValue]))
                    return passedrevenuelist[indexValue]
            else:
                self.log.debug("Passed Line item not found in list")
                return 0

        except Exception as E:
            self.log.error("Exception occurred in getLineItemsBreakupValue Method and Exception is : " + str(E))

    def removeLineItemFromBreakupList(self, carrierBreakupList, lineItemName):
        """
        Removing the Line Items from Breakup List
        :param carrierBreakupList: Carrier Brakup List
        :param lineItemName: Line item name want to remove
        :return: removed item list
        """
        try:
            self.log.info("Inside removeLineItemFromBreakupList Method")
            carrierbreakuplist = ast.literal_eval(str(carrierBreakupList))

            for i in range(len(carrierbreakuplist[0])):
                if carrierbreakuplist[0][i].lower() == lineItemName.lower():
                    carrierbreakuplist[0].pop(i)
                    carrierbreakuplist[1].pop(i)
                    carrierbreakuplist[2].pop(i)
                    break
            self.log.info("BreakUp List : " + str(carrierbreakuplist))
            return carrierbreakuplist

        except Exception as E:
            self.log.error("Exception found in removeLineItemFromBreakupList Method and Exception is : " + str(E))
