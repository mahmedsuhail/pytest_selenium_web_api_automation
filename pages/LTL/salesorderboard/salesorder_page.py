import ast
from datetime import datetime, timedelta

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from pages.basepage import BasePage


class SalesorderPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # ---------------------------------------------- Locators------------------------------------------------
    _sales_order_icon = "//nav[contains(@class,'side-navstyle__LeftBarNav')] //a[@title='Sales Order Board']"

    _sales_order_board_container = "salesOrderBoard_tl-1810_wrapper"

    _so_data_parent = "//div[@class='ag-center-cols-container']"
    _so_data_rowid = "//div[@row-id='"
    _so_data_colid = "//div[@col-id='"
    column_header_parent = "(//div[@class='ag-header-row ag-header-row-column'])[2]"
    column_filter_parent = "//div[@class='ag-header-row ag-header-row-floating-filter']"

    _salesOrder_column_headers = column_header_parent + "/child::div"

    column_header_base = column_header_parent + _so_data_colid
    column_filter_base = column_filter_parent + " //div[@aria-colindex='"

    _loading_span = "//div[@class='ag-overlay']//span[@class='ag-overlay-loading-center']"

    _salesorder_rowdata_1 = _so_data_parent + _so_data_rowid
    _list_column_values = _so_data_parent + _so_data_colid

    _so_inner_spinner = "//div[@ref='eFullWidthContainer']/child::div"
    _so_no_rows = "//div[@class='ag-overlay'] //span[text()='No Rows To Show']"
    _so_scroll_column = _so_data_parent + _so_data_colid
    _bolnumber_id = "salesOrderBoard_LTL1-1734_customBolHyperlink_"

    _pagination_textbox = "//div[contains(@class,'PaginationControls')]//input"
    _firstpage_button = "//span[text()='First']"
    _lastpage_button = "//span[text()='Last']"
    _prevpage_button = "//span[text()='Prev']"
    _nextpage_button = "//span[text()='Next']"
    _totalpage_span = "//div[contains(@class,'PageSelector')]//span"

    _salesorderboard = "SalesOrderBoard-container"
    # _filer_input = "salesOrderBoard_LTL1-1734"
    _so_board_data_spinner = "//div[@id='salesOrderBoard_ltl1-1734_table_container']//div[text()='Loading']"

    _so_data_base = "salesOrderBoard_LTL1-1734_"
    _so_board_data_button = _so_data_base + "iconContainer_"
    _manualDispatch_button_end = "_phone"
    _manualDispatch_popup = "_manualDispatch"

    _manually_finalize_button_end = "_circle_Check"
    _manually_finalize_popup = "_manualFinalize"

    _edi_dispatch_button_end = "_circle_Info"
    _edi_dispatch_popup = "_ediDispatch"

    _soboard_shipmentstatusfilter_allorders = "//li[text()='All Orders']"
    _soboard_shipmentstatusfilter_pending = "//li[text()='Pending']"
    _soboard_shipmentstatusfilter_dispatched = "//li[text()='Dispatched']"
    _soboard_shipmentstatusfilter_intransit = "//li[text()='In Transit']"
    _soboard_shipmentstatusfilter_delivered = "//li[text()='Delivered']"
    _soboard_ltlfiltertab_quotes = "//li[text()='Quotes']"

    # =====================================SO Board Pop up===============================================
    _so_board_bolnumber = "//div[text()='BOL #']/following-sibling::div"
    _so_board_ponumber = "//div[text()='PO #']/following-sibling::div"
    _so_board_refnumber = "//div[text()='Ref #']/following-sibling::div"
    _so_board_expDate = "//div[text()='Exp. Date']/following-sibling::div"
    _so_board_quotedDate = "//div[text()='Quoted Date']/following-sibling::div"
    _so_board_carrier_name = "//div[contains(@class,'order-detailsstyles__AddressWrapper')][1]/div[1]"
    _so_board_customer_name = "//div[text()='Shipper']/ancestor::div[2]/preceding-sibling::div"

    # # =====================================SO Board address Pop up===============================================
    _so_board_shipper_company = "//div[text()='Shipper']/following-sibling::div[1]"
    _so_board_shipper_add1 = "//div[text()='Shipper']/following-sibling::div[2]"
    _so_board_shipper_add2 = "//div[text()='Shipper']/following-sibling::div[3]"
    _so_board_shipper_zip = "//div[text()='Shipper']/following-sibling::div[4]"
    _so_board_shipper_contact = "//div[text()='Shipper']/following-sibling::div[5]"
    _so_board_shipper_phone = "//div[text()='Shipper']/following-sibling::div[6]"

    _so_board_consignee_company = "//div[text()='Consignee']/following-sibling::div[1]"
    _so_board_consignee_add1 = "//div[text()='Consignee']/following-sibling::div[2]"
    _so_board_consignee_add2 = "//div[text()='Consignee']/following-sibling::div[3]"
    _so_board_consignee_zip = "//div[text()='Consignee']/following-sibling::div[4]"
    _so_board_consignee_contact = "//div[text()='Consignee']/following-sibling::div[5]"
    _so_board_consignee_phone = "//div[text()='Consignee']/following-sibling::div[6]"

    # =====================================SO Board shipping Data in Pop up============================================
    _so_board_shipping_item_list = "//div[text()='Commodity Description']/following-sibling::div"
    _so_board_weight = "//div[text()='Weight (Lbs)']/following-sibling::div"
    _so_board_unitType = "//div[text()='Unit Type']/following-sibling::div"
    _so_board_pallet = "//div[text()='# Pallets']/following-sibling::div"
    _so_board_pieces = "//div[text()='# Pieces']/following-sibling::div"
    _so_board_length = "//div[text()='Length']/following-sibling::div"
    _so_board_width = "//div[text()='Width']/following-sibling::div"
    _so_board_height = "//div[text()='Height']/following-sibling::div"
    _so_board_class = "//div[text()='Class']/following-sibling::div"
    _so_board_hazmat = "//input[@id='sales_order_board_LTL1-868_hazardous_checkbox']/following-sibling::div/child::*"

    # ================================SO Board Accessorial Data in Pop up====================================
    _so_board_get_accessorial = "//div[text()='Accessorials']/parent::div/following-sibling::div//div[@title]"

    _so_board_pu_number = "//input[@name='pickupNumber']"
    _so_board_dispatch_button = "//span[text()='DISPATCH']/parent::button"
    _so_board_confirmation_popup = "//div[contains(@class,'ReactModal__Content')]"
    _so_board_confirmation_msg = _so_board_confirmation_popup + "//div[contains(@class,'_PopupMessage')]"
    _Ok_button_confirmation_msg = _so_board_confirmation_popup + "//span[text()='OK']/parent::button"

    _acc_dispatch_confirmation_popup = "//div[contains(@class,'ReactModal__Content')] //div[text()='Dispatch Confirmation']"
    _acc_yes_confirmation_popup = _so_board_confirmation_popup + "//span[text()='YES']/parent::button"
    _acc_no_confirmation_popup = _so_board_confirmation_popup + "//span[text()='NO']/parent::button"

    _so_board_pro_number = "//input[@name='proNumber']"
    _so_board_pickup_date = "//input[@placeholder='Enter Date']"
    _so_board_finalize_button = "//span[text()='FINALIZE']/parent::button"
    _so_board_finalize_popup_add_note_button = "//span[text()='ADD NOTE']/parent::button"

    # =====================================Message==========================================================
    _manual_dispatch_message = "Manual Dispatch Successful"
    _finalize_message = "Shipment finalized successfully."
    _edi_dispatch_message = "EDI Dispatch Successful"
    _edi_dispatch_accessorial_msg = "The shipment is either Guaranteed or contains pick up accessorials. Do you want to proceed with the dispatch?"

    # ----------------------- Saved Quote Page Common Locators----------------------------------
    _sq_data_base = "salesOrderBoard_LTL1-1734_"
    _sq_buttonsection = "//div[contains(@class,'book-itstyle__BookItContainer')]"
    _sq_bookit_button = _sq_buttonsection + "//span[text()='BOOK IT']"
    _sq_email_button = _sq_buttonsection + "//span[text()='EMAIL']"
    _sqpopup_customercontactname = "(//div[text()='Name:']/following-sibling::div)[2]"
    _sqpopup_pickupdate = "//div[text()='Pickup']/following-sibling::div[2]"

    # ----------------------------------------SO Board Method-------------------------------------------

    def clickSalesOrderIcon(self):
        self.log.info("Click Sales Order Button")
        self.elementClick(self._sales_order_icon, locatorType="xpath")

    def validateSalesOrderIcon(self):
        value = self.getAttribute(self._sales_order_icon, locatorType="xpath", attributeType="class")
        self.log.info(value)
        if value == "active":
            return True
        else:
            return False

    def validateSalesBoardColumnHeaders(self):
        """Inside this method, we are validating sales order board column headers with the headers list passed from
        csv file and returning True if it matches """

        self.log.info("Inside validateSalesBoardColumnHeaders method")
        try:
            self.waitForElement(self._so_inner_spinner, "xpath", "display")
            self.waitForElement(self._so_inner_spinner, "xpath", "notdisplay")

            _salesorder_headersList = self.csvRowDataToList("salesorderboard_headers", "defaultlist.csv")
            headersList = self.getElementList(self._salesOrder_column_headers, "xpath")
            self.log.info("Length of headers list : " + str(len(headersList)))
            commonflag = True
            for i in range(0, len(headersList)):
                self.driver.execute_script("arguments[0].scrollIntoView();", headersList[i])
                if _salesorder_headersList[i].strip().lower() == headersList[i].text.lower():
                    commonflag = commonflag and True
                else:
                    self.log.error("Sales Order Board Column Headers are not matching : " + str(headersList[i].text))
                    commonflag = commonflag and False

            return commonflag

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error(
                "Passed locators or locator types  are incorrect for validateSalesBoardColumnHeaders method ")

    def validateSalesOrderBoardByDefault(self):
        """Inside this method, we are validating sales order board default validation by verifying pick up Date is
        set to ascending order and values are in ascending order and returning true if it satisfies the default conditions"""

        self.log.info("Inside validateSalesOrderBoardByDefault method")
        try:
            flag1 = self.checkSortedValue("pickupDate", "sorted-asc")
            flag2 = self.validateColumnValuesBasedOnOrder("pickupDate", "asc")
            if flag1 and flag2:
                return True
            else:
                self.log.error("Pick up date is not displayed in ascending order by default")
                return False

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Either Passed locators or locator types  are incorrect for "
                           "validateSalesOrderBoardByDefault method ")

    def checkSortedValue(self, columnname="", expectedsortvalue=""):
        """Inside this method, we are checking the expected sorted value is present or not and this method is being
        used in validateSalesOrderBoardByDefault method """

        self.log.info("Inside validateSalesOrderBoardByDefault method")
        if expectedsortvalue in self.getAttribute(self.column_header_base + columnname + "']//div[3]",
                                                  "xpath", attributeType="class"):
            return True

    def applySortAndGetColumnValues(self, columnname="", order=""):
        """Inside this method, we are applying sorting based on the passed order value on the column passed
        columnname and returning the column values as a list"""

        self.log.info("Inside validateColumnSorting method")
        try:

            # self.pageVerticalScroll(self._column_div + columnname + "']", "xpath")
            self.pageVerticalScroll(self._so_scroll_column + columnname + "']", "xpath")

            if order.lower() == "asc":

                if "sorted-asc" not in self.getAttribute(self.column_header_base + columnname + "']//div[3]",
                                                         "xpath", attributeType="class"):

                    while "sorted-asc" not in self.getAttribute(self.column_header_base + columnname + "']//div[3]",
                                                                "xpath", attributeType="class"):
                        self.pageVerticalScroll(self._so_scroll_column + columnname + "']", "xpath")
                        self.elementClick(self.column_header_base + columnname + "']//div[3]",
                                          "xpath")

                    self.waitForElement(self._so_inner_spinner, "xpath", "notdisplay", timeout=20)

            elif order.lower() == "desc":

                if "sorted-desc" not in self.getAttribute(self.column_header_base + columnname + "']//div[3]",
                                                          "xpath", attributeType="class"):
                    while "sorted-desc" not in self.getAttribute(
                            self.column_header_base + columnname + "']//div[3]",
                            "xpath", attributeType="class"):
                        self.pageVerticalScroll(self._so_scroll_column + columnname + "']", "xpath")
                        self.elementClick(self.column_header_base + columnname + "']//div[3]",
                                          "xpath")

                    self.waitForElement(self._so_inner_spinner, "xpath", "notdisplay", timeout=20)

            else:
                self.log.error(
                    "Either passed column or order is invalid Columnheader : " + str(columnname) + " Order : " + str(
                        order))

            column_values_count = self.getElementList(self._list_column_values + columnname + "']", "xpath")

            column_values_list = []
            for i in range(0, len(column_values_count)):
                element = self._salesorder_rowdata_1 + str(i) + "']" + self._so_data_colid + columnname + "']"
                textVal = self.getText(element, "xpath")
                if self.isElementDisplayed(element, "xpath") and textVal != "" and textVal is not None:
                    self.pageVerticalScroll(element, "xpath")
                    column_values_list.append(textVal)

            return column_values_list

        except Exception as e:
            self.log.error("Exception : " + str(e))
            self.log.error(
                "Either passed locator or column or order is invalid Columnheader : " + str(
                    columnname) + " Order : " + str(
                    order))

    def validateColumnValuesBasedOnOrder(self, columnname="", order=""):
        """Inside this method, we are validating the returned list from applySortAndGetColumnValues() method with the
        sorted list based on order passed """
        self.log.info("Inside validateColumnValuesBasedOnOrder method")
        try:

            resultList = self.applySortAndGetColumnValues(columnname, order)
            self.log.info("resultList : " + str(resultList))
            value = ""
            dateColumnsList = ["pickupDate", "estimatedTimeofArrival", "deliveryDate"]
            commonflag = True
            if columnname in dateColumnsList:
                value = "date"
            else:
                value = "others"

            self.log.info("Final value : " + str(value))

            if value == "others":
                if order.lower() == "asc":
                    sortedList = sorted(resultList)
                else:
                    sortedList = sorted(resultList, reverse=True)

                self.log.info("Returned list : " + str(resultList))
                self.log.info(
                    "Sorted list based on order passed : " + "order : " + str(order) + " List values : " + str(
                        sortedList))

                if resultList == sortedList:
                    return True

                else:
                    self.log.error("lists are not matching in validateColumnValuesBasedOnOrder method")
                    return False

            elif value == "date" and order.lower() == "asc":
                for i in range(0, len(resultList) - 1):
                    if datetime.strptime(resultList[i], "%m/%d/%Y").date() <= datetime.strptime(
                            resultList[i + 1], "%m/%d/%Y").date():
                        commonflag = commonflag and True
                    else:
                        self.log.error("Dates are not in ascending order")
                        commonflag = commonflag and False

                self.log.debug(str(columnname) + "verifying dates in ascending order")
                return commonflag

            elif value == "date" and order.lower() == "desc":
                for i in range(0, len(resultList) - 1):
                    if datetime.strptime(resultList[i], "%m/%d/%Y").date() >= datetime.strptime(
                            resultList[i + 1], "%m/%d/%Y").date():
                        commonflag = commonflag and True
                    else:
                        self.log.error("Dates are not in descending order")
                        commonflag = commonflag and False

                self.log.debug(str(columnname) + "verifying dates in descending order")
                return commonflag
            else:
                self.log.error(
                    "Either passed column or order is invalid Columnheader : " + str(columnname) + " Order : " + str(
                        order))

        except Exception as e:
            self.log.error("Exception : " + str(e))
            self.log.error(
                "Either passed column or order is invalid Columnheader : " + str(columnname) + " Order : " + str(
                    order))

    def applyFilterBasedOnColumnName(self, columnName, passedData, dateFormat='%m/%d/%Y'):
        """
        Apply filter based on column Name
        :param passedData: value want to enter in column filter
        :param columnName: the column want to apply pass
        :return: if able to perform filter in column than True else False
        """
        self.log.info("Inside applyFilterBasedOnColumnName Method , Passed Data : " + str(passedData) +
                      ", column Name" + str(columnName))
        try:
            if not passedData == "":
                self.soBoardLoadWait("notdisplay")

                # indexVal = self.getAttribute(self.column_header_base + columnName + "']//span", "xpath",
                #                              "aria-colindex")
                # self.log.info("index value : " + str(indexVal))

                self.pageVerticalScroll(self._so_scroll_column + columnName + "']", "xpath")
                if self.isElementPresent(self._so_data_base + columnName + "_datePicker", "id"):

                    self.elementClick(self._so_data_base + columnName + "_datePicker", "id")
                    date_to_be_selected = self.selectDateBasedOnPassedValue(passedData=passedData,
                                                                            dateFormat=dateFormat)

                    selected_datevalue = self.getAttribute(self._so_data_base + columnName + "_datePicker",
                                                           "id",
                                                           "value")
                    self.log.info("selected_datevalue : " + str(selected_datevalue))
                    if selected_datevalue == date_to_be_selected:
                        self.log.info("True")
                        return True
                    else:
                        return False

                else:
                    self.sendKeys(passedData, self._so_data_base + "customText_" + columnName + "_textField", "id")

                self.soBoardLoadWait("notdisplay", timeout=50)

                # if self.elementPresenceCheck(self._so_no_rows, "xpath"):
                if self.validateNoRowsToShow():
                    self.log.error(
                        "As per passed search value : " + passedData + " , in column : " + columnName + " data not found")
                    return False
                else:
                    return True
            else:
                self.log.debug("No Value passed in applyFilterBasedOnColumnName Method, Hence returning False")
                return False

        except Exception as E:
            self.log.error("Exception occurred in applyFilterBasedOnColumnName Method and exception is : " + str(E))

    def columnValueCompare(self, columnName, expectedData, bolNumber):
        """
        Comparing the cell value and expected Data
        :param bolNumber:
        :param rowId: to the row we want to search and get the data for comparision
        :param columnName: the column name for comparision has to perform
        :param expectedData: expected cell value
        :return: if match than True else False
        """
        self.log.info("Inside columnValueCompare Method, columnname : " + str(columnName) + " , expectedData : " + str(
            expectedData))
        try:
            self.soBoardLoadWait("notdisplay")
            # if not self.elementPresenceCheck(self._so_no_rows, "xpath"):
            if not self.validateNoRowsToShow():
                getCellValue = self.getCellData(columnName, bolNumber)
                self.log.info("Get cell Value : " + str(getCellValue))
                if "$" in getCellValue:
                    self.log.info("inside $ replace condition")
                    getCellValue = getCellValue.replace("$", "").replace(",", "")
                elif getCellValue[2:3] == ":" and getCellValue[-3:-2] == ":":
                    self.log.info("Inside Time Condition")
                    expectedData1 = datetime.strptime(expectedData, "%I:%M %p")
                    expectedData = datetime.strftime(expectedData1, "%H:%M:%S")
                    self.log.info("Expected Data : " + str(expectedData))

                return self.validators.verify_text_match(getCellValue, str(expectedData))
            else:
                self.log.error("Data not loaded in Board")
                return False

        except Exception as E:
            self.log.error("Exception found in columnValueCompare Method and Exception is : " + str(E))

    # def columnValueCompare(self, columnName, expectedData, rowId=0):
    #     """
    #     Comparing the cell value and expected Data
    #     :param rowId: to the row we want to search and get the data for comparision
    #     :param columnName: the column name for comparision has to perform
    #     :param expectedData: expected cell value
    #     :return: if match than True else False
    #     """
    #     self.log.info("Inside columnValueCompare Method, columnname : " + str(columnName) + " , expectedData : " + str(
    #         expectedData))
    #     try:
    #         self.soBoardLoadWait("notdisplay")
    #         if not self.elementPresenceCheck(self._so_no_rows, "xpath"):
    #             # self.pageVerticalScroll(self._so_scroll_column + columnName + "']", "xpath")
    #             getCellValue = self.getCellData(columnName, rowId)
    #             self.log.info("Get cell Value : " + str(getCellValue))
    #             if "$" in getCellValue:
    #                 self.log.info("inside $ replace condition")
    #                 getCellValue = getCellValue.replace("$", "").replace(",", "")
    #             elif getCellValue[2:3] == ":" and getCellValue[-3:-2] == ":":
    #                 self.log.info("Inside Time Condition")
    #                 expectedData1 = datetime.strptime(expectedData, "%I:%M %p")
    #                 expectedData = datetime.strftime(expectedData1, "%H:%M:%S")
    #                 self.log.info("Expected Data : " + str(expectedData))
    #
    #             return self.validators.verify_text_match(getCellValue, str(expectedData))
    #         else:
    #             self.log.error("Data not loaded in Board")
    #             return False
    #
    #     except Exception as E:
    #         self.log.error("Exception found in columnValueCompare Method and Exception is : " + str(E))

    def getCellData(self, columnName, bolNumber):
        """
        Getting cell Value based on passed column name and row id , by default row id will be 0
        :param bolNumber:
        :param columnName: column Name (not display one)
        :return: cell value
        """

        self.log.info("Inside getCellValue Method and passed column name is : " + str(columnName))
        try:
            if columnName == "billOfLading":
                bolNumber_link = self._bolnumber_id + str(bolNumber) + "_hyperLink"
                getCellValue = self.getText(bolNumber_link, "id")

            else:
                getCellValue = self.getText(self._so_data_base + bolNumber + "_" + columnName, "id")
            # getCellValue = self.getText(self._so_data_parent + self._so_data_rowid + str(rowId) + "']"
            #                             + self._so_data_colid + columnName + "']", "xpath")
            self.log.info("extracted value for passed column is : " + str(getCellValue))
            return getCellValue
        except Exception as E:
            self.log.error("Exception occurred in getCellValue Method and Exception is : " + str(E))

    # def getCellData(self, columnName, rowId=0):
    #     """
    #     Getting cell Value based on passed column name and row id , by default row id will be 0
    #     :param columnName: column Name (not display one)
    #     :param rowId: row id
    #     :return: cell value
    #     """
    #
    #     self.log.info("Inside getCellValue Method and passed column name is : " + str(columnName))
    #     try:
    #         getCellValue = self.getText(self._so_data_parent + self._so_data_rowid + str(rowId) + "']"
    #                                     + self._so_data_colid + columnName + "']", "xpath")
    #         self.log.info("extracted value for passed column is : " + str(getCellValue))
    #         return getCellValue
    #     except Exception as E:
    #         self.log.error("Exception occurred in getCellValue Method and Exception is : " + str(E))

    def validateDataAfterApplyingFilter(self, columnName, passedData):
        """Under this method, we are validating the passed data or searched data is present in the filtered list of
        data and returning True if it is present in the list """
        self.log.info("Inside validateDataAfterApplyingFilter")

        try:
            column_values_list = self.getElementList(self._list_column_values + columnName + "']", "xpath")
            commonflag = True
            self.log.info("column_values_count after applying filter : " + str(len(column_values_list)))
            if len(column_values_list) > 0:
                for i in column_values_list:
                    # self.driver.execute_script("arguments[0].scrollIntoView();", i)
                    if i.text != "" and i.text is not None:
                        if passedData.lower() in i.text.lower():
                            commonflag = commonflag and True
                        else:
                            self.log.error("Passed data : " + str(passedData) + " is not present in " + str(i.text))
                            commonflag = commonflag and False

                self.log.info("Final Flag value : " + str(commonflag))
                return commonflag

            else:
                self.log.error(
                    "As per passed search value : " + passedData + " , in column : " + columnName + " data not found")
                return False

        except Exception as e:
            self.log.error("Exception found in validateDataAfterApplyingFilter Method and Exception is : " + str(e))

    def paginationControl(self, pageValue=""):
        """Under this method, we are navigating to the page based on the pageValue passed and returning True if
        pagination is applied successfully """
        self.log.info("Inside paginationControl method")
        try:
            if type(pageValue) == str or pageValue == "":
                if pageValue.lower() == "first":
                    self.elementClick(self._firstpage_button, "xpath")
                    commonFlag = True
                elif pageValue.lower() == "last":
                    self.elementClick(self._lastpage_button, "xpath")
                    commonFlag = True
                elif pageValue.lower() == "prev":
                    self.elementClick(self._prevpage_button, "xpath")
                    commonFlag = True
                elif pageValue.lower() == "next":
                    self.elementClick(self._nextpage_button, "xpath")
                    commonFlag = True
                elif pageValue == "":
                    totalpages = self.getText(self._totalpage_span, "xpath").split(" ")
                    totalpages_count = totalpages[1]
                    self.log.info("Totalpages_count : " + str(totalpages_count))
                    pageValue = int(int(totalpages_count) / 2)
                    self.log.info("pagenumber : " + str(pageValue))

                    self.sendKeys(pageValue, self._pagination_textbox, "xpath")
                    commonFlag = True
                else:
                    self.log.error(
                        "Either pagevalue passed is invalid or locators and locators type are incorrect for "
                        "paginationControl "
                        "method : " + str(pageValue))
                    commonFlag = False

            elif type(pageValue) == int:
                self.sendKeys(pageValue, self._pagination_textbox, "xpath")
                commonFlag = True

            elif pageValue.isdigit():
                self.sendKeys(pageValue, self._pagination_textbox, "xpath")
                commonFlag = True

            else:
                self.log.error(
                    "Either pagevalue passed is invalid or locators and locators type are incorrect for "
                    "paginationControl "
                    "method : " + str(pageValue))
                commonFlag = False

            self.soBoardLoadWait("notdisplay")
            return commonFlag

        except Exception as e:
            self.log.error("Exception : " + str(e))
            self.log.error(
                "Either pagevalue passed is invalid or locators and locators type are incorrect for paginationControl "
                "method : " + str(pageValue))

    def clearFilter(self, columnName):
        """Under this method, we are clearing the filtered value applied on the column passed"""
        self.log.info("Inside clearFilter method")
        try:
            # indexval = self.getAttribute(self.column_header_base + columnName + "']//span", "xpath", "aria-colindex")
            # self.log.info("index value : " + str(indexval))

            if self.isElementPresent(self._so_data_base + columnName + "_datePicker", "id"):
                KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
                self.sendKeys(KeyCombination, self._so_data_base + columnName + "_datePicker", "id")
                self.elementClick(self._salesorderboard, "id")
            else:
                self.sendKeys(" ", self._so_data_base + "customText_" + columnName + "_textField", "id")

            self.soBoardLoadWait("notdisplay")

        except Exception as e:
            self.log.error("Exception : " + str(e))
            self.log.error("locators and locators type are incorrect for clearFilter method ")

    def validateDefaultPage(self):
        """Under this method, we are validating that the page is set to default page on applying filter in any other
        pages and returning True if page is set to Default page"""
        self.log.info("Inside validateDefaultPage method")
        try:
            pageNumber = self.getAttribute(self._pagination_textbox, "xpath", attributeType="placeholder")
            if pageNumber == "1":
                return True
            else:
                self.log.error("Sales order board is not set to default page after applying filter on navigated page")
                return False
        except Exception as e:
            self.log.error("Exception : " + str(e))
            self.log.error("locators and locators type are incorrect for validateDefaultPage method ")

    def navigateToSOPage(self, bolNumber):
        """
        Navigating to the SO Page from SO Board via click BOl Number
        :param bolNumber: Bol number want to click
        :return: True if success else False
        """
        self.log.info("Inside navigateToSOPage Method and passed bolNUmber : " + str(bolNumber))
        try:
            getValue = self.getFilterValue("billOfLading")
            bolNumber_link = self._bolnumber_id + str(bolNumber) + "_hyperLink"
            if not bolNumber == "":
                if not getValue == bolNumber:
                    filterValue = self.applyFilterBasedOnColumnName("billOfLading", bolNumber)
                    self.log.debug("Applied Filter returned Value : " + str(filterValue))
                else:
                    filterValue = True

                if filterValue and self.isElementDisplayed(bolNumber_link, "id"):
                    self.elementClick(bolNumber_link, "id")
                    self.waitForElement(bolNumber_link, "id", "notdisplay", timeout=15)
                    return True
                else:
                    return False
            else:
                self.log.debug("No BOL Passed to navigateToSOPage Method, Hence returning False.")
                return False
        except Exception as E:
            self.log.error("Exception occurred in navigateToSOPage Method and exception is : " + str(E))
            return False

    def soBoardLoadWait(self, waitFor="notdisplay", timeout=10):
        """
        wait for SO Board to Load Data
        :param timeout: time out time
        :param waitFor: wait for element to display or not display
        :return:
        """
        self.log.info("Inside soBoardLoadWait Method")
        self.waitForElement(self._sales_order_board_container, "id", event="display", timeout=20)
        # self.waitForElement(self._so_inner_spinner, "xpath", "display", timeout=timeout)
        if self.isElementPresent(self._so_inner_spinner, "xpath"):
            self.waitForElement(self._so_inner_spinner, "xpath", waitFor, timeout=timeout)

    def getConvertedWeightValue(self, weight, unitType):
        """
        Get Converted weight Value
        :param weight:
        :param unitType:
        :return: weight Value in lbs
        """
        try:
            self.log.info("Inside getConvertedWeightValue Method, passed weight Value : " + str(weight) +
                          " ,Unit Type: " + str(unitType))

            # weight = float(weight)
            # weight = float(weight.replace(".", ""))
            weight = round(float(weight))
            if unitType.lower() == "kg":
                wtUnits = 2.20462
            else:
                wtUnits = 1

            TotalWeight = wtUnits * int(float(weight))
            self.log.debug("Returning Total Weight : " + str(TotalWeight))
            return TotalWeight

        except Exception as E:
            self.log.error("Exception occurred in getConvertedWeightValue Method and Exception is : " + str(E))

    def validateColumnValue(self, columnName, expectedColumnValueList, bolNumber):
        """
        Validating the Column Value for specific BOL
        :param bolNumber: the bol Number want to check the status
        :param columnName: the column value want to retrieve
        :param expectedColumnValueList: expected column Value
        :return: True If Match else False
        """
        try:
            self.log.info("Inside validateColumnValue Method, column Name : " + str(columnName) +
                          ", ExpectedColumn Value : " + str(expectedColumnValueList) + " ,BOLNumber :" + str(bolNumber))
            self.waitForElement(self._so_data_base + bolNumber + "_" + columnName, "id", event="display", timeout=40)
            self.pageVerticalScroll(self._so_data_base + bolNumber + "_" + columnName, "id")
            getColumnValue = self.getText(self._so_data_base + bolNumber + "_" + columnName, "id")
            self.log.debug("get value from SO Board : " + str(getColumnValue))

            if type(expectedColumnValueList) is str:
                expectedColumnValueList = expectedColumnValueList.split(",")
            elif type(expectedColumnValueList) is tuple:
                expectedColumnValueList = list(expectedColumnValueList)
            elif type(expectedColumnValueList) is list:
                expectedColumnValueList = expectedColumnValueList
            else:
                self.log.info("Incorrect type passed in validateColumnValue Method")

            self.log.debug("Prepared list in validateColumnValue Method : " + str(expectedColumnValueList))

            flag = False
            for i in expectedColumnValueList:
                if getColumnValue.lower() == i.lower():
                    self.log.info("Get value found in expected List")
                    return True

            if not flag:
                self.log.info("Get value not found in expected List")
                return False

        except Exception as E:
            self.log.error("Exception occurred in validateColumnValue Method and Exception is : " + str(E))

    def getFilterValue(self, columnName):
        """
        get the applied filter value and return
        :param columnName: for the column want to return the value
        :return: applied filter value
        """
        try:
            self.log.info("Inside getFilterValue Method, columnName : " + str(columnName))
            self.log.info("Prepared Locator : " + str(self._so_data_base + "customText_" + columnName + "_textField"))
            filterValue = self.getAttribute(self._so_data_base + "customText_" + columnName + "_textField", "id",
                                            attributeType="value")

            return filterValue

        except Exception as E:
            self.log.error("Exception occurred in getFilterValue Method and Exception is : " + str(E))

    # =================================SO Board Update Method======================================================
    def getButtonEnabled(self, bolNumber, ButtonName):
        """
        Validating the SO Button Status
        :param bolNumber: for the Bol Number want to execute the method
        :param ButtonName: The Button want Validate
        :return: True if Button enabled else False
        """
        try:
            self.log.info("Inside getButtonEnabled Method")
            if ButtonName.lower().replace(" ", "") == "manualdispatch":
                locator = self._so_board_data_button + bolNumber + self._manualDispatch_button_end
                getProperty = self.getAttribute(locator, "id", "data-disabled")
                if getProperty == "false":
                    self.log.debug("Manual Dispatch Button enabled")
                    return True
                else:
                    self.log.debug("Manual Dispatch Button Disabled")
                    return False
            elif ButtonName.lower().replace(" ", "") == "finalize":
                locator = self._so_board_data_button + bolNumber + self._manually_finalize_button_end
                getProperty = self.getAttribute(locator, "id", "data-disabled")
                if getProperty == "false":
                    self.log.debug("Manually Finalize Button enabled")
                    return True
                else:
                    self.log.debug("Manually Finalized Button Disabled")
                    return False
            elif ButtonName.lower().replace(" ", "") == "edidispatch":
                locator = self._so_board_data_button + bolNumber + self._edi_dispatch_button_end
                getProperty = self.getAttribute(locator, "id", "data-disabled")
                if getProperty == "false":
                    self.log.debug("EDI Dispatch Button enabled")
                    return True
                else:
                    self.log.debug("EDI Dispatch Button Disabled")
                    return False
            else:
                self.log.error("Incorrect ButtonName passed")
                return False

        except Exception as E:
            self.log.error("Exception occurred in getButtonEnabled Method and Exception is : " + str(E))

    def getButtonEnabledInPopups(self, button_name):
        """
        Getting the Button enabled or Not
        :param button_name: the Button name for which check the enabled property
        :return: True If Enabled else Disabled
        """
        try:
            self.log.info(f"Inside getButtonEnabledInPopups Method and the passed button name is : {str(button_name)}")
            if button_name.lower().replace(" ", "") == "finalize":
                if self.getAttribute(self._so_board_finalize_button, "xpath", "data-disabled") == "true":
                    self.log.debug("Button disabled")
                    return False
                else:
                    self.log.debug("Button Enabled")
                    return True

            if button_name.lower().replace(" ", "") == "addnote":
                if self.getAttribute(self._so_board_finalize_popup_add_note_button, "xpath", "data-disabled") == "true":
                    self.log.debug("Button disabled")
                    return False
                else:
                    self.log.debug("Button Enabled")
                    return True

        except Exception as E:
            self.log.error(f"Exception Occurred in getButtonEnabledInPopups Method and Exception is : {str(E)}")

    def SOBoardPopupWait(self, bolNumber, action):
        """
        Preparing popup locator in one place and returning
        :return:
        """
        try:
            self.log.info("Inside SOBoardPopupWait Method")
            popupLocator = ""
            if action.lower().replace(" ", "") == "manualdispatch":
                popupLocator = self._so_board_data_button + bolNumber + self._manualDispatch_popup

            elif action.lower().replace(" ", "") == "finalize":
                popupLocator = self._so_board_data_button + bolNumber + self._manually_finalize_popup

            elif action.lower().replace(" ", "") == "edidispatch":
                popupLocator = self._so_board_data_button + bolNumber + self._edi_dispatch_popup

            else:
                self.log.error("Incorrect action name passed")

            self.waitForElement(self._so_board_data_spinner, "xpath", "notdisplay", timeout=30)
            self.waitForElement(popupLocator, "id", "display", timeout=30)
        except Exception as E:
            self.log.error("Exception occurred in SOBoardPopupWait Method and Exception is : " + str(E))

    def click_sales_order_link_in_so_board_update_popup(self, bol_number):
        """
        Method to click on the SalesOrder link in the so board update popup's(EDI Dispatch, Manual Finalize,
        Manual Dispatch)
        :param bol_number: bol number for which operation has to carry out.
        """
        try:
            self.log.info("Inside click_sales_order_link_in_so_board_update_popup Method and passed bol number is " +
                          str(bol_number))
            sales_order_hyper_link_in_popup = "(//a[@href='/sales-order/" + str(bol_number) + "'])[2]"
            self.pageVerticalScroll(sales_order_hyper_link_in_popup, "xpath")
            self.elementClick(sales_order_hyper_link_in_popup, "xpath")
        except Exception as E:
            self.log.error("Exception occurred in click_sales_order_link_in_so_board_update_popup Method "
                           "and Exception is : " + str(E))

    def clickSOBoardActionButton(self, bolNumber, ButtonName):
        """
        Clicking the SO Board Action Button (Data Button like dispatch finalize)
        :param bolNumber: for the Bol Number want to execute the method
        :param ButtonName: The Button want to click
        :return: True if Button enabled else False
        """
        try:
            self.log.info("Inside clickSOBoardActionButton Method")
            if ButtonName.lower().replace(" ", "") == "manualdispatch":
                locator = self._so_board_data_button + bolNumber + self._manualDispatch_button_end

            elif ButtonName.lower().replace(" ", "") == "finalize":
                locator = self._so_board_data_button + bolNumber + self._manually_finalize_button_end

            elif ButtonName.lower().replace(" ", "") == "edidispatch":
                locator = self._so_board_data_button + bolNumber + self._edi_dispatch_button_end

            else:
                self.log.error("Incorrect ButtonName passed in clickSOBoardActionButton Method")
                return False

            self.elementClick(locator, "id")
            self.SOBoardPopupWait(bolNumber, ButtonName)
            return True
        except Exception as E:
            self.log.error("Exception occurred in clickSOBoardActionButton Method and Exception is : " + str(E))

    def validateIdentifierDataSOBoard(self, bolNumber, action, fieldName, ExpectedData=""):
        """
        Validating the identifier data in SO Board Popup
        :param action: against what action popup open
        :param bolNumber: BOL Number for that action performing
        :param fieldName: the field name want to validate
        :param ExpectedData: compare data
        :return: True if Match else False
        """
        try:
            self.log.info("Inside validateIdentifierDataSOBoard Method")
            self.SOBoardPopupWait(bolNumber, action)

            if fieldName.replace(" ", "") == "BOL#":
                self.log.debug("inside BOL# Condition")
                getBol = self.getText(self._so_board_bolnumber, "xpath")
                return self.validators.verify_text_match(getBol, ExpectedData)
            elif fieldName.replace(" ", "") == "PO#":
                self.log.debug("inside PO# Condition")
                getPo = self.getText(self._so_board_ponumber, "xpath")
                return self.validators.verify_text_match(getPo, ExpectedData)
            elif fieldName.replace(" ", "") == "REF#":
                self.log.debug("inside REF# Condition")
                getRef = self.getText(self._so_board_refnumber, "xpath")
                return self.validators.verify_text_match(getRef, ExpectedData)
            elif fieldName.replace(" ", "").replace(".", "").lower() == "expdate":
                self.log.debug("inside EXP Date Condition")
                getExpDate = self.getText(self._so_board_expDate, "xpath")
                getquoteDate = self.getText(self._so_board_quotedDate, "xpath")
                ExpData = (datetime.strptime(getquoteDate, "%m/%d/%y") + timedelta(days=7)).strftime("%m/%d/%y")
                return self.validators.verify_text_match(getExpDate, ExpData)
            else:
                self.log.error("Incorrect Field name passed in validateIdentifierDataSOBoard Method, "
                               "Hence Failing the Method")
                return False

        except Exception as E:
            self.log.error("Exception occurred in validateIdentifierDataSOBoard Method and Exception is : " + str(E))

    def validateBaseDataInSOBoardPopup(self, bolNumber, action, fieldName, ExpectedData=""):
        """
        Validating the Carrier Data in SO Board Action Popup
        :param bolNumber: against what action popup open
        :param action: BOL Number for that action performing
        :param fieldName: the field name want to validate
        :param ExpectedData: compare data
        :return: True if Match else False
        """
        try:
            self.log.info("Inside validateBaseDataInSOBoardPopup Method")
            self.SOBoardPopupWait(bolNumber, action)

            if fieldName.lower().replace(" ", "") == "carriername":
                carrierName = self.getText(self._so_board_carrier_name, "xpath")
                return self.validators.verify_text_match(carrierName, ExpectedData)
            elif fieldName.lower().replace(" ", "") == "customername":
                getCustomerName = self.getText(self._so_board_customer_name, "xpath")
                return self.validators.verify_text_match(getCustomerName, ExpectedData)
            else:
                self.log.error("Incorrect Field name passed to validateBaseDataInSOBoardPopup Method")
                return False

        except Exception as E:
            self.log.error("Exception occurred in validateCarrierDataInSOBoard Method and Exception is : " + str(E))

    def validateAddressDataInSOBoard(self, section, companyName, Add1, Add2, Zip, Contact, Phone):
        """
        Validate the Address Data in SO Board(shipper and consignee)
        :return: True if Match else False
        """
        try:
            self.log.info("Inside validateAddressDataInSOBoard Method")
            if section.lower() == "origin":
                getCompanyName = self.getText(self._so_board_shipper_company, "xpath")
                getAdd1 = self.getText(self._so_board_shipper_add1, "xpath")
                getAdd2 = self.getText(self._so_board_shipper_add2, "xpath")
                getZip = self.getText(self._so_board_shipper_zip, "xpath")
                getContact = self.getText(self._so_board_shipper_contact, "xpath")
                getPhone = self.getText(self._so_board_shipper_phone, "xpath")

            elif section.lower() == "destination":
                getCompanyName = self.getText(self._so_board_consignee_company, "xpath")
                getAdd1 = self.getText(self._so_board_consignee_add1, "xpath")
                getAdd2 = self.getText(self._so_board_consignee_add2, "xpath")
                getZip = self.getText(self._so_board_consignee_zip, "xpath")
                getContact = self.getText(self._so_board_consignee_contact, "xpath")
                getPhone = self.getText(self._so_board_consignee_phone, "xpath")
            else:
                self.log.error("Incorrect Section name passed in validateAddressDataInSOBoard Method")

            validateCompany = self.validators.verify_text_match(getCompanyName, companyName)
            validateAdd1 = self.validators.verify_text_match(getAdd1, Add1)
            validateAdd2 = self.validators.verify_text_match(getAdd2, Add2)
            validateZip = self.validators.verify_text_match(getZip, Zip)
            validateContact = self.validators.verify_text_match(getContact, Contact)
            getPhone = getPhone.replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
            validatePhone = self.validators.verify_text_match(getPhone, Phone)
            self.log.info("Validating for section : " + str(section))
            self.log.debug(
                "companyName: " + str(validateCompany) + " ,Add1 : " + str(validateAdd1) + " ,Add2" +
                str(validateAdd2) + " ,Complete Zip: " + str(validateZip) + " ,Contact Name: " + str(validateContact) +
                " ,Phone: " + str(validatePhone))
            if validateCompany and validateAdd1 and validateAdd2 and validateZip and validateContact and validatePhone:
                return True
            else:
                return False

        except Exception as E:
            self.log.error("Exception occurred in validateAddressDataInSOBoard Method and Exception is : " + str(E))

    def validateShippingItemData(self, commodity, weight, wtUnits, unitType, pallets, pieces, length, width,
                                 height, dimUnit, cls, HzVal):
        """
        Validating the Shipping Items Data in SO Board Popup
        :param commodity: Commodity Name
        :param weight: weight Value in lbs
        :param wtUnits: wt unit type to conv value to lbs
        :param unitType: Package type
        :param pallets: Pallet count
        :param pieces:  Pieces Count
        :param length: Length count in inches
        :param width: width in inches
        :param height: height in inches
        :param cls: class Value
        :param HzVal: hz value true or false
        :return: if all value match than True else False
        """
        try:
            self.log.info("Inside validateShippingItemData Method")
            commodityCount = self.getElementList(self._so_board_shipping_item_list, "xpath")
            IndexValue = 0
            # To get which row containing passed shipping data
            for i in commodityCount:
                IndexValue += 1
                if i.text == commodity:
                    break

            if not IndexValue == 0:
                # length = int(float(length))
                # width = int(float(width))
                # height = int(float(height))
                # weight = int(float(weight))
                # pallets = int(float(pallets))
                # pieces = int(float(pieces))
                length = length.replace(".", "")
                width = width.replace(".", "")
                height = height.replace(".", "")
                # weight = weight.replace(".", "")
                # pallets = pallets.replace(".", "")
                # pieces = pieces.replace(".", "")
                weight = round(float(weight))
                pallets = round(float(pallets))
                pieces = round(float(pieces))
                if unitType.lower() == "pallets(40x48)":
                    length = "40"
                    width = "48"
                if wtUnits.lower() == "kg":
                    weight = round(int(float(weight)) * 2.20462)
                if dimUnit.lower() == "feet":
                    length = int(float(length)) * 12
                    width = int(float(width)) * 12
                    height = int(float(height)) * 12

                getCommodity = self.getText("(" + self._so_board_shipping_item_list + ")[" + str(IndexValue) + "]",
                                            "xpath")
                getWeight = self.getText("(" + self._so_board_weight + ")[" + str(IndexValue) + "]", "xpath")
                getUnitType = self.getText("(" + self._so_board_unitType + ")[" + str(IndexValue) + "]", "xpath")
                getPallet = self.getText("(" + self._so_board_pallet + ")[" + str(IndexValue) + "]", "xpath")
                getPiece = self.getText("(" + self._so_board_pieces + ")[" + str(IndexValue) + "]", "xpath")
                getLength = self.getText("(" + self._so_board_length + ")[" + str(IndexValue) + "]", "xpath")
                getWidth = self.getText("(" + self._so_board_width + ")[" + str(IndexValue) + "]", "xpath")
                getHeight = self.getText("(" + self._so_board_height + ")[" + str(IndexValue) + "]", "xpath")
                getClass = self.getText("(" + self._so_board_class + ")[" + str(IndexValue) + "]", "xpath")
                # getHazmat = self.getElementList("("+self._so_board_hazmat+")["+str(IndexValue)+"]", "xpath")
                # if len(getHazmat) > 0:
                #     haz = "yes"
                # else:
                #     haz = "no"

                if self.isElementPresent("(" + self._so_board_hazmat + ")[" + str(IndexValue) + "]", "xpath"):
                    haz = "yes"
                else:
                    haz = "no"

                compCommodity = self.validators.verify_text_match(getCommodity, commodity)
                compWeight = self.validators.verify_text_match(getWeight, weight)
                compUnitType = self.validators.verify_text_match(getUnitType, unitType)
                compPallet = self.validators.verify_text_match(getPallet, pallets)
                compPiece = self.validators.verify_text_match(getPiece, pieces)
                compLength = self.validators.verify_text_match(getLength, length)
                compWidth = self.validators.verify_text_match(getWidth, width)
                compHeight = self.validators.verify_text_match(getHeight, height)
                compClass = self.validators.verify_text_match(getClass, cls)
                compHz = self.validators.verify_text_match(haz, HzVal.lower())
                self.log.debug(f"Compare Result:- commodity : {str(compCommodity)}, Weight: {str(compWeight)}, "
                               f"UnitType: {str(compUnitType)}, Pallet: {str(compPallet)}, "
                               f"Pieces: {str(compPiece)}, Length: {str(compLength)}, Width: {str(compWidth)}, "
                               f"Height: {str(compHeight)}, class: {str(compClass)}, Hazmat: {str(compHz)}")

                if compCommodity \
                        and compWeight \
                        and compUnitType \
                        and compPallet \
                        and compPiece \
                        and compLength \
                        and compWidth \
                        and compHeight \
                        and compClass \
                        and compHz:
                    return True
                else:
                    return False

            else:
                self.log.error("Passed commodity not found in SO Board Popup")
                return False
        except Exception as E:
            self.log.error("Exception occurred in validateShippingItemData Method and Exception is : " + str(E))

    def getAccessorialList(self, breakupList):
        """
        Prepare the Accessorial list from breakup items
        :param breakupList: List of Breakup Items
        :return: Prepared List
        """
        try:
            self.log.info("Inside getAccessorialList Method")
            if not type(breakupList) == list:
                breakupList = ast.literal_eval(breakupList)
            self.log.info(breakupList)
            itemName = breakupList[0]
            self.log.info("List : " + str(itemName))
            itemName.remove("Initial Cost")
            itemName.remove("Discount")
            itemName.remove("Fuel Surcharge")
            return itemName
        except Exception as E:
            self.log.error("Exception occurred in getAccessorialList Method and exception is : " + str(E))

    def validateAndAddAccessorial(self, validateList, MainList):
        """
        Adding accessorial in List if not present in Main List
        :param validateList: List value have to compare with main list
        :param MainList: Main list from that comparing the value
        :return: Modified List
        """
        try:
            self.log.info("Inside validateAndAddAccessorial Method")
            # MainList = ast.literal_eval(MainList)
            for index in validateList:
                self.log.info("Index Value : " + str(index))
                if index not in MainList[0]:
                    MainList[0].append(index)
            self.log.info("Main List : " + str(MainList))
            return MainList
        except Exception as E:
            self.log.error("Exception occurred in validateAndAddAccessorial Method and Exception is : " + str(E))

    def validateAccessorialList(self, accessorialList):
        """
        Validating the Accessorial Name display on popup from passed list
        :return: True if match else False
        """
        try:
            self.log.info("Inside validateAccessorialList Method")
            # getting list after removing not required break up items
            getaccessorialList = self.getAccessorialList(accessorialList)
            getaccessorialList = [x.lower() for x in getaccessorialList]
            self.log.info("Count of Passed Accessorial : " + str(len(getaccessorialList)) +
                          " ,Passed Accessorial List : " + str(getaccessorialList))

            accessorialName = self.getElementList(self._so_board_get_accessorial, "xpath")
            self.log.info("From Application Accessorial Count : " + str(len(accessorialName)))
            flag = True
            if len(accessorialName) == len(getaccessorialList):
                for i in accessorialName:
                    self.driver.execute_script("arguments[0].scrollIntoView();", i)
                    self.log.info(i.text)
                    if i.text.lower() in getaccessorialList:
                        flag = flag and True
                    else:
                        self.log.error("Accessorial not found in passed list : " + str(i.text))
                        flag = flag and False
                self.log.debug("Flag Value from validateAccessorialList Method : " + str(flag))
                return flag
            return False
        except Exception as E:
            self.log.error("Exception occurred in validateAccessorialList Method and Exception is : " + str(E))

    def clickOnActionButtonOnSOBoardPopup(self, action, puNumber="", proNumber="", pickupDate="", acc="yes",
                                          dateFormat="%m/%d/%Y"):
        """
        Entering the passed value and clicking the passed action
        :param acc: whether accessirial present or not
        :param pickupDate: selection pickup Date
        :param dateFormat: Date passing format
        :param proNumber: PRO Number
        :param action: Button Name
        :param puNumber: Pickup Number
        :return: True if all as expected else False
        """
        try:
            date_to_be_selected = ""
            self.log.info("Inside clickOnActionButtonOnSOBoardPopup Method")
            if action.lower().replace(" ", "") == "manualdispatch":
                if not puNumber == "":
                    self.sendKeys(puNumber, self._so_board_pu_number, "xpath")
                self.pageVerticalScroll(self._so_board_dispatch_button, "xpath")
                self.elementClick(self._so_board_dispatch_button, "xpath")
                self.waitForElement(self._so_board_confirmation_popup, "xpath", "display", timeout=30)
                getMessage = self.getText(self._so_board_confirmation_msg, "xpath")
                self.elementClick(self._Ok_button_confirmation_msg, "xpath")
                self.waitForElement(self._so_board_dispatch_button, "xpath", "notdisplay", timeout=30)
                return self.validators.verify_text_match(getMessage, self._manual_dispatch_message)
            elif action.lower().replace(" ", "") == "finalize":
                flagFinalize = True
                if not proNumber == "":
                    self.sendKeys(proNumber, self._so_board_pro_number, "xpath")
                if not pickupDate == "":
                    self.pageVerticalScroll(self._so_board_pickup_date, "xpath")
                    self.elementClick(self._so_board_pickup_date, "xpath")
                    date_to_be_selected = self.selectDateBasedOnPassedValue(no_of_days=pickupDate,
                                                                            dateFormat=dateFormat)
                    self.log.info("date_to_be_selected : " + str(date_to_be_selected))
                    selected_date_value = self.getAttribute(self._so_board_pickup_date, "xpath", "value")
                    self.log.info("selected_datevalue : " + str(selected_date_value))
                    if selected_date_value == date_to_be_selected:
                        self.log.info("Dates are Matching")
                        flagFinalize = flagFinalize and True
                    else:
                        self.log.error("Dates are not matching.")
                        flagFinalize = flagFinalize and False

                self.pageVerticalScroll(self._so_board_finalize_button, "xpath")
                self.elementClick(self._so_board_finalize_button, "xpath")
                self.waitForElement(self._so_board_confirmation_popup, "xpath", "display", timeout=30)
                getMessage = self.getText(self._so_board_confirmation_msg, "xpath")
                self.elementClick(self._Ok_button_confirmation_msg, "xpath")
                self.waitForElement(self._so_board_dispatch_button, "xpath", "notdisplay", timeout=30)
                msgValidation = self.validators.verify_text_match(getMessage, self._finalize_message)
                return flagFinalize and msgValidation, date_to_be_selected

            elif action.lower().replace(" ", "") == "edidispatch":
                if not puNumber == "":
                    self.sendKeys(puNumber, self._so_board_pu_number, "xpath")
                if acc.lower() == "no":
                    self.pageVerticalScroll(self._so_board_dispatch_button, "xpath")
                    self.elementClick(self._so_board_dispatch_button, "xpath")
                    self.waitForElement(self._so_board_confirmation_popup, "xpath", "display", timeout=20)
                    getMessage = self.getText(self._so_board_confirmation_msg, "xpath")
                    self.elementClick(self._Ok_button_confirmation_msg, "xpath")
                    self.waitForElement(self._so_board_dispatch_button, "xpath", "notdisplay", timeout=20)
                    return self.validators.verify_text_match(getMessage, self._edi_dispatch_message)
                elif acc.lower() == "yes":
                    self.pageVerticalScroll(self._so_board_dispatch_button, "xpath")
                    self.elementClick(self._so_board_dispatch_button, "xpath")
                    self.waitForElement(self._so_board_confirmation_popup, "xpath", "display", timeout=20)
                    return self.ediDispatchAccessorialConfirmationPopup()
                else:
                    self.log.error("For EDI Dispatch wrong argument get passed.")

        except Exception as E:
            self.log.error(
                "Exception occurred in clickOnActionButtonOnSOBoardPopup Method and Exception is : " + str(E))

    def ediDispatchAccessorialConfirmationPopup(self, action="yes"):
        """
        Handling EDI Dispatch Accessorial Popup
        :param action: passing clicking Button
        :return: True if success else False
        """
        try:
            self.log.info("Inside ediDispatchAccessorialConfirmationPopup Method")
            self.waitForElement(self._so_board_confirmation_popup, "xpath", "display", timeout=30)
            getMessage = self.getText(self._so_board_confirmation_msg, "xpath")
            if action.lower() == "yes":
                self.elementClick(self._acc_yes_confirmation_popup, "xpath")
                self.waitForElement(self._acc_dispatch_confirmation_popup, "xpath", "notdisplay", timeout=30)
                acc_msg_result = self.validators.verify_text_match(getMessage, self._edi_dispatch_accessorial_msg)
            else:
                self.elementClick(self._acc_no_confirmation_popup, "xpath")

            self.waitForElement(self._so_board_confirmation_popup, "xpath", "display", timeout=30)
            getMessage = self.getText(self._so_board_confirmation_msg, "xpath")
            self.elementClick(self._Ok_button_confirmation_msg, "xpath")
            self.waitForElement(self._so_board_dispatch_button, "xpath", "notdisplay", timeout=30)

            return acc_msg_result and getMessage
        except Exception as E:
            self.log.error(
                "Exception Occurred in ediDispatchAccessorialConfirmationPopup Method and Exception is : " + str(E))

    def clickAndValidateShipmentStatusFilter(self, filter):
        """
        Method to Click on the filter based on the passed argument and validate the same filter is selected.
        :param filter Filter name for the selection
        :return: True if filter is selected else false
        """
        try:
            self.log.info("Inside clickAndValidateShipmentStatusFilter Method")
            filterStatus = False
            if filter.lower().replace(" ", "") == "allorders":
                self.waitForElement(self._soboard_shipmentstatusfilter_allorders, "xpath", timeout=20)
                self.elementClick(self._soboard_shipmentstatusfilter_allorders, "xpath")
                filterStatus = self.validators.verify_text_match(
                    self.getAttribute(self._soboard_shipmentstatusfilter_allorders, "xpath", "aria-selected"), "true")
            elif filter.lower().replace(" ", "") == "pending":
                self.waitForElement(self._soboard_shipmentstatusfilter_pending, "xpath", timeout=20)
                self.elementClick(self._soboard_shipmentstatusfilter_pending, "xpath")
                filterStatus = self.validators.verify_text_match(
                    self.getAttribute(self._soboard_shipmentstatusfilter_pending, "xpath", "aria-selected"), "true")
            elif filter.lower().replace(" ", "") == "dispatched":
                self.waitForElement(self._soboard_shipmentstatusfilter_dispatched, "xpath", timeout=20)
                self.elementClick(self._soboard_shipmentstatusfilter_dispatched, "xpath")
                filterStatus = self.validators.verify_text_match(
                    self.getAttribute(self._soboard_shipmentstatusfilter_dispatched, "xpath", "aria-selected"), "true")
            elif filter.lower().replace(" ", "") == "intransit":
                self.waitForElement(self._soboard_shipmentstatusfilter_intransit, "xpath", timeout=20)
                self.elementClick(self._soboard_shipmentstatusfilter_intransit, "xpath")
                filterStatus = self.validators.verify_text_match(
                    self.getAttribute(self._soboard_shipmentstatusfilter_intransit, "xpath", "aria-selected"), "true")
            elif filter.lower().replace(" ", "") == "delivered":
                self.waitForElement(self._soboard_shipmentstatusfilter_delivered, "xpath", timeout=20)
                self.elementClick(self._soboard_shipmentstatusfilter_delivered, "xpath")
                filterStatus = self.validators.verify_text_match(
                    self.getAttribute(self._soboard_shipmentstatusfilter_delivered, "xpath", "aria-selected"), "true")
            elif filter.lower().replace(" ", "") == "quotes":
                self.waitForElement(self._soboard_ltlfiltertab_quotes, "xpath", timeout=20)
                self.elementClick(self._soboard_ltlfiltertab_quotes, "xpath")
                filterStatus = self.validators.verify_text_match(
                    self.getAttribute(self._soboard_ltlfiltertab_quotes, "xpath", "aria-selected"), "true")
            self.soBoardLoadWait()
            return filterStatus
        except Exception as E:
            self.log.error(
                "Exception Occurred in clickAndValidateShipmentStatusFilter Method and Exception is : " + str(E))

    def validateNoRowsToShow(self):
        """
        Method to validate no rows to show when with or without filter applied
        :return: True if no rows to show else False
        """
        self.log.info("Inside validateNoRowsToShow Method")
        try:
            status = self.isElementPresent(self._so_no_rows, "xpath")
            if status:
                self.log.info("Data is not present in filter.")
                return True
            else:
                self.log.info("Data is present in filter.")
                return False
        except Exception as E:
            self.log.error("Exception occurred in validateNoRowsToShow Method and exception is : " + str(E))

    # ------------------------------------- Saved Quote page common Method-------------------------------------------

    def openSavedQuotePopup(self, quotevalue):
        """
        Method to open the saved quote popup in quote tab.
        :param quotevalue: quote value of shipment
        :return : True if saved quote popup is opened.
        """
        self.log.info("Inside openSavedQuotePopup Method and passed quote number : " + str(quotevalue))
        try:
            self.pageVerticalScroll(self._sq_data_base + quotevalue + "_referenceNumber", "id")
            self.element_double_click(locator=(By.ID, self._sq_data_base + quotevalue + "_referenceNumber"))
            self.waitForElement(self._sq_bookit_button, "xpath", event="display", timeout=20)
            status = self.isElementDisplayed(self._sq_bookit_button, "xpath")
            self.log.info("Inside openSavedQuotePopup Method status  : " + str(status))
            return status
        except Exception as E:
            self.log.error("Exception occurred in openSavedQuotePopup Method and exception is : " + str(E))
            return False

    def savedQuotePopupCustomerContactNameValidation(self, customercontactname):
        """
        Method to fetch customer contact name from the Saved Quote Popup and validate.
        :param: customercontactname : customer contact name
        :return True if customer contact name matches else False
        """
        self.log.info("Inside savedQuotePopupCustomerContactNameValidation Method, passed customercontactname : " + str(
            customercontactname))
        try:
            customerContactNameFromSQ = self.getText(self._sqpopup_customercontactname, "xpath")
            return self.validators.verify_text_match(customerContactNameFromSQ, customercontactname)
        except Exception as E:
            self.log.error(
                "Exception occurred in savedQuotePopupCustomerContactNameValidation Method and exception is : " + str(
                    E))
            return False

    def savedQuotePopupPickupDateValidation(self, pickupDate):
        """
        Method to fetch pickup date from the Saved Quote Popup and validate.
        :param: pickup : pickup date
        :return True if pickdate matches else False
        """
        self.log.info("Inside savedQuotePopupPickupDateValidation Method, passed pickup date : " + str(pickupDate))
        try:
            pickupDateFromSQ = self.getText(self._sqpopup_pickupdate, "xpath")
            return self.validators.verify_text_match(pickupDateFromSQ, pickupDate)
        except Exception as E:
            self.log.error(
                "Exception occurred in savedQuotePopupPickupDateValidation Method and exception is : " + str(E))
            return False

    def clickButtonInSavedQuotePopup(self, button):
        """
        Method to click on the button in the Saved Quote popup window.
        :param button: button name
        """
        self.log.info("Inside clickButtonInSavedQuotePopup Method and button: " + str(button))
        try:
            if button.lower() == "bookit":
                self.pageVerticalScroll(self._sq_bookit_button, "xpath")
                self.elementClick(self._sq_bookit_button, "xpath")
                self.waitForElement(self._sq_bookit_button, "xpath", "notdisplay")
            elif button.lower() == "sendemail":
                self.elementClick(self._sq_email_button, "xpath")
                self.waitForElement(self._sq_email_button, "xpath")
            else:
                self.log.error("Incorrect Button Name Passed")
        except Exception as E:
            self.log.error("Exception occurred in clickButtonInSavedQuotePopup Method and exception is : " + str(E))

    def validateShippingItemDataInSavedQuotePopup(self, commodity, weight, pallets, pieces, length, width,
                                                  height, HzVal, wtUnits, unitType, dimUnit):
        """
        Validating the Shipping Items Data in Saved Quote Popup
        :param commodity: Commodity Name
        :param weight: weight Value in lbs
        :param pallets: Pallet count
        :param pieces:  Pieces Count
        :param length: Length count in inches
        :param width: width in inches
        :param height: height in inches
        :param HzVal: hz value true or false
        :param wtUnits: wt unit type to conv value to lbs
        :param unitType: Package type
        :param dimUnit: dimension in inches
        :return: if all value match than True else False
        """
        try:
            self.log.info("Inside validateShippingItemDataInSavedQuotePopup Method")
            self.pageVerticalScroll(self._so_board_shipping_item_list, "xpath")
            commodityCount = self.getElementList(self._so_board_shipping_item_list, "xpath")
            IndexValue = 0
            # To get which row containing passed shipping data
            for i in commodityCount:
                IndexValue += 1
                if i.text == commodity:
                    break

            if not IndexValue == 0:
                length = round(float(length))
                width = round(float(width))
                height = round(float(height))
                weight = round(float(weight))
                pallets = round(float(pallets))
                pieces = round(float(pieces))
                if unitType.lower() == "pallets(40x48)":
                    length = "40"
                    width = "48"
                if wtUnits.lower() == "kg":
                    weight = round(int(float(weight)) * 2.20462)
                if dimUnit.lower() == "feet":
                    length = int(float(length)) * 12
                    width = int(float(width)) * 12
                    height = int(float(height)) * 12

                getCommodity = self.getText("(" + self._so_board_shipping_item_list + ")[" + str(IndexValue) + "]",
                                            "xpath")
                getWeight = self.getText("(" + self._so_board_weight + ")[" + str(IndexValue) + "]", "xpath")
                getPallet = self.getText("(" + self._so_board_pallet + ")[" + str(IndexValue) + "]", "xpath")
                getPiece = self.getText("(" + self._so_board_pieces + ")[" + str(IndexValue) + "]", "xpath")
                getLength = self.getText("(" + self._so_board_length + ")[" + str(IndexValue) + "]", "xpath")
                getWidth = self.getText("(" + self._so_board_width + ")[" + str(IndexValue) + "]", "xpath")
                getHeight = self.getText("(" + self._so_board_height + ")[" + str(IndexValue) + "]", "xpath")

                if self.isElementPresent("(" + self._so_board_hazmat + ")[" + str(IndexValue) + "]", "xpath"):
                    haz = "yes"
                else:
                    haz = "no"

                compCommodity = self.validators.verify_text_match(getCommodity, commodity)
                compWeight = self.validators.verify_text_match(getWeight, weight)
                compPallet = self.validators.verify_text_match(getPallet, pallets)
                compPiece = self.validators.verify_text_match(getPiece, pieces)
                compLength = self.validators.verify_text_match(getLength, length)
                compWidth = self.validators.verify_text_match(getWidth, width)
                compHeight = self.validators.verify_text_match(getHeight, height)
                compHz = self.validators.verify_text_match(haz, HzVal.lower())
                self.log.debug(f"Compare Result:- commodity : {str(compCommodity)}, Weight: {str(compWeight)}, "
                               f"Pallet: {str(compPallet)}, Pieces: {str(compPiece)}, Length: {str(compLength)}, "
                               f"Width: {str(compWidth)}, Height: {str(compHeight)}, Hazmat: {str(compHz)}")

                if compCommodity and compWeight and compPallet and compPiece and compLength and compWidth and compHeight and compHz:
                    return True
                else:
                    return False
            else:
                self.log.error("Passed commodity not found in SO Board Popup")
                return False
        except Exception as E:
            self.log.error(
                "Exception occurred in validateShippingItemDataInSavedQuotePopup Method and Exception is : " + str(E))
