import re
import datetime
from pages.basepage import BasePage
from selenium.webdriver.common.by import By

ORIGIN_ADDRESS = (By.ID, "routeDetails_LTL1-1393_RouteCard_Origin_Address")
ORIGIN_ADDRESS_ZIP = (By.ID, "routeDetails_LTL1-1393_RouteCard_Origin_City_State_Zip")

DESTINATION_ADDRESS = (By.ID, "routeDetails_LTL1-1393_RouteCard_Destination_Address")
DESTINATION_ADDRESS_ZIP = (By.ID, "routeDetails_LTL1-1393_RouteCard_Destination_City_State_Zip")


class SalesorderRouteDetailsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # ---------------------------------------------- Locators------------------------------------------------
    _so_routedetails_container = "//section[@id = 'routeDetails_LTL1-1393_RouteDetails_Wrapper']/preceding-sibling::div"
    _origin_company_name = "routeDetails_LTL1-1393_RouteCard_Origin_Company"
    _origin_appointment = "routeDetails_LTL1-1393_RouteCard_Origin_Appointment"
    _origin_phonenumber = "routeDetails_LTL1-1393_RouteCard_Origin_Phone"
    _origin_checkin = "routeDetails_LTL1-1393_RouteCard_Origin_Check_In"
    _origin_checkout = "routeDetails_LTL1-1393_RouteCard_Origin_Check_Out"

    _dest_company_name = "routeDetails_LTL1-1393_RouteCard_Destination_Company"
    _dest_appointment = "routeDetails_LTL1-1393_RouteCard_Destination_Appointment"
    _dest_phonenumber = "routeDetails_LTL1-1393_RouteCard_Destination_Phone"
    _dest_checkin = "routeDetails_LTL1-1393_RouteCard_Destination_Check_In"
    _dest_checkout = "routeDetails_LTL1-1393_RouteCard_Destination_Check_Out"

    _popup = "//div[contains(@class,'ReactModal__Content')]"
    _close_popup = _popup + "//button"
    _get_success_popup_msg = _popup + "//div[contains(@class,'_TextContainer')]"
    _save_success_msg = "Changes saved successfully."
    _date_picker_icon = "//div[contains(@class, 'DatepickerIcon')]"
    _origin_datepicker = "//div[@id='routeDetails_LTL1-1393_RouteCard_Origin_Container']" + _date_picker_icon
    _dest_datepicker = "//div[@id='routeDetails_LTL1-1393_RouteCard_Destination_Container']" + _date_picker_icon
    _origin_modal = "routeDetails_LTL1-1393_RouteCard_Origin_modal"
    _destination_modal = "routeDetails_LTL1-1393_RouteCard_Destination_modal"
    _popup_savebutton = "//span[contains(text(), 'Save & Close')]"
    _popup_dest_cancelbutton = "//div[@id='" + _destination_modal + "']//span[text()='Cancel']"
    _popup_origin_cancelbutton = "//div[@id='" + _origin_modal + "']//span[text()='Cancel']"
    _validation_warning_message = "//div[contains(@class, 'ValidationMessage')]"
    _actual_pickup_warning_message = "Actual Delivery date should be greater than actual pick up date."
    _requested_pickup_warning_message = "Actual Delivery date should be greater than requested pick up date."

    # ------------- Route Details Section methods in SO Page---------------------

    def ValidateRouteDetailsSection(self):
        """
        Validate the Route Details section Container
        :return: True if Text Match else False
        """
        try:
            self.log.info("Inside Route Details Section Method")
            textValue = self.getText(self._so_routedetails_container, "xpath")
            if "Route Details" in textValue:
                return True
            else:
                self.log.error("Route Details container text is not found")
                return False
        except Exception as E:
            self.log.error("Exception occurred in ValidateRouteDetailsSection Method and exception is : " + str(E))

    def validateRDSectionBasedOnPassedValue(self, passedValue, fieldName="", section=""):
        """
        Under this method, we are validating the Route Details section values in SO page for both origin and
        destination address.

        """
        self.log.info("Inside validateRDSectionBasedOnPassedValue method")
        try:
            getValue = ""
            appointmentvalue = ""

            if section == "" and fieldName.lower() == "transitdays":
                transitdays = re.sub("[()]", "", self.getText(self._so_routedetails_container + "/div", "xpath"))
                transitdaysarray = transitdays.rsplit(" ", 3)
                getValue = transitdaysarray[1]
                passedValue = passedValue.split(" ")[0]

            elif fieldName.lower() == "completeaddress":
                if section.lower() == "origin":
                    getValue = self.get_text(locator=ORIGIN_ADDRESS) + ", " + self.get_text(locator=ORIGIN_ADDRESS_ZIP)
                elif section.lower() == "destination":
                    getValue = self.get_text(locator=DESTINATION_ADDRESS) + ", " + self.get_text(locator=DESTINATION_ADDRESS_ZIP)
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))
                k = passedValue.rfind(",")
                passedValue = passedValue[:k] + "" + passedValue[k + 1:]

            elif fieldName.lower() == "companyname":
                if section.lower() == "origin":
                    getValue = self.getText(self._origin_company_name, "id")
                elif section.lower() == "destination":
                    getValue = self.getText(self._dest_company_name, "id")
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))

            elif fieldName.lower() == "phonenumber":
                if section.lower() == "origin":
                    getValue = re.sub("[() ]", "", self.getText(self._origin_phonenumber, "id")).replace("-", "")
                elif section.lower() == "destination":
                    getValue = re.sub("[() ]", "", self.getText(self._dest_phonenumber, "id")).replace("-", "")
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))
                passedValue = re.sub("[() ]", "", passedValue).replace("-", "")

            elif fieldName.lower() == "appointmentdate":
                self.log.info("Inside Appointment Date Condition")
                if section.lower() == "origin":
                    appointmentvalue = self.getText(self._origin_appointment, "id")
                elif section.lower() == "destination":
                    appointmentvalue = self.getText(self._dest_appointment, "id")
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))
                appointment = appointmentvalue.split('\n')
                appointmentarray = appointment[0].split(", ")
                getValue = appointmentarray[1]
                passedValuearray = passedValue.split("/")
                if int(passedValuearray[0]) < 10:
                    if not re.findall("^0", passedValuearray[0]):
                        passedValuearray[0] = "0" + passedValuearray[0]

                if int(passedValuearray[1]) < 10:
                    if not re.findall("^0", passedValuearray[1]):
                        passedValuearray[1] = "0" + passedValuearray[1]

                passedValue = passedValuearray[0] + "/" + passedValuearray[1]
                self.log.info("Passed Value : " + str(passedValue))

            elif fieldName.lower() == "appointmenttimings":
                if section.lower() == "origin":
                    appointmentvalue = self.getText(self._origin_appointment, "id")
                    appointment = appointmentvalue.split('\n')
                    getValue = appointment[1]
                    origin_timearray = passedValue.split(" - ")
                    # convert to 24 hour format
                    ready_time = datetime.datetime.strptime(origin_timearray[0], "%I:%M %p")
                    origin_ready_time = datetime.datetime.strftime(ready_time, "%H:%M")
                    close_time = datetime.datetime.strptime(origin_timearray[1], "%I:%M %p")
                    origin_close_time = datetime.datetime.strftime(close_time, "%H:%M")
                    passedValue = origin_ready_time + " - " + origin_close_time
                    self.log.info("Passed origin timings in 24hrs format : " + str(passedValue))
                elif section.lower() == "destination":
                    appointmentvalue = self.getText(self._dest_appointment, "id")
                    # Condition is kept due to bug (For destination address selected from address book, timings are not
                    # displayed)
                    if '\n' in appointmentvalue:
                        appointment = appointmentvalue.split('\n')
                        self.log.info("appointment : " + str(appointment[1]))
                        getValue = appointment[1]
                        dest_timearray = passedValue.split(" - ")
                        # convert to 24 hour format
                        ready_time = datetime.datetime.strptime(dest_timearray[0], "%I:%M %p")
                        dest_ready_time = datetime.datetime.strftime(ready_time, "%H:%M")
                        close_time = datetime.datetime.strptime(dest_timearray[1], "%I:%M %p")
                        dest_close_time = datetime.datetime.strftime(close_time, "%H:%M")
                        passedValue = dest_ready_time + " - " + dest_close_time
                        self.log.info("Passed dest timings in 24hrs format : " + str(passedValue))
                    else:
                        getValue = ""
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))

            elif fieldName.lower() == "checkin":
                if section.lower() == "origin":
                    getValue = self.getText(self._origin_checkin, "id")
                elif section.lower() == "destination":
                    getValue = self.getText(self._dest_checkin, "id")
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))

            elif fieldName.lower() == "checkout":
                if section.lower() == "origin":
                    getValue = self.getText(self._origin_checkout, "id")
                elif section.lower() == "destination":
                    getValue = self.getText(self._dest_checkout, "id")
                else:
                    self.log.error("Incorrect Section Name passed for field Name : " + str(fieldName) +
                                   " , passed section Name is : " + str(section))
            else:
                self.log.error(
                    "Either passed field Name value is incorrect : " + str(fieldName))
                return False

            self.log.info("Expected value : " + str(passedValue.lower()))
            self.log.info("Actual Value : " + str(getValue.lower()))

            self.log.debug("Get Value from application : " + str(getValue.lower()) + " , passed Value : " +
                           str(passedValue.lower()))
            return self.validators.verify_text_match(getValue.lower(), passedValue.lower())

        except Exception as E:
            self.log.error(
                "Exception occurred in validateRDSectionBasedOnPassedValue Method and Exception is : " + str(E))

    def get_actual_pickup_date(self):
        """Retrieving Actual Pick up Date Value"""
        try:
            self.log.info("Inside get_actual_pickup_date Method")
            complete_appointment_text = self.getText(self._origin_checkin, "id")
            if complete_appointment_text != "":
                appointment_value = complete_appointment_text.split('\n')
                appointment_date = appointment_value[0].split(", ")
                return appointment_date[1]
                # return complete_appointment_text
            else:
                self.log.debug("Atual Date field is empty.")
                return None
        except Exception as E:
            self.log.error("Exception occurred in get_actual_pickup_date Method and Exception is : " + str(E))

    def actualDateSelection(self, no_of_days, section):
        """Under this method, we are selecting actual date based on no of days and section passed"""
        self.log.info("Inside updateActualDate method")
        try:
            actual_check_in = ""
            # date_to_be_validated = ""
            actual_check_out = ""
            # verifiedSuccessMsg = False
            setStatusFlag = True
            if section.lower() == "origin":
                if not self.isElementDisplayed(self._origin_modal, "id"):
                    self.elementClick(self._origin_datepicker, "xpath")
                datepicker_popup = self.isElementDisplayed(self._origin_modal, "id")
            elif section.lower() == "dest" or section.lower() == "destination":
                if not self.isElementDisplayed(self._destination_modal, "id"):
                    self.elementClick(self._dest_datepicker, "xpath")
                datepicker_popup = self.isElementDisplayed(self._destination_modal, "id")
            else:
                self.log.error("Invalid section passed : " + str(section))
                return False

            if datepicker_popup:

                date_to_be_selected = self.selectDateBasedOnPassedValue(no_of_days=no_of_days)
                DateValue = str(date_to_be_selected).split('/')
                month_value = DateValue[0]
                day_value = DateValue[1]
                year_value = DateValue[2]

                self.log.info("date_to_be_selected : " + str(date_to_be_selected))
                weekday_value = self.util.get_week_day_value(date_to_be_selected)
                date_to_be_validated = weekday_value + ", " + month_value + "/" + day_value
                self.log.info("date_to_be_validated : " + str(date_to_be_validated))
                self.elementClick(self._popup_savebutton, "xpath")
                self.waitForElement(self._popup, "xpath", event="display", timeout=15)
                successMsg = self.getText(self._get_success_popup_msg, "xpath")
                verifiedSuccessMsg = self.validators.verify_text_match(successMsg, self._save_success_msg)
                self.elementClick(self._close_popup, "xpath")
            else:
                self.log.error("Date picker is not displayed")
                return False

            if section.lower() == "origin":
                actual_check_in = self.getText(self._origin_checkin, "id")
                actual_check_out = self.getText(self._origin_checkout, "id")
            elif section.lower() == "dest":
                actual_check_in = self.getText(self._dest_checkin, "id")
                actual_check_out = self.getText(self._dest_checkout, "id")

            verifiedActualCheckIn = self.validators.verify_text_match(actual_check_in, date_to_be_validated)
            verifiedActualCheckOut = self.validators.verify_text_match(actual_check_out, date_to_be_validated)

            if verifiedSuccessMsg and verifiedActualCheckIn and verifiedActualCheckOut:
                setStatusFlag = setStatusFlag and True
            else:
                setStatusFlag = setStatusFlag and False

            self.log.debug("verifiedSuccessMsg : " + str(verifiedSuccessMsg) + " , verifiedActualCheckIn : " +
                           str(verifiedActualCheckIn) + " , verifiedActualCheckOut : " + str(verifiedActualCheckOut))
            return year_value, setStatusFlag

        except Exception as E:
            self.log.error("Exception occurred in updateActualDate Method and Exception is : " + str(E))
            return False

    def verifyActualPickupAndDeliveryDateValidations(self):
        """Under this method we are validating all the warning messages and success messages on selection origin
        pickup and destination actual delivery date """
        self.log.info("Inside defaultActualDateValidation Method")
        try:
            returnedResultList = []
            setStatusFlag = True
            origin_appointment = self.getText(self._origin_appointment, "id").split('\n')
            origin_appointmentdate = origin_appointment[0].split(", ")
            self.log.info("Origin appointment date : " + str(origin_appointmentdate[1]))
            currentdate = self.util.getCurrentDate("%m/%d")

            # Validating warning message 1 and warning message 2
            if self.getText(self._origin_checkin, "id") == "" and self.getText(self._origin_checkout, "id") == "":
                delta = \
                    datetime.datetime.strptime(origin_appointmentdate[1], "%m/%d").date() - \
                    datetime.datetime.strptime(currentdate, "%m/%d").date()
                difference_days = delta.days
                self.log.info("difference_days : " + str(difference_days))

                # validating warning message 1 by selecting same date as Origin appointment date
                self.elementClick(self._dest_datepicker, "xpath")
                dateSelected = self.selectDateBasedOnPassedValue(no_of_days=difference_days)
                self.log.info("dateSelected : " + str(dateSelected))
                self.elementClick(self._popup_savebutton, "xpath")
                validation_warningmsg1 = self.getText(self._validation_warning_message, "xpath")
                verifiedWarningMessage1 = \
                    self.validators.verify_text_match(validation_warningmsg1, self._actual_pickup_warning_message)
                self.elementClick(self._popup_dest_cancelbutton, "xpath")

                # Validating Success message on choosing origin actual pickup date 1 day greater than Origin
                # appointment date
                no_of_days = abs(difference_days) + 1
                self.log.info("no_of_days to validate success message for delivery date : " + str(no_of_days))
                returnedResultList = self.actualDateSelection(no_of_days, "origin")
                self.log.info("returned year --- " + str(returnedResultList[0]))
                setStatusFlag = verifiedWarningMessage1 and returnedResultList[1]

            # Validation for dest actual delivery date
            self.elementClick(self._dest_datepicker, "xpath")
            datepicker_popup = self.isElementDisplayed(self._destination_modal, "id")
            if datepicker_popup:
                originalactual_check_in = self.getText(self._origin_checkin, "id").split('\n')
                originalactual_check_in_date = originalactual_check_in[0].split(", ")

                # Validating warning message 1 on choosing same date as origin appointment date
                delta = \
                    datetime.datetime.strptime(currentdate, "%m/%d").date() - \
                    datetime.datetime.strptime(origin_appointmentdate[1], "%m/%d").date()
                no_of_days = abs(delta.days)

                date_to_be_selected = self.selectDateBasedOnPassedValue(no_of_days=no_of_days)
                self.log.info("Date to be selected : " + str(date_to_be_selected) +
                              " Origin appointment date : " + str(origin_appointmentdate[1]))
                self.elementClick(self._popup_savebutton, "xpath")
                validation_msg1 = self.getText(self._validation_warning_message, "xpath")
                verifiedWarningMessage1 = \
                    self.validators.verify_text_match(validation_msg1, self._actual_pickup_warning_message)

                # Validating warning message 1 on choosing same date as origin actual pickup date
                delta = \
                    datetime.datetime.strptime(self.util.getCurrentDate("%m/%d/%Y"), "%m/%d/%Y").date() - \
                    datetime.datetime.strptime(originalactual_check_in_date[1] + "/" +
                                               returnedResultList[0], "%m/%d/%Y").date()
                no_of_days = abs(delta.days)
                self.log.info("No of days---- " + str(no_of_days))

                date_to_be_selected = self.util.getDateValueWithoutHolidayAndWeekends(no_of_days=no_of_days)
                selected_date = self.selectDateBasedOnPassedValue(passedData=date_to_be_selected)
                self.log.info("Date to be selected : " + str(selected_date) + " Origin actual pickup date : " +
                              str(originalactual_check_in_date[1]))
                self.elementClick(self._popup_savebutton, "xpath")
                validation_msg2 = self.getText(self._validation_warning_message, "xpath")
                verifiedWarningMessage2 = self.validators.verify_text_match(validation_msg2,
                                                                            self._actual_pickup_warning_message)

                # Validating success message on selecting dest actual delivery date 1 day greater than Origin actual
                # pickup date
                no_of_days = no_of_days + 1
                returnedValues1 = self.actualDateSelection(no_of_days, "dest")
                setStatusFlag = \
                    setStatusFlag and verifiedWarningMessage1 and verifiedWarningMessage2 and returnedValues1[1]

            else:
                self.log.error("Destination Date Picker is not displayed")
                setStatusFlag = False

            return setStatusFlag

        except Exception as E:
            self.log.error("Exception occurred in defaultActualDateValidation Method and Exception is : " + str(E))
            return False
