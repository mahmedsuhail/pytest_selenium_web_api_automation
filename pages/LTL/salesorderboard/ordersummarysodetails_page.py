from pages.basepage import BasePage
from selenium.webdriver.common.by import By

STR_COMMODITY_DESCRIPTION_ID = "orderSummary_LTL1-1394_commodity_item{{index}}_description"
CARRIER_NAME = (By.XPATH, "//div[contains(@class, 'CarrierName')]")


class OrderSummarySODetailsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # ---------------------------------------------- Locators------------------------------------------------
    _so_summary_container = "div[id='orderSummary_LTL1-1394_card'] div"
    _so_carrier_name = "div[id='orderSummary_LTL1-1394_card'] div[class*='CompanyName']"
    _so_commodity_base = "orderSummary_LTL1-1394_commodity_item"
    _so_shipmentValue_base = "orderSummary_LTL1-1394_commodity_item-"

    # ---------------------------------------------- Methods------------------------------------------------

    def validateSalesOrderSummarySection(self):
        """
        Validating the Sales Order Summary Section
        :return: True if Match else False
        """
        try:
            self.log.info("Inside validateSalesOrderSummarySection Method")
            summarySectionName = self.getText(self._so_summary_container, "css")
            return self.validators.verify_text_match(summarySectionName, "Order Summary")
        except Exception as E:
            self.log.error("Exception occurred in validateSalesOrderSummarySection Method and Exception is : " + str(E))

    def getOrderSummaryIndexValue(self, commodityName):
        """
        Get index value of SO Items based on Passed commodity Name
        :return: Index value of passed commodity
        """
        try:
            self.log.info("Inside getOrderSummaryIndexValue Method")

            getValue = self.getAttribute(
                "//div[@id='orderSummary_LTL1-1394_card'] //div[text()='" + commodityName + "']", "xpath", "id")
            index_value = getValue[getValue.index('_item') + 5:getValue.index('_description')]
            self.log.info("Index value : " + str(index_value))
            return index_value

        except Exception as E:
            self.log.error("Exception occurred in getOrderSummaryIndexValue Method and exception is : " + str(E))

    def validateSalesOrderSummaryData(self, fieldName, passedData, indexValue=0):

        try:
            self.log.info("Inside validateSalesOrderSummaryData Method, for field Name : " + str(fieldName) +
                          " and passed Data : " + str(passedData))
            if fieldName.lower() == "carrier":
                carrier_name = self.get_text(locator=CARRIER_NAME)
                return self.validators.verify_text_match(carrier_name, passedData)
            elif fieldName.lower() == "commodity":
                commodity = self.get_text(locator=(By.ID, STR_COMMODITY_DESCRIPTION_ID.replace("{{index}}", str(indexValue))))
                return self.validators.verify_text_match(commodity, passedData)
            elif fieldName.lower() == "shipmentvalue":
                shipValue = self.getText(self._so_shipmentValue_base + str(indexValue) + "_shipment_value", "id")
                return self.validators.verify_text_match(shipValue.replace("$", ""), passedData)
            elif fieldName.lower() == "unitcount":
                unitValue = self.getText(self._so_commodity_base + str(indexValue) + "_unit_count", "id")
                # passedValue = str(int(float(passedData)))
                # passedValue = passedData.replace(".", "")
                passedValue = round(float(passedData))
                self.log.info("Passed Data for Unit Count : " + str(passedValue))
                return self.validators.verify_text_match(unitValue, passedValue)
            elif fieldName.lower() == "miles":
                milesValue = self.getText(self._so_commodity_base + str(indexValue) + "_miles", "id")
                return self.validators.verify_text_match(milesValue, passedData)

        except Exception as E:
            self.log.error("Exception occurred in validateSalesOrderSummaryData Method and Exception is : " + str(E))

    def validateWeightSOSummaryData(self, passedData, weightUnit, indexValue=0):

        try:
            self.log.info("Inside validateWeightSOSummaryData Method, for Passed Data: " + str(passedData) +
                          ", UnitType : " + str(weightUnit))

            if weightUnit.lower() == "kg":
                wtUnits = 2.20462
                # TotalWeight = wtUnits * int(float(passedData))
                # TotalWeight = wtUnits * int(passedData.replace(".", ""))
                TotalWeight = wtUnits * int(round(float(passedData)))
                TotalWeight = "{:.2f}".format(TotalWeight)
            else:
                wtUnits = 1
                # TotalWeight = wtUnits * int(float(passedData))
                TotalWeight = wtUnits * int(round(float(passedData)))

            TotalWeight = round(float(TotalWeight))

            self.log.info("Total Weight : " + str(TotalWeight))

            weightValue = self.getText(self._so_commodity_base + str(indexValue) + "_weight", "id")
            self.log.info("From Application get value : " + str(weightValue))
            return self.validators.verify_text_match(weightValue.replace("lbs", "").strip(), str(TotalWeight))

        except Exception as E:
            self.log.error("Exception Occurred in validateWeightSOSummaryData Method and Exception is : " + str(E))
