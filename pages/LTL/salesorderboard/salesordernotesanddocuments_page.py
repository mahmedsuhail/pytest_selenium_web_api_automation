from pages.basepage import BasePage


class SalesorderNotesAndDocumentsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # ---------------------------------------------- Locators------------------------------------------------

    # ------------- Notes Section Locators in SO Page ---------------------

    # _sonote_section = "notes_LTL1-1399_component"
    _sonote_section = "Notesstyles__NotesContainer"
    # _sonote_sectionheader = "//div[@id='" + _sonote_section + "']//div[contains(@class,'sc-AxiKw')]"
    _sonote_sectionheader = "//div[contains(@class,'" + _sonote_section + "')]//div[contains(@class,'Title')]"
    _sonote_save_button = "notes_LTL1-1399_container_saveButton"
    _sonote_cancel_button = "notes_LTL1-1399_container_cancelButton"
    # _sonote_error_msg = "//div[@id='" + _sonote_section + "']//div[contains(@class,'ErrorMessage')]"
    _sonote_error_msg = "//div[contains(@class,'" + _sonote_section + "')]//div[contains(@class,'ErrorMessage')]"
    _sonote_text = "notes_LTL1-1399_container_textarea"
    _sonote_defaulttextmsg = "Enter text..."
    # _sonote_type_icon = "notes_LTL1-1399_select"
    _sonote_type_icon = "notes_LTL1-1399_container_select"
    _sonote_type_list = "//div[@id='" + _sonote_type_icon + "']//div[contains(@class,'-menu')]//div"
    _sonote_msg = "Please enter note text"
    _sonote_exceed_string = "A 500 word essay should consist of an introduction, a body, and a conclusion? Yes, all" \
                            " the above-mentioned sections are necessary, so we advise you to create an outline before " \
                            "you start writing your essay.Despite the fact that it's the shortest essay you may " \
                            "write, it requires you to spend a lot of time to make relevant corrections in the form" \
                            " of cutting. A 500-word essay is recognized as one of the hardest types of essays to" \
                            " edit.So be ready to spend about 3 hours writing it and 2 hours correcting it.However," \
                            " it takes so little time to proofread, isn't it great?"
    # _saved_sonote_section = "//div[contains(@class,'note-liststyle__NotesContainer')]"
    # _saved_sonote_section = "//div[@id='notes_LTL1-1399_container_notesContainer']"
    _saved_sonote_section = "//div[@id='notes_LTL1-1399_container_notesWrapper']"
    # _saved_sonote_base = "notes_LTL1-1399_container_notesContainer"
    _saved_sonote_base = "notes_LTL1-1399_container_item"
    _saved_sonote_type = "//div[contains(@class,'NoteHeader')]/div[1]"
    _saved_sonote_text = "//div[contains(@class,'NoteText')]"
    _so_note_spinner = "//div[text()='Loading']"

    # ------------- Documents Section Locators in SO Page ---------------------

    _sodoc_section = "documents_ltl1-1398_component"
    # _sodoc_header = "//div[@id='" + _sodoc_section + "']//div[contains(@class,'sc-AxiKw')]"
    _sodoc_header = "//div[@id='" + _sodoc_section + "']//div[contains(@class,'Title')]"
    _sodoc_docdeletebtn = "documents_ltl1-2635_deleteIcon"
    _sodoc_docaddbtn = "documents_ltl1-2635_addIcon"
    _sodoc_doc = "documents_ltl1-1398_docIcon_"
    _sodoc_reqvendocbtn = "documents_ltl1-2635_requestVendorDocsButton"
    _sodoc_reqdocwindow = "documents_ltl1-2635_requestVendorDocsRequestDocumentsModal"
    _sodoc_reqdocwindowcancelbtn = "documents_ltl1-2635_requestVendorDocs_cancelButton"
    _sodoc_reqdocwindowokbtn = "documents_ltl1-2635_requestVendorDocs_okButton"
    _sodoc_reqdocwindowheader = "//div[@id='" + _sodoc_reqdocwindow + "']//div[text()='Request Documents']"
    _sodoc_reqdocwindowpodcheckbox = "//input[@name='isPodChecked']/following-sibling::div"
    _sodoc_reqdocwindowacctermscondcheckbox = "//input[@name='isTocChecked']/following-sibling::div"
    _sodoc_reqdocwindowtermsandconditions = "//div[contains(@class,'request-vendor-docsstyle__HeaderWrapper')]"
    _sodoc_reqdocsavingfailure = "//div[contains(@class,'request-vendor-docsstyle__MessageDiv-SalesOrder')]"
    _sodoc_reqdocrecvconfirmpopwindow = "documents_ltl1-2635_requestVendorDocsRequestReceivedModal"
    _sodoc_reqdocrecvconfirmtxt = "//div[@id='" + _sodoc_reqdocrecvconfirmpopwindow \
                                  + "']//div[contains(@class,'request-vendor-docsstyle__ConfirmationTextWrapper')]"
    # _sodoc_reqdocrecvconfirmtxt = "//div[@id='" + _sodoc_reqdocrecvconfirmpopwindow + "']/div[2]/div/div[1]"
    _sodoc_reqdocreqrecvpopupokbtn = "documents_ltl1-2635_requestVendorDocsRequestReceivedOkButton"
    _sodoc_reqdocsavingfailuremsg = "Failed to save document request"
    _sodoc_reqdocwindowtermsandconditionsvalue = "Terms and Conditions:\n"\
                                                 "A manual document retrieval fee may apply where applicable."
    _sodoc_okbtn_checkedclassvalue = "sc-AxirZ kmiYJL"
    _sodoc_reqdocrecvconfirmtxtvalue = "Your request has been received. Any available document images\n"\
                                       "will be uploaded within 24-72 hrs."
    _sodoc_bol_and_label = "//div[text()='BOL & Label']"
    _sodoc_bill_of_lading = "//div[text()='Bill of Lading']"
    _sodoc_email_or_download_confirm_popup = "emailOrDownload_ltl1-2635_modal"
    _sodoc_email_or_download_confirm_popup_email_button = "emailOrDownload_ltl1-2635_emailButton"
    _sodoc_email_or_download_confirm_popup_download_button = "emailOrDownload_ltl1-2635_downloadButton"
    _sodoc_bol_and_label_email_confirm_popup = "bolLabel_ltl1-2635_modal"
    _sodoc_bol_and_label_email_email_button_confirm_popup = "bolLabel_ltl1-2635_emailButton"
    _sodoc_bol_and_label_email_cancel_button_confirm_popup = "bolLabel_ltl1-2635_cancelButton"
    _sodoc_bol_and_label_email_confirm_popup_no_of_labels_field = "bolLabel_ltl1-2635_textfield"
    _no_of_labels = "1"
    _sodoc_email_document_to_field = "(//div[contains(@class,'EmailPopupMainstyles__TextFieldContainer')])[1]" \
                                     "//div[contains(@class,'Selectstyles__StyledSelect')]"
    _sodoc_email_document_cancel_button = "//div[contains(@class,'EmailPopupMainstyles__ButtonWrapper')]//button[1]"

    # ---------------------------------------------- Methods------------------------------------------------

    # ------------- Notes Section methods in SO Page---------------------

    def getSelectedNoteTypeInSOPage(self):
        """
        Get selected Note Type
        :return: Selected Note type
        """
        self.log.info("Inside getSelectedNoteTypeInSOPage Method")
        try:
            return self.getText(self._sonote_type_icon, "id")
        except Exception as E:
            self.log.error("Method getSelectedNoteTypeInSOPage : Passed locator is wrong/got updated and the Exception is " + str(E))

    def defaultSOPageNotesContainerValidation(self):
        """
        Validating the Default Note section
        :return: True if all as expected else False
        """
        try:
            self.log.info("Inside defaultSOPageNotesContainerValidation Method")
            combinedResult = True
            expectedList = self.csvRowDataToList("sopagenotelist", "defaultlist.csv")
            actualNoteList = []

            self.pageVerticalScroll("//div[contains(@class,'" + self._sonote_section + "')]", "xpath")
            headerValue = self.getText(self._sonote_sectionheader, "xpath")
            if "Notes" == headerValue:
                combinedResult = combinedResult and True
            else:
                self.log.error("SalesOrder Page Notes container/text is not found")
                combinedResult = combinedResult and False

            defaultNoteTypeValue = self.getSelectedNoteTypeInSOPage()
            defaultNoteTypeResult = self.validators.verify_text_match(defaultNoteTypeValue, "User")
            self.log.info("Validating default selected Note type : " + str(defaultNoteTypeResult))
            combinedResult = combinedResult and defaultNoteTypeResult

            combinedResult = combinedResult and self.validators.verify_text_match(
                self.getAttribute(self._sonote_text, "id", "placeholder"), self._sonote_defaulttextmsg)
            self.log.info("combined Result : " + str(combinedResult))

            self.elementClick(self._sonote_type_icon, "id")
            self.waitForElement(self._sonote_type_list, "xpath", "display")
            elementList = self.getElementList(self._sonote_type_list, "xpath")

            for i in elementList[1:]:
                actualNoteList.append(i.text)

            self.log.info("Actual Note List: " + str(actualNoteList))
            self.log.info("Expected Note List : " + str(expectedList))
            noteListMatchResult = self.validators.verify_list_match(expectedList, actualNoteList)
            self.log.info("Note List Match: " + str(noteListMatchResult))
            combinedResult = combinedResult and noteListMatchResult

            for j in range(len(actualNoteList)):
                self.log.info("selected Note Type : " + str(actualNoteList[j]))
                self.elementClick("(" + self._sonote_type_list + ")[" + str(j + 2) + "]", "xpath")
                textValue = self.getSelectedNoteTypeInSOPage()
                self.log.info("textValue Result : " + str(textValue))
                combinedResult = combinedResult and self.validators.verify_text_match(textValue, actualNoteList[j])
                self.log.info("combined Result : " + str(combinedResult))

                if not j == len(actualNoteList) - 1:
                    self.elementClick(self._sonote_type_icon, "id")
                    self.waitForElement(self._sonote_type_list, "xpath", "display")

            self.log.info("Character Length : " + str(len(self._sonote_exceed_string)))
            self.sendKeys(self._sonote_exceed_string, self._sonote_text, "id")
            self.log.info("Notes String : " + str(len(self.getText(self._sonote_text, "id"))))
            if len(self.getText(self._sonote_text, "id")) == 500:
                self.log.info("Notes string length as expected.")
                combinedResult = combinedResult and True
            else:
                combinedResult = combinedResult and False

            self.log.info("Combined Final Result : " + str(combinedResult))
            return combinedResult
        except Exception as E:
            self.log.error("Method defaultSOPageNotesContainerValidation : Passed locator is wrong/got updated and the Exception is " + str(E))
            return False

    def selectNoteTypeInSOPage(self, noteType="User"):
        """
        Selecting the Note type as per the passed Value
        :param noteType: The Note type want to select
        :return: if as per passed value selected than True else False
        """
        self.log.info("Inside selectNoteType Method and Passed Note type : " + str(noteType))
        try:
            self.pageVerticalScroll(self._sonote_type_icon, "id")
            if not (self.getSelectedNoteTypeInSOPage() == noteType):
                self.elementClick(self._sonote_type_icon, "id")
                self.waitForElement(self._sonote_type_list, "xpath", "display")
                self.selectBasedOnText(self._sonote_type_list, "xpath", noteType)
            return self.validators.verify_text_match(self.getSelectedNoteTypeInSOPage(), noteType)
        except Exception as E:
            self.log.error(f"Method selectNoteTypeInSOPage : Passed noted type didnot got selected or passed locator is"
                           f" wrong/got updated and the Exception is {str(E)}")

    def validateSavedNotesInSOPage(self, passedNoteType, passedContent):
        """
        The method is to validate notes saved.
        :param passedContent: The Note Text expecting in Notes Section
        :param passedNoteType: The Note type expected to present
        :return: True if value as expected else false
        """
        self.log.info("Inside validateSavedNotesInSOPage Method and the passedNoteType is " + str(
            passedNoteType) + " and content is " + str(passedContent))
        try:
            notesSectionResult = False
            count = self.totalCountOfNoteList()
            for index in range(count):
                _savedNoteType = "//div[@id='" + self._saved_sonote_base + str(index) + "']" + self._saved_sonote_type
                getSavedNoteType = self.getText(_savedNoteType, "xpath")
                if passedNoteType.lower() == getSavedNoteType.lower():
                    _savedNoteText = "//div[@id='" + self._saved_sonote_base + str(index) + "']" + self._saved_sonote_text
                    notesSectionResult = self.validators.verify_text_match(self.getText(_savedNoteText, "xpath"), passedContent)
                    if notesSectionResult:
                        break
            return notesSectionResult
        except Exception as E:
            self.log.error("Method validateSavedNotesInSOPage : Passed locator is wrong/got updated and the "
                           "Exception is" + str(E))

    def addAndCancelNoteInSOPage(self, ButtonName, passedNoteType="", content="", verify="yes"):
        """
        Method is responsible to Add and Cancel feature of Notes
        :param passedNoteType: the Note type want to select
        :param verify: whether we have to verify validation message or not
        :param ButtonName: submit or Cancel excepted
        :param content: string which we want to add a notes
        :return:True if all fine else False
        """
        self.log.info("Inside addAndCancelNote Method, Passed ButtonName : " + str(ButtonName) +
                      ", passed Note Type : " + str(passedNoteType) + ", Content :" + str(content) +
                      ", verify: " + str(verify))
        try:
            combinedResult = True
            if not passedNoteType == "":
                noteTypeResult = self.selectNoteTypeInSOPage(passedNoteType)
                combinedResult = combinedResult and noteTypeResult
            selectedNoteType = self.getSelectedNoteTypeInSOPage()
            self.sendKeys(content, self._sonote_text, "id")
            count = self.totalCountOfNoteList()
            if ButtonName.lower() == "submit":

                self.elementClick(self._sonote_save_button, "id")
                self.waitForElement(self._so_note_spinner, "xpath", event="notdisplay", timeout=15)

                if verify == "yes" and self.isElementPresent(self._sonote_error_msg, "xpath"):
                    self.log.error("Error Message :" + str(self.getText(self._sonote_error_msg, "xpath")))
                    return False
                elif self.isElementPresent(self._saved_sonote_section, "xpath") and verify == "yes":
                    _savedNoteType = "//div[@id='" + self._saved_sonote_base + "0']" + self._saved_sonote_type
                    savedNoteType = self.validators.verify_text_match(self.getText(_savedNoteType, "xpath"), selectedNoteType)
                    self.log.info("Compare Note Type in saved Note : " + str(savedNoteType))
                    combinedResult = combinedResult and savedNoteType
                    _savedNoteText = "//div[@id='" + self._saved_sonote_base + "0']" + self._saved_sonote_text
                    savedNoteText = self.validators.verify_text_match(self.getText(_savedNoteText, "xpath"), content)
                    self.log.info("Compare Note Text in saved Note : " + str(savedNoteText))
                    combinedResult = combinedResult and savedNoteText
                    countAfterSave = self.totalCountOfNoteList()
                    if (count + 1) == countAfterSave:
                        combinedResult = combinedResult and True
                    else:
                        combinedResult = combinedResult and False

            elif ButtonName.lower() == "cancel":
                self.elementClick(self._sonote_cancel_button, "id")

                if verify == "yes" and self.isElementPresent(self._sonote_error_msg, "xpath"):
                    self.log.error("Error Message :" + str(self.getText(self._sonote_error_msg, "xpath")))
                    return False
                elif verify == "yes":
                    noteText = self.validators.verify_text_match(self.getText(self._sonote_text, "id"), "")
                    notetype = self.validators.verify_text_match(self.getSelectedNoteTypeInSOPage(), "User")
                    self.log.info("Cancel : Note Text result: " + str(noteText) + " , Note Type : " + str(notetype))
                    combinedResult = noteText and notetype
                    countAfterSave = self.totalCountOfNoteList()
                    if count == countAfterSave:
                        combinedResult = combinedResult and True
                    else:
                        combinedResult = combinedResult and False
            else:
                self.log.error("Incorrect Button Name Passed.")
                combinedResult = combinedResult and False
            self.log.info("combined Result from add cancel Note: " + str(combinedResult))
            return combinedResult

        except Exception as E:
            self.log.error("Method addAndCancelNoteInSOPage : Passed locator is wrong/got updated and the Exception is " + str(E))

    def totalCountOfNoteList(self):
        """
        Method to get the count of the saved notes from the notes list.
        :return: total count of the saved notes
        """
        try:
            elementList = self.getElementList(self._saved_sonote_section + "/child::div", locatorType="xpath")
            noOfNotesSaved = len(elementList)
            self.log.info("Total no. of notes saved in the Notes list : " + str(noOfNotesSaved))
            return noOfNotesSaved
        except Exception as E:
            self.log.error("Method totalCountOfNoteList : Passed locator is wrong/got updated and the Exception is " + str(E))

    def verifyNotesValidationInSOPage(self):
        """
        Verify the Notes Validation Message as Passed
        :return: True if Match else False
        """
        try:
            self.log.info("Inside verifyNotesValidationInSOPage Method")
            ErrorMessage = self.getText(self._sonote_error_msg, "xpath")
            return self.validators.verify_text_match(ErrorMessage, self._sonote_msg)
        except Exception as E:
            self.log.error("Method verifyNotesValidationInSOPage : Passed locators is wrong/updated and the Exception : " + str(E))

    # ------------- Documents Section methods in SO Page---------------------

    def validateDefaultDocumentSection(self, withorwithoutinsurance):
        """
        Method to validate default document container section & request vendor documents window.
        :param: withorwithoutinsurance - bol booked with insurance or without insurance.
        :return: True if all parameters are as expected otherwise False.
        """
        self.log.info("Inside validateDefaultDocumentSection Method and the insurance status for BOL is " + str(withorwithoutinsurance))
        try:
            combinedResult = True
            self.pageVerticalScroll(self._sodoc_section, "id")

            # Document Header section
            headerTextResult = self.validators.verify_text_match(self.getText(self._sodoc_header, "xpath"), "Documents")
            self.log.info("headerTextResult : " + str(headerTextResult))
            combinedResult = combinedResult and headerTextResult

            # Delete Button
            deletebtnAtDocHeaderResult = self.isElementPresent(self._sodoc_docdeletebtn, "id")
            self.log.info("deletebtnAtDocHeaderResult : " + str(deletebtnAtDocHeaderResult))
            combinedResult = combinedResult and deletebtnAtDocHeaderResult

            # Add Button
            addbtnAtDocHeaderResult = self.isElementPresent(self._sodoc_docaddbtn, "id")
            self.log.info("addbtnAtDocHeaderResult : " + str(addbtnAtDocHeaderResult))
            combinedResult = combinedResult and addbtnAtDocHeaderResult

            # Document List section
            docListFromUI = []
            docList = self.getElementList("//div[contains(@id,'" + self._sodoc_doc + "')]/parent::div", locatorType="xpath")
            for index in docList:
                docListFromUI.append(index.text)

            expDocListFromUI = self.csvRowDataToList("expsodoclist", "defaultlist.csv")

            if withorwithoutinsurance.lower() == "no":
                expDocListFromUI.remove('Insurance Certificat...')

            self.log.info("Expected List for Documents : " + str(expDocListFromUI))
            self.log.info("Actual List from UI for Documents : " + str(docListFromUI))
            docsListResult = self.validators.verify_list_match(expDocListFromUI, docListFromUI)
            self.log.info("docsListResult : " + str(docsListResult))
            combinedResult = combinedResult and docsListResult

            # Request Vendor Documents Window Validation
            # Clicking Request Vendor Docs button
            self.elementClick(self._sodoc_reqvendocbtn, locatorType="id")
            self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="display")

            # Request Document Window Header
            docSectionHeaderResult = self.isElementPresent(self._sodoc_reqdocwindowheader, "xpath")
            self.log.info("docSectionHeaderResult : " + str(docSectionHeaderResult))
            combinedResult = combinedResult and docSectionHeaderResult

            # Terms & Conditions Text section
            termsAndConditionsValueResult = self.validators.verify_text_match(
                self.getText(self._sodoc_reqdocwindowtermsandconditions, "xpath"),
                self._sodoc_reqdocwindowtermsandconditionsvalue)
            self.log.info("termsAndConditionsValueResult : " + str(termsAndConditionsValueResult))
            combinedResult = combinedResult and termsAndConditionsValueResult

            # Checkbox Validation (unchecked state)
            podValue = self.isElementPresent(self._sodoc_reqdocwindowpodcheckbox + "/*", "xpath")
            termsAndConditionsValue = self.isElementPresent(self._sodoc_reqdocwindowacctermscondcheckbox + "/*", "xpath")
            if not podValue and not termsAndConditionsValue:
                combinedResult = combinedResult and True
                self.log.info("Checkboxes are in un-checked state and the combined result is " + str(combinedResult))
            else:
                combinedResult = combinedResult and False
                self.log.error("Checkbox(es) are in checked state and the combined result is " + str(combinedResult))

            # ok button validation (in disabled state)
            okButtonValue = self.getAttribute(self._sodoc_reqdocwindowokbtn, locatorType="id", attributeType="data-disabled")
            # if okButtonValue is not self._sodoc_okbtn_checkedclassvalue:
            if okButtonValue == "true":
                combinedResult = combinedResult and True
                self.log.info("OK button is in disabled state and the combined result is " + str(combinedResult))
            else:
                combinedResult = combinedResult and False
                self.log.error("Ok button is in highlighted and the combined result is " + str(combinedResult))

            # Clicking the checkboxes of Proof of Delivery & Accept Terms & Conditions
            self.elementClick(self._sodoc_reqdocwindowpodcheckbox, locatorType="xpath")
            self.elementClick(self._sodoc_reqdocwindowacctermscondcheckbox, locatorType="xpath")

            # Checkbox Validation (checked state)
            podCheckboxCheckedValue = self.isElementPresent(self._sodoc_reqdocwindowpodcheckbox + "/*", "xpath")
            termsAndConditionsCheckboxCheckedValue = self.isElementPresent(self._sodoc_reqdocwindowacctermscondcheckbox + "/*", "xpath")
            if podCheckboxCheckedValue and termsAndConditionsCheckboxCheckedValue:
                combinedResult = combinedResult and True
                self.log.info("After checkboxes checked,Checkboxes are in checked state and the combined result is " + str(combinedResult))
            else:
                combinedResult = combinedResult and False
                self.log.error("After checkboxes checked,Checkbox(es) are in un-checked state and the combined result is " + str(combinedResult))

            # OK Button Validation After the checkboxes checked(Highlighted state)
            okButtonStatusAfterCheckboxChecked = self.getAttribute(self._sodoc_reqdocwindowokbtn, locatorType="id", attributeType="data-disabled")
            # if okButtonStatusAfterCheckboxChecked == self._sodoc_okbtn_checkedclassvalue:
            if okButtonStatusAfterCheckboxChecked == "false":
                combinedResult = combinedResult and True
                self.log.info("OK button is in enabled state and the combined result is " + str(combinedResult))
            else:
                combinedResult = combinedResult and False
                self.log.error("OK button is in disabled state and the combined result is " + str(combinedResult))

            # Cancel button
            cancelbtnStatus = self.isElementPresent(self._sodoc_reqdocwindowcancelbtn, "id")
            self.log.info("Cancel Button Display Status : " + str(cancelbtnStatus))
            combinedResult = combinedResult and cancelbtnStatus

            self.elementClick(self._sodoc_reqdocwindowcancelbtn, locatorType="id")
            self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="notdisplay")

            self.log.info("validateDefaultDocumentSection - Combined Final Result : " + str(combinedResult))
            return combinedResult

        except Exception as E:
            self.log.error("Method validateDefaultDocumentSection : Passed locator is wrong/got updated and the Exception is " + str(E))

    def requestVendorDocumentsBehaviour(self, ButtonName):
        """
        Method to execute the different behaviour of the button in the request vendor document window.
        :param: ButtonName - Based on the passed button name respective operations & validations are done
        :return: True if all parameters are as expected otherwise False.
        """
        self.log.info("Inside requestVendorDocumentsBehaviour Method")
        try:
            combinedResult = True
            self.pageVerticalScroll(self._sodoc_section, "id")

            # Clicking Request Vendor Docs button
            self.elementClick(self._sodoc_reqvendocbtn, locatorType="id")
            self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="display")

            # Clicking the checkboxes of Proof of Delivery & Accept Terms & Conditions
            self.elementClick(self._sodoc_reqdocwindowpodcheckbox, locatorType="xpath")
            self.elementClick(self._sodoc_reqdocwindowacctermscondcheckbox, locatorType="xpath")

            if ButtonName.lower() == "submit":

                # Clicking OK button in Request Documents Window
                self.elementClick(self._sodoc_reqdocwindowokbtn, locatorType="id")
                self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="notdisplay", timeout=25)
                if self.isElementPresent(self._sodoc_reqdocwindow, locatorType="id"):
                    reqDocSavingFailureMsgFromUI = self.getText(self._sodoc_reqdocsavingfailure, "xpath")
                    errorMsgStatus = self.validators.verify_text_match(reqDocSavingFailureMsgFromUI,
                                                                       self._sodoc_reqdocsavingfailuremsg)
                    combinedResult = combinedResult and not errorMsgStatus
                    self.elementClick(self._sodoc_reqdocwindowcancelbtn, locatorType="id")
                    self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="notdisplay")

                else:
                    self.waitForElement(self._sodoc_reqdocrecvconfirmpopwindow, locatorType="id", event="display",
                                        timeout=25)
                    self.pageVerticalScroll(self._sodoc_reqdocrecvconfirmtxt, "xpath")
                    confirmTextElementDispalyResult = self.isElementDisplayed(self._sodoc_reqdocrecvconfirmtxt, "xpath")
                    self.log.info("confirmTextElementDispalyResult : " + str(confirmTextElementDispalyResult))
                    reqDocRecvConfirmtxtFromUI = self.getText(self._sodoc_reqdocrecvconfirmtxt, "xpath")
                    confirmationText = self.validators.verify_text_match(reqDocRecvConfirmtxtFromUI,
                                                                         self._sodoc_reqdocrecvconfirmtxtvalue)
                    self.log.info("Request received Confirmation text validation : " + str(confirmationText))
                    combinedResult = combinedResult and confirmationText
                    self.elementClick(self._sodoc_reqdocreqrecvpopupokbtn, locatorType="id")
                    self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="notdisplay")

            elif ButtonName.lower() == "cancel":

                # Clicking Cancel button in Request Documents Window
                self.elementClick(self._sodoc_reqdocwindowcancelbtn, locatorType="id")
                self.waitForElement(self._sodoc_reqdocwindow, locatorType="id", event="notdisplay")

            else:
                self.log.error("Incorrect Button Name Passed.")
                combinedResult = combinedResult and False
            self.log.info("requestVendorDocumentsBehaviour - combined Result : " + str(combinedResult))
            return combinedResult

        except Exception as E:
            self.log.error("Method requestVendorDocumentsBehaviour : Passed locator is wrong/got updated and "
                           "the Exception is " + str(E))

    def navigate_to_email_document_popup(self, button_name):
        """
        Method to navigate to the respective email document popup based on the button passed.
        :param: ButtonName - Based on the passed button name respective operations are done
        """
        self.log.info("Inside navigate_to_email_document_popup Method and the button name is " + str(button_name))
        try:
            self.pageVerticalScroll(self._sodoc_section, "id")
            if button_name.lower().replace(" ", "") == "bolandlabel":
                self.elementClick(self._sodoc_bol_and_label, locatorType="xpath")
                self.waitForElement(self._sodoc_email_or_download_confirm_popup, locatorType="id", event="display")
                self.elementClick(self._sodoc_email_or_download_confirm_popup_email_button, locatorType="id")
                self.waitForElement(self._sodoc_bol_and_label_email_confirm_popup, locatorType="id", event="display")
                self.sendKeys(self._no_of_labels,
                              self._sodoc_bol_and_label_email_confirm_popup_no_of_labels_field, "id")
                self.elementClick(self._sodoc_bol_and_label_email_email_button_confirm_popup, locatorType="id")
                self.waitForElement(self._sodoc_email_document_to_field, locatorType="xpath", event="display")
            elif button_name.lower().replace(" ", "") == "billoflading":
                self.elementClick(self._sodoc_bill_of_lading, locatorType="xpath")
                self.waitForElement(self._sodoc_email_or_download_confirm_popup, locatorType="id", event="display")
                self.elementClick(self._sodoc_email_or_download_confirm_popup_email_button, locatorType="id")
                self.waitForElement(self._sodoc_email_document_to_field, locatorType="xpath", event="display")
            else:
                self.log.error("Incorrect Button Name Passed.")
        except Exception as E:
            self.log.error("Method navigate_to_email_document_popup : Passed locator is wrong/got updated "
                           "and the Exception is " + str(E))

    def validate_email_popup_with_prepopulated_email(self):
        """
        Method to validate whether To field is prepopulated with email address and not with NA or empty
        :return: True if email is present in the To field
        """
        self.log.info("Inside validate_email_popup_with_prepopulated_email Method")
        try:
            to_field_email_address = self.getText(self._sodoc_email_document_to_field, "xpath")
            if to_field_email_address != "NA" or to_field_email_address != "":
                self.elementClick(self._sodoc_email_document_cancel_button, locatorType="xpath")
                return True
            else:
                self.elementClick(self._sodoc_email_document_cancel_button, locatorType="xpath")
                return False
        except Exception as E:
            self.log.error("Method validate_email_popup_with_prepopulated_email : Passed locator is wrong/got updated "
                           "and the Exception is " + str(E))
