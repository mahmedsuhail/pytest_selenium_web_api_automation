import re

from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage


class SalesOrderDetailsPage(SalesorderNotesAndDocumentsPage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.driver = driver
        self.log = log

    # ---------------------------------------------- Locators------------------------------------------------
    _so_page = "//h1[text()='Sales Order']"
    _so_bol_label = _so_page + "/following-sibling::div"
    # _so_network_container = "div[id='networkInfo_LTL1-1392_networkContainer'] div[class*='sc-AxiKw']"
    _so_network_container = "div[id='networkInfo_LTL1-1392_networkContainer'] div[class*='Title']"

    _so_customer_value = "networkInfo_LTL1-1392_textValue1"

    _so_serviceType_value = "networkInfo_LTL1-1392_textValue2"

    _so_createdDate_value = "networkInfo_LTL1-1392_textValue3"

    _so_generatedOn_value = "networkInfo_LTL1-1392_textValue4"

    _so_contact_value = "networkInfo_LTL1-1392_textValue5"

    _so_ratingMethod_value = "networkInfo_LTL1-1392_textValue6"

    _so_quoteRequest_value = "networkInfo_LTL1-1392_textValue7"

    _so_salesRep_value = "networkInfo_LTL1-1392_textValue8"
    # _so_orderBoard_button = "//span[text()='Order Board']/parent::button"
    _so_orderBoard_button = "footer_ltl1-2633_orderBoardButton"
    _so_page_spinner = "//div[text()='Loading']"

    # -------------------Status section locator------------------------------------------
    _so_status_container = "div[id='statuses_LTL1-1395_statusesContainer'] div[class*='sc-AxiKw']"
    _so_process_status = "statuses_LTL1-1395_statusValue1"
    _so_invoice_status = "statuses_LTL1-1395_statusValue2"
    _so_vendor_bill_status = "statuses_LTL1-1395_statusValue3"
    _so_edi_status = "statuses_LTL1-1395_statusValue4"
    _so_tracking_status = "statuses_LTL1-1395_statusValue5"
    _so_choosable_tracking_status = "//div[@id ='statuses_LTL1-1395_statusesContainer']//span"
    _so_status_dropdown = "statuses_LTL1-1395_statusValue5_ToggleSelect"
    _so_status_popup = "//div[@id = 'statuses_LTL1-1395_statusValue5_PopperContainer']"
    _so_status_selection = _so_status_popup + "//li[contains(text(),'"

    # ----------------------Identifier Locator---------------------------------------------
    _so_identifier_type = "identifiers_TL-175_identifiers_LTL1-1397_identifier-type_"
    _so_identifier_value = "identifiers_TL-175_identifiers_LTL1-1397_identifier-value_"
    _so_identifier_value_part_2 = "']/parent::div/following-sibling::div/span"
    _so_identifier_section = "identifiers_TL-175_identifiers_LTL1-1397_identifiersContainer"
    _so_identifier_editbtn = "identifiers_TL-175_identifiers_LTL1-1397_edit-icon"
    _so_identifier_row = "identifiers_TL-175_identifiers_LTL1-1397_row-"
    _so_identifier_editidentifiertype = "identifiers_TL-175_identifiers_LTL1-1397_identifier-type_select_"
    _so_identifier_editidentifiertypevalue = "identifiers_TL-175_identifiers_LTL1-1397_identifier-value_textField_"
    _so_identifier_editidentifiertypelist = "//div[contains(@class,'-menu')]//div"
    _so_identifier_cancelbtn = "identifiers_TL-175_identifiers_LTL1-1397_cancel-button"
    _so_identifier_saveandconfirmbtn = "identifiers_TL-175_identifiers_LTL1-1397_save-button"
    _so_identifier_addbtn = "identifiers_TL-175_identifiers_LTL1-1397_add-icon"
    _so_identifier_deletebtn = "identifiers_TL-175_identifiers_LTL1-1397_delete-icon_"
    _so_identifier_exceed_string = "testing the 50 character validation, in the field!@ to make sure its correct."
    _so_identifier_exceed_number = "123456789012345678901234567890123456789012345678901234567890"

    # Identifier locators with xpath

    # _so_identifier_typeandvalue = "//span[contains(@class,'Identifiersstyle__TextValue')]"
    # _so_identifier_value_part_2 = "']/parent::div/following-sibling::div/span"
    # _so_identifier_section = "identifiers_TL-175_identifiers_LTL1-1397_identifiersContainer"
    # _so_identifier_editbtn = "//div[contains(@class,'Identifiersstyle__EditButton')]"
    # _so_identifier_row = "//div[contains(@class,'Identifiersstyle__Row')]"
    # _so_identifier_editidentifierheader = "//div[contains(@class,'Identifiersstyle__EditIdentifiersHeader')]"
    # _so_identifier_editidentifiertype = "//following-sibling::div[contains(@class,'Selectstyles__StyledSelect')]"
    # _so_identifier_editidentifiertypevalue = "//following-sibling::div/input"
    # _so_identifier_editidentifiertypelist = "//div[contains(@class,'-menu')]//div"
    # _so_editidentifiertypelist = "/parent::div//following-sibling::div[contains(@class,'-menu')]//div"
    # _so_identifier_cancelbtn = _so_identifier_editidentifierheader + \
    #                            "//span[contains(text(),'Cancel')]/parent::button"
    # _so_identifier_saveandconfirmbtn = "//span[contains(text(),'Save & confirm')]/parent::button"
    # _so_identifier_addbtn = "//div[contains(@class,'Identifiersstyle__AddIdentifierButton')]"
    # _so_identifier_deletebtn ="//div[contains(@class,'Identifiersstyle__DeleteButton')]"
    # _so_editidentifier_selecttype = "//div[text()='Select Type']/parent::div"
    # _so_identifier_exceed_string = "testing the 50 character validation, in the field!@ to make sure its correct."
    # _so_identifier_exceed_number = "123456789012345678901234567890123456789012345678901234567890"

    # -----------------------SO Page Buttons---------------------------------------------
    _manual_dispatch = "footer_ltl1-2633_manualDispatchButton"
    _edi_dispatch = "footer_ltl1-2633_ediDispatchButton"
    _cancel_order_button = "footer_ltl1-2633_cancelOrderButton"
    _createneworder_button = "footer_ltl1-2633_createNewOrderButton"

    _popup = "//div[contains(@class,'ReactModal__Content')]"
    _get_popup_msg = _popup + " //div[contains(@class,'_Message')]"
    _close_popup = _popup + "//button"
    _cancel_confirm_yes = _popup + "//span[text()='Yes']/parent::button"
    _cancel_confirm_no = _popup + "//span[text()='No']/parent::button"
    # _cancelFlow_button_submit = _popup + "//span[text()='Submit']/parent::button"
    _cancelFlow_button_submit = "cancelOrder_ltl1-2633_reasonSubmitButton"
    # _cancelFlow_button_cancel = _popup + "//span[text()='Cancel']/parent::button"
    _cancelFlow_button_cancel = "cancelOrder_ltl1-2633_reasonCancelButton"
    _cancel_success_popup = "//div[text()='Cancellation Successful']/parent::div"

    _cancelFlow_newso_radio = "//input[@value='New Sales Order (BOL) Created.']/following-sibling::label"
    _cancelFlow_bycustomer_radio = "//input[@value='Order Canceled by the customer']/following-sibling::label"
    _cancel_bol_field = "//span[text()='Please enter the BOL#']/following-sibling::div/input"
    _get_success_popup_msg = _popup + "//div[contains(@class,'_TextContainer')]"

    _copy_order_button = "footer_ltl1-2633_copyOrderButton"
    _confirm_copy_popup = "copyOrder_ltl1-2633_modal"
    _create_order_popup = "createOrder_ltl1-2633_modal"
    _get_copy_popup_msg = "div[id='" + _confirm_copy_popup + "'] div[class*='copy-order'] div:first-child"
    _copy_confirm_yes_button = "copyOrder_ltl1-2633_secondaryButton"
    _copy_confirm_no_button = "copyOrder_ltl1-2633_primaryButton"
    _get_create_order_popup_msg = "div[id='" + _create_order_popup + "'] div[class*='copy-order']:first-child"
    _go_to_bol_button = "createOrder_ltl1-2633_secondaryButton"
    _so_board_button = "createOrder_ltl1-2633_primaryButton"

    # Messages Locator
    _manual_dispatch_msg = "Manual Dispatch Successful."
    _edi_dispatch_msg = "EDI Dispatch Successful."
    _warning_msg = "You are canceling an order that has already been dispatched. You must still contact the carrier " \
                   "in order to completely cancel this shipment. Please notate any resulting cancellation charges."
    _cancel_confirmation_msg = "Are you sure you want to cancel this Sales Order?"
    _cancel_reason_msg = "You are about to cancel this sales order. Please confirm the reason for cancellation:"
    _cancel_success_msg = "Sales order cancellation successful."
    _save_success_msg = "Changes saved successfully."
    _save_failure_msg = "Changes not saved successfully."
    _copy_confirm_msg = "Do you want to copy this order?"
    _order_create_msg = "^Order #.* has been created!"

    # Update Order Locators

    _updateorder_button = "footer_LTL1-875_updateOrderButton"
    _editsalesorder_button = "footer_ltl1-2633_editSalesOrderButton"
    _createquotebasecontainer = "//div[@id='CreateQuoteBase-container']"
    _routedetailssection = _createquotebasecontainer + "//div[text()='Route Details']"
    _updatemodal = "footer_LTL1-950_saveChangesModal"
    _updatemodal_confirmbutton = "footer_LTL1-950_saveChangesModalButtonYes"

    # ---------------------------------------------- Methods------------------------------------------------

    def clickAndValidateCancelFlow(self, userName, insValue, reason="bycustomer", action="yes"):
        """
        Clicking cancel button and validating the Cancel flow.
        :return: True if All Condition fulfill else False
        """
        try:
            flag = True
            self.log.info("Inside clickAndValidateCancelFlow Method")
            getTrackingStatus = self.getText("//div[contains(@id,'" + self._so_tracking_status + "')]", "xpath")
            getProcessStatus = self.getText(self._so_process_status, "id")
            self.waitForElement(self._cancel_order_button, "id")
            self.elementClick(self._cancel_order_button, "id")

            # Based on status value preparing for the display popup
            if not getTrackingStatus.lower() == "pending":
                self.waitForElement(self._popup, "xpath", event="display")
                message = self.getText(self._get_popup_msg, "xpath")
                getValue = self.validators.verify_text_match(message, self._warning_msg)
                self.elementClick(self._close_popup, "xpath")
                flag = flag and getValue
                self.log.debug("Other than Pending Status Cancel popup Message get Validate : " + str(flag))
            else:
                self.waitForElement(self._popup, "xpath", "display")
                message = self.getText(self._get_popup_msg, "xpath")
                getValue = self.validators.verify_text_match(message, self._cancel_confirmation_msg)

                if action.lower() == "yes":
                    self.elementClick(self._cancel_confirm_yes, "xpath")
                    self.waitForElement(self._popup, "xpath", event="notdisplay")
                else:
                    self.elementClick(self._cancel_confirm_no, "xpath")
                    self.waitForElement(self._popup, "xpath", event="notdisplay")
                    return flag

                flag = flag and getValue
                self.log.debug("Pending Status Cancel popup Message get Validate : " + str(flag))

            # Validating the cancel and check
            # Validating the Message display in Popup
            self.waitForElement(self._popup, "xpath", event="display")
            message = self.getText(self._get_popup_msg, "xpath")
            getValue = self.validators.verify_text_match(message, self._cancel_reason_msg)
            flag = flag and getValue
            self.log.debug("Validating the Cancel reason Message : " + str(flag))
            getBol = self.getText("//span[text()='BOL #" + self._so_identifier_value_part_2, "xpath")

            # Action for canceling based on passed Value
            if reason.lower().replace(" ", "") == "bycustomer":
                # select the 1st option
                self.elementClick(self._cancelFlow_bycustomer_radio, "xpath")
                self.elementClick(self._cancelFlow_button_submit, "id")
            elif reason.lower().replace(" ", "") == "newso":
                # select the 2nd option
                self.elementClick(self._cancelFlow_newso_radio, "xpath")
                newBol = int(getBol) + 100
                self.sendKeys(newBol, self._cancel_bol_field, "xpath")
                self.elementClick(self._cancelFlow_button_submit, "id")
            else:
                self.log.error("Incorrect argument passed for cancel selection in clickAndValidateCancelFlow Method")

            # Waiting and Validating the Cancel Confirmation popup and close the popup at end
            self.waitForElement(self._so_page_spinner, "xpath", event="notdisplay", timeout=40)
            self.waitForElement(self._cancelFlow_button_submit, "id", event="notdisplay", timeout=30)
            self.waitForElement(self._close_popup, "xpath", event="display")
            self.waitForElement(self._so_page_spinner, "xpath", event="notdisplay", timeout=30)

            message = self.getText(self._get_popup_msg, "xpath")
            getValue = self.validators.verify_text_match(message, self._cancel_success_msg)
            self.elementClick(self._close_popup, "xpath")
            self.waitForElement(self._close_popup, "xpath", event="notdisplay", timeout=20)

            flag = flag and getValue
            self.log.debug("Validating the Cancel Confirmation Successful Message : " + str(flag))

            manualButtonValue = self.getButtonEnabled("manualDispatch")
            ediButtonValue = self.getButtonEnabled("ediDispatch")
            cancelButtonValue = self.getButtonEnabled("cancel")
            createNewOrderButtonValue = self.getButtonEnabled("createneworder")
            self.log.info("Button Status : Manual Dispatch > " + str(manualButtonValue) + ", Edi Dispatch > " +
                          str(ediButtonValue) + ", Cancel > " + str(cancelButtonValue) + ", Create New Order > " +
                          str(createNewOrderButtonValue))
            combinedValue = \
                not manualButtonValue and not ediButtonValue and not cancelButtonValue and createNewOrderButtonValue

            flag = flag and combinedValue
            self.log.debug("Validating the Button Status after performing Cancel : " + str(flag))

            # Validating the Insurance Notes
            if insValue.lower() == "yes":
                ins_prepare_notes = "Insurance against BOL " + getBol + " is successfully canceled via GTZTMS."
                # Commented Due to Bug
                # ins_prepare_notes = "Insurance against BOL " + getBol + " is successfully canceled via TMS"
                ins_notes_result = self.validateSavedNotesInSOPage("Cancel", ins_prepare_notes)
                flag = flag and ins_notes_result
                self.log.debug("Notes Result for Insurance Notes : " + str(flag))

            # Validating the Canceled Notes with reason
            self.log.info("Get User Name : " + str(userName))

            if reason.lower().replace(" ", "") == "bycustomer":
                prepare_notes = "BOL " + getBol + " is canceled by " + str(userName) + " from GTZTMS."
                # Commented due to Bug
                # prepare_notes = "BOL " + getBol + " is canceled by SmoketestDOQA from TMS Reason: Order Canceled " \
                #                                   "by the customer."
                notes_result = self.validateSavedNotesInSOPage("Cancel", prepare_notes)
                flag = flag and notes_result
                self.log.debug("Notes Result from By Customer Condn. : " + str(flag))
            elif reason.lower().replace(" ", "") == "newso":
                prepare_notes = "BOL " + getBol + " is canceled by " + str(userName) + " from GTZTMS."
                # Bug
                # prepare_notes = "BOL " + getBol + \
                #                 " is canceled by SmoketestDOQA from TMS Reason: New Sales Order (BOL) Created.."
                notes_result = self.validateSavedNotesInSOPage("Cancel", prepare_notes)
                flag = flag and notes_result
                self.log.debug("Notes Result from new SO Condn. : " + str(flag))
            else:
                self.log.error("Incorrect Notes Cancel reason Passed in clickAndValidateCancelFlow Method")
                flag = flag and False
                self.log.debug("Notes Result : " + str(flag))

            # Status Section Validation
            validateProcessStatus = self.validateStatusSectionBasedOnPassedValue("process", "Canceled")
            self.log.debug("Validating the Process status value in Status section after Cancel Order " +
                           str(validateProcessStatus))
            flag = flag and validateProcessStatus

            validateInvoiceStatus = self.validateStatusSectionBasedOnPassedValue("invoice", "Pending")
            self.log.debug("Validating the Invoice status value in Status section after Cancel Order " +
                           str(validateInvoiceStatus))
            flag = flag and validateInvoiceStatus

            validateVendorStatus = self.validateStatusSectionBasedOnPassedValue("Vendorbill", "None")
            self.log.debug("Validating the Vendor Bill status value in Status section after Cancel Order " +
                           str(validateVendorStatus))
            flag = flag and validateVendorStatus

            if getProcessStatus == "Auto Dispatch":
                validateEDIStatus = self.validateStatusSectionBasedOnPassedValue("edi", "Waiting for 997")
            else:
                validateEDIStatus = self.validateStatusSectionBasedOnPassedValue("edi", "")

            self.log.debug("Validating the EDI status value in Status section after Cancel Order " +
                           str(validateEDIStatus))
            flag = flag and validateEDIStatus

            validateTrackingStatus = self.validateStatusSectionBasedOnPassedValue("tracking", "CANCELED")
            self.log.debug("Validating the tracking status value in Status section after Cancel Order " +
                           str(validateTrackingStatus))
            flag = flag and validateTrackingStatus

            return flag

        except Exception as E:
            self.log.error("Exception occurred in clickAndValidateCancelFlow Method and exception is : " + str(E))

    def clickAndValidateEDIDispatch(self):
        """
        Clicking and Validating the EDI Dispatch Flow
        :return: True if Dispatch get Successful else False
        """
        try:
            self.log.info("Inside clickAndValidateEDIDispatch Method")
            self.waitForElement(self._edi_dispatch, "id")
            self.elementClick(self._edi_dispatch, "id")
            self.waitForElement(self._popup, "xpath")
            message = self.getText(self._get_popup_msg, "xpath")
            getValue = self.validators.verify_text_match(message, self._edi_dispatch_msg)
            self.elementClick(self._close_popup, "xpath")
            self.waitForElement(self._close_popup, "xpath", "notdisplay")
            manualButtonValue = self.getButtonEnabled("manualDispatch")
            ediButtonValue = self.getButtonEnabled("ediDispatch")
            createNewOrderButtonValue = self.getButtonEnabled("createneworder")
            self.log.info(
                "EDI Dispatch Message Status : " + str(getValue) + ", Button Status : Manual Dispatch > " + str(
                    manualButtonValue) + ", Edi Dispatch > " + str(ediButtonValue) + ", Create New Order > " + str(
                    createNewOrderButtonValue))
            combinedValue = getValue and not manualButtonValue and not ediButtonValue and createNewOrderButtonValue
            return combinedValue
        except Exception as E:
            self.log.error("Exception occurred in clickAndValidateEDIDispatch Method and Exception is : " + str(E))

    def clickAndValidateManualDispatch(self):
        """
        Clicking and Validating the Manual Dispatch Flow
        :return: True if Dispatch get Successful else False
        """
        try:
            self.log.info("Inside clickAndValidateManualDispatch Method")
            self.waitForElement(self._manual_dispatch, "id", timeout=20)
            self.elementClick(self._manual_dispatch, "id")
            self.waitForElement(self._popup, "xpath", timeout=20)
            message = self.getText(self._get_popup_msg, "xpath")
            getValue = self.validators.verify_text_match(message, self._manual_dispatch_msg)
            self.elementClick(self._close_popup, "xpath")
            self.waitForElement(self._close_popup, "xpath", "notdisplay")
            manualButtonValue = self.getButtonEnabled("manualDispatch")
            ediButtonValue = self.getButtonEnabled("ediDispatch")
            createNewOrderButtonValue = self.getButtonEnabled("createneworder")
            self.log.info(
                "Manual Dispatch Message Status : " + str(getValue) + ",Button Status : Manual Dispatch > " + str(
                    manualButtonValue) + ", Edi Dispatch > " + str(ediButtonValue) + ", Create New Order > " + str(
                    createNewOrderButtonValue))
            combinedValue = getValue and not manualButtonValue and not ediButtonValue and createNewOrderButtonValue
            return combinedValue

        except Exception as E:
            self.log.error("Exception occurred in clickAndValidateManualDispatch Method and Exception is : " + str(E))

    def clickSalesOrderPageActionButton(self, ButtonName):
        """
        Clicking actions buttons in SalesOrder Page..
        :param: Button name
        """
        try:
            self.log.info("Inside clickSalesOrderPageActionButton Method")
            if ButtonName.lower().replace(" ", "") == "createneworder":
                self.pageVerticalScroll(self._createneworder_button, "id")
                self.elementClick(self._createneworder_button, "id")
                self.waitForElement(self._createneworder_button, locatorType="id", event="notdisplay")
                self.waitForElement(self._so_page, locatorType="xpath", event="notdisplay")

        except Exception as E:
            self.log.error("Exception occurred in clickSalesOrderPageActionButton Method and Exception is : " + str(E))

    def getButtonEnabled(self, ButtonName):
        """
        Getting the Button enabled or Not
        :param ButtonName: the Button name for which check the enabled property
        :return: True If Enabled else Disabled
        """
        try:
            self.log.info("Inside getButtonEnabled Method")
            buttonValue = True
            if ButtonName.lower().replace(" ", "") == "manualdispatch":
                getAttValue = self.getAttribute(self._manual_dispatch, "id", "data-disabled")
                self.log.info("Get Value : " + str(getAttValue))
                if getAttValue == "true":
                    self.log.debug("Button disabled")
                    return buttonValue and False
                else:
                    self.log.debug("Button Enabled")
                    return buttonValue and True
            if ButtonName.lower().replace(" ", "") == "edidispatch":
                getAttValue = self.getAttribute(self._edi_dispatch, "id", "data-disabled")
                self.log.info("Get Value : " + str(getAttValue))
                if getAttValue == "true":
                    self.log.debug("Button disabled")
                    return buttonValue and False
                else:
                    self.log.debug("Button Enabled")
                    return buttonValue and True
            if ButtonName.lower().replace(" ", "") == "cancel":
                getAttValue = self.getAttribute(self._cancel_order_button, "id", "data-disabled")
                self.log.info("Get Value : " + str(getAttValue))
                if getAttValue == "true":
                    self.log.debug("Button disabled")
                    return buttonValue and False
                else:
                    self.log.debug("Button Enabled")
                    return buttonValue and True
            if ButtonName.lower().replace(" ", "") == "createneworder":
                getAttValue = self.getAttribute(self._createneworder_button, "id", "data-disabled")
                self.log.info("Get Value : " + str(getAttValue))
                if getAttValue == "true":
                    self.log.debug("Button disabled")
                    return buttonValue and False
                else:
                    self.log.debug("Button Enabled")
                    return buttonValue and True

        except Exception as E:
            self.log.error("Exception Occurred in getButtonEnabled Method and Exception is : " + str(E))

    def soOrderBoardButton(self):
        """
        Navigate to SO Board from SO Page
        :return:
        """
        self.log.info("Inside soOrderBoardButton Method")
        self.pageVerticalScroll(self._so_orderBoard_button, "id")
        self.elementClick(self._so_orderBoard_button, "id")
        self.waitForElement(self._so_orderBoard_button, "id", "notdisplay")

    def soPageLoadWait(self, timeout=10):
        """
        wait for SO Page to Load
        """
        try:
            self.log.info("Inside soPageLoadWait Method")
            self.waitForElement(self._so_page_spinner, "xpath", event="display")
            self.waitForElement(self._so_page_spinner, "xpath", event="notdisplay", timeout=20)
            self.waitForElement(self._so_page, "xpath", event="display", timeout=timeout)

        except Exception as E:
            self.log.error("Exception Occurred in soPageLoadWait Method and exception is : " + str(E))

    def validateSODetailsPage(self, bolNumber):
        """
        Validate the SO Details Page based on BOL Number
        :param bolNumber: passed bol Number
        :return: if BOLNumber matched than True else False
        """
        try:
            self.log.info("Inside validateSODetailsPage Method")
            self.waitForElement(self._so_page_spinner, "xpath", event="display", timeout=20)
            self.waitForElement(self._so_page_spinner, "xpath", event="notdisplay", timeout=20)
            self.soPageLoadWait(25)
            bolValue = self.getText(self._so_bol_label, "xpath")
            getBol = bolValue.split("#")[1].strip()
            return self.validators.verify_text_match(getBol, bolNumber)
        except Exception as E:
            self.log.error("Exception occurred in validateSODetailsPage Method and exception is : " + str(E))

    def ValidateNetworkSection(self):
        """
        Validate the Network section Container
        :return: True if Text Match else False
        """
        try:
            self.log.info("Inside networkSection Method")
            textValue = self.getText(self._so_network_container, "css")
            return self.validators.verify_text_match(textValue, "Network Info")
        except Exception as E:
            self.log.error("Exception occurred in ValidateNetworkSection Method and exception is : " + str(E))

    def validateNWSectionBasedOnPassedValue(self, fieldName, passedValue=""):
        """
        Validate the Network section data based on the passed field Name
        :return:
        """
        try:
            self.log.info("Inside validateNWSectionBasedOnPassedValue Method and passed field Name : " + str(fieldName))
            if fieldName.lower() == "customer":
                getValue = self.getText(self._so_customer_value, "id")
            elif fieldName.lower() == "servicetype":
                getValue = self.getText(self._so_serviceType_value, "id")
            elif fieldName.lower() == "createddate":
                getValue = self.getText(self._so_createdDate_value, "id")
            elif fieldName.lower() == "generatedon":
                getValue = self.getText(self._so_generatedOn_value, "id")
                passedValue = "GTZ TMS"
            elif fieldName.lower() == "customercontact":
                getValue = self.getText(self._so_contact_value, "id")
            elif fieldName.lower() == "ratingmethod":
                getValue = self.getText(self._so_ratingMethod_value, "id")
            elif fieldName.lower() == "quoterequest":
                getValue = self.getText(self._so_quoteRequest_value, "id").replace("#", "")
            elif fieldName.lower() == "salesrep":
                getValue = self.getText(self._so_salesRep_value, "id")

            else:
                self.log.error("Passed Field name incorrect.")
                return False

            self.log.debug("Get Value from application : " + str(getValue) + " , passed Value : " + str(passedValue))
            return self.validators.verify_text_match(getValue.lower(), passedValue.lower())

        except Exception as E:
            self.log.error(
                "Exception occurred in validateNWSectionBasedOnPassedValue Method and Exception is : " + str(E))

    # ---------------------------------------Status section Method------------------------------------------------------

    def validateStatusSection(self):
        """
        Validating the Status Section
        :return: True if section validation matched else False
        """
        try:
            self.log.info("Inside validateStatusSection Method")
            textValue = self.getText(self._so_status_container, "css")
            return self.validators.verify_text_match(textValue, "Statuses")
        except Exception as E:
            self.log.error("Exception occurred in validateStatusSection Method and exception is : " + str(E))

    def validateStatusSectionBasedOnPassedValue(self, fieldName, expectedColumnValueList):
        """
        Validating the Status section Based on passed field name and value
        :param fieldName: from application for which field value have to retrieve compare
        :param expectedColumnValueList: from the value compare has to perform
        :return: True if matched else False
        """
        try:
            self.log.info("Inside validateStatusSectionBasedOnPassedValue Method and passed field name is: " +
                          str(fieldName) + " ,passed Value : " + str(expectedColumnValueList))
            # getText = ""
            if fieldName.lower() == "process":
                getText = self.getText(self._so_process_status, "id")
            elif fieldName.lower() == "invoice":
                getText = self.getText(self._so_invoice_status, "id")
            elif fieldName.lower() == "vendorbill":
                getText = self.getText(self._so_vendor_bill_status, "id")
            elif fieldName.lower() == "edi":
                getText = self.getText(self._so_edi_status, "id")
            elif fieldName.lower() == "tracking":
                getText = self.getText("//div[contains(@id,'" + self._so_tracking_status + "')]", "xpath")
            else:
                self.log.error("Incorrect field name passed, Passed field name is : " + str(fieldName))
                return False

            if type(expectedColumnValueList) is str:
                expectedColumnValueList = expectedColumnValueList.split(",")
            elif type(expectedColumnValueList) is tuple:
                expectedColumnValueList = list(expectedColumnValueList)
            elif type(expectedColumnValueList) is list:
                expectedColumnValueList = expectedColumnValueList
            else:
                self.log.info("Incorrect type passed in validateColumnValue Method")

            self.log.debug("Prepared list in validateColumnValue Method : " + str(expectedColumnValueList))

            flag = False
            for i in expectedColumnValueList:
                if getText.lower() == i.lower():
                    self.log.info("Get value found in expected List")
                    return True

            if not flag:
                self.log.info("Get value not found in expected List")
                return False
        except Exception as E:
            self.log.error(
                "Exception occurred in validateStatusSectionBasedOnPassedValue Method and exception is :" + str(E))

    # Validate Statuses Section on Changing

    def validateStatusesSectionOnUpdate(self, statusValue=""):
        """Under this method, we are updating the passed status based on the current status of the BOL"""
        self.log.info("Inside validateStatusesSectionOnUpdate method")
        try:
            setStatusFlag = True
            currentStatus = self.getText("//div[contains(@id,'" + self._so_tracking_status + "')]", "xpath")
            self.log.info("Current status : " + str(currentStatus))
            if statusValue.lower() == "pending" or statusValue.lower() == "delivered" or \
                    statusValue.lower() == "pickup" or statusValue.lower() == "in transit":
                self.log.info("Inside if StatusValue : " + str(statusValue.lower()))
                if currentStatus.lower() == "pending":
                    self.log.info("Inside " + str(currentStatus.lower()) + " method")
                    self.clickAndValidateManualDispatch()
                    # To get updated process status, we are refreshing page in order to validate the same
                    self.refresh_page()
                    self.soPageLoadWait(15)
                    # self.elementClick(self._so_status_dropdown, "id")
                    # self.elementClick(self._so_status_selection + str.title(statusValue) + "')]", "xpath")

                    verifiedSuccessMsg = self.update_status_from_drop_down(statusValue)

                elif currentStatus.lower() == "dispatched" or currentStatus.lower() == "in transit" or \
                        currentStatus.lower() == "pickup":
                    self.log.info("Inside " + str(currentStatus.lower()) + " method")
                    # self.elementClick(self._so_status_dropdown, "id")
                    # self.elementClick(self._so_status_selection + str.title(statusValue) + "')]", "xpath")

                    verifiedSuccessMsg = self.update_status_from_drop_down(statusValue)

                elif statusValue.lower() == "pending" and \
                        (currentStatus.lower() == "delivered" or currentStatus.lower() == "pickup" or
                         currentStatus.lower() == "in transit"):
                    self.log.error("Passed status is invalid, hence cannot update status : Current status : " +
                                   str(currentStatus.lower()) + " Passed status : " + str(statusValue))
                    return False
                elif (statusValue.lower() == "pickup" or statusValue.lower() == "in transit") and \
                        (currentStatus.lower() == "delivered" or currentStatus.lower() == "pending"):
                    self.log.error("Passed status is invalid, hence cannot update status : Current status : " + str(
                        currentStatus.lower()) + " Passed status : " + str(statusValue))
                    return False
                else:
                    self.log.info("Inside else StatusValue : " + str(statusValue.lower()))
                    self.log.error("Passed status is invalid, hence cannot update status : Current status : " +
                                   str(currentStatus.lower()) + " Passed status : " + str(statusValue))
                    return False

                processStatus = self.getText(self._so_process_status, "id")
                self.log.info("processStatus : " + str(processStatus))
                updatedStatus = self.getText(self._so_tracking_status, "id")
                self.log.info("updatedStatus : " + str(updatedStatus))

                # processStatus is not set to pending status after making status from dispatched to pending ,
                # hence created bug here
                if statusValue.lower() == "pending":
                    if verifiedSuccessMsg and updatedStatus.lower() == "pending":
                        setStatusFlag = setStatusFlag and True
                    else:
                        setStatusFlag = setStatusFlag and False
                elif statusValue.lower() == "delivered":
                    if verifiedSuccessMsg and updatedStatus.lower() == "delivered" and (
                            processStatus.lower() == "manual dispatch" or processStatus.lower() == "manual finalized"):
                        setStatusFlag = setStatusFlag and True
                    else:
                        setStatusFlag = setStatusFlag and False
                elif statusValue.lower() == "pickup":
                    if verifiedSuccessMsg and updatedStatus.lower() == "pickup" \
                            and processStatus.lower() == "manual finalized":
                        setStatusFlag = setStatusFlag and True
                    else:
                        setStatusFlag = setStatusFlag and False
            return setStatusFlag
        except Exception as E:
            self.log.error("Exception occurred in validateStatusesSectionOnUpdate Method and Exception is : " + str(E))
            return False

    def update_status_from_drop_down(self, update_value=""):
        try:
            self.log.info("Inside update_status_from_drop_down Method")
            if update_value != "":
                self.elementClick(self._so_status_dropdown, "id")
                self.waitForElement(self._so_status_popup, "xpath", "display")
                self.elementClick(self._so_status_selection + str.title(update_value) + "')]", "xpath")

                self.waitForElement(self._popup, "xpath", timeout=25)
                successMsg = self.getText(self._get_success_popup_msg, "xpath")
                verifiedSuccessMsg = self.validators.verify_text_match(successMsg, self._save_success_msg)
                self.elementClick(self._close_popup, "xpath")
                return verifiedSuccessMsg
            else:
                self.log.error("Passed Value is Empty in update_status_from_drop_down Method.")
                return None
        except Exception as e:
            self.log.error("Exception occurred in update_status_from_drop_down Method and exception is : " + str(e))

    # ------------------------------Identifier Section----------------------------------------------------------
    def getIdentifierValue(self, identifier, expectedValue):
        """
        Retrieving the Identifier Value based on passed identifier name and returning after compare as per passed
        compared Value
        :param expectedValue: value we are expecting
        :param identifier: identifier name
        :return: True if match else False
        """
        try:
            self.log.info(
                "Inside getIdentifierValue Method, passed identifier : " + str(identifier) + " ,comparedValue : " +
                str(expectedValue))

            # identifierList = self.getElementList("//div[contains(@id,'" + self._so_identifier_row + "')]", "xpath")
            # flag = False
            # for i in range(len(identifierList)):
            #     if identifier.lower() == self.getText(self._so_identifier_type + str(i), "id").lower():
            #         flag = True
            #         return self.validators.verify_text_match(self.getText(self._so_identifier_value + str(i), "id"),
            #                                                  expectedValue)
            # if not flag:
            #     self.log.error("passed identifier not found in Identifier Section, passed identifier is : "
            #                    + str(identifier))
            #     return flag
            if self.isElementPresent("//span[text()='" + identifier + self._so_identifier_value_part_2, "xpath"):
                getIdentifierValue = self.getText("//span[text()='" + identifier + self._so_identifier_value_part_2,
                                                  "xpath")
                self.log.debug("Get Identifier Value from Application : " + str(getIdentifierValue))
                return self.validators.verify_text_match(getIdentifierValue, expectedValue)
            else:
                return False

        except Exception as E:
            self.log.error("Exception occurred in getIdentifierValue Method and Exception is : " + str(E))

    def validateIdentifierSectionEditable(self):
        """
        Method to Validate Identifier section is editable
        :return: True if identifier is editable else False
        """
        self.log.info("Inside validateIdentifierSectionEditable Method")
        try:

            self.pageVerticalScroll("//div[contains(@id,'" + self._so_tracking_status + "')]", "xpath")
            getTextShipmentStatus = self.getText("//div[contains(@id,'" + self._so_tracking_status + "')]", "xpath")
            self.log.info("Shipment Status for the BOL : " + str(getTextShipmentStatus))
            self.pageVerticalScroll(self._so_identifier_section, "id")
            if getTextShipmentStatus == "Canceled":
                self.log.info("We Cannot Edit the Identifier Section.")
                # return self.isElementPresent(self._so_identifier_editbtn, "xpath")
                return self.isElementPresent(self._so_identifier_editbtn, "id")
            else:
                self.log.info("We Can Edit the Identifier Section.")
                # return self.isElementPresent(self._so_identifier_editbtn, "xpath")
                return self.isElementPresent(self._so_identifier_editbtn, "id")

        except Exception as E:
            self.log.error(
                "Exception occurred in validateIdentifierSectionEditable Method and Exception is : " + str(E))

    def getAllIdentifierData(self, identifierSection):
        """
        Method to fetch all the identifier type & its value in view/edit identifier section based on the passed argument
        :param: identifierSection - Identifier View/Edit Section
        :return: List containing the identifier type & its value based on argument.
        """
        try:
            self.log.info("Inside getAllIdentifierData Method and the identifier section is " + str(identifierSection))
            identifierTypeList = []
            identifierTypeValueList = []
            identifierList = self.getElementList(self._so_identifier_row, "id")
            # identifierList = self.getElementList(self._so_identifier_row, "xpath")

            # if identifierSection.lower() == "view":
            #     for i in range(len(identifierList)):
            #         identifierTypeList.append(self.getText("(" + self._so_identifier_row + "[" + str(i + 1) + "]" +
            #                                                self._so_identifier_typeandvalue + ")[1]", "xpath"))
            #         identifierTypeValueList.append(self.getText("(" + self._so_identifier_row + "[" + str(i + 1) + "]"
            #                                                     + self._so_identifier_typeandvalue + ")[2]", "xpath"))
            #     self.log.info("Identifier Type List in View Section : " + str(identifierTypeList))
            #     self.log.info("Identifier Type Value List in View Section : " + str(identifierTypeValueList))
            #     identifierAndItsValue = \
            #         [[identifierType] + [identifierTypeValue]
            #          for identifierType, identifierTypeValue in zip(identifierTypeList, identifierTypeValueList)]
            #     self.log.info("Identifier & its value in " + str(identifierSection) +
            #                   " section : " + str(identifierAndItsValue))
            #     return identifierAndItsValue
            # elif identifierSection.lower() == "edit":
            #     for i in range(len(identifierList)):
            #         identifierTypeList.append(self.getSelectedIdentifierTypeInSOPage(i))
            #         identifierTypeValueList.append(self.getAttribute(self._so_identifier_row + "[" + str(i + 1) + "]"
            #                                                          + self._so_identifier_editidentifiertypevalue,
            #                                                          "xpath", "value"))
            #     self.log.info("Identifier Type List in Edit Section : " + str(identifierTypeList))
            #     self.log.info("Identifier Type Value List in Edit Section : " + str(identifierTypeValueList))
            #     identifierAndItsValue = \
            #         [[identifierType] + [identifierTypeValue]
            #          for identifierType, identifierTypeValue in zip(identifierTypeList, identifierTypeValueList)]
            #     self.log.info("Identifier & its value in " + str(identifierSection) + " section : "
            #                   + str(identifierAndItsValue))
            #     return identifierAndItsValue

            if identifierSection.lower() == "view":
                for i in range(len(identifierList)):
                    identifierTypeList.append(self.getText(self._so_identifier_type + str(i), "id"))
                    identifierTypeValueList.append(self.getText(self._so_identifier_value + str(i), "id"))
                self.log.info("Identifier Type List in View Section : " + str(identifierTypeList))
                self.log.info("Identifier Type Value List in View Section : " + str(identifierTypeValueList))
                identifierAndItsValue = [[identifierType] + [identifierTypeValue] for
                                         identifierType, identifierTypeValue in
                                         zip(identifierTypeList, identifierTypeValueList)]
                self.log.info(
                    "Identifier & its value in " + str(identifierSection) + " section : " + str(identifierAndItsValue))
                return identifierAndItsValue
            elif identifierSection.lower() == "edit":
                for i in range(len(identifierList)):
                    identifierTypeList.append(self.getSelectedIdentifierTypeInSOPage(i))
                    identifierTypeValueList.append(
                        self.getAttribute(self._so_identifier_editidentifiertypevalue + str(i), "id", "value"))
                self.log.info("Identifier Type List in Edit Section : " + str(identifierTypeList))
                self.log.info("Identifier Type Value List in Edit Section : " + str(identifierTypeValueList))
                identifierAndItsValue = [[identifierType] + [identifierTypeValue] for
                                         identifierType, identifierTypeValue in
                                         zip(identifierTypeList, identifierTypeValueList)]
                self.log.info(
                    "Identifier & its value in " + str(identifierSection) + " section : " + str(identifierAndItsValue))
                return identifierAndItsValue
            else:
                self.log.error(
                    "Passed identifier section " + str(identifierSection) + " is not matching with the conditions.")
        except Exception as E:
            self.log.error("Exception occurred in getAllIdentifierData Method and Exception is : " + str(E))

    def defaultIdentifierEditValidations(self):
        """
        Method to validate the Identifier edit sections
        :return: True if all expected else False
        """
        self.log.info("Inside defaultIdentifierEditValidations Method")
        try:
            combinedResult = True
            self.pageVerticalScroll(self._so_identifier_section, "id")

            if self.validateIdentifierSectionEditable():
                self.log.info("Identifier Section is Editable")
                # self.elementClick(self._so_identifier_editbtn, locatorType="xpath")
                # self.waitForElement(self._so_identifier_cancelbtn, locatorType="xpath", event="display")
                self.elementClick(self._so_identifier_editbtn, locatorType="id")
                self.waitForElement(self._so_identifier_cancelbtn, locatorType="id", event="display")

                # # Commented for optimization
                # identifiersInViewSectionListPair = self.getAllIdentifierData("view")
                #
                # self.elementClick(self._so_identifier_editbtn, locatorType="id")
                # self.waitForElement(self._so_identifier_cancelbtn, locatorType="id", event="display")
                #
                # # Commented for optimization.
                # # Identifier's type & its Value in View and Edit
                # identifiersInEditSectionListPair = self.getAllIdentifierData("edit")
                # self.log.info("identifiersInViewSectionListPair : " + str(identifiersInViewSectionListPair))
                # self.log.info("identifiersInEditSectionListPair : " + str(identifiersInEditSectionListPair))
                # validateIdentifierViewAnEditIsSame = \
                #     self.validadors.verify_list_contains(identifiersInViewSectionListPair,
                #                                          identifiersInEditSectionListPair)
                # self.log.info("Validate Identifier's type & its Value in View and Edit is same Result: " +
                #               str(validateIdentifierViewAnEditIsSame))
                # combinedResult = combinedResult and validateIdentifierViewAnEditIsSame
                #
                # # Commented for optimization.
                # # Cancel Button
                # # cancelbtnStatus = \
                # #     self.validators.verify_text_match(self.getAttribute(self._so_identifier_cancelbtn, "id",
                # #                                                         "data-disabled"), "false")
                # cancelbtnStatus = \
                #     self.validators.verify_text_match(self.getAttribute(self._so_identifier_cancelbtn, "xpath",
                #                                                         "data-disabled"), "false")
                # self.log.info("Cancel Button Display Status : " + str(cancelbtnStatus))
                # combinedResult = combinedResult and cancelbtnStatus
                #
                # # Save & Confirm Button
                # # saveAndConfirmBtnStatus = \
                # #     self.validators.verify_text_match(self.getAttribute(self._so_identifier_saveandconfirmbtn, "id",
                # #                                            "data-disabled"), "false")
                # saveAndConfirmBtnStatus = \
                #     self.validators.verify_text_match(self.getAttribute(self._so_identifier_saveandconfirmbtn,
                #                                                         "xpath", "data-disabled"), "false")
                # self.log.info("Save & Confirm Button Display Status : " + str(saveAndConfirmBtnStatus))
                # combinedResult = combinedResult and cancelbtnStatus
                #
                # # Add Button
                # # addBtnStatus = self.isElementPresent(self._so_identifier_addbtn, "id")
                # addBtnStatus = self.isElementPresent(self._so_identifier_addbtn, "xpath")
                # self.log.info("Add Button Display Status : " + str(addBtnStatus))
                # combinedResult = combinedResult and addBtnStatus
                #
                # # Commented for optimization.
                # # After clicking Add Button Default State of Identifier Type & Value
                # addIdentifierTypeDefaultResult = \
                #     self.validators.verify_text_match(self.getSelectedIdentifierTypeInSOPage(identifierCountInSection),
                #                          "Select Type")
                # self.log.info("addIdentifierTypeDefault Status : " + str(addIdentifierTypeDefaultResult))
                # combinedResult = combinedResult and addIdentifierTypeDefaultResult
                # #addIdentifierTypeValueDefaultResult = self.validators.verify_text_match(
                # #   self.getAttribute(self._so_identifier_editidentifiertypevalue + str(identifierCountInSection),
                # #                    "id", "value"), "")
                # addIdentifierTypeValueDefaultResult = \
                #     self.validators.verify_text_match(self.getAttribute(self._so_identifier_row + "[" +
                #                                            str(identifierCountInSection + 1) + "]" +
                #                                            self._so_identifier_editidentifiertypevalue, "xpath",
                #                                            "value"), "")
                # self.log.info("addIdentifierTypeValueDefault Status : " + str(addIdentifierTypeValueDefaultResult))
                # combinedResult = combinedResult and addIdentifierTypeValueDefaultResult
                #
                # change
                # Delete Button
                # identifierCountInSectionList = self.getElementList("//div[contains(@id,'" +
                #                                                    self._so_identifier_row + "')]", "xpath")
                # self.elementClick(self._so_identifier_addbtn, locatorType="id")
                # deleteBtnStatus = self.isElementPresent(self._so_identifier_deletebtn +
                #                                         str(identifierCountInSection), "id")
                #
                # BOL# & Quote# is not editable
                # IdValue = self.getAttribute("//div[text()='" + str(identifier) +
                #                             "']/parent::div/parent::div/parent::div", locatorType="xpath",
                #                             attributeType="id")
                # self.log.info("Attribute value, value :" + str(IdValue))
                # defaultIdentifierIndex = IdValue.replace(self._so_identifier_editidentifiertype, "")
                # identifierTypeValueDisabledStatus = \
                #     self.validators.verify_text_match(self.getAttribute(self._so_identifier_editidentifiertypevalue +
                #                                            str(defaultIdentifierIndex),
                #                                            "id", "data-disabled"), "true")
                # self.log.info("identifierType " + str(identifier) + " Value Disabled Status : " +
                #               str(identifierTypeValueDisabledStatus))
                # combinedResult = combinedResult and identifierTypeValueDisabledStatus
                #
                # Identifier Drop Down List
                # self.pageVerticalScroll(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                # self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                # self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                #                     str(identifierCountInSection) + "']" + self._so_identifier_editidentifiertypelist,
                #                     "xpath", "display")
                # elementList = self.getElementList("//div[@id='" + self._so_identifier_editidentifiertype +
                #                                   str(identifierCountInSection) + "']" +
                #                                   self._so_identifier_editidentifiertypelist, "xpath")
                # self.pageVerticalScroll(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                # self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                # self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                #                     str(identifierCountInSection) + "']" + self._so_identifier_editidentifiertypelist,
                #                     "xpath", "notdisplay")
                #
                # Identifier selection
                # if textValue == "PRO #":
                #     self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                #     self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                #                         str(identifierCountInSection) + "']" +
                #                         self._so_identifier_editidentifiertypelist, "xpath", "display")
                #     self.pageVerticalScroll("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                #                             str(identifierCountInSection) + "']" +
                #                             self._so_identifier_editidentifiertypelist + ")[" + str(j + 1) + "]",
                #                             "xpath")
                #     self.elementClick("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                #                       str(identifierCountInSection) + "']" +
                #                       self._so_identifier_editidentifiertypelist + ")[" + str(j + 1) + "]", "xpath")
                # else:
                #     self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                #     self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                #                         str(identifierCountInSection) + "']" +
                #                         self._so_identifier_editidentifiertypelist, "xpath", "display")
                #     self.pageVerticalScroll("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                #                             str(identifierCountInSection) + "']" +
                #                             self._so_identifier_editidentifiertypelist + ")[" + str(j + 2) + "]",
                #                             "xpath")
                #     self.elementClick("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                #                       str(identifierCountInSection) + "']" +
                #                       self._so_identifier_editidentifiertypelist + ")[" + str(j + 2) + "]", "xpath")
                # self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                # self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                #                     str(identifierCountInSection) + "']" + self._so_identifier_editidentifiertypelist,
                #                     "xpath", "display")
                # elementList = self.getElementList("//div[@id='" + self._so_identifier_editidentifiertype +
                #                                   str(identifierCountInSection) + "']" +
                #                                   self._so_identifier_editidentifiertypelist, "xpath")
                # self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                # self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                #                     str(identifierCountInSection) + "']" +
                #                     self._so_identifier_editidentifiertypelist, "xpath", "notdisplay")
                #
                # length validation
                # self.elementClick(self._so_identifier_cancelbtn, "id")
                # self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")

                # Validation started
                # Delete Button
                # identifierCountInSectionList = self.getElementList(self._so_identifier_row, "xpath")
                identifierCountInSectionList = self.getElementList(
                    "//div[contains(@id,'" + self._so_identifier_row + "')]", "xpath")
                identifierCountInSection = len(identifierCountInSectionList)
                self.log.info("No. of identifier in the section : " + str(identifierCountInSection))
                # self.elementClick(self._so_identifier_addbtn, locatorType="xpath")
                self.elementClick(self._so_identifier_addbtn, locatorType="id")
                # deleteBtnStatus = \
                #     self.isElementPresent(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]" +
                #                           self._so_identifier_deletebtn, "xpath")
                deleteBtnStatus = self.isElementPresent(self._so_identifier_deletebtn + str(identifierCountInSection),
                                                        "id")
                self.log.info("Delete Button Display Status : " + str(deleteBtnStatus))
                combinedResult = combinedResult and deleteBtnStatus

                # BOL & Quote is not editable (identifer type and its value section is disabled and delete button
                # not present)
                identifierList = self.csvRowDataToList("identifiertypelist", "defaultlist.csv")
                defaultIdentifiersList = identifierList[:2]
                expectedIdentifierList = identifierList[2:]
                self.log.info(
                    "defaultIdentifiersList : " + str(defaultIdentifiersList) + " , expectedIdentifierList : " + str(
                        expectedIdentifierList))
                for identifier in defaultIdentifiersList:
                    identifierTypeDisabledStatus = \
                        self.validators.verify_text_match(
                            self.getAttribute(
                                "//div[text()='" + str(identifier) + "']/following-sibling::input",
                                "xpath",
                                "disabled"),
                            "true")
                    self.log.info(
                        "identifierType " + str(identifier) + " Disabled Status: " + str(identifierTypeDisabledStatus))
                    combinedResult = combinedResult and identifierTypeDisabledStatus
                    identifierTypeValueDisabledStatus = \
                        self.validators.verify_text_match(
                            self.getAttribute("(//div[text()='" + str(identifier) +
                                              "']/ancestor::div//following-sibling::div/input)[1]",
                                              "xpath",
                                              "data-disabled"),
                            "true")
                    self.log.info("identifierType " + str(identifier) + " Value Disabled Status : " +
                                  str(identifierTypeValueDisabledStatus))
                    combinedResult = combinedResult and identifierTypeValueDisabledStatus
                self.log.info("Final Status : BOL & Quote disabled at type & value field ,combinedResult : " +
                              str(combinedResult))

                # # Identifier Drop Down List
                actualIdentifierListUI = []
                # self.pageVerticalScroll(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]"
                # + self._so_identifier_editidentifiertype, "xpath")
                # self.elementClick(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]"
                # + self._so_identifier_editidentifiertype, "xpath")
                # self.waitForElement(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]" +
                #                     self._so_identifier_editidentifiertype +
                #                     self._so_identifier_editidentifiertypelist, "xpath", "display")
                self.pageVerticalScroll(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype + str(
                    identifierCountInSection) + "']" + self._so_identifier_editidentifiertypelist, "xpath", "display")
                # elementList = self.getElementList(self._so_identifier_row + "[" +
                #                                   str(identifierCountInSection + 1) + "]" +
                #                                   self._so_identifier_editidentifiertype +
                #                                   self._so_identifier_editidentifiertypelist, "xpath")
                elementList = self.getElementList("//div[@id='" + self._so_identifier_editidentifiertype +
                                                  str(identifierCountInSection) + "']" +
                                                  self._so_identifier_editidentifiertypelist, "xpath")
                for i in elementList[1:]:
                    actualIdentifierListUI.append(i.text)
                self.log.info("Actual  Identifier Type List from UI: " + str(actualIdentifierListUI))
                self.log.info("Expected Identifier Type List : " + str(expectedIdentifierList))
                IdentifierListMatchResult = \
                    self.validators.verify_list_contains(actualIdentifierListUI, expectedIdentifierList)
                self.log.info("Identifier List Match Status: " + str(IdentifierListMatchResult))
                combinedResult = combinedResult and IdentifierListMatchResult
                self.pageVerticalScroll(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype + str(
                    identifierCountInSection) + "']" + self._so_identifier_editidentifiertypelist, "xpath", "display")
                # self.pageVerticalScroll(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]" +
                #                         self._so_identifier_editidentifiertype, "xpath")
                # self.elementClick(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]" +
                #                   self._so_identifier_editidentifiertype, "xpath")
                # self.waitForElement(self._so_identifier_row + "[" + str(identifierCountInSection + 1) + "]" +
                #                     self._so_identifier_editidentifiertype +
                #                     self._so_identifier_editidentifiertypelist, "xpath", "notdisplay")

                # Identifier Drop Down List
                # actualIdentifierListUI = []
                # self.pageVerticalScroll(self._so_editidentifier_selecttype, "xpath")
                # self.elementClick(self._so_editidentifier_selecttype, "xpath")
                # self.waitForElement(self._so_editidentifier_selecttype +
                #                     self._so_editidentifiertypelist, "xpath", "display")
                # elementList = self.getElementList(self._so_editidentifier_selecttype +
                #                                   self._so_editidentifiertypelist, "xpath")
                # for i in elementList[1:]:
                #     actualIdentifierListUI.append(i.text)
                # self.log.info("Actual  Identifier Type List from UI: " + str(actualIdentifierListUI))
                # self.log.info("Expected Identifier Type List : " + str(expectedIdentifierList))
                # IdentifierListMatchResult = self.validadors.verify_list_contains(actualIdentifierListUI,
                #                                                                  expectedIdentifierList)
                # self.log.info("Identifier List Match Status: " + str(IdentifierListMatchResult))
                # combinedResult = combinedResult and IdentifierListMatchResult
                # self.pageVerticalScroll(self._so_editidentifier_selecttype, "xpath")
                # self.elementClick(self._so_editidentifier_selecttype, "xpath")
                # self.waitForElement(self._so_editidentifier_selecttype + self._so_editidentifiertypelist,
                #                     "xpath", "display")

                # Identifier selection from drop down & selection validated,Option Validation when the identifier
                # selected
                for j in range(len(actualIdentifierListUI)):
                    identifierListUIWhenSelected = []
                    self.log.info("Identifier going to select : " + str(actualIdentifierListUI[j]))
                    textValue = self.getSelectedIdentifierTypeInSOPage(identifierCountInSection)
                    self.log.info("Identifier which is in selected state before selection : " + str(textValue))

                    if textValue == "PRO #":
                        self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                        self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                                            str(identifierCountInSection) + "']" +
                                            self._so_identifier_editidentifiertypelist, "xpath", "display")
                        self.pageVerticalScroll("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                                                str(identifierCountInSection) + "']" +
                                                self._so_identifier_editidentifiertypelist + ")[" + str(j + 1) + "]",
                                                "xpath")
                        self.elementClick("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                                          str(identifierCountInSection) + "']" +
                                          self._so_identifier_editidentifiertypelist + ")[" + str(j + 1) + "]", "xpath")
                    else:
                        self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                        self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                                            str(identifierCountInSection) + "']" +
                                            self._so_identifier_editidentifiertypelist, "xpath", "display")
                        self.pageVerticalScroll("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                                                str(identifierCountInSection) + "']" +
                                                self._so_identifier_editidentifiertypelist + ")[" + str(j + 2) + "]",
                                                "xpath")
                        self.elementClick("(" + "//div[@id='" + self._so_identifier_editidentifiertype +
                                          str(identifierCountInSection) + "']" +
                                          self._so_identifier_editidentifiertypelist + ")[" + str(j + 2) + "]", "xpath")

                    # if textValue == "PRO #":
                    #     self.elementClick("//div[text()='" + textValue + "']/parent::div", "xpath")
                    #     self.waitForElement("//div[text()='" + textValue + "']/parent::div" +
                    #                         self._so_editidentifiertypelist,"xpath", "display")
                    #     self.pageVerticalScroll("(//div[text()='" + textValue + "']/parent::div" +
                    #                             self._so_editidentifiertypelist + ")[" + str(j + 1) + "]", "xpath")
                    #     self.elementClick("(//div[text()='" + textValue + "']/parent::div" +
                    #                       self._so_editidentifiertypelist + ")[" + str(j + 1) + "]", "xpath")
                    # else:
                    #     self.elementClick("//div[text()='" + textValue + "']/parent::div", "xpath")
                    #     self.waitForElement("//div[text()='" + textValue + "']/parent::div" +
                    #                         self._so_editidentifiertypelist ,"xpath", "display")
                    #     self.pageVerticalScroll("(//div[text()='" + textValue + "']/parent::div" +
                    #                             self._so_editidentifiertypelist + ")[" + str(j + 2) + "]", "xpath")
                    #     self.elementClick("(//div[text()='" + textValue + "']/parent::div" +
                    #                       self._so_editidentifiertypelist + ")[" + str(j + 2) + "]", "xpath")

                    textValue = self.getSelectedIdentifierTypeInSOPage(identifierCountInSection)
                    self.log.info("Identifier which is in selected state after selection : : " + str(textValue))
                    combinedResult = \
                        combinedResult and self.validators.verify_text_match(textValue, actualIdentifierListUI[j])
                    self.log.info("Identifier selection from drop down & selection Status - combined Result : " +
                                  str(combinedResult))

                    self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                    self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                                        str(identifierCountInSection) + "']" +
                                        self._so_identifier_editidentifiertypelist, "xpath", "display")
                    elementList = self.getElementList("//div[@id='" + self._so_identifier_editidentifiertype + str(
                        identifierCountInSection) + "']" + self._so_identifier_editidentifiertypelist, "xpath")

                    # self.elementClick("//div[text()='" + textValue + "']/parent::div", "xpath")
                    # self.waitForElement("//div[text()='" + textValue + "']/parent::div" +
                    #                     self._so_editidentifiertypelist, "xpath", "display")
                    #
                    # # Validate identifier(except PRO#) is present as a option in the drop down of the Identifier
                    # # Type when respective
                    # identifer is selected
                    # elementList = self.getElementList("//div[text()='" + textValue + "']/parent::div" +
                    #                                   self._so_editidentifiertypelist, "xpath")

                    for i in elementList[1:]:
                        identifierListUIWhenSelected.append(i.text)
                    self.log.info("identifierListUIWhenSelected : " + str(identifierListUIWhenSelected))
                    if textValue == "PRO #":
                        if "PRO #" not in identifierListUIWhenSelected:
                            combinedResult = combinedResult and True
                        else:
                            self.log.error(
                                "PRO # is present as a option in the drop down list eventhough PRO# is selected.")
                            combinedResult = combinedResult and False
                    else:
                        if textValue in identifierListUIWhenSelected:
                            combinedResult = combinedResult and True
                        else:
                            self.log.error("The identifier type " + str(textValue) +
                                           " is not present as a option in the drop down list eventhough " +
                                           str(textValue) + " is selected.")
                            combinedResult = combinedResult and False

                    self.elementClick(self._so_identifier_editidentifiertype + str(identifierCountInSection), "id")
                    self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype +
                                        str(identifierCountInSection) + "']" +
                                        self._so_identifier_editidentifiertypelist, "xpath", "notdisplay")

                    # self.elementClick("//div[text()='" + textValue + "']/parent::div", "xpath")
                    # self.waitForElement("//div[text()='" + textValue + "']/parent::div" +
                    #                     self._so_editidentifiertypelist, "xpath", "notdisplay")
                self.log.info("Exit for loop,selection & option  validation - combined Result : " + str(combinedResult))

                # Identifier Type text field data & max length validation
                for identifier in expectedIdentifierList:
                    if identifier == "PRO #":
                        inputdata = self._so_identifier_exceed_number
                    else:
                        inputdata = self._so_identifier_exceed_string
                    validateDataEnteredResult = \
                        self.validateEnteredDataIdentifierTypeValueFieldInEditPage(identifier, identifierCountInSection,
                                                                                   inputdata)
                    self.log.info("max validateDataEnteredResult Status : " + str(validateDataEnteredResult))
                    combinedResult = combinedResult and validateDataEnteredResult
                self.log.info("Exit for loop,max data entered - combined Result : " + str(combinedResult))

                # self.elementClick(self._so_identifier_cancelbtn, "xpath")
                # self.waitForElement(self._so_identifier_editbtn, locatorType="xpath", event="display")
                self.elementClick(self._so_identifier_cancelbtn, "id")
                self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")
                self.log.info("defaultIdentifierEditValidations - combined Result : " + str(combinedResult))
                return combinedResult
            else:
                self.log.error("Identifier Section is not Editable")
                return False

        except Exception as E:
            self.log.error("Exception occurred in defaultIdentifierEditValidations Method and Exception is : " + str(E))

    def getSelectedIdentifierTypeInSOPage(self, index):
        """
        Method to get the selected identifier type.
        :return: Selected Identifier type
        """
        self.log.info("Inside getSelectedIdentifierTypeInSOPage Method")
        try:
            return self.getText(self._so_identifier_editidentifiertype + str(index), "id")
            # return self.getText(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                     self._so_identifier_editidentifiertype, "xpath")
        except Exception as E:
            self.log.error("Method getSelectedIdentifierTypeInSOPage : Passed locator is wrong/got updated and the "
                           "Exception is " + str(E))

    def selectIdentifierTypeInSOPage(self, identifierType, index):
        """
        Selecting the Identifier type as per the passed Value
        :param index: what index value
        :param identifierType: The Identifier type you want to select
        :return: if as per passed value selected then True else False
        """
        self.log.info("Inside selectIdentifierTypeInSOPage Method and Passed identifier type is " + str(
            identifierType) + " and the index is " + str(index))
        try:

            self.pageVerticalScroll(self._so_identifier_editidentifiertype + str(index), "id")
            status = self.isElementPresent(
                "//div[@id='" + self._so_identifier_editidentifiertype + str(index) + "']/div/div/input[@disabled]",
                "xpath")
            self.log.info("Identifier Type row Enabled or Disabled Status :" + str(status))
            if not status:
                if not (self.getSelectedIdentifierTypeInSOPage(index) == identifierType):
                    self.elementClick(self._so_identifier_editidentifiertype + str(index), "id")
                    self.waitForElement("//div[@id='" + self._so_identifier_editidentifiertype + str(
                        index) + "']" + self._so_identifier_editidentifiertypelist, "xpath", "display")
                    self.selectBasedOnText("//div[@id='" + self._so_identifier_editidentifiertype + str(
                        index) + "']" + self._so_identifier_editidentifiertypelist, "xpath", identifierType)
                return self.validators.verify_text_match(self.getSelectedIdentifierTypeInSOPage(index), identifierType)

            # self.pageVerticalScroll(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                         self._so_identifier_editidentifiertype, "xpath")
            # status = self.isElementPresent(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                                self._so_identifier_editidentifiertype +
            #                                "/div/div/input[@disabled]", "xpath")
            # self.log.info("Identifier Type row Enabled or Disabled Status :" + str(status))
            # if not status:
            #     if not (self.getSelectedIdentifierTypeInSOPage(index) == identifierType):
            #         self.elementClick(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                           self._so_identifier_editidentifiertype, "xpath")
            #         self.waitForElement(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                             self._so_identifier_editidentifiertype +
            #                             self._so_identifier_editidentifiertypelist, "xpath", "display")
            #         self.selectBasedOnText(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                                self._so_identifier_editidentifiertype +
            #                                self._so_identifier_editidentifiertypelist, "xpath", identifierType)
            #     return self.validators.verify_text_match(self.getSelectedIdentifierTypeInSOPage(index),
            #                                              identifierType)
            else:
                self.log.error("Identifier Type row is in disabled state and the passed Identifier Type is " +
                               str(identifierType) + " and index is " + str(index))
                return False

        except Exception as E:
            self.log.error("Method selectIdentifierTypeInSOPage : Passed identifier type didnot got selected or "
                           "passed locator is wrong/got updated and the Exception is " + str(E))

    def enterIdentifierTypeValueFieldInEditPage(self, identifierType, index, data=""):
        """
        Method to select the identifier and enter input data as per the arguments
        :return: True if data entered successfully
        """
        self.log.info("Inside enterIdentifierTypeValueFieldInEditPage Method and the passed identifier type is " +
                      str(identifierType) + " ,index is " + str(index) + " and data is " + str(data))
        try:

            status = self.isElementPresent("//div[@id='" + self._so_identifier_editidentifiertype +
                                           str(index) + "']/div/div/input[@disabled]", "xpath")
            # status = self.isElementPresent(self._so_identifier_row + "[" + str(index + 1) + "]" +
            #                                self._so_identifier_editidentifiertype + "/div/div/input[@disabled]",
            #                                "xpath")
            self.log.info("Identifier Type row Enabled or Disabled Status :" + str(status))
            if not status:
                if "Customer BOL #".lower() == identifierType.lower():
                    self.selectIdentifierTypeInSOPage(identifierType, index)
                    self.sendKeys(data, self._so_identifier_editidentifiertypevalue + str(index), "id")
                    # self.sendKeys(data, self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #               self._so_identifier_editidentifiertypevalue , "xpath")
                    return True
                elif "P/U #".lower() == identifierType.lower():
                    self.selectIdentifierTypeInSOPage(identifierType, index)
                    self.sendKeys(data, self._so_identifier_editidentifiertypevalue + str(index), "id")
                    # self.sendKeys(data, self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #               self._so_identifier_editidentifiertypevalue, "xpath")
                    return True
                elif "Reference #".lower() == identifierType.lower():
                    self.selectIdentifierTypeInSOPage(identifierType, index)
                    self.sendKeys(data, self._so_identifier_editidentifiertypevalue + str(index), "id")
                    # self.sendKeys(data, self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #               self._so_identifier_editidentifiertypevalue, "xpath")
                    return True
                elif "P/O #".lower() == identifierType.lower():
                    self.selectIdentifierTypeInSOPage(identifierType, index)
                    self.sendKeys(data, self._so_identifier_editidentifiertypevalue + str(index), "id")
                    # self.sendKeys(data, self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #               self._so_identifier_editidentifiertypevalue, "xpath")
                    return True
                elif "PRO #".lower() == identifierType.lower():
                    self.selectIdentifierTypeInSOPage(identifierType, index)
                    self.sendKeys(data, self._so_identifier_editidentifiertypevalue + str(index), "id")
                    # self.sendKeys(data, self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #               self._so_identifier_editidentifiertypevalue, "xpath")
                    return True
                else:
                    self.log.info(
                        "Identifier type " + str(identifierType) + " is not present in the identifier section")
                    return False
            else:
                self.log.error("Identifier Type row is in disabled state and the passed Identifier Type is " + str(
                    identifierType) + " and index is " + str(index))
                return False
        except Exception as E:
            self.log.error("Method enterIdentifierTypeValueFieldInEditPage : Passed locator is wrong/got updated "
                           "and the Exception is " + str(E))

    def validateEnteredDataIdentifierTypeValueFieldInEditPage(self, selectedIdentifierType, index, inputData):
        """
        Method to validate the max characters of different identifier types.
        :return: True if it matches else False
        """
        self.log.info("Inside enterIdentifierTypeValueFieldInEditPage Method")
        try:
            dataValidationResult = True
            self.log.info(
                "Length of Text Field Value of identifier " + selectedIdentifierType + " going to enter is " + str(
                    len(inputData)))
            status = self.enterIdentifierTypeValueFieldInEditPage(selectedIdentifierType, index, inputData)
            self.log.info("Data entered status : " + str(status))
            if status:
                # self.log.info("Identifier text field value after entering : " +
                #               str(len(self.getAttribute(self._so_identifier_row + "[" + str(index + 1) + "]" +
                #                                         self._so_identifier_editidentifiertypevalue, "xpath",
                #                                         "value"))))
                if "P/O #".lower() == selectedIdentifierType.lower():
                    # if len(self.getAttribute(self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #                          self._so_identifier_editidentifiertypevalue, "xpath", "value")) == 25:
                    if len(self.getAttribute(self._so_identifier_editidentifiertypevalue + str(index), "id",
                                             "value")) == 25:
                        dataValidationResult = dataValidationResult and True
                        self.log.info("Identifier text field value length as expected and the dataValidationResult "
                                      "is " + str(dataValidationResult))
                    else:
                        dataValidationResult = dataValidationResult and False
                        self.log.error("Identifier" + str(selectedIdentifierType) +
                                       " text field value length is not as expected and the dataValidationResult is " +
                                       str(dataValidationResult))

                elif "PRO #".lower() == selectedIdentifierType.lower():
                    if len(self.getAttribute(self._so_identifier_editidentifiertypevalue +
                                             str(index), "id", "value")) == 15:
                        dataValidationResult = dataValidationResult and True
                        self.log.info("Identifier text field value length as expected and the dataValidationResult"
                                      " is " + str(dataValidationResult))
                    else:
                        dataValidationResult = dataValidationResult and False
                        self.log.error(
                            "Identifier" + str(selectedIdentifierType) +
                            " text field value length is not as expected and the dataValidationResult is " +
                            str(dataValidationResult))
                # change
                # self.log.info("Identifier text field value after entering : "
                # + str(len(self.getAttribute(self._so_identifier_editidentifiertypevalue +
                #                             str(index), "id", "value"))))
                # if len(self.getAttribute(self._so_identifier_editidentifiertypevalue +
                #                          str(index), "id", "value")) == 25:
                # if len(self.getAttribute(self._so_identifier_editidentifiertypevalue +
                #                          str(index), "id", "value")) == 50:

                else:

                    # if len(self.getAttribute(self._so_identifier_row + "[" + str(index + 1) + "]" +
                    #                          self._so_identifier_editidentifiertypevalue, "xpath", "value")) == 50:
                    if len(self.getAttribute(self._so_identifier_editidentifiertypevalue + str(index), "id",
                                             "value")) == 50:
                        dataValidationResult = dataValidationResult and True
                        self.log.info(
                            "Identifier text field value length as expected and the dataValidationResult is " + str(
                                dataValidationResult))
                    else:
                        dataValidationResult = dataValidationResult and False
                        self.log.error("Identifier" + str(selectedIdentifierType) +
                                       " text field value length is not as expected and the dataValidationResult is " +
                                       str(dataValidationResult))
                self.log.info("Identifier length & data validation Status - combined Result : " +
                              str(dataValidationResult))
            else:
                dataValidationResult = dataValidationResult and False
            return dataValidationResult

        except Exception as E:
            self.log.error("Method validateIdentifierTypeValueFieldInEditPage : Passed locator is wrong/got updated "
                           "and the Exception is " + str(E))

    def identifierEditSectionButtonBehaviour(self, identifierType, buttonName, data=""):
        """
        Method to validate the cancel,saving ,deleteing & editing the identifiers based on the passed data as arguments.
        :param: identifierType - Identifier type for saving or editing or deleting.
        :param: buttonName - Operation of the identifier edit section
        :param: data - input data for the identifier type
        :return: True if all validations are as expected otherwise False.
        """
        self.log.info("Inside identifierEditSectionButtonBehaviour Method and the passed identifier type is " + str(
            identifierType) + " ,button name is " + str(buttonName) + " and data is " + str(data))
        try:
            combinedResult = True
            self.pageVerticalScroll(self._so_identifier_section, "id")

            if self.validateIdentifierSectionEditable():
                self.log.info("Identifier Section is Editable")
                # self.elementClick(self._so_identifier_editbtn, locatorType="xpath")
                # self.waitForElement(self._so_identifier_cancelbtn, locatorType="xpath", event="display")
                self.elementClick(self._so_identifier_editbtn, locatorType="id")
                self.waitForElement(self._so_identifier_cancelbtn, locatorType="id", event="display")

                # As part of the optimization
                # identifiersInViewSectionListPair = self.getAllIdentifierData("view")
                # # Identifier's type & its Value in View and Edit
                # identifiersInEditSectionListPair = self.getAllIdentifierData("edit")
                # validateIdentifierViewAnEditIsSame = \
                #     self.validadors.verify_list_contains(identifiersInViewSectionListPair,
                #                                          identifiersInEditSectionListPair)
                # self.log.info("Validate Identifier's type & its Value in View and Edit is same Result: " +
                #               str(validateIdentifierViewAnEditIsSame))
                # combinedResult = combinedResult and validateIdentifierViewAnEditIsSame
                #
                # change
                # identifierCountInSectionList = \
                #     self.getElementList("//div[contains(@id,'" + self._so_identifier_row + "')]", "xpath")
                # self.elementClick(self._so_identifier_editbtn, locatorType="id")
                # self.waitForElement(self._so_identifier_cancelbtn, locatorType="id", event="display")

                # identifierCountInSectionList = self.getElementList(self._so_identifier_row, "xpath")
                identifierCountInSectionList = self.getElementList("//div[contains(@id,'" +
                                                                   self._so_identifier_row + "')]", "xpath")
                totalIdentifierCount = len(identifierCountInSectionList)
                self.log.info("No. of identifier's in the section : " + str(totalIdentifierCount))

                if buttonName.lower() == "cancel":
                    identifiersInEditSectionListPair = self.getAllIdentifierData("edit")
                    # self.pageVerticalScroll(self._so_identifier_addbtn, "xpath")
                    self.pageVerticalScroll(self._so_identifier_addbtn, "id")
                    # self.elementClick(self._so_identifier_addbtn, "xpath")
                    self.elementClick(self._so_identifier_addbtn, "id")
                    # self.waitForElement(self._so_identifier_row + "[" + str(totalIdentifierCount + 1) + "]" +
                    #                     self._so_identifier_deletebtn, locatorType="xpath", event="display")
                    self.waitForElement(self._so_identifier_deletebtn + str(totalIdentifierCount), locatorType="id",
                                        event="display")
                    self.enterIdentifierTypeValueFieldInEditPage(identifierType, totalIdentifierCount, data)
                    # self.pageVerticalScroll(self._so_identifier_cancelbtn, "xpath")
                    self.pageVerticalScroll(self._so_identifier_cancelbtn, "id")
                    # self.elementClick(self._so_identifier_cancelbtn, "xpath")
                    self.elementClick(self._so_identifier_cancelbtn, "id")
                    # self.waitForElement(self._so_identifier_editbtn, locatorType="xpath", event="display")
                    self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")
                    identifiersInViewSectionListPair = self.getAllIdentifierData("view")
                    self.log.info("identifiersInViewSectionListPair: " + str(identifiersInViewSectionListPair))
                    self.log.info("identifiersInEditSectionListPair: " + str(identifiersInEditSectionListPair))
                    validateIdentifierViewAnEditIsSame = self.validators.verify_list_contains(
                        identifiersInEditSectionListPair,
                        identifiersInViewSectionListPair)
                    self.log.info("Cancel : Validate Identifier's type & its Value in Edit and View is same after "
                                  "saving Result: " + str(validateIdentifierViewAnEditIsSame))
                    combinedResult = combinedResult and validateIdentifierViewAnEditIsSame

                    # change
                    # self.pageVerticalScroll(self._so_identifier_addbtn, "id")
                    # self.elementClick(self._so_identifier_addbtn, "id")
                    # self.waitForElement(self._so_identifier_deletebtn + str(totalIdentifierCount),
                    #                     locatorType="id", event="display")
                    # self.pageVerticalScroll(self._so_identifier_cancelbtn, "id")
                    # self.elementClick(self._so_identifier_cancelbtn, "id")
                    # self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")
                    # identifierList = self.getElementList("//div[contains(@id,'" + self._so_identifier_row + "')]",
                    #                                      "xpath")
                    # self.pageVerticalScroll(self._so_identifier_deletebtn + str(editIndex), "id")
                    # self.elementClick(self._so_identifier_deletebtn + str(editIndex), locatorType="id")
                    # self.pageVerticalScroll(self._so_identifier_addbtn, "id")
                    # self.elementClick(self._so_identifier_addbtn, "id")
                    # self.waitForElement(self._so_identifier_deletebtn + str(totalIdentifierCount), locatorType="id",
                    #                     event="display")
                    # self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")
                    # self.pageVerticalScroll(self._so_identifier_cancelbtn, "id")
                    # self.elementClick(self._so_identifier_cancelbtn, "id")
                    # self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")

                elif buttonName.lower() == "delete" or buttonName.lower() == "saveandconfirm" \
                        or buttonName.lower() == "edit":
                    # flag = False
                    # identifierList = self.getElementList(self._so_identifier_row, "xpath")
                    identifierList = self.getElementList(self._so_identifier_row, "id")

                    if buttonName.lower() == "saveandconfirm":
                        # self.pageVerticalScroll(self._so_identifier_addbtn, "xpath")
                        # self.elementClick(self._so_identifier_addbtn, "xpath")
                        # self.waitForElement(self._so_identifier_row + "[" + str(totalIdentifierCount + 1) + "]"
                        # + self._so_identifier_deletebtn , locatorType="xpath" ,event="display")
                        self.pageVerticalScroll(self._so_identifier_addbtn, "id")
                        self.elementClick(self._so_identifier_addbtn, "id")
                        self.waitForElement(self._so_identifier_deletebtn + str(totalIdentifierCount), locatorType="id",
                                            event="display")
                        self.enterIdentifierTypeValueFieldInEditPage(identifierType, totalIdentifierCount, data)

                    elif buttonName.lower() == "delete":
                        for editIndex in range(len(identifierList)):
                            if self.getSelectedIdentifierTypeInSOPage(editIndex) == identifierType:
                                # self.pageVerticalScroll(self._so_identifier_row + "[" + str(editIndex + 1) + "]"
                                # + self._so_identifier_deletebtn , "xpath")
                                # self.elementClick(self._so_identifier_row + "[" + str(editIndex + 1) + "]"
                                # + self._so_identifier_deletebtn , locatorType="xpath")
                                self.pageVerticalScroll(self._so_identifier_deletebtn + str(editIndex), "id")
                                self.elementClick(self._so_identifier_deletebtn + str(editIndex), locatorType="id")
                                break

                    elif buttonName.lower() == "edit":
                        for editIndex in range(len(identifierList)):
                            if self.getSelectedIdentifierTypeInSOPage(editIndex) == identifierType:
                                self.enterIdentifierTypeValueFieldInEditPage(identifierType, editIndex, data)
                                break

                    identifiersInEditSectionListPair = self.getAllIdentifierData("edit")
                    # self.pageVerticalScroll(self._so_identifier_saveandconfirmbtn, "xpath")
                    # self.elementClick(self._so_identifier_saveandconfirmbtn, "xpath")
                    self.pageVerticalScroll(self._so_identifier_saveandconfirmbtn, "id")
                    self.elementClick(self._so_identifier_saveandconfirmbtn, "id")
                    self.waitForElement(self._popup, locatorType="xpath", event="display", timeout=30)
                    if self.getText(self._get_success_popup_msg, "xpath") == self._save_success_msg:
                        self.elementClick(self._close_popup, "xpath")
                        # self.waitForElement(self._so_identifier_editbtn, locatorType="xpath", event="display")
                        self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")
                        identifiersInViewSectionListPair = self.getAllIdentifierData("view")
                        self.log.info("identifiersInViewSectionListPair: " + str(identifiersInViewSectionListPair))
                        self.log.info("identifiersInEditSectionListPair: " + str(identifiersInEditSectionListPair))
                        validateIdentifierViewAnEditIsSame = self.validators.verify_list_contains(
                            identifiersInEditSectionListPair,
                            identifiersInViewSectionListPair)
                        self.log.info("Operation " + str(buttonName) +
                                      " : Validate Identifier's type & its Value in Edit and View is same after "
                                      "saving Result: " + str(validateIdentifierViewAnEditIsSame))
                        combinedResult = combinedResult and validateIdentifierViewAnEditIsSame
                        flag = True
                    else:
                        combinedResult = combinedResult and False
                        self.log.error("Save changes didnot happened.")
                        self.elementClick(self._close_popup, "xpath")
                        flag = True

                    if not flag:
                        combinedResult = combinedResult and False
                        # self.pageVerticalScroll(self._so_identifier_cancelbtn, "xpath")
                        # self.elementClick(self._so_identifier_cancelbtn, "xpath")
                        # self.waitForElement(self._so_identifier_editbtn, locatorType="xpath", event="display")
                        self.pageVerticalScroll(self._so_identifier_cancelbtn, "id")
                        self.elementClick(self._so_identifier_cancelbtn, "id")
                        self.waitForElement(self._so_identifier_editbtn, locatorType="id", event="display")

                else:
                    self.log.error("Incorrect Button Name Passed.")
                    combinedResult = combinedResult and False

            else:
                combinedResult = combinedResult and False
                self.log.error("Identifier Section is Not Editable")

            self.log.info("identifierEditSectionButtonBehaviour - combined Result : " + str(combinedResult))
            return combinedResult

        except Exception as E:
            self.log.error("Method identifierEditSectionButtonBehaviour : Passed locator is wrong/got updated and the "
                           "Exception is " + str(E))

    # ====================================================== Copy Order ===============================================
    def clickAndValidateCopyOrderButton(self):
        """
        Clicking Copy Order Button
        :return: True if all steps as per expected else False
        """
        try:
            self.log.info("Inside clickAndValidateCopyOrderButton Method")
            flag = True
            self.elementClick(self._copy_order_button, "id")
            self.waitForElement(self._confirm_copy_popup, "id", "display", timeout=20)
            getPopMsg = self.getText(self._get_copy_popup_msg, "css")
            msgCompare = self.validators.verify_text_match(getPopMsg, self._copy_confirm_msg)
            self.log.debug("Confirmation Message Text : " + str(msgCompare))
            flag = flag and msgCompare
            self.elementClick(self._copy_confirm_yes_button, "id")
            self.waitForElement(self._confirm_copy_popup, "id", "notdisplay")
            self.waitForElement(self._so_page_spinner, "xpath", "notdisplay", timeout=40)
            self.waitForElement(self._create_order_popup, "id", "display", timeout=15)
            getPopMsg = self.getText(self._get_create_order_popup_msg, "css")
            compareResult = re.search(self._order_create_msg, getPopMsg)
            if compareResult:
                self.log.debug("True Condition inside clickAndValidateCopyOrderButton Method")
                flag = flag and True
            else:
                self.log.debug("False condition in clickAndValidateCopyOrderButton Method")
                flag = flag and False

            getBOLText = self.getText(self._go_to_bol_button, "id")
            BolNumber = getBOLText.split("#")[1]
            self.log.info("BOl Number : " + str(BolNumber))
            self.elementClick(self._go_to_bol_button, "id")
            self.waitForElement(self._go_to_bol_button, "id", "notdisplay")
            result = self.validateSODetailsPage(BolNumber)
            flag = flag and result
            return flag, BolNumber

        except Exception as E:
            self.log.error("Exception occurred in clickAndValidateCopyOrderButton Method and Exception is : " + str(E))

    # ------------ Edit Sales Order Section---------

    def verifyEditButtonEnabledAndClick(self):
        """Under this method, we are clicking edit button based if the button is enabeled and returning true \
        else false"""
        self.log.info("Inside verifyStatusAndClickEditButton Method")
        try:
            buttonAttribute = self.getAttribute(self._editsalesorder_button, "id", "data-disabled")
            self.log.info("Class Value : " + str(buttonAttribute))
            if buttonAttribute == "false":
                self.log.info("Button Enabled")
                self.pageVerticalScroll(self._editsalesorder_button, "id")
                self.elementClick(self._editsalesorder_button, "id")
                return True
            else:
                self.log.debug("Button Disabled")
                return False

        except Exception as E:
            self.log.error(
                "Exception occurred in verifyStatusAndClickEditButton Method and Exception is : " + str(E))
            return False
