import re
from datetime import *
import calendar

from selenium.webdriver.common.keys import Keys

from pages.basepage import BasePage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL


class RouteDetailsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.qoprdp = CreateQuotePageLTL(self.driver, log)

    # ------------------------ Route Details Section Origin and Destination Address Locators --------------------
    # Address Book popup Common Locators
    _addresslist_spinner_div = \
        "//div[@id='addressBook_LTL-1489_popup']//div[contains(@class,'spinnerstyle__SpinnerLoader')]"
    _addressbook_presence = "//div[@id='addressBook_LTL-1489_popup']//div[contains(@class,'Title')]"

    _address_ItemHeader_list = "#addressBook_LTL-1489_sectionResults div[class*='ItemHeaderDiv']:nth-child(1)"
    _address_ItemContact_list = "#addressBook_LTL-1489_sectionResults div[class*='ItemContactInfoDiv']:nth-child(2)"
    _address_Article_list = "#addressBook_LTL-1489_sectionResults article"
    _address_popup_closed = "addressBook_LTL-1489_popup"
    _address_popup_close_icon = "//div[text()='Address Book']/following-sibling::div"

    _addressbook_search_textbox = "addressBook_LTL-1489_inputField"
    _address_result_section = "#addressBook_LTL-1489_sectionResults"

    # origin and destination common locators
    _address_icon = "div[@class=' css-1wy0on6']"
    _textfield_div = "div[@class=' css-1uccc91-singleValue']"
    _siteType_button = "button[contains(@class,'cMavos')]"
    _noncommercial_button = "button[contains(@class,'fRJOuW')]//span"
    _loading_element = "div[@class=' css-1wy0on6']//div[contains(@class, 'loadingIndicator')] "
    _dropdown_list = "div[contains(@class,'-menu')]/div/div"

    # Origin Address Book Locators
    _origin_address_icon = "routeDetails_LTL1-601_addressBookIcon1"
    _origin_textfield = "//div[@id='routeDetails_LTL-1489_inputField_1']//input"
    _origin_textfield_value = "routeDetails_LTL-1489_inputField_1"
    _origin_siteType = "//label[@for='origin-site-type']/following-sibling::div //" + _siteType_button
    _origin_siteType1 = "//label[@for='origin-site-type']/following-sibling::div //" + _noncommercial_button
    # _origin_text_placeholder = \
    #     "//div[@id='routeDetails_LTL-1489_inputField_1']//div[@class=' css-1wa3eu0-placeholder']"

    # Destination Address Book Locators
    _destination_address_icon = "routeDetails_LTL1-601_addressBookIcon2"

    _destination_textfield = "//div[@id='routeDetails_LTL-1489_inputField_3']//input"
    _destination_textfield_value = "routeDetails_LTL-1489_inputField_3"
    _destination_siteType = "//label[@for='destination-site-type']/following-sibling::div //" + _siteType_button
    _destination_siteType1 = "//label[@for='destination-site-type']/following-sibling::div //" + _noncommercial_button
    # _destination_text_placeholder = \
    #     "//div[@id='routeDetails_LTL-1489_inputField_3']//div[@class=' css-1wa3eu0-placeholder']"

    # Site Type Locators
    _origin_siteType_Business = "routeDetails_LTL-1489_originButton_1"
    _origin_siteType_Residence = "routeDetails_LTL-1489_originButton_2"
    _origin_siteType_Noncommercial = "routeDetails_LTL-1489_originButton_3"
    _origin_noncommercial_list = "//label[@for='origin-site-type']/following-sibling::div//section//ul//li"

    _destination_siteType_Business = "routeDetails_LTL-1489_destinationButton_1"
    _destination_siteType_Residence = "routeDetails_LTL-1489_destinationButton_2"
    _destination_siteType_Noncommercial = "routeDetails_LTL-1489_destinationButton_3"
    _destination_noncommercial_list = "//label[@for='destination-site-type']/following-sibling::div//section//ul//li"

    # Direct search Locators for origin and destination
    _origin_loadingelement = "//label[text()='*Origin']/following-sibling::div//div[contains(text(),'Loading')]"
    _destination_loadingelement = \
        "//label[text()='*Destination']/following-sibling::div //div[contains(text(),'Loading')]"
    _origin_dropdownlist = "//label[text()='*Origin']/following-sibling::div//" + _dropdown_list
    _destination_dropdownlist = "//label[text()='*Destination']/following-sibling::div//" + _dropdown_list

    # Frieght Date Locators

    _date_picker = "//label[@for='freight-date']/following-sibling::div//div[@class='react-datepicker__tab-loop']"
    _freightDate_label = "//label[@for='freight-date']"
    _freightDate_input = "routeDetails_LTL-1489_inputField_2"

    # Origin and Destination Address Containers

    _origin_companyname_textbox = "location_origin_LTL1-603_textField_1"
    _origin_contactname_textbox = "location_origin_LTL1-603_textField_2"
    _origin_contactphone_textbox = "location_origin_LTL1-603_textField_3"
    _origin_addressline1_textbox = "location_origin_LTL1-603_textField_4"
    _origin_addressline2_textbox = "location_origin_LTL1-603_textField_5"
    _origin_addresscityname_textbox = "location_origin_LTL1-603_textField_6"
    _origin_addresszipcode_textbox = "location_origin_LTL1-603_textField_7"
    _origin_pickupremarks_textbox = "location_origin_LTL1-603_textField_8"
    _origin_readytime_value = "//div[@id='location_origin_LTL1-603_select_1']//div[@class=' css-r447wv-singleValue']"
    #  "//input[@id='react-select-3-input']/preceding-sibling::div"
    _origin_closetime_value = "//div[@id='location_origin_LTL1-603_select_2']//div[@class=' css-r447wv-singleValue']"
    # "//input[@id='react-select-4-input']/preceding-sibling::div"
    _origin_readytime_div = "location_origin_LTL1-603_select_1"
    _origin_readytime_menu = "//div[@id='location_origin_LTL1-603_select_1']//"
    _origin_closetime_div = "location_origin_LTL1-603_select_2"
    _origin_closetime_menu = "//div[@id='location_origin_LTL1-603_select_2']//"

    _destination_companyname_textbox = "location_destination_LTL1-603_textField_1"
    _destination_contactname_textbox = "location_destination_LTL1-603_textField_2"
    _destination_contactphone_textbox = "location_destination_LTL1-603_textField_3"
    _destination_addressline1_textbox = "location_destination_LTL1-603_textField_4"
    _destination_addressline2_textbox = "location_destination_LTL1-603_textField_5"
    _destination_addresscityname_textbox = "location_destination_LTL1-603_textField_6"
    _destination_addresszipcode_textbox = "location_destination_LTL1-603_textField_7"
    _destination_deliveryremarks_textbox = "location_destination_LTL1-603_textField_8"
    _destination_readytime_value = "//div[@id='location_destination_LTL1-603_select_1']//div[@class=' " \
                                   "css-r447wv-singleValue'] "
    # "//input[@id='react-select-5-input']/preceding-sibling::div"
    _destination_closetime_value = "//div[@id='location_destination_LTL1-603_select_2']//div[@class=' " \
                                   "css-r447wv-singleValue'] "
    # "//input[@id='react-select-6-input']/preceding-sibling::div"
    _destination_readytime_div = "location_destination_LTL1-603_select_1"
    _destination_readytime_menu = "//div[@id='location_destination_LTL1-603_select_1']//"

    _destination_closetime_div = "location_destination_LTL1-603_select_2"
    _destination_closetime_menu = "//div[@id='location_destination_LTL1-603_select_2']//"

    # /---------------------Route Details Section Methods Start --------------------------------/

    def searchAddressBook(self, searchAddressValue, field=""):
        """ Under this method we are searching given address in addressbook based on the field"""
        try:
            if field.lower() == "origin":
                addressbook_opened_status = self.isElementDisplayed(self._address_popup_closed, locatorType="id")
                if not addressbook_opened_status:
                    self.pageVerticalScroll(self._origin_address_icon, locatorType="id")
                    self.elementClick(self._origin_address_icon, locatorType="id")

            elif field.lower() == "destination":
                addressbook_opened_status = self.isElementDisplayed(self._address_popup_closed, locatorType="id")
                if not addressbook_opened_status:
                    self.pageVerticalScroll(self._destination_address_icon, locatorType="id")
                    self.elementClick(self._destination_address_icon, locatorType="id")
            else:
                self.log.error(
                    "Passed Field value in verifySelectedAddressInTextField() method is incorrect " + str(
                        field) + "searched address value :" + str(searchAddressValue))

            addressbook_popup_text = self.getText(self._addressbook_presence, locatorType="xpath")

            if addressbook_popup_text == "Address Book":
                self.log.info("Address book pop up is displayed : " + str(addressbook_popup_text))
            else:
                self.log.error("Address book pop up is not displayed : " + str(addressbook_popup_text))

            # self.waitForElement(self._address_result_section, locatorType="css", event="display")
            if self.getAttribute(self._addressbook_search_textbox, "id",
                                 attributeType="placeholder") != 'Search Address Book':
                KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
                self.log.info("variable :" + str(KeyCombination))
                self.sendKeys(KeyCombination, self._addressbook_search_textbox, locatorType="xpath")
            self.waitForElement(self._addresslist_spinner_div, locatorType="xpath", event="notdisplay")
            self.sendKeys(searchAddressValue, self._addressbook_search_textbox, locatorType="id")
            self.waitForElement(self._addresslist_spinner_div, locatorType="xpath", event="display")
            self.waitForElement(self._addresslist_spinner_div, locatorType="xpath", event="notdisplay")

        except Exception as e:
            self.log.error(
                "Passed locators are incorrect or updated for searchAddressBookValidation method" + str(
                    field) + " searched address value: " + str(searchAddressValue))
            self.log.error("Exception" + str(e))

    def searchAndSelectAddressFromAddressBook(self, searchAddressValue, companyName, field=""):
        """
        Under this method, we are searching given address and selecting it from address book based on the field """
        try:
            self.searchAddressBook(searchAddressValue, field)
            # self.selectBasedOnText(self._address_ItemHeader_list, locatorType="css", selectValue=companyName)
            self.selectBasedOnValuePassed(self._address_Article_list, locatorType="css", selectValue=companyName)
            self.waitForElement(self._address_popup_closed, locatorType="id", event="notdisplay")

        except Exception as e:
            self.log.error("Passed locators are incorrect or updated for searchAddressBookValidation method" + str(
                field) + "searched address value :" + str(searchAddressValue))
            self.log.error("Exception" + str(e))

    def getSelectedAddressInTextField(self, field=""):
        """Get the Selected text value from address field based on the argument"""
        try:
            self.log.info("Inside getSelectedAddressInTextField Method and Passed Field : " + str(field))
            if field.lower() == "origin":
                OriginField_textvalue = self.getText(self._origin_textfield_value, locatorType="id")
                return OriginField_textvalue
            elif field.lower() == "destination":
                DestinationField_textvalue = self.getText(self._destination_textfield_value, locatorType="id")
                return DestinationField_textvalue
            else:
                self.log.error("Passed Field value in getSelectedAddressInTextField() method is incorrect " +
                               str(field))
        except Exception as e:
            self.log.error("Exception occurred in getSelectedAddressInTextField Method and Exception is : " + str(e))

    def verifySelectedAddressInTextField(self, cityName, field=""):
        """
            Under this method , we are validating the text selected from address book based on the field"""
        self.log.info("Inside verifySelectedAddressInTextField Method and passed city Name " + str(cityName))
        try:
            if field.lower() == "origin":
                self.waitForElement(self._origin_textfield_value, locatorType="id", event="text",
                                    textValue=cityName, timeout=20)
                OriginField_textvalue = self.getText(self._origin_textfield_value, locatorType="id")
                if cityName in OriginField_textvalue:
                    self.log.info("Address selected from Address book")
                    return True
                else:
                    self.log.error("Address did not selected from Address book : display value " + str(
                        self._origin_textfield_value) + "expected partial Value :" + str(cityName))
                    return False
            elif field.lower() == "destination":
                self.waitForElement(self._destination_textfield_value, locatorType="id", event="text",
                                    textValue=cityName, timeout=20)
                DestinationField_textvalue = self.getText(self._destination_textfield_value, locatorType="id")

                if cityName in DestinationField_textvalue:
                    self.log.info("Address selected from Address book")
                    return True
                else:
                    self.log.error("Address did not selected from Address book : display value " + str(
                        self._destination_textfield_value) + " expected partial Value :" + str(cityName))
                    return False
            else:
                self.log.error(
                    "Passed Field value in verifySelectedAddressInTextField() method is incorrect " + str(
                        field) + "searched address value :" + str(cityName))

        except Exception as e:
            self.log.error(
                "Passed locators or locator types are incorrect for verifySelectedAddressInTextField method" + str(
                    field) + "searched address value :" + str(cityName))
            self.log.error("Exception" + str(e))

    def verifySiteTypeSelected(self, SiteType, field=""):
        """
             Under this method , we are validating site type for the selected address based on the field """
        selected_SiteType_value = ""
        try:
            if field.lower() == "origin":
                if self.isElementPresent(self._origin_siteType, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._origin_siteType, locatorType="xpath")
                elif self.isElementPresent(self._origin_siteType1, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._origin_siteType1, locatorType="xpath")

            elif field.lower() == "destination":
                if self.isElementPresent(self._destination_siteType, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._destination_siteType, locatorType="xpath")
                elif self.isElementPresent(self._destination_siteType1, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._destination_siteType1, locatorType="xpath")

            else:
                self.log.error("Passed Field value is incorrect or not present : " + str(field))

            if selected_SiteType_value.lower() == SiteType.lower():
                self.log.info("Site Type selected is same as given for " + str(field))
                return True
            else:
                self.log.error(
                    "Site Type selected is not same as given - Expected Value :" +
                    str(SiteType) + "Actual Value : " + str(selected_SiteType_value))
                return False

        except Exception as e:
            self.log.error(
                "Passed locators or locator types  are incorrect for verifySiteTypeSelected method - Field "
                "passed : " + str(field) + "Locator : " + str(selected_SiteType_value))
            self.log.error("Exception" + str(e))

    def verifyAddressFromMatchingList(self, addressList, searchValue):
        """
        Under this method , we are validating required address found in all the matching address list and returning
        True if it is found
        """
        try:
            self.log.info("Inside verifyAddressFromMatchingList method, search value: " + str(searchValue))
            flag = 0
            for i in addressList:
                self.log.info(i.text)
                if searchValue in i.text:
                    flag = 1
            if flag == 1:
                return True
            else:
                return False

        except Exception as e:
            self.log.error(
                "Passed locators or locator types  are incorrect for verifyAddressFromMatchingList method" + str(
                    addressList) + "City name not found: " + str(searchValue))
            self.log.error("Exception" + str(e))

    def validateMatchingAddressFromAddressBook(self, searchAddressValue, cityName, field=""):
        """
        Under this method, we are validating the required address is found in all the matching address and returning
        True if address is found in matching address list based on the field
        """
        try:
            self.searchAddressBook(searchAddressValue, field)
            company_names_list = self.getElementList(self._address_ItemContact_list, locatorType="css")
            self.log.info("City counts in dropdown : " + str(len(company_names_list)))

            MatchingAddressFoundResult = self.verifyAddressFromMatchingList(company_names_list, cityName)
            return MatchingAddressFoundResult

        except Exception as e:
            self.log.error("Passed locators or locator types  are incorrect for "
                           "validateMatchingAddressFromAddressBook method " + str(field) + "searched value : " +
                           str(searchAddressValue))
            self.log.error("Exception" + str(e))

    def selectSiteType(self, siteType, field=""):
        """ Under this method, we are selecting given site type based on the field"""
        try:
            if field.lower() == "origin":
                if siteType.lower() == "business":
                    self.elementClick(self._origin_siteType_Business, locatorType="id")
                elif siteType.lower() == "residence":
                    self.elementClick(self._origin_siteType_Residence, locatorType="id")
                else:
                    self.elementClick(self._origin_siteType_Noncommercial, locatorType="id")
                    self.selectBasedOnText(self._origin_noncommercial_list, locatorType="xpath",
                                           selectValue=siteType.upper())

            elif field.lower() == "destination":
                if siteType.lower() == "business":
                    self.pageVerticalScroll(self._destination_siteType_Business, locatorType="id")
                    self.elementClick(self._destination_siteType_Business, locatorType="id")
                elif siteType.lower() == "residence":
                    self.pageVerticalScroll(self._destination_siteType_Residence, locatorType="id")
                    self.elementClick(self._destination_siteType_Residence, locatorType="id")
                else:
                    self.pageVerticalScroll(self._destination_siteType_Noncommercial, locatorType="id")
                    self.elementClick(self._destination_siteType_Noncommercial, locatorType="id")
                    self.selectBasedOnText(self._destination_noncommercial_list, locatorType="xpath",
                                           selectValue=siteType.upper())

            else:
                self.log.error(
                    "Passed Site Type is invalid or not present for :" + str(field) + "Value : " + str(siteType))

        except Exception as e:
            self.log.error("Passed locators or locator types  are incorrect for "
                           "selectSiteType method " + str(field) + "Value : " + str(siteType))
            self.log.error("Exception" + str(e))

    # --------------Direct address search and select in origin and destination Field-------------------------------

    def directSearch(self, searchValue, field=""):
        self.log.info("Inside directSearch method")
        try:
            dropdownlist = ""
            if field.lower() == "origin":
                if self.getText(self._origin_textfield_value, "id") != 'Enter Origin':
                    self.log.info("Origin Address Display as selected")
                    KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
                    self.log.info("variable :" + str(KeyCombination))
                    self.sendKeys(KeyCombination, self._origin_textfield, locatorType="xpath")
                self.sendKeys(searchValue, self._origin_textfield, locatorType="xpath")
                self.waitForElement(self._origin_loadingelement, locatorType="xpath", event="display", timeout=25)
                self.waitForElement(self._origin_loadingelement, locatorType="xpath", event="notdisplay", timeout=25)
                dropdownlist = self.getElementList(self._origin_dropdownlist, locatorType="xpath")
            elif field.lower() == "destination":
                if self.getText(self._destination_textfield_value, "id") != 'Enter Destination':
                    self.log.info("Destination Address Display as selected")
                    KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
                    self.log.info("variable :" + str(KeyCombination))
                    self.sendKeys(KeyCombination, self._destination_textfield, locatorType="xpath")
                self.sendKeys(searchValue, self._destination_textfield, locatorType="xpath")
                self.waitForElement(self._destination_loadingelement, locatorType="xpath", event="display", timeout=25)
                self.waitForElement(self._destination_loadingelement, locatorType="xpath", event="notdisplay",
                                    timeout=25)
                dropdownlist = self.getElementList(self._destination_dropdownlist, locatorType="xpath")
            else:
                self.log.error("Passed field value is invalid or not present :" + str(field))

            return dropdownlist

        except Exception as e:
            self.log.error("Passed locators or locator types  are incorrect for "
                           "directSearchAndSelectAddress method : " + str(field) + " value : " + str(searchValue))
            self.log.error("Exception" + str(e))

    def validateMatchingAddressForDirectSearch(self, searchValue, field=""):
        """Under this method, we are validating searched address is present in all the matching address list found
        and returning True if it matches """
        self.log.info("Inside validateMatchingAddressForDirectSearch method")
        dropdownlist = self.directSearch(searchValue, field)
        result = self.verifyAddressFromMatchingList(dropdownlist, searchValue)
        return result

    def directSearchAndSelectAddress(self, searchValue, field=""):
        """Under this method, we are searching for given address and selecting it based on the field and validating
        the selected value with textfield value """
        self.log.info("Inside directSearchAndSelectAddress method")
        try:
            # selected_value = ""
            selected_textValue = ""
            if field == "origin":
                if self.getText(self._origin_textfield_value, "id") != 'Enter Origin':
                    self.log.info("Origin Address Display as selected")
                    KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
                    self.log.info("variable :" + str(KeyCombination))
                    self.sendKeys(KeyCombination, self._origin_textfield, locatorType="xpath")
                self.sendKeys(searchValue, self._origin_textfield, locatorType="xpath")
                self.waitForElement(self._origin_loadingelement, locatorType="xpath", event="display", timeout=25)
                self.waitForElement(self._origin_loadingelement, locatorType="xpath", event="notdisplay", timeout=25)
                selected_value = self.selectBasedOnValuePassed(self._origin_dropdownlist, locatorType="xpath",
                                                               selectValue=searchValue)
                self.waitForElement(self._origin_textfield_value, locatorType="id", event="text",
                                    textValue=searchValue, timeout=20)
                selected_textValue = self.getText(self._origin_textfield_value, locatorType="id")
            elif field == "destination":
                if self.getText(self._destination_textfield_value, "id") != 'Enter Destination':
                    self.log.info("Destination Address Display as selected")
                    KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
                    self.log.info("variable :" + str(KeyCombination))
                    self.sendKeys(KeyCombination, self._destination_textfield, locatorType="xpath")
                self.sendKeys(searchValue, self._destination_textfield, locatorType="xpath")
                self.waitForElement(self._destination_loadingelement, locatorType="xpath", event="display", timeout=25)
                self.waitForElement(self._destination_loadingelement, locatorType="xpath", event="notdisplay",
                                    timeout=25)
                selected_value = self.selectBasedOnValuePassed(self._destination_dropdownlist, locatorType="xpath",
                                                               selectValue=searchValue)
                self.waitForElement(self._destination_textfield_value, locatorType="id", event="text",
                                    textValue=searchValue, timeout=20)
                selected_textValue = self.getText(self._destination_textfield_value, locatorType="id")
            else:
                self.log.error("Passed field value is invalid or not present :" + str(field))

            if searchValue.lower() in selected_textValue.lower():
                return True
            else:
                return False

        except Exception as e:
            self.log.error("Passed locators or locator types  are incorrect for "
                           "directSearchAndSelectAddress method : " + str(field) + " value : " + str(searchValue))
            self.log.error("Exception" + str(e))

    def validateAccessorials(self, origin_dest_accessorials, finalSelectedAccesorials):
        """Under this method, we are validating the selected accessorials with accessorials passed and returning True
        if both are equal """
        self.log.info("Inside validateAccessorials method")
        if set(origin_dest_accessorials) == set(finalSelectedAccesorials):
            return True
        else:
            self.log.error("Selected accessorials is not matched with passed accessorials for origin and destination "
                           "Passed Origin-dest accessorials list : " + str(origin_dest_accessorials) +
                           " Final Selected accessorials list: " + str(finalSelectedAccesorials))
            return False

    def validateAccessorialsForDirectSearch(self, accessorialsList):
        """Under this method, we are validating the selected accessorials list count and returning true if it is 0"""

        self.log.info("Inside validateAccessorialsForDirectSearch method")
        if len(accessorialsList) == 0:
            return True
        else:
            return False

    def getSystemDate(self):
        self.log.info("Inside getSystemDate method")
        currentDate = self.getCurrentDate()
        return currentDate

    def validateSelectedFreightDate(self, dateValue="", passedFormat="%m/%d/%Y"):
        """Under this method, we are validating the Frieght date value with the value passed and returning True if
        both are equal """
        self.log.info("Inside validateSelectedFreightDate method")
        try:
            if dateValue == "":
                dateValue = self.util.getCurrentDate()

            FreightDateSelected = self.getAttribute(self._freightDate_input, locatorType="id", attributeType="value")
            dateSelected = self.util.getFormattedDate(FreightDateSelected, "%m/%d/%Y", passedFormat)
            if dateValue == dateSelected:
                return True
            else:
                self.log.error("Selected date is not displayed in the Freight Date field, Selected Date : " + str(
                    dateValue) + "Date displayed in the Freight Date Field : " + str(dateSelected))
                return False

        except Exception as e:
            self.log.error("Passed dateValue is invalid or cannot find the locator : " + str(
                self._freightDate_input) + "Date Value Passed : " + str(dateValue))
            self.log.error("Exception" + str(e))

    def getSelectedFreightDate(self, passedFormat="%m/%d/%Y"):
        """
        Method to fetch the selected freight date
        :param: passedFormat Format of the date
        :return: freight date selected
        """
        self.log.info("Inside getSelectedFreightDate method")
        try:
            FreightDateSelected = self.getAttribute(self._freightDate_input, locatorType="id", attributeType="value")
            dateSelected = self.util.getFormattedDate(FreightDateSelected, "%m/%d/%Y", passedFormat)
            return dateSelected
        except Exception as e:
            self.log.error("cannot find the locator  and the exception" + str(e))

    def checkDateIsDisabled(self, no_of_days):
        """Under this method, we are validating previous date is disabled or not and returning True if it is disabled"""
        self.log.info("Inside choosePreviousDate method")
        try:
            date_to_be_selected = self.util.get_date_value(no_of_days)
            DateValue = date_to_be_selected.split('/')
            month_value = DateValue[0]
            day_value = DateValue[1]
            year_value = DateValue[2]
            self.log.info("previous date" + str(date_to_be_selected))

            self.elementClick(self._freightDate_input, locatorType="id")

            selected_month = calendar.month_name[int(month_value)] + " " + year_value

            while selected_month.lower() != self.getText(self._datepicker_monthValue, "xpath").lower():
                if self.isElementDisplayed(self._previousmonth_button, "xpath"):
                    self.elementClick(self._previousmonth_button, "xpath")
                else:
                    break

            chooseday = "0" + str(day_value)

            if selected_month.lower() != self.getText(self._datepicker_monthValue, "xpath").lower():
                if not self.isElementDisplayed("//div[@class='react-datepicker']//div[contains(@class," + chooseday +
                                               ") and contains(@class, 'outside-month')]"):
                    return True
                elif self.isElementDisplayed("//div[@class='react-datepicker']//div[contains(@class," + chooseday +
                                             ") and contains(@class, 'outside-month')]"):
                    disabled_value = self.getAttribute(self._dateTobeSelected_part1 + chooseday + "')]",
                                                       locatorType="xpath", attributeType="aria-disabled")

                    if disabled_value == "true":
                        self.log.info("disabled_value : " + str(disabled_value))
                        return True
                    else:
                        self.log.info("disabled_value : " + str(disabled_value))
                        self.log.error("selected past date is enabled past date selected: " + str(chooseday))
                        return False
                else:
                    self.log.error("Element not displayed")
                    return False

            else:
                chooseday = "0" + str(day_value)
                disabled_value = self.getAttribute(self._dateTobeSelected_part1 + chooseday + "')]",
                                                   locatorType="xpath", attributeType="aria-disabled")

                if disabled_value == "true":
                    self.log.info("disabled_value : " + str(disabled_value))
                    return True
                else:
                    self.log.info("disabled_value : " + str(disabled_value))
                    self.log.error("selected past date is enabled past date selected: " + str(chooseday))
                    return False

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Passed locators or locator types  are incorrect for "
                           "checkDateIsDisabled method " + "Locator : " + str(self._freightDate_input))

    def dateSelection(self, no_of_days=0):
        """Under this method, we are selecting the date based on the value passed and validating Freight date value
        with the selected date value and returning True if both are equal """
        self.log.info("Inside dateSelection Method")
        try:
            self.log.info("Inside dateSelection Method and passed Value : " + str(no_of_days))
            self.waitForElement(self._freightDate_input, locatorType="id", timeout=10)
            self.elementClick(self._freightDate_input, locatorType="id")
            if no_of_days >= 0 or no_of_days < 0:
                date_to_be_selected = self.selectDateBasedOnPassedValue(no_of_days=no_of_days)

                validateselectedTextResult = self.validateSelectedFreightDate(date_to_be_selected)
                self.log.info("validateselectedTextResult : " + str(validateselectedTextResult))
                return validateselectedTextResult
            else:
                self.log.error("Passed no of days is either incorrect or invalid")
                return False

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Passed locators or locator types  are incorrect for clickPassedDate method ")

    def checkDatepickerClosed(self):
        self.log.info("Inside checkDatepickerClosed method")
        if self.isElementDisplayed(self._date_picker, "xpath"):
            self.elementClick(self._freightDate_label, "xpath")

    def validateAddressContainerFields(self, field, cityName, zipCode, readytime, closetime, companyname="",
                                       contactname="",
                                       contactphone="", addressline1="", addressline2="", remarks=""):
        try:
            self.log.info("Inside validateAddressContainerFields method")
            # flag = False
            contactphone = re.sub("[() ]", "", contactphone).replace("-", "")
            if field.lower() == "origin":
                origincompanyName = self.getAttribute(self._origin_companyname_textbox, "id", attributeType="value")
                origincontactName = self.getAttribute(self._origin_contactname_textbox, "id", attributeType="value")
                origincontactphone = re.sub("[() ]", "", self.getAttribute(self._origin_contactphone_textbox, "id",
                                                                           attributeType="value")).replace("-", "")
                originaddressline1 = self.getAttribute(self._origin_addressline1_textbox, "id", attributeType="value")
                originaddressline2 = self.getAttribute(self._origin_addressline2_textbox, "id", attributeType="value")
                origincityname = self.getAttribute(self._origin_addresscityname_textbox, "id", attributeType="value")
                originzipcode = self.getAttribute(self._origin_addresszipcode_textbox, "id", attributeType="value")
                originpickupremarks = self.getAttribute(self._origin_pickupremarks_textbox, "id", attributeType="value")
                originreadytime = self.getText(self._origin_readytime_value, "xpath")
                originclosetime = self.getText(self._origin_closetime_value, "xpath")

                if readytime != "" and closetime != "":
                    readytime1 = datetime.strptime(readytime, "%I:%M %p")
                    finalreadytime = datetime.strftime(readytime1, "%H:%M")

                    closetime1 = datetime.strptime(closetime, "%I:%M %p")
                    finalclosetime = datetime.strftime(closetime1, "%H:%M")

                else:
                    finalreadytime = originreadytime
                    finalclosetime = originclosetime

                origin_companyname_comparevalue = origincompanyName == companyname
                origin_contactname_comparevalue = origincontactName == contactname
                origin_contactphone_comparevalue = origincontactphone == contactphone
                origin_addressline1_comparevalue = originaddressline1 == addressline1
                origin_addressline2_comparevalue = originaddressline2 == addressline2
                origin_cityname_comparevalue = cityName in origincityname
                origin_zipcode_comparevalue = originzipcode == zipCode
                origin_pickupmarks_comparevalue = originpickupremarks == remarks
                origin_readytime_comparevalue = originreadytime == finalreadytime
                origin_closetime_comparevalue = originclosetime == finalclosetime

                self.log.info("origin_companyname_comparevalue : " + str(origin_companyname_comparevalue) +
                              " Expected value :" + str(companyname) + " Actual value : " + str(origincompanyName))
                self.log.info("origin_contactname_comparevalue : " + str(origin_contactname_comparevalue) +
                              " Expected value :" + str(contactname) + " Actual value : " + str(origincontactName))
                self.log.info("origin_contactphone_comparevalue : " + str(origin_contactphone_comparevalue) +
                              " Expected value :" + str(contactphone) + " Actual value : " + str(origincontactphone))
                self.log.info("origin_addressline1_comparevalue : " + str(origin_addressline1_comparevalue) +
                              " Expected value :" + str(addressline1) + " Actual value : " + str(originaddressline1))
                self.log.info("origin_addressline2_comparevalue : " + str(origin_addressline2_comparevalue) +
                              " Expected value :" + str(addressline2) + " Actual value : " + str(originaddressline2))
                self.log.info("origin_cityname_comparevalue : " + str(origin_cityname_comparevalue) +
                              " Expected value :" + str(cityName) + " Actual value : " + str(origincityname))
                self.log.info("origin_zipcode_comparevalue : " + str(origin_zipcode_comparevalue) +
                              " Expected value :" + str(zipCode) + " Actual value : " + str(originzipcode))
                self.log.info("origin_pickupmarks_comparevalue : " + str(origin_pickupmarks_comparevalue) +
                              " Expected value :" + str(remarks) + " Actual value : " + str(originpickupremarks))
                self.log.info("origin_readytime_comparevalue : " + str(origin_readytime_comparevalue) +
                              " Expected value :" + str(finalreadytime) + " Actual value : " + str(originreadytime))
                self.log.info("origin_closetime_comparevalue : " + str(origin_closetime_comparevalue) +
                              " Expected value :" + str(finalclosetime) + " Actual value : " + str(originclosetime))

                if origin_companyname_comparevalue \
                        and origin_contactname_comparevalue \
                        and origin_contactphone_comparevalue \
                        and origin_addressline1_comparevalue \
                        and origin_addressline2_comparevalue \
                        and origin_cityname_comparevalue \
                        and origin_zipcode_comparevalue \
                        and origin_pickupmarks_comparevalue \
                        and origin_readytime_comparevalue \
                        and origin_closetime_comparevalue:
                    self.log.info("Inside If in validateAddressContainerFields - origin condition")
                    flag = True
                else:
                    self.log.error("Origin container field values are not matching for the selected origin address")
                    flag = False

            elif field.lower() == "destination":
                destcompanyName = self.getAttribute(self._destination_companyname_textbox, "id", attributeType="value")
                destcontactName = self.getAttribute(self._destination_contactname_textbox, "id", attributeType="value")
                destcontactphone = re.sub("[() ]", "", self.getAttribute(self._destination_contactphone_textbox, "id",
                                                                         attributeType="value")).replace("-", "")
                destaddressline1 = self.getAttribute(self._destination_addressline1_textbox, "id",
                                                     attributeType="value")
                destaddressline2 = self.getAttribute(self._destination_addressline2_textbox, "id",
                                                     attributeType="value")
                destcityname = self.getAttribute(self._destination_addresscityname_textbox, "id", attributeType="value")
                destzipcode = self.getAttribute(self._destination_addresszipcode_textbox, "id", attributeType="value")
                destdeliveryremarks = self.getAttribute(self._destination_deliveryremarks_textbox, "id",
                                                        attributeType="value")

                destreadytime = self.getText(self._destination_readytime_value, "xpath")
                destclosetime = self.getText(self._destination_closetime_value, "xpath")

                if readytime != "" and closetime != "":
                    readytime1 = datetime.strptime(readytime, "%I:%M %p")
                    finalreadytime = datetime.strftime(readytime1, "%H:%M")

                    closetime1 = datetime.strptime(closetime, "%I:%M %p")
                    finalclosetime = datetime.strftime(closetime1, "%H:%M")

                else:
                    finalreadytime = destreadytime
                    finalclosetime = destclosetime

                dest_companyname_comparevalue = destcompanyName == companyname
                dest_contactname_comparevalue = destcontactName == contactname
                dest_contactphone_comparevalue = destcontactphone == contactphone
                dest_addressline1_comparevalue = destaddressline1 == addressline1
                dest_addressline2_comparevalue = destaddressline2 == addressline2
                dest_cityname_comparevalue = cityName in destcityname
                dest_zipcode_comparevalue = destzipcode == zipCode
                dest_deliverymarks_comparevalue = destdeliveryremarks == remarks
                dest_readytime_comparevalue = destreadytime == finalreadytime
                dest_closetime_comparevalue = destclosetime == finalclosetime

                self.log.info("dest_companyname_comparevalue : " + str(dest_companyname_comparevalue) +
                              " Expected value :" + str(companyname) + " Actual value : " + str(destcompanyName))
                self.log.info("dest_contactname_comparevalue : " + str(dest_contactname_comparevalue) +
                              " Expected value :" + str(contactname) + " Actual value : " + str(destcontactName))
                self.log.info("dest_contactphone_comparevalue : " + str(dest_contactphone_comparevalue) +
                              " Expected value :" + str(contactphone) + " Actual value : " + str(destcontactphone))
                self.log.info("dest_addressline1_comparevalue : " + str(dest_addressline1_comparevalue) +
                              " Expected value :" + str(addressline1) + " Actual value : " + str(destaddressline1))
                self.log.info("dest_addressline2_comparevalue : " + str(dest_addressline2_comparevalue) +
                              " Expected value :" + str(addressline2) + " Actual value : " + str(destaddressline2))
                self.log.info("dest_cityname_comparevalue : " + str(dest_cityname_comparevalue) +
                              " Expected value :" + str(cityName) + " Actual value : " + str(destcityname))
                self.log.info("dest_zipcode_comparevalue : " + str(dest_zipcode_comparevalue) +
                              " Expected value :" + str(zipCode) + " Actual value : " + str(destzipcode))
                self.log.info("dest_deliverymarks_comparevalue : " + str(dest_deliverymarks_comparevalue) +
                              " Expected value :" + str(remarks) + " Actual value : " + str(destdeliveryremarks))
                self.log.info("dest_readytime_comparevalue : " + str(dest_readytime_comparevalue) +
                              " Expected value :" + str(finalreadytime) + " Actual value : " + str(destreadytime))
                self.log.info("dest_closetime_comparevalue : " + str(dest_closetime_comparevalue) +
                              " Expected value :" + str(finalclosetime) + " Actual value : " + str(destclosetime))

                if dest_companyname_comparevalue \
                        and dest_contactname_comparevalue \
                        and dest_contactphone_comparevalue \
                        and dest_addressline1_comparevalue \
                        and dest_addressline2_comparevalue \
                        and dest_cityname_comparevalue \
                        and dest_zipcode_comparevalue \
                        and dest_deliverymarks_comparevalue \
                        and dest_readytime_comparevalue \
                        and dest_closetime_comparevalue:
                    self.log.info("Inside If in validateAddressContainerFields - destination condition")
                    flag = True
                else:
                    self.log.error("Destination container field values are not matching for the selected destination "
                                   "address")
                    flag = False

            else:
                self.log.error("Passed field value is invalid or not present :" + str(field))
                return False

            return flag

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Passed locators or locator types  are incorrect for validateAddressContainerFields method ")

    def validateDefaultTime(self):
        self.log.info("Inside validateDefaultTime method")

        # Default Ready and Close Time for Origin address
        originreadytime = self.getText(self._origin_readytime_value, "xpath")

        origintimearray = originreadytime.split(":")
        originhour = origintimearray[0]
        originminutes = origintimearray[1]

        t1 = timedelta(hours=int(originhour), minutes=int(originminutes))
        t2 = timedelta(hours=8, minutes=0)
        origin_closetime = t1 + t2
        expectedoriginclosetime = str(origin_closetime).rsplit(':', 1)[0]
        if "1 day" in expectedoriginclosetime:
            closetime_array = expectedoriginclosetime.split(",")
            expectedoriginclosetime = closetime_array[1]

        actualoriginclosetime = self.getText(self._origin_closetime_value, "xpath")
        actualoriginclosetimearray = actualoriginclosetime.split(":")
        actualoriginhour = actualoriginclosetimearray[0]
        actualoriginminutes = actualoriginclosetimearray[1]
        time1 = timedelta(hours=int(originhour), minutes=int(originminutes))
        time2 = timedelta(hours=int(actualoriginhour), minutes=int(actualoriginminutes))

        origindifferenceminutes = (time2 - time1).seconds / 60
        self.log.info("origindifferenceminutes : " + str(origindifferenceminutes))

        self.log.info("Origin ready time : " + str(originreadytime))
        self.log.info("Expected origin close time : " + str(expectedoriginclosetime))
        self.log.info("Actual origin close time : " + str(actualoriginclosetime))

        # Default Ready and Close Time for Destination address

        destreadytime = self.getText(self._destination_readytime_value, "xpath")

        desttimearray = destreadytime.split(":")
        desthour = desttimearray[0]
        destminutes = desttimearray[1]

        t3 = timedelta(hours=int(desthour), minutes=int(destminutes))
        t4 = timedelta(hours=8, minutes=00)
        dest_closetime = t3 + t4
        expecteddestclosetime = str(dest_closetime).rsplit(':', 1)[0]

        if "1 day" in expecteddestclosetime:
            closetime_array = expecteddestclosetime.split(",")
            expecteddestclosetime = closetime_array[1]

        actualdestclosetime = self.getText(self._destination_closetime_value, "xpath")
        actualdestclosetimearray = actualdestclosetime.split(":")
        actualdesthour = actualdestclosetimearray[0]
        actualdestminutes = actualdestclosetimearray[1]
        time3 = timedelta(hours=int(desthour), minutes=int(destminutes))
        time4 = timedelta(hours=int(actualdesthour), minutes=int(actualdestminutes))

        destdifferenceminutes = (time4 - time3).seconds / 60
        self.log.info("destdifferenceminutes : " + str(destdifferenceminutes))

        self.log.info("Destination ready time : " + str(destreadytime))
        self.log.info("Expected destination close time : " + str(expecteddestclosetime))
        self.log.info("Actual destination close time : " + str(actualdestclosetime))

        if expectedoriginclosetime in actualoriginclosetime and expecteddestclosetime in actualdestclosetime:
            return True
        elif origindifferenceminutes >= 120 and destdifferenceminutes >= 30:
            return True
        else:
            self.log.error("Expected Close timings is not set in the close time either for origin or destination")
            return False

    def selectTimeAndValidate(self, readyTime):
        self.log.info("Inside selectTimeAndValidate method")
        try:
            readytime1 = datetime.strptime(readyTime, "%I:%M %p")
            finalreadytime = datetime.strftime(readytime1, "%H:%M")

            self.log.info("selectTime And Validate for Origin container")
            self.pageVerticalScroll(self._origin_readytime_div, "id")
            self.elementClick(self._origin_readytime_div, "id")

            self.waitForElement(self._origin_readytime_menu + self._dropdown_list, "xpath",
                                event="display")

            self.selectBasedOnText(self._origin_readytime_menu + self._dropdown_list, "xpath",
                                   selectValue=finalreadytime)

            self.log.info("selectTime And Validate for Destination container")
            self.pageVerticalScroll(self._destination_readytime_div, "id")
            self.elementClick(self._destination_readytime_div, "id")

            self.waitForElement(self._destination_readytime_menu + self._dropdown_list, "xpath",
                                event="display")

            self.selectBasedOnText(self._destination_readytime_menu + self._dropdown_list, "xpath",
                                   selectValue=finalreadytime)

            validatedResult = self.validateDefaultTime()
            self.log.info(
                "Validated close time result after selecting ready time in origin and destination containers : " + str(
                    validatedResult))

            if validatedResult:
                return True
            else:
                self.log.error("Validated close time result after selecting ready time in origin and destination "
                               "containers : " + str(validatedResult))
                return False

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Either Passed locators/locator types  are incorrect or passed time is invalid for "
                           "selectTimeAndValidate method, ready time: " + str(readyTime))

    def validateMandatoryFields(self):
        self.log.info("Inside validateMandatoryFields method")
        try:
            self.qoprdp.createOrder()
            # Origin Address Container Mandatory Fields
            self.log.info("Origin Address Container Mandatory Fields")
            origincompanyName = self.getAttribute(self._origin_companyname_textbox, "id", attributeType="placeholder")
            origincontactName = self.getAttribute(self._origin_contactname_textbox, "id", attributeType="placeholder")
            origincontactphone = self.getAttribute(self._origin_contactphone_textbox, "id", attributeType="placeholder")
            originaddressline1 = self.getAttribute(self._origin_addressline1_textbox, "id", attributeType="placeholder")
            originaddressline2 = self.getAttribute(self._origin_addressline2_textbox, "id", attributeType="placeholder")

            # Destination Address Container Mandatory Fields
            self.log.info("Destination Address Container Mandatory Fields")
            destcompanyName = self.getAttribute(self._destination_companyname_textbox, "id",
                                                attributeType="placeholder")
            destcontactName = self.getAttribute(self._destination_contactname_textbox, "id",
                                                attributeType="placeholder")
            destcontactphone = self.getAttribute(self._destination_contactphone_textbox, "id",
                                                 attributeType="placeholder")
            destaddressline1 = self.getAttribute(self._destination_addressline1_textbox, "id",
                                                 attributeType="placeholder")
            destaddressline2 = self.getAttribute(self._destination_addressline2_textbox, "id",
                                                 attributeType="placeholder")

            if origincompanyName \
                    == origincontactName \
                    == origincontactphone \
                    == originaddressline1 \
                    == destcompanyName \
                    == destcontactName \
                    == destcontactphone \
                    == destaddressline1 \
                    == "Required" \
                    and originaddressline2 != "Required" \
                    and destaddressline2 != "Required":
                self.log.info("Inside if in mandatory verification")
                return True
            else:
                self.log.info("Inside else in mandatory verification")
                self.log.error("Mandatory fields are required for origin and destination address before placing an "
                               "order")
                return False

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Either Passed locators/locator types  are incorrect for "
                           "validateMandatoryFields method")

    def enterRequiredAddressContainerDetails(self, companyname="", contactname="", contactphone="",
                                             addressline1="", addressline2="", readytime="", closetime="", remarks="",
                                             field=""):
        self.log.info("Inside enterAddressContainerDetails method")
        try:

            if field.lower() == "origin":

                self.sendKeys(companyname, self._origin_companyname_textbox)

                self.sendKeys(contactname, self._origin_contactname_textbox)

                self.sendKeys(contactphone, self._origin_contactphone_textbox)

                self.sendKeys(addressline1, self._origin_addressline1_textbox)

                if addressline2 != "":
                    self.sendKeys(addressline2, self._origin_addressline2_textbox)

                if remarks != "":
                    self.sendKeys(remarks, self._origin_pickupremarks_textbox)

                if readytime != "":
                    readytime1 = datetime.strptime(readytime, "%I:%M %p")
                    finalreadytime = datetime.strftime(readytime1, "%H:%M")
                    self.pageVerticalScroll(self._origin_readytime_div, "id")
                    self.elementClick(self._origin_readytime_div, "id")

                    self.waitForElement(self._origin_readytime_menu + self._dropdown_list, "xpath",
                                        event="display")

                    self.selectBasedOnText(self._origin_readytime_menu + self._dropdown_list, "xpath",
                                           selectValue=finalreadytime)

                if closetime != "":
                    closetime1 = datetime.strptime(closetime, "%I:%M %p")
                    finalclosetime = datetime.strftime(closetime1, "%H:%M")
                    self.pageVerticalScroll(self._origin_closetime_div, "id")
                    self.elementClick(self._origin_closetime_div, "id")

                    self.waitForElement(self._origin_closetime_menu + self._dropdown_list, "xpath",
                                        event="display")

                    self.selectBasedOnText(self._origin_closetime_menu + self._dropdown_list, "xpath",
                                           selectValue=finalclosetime)

            elif field.lower() == "destination":

                self.sendKeys(companyname, self._destination_companyname_textbox)

                self.sendKeys(contactname, self._destination_contactname_textbox)

                self.sendKeys(contactphone, self._destination_contactphone_textbox)

                self.sendKeys(addressline1, self._destination_addressline1_textbox)

                if addressline2 != "":
                    self.sendKeys(addressline2, self._destination_addressline2_textbox)

                if remarks != "":
                    self.sendKeys(remarks, self._destination_deliveryremarks_textbox)

                if readytime != "":
                    readytime1 = datetime.strptime(readytime, "%I:%M %p")
                    finalreadytime = datetime.strftime(readytime1, "%H:%M")

                    self.pageVerticalScroll(self._destination_readytime_div, "id")
                    self.elementClick(self._destination_readytime_div, "id")

                    self.waitForElement(self._destination_readytime_menu + self._dropdown_list, "xpath",
                                        event="display")

                    self.selectBasedOnText(self._destination_readytime_menu + self._dropdown_list, "xpath",
                                           selectValue=finalreadytime)
                if closetime != "":
                    closetime1 = datetime.strptime(closetime, "%I:%M %p")
                    finalclosetime = datetime.strftime(closetime1, "%H:%M")
                    self.pageVerticalScroll(self._destination_closetime_div, "id")
                    self.elementClick(self._destination_closetime_div, "id")

                    self.waitForElement(self._destination_closetime_menu + self._dropdown_list, "xpath",
                                        event="display")

                    self.selectBasedOnText(self._destination_closetime_menu + self._dropdown_list, "xpath",
                                           selectValue=finalclosetime)

            else:
                self.log.error("Passed field value is invalid or not present :" + str(field))
                return False

        except Exception as e:
            self.log.error("Exception" + str(e))
            self.log.error("Passed locators or locator types  are incorrect for enterAddressContainerDetails method ")

    # --------- Cost Only Section starts--------

    def validateAddressBookNotPresentInCostOnly(self):
        """
            Validating default cost page

        :return: if disabled than return True
        """
        self.log.info("Inside validateDefaultCostPage Method")
        originAddressIconStatusResult = self.isElementDisplayed(self._origin_address_icon, locatorType="id")
        destinationAddressIconStatusResult = self.isElementDisplayed(self._destination_address_icon, locatorType="id")
        if not originAddressIconStatusResult and not destinationAddressIconStatusResult:
            self.log.info("Origin & Destination address books are not present.")
            return True
        else:
            self.log.info("Origin or Destination address book is present.")
            return False

    def selectedSiteType(self, field=""):
        """
             Under this method , we are returning the site type selected based on the parameter."""
        selected_SiteType_value = ""
        try:
            if field.lower() == "origin":
                if self.isElementPresent(self._origin_siteType, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._origin_siteType, locatorType="xpath")
                elif self.isElementPresent(self._origin_siteType1, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._origin_siteType1, locatorType="xpath")
                return selected_SiteType_value

            elif field.lower() == "destination":
                if self.isElementPresent(self._destination_siteType, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._destination_siteType, locatorType="xpath")
                elif self.isElementPresent(self._destination_siteType1, locatorType="xpath"):
                    selected_SiteType_value = self.getText(self._destination_siteType1, locatorType="xpath")
                return selected_SiteType_value
            else:
                self.log.error("Passed Field value is incorrect or not present : " + str(field))
        except Exception as e:
            self.log.error(
                "Passed locators or locator types  are incorrect for verifySiteTypeSelected method - Field "
                "passed : " + str(field) + "Locator : " + str(selected_SiteType_value))
            self.log.error("Exception" + str(e))

    def verifyDefaultRouteDetailsCostOnly(self):
        """
        Validating mandatory fields & default icon status in the routesdetails section
        :return: True if all validations as expected else False
        """
        try:
            self.log.info("Inside verifyDefaultRouteDetailsCostOnly Method Required Field Validation")
            # Validating the Required fields in route details section
            requiredFlag = True

            origin = self.getText(self._origin_textfield_value, "id")
            requiredFlag = requiredFlag and self.validators.verify_text_match(origin, "Required")
            self.log.info("origin field : " + str(requiredFlag))

            destination = self.getText(self._destination_textfield_value, "id")
            requiredFlag = requiredFlag and self.validators.verify_text_match(destination, "Required")
            self.log.info("destination field : " + str(requiredFlag))
            self.log.info("Final Flag Status : " + str(requiredFlag))

            originAddressIconStatusResult = self.isElementDisplayed(self._origin_address_icon, locatorType="id")
            destinationAddressIconStatusResult = self.isElementDisplayed(self._destination_address_icon,
                                                                         locatorType="id")
            if not originAddressIconStatusResult and not destinationAddressIconStatusResult:
                self.log.info("Origin & Destination address books are not present.")
                addressBookIconStatusResult = True
            else:
                self.log.info("Origin or Destination address book is present.")
                addressBookIconStatusResult = False

            self.log.info(
                "Final Status : requiredFlag - " + str(requiredFlag) + ", addressBookIconStatusResult - " + str(
                    addressBookIconStatusResult))
            if requiredFlag and addressBookIconStatusResult:
                return True
            else:
                return False

        except Exception as e:
            self.log.error(
                "Passed locators or locator types  are incorrect for verifyDefaultRouteDetailsCostOnly method")
            self.log.error("Exception" + str(e))

    def validateDefaultTimeInContainer(self):
        """
        Validating the default address details container for Ready Time and Close Time.
        :return: True if not changed else False
        """
        try:
            self.log.info("Inside validateDefaultTimeInContainer Method")
            originreadytime = self.getText(self._origin_readytime_value, "xpath")
            originclosetime = self.getText(self._origin_closetime_value, "xpath")

            destreadytime = self.getText(self._destination_readytime_value, "xpath")
            destclosetime = self.getText(self._destination_closetime_value, "xpath")
            self.log.debug(
                "origin ready time : " + str(originreadytime) + " ,Origin Close Time : " + str(originclosetime) +
                " ,Destination ready Time : " + str(destreadytime) + " ,Detination close Time : " + str(destclosetime))

            if originreadytime == "09:00" and originclosetime == "17:00" and destreadytime == "09:00" \
                    and destclosetime == "17:00":
                self.log.debug("Time not changed in address container")
                return True
            else:
                self.log.debug("Time changed in address container")
                return False
        except Exception as E:
            self.log.error("Exception Occurred in validateDefaultTimeInContainer Method and Exception is : " + str(E))

    @property
    def origin_textfield_value(self):
        return self._origin_textfield_value
