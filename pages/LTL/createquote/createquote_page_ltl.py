from selenium.webdriver.common.by import By

from pages.common.createquote_page import CreateQuotePage

CREATE_ORDER_BUTTON = (By.ID, "footer_LTL1-1247_createOrderButtonCreateOrder")


class CreateQuotePageLTL(CreateQuotePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    # Locators
    # ----------------------- Quote Page Common Locators----------------------------------
    _quote_order_icon = "//nav[contains(@class,'side-navstyle__LeftBarNav')] //a[@href='/create-quote']"
    _create_quote = "//div[text()='Create Quote']"
    # _middle_section_disabled_elements = "div[class*='appstyle__RightSection'] div[class*='cpVmJf']"
    _middle_section_disabled_elements = "div[subscript = 'center'] div[class*= 'hGYFfd']"
    _middle_section_enabled_elements = "div[subscript = 'center'] div[class*='hmmPrY']"
    _navigation_confirmation_popup = "//div[contains(@class,'Content--after-open')]"
    _navigation_cancel_button = _navigation_confirmation_popup + "//span[text()='Cancel']/parent::button"
    _navigation_confirm_button = _navigation_confirmation_popup + "//span[text()='Confirm']/parent::button"
    _alert_message_element = _navigation_confirmation_popup + "//div[contains(@class,'alertstyles__Text')]"
    _alert_message_content = "You are attempting to navigate away in the process of creating an order. Are you sure?"
    _qoute_page_spinner = \
        "//div[contains(@class,'SpinnerOverlay') and contains (.,'Loading') and following-sibling::div]/div"
    # ---------------------- Customer Section Locators--------------------------
    _get_customer_value = "createQuote_LTL1-279_select_2"
    # _customer_name_field = "//label[text()='*Customer']/following-sibling::div //input"
    _customer_name_field = "//div[@id='" + _get_customer_value + "'] //input"
    _customer_rates_radio = "label[for='createQuote_LTL1-279_createQuoteRadios-0']"
    _cost_only_radio = "//input[@id='createQuote_LTL1-279_createQuoteRadios-1']/following-sibling::label"
    _customer_lists = "//div[@id='" + _get_customer_value + \
                      "'] //div[contains(@class,'-menu')] //div[contains(@class,'itemstyles__Header')]"
    _customer_contact_list = "//div[@id='" + _get_customer_value + \
                             "'] //div[contains(@class,'-menu')] //div[contains(@class,'itemstyles__Detail')]"
    _customer_spinner = "//div[@id='" + _get_customer_value + "'] //div[contains(@class,'css-1wy0on6')]/child::*"
    _cost_currency = "createQuote_LTL1-279_select_1"
    _cost_currency_drop_list = "//div[@id='createQuote_LTL1-279_select_1'] //div[contains(@class,'-menu')] //div"
    _servicetype_field = "createQuote_LTL1-279_select_3"
    _servicetype_drop_list = "//div[@id='createQuote_LTL1-279_select_3'] //div[contains(@class,'-menu')] //div"
    _equipmenttype_field = "createQuote_TL-688_EquipmentTypeSection"
    _equipmenttype_input = "//div[@id='createQuote_TL-688_EquipmentTypeSection']//input"
    _equipmenttype_drop_list = "#createQuote_TL-688_EquipmentTypeSection div[id*='select']"
    # ---------------------- Accessorials & Special Services Section Locators--------------------------
    _acc_section = "undefined_Card-Title"
    _acc_checkbox_label = "#" + _acc_section + " label[class*='CheckboxContainer']"

    _acc_checkboxclass = "/following-sibling::div"
    _acc_getAccValue = "/child::*"
    _ssacc_protectfromfreeze = "//input[@id='accessorials-n-services_ltl-1491_116']" + _acc_checkboxclass
    _ssacc_sortandsegregate = "//input[@id='accessorials-n-services_ltl-1491_108']" + _acc_checkboxclass
    _oacc_appointment = "//input[@id='accessorials-n-services_ltl-1491_152']" + _acc_checkboxclass
    _oacc_inside = "//input[@id='accessorials-n-services_ltl-1491_105']" + _acc_checkboxclass
    _oacc_liftgate = "//input[@id='accessorials-n-services_ltl-1491_11']" + _acc_checkboxclass
    _dacc_notifypriortoarrival = "//input[@id='accessorials-n-services_ltl-1491_17']" + _acc_checkboxclass
    _dacc_appointment = "//input[@id='accessorials-n-services_ltl-1491_153']" + _acc_checkboxclass
    _dacc_inside = "//input[@id='accessorials-n-services_ltl-1491_15']" + _acc_checkboxclass
    _dacc_liftgate = "//input[@id='accessorials-n-services_ltl-1491_12']" + _acc_checkboxclass
    _dacc_tradeshowdelivery = "//input[@id='accessorials-n-services_ltl-1491_101']" + _acc_checkboxclass
    _acc_specialservices = "accessorials-n-services_ltl-1491_0"
    _acc_origin = "accessorials-n-services_ltl-1491_1"
    _acc_destination = "accessorials-n-services_ltl-1491_2"
    _acc_checkbox_checked = "sc-fzozJi lgyiUX"
    _acc_checkbox_unchecked = "sc-fzozJi gJiFpy"

    # ----------------------------------- Notes Section Locators-----------------------------------------------
    _note_section = "//div[contains(@class,'Notesstyles__NotesContainer')]"
    _note_save_button = "notes_LTL1-602_container_saveButton"
    _note_cancel_button = "notes_LTL1-602_container_cancelButton"
    _note_error_msg = _note_section + "//div[contains(@class,'ErrorMessage')]"
    _note_text = "notes_LTL1-602_container_textarea"
    _note_defaulttextmsg = "Enter text..."

    # _note_type_icon = "//label[@for='noteType']/following-sibling::div //div[contains(@class,'css-16ii')]"
    # _note_type_icon = "//label[@for='noteType']/following-sibling::div //div[contains(@class,'css-1wy0on6')]"
    # _note_type_get = "//label[@for='noteType']/following-sibling::div //div[contains(@class,'css-16ii')]/div"
    # _note_type_list = "//label[@for='noteType']/following-sibling::div //div[contains(@class,'-menu')]/div/div"

    # _note_save_button = \
    #     "//label[@for='noteType']/ancestor::div[2]/following-sibling::div[contains(@class,'ButtonWrapper')]/button[2]"
    # _note_cancel_button = \
    #     "//label[@for='noteType']/ancestor::div[2]/following-sibling::div[contains(@class,'ButtonWrapper')]/button[1]"
    # _note_error_msg = \
    #     "//label[@for='noteType']/ancestor::div[2]/following-sibling::div[contains(@class,'ErrorMessage')]"
    # _note_text = \
    #     "//label[@for='noteType']/ancestor::div[2]/following-sibling::div[contains(@class,'TextAreaWrapper')]/textarea"

    _note_msg = "Please enter note text"
    _note_exceed_string = "Its says I need to type at least ten characters, so here's this. Y'know what? I'm gonna " \
                          "type one hundred characters instead. Actually, I'm going to type five hundred characters. " \
                          "I'm definitely not going to type anywhere near one thousand characters, because that'd be " \
                          "ridiculous. Even if I wanted to type one thousand characters, I have to go to bed now " \
                          "anyway,so I simply don't have the time. I mean, I could just type a bunch of random " \
                          "letters or hold down one key, but that would be no fun at all, finally hav to verify " \
                          "the Notes."

    _saved_note_section = "//div[contains(@class,'note-liststyle__NotesContainer')]"
    # _saved_note_base = "//span[contains(@class,'note-liststyle__NotesSectionText')]/following-sibling::div[1]"
    _saved_note_base = "noteList_LTL1-602_item0"
    _saved_note_type = "//div[@id='" + _saved_note_base + "']//div[contains(@class,'NoteHeader')]/div[1]"
    _saved_note_text = "//div[@id='" + _saved_note_base + "']//div[contains(@class,'NoteText')]"

    # -------------------------Create Order -------------------------------------------
    _create_order_popup = "footer_LTL1-1247_createOrderModalSuccess"

    _order_popup = "//div[contains(@class,'ReactModal__Content')]"
    _create_order_popup_msg = _order_popup + "/div[2]"
    _order_popup_close = _order_popup + "//span/parent::div"

    _soBoard_button = "footer_LTL1-1247_createOrderModalButtonGoToSalesOrderBoard"
    _bol_page_button = "footer_LTL1-1247_createOrderModalButtonGoToBol"
    _bol_text = "//button[@id='" + _bol_page_button + "']/span"

    _createorder_button = "footer_LTL1-1247_createOrderButtonCreateOrder"
    _order_confirmation_popup = "footer_LTL1-1297_createOrderConfirmationModal"
    _confirmation_popup_text = "//div[@id='" + _order_confirmation_popup + "']/div[2]"
    _confirm_button_confirmation_popup = "footer_LTL1-1297_createOrderConfirmationModalConfirm"
    _cancel_button_confirmation_popup = "footer_LTL1-1297_createOrderConfirmationModalCancel"

    _identifier_quote = "//span[text()='Quote #']/parent::div/following-sibling::div//span"
    _identifier_reference = "//span[text()='Reference #']/parent::div/following-sibling::div//span"
    # _identifier_quote = "//label[text()='Identifier']/following-sibling::span"

    _confirmation_message = \
        "Do you wish to create an order? Please confirm that all information you have entered is correct."
    _confirmation_note_message = "Delivery Times other than 9-5 must be noted."
    _success_msg = "Order Created!"

    # _saved_sonote_section = "//div[@id='notes_LTL1-602_container_notesContainer']"
    _saved_sonote_section = "//div[@id='notes_LTL1-602_container_notesWrapper']"
    _saved_sonote_base = "notes_LTL1-602_container_item"
    _saved_sonote_type = "//div[contains(@class,'NoteHeader')]/div[1]"
    _saved_sonote_text = "//div[contains(@class,'NoteText')]"

    # Update Order Locators

    _quote_page_spinner = "//div[text()='Loading']"
    _updateorder_button = "footer_LTL1-875_updateOrderButton"
    _editsalesorder_button = "footer_ltl1-2633_editSalesOrderButton"
    _create_quote_base_container = "//div[@id='CreateQuoteBase-container']"
    _DestinationAddressContainerSection = "location_destination_LTL1-603_locationCard"
    _updatemodal = "footer_LTL1-950_saveChangesModal"
    _updatemodal_confirmbutton = "footer_LTL1-950_saveChangesModalButtonYes"

    _savedquotebtn = "footer_LTL1-1639_saveQuoteButton"
    _savedquote_popup = "footer_LTL1-1639_saveQuoteModal"
    _savedquote_refinputfield = "footer_LTL1-1639_saveQuoteConfirmation_inputField_1"
    _savedquote_cancelbtn = "footer_LTL1-1639_saveQuoteConfirmationModalCancel"
    _savedquote_savebtn = "footer_LTL1-1639_saveQuoteConfirmationModalConfirm"
    _savedquote_header = "//div[@id ='" + _savedquote_popup + "']//div[contains(@class,'ModalViewstyles__Title')]"
    _savedquote_headervalue = "Save to Saved Quotes"

    _return_order_button = "footer_LTL1-875_returnToOrderButton"
    _return_order_modal = "footer_LTL1-878_returnToOrderModal"
    _return_order_yes_button = "//div[@id='footer_LTL1-878_returnToOrderModal']//button[" \
                               "@id='footer_LTL1-878_returnToOrderModalButtonYes'] "
    _return_order_no_button = "//div[@id='footer_LTL1-878_returnToOrderModal']//button[" \
                              "@id='footer_LTL1-878_returnToOrderModalButtonNo'] "

    # ------------------------------------- Quote page common Method-------------------------------------------

    def validateCreateOrderMessage(self):
        """
        validating the Order Confirmation Message
        :return: True if Message match else False
        """
        try:
            self.log.info("Inside validateCreateOrderMessage Method")
            appMsg = self.getText(self._create_order_popup_msg, "xpath")
            self.log.info("Get text from application")
            return self.validators.verify_text_match(appMsg, self._success_msg)
        except Exception as E:
            self.log.error("Exception occurred in validateCreateOrderMessage Method and Exception is : " + str(E))

    def getIdentifierValue(self, identifier):
        """
        Getting identifier Value based on passed identifier Name
        :param identifier: identifier Name
        :return: Identifier Value
        """
        try:
            self.log.info("Inside getIdentifierValue Method")
            if identifier.lower() == "quote":
                return self.getText(self._identifier_quote, "xpath")
            elif identifier.lower() == "reference":
                return self.getText(self._identifier_reference, "xpath")
            else:
                self.log.error("Incorrect Identifier Passed in getIdentifierValue Method.")
        except Exception as E:
            self.log.error("Exception occurred in getIdentifierValue Method, and Exception is : " + str(E))

    def orderPopupClick(self, Button):
        """
        Order Popup click Method
        :param Button: the Button want to click pass as argument
        :return: True if clicked
        """
        self.log.info("Inside orderPopupClick Method")
        try:
            if Button.lower() == "close":
                self.elementClick(self._order_popup_close, "xpath")
                self.waitForElement(self._order_popup, "xpath", event="notdisplay")
                return True
            else:
                self.log.error("Incorrect Button Name Passed")
                return False
        except Exception as E:
            self.log.error("Exception Occurred in orderPopupClick Method and Exception is : " + str(E))

    def getBolNumber(self):
        """
        Get the BOL Number from Order confirmation Popup
        :return: BOLNumber
        """
        self.log.info("inside getBolNumber Method")
        try:
            fullBOLText = self.getText(self._bol_page_button, "id")
            self.log.info("Complete BOl Text : " + str(fullBOLText))
            BolNumber = fullBOLText.split("#")[1]
            self.log.info("BOl Number : " + str(BolNumber))
            return BolNumber
        except Exception as E:
            self.log.error("Exception occured in getBolNumber Method : " + str(E))

    def defaultValidationsSaveOrder(self):
        """
        Method for default validations regarding the save order.
        :return: if all validations are correct else false
        """
        self.log.info("Inside defaultValidationsSaveOrder Method")
        try:
            combinedResult = True
            self.waitForElement(self._savedquotebtn, "id", event="display")
            self.elementClick(self._savedquotebtn, "id")
            self.waitForElement(self._savedquote_popup, "id", event="display")

            savedQuoteHeaderResult = self.validators.verify_text_match(self.getText(self._savedquote_header, "xpath"),
                                                                       self._savedquote_headervalue)
            self.log.info("Saved Quote Header Status : " + str(savedQuoteHeaderResult))
            combinedResult = combinedResult and savedQuoteHeaderResult

            self.elementClick(self._savedquote_savebtn, "id")
            emptyInputValidationResult = self.validators.verify_text_match(
                self.getAttribute(self._savedquote_refinputfield, "id", "placeholder"),
                "Please enter a reference")
            self.log.info(
                "Input field place holder validation status for empty data : " + str(emptyInputValidationResult))
            combinedResult = combinedResult and emptyInputValidationResult

            self.enterReferenceForSaveOrder("test!")
            self.elementClick(self._savedquote_savebtn, "id")
            self.sendKeys("", self._savedquote_refinputfield, "id")
            specialCharaInputValidationResult = self.validators.verify_text_match(
                self.getAttribute(self._savedquote_refinputfield, "id", "placeholder"),
                "Reference cannot contain special symbols")
            self.log.info("Input field place holder validation status for special characters : " + str(
                specialCharaInputValidationResult))
            combinedResult = combinedResult and specialCharaInputValidationResult

            self.enterReferenceForSaveOrder("mi")
            self.elementClick(self._savedquote_savebtn, "id")
            self.sendKeys("", self._savedquote_refinputfield, "id")
            minimumCharaInputValidationResult = self.validators.verify_text_match(
                self.getAttribute(self._savedquote_refinputfield, "id", "placeholder"),
                "Reference must be at least 3 characters")
            self.log.info("Input field place holder validation status for minimum characters : " + str(
                minimumCharaInputValidationResult))
            combinedResult = combinedResult and minimumCharaInputValidationResult

            self.enterReferenceForSaveOrder("maximum characters checking in the saved quotes input field")
            self.elementClick(self._savedquote_savebtn, "id")
            self.sendKeys("", self._savedquote_refinputfield, "id")
            maximumCharaInputValidationResult = self.validators.verify_text_match(
                self.getAttribute(self._savedquote_refinputfield, "id", "placeholder"),
                "Reference can be up to 15 characters")
            self.log.info("Input field place holder validation status for maximum characters : " + str(
                maximumCharaInputValidationResult))
            combinedResult = combinedResult and maximumCharaInputValidationResult

            self.elementClick(self._savedquote_cancelbtn, "id")
            self.waitForElement(self._savedquote_popup, "id", event="notdisplay")

            return combinedResult
        except Exception as E:
            self.log.error("Exception occurred in defaultValidationsSaveOrder Method and Exception is : " + str(E))

    def verifySaveOrderButtonEnabled(self):
        """
        Validate Save Order Button is enabled or not
        :return: if enabled than true else false
        """
        self.log.info("Inside verifySaveOrderButtonEnabled Method")
        try:
            buttonAttribute = self.getAttribute(self._savedquotebtn, "id", "data-disabled")
            self.log.info("Class Value : " + str(buttonAttribute))
            if buttonAttribute == "false":
                self.log.info("Button Enabled")
                return True
            else:
                self.log.info("Button Disabled")
                return False

        except Exception as E:
            self.log.error("Exception occurred in verifySaveOrderButtonEnabled Method and Exception is : " + str(E))

    def enterReferenceForSaveOrder(self, sqreference):
        """
        Method to enter reference for the save quote.
        """
        self.log.info("Inside enterReferenceForSaveOrder Method and passed saved quote reference - " + str(sqreference))
        try:
            self.waitForElement(self._savedquotebtn, "id", event="display")
            self.elementClick(self._savedquotebtn, "id")
            self.waitForElement(self._savedquote_popup, "id", event="display")
            self.sendKeys(sqreference, self._savedquote_refinputfield, "id")
            self.elementClick(self._savedquote_savebtn, "id")
            self.waitForElement(self._savedquote_popup, "id", event="notdisplay")
        except Exception as E:
            self.log.error("Exception occurred in enterReferenceForSaveOrder Method and Exception is : " + str(E))

    def verifyCreateOrderButtonEnabled(self):
        """
        Validate Create Order Button is enabled or not in footer
        :return: if enabled than true else false
        """
        self.log.info("Inside createOrderButtonEnabled Method")
        try:
            button_disabled = self.get_attribute(locator=CREATE_ORDER_BUTTON, attribute_type="data-disabled")

            if button_disabled is None:
                self.log.info("Button enabled")
                return True

            if button_disabled:
                self.log.info("Button disabled")
                return False

            return button_disabled
        except Exception as E:
            self.log.error("Exception occurred in createOrderButtonEnabled Method and Exception is : " + str(E))

    def createOrder(self, DefaultTime=True):
        """
        Clicking Create Order Button
        :return:
        """
        self.log.info("Inside create Order Button")
        try:
            commonResult = True
            self.elementClick(self._createorder_button, "id")
            if DefaultTime:
                self.waitForElement(self._order_confirmation_popup, "id", "display")
                textValue = self.getText(self._confirmation_popup_text, "xpath")
                self.log.info("Display confirmation Message : " + str(textValue))
                splitMessage = textValue.split("Note:")
                messageResult = self.validators.verify_text_match(splitMessage[0].strip(), self._confirmation_message)

                noteResult = self.validators.verify_text_match(splitMessage[1].strip(), self._confirmation_note_message)
                self.elementClick(self._confirm_button_confirmation_popup, "id")
                commonResult = messageResult and noteResult

            self.waitForElement(self._order_confirmation_popup, "id", "notdisplay")

            return commonResult

        except Exception as E:
            self.log.error("Create Order Failed : " + str(E))

    def waitForOrderPopup(self):
        """
        Waiting for order confirmation popup
        :return:
        """
        self.log.info("Inside Wait for Popup")
        try:
            # self.waitForElement(self._order_popup, "xpath", event="display", timeout=30)
            self.waitForElement(self._order_popup, "xpath", event="display", timeout=120)
        except Exception as E:
            self.log.error("Exception in waitForOrderPopup Method and Exception is : " + str(E))

    def verifyNavigationConfirmationText(self):
        """
        validating navigation confirmation text
        :return:
        """
        try:
            self.waitForElement(self._navigation_confirmation_popup, "xpath", "display")
            actual = self.getText(self._alert_message_element, "xpath")
            return self.validators.verify_text_match(actual, self._alert_message_content)
        except Exception as E:
            self.log.error("Passed Locator incorrect or updated for method verifyNavigationConfirmationText.")
            self.log.error(f"Exception: {str(E)}")

    def confirmationPopDisplay(self):
        """confirmation popup display check"""
        try:
            self.log.info("Inside confirmationPopDisplay Method")
            return self.isElementPresent(self._navigation_confirmation_popup, "xpath")
        except Exception as e:
            self.log.error("exception occurred in confirmationPopDisplay method and Exception is ." + str(e))

    def clickButtonInQuote(self, ButtonName):
        """
        Clicking the Button
        :param ButtonName: Name of Button want to click
        :return:
        """
        try:
            self.log.info("Inside clickButtonInQuote Method for Button Name :" + str(ButtonName))
            if ButtonName.lower() == "navigationcancel":
                self.waitForElement(self._navigation_confirmation_popup, "xpath", "display")
                self.elementClick(self._navigation_cancel_button, "xpath")
                self.waitForElement(self._navigation_confirmation_popup, "xpath", "notdisplay")
                return not self.isElementDisplayed(self._navigation_confirmation_popup, "xpath")
            elif ButtonName.lower() == "navigationconfirm":
                self.waitForElement(self._navigation_confirmation_popup, "xpath", "display")
                self.elementClick(self._navigation_confirm_button, "xpath")
                self.waitForElement(self._navigation_confirmation_popup, "xpath", "notdisplay")
                return not self.isElementDisplayed(self._navigation_confirmation_popup, "xpath")
            else:
                self.log.error("Incorrect Button Name Passed, Passed Button Name is : " + str(ButtonName))
        except Exception as E:
            self.log.error("Passed Locator incorrect or element get updated for method clickButtonInQuote,"
                           " Passed Button Name :" + str(ButtonName))
            self.log.error(f"Exception: {str(E)}")

    def validateQuoteOrderIcon(self):
        """
            Validating whether icon highlighted or not.
        :return: if highlight than true else false
        """
        value = self.getAttribute(self._quote_order_icon, locatorType="xpath", attributeType="class")
        self.log.info(value)
        if value == "active":
            return True
        else:
            return False

    def verifyQuotePage(self):
        """
            Verify Quote page is in default state or not.
        """
        self.log.info("Inside verifyQuoteContainer")
        result = self.isElementDisplayed(self._create_quote, locatorType="xpath")
        self.log.info(result)
        # To validate disable route section ###
        disablePage = self.verify_quote_page_is_disabled()
        if result and disablePage:
            return True  # page in default state
        else:
            return False  # page not in default state

    def waitForPageLoaderShowAndDisappear(self):
        """
        Waiting for page loader first shows up and then disappear
        :return:
        """
        self.log.info("Inside Wait for page loader")
        try:
            self.waitForElement(self._qoute_page_spinner, "xpath", event="display", timeout=40)
            self.waitForElement(self._qoute_page_spinner, "xpath", event="notdisplay", timeout=40)

        except Exception as E:
            self.log.error("Exception in waitForPageLoaderShowAndDisappear Method and Exception is : " + str(E))

    # ------------------------------------Customer Section Start-------------------------------------------

    def validateCreateQuoteContainerFields(self):
        """
            Validating Create Quote Container Customer field element display
            :return: if display than true else false
        """
        EnteredValue = self.getText(self._get_customer_value, "id")
        self.log.info(EnteredValue)
        if EnteredValue == "Enter customer name":
            return True
        else:
            return False

    def clickCustomerRatesRadio(self):
        """
            Clicking Customer Rates Radio Button
        """
        self.log.info("Inside clickCustomerRatesRadio method")
        self.elementClick(self._customer_rates_radio, "css")

    def clickCostOnlyRadio(self):
        """
            Clicking Cost Only Radio Button
        """
        try:
            self.log.info("Inside clickCostOnlyRadio method")
            self.elementClick(self._cost_only_radio, "xpath")
            self.waitForQuotePageEnable(timeout=40)
        except Exception as E:
            self.log.error("Method clickCostOnlyRadio : Passed locator incorrect or element get updated")
            self.log.error("Exception : " + str(E))

    def selectServiceType(self, serviceType="LTL"):
        """ Select Service Type based on passed value"""
        try:
            self.log.info("Inside selectServiceType method")
            if self.getText(self._servicetype_field, "id") != serviceType.upper():
                self.elementClick(self._servicetype_field, "id")
                self.selectBasedOnText(self._servicetype_drop_list, "xpath", serviceType.upper())

            if self.getText(self._servicetype_field, "id") == serviceType.upper():
                self.log.info(f"Selected passed service type: {serviceType}")
                return True
            else:
                self.log.error(
                    "Passed Service Type is invalid, hence cannot select, Service Type :  " + str(serviceType.upper()))
                return False

        except Exception as E:
            self.log.error("Method selectServiceType : Passed locator incorrect or element get updated")
            self.log.error("Exception : " + str(E))

    def waitForCustomerList(self):
        """
            Waiting for Customer list to display in page.
        """
        self.log.info("inside customer list wait method")
        self.waitForElement(self._customer_lists, "xpath", "display", timeout=40)

    def verifySelectedCustomer(self, customerName=""):
        """
        :param customerName: Default Empty
        :return: comparing between passed customer name and from element get customer name
                if get element from page is in passed customer than returning true else False.
        """
        self.log.info("Inside verifySelectedCustomer Method")
        ExpectedCustomerName = customerName
        self.log.info("ExpectedCustomerName : " + str(ExpectedCustomerName))
        ActualCustomerName = self.getText(self._get_customer_value, "id")
        self.log.info("ActualCustomerName : " + str(ActualCustomerName))
        if ActualCustomerName.lower().strip() in ExpectedCustomerName.lower().strip():
            self.log.info("Returning True")
            return True
        else:
            self.log.info("Returning False")
            return False

    def verifyCustomerListBasedOnSearchData(self, customerName):
        """
            Based on the Customer search Data validating the List of display Customers

            :return : if customer list is 0 than False
                      elif search data not found in list than false
                      else True
        """
        self.log.info("Inside verifyCustomerListBasedOnSearchData Method")
        customerLists = self.getElementList(self._customer_lists, "xpath")
        if len(customerLists) == 0:
            self.log.error("Based on the search text in customer field data not populating.")
            return False

        for i in customerLists:
            self.log.info("Inside For Loop")
            textOfEachElement = i.text  # each element text Value
            self.log.info("List element text Value : " + str(textOfEachElement))
            if customerName.lower() not in textOfEachElement.lower():
                return False
        return True

    # TODO - move to common
    def searchAndSelectCustomer(self, customerName, index=-1):
        """
        Under this Method we are searching and selecting customer from drop down list.
        Once customer selected validating other section accessibility and returning result
        """
        self.log.info("Inside searchAndSelectCustomer Method, passed customer Name is : " + str(customerName) +
                      ",index: " + str(index))
        self.search_customer(customerName)
        searchListResult = self.verifyCustomerListBasedOnSearchData(customerName)

        index = int(index)
        # if index > 0:
        #     selectedCustomer = self.selectCustomerFromList(index + 1)
        # else:
        #     selectedCustomer = self.selectCustomerFromList(index - 1)
        #
        # # selectedCustomer = self.selectCustomerFromList(index+1)

        # self.searchCustomer(customerName)
        # searchListResult = self.verifyCustomerListBasedOnSearchData(customerName)
        selectedCustomer = self.select_customer_from_list(index)

        self.log.info("Selected Customer : " + str(selectedCustomer[0]))

        serviceTypeResult = self.selectServiceType()

        customerResult = self.verifySelectedCustomer(selectedCustomer[0])
        self.waitForElement(self._middle_section_enabled_elements, "css", event="display", timeout=15)
        if self.verify_quote_page_is_disabled():
            pageDisable = False
        else:
            pageDisable = True
        self.log.info("Returning Value from searchListResult : " + str(searchListResult) + " ,customerResult : " +
                      str(customerResult) + " ,pageDisable : " + str(pageDisable) + " ,serviceTypeResult : " + str(
            serviceTypeResult))

        if searchListResult and customerResult and pageDisable and serviceTypeResult:
            return True, selectedCustomer[0], selectedCustomer[1], selectedCustomer[2]
        else:
            return False, selectedCustomer[0], selectedCustomer[1], selectedCustomer[2]

    def waitForQuotePageEnable(self, timeout=15):
        try:
            self.log.info("Inside waitForQuotePageEnable Method")
            self.waitForElement(self._middle_section_enabled_elements, "css", event="display", timeout=timeout)
        except Exception as E:
            self.log.error("Exception occurred in waitForQuotePageEnable Method and Exception is : " + str(E))

    # ------------------------------------Accessorials Method Section -------------------------------------------

    def getAccessorialsList(self, accessorialList, field=""):
        """Under this method, we are returning accessorials list in the format required to validate the selected
        accessorials for given address based on the field """
        try:

            selectedaccessorials_list = []
            if accessorialList != "":
                accessorials_list = accessorialList.split(',')
                if field.lower() == "origin":
                    for i in accessorials_list:
                        i = i.replace("\n", "")
                        if i.lower().replace(" ", "").replace(field, "") == "sortandsegregate" or i.lower().replace(
                                " ", "").replace(field, "") == "protectfromfreeze":
                            selectedaccessorials_list.append(i.lower().strip().replace(" ", ""))
                        else:
                            selectedaccessorials_list.append(
                                field + i.lower().strip().replace(" ", ""))

                elif field.lower() == "destination":
                    for i in accessorials_list:
                        i = i.replace("\n", "")
                        if i.lower().replace(" ", "").replace(field, "") == "sortandsegregate" or i.lower().replace(
                                " ", "").replace(field, "") == "protectfromfreeze":
                            # or i.lower().strip() == "tradeshow delivery":
                            selectedaccessorials_list.append(i.lower().strip().replace(" ", ""))
                        else:
                            selectedaccessorials_list.append(
                                field + i.lower().strip().replace(" ", ""))

                else:
                    self.log.error("Passed Field value is incorrect or not present : " + str(field))

            return selectedaccessorials_list

        except Exception as e:
            self.log.error("Passed locators or locator types  are incorrect for "
                           "getAccessorialsList method : " + str(field) + " value : " + str(accessorialList))
            self.log.error("Exception" + str(e))

    def validateAccessorialsBasedOnSections(self, accSection, accessorialList):
        """
        The method to validate accessorials based on sections.
        :param accSection: Accessorial section
        :param accessorialList: Accessorial list
        :return: If all the accessorials are correct return True.
        """
        self.log.info("Inside validateAccessorialsBasedOnSections Method")
        self.log.info("Accessorial List :" + str(accessorialList))
        if accessorialList != "":
            accessorialBasedOnSectionResult = True
            accessorialBasedOnSectionstr = self.getText(accSection, locatorType="id")
            accessorialBasedOnSection = accessorialBasedOnSectionstr.rstrip()
            for accessorialfromlist in accessorialList:
                if accessorialBasedOnSection.find(str(accessorialfromlist)) == -1:
                    accessorialPresent = False
                else:
                    accessorialPresent = True
                accessorialBasedOnSectionResult = accessorialPresent and accessorialBasedOnSectionResult
            return accessorialBasedOnSectionResult
        else:
            return True

    def validateAllAccessorialsBasedOnSections(self):
        """
        The method to validate all the accessorials based on the section.
        :return: If all the accessorials are correct return True.
        """
        self.log.info("Inside validateAllAccessorialsBasedOnSections Method")
        _ssaccessorialList = self.csvRowDataToList("specialservicesaccessorials", "defaultlist.csv")
        self.log.info("_ssaccessorialList :" + str(_ssaccessorialList))
        _oaccessorialList = self.csvRowDataToList("originaccessorials", "defaultlist.csv")
        self.log.info("_oaccessorialList :" + str(_oaccessorialList))
        _daccessorialList = self.csvRowDataToList("destinationaccessorials", "defaultlist.csv")
        self.log.info("_saccessorialList :" + str(_daccessorialList))
        ssaccBasedOnSectionResult = self.validateAccessorialsBasedOnSections(self._acc_specialservices,
                                                                             _ssaccessorialList)
        oaccBasedOnSectionResult = self.validateAccessorialsBasedOnSections(self._acc_origin, _oaccessorialList)
        daccBasedOnSectionResult = self.validateAccessorialsBasedOnSections(self._acc_destination, _daccessorialList)

        if ssaccBasedOnSectionResult and oaccBasedOnSectionResult and daccBasedOnSectionResult:
            return True
        else:
            return False

    def selectedAccessorials(self, checkboxStatus):
        """
        The method is to return what all accessorial's checkboxes are checked.
        :param checkboxStatus: True/False
        :return: If parameter checkboxstatus is True then return Selected Accessorial List other return un selected
        Accessorial list.
        """
        self.log.info("Inside selectedAccessorials Method")
        selectedAccessorialList = []
        unselectedAccessorialList = []
        _allAccessorialList = self.csvRowDataToList("allaccessorials", "defaultlist.csv")
        for accessorialfromlist in _allAccessorialList:
            accCheckboxStatus = self.validateAccessorialCheckbox(accessorialfromlist)
            if accCheckboxStatus:
                selectedAccessorialList.append(accessorialfromlist)
            else:
                unselectedAccessorialList.append(accessorialfromlist)

        self.log.info("The list contains accessorial's whose checkboxes are checked : " + str(selectedAccessorialList))
        self.log.info(
            "The list contains accessorial's whose checkboxes are unchecked  : " + str(unselectedAccessorialList))
        if checkboxStatus:
            return selectedAccessorialList
        else:
            return unselectedAccessorialList

    def validateAllAccessorialsEditable(self):
        """
        The method will validate all the accessorials are editable or not.
        Step1: It will check whether accessorial checkbox is in un-checked state.
        Step2: Click on the accessorial checkbox.
        Step3: It will check whether accessorial checkbox is in checked state.
        Step4: Click on the accessorial checkbox.
        Step5: It will check whether accessorial checkbox is in un-checked state.
        Step5: Validate the accessorial checkbox status
        :return: If all the accessorials are editable it will return True.
        """
        self.log.info("Inside validateAllAccessorialsEditable Method")
        checkboxStatus = True
        _allAccessorialList = self.csvRowDataToList("allaccessorials", "defaultlist.csv")
        for accessorialfromlist in _allAccessorialList:
            accCheckboxStatus0 = self.validateAccessorialCheckbox(accessorialfromlist)
            self.clickingAccessorialsCheckbox(accessorialfromlist)
            accCheckboxStatus1 = self.validateAccessorialCheckbox(accessorialfromlist)
            self.clickingAccessorialsCheckbox(accessorialfromlist)
            accCheckboxStatus2 = self.validateAccessorialCheckbox(accessorialfromlist)
            checkboxStatus = checkboxStatus and not accCheckboxStatus0 and accCheckboxStatus1 and not accCheckboxStatus2
            self.log.info("Accessorial from the list " + str(accessorialfromlist))

        self.log.info("Checkbox final status " + str(checkboxStatus))
        return checkboxStatus

    def clickingAccessorialsCheckbox(self, accessorialcheckbox):
        """
        Based on the accessorial passed,the method will click the checkbox of the respective accessorial.
        :param accessorialcheckbox: Accessorial Name
        :return: None
        """
        self.log.info("Inside clickingAccessorialsCheckbox method " + str(accessorialcheckbox))
        try:
            if "protectfromfreeze".lower() == accessorialcheckbox.lower():
                self.elementClick(self._ssacc_protectfromfreeze, locatorType="xpath")
            elif "sortandsegregate".lower() == accessorialcheckbox.lower():
                self.elementClick(self._ssacc_sortandsegregate, locatorType="xpath")
            elif "originappointment".lower() == accessorialcheckbox.lower():
                self.elementClick(self._oacc_appointment, locatorType="xpath")
            elif "origininside".lower() == accessorialcheckbox.lower():
                self.elementClick(self._oacc_inside, locatorType="xpath")
            elif "originliftgate".lower() == accessorialcheckbox.lower():
                self.elementClick(self._oacc_liftgate, locatorType="xpath")
            elif "destinationnotifypriortoarrival".lower() == accessorialcheckbox.lower():
                self.elementClick(self._dacc_notifypriortoarrival, locatorType="xpath")
            elif "destinationappointment".lower() == accessorialcheckbox.lower():
                self.elementClick(self._dacc_appointment, locatorType="xpath")
            elif "destinationinside".lower() == accessorialcheckbox.lower():
                self.elementClick(self._dacc_inside, locatorType="xpath")
            elif "destinationliftgate".lower() == accessorialcheckbox.lower():
                self.elementClick(self._dacc_liftgate, locatorType="xpath")
            elif "destinationtradeshowdelivery".lower() == accessorialcheckbox.lower():
                self.elementClick(self._dacc_tradeshowdelivery, locatorType="xpath")
            else:
                self.log.error("Incorrect Accessorial passed " + str(accessorialcheckbox))

        except Exception as E:
            self.log.error(f"Passed locator is incorrect or element got updated for the origin\\destination "
                           f"accessorials checkboxes {str(accessorialcheckbox)} and exception : {str(Exception)}")
            self.log.error(f"Exception: {str(E)}")

    def selectAccessorial(self, accessorialValue, section):
        """
        This method is selecting accessorial based on passed section
        for passing section calling method can use get_key_for_value method available in util class
        :param accessorialValue: accessorial name want to select
        :param section: whether its available in origin, destination or special services
        :return:
        """
        self.log.info("Inside selectAccessorial method and data is " + str(accessorialValue) +
                      " and section is :" + str(section))
        try:
            if accessorialValue != "":
                preparedList = accessorialValue.lower().replace(" ", "").replace("\n", "").split(",")
                self.log.info("Prepared List : " + str(preparedList))
                if "ori" in section.lower():
                    self.log.info("Inside Origin condition")
                    for index in preparedList:
                        if index.strip() in ["sortandsegregate", "protectfromfreeze"]:
                            self.clickingAccessorialsCheckbox(index.lower().replace(" ", "").replace("\n", ""))
                        else:
                            self.clickingAccessorialsCheckbox(
                                "origin" + index.lower().replace(" ", "").replace("\n", ""))
                    return True
                elif "dest" in section.lower():
                    self.log.info("Inside destination condition")
                    for index in preparedList:
                        self.log.info("Inside destination Loop and index value is :" + str(index))
                        if index.strip() in ["sortandsegregate", "protectfromfreeze"]:
                            self.clickingAccessorialsCheckbox(index.lower().replace(" ", "").replace("\n", ""))
                        else:
                            self.clickingAccessorialsCheckbox(
                                "destination" + index.lower().replace(" ", "").replace("\n", ""))
                    return True
                else:
                    self.log.error("incorrect section name passed in selectAccessorial Method")
                    return False

            else:
                self.log.debug("No accessorials passed, hence returning True")
                return True

        except Exception as E:
            self.log.error("Some exception throw in selectAccessorial Method")
            self.log.error("Exception : " + str(E))

    def validateAccessorialCheckbox(self, accessorialcheckbox):
        """
        Based on the accessorial passed,the method will check whether respective accessorials checkbox is checked
        or not.
        :param accessorialcheckbox: Accessorial Name
        :return: Return True if the checkbox is checked otherwise False
        """
        value = ""
        self.log.info("Inside validateAccessorialCheckbox method " + str(accessorialcheckbox))
        try:
            accValueList = False
            if "protectfromfreeze".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._ssacc_protectfromfreeze + self._acc_getAccValue,
                                                     locatorType="xpath")
                # value = self.getAttribute(self._ssacc_protectfromfreeze, locatorType="xpath",
                #                           attributeType="class")
            elif "sortandsegregate".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._ssacc_sortandsegregate + self._acc_getAccValue,
                                                     locatorType="xpath")
                # value = self.getAttribute(self._ssacc_sortandsegregate, locatorType="xpath",
                #                           attributeType="class")
            elif "originappointment".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._oacc_appointment + self._acc_getAccValue,
                                                     locatorType="xpath")
                # value = self.getAttribute(self._oacc_appointment, locatorType="xpath",
                #                           attributeType="class")
            elif "origininside".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._oacc_inside + self._acc_getAccValue, locatorType="xpath")
                # value = self.getAttribute(self._oacc_inside, locatorType="xpath",
                #                           attributeType="class")
            elif "originliftgate".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._oacc_liftgate + self._acc_getAccValue, locatorType="xpath")
                # value = self.getAttribute(self._oacc_liftgate, locatorType="xpath",
                #                           attributeType="class")
            elif "destinationnotifypriortoarrival".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._dacc_notifypriortoarrival + self._acc_getAccValue,
                                                     locatorType="xpath")
                # value = self.getAttribute(self._dacc_notifypriortoarrival, locatorType="xpath",
                #                           attributeType="class")
            elif "destinationappointment".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._dacc_appointment + self._acc_getAccValue,
                                                     locatorType="xpath")
                # value = self.getAttribute(self._dacc_appointment, locatorType="xpath",
                #                           attributeType="class")
            elif "destinationinside".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._dacc_inside + self._acc_getAccValue, locatorType="xpath")
                # value = self.getAttribute(self._dacc_inside, locatorType="xpath",
                #                           attributeType="class")
            elif "destinationliftgate".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._dacc_liftgate + self._acc_getAccValue, locatorType="xpath")
                # value = self.getAttribute(self._dacc_liftgate, locatorType="xpath",
                #                           attributeType="class")
            elif "destinationtradeshowdelivery".lower() == accessorialcheckbox.lower():
                accValueList = self.isElementPresent(self._dacc_tradeshowdelivery + self._acc_getAccValue,
                                                     locatorType="xpath")
                # value = self.getAttribute(self._dacc_tradeshowdelivery, locatorType="xpath",
                #                           attributeType="class")
            else:
                self.log.error("Incorrect Accessorial passed " + str(accessorialcheckbox))

            self.log.info("acc Value : " + str(accValueList))
            if accValueList:
                self.log.info("Accessorial Checkbox " + accessorialcheckbox + " is checked")
                self.log.info(True)
                return True
            else:
                self.log.info("Accessorial Checkbox " + accessorialcheckbox + " is unchecked")
                self.log.info(False)
                return False
        except Exception as E:
            self.log.error(f"Passed locator is incorrect or element got updated for the origin\\destination "
                           f"accessorials {str(accessorialcheckbox)} Exception is : {str(E)}")

    #  ---------------------------------------Notes Section Method----------------------------------------------------

    def verifyNotesValidation(self):
        """
        Verify the Notes Validation Message as Passed
        :return: True if Match else False
        """
        try:
            self.log.info("Inside verifyNotesValidation Method")
            ErrorMessage = self.getText(self._note_error_msg, "xpath")
            return self.validators.verify_text_match(ErrorMessage, self._note_msg)
        except Exception as e:
            self.log.error(
                "Method verifyNotesValidation : Passed locators is wrong/updated and the Exception : " + str(e))

    # def getSelectedNoteType(self):
    #     """
    #     Get selected Note Type
    #     :return: Selected Note type
    #     """
    #     self.log.info("Inside getSelectedNoteType Method")
    #     try:
    #         return self.getText(self._note_type_get, "xpath")
    #     except Exception as e:
    #         self.log.error("Passed locator incorrect or element get updated")
    #         self.log.error("Exception : " + str(e))

    def defaultNoteValidation(self):
        """
        Validating the Default Note section
        :return: True if all as expected else False
        """
        try:
            self.log.info("Inside defaultNoteValidation Method")
            combinedResult = True
            # expectedList = self.csvRowDataToList("quotenotelist", "defaultlist.csv")
            # # For scroll towards element
            # self.pageVerticalScroll(self._note_type_icon, "xpath")
            #
            # defaultNoteTypeValue = self.getSelectedNoteType()
            # defaultNoteTypeResult = self.validators.verify_text_match(defaultNoteTypeValue, "User")
            # self.log.info("Validating default selected Note type : " + str(defaultNoteTypeResult))
            # combinedResult = combinedResult and defaultNoteTypeResult
            #
            # self.elementClick(self._note_type_icon, "xpath")
            # self.waitForElement(self._note_type_list, "xpath", "display")
            # elementList = self.getElementList(self._note_type_list, "xpath")
            # actualNoteList = []
            #
            # for i in elementList:
            #     self.log.info("Note type drop down Value : " + str(i.text))
            #     actualNoteList.append(i.text)
            #
            # self.log.info("Expected Note List : " + str(expectedList))
            # noteListMatchResult = self.validators.verify_list_match(expectedList, actualNoteList)
            # self.log.info("Note List Match: " + str(noteListMatchResult))
            # combinedResult = combinedResult and noteListMatchResult
            #
            # for j in range(1, len(actualNoteList)):
            #     self.log.info("selected Note Type : " + str(actualNoteList[j]))
            #     self.elementClick("(" + self._note_type_list + ")[" + str(j + 1) + "]", "xpath")
            #     textValue = self.getSelectedNoteType()
            #     combinedResult = combinedResult and self.validators.verify_text_match(textValue, actualNoteList[j])
            #     self.log.info("combined Result : " + str(combinedResult))
            #
            #     if not j == len(actualNoteList) - 1:
            #         self.elementClick(self._note_type_icon, "xpath")
            #         self.waitForElement(self._note_type_list, "xpath", "display")

            # Scrolling towards notes section.
            self.pageVerticalScroll(self._note_section, "xpath")
            combinedResult = combinedResult and self.validators.verify_text_match(
                self.getAttribute(self._note_text, "id", "placeholder"), self._note_defaulttextmsg)
            self.log.info("combined Result : " + str(combinedResult))
            self.log.info("Character Length : " + str(len(self._note_exceed_string)))
            self.sendKeys(self._note_exceed_string, self._note_text, "id")
            self.log.info("Notes String : " + str(len(self.getText(self._note_text, "id"))))
            if len(self.getText(self._note_text, "id")) == 500:
                self.log.info("Notes string length as expected.")
                combinedResult = combinedResult and True
            else:
                combinedResult = combinedResult and False

            self.log.info("Combined Final Result : " + str(combinedResult))
            return combinedResult
        except Exception as e:
            self.log.error(
                "Method defaultNoteValidation : Passed locators is wrong/updated and the Exception : " + str(e))
            return False

    def addAndCancelNote(self, ButtonName, passedNoteType="", content="", verify="yes"):
        """
        Method is responsible to Add and Cancel feature of Notes
        :param passedNoteType: the Note type want to select
        :param verify: whether we have to verify validation message or not
        :param ButtonName: submit or Cancel excepted
        :param content: string which we want to add a notes
        :return:True if all fine else False
        """
        self.log.info("Inside addAndCancelNote Method, Passed ButtonName : " + str(ButtonName) +
                      ", passed Note Type : " + str(passedNoteType) + ", Content :" + str(content) +
                      ", verify: " + str(verify))
        try:
            combinedResult = True
            # if not passedNoteType == "":
            #     noteTypeResult = self.selectNoteType(passedNoteType)
            #     combinedResult = combinedResult and noteTypeResult
            # selectedNoteType = self.getSelectedNoteType()
            if passedNoteType == "":
                selectedNoteType = "User"
            else:
                selectedNoteType = passedNoteType
            self.log.info("selectedNoteType : " + str(selectedNoteType))

            self.sendKeys(content, self._note_text, "id")
            if ButtonName.lower() == "submit":
                self.elementClick(self._note_save_button, "id")

                if verify == "yes" and self.isElementPresent(self._note_error_msg, "xpath"):
                    self.log.error("Error Message :" + str(self.getText(self._note_error_msg, "xpath")))
                    return False

                if (not self.isElementPresent(self._note_error_msg, "xpath")) and \
                        self.isElementPresent(self._saved_note_section, "xpath") and verify == "yes":
                    savedNoteType = self.validators.verify_text_match(self.getText(self._saved_note_type, "xpath"),
                                                                      selectedNoteType)
                    self.log.info("Compare Note Type in saved Note : " + str(savedNoteType))
                    print("addAndCancelNote method : Compare Note Type in saved Note : " + str(savedNoteType))
                    combinedResult = combinedResult and savedNoteType
                    savedNoteText = self.validators.verify_text_match(self.getText(self._saved_note_text, "xpath"),
                                                                      content)
                    self.log.info("Compare Note Text in saved Note : " + str(savedNoteText))
                    print("addAndCancelNote method : Compare Note Text in saved Note : " + str(savedNoteText))
                    combinedResult = combinedResult and savedNoteText
            elif ButtonName.lower() == "cancel":
                self.elementClick(self._note_cancel_button, "id")
                if verify == "yes" and self.isElementPresent(self._note_error_msg, "xpath"):
                    self.log.info("Error Message :" + str(self.getText(self._note_error_msg, "xpath")))
                    return False
            else:
                self.log.error("Incorrect Button Name Passed.")
                combinedResult = combinedResult and False
            self.log.info("combined Result from add cancel Note: " + str(combinedResult))
            print("addAndCancelNote method - combinedResult : " + str(combinedResult))
            return combinedResult

        except Exception as e:
            self.log.error("Method addAndCancelNote : Passed locators is wrong/updated and the Exception : " + str(e))

    def verifyNoteSection(self, passedNoteType="User", passedContent=""):
        """
        As per the passed Value it will Validate the Notes Section
        :param passedContent: The Note Text expecting in Notes Section
        :param passedNoteType: The Note type expected to select need to pass
        :return: True if all value as expected else false
        """
        self.log.info(
            "Inside verifyNoteSection Method and passedNoteType : " + str(passedNoteType) + ", passedContent : " + str(
                passedContent))
        try:
            noteText = self.validators.verify_text_match(self.getText(self._note_text, "id"), passedContent)
            # notetype = self.validators.verify_text_match(self.getSelectedNoteType(), "User")
            self.log.info("Note Text result: " + str(noteText))
            # combinedResult = noteText and notetype
            return noteText
        except Exception as e:
            self.log.error("Method verifyNoteSection : Passed locators is wrong/updated and the Exception : " + str(e))

    def totalCountOfNoteList(self):
        """
        Method to get the count of the saved notes from the notes list.
        :return: total count of the saved notes
        """
        try:
            elementList = self.getElementList(self._saved_sonote_section + "/child::div", locatorType="xpath")
            noOfNotesSaved = len(elementList)
            self.log.info("Total no. of notes saved in the Notes list : " + str(noOfNotesSaved))
            return noOfNotesSaved
        except Exception as E:
            self.log.error(
                "Method totalCountOfNoteList : Passed locator is wrong/got updated and the Exception is " + str(E))

    # Added method to validate notes section in quote page
    def validateSavedNotesInQuotePage(self, passedNoteType, passedContent):
        """
        The method is to validate notes saved.
        :param passedContent: The Note Text expecting in Notes Section
        :param passedNoteType: The Note type expected to present
        :return: True if value as expected else false
        """
        self.log.info("Inside validateSavedNotesInQuotePage Method and the passedNoteType is " + str(
            passedNoteType) + " and content is " + str(passedContent))
        try:
            notesSectionResult = False
            count = self.totalCountOfNoteList()
            for index in range(count):
                _savedNoteType = "//div[@id='" + self._saved_sonote_base + str(index) + "']" + self._saved_sonote_type
                getSavedNoteType = self.getText(_savedNoteType, "xpath")
                if passedNoteType.lower() == getSavedNoteType.lower():
                    _savedNoteText = "//div[@id='" + self._saved_sonote_base + str(
                        index) + "']" + self._saved_sonote_text
                    notesSectionResult = self.validators.verify_text_match(self.getText(_savedNoteText, "xpath"),
                                                                           passedContent)
                    if notesSectionResult:
                        break
            return notesSectionResult
        except Exception as E:
            self.log.error("Method validateSavedNotesInQuotePage : Passed locator is wrong/got updated and the "
                           "Exception is " + str(E))

    # def selectNoteType(self, noteType="User"):
    #     """
    #     Selecting the Note type as per the passed Value
    #     :param noteType: The Note type want to select
    #     :return: if as per passed value selected than True else False
    #     """
    #     self.log.info("Inside selectNoteType Method and Passed Note type : " + str(noteType))
    #     self.pageVerticalScroll(self._note_type_icon, "xpath")
    #     if not (self.getSelectedNoteType() == noteType):
    #         self.elementClick(self._note_type_icon, "xpath")
    #         self.waitForElement(self._note_type_list, "xpath", "display")
    #         self.selectBasedOnText(self._note_type_list, "xpath", noteType)
    #     return self.validators.verify_text_match(self.getSelectedNoteType(), noteType)

    # ------------------------------------Cost Section Start-------------------------------------------

    def dropDownListCompareCostSection(self, fieldName):
        """
        method to check the drop down vales in the Cost Only flow.
        """
        try:
            self.log.info("Inside dropDownListCompareCostSection and the passed field is " + str(fieldName))
            getListData = []
            list_data = self.csvRowDataToList(fieldName, "defaultlist.csv")
            self.elementClick(self._cost_currency, "id")
            elementList = self.getElementList(self._cost_currency_drop_list, "xpath")
            for i in elementList[1:]:
                textValue = i.text
                self.log.info("element Text : " + str(textValue))
                getListData.append(textValue.strip())
            self.log.info("Expected Dropdown List: " + str(list_data))
            self.log.info("Actual Dropdown List from the UI :" + str(getListData))
            return self.validators.verify_list_match(list_data, getListData)

        except Exception as e:
            self.log.error("Passed locator incorrect or element get updated, Method dropDownListCompareCostSection "
                           "called for field name : " + str(fieldName) + " and Exception is" + str(e))

    def addressContainerPresenceCheck(self):
        self.log.info("Inside addressContainerPresenceCheck method")
        try:
            if self.isElementDisplayed(self._middle_section_disabled_elements, "css"):
                return True
            else:
                return False
        except Exception as E:
            self.log.error("Exception occurred in addressContainerPresenceCheck Method and Exception is : " + str(E))

    # ----------- Edit Sales order details in create quote page methods ---------------------

    def createQuotePageLoad(self):
        self.log.info("Inside createQuotePageLoad Method")
        try:
            self.waitForElement(self._quote_page_spinner, "xpath",
                                event="display", timeout=30)
            self.waitForElement(self._quote_page_spinner, "xpath",
                                event="notdisplay", timeout=30)
            self.waitForElement(self._DestinationAddressContainerSection, "id", event="display", timeout=30)
            if self.isElementDisplayed(self._DestinationAddressContainerSection, "id"):
                return True
            else:
                self.log.error("Create Quote page didn't appeared with in time given")
                return False
        except Exception as E:
            self.log.error("Exception occurred in createQuotePageLoad Method and Exception is : " + str(E))

    def clickReturnToOrderButton(self, status):
        self.log.info("Inside clickReturnToOrderButton Method")
        try:
            self.waitForElement(self._return_order_button, "id", event="display", timeout=25)
            if self.isElementDisplayed(self._return_order_button, "id"):
                self.elementClick(self._return_order_button, "id")
                self.waitForElement(self._return_order_modal, "id")
                if self.isElementDisplayed(self._return_order_modal, "id"):
                    if status.lower() == "yes":
                        self.elementClick(self._return_order_yes_button, "xpath")
                    else:
                        self.elementClick(self._return_order_no_button, "xpath")
        except Exception as E:
            self.log.error("Exception occurred in clickReturnToOrderButton Method and Exception is : " + str(E))

    def verifyUpdateOrderButtonEnabled(self):
        """
        Validate Update Order Button is enabled or not in footer
        :return: if enabled than true else false
        """
        return self.wait_for_element_clickable(locator=(By.ID, self._updateorder_button)) is not None

    def updateSalesOrderDetails(self):
        """ Under this method, clicking update order button and returning true if so page is displayed else return
        false """
        try:
            self.log.info("Inside updateSalesOrderDetails Method")
            self.elementClick(self._updateorder_button, "id")
            self.waitForElement(self._updatemodal, "id", timeout=25)
            if self.isElementDisplayed(self._updatemodal, "id"):
                self.elementClick(self._updatemodal_confirmbutton, "id")
                return True
            else:
                self.log.debug("Update order modal is not displayed")
                return False

        except Exception as E:
            self.log.error("Exception occurred in updateSalesOrderDetails Method and Exception is : " + str(E))

    def createQuotePagePresenceCheck(self):
        self.log.info("Inside createQuotePagePresenceCheck method")
        try:
            if self.isElementDisplayed(self._return_order_button, "id"):
                return True
            else:
                self.log.debug("Return button is not displayed an clicking no button")
                return False
        except Exception as E:
            self.log.error("Exception occurred in createQuotePagePresenceCheck Method and Exception is : " + str(E))

    @property
    def acc_destination(self):
        return self._acc_destination

    @property
    def create_quote_base_container(self):
        return self._create_quote_base_container

    @property
    def quote_page_spinner(self):
        return self._quote_page_spinner

    @property
    def acc_origin(self):
        return self._acc_origin
