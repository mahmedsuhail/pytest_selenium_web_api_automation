import random

from selenium.webdriver.common.by import By

from pages.basepage import BasePage


class ShippingItemsPage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.driver = driver
        self.log = log

    # Locators
    # -----------------------------------Shipping Items Locators--------------------------------
    _si_hazmat_grp_text = "//label[contains(text(),'Hazmat Group')]"
    _si_hazmat_class = "//label[contains(text(),'Hazmat Class')]"
    _si_unit_type = "//label[contains(text(),'Handling Unit Type')]"
    _si_weight_unit = "//label[contains(text(),'Weight Units')]"
    _si_class = "//label[contains(text(),'Class')]"
    _si_dim = "//label[contains(text(),'Dim Units')]"
    _si_prefix = "//label[contains(text(),'Prefix')]"

    _si_commodities_field = "itemEditor_LTL1-276_inputField_1"
    _si_piece_field = "itemEditor_LTL1-276_inputField_2"
    _si_weight_field = "itemEditor_LTL1-276_inputField_3"

    # _si_weight_drop_icon = _si_weight_unit + "/following-sibling::div //div[contains(@class,'css-1wy')]"
    # _si_weight_drop_icon = _si_weight_unit + "/following-sibling::div //div[contains(@class,'css-16ii')]"
    _si_weight_drop_icon = "itemEditor_LTL1-276_select_1"

    _si_weight_drop_list = "//div[@id='itemEditor_LTL1-276_select_1'] //div[contains(@class,'-menu')] //div"
    # _si_get_weight_units = "//input[@id='itemEditor_LTL1-276_select_1']/preceding-sibling::div"
    # _si_get_weight_units = _si_weight_unit + "/following-sibling::div //div[contains(@class,'css-16ii')]"
    _si_get_weight_units = "itemEditor_LTL1-276_select_1"

    # _si_unit_type_drop_icon = _si_unit_type + "/following-sibling::div //div[contains(@class,'css-1wy')]"
    # _si_unit_type_drop_icon = _si_unit_type + "/following-sibling::div //div[contains(@class,'css-16ii')]"
    _si_unit_type_drop_icon = "itemEditor_LTL1-276_select_2"

    _si_unit_type_drop_list = "//div[@id='itemEditor_LTL1-276_select_2'] //div[contains(@class,'-menu')] //div"

    # _si_get_unit_type = "//input[@id='itemEditor_LTL1-276_select_2']/preceding-sibling::div"
    # _si_get_unit_type = _si_unit_type + "/following-sibling::div //div[contains(@class,'css-16ii')]"
    _si_get_unit_type = "itemEditor_LTL1-276_select_2"

    # _si_class_drop_icon = _si_class + "/following-sibling::div //div[contains(@class,'css-1wy')]"
    # _si_class_drop_icon = _si_class + "/following-sibling::div //div[contains(@class,'css-16ii')]"
    # _si_get_class = "//input[@id='react-select-11-input']/preceding-sibling::div"
    # _si_get_class = "//input[@id='itemEditor_LTL1-276_select_3']/preceding-sibling::div"
    # _si_get_class = _si_class + "/following-sibling::div //div[contains(@class,'css-16ii')]"

    _si_class_drop_icon = "itemEditor_LTL1-276_select_3"
    _si_class_drop_list = "//div[@id='itemEditor_LTL1-276_select_3'] //div[contains(@class,'-menu')] //div"
    _si_get_class = "itemEditor_LTL1-276_select_3"

    _si_nmfc_field = "itemEditor_LTL1-276_inputField_4"
    _si_density_value = "itemEditor_LTL1-276_densityInput"
    _si_huc_field = "itemEditor_LTL1-276_inputField_5"
    _si_length_field = "itemEditor_LTL1-276_inputField_6"
    _si_width_field = "itemEditor_LTL1-276_inputField_7"
    _si_height_field = "itemEditor_LTL1-276_inputField_8"

    # _si_dim_drop_icon = _si_dim + "/following-sibling:: div //div[contains(@class,'css-1wy')]"
    # _si_dim_drop_icon = _si_dim + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_dim_drop_icon = "itemEditor_LTL1-276_select_4"

    # _si_dim_drop_list = _si_dim + "/following-sibling:: div //div[contains(@class,'-menu')] //div"
    _si_dim_drop_list = "//div[@id='itemEditor_LTL1-276_select_4'] //div[contains(@class,'-menu')] //div"

    # _si_get_dim = "//input[@id='react-select-12-input']/preceding-sibling::div"
    # _si_get_dim = "//input[@id='itemEditor_LTL1-276_select_4']/preceding-sibling::div"
    # _si_get_dim = _si_dim + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_get_dim = "itemEditor_LTL1-276_select_4"

    _si_stackable_check = "//input[@name='Stackable']/following-sibling::div"
    _si_stackable_check_list = "//input[@name='Stackable']/following-sibling::div/child::*"
    _si_hazmat_check = "//input[@name='Hazmat']/following-sibling::div"
    _si_hazmat_check_list = "//input[@name='Hazmat']/following-sibling::div/child::*"
    _si_linear_feet_text = ".item-editorstyles__Quote-CreateQuote__sc-32pugl-2.jyKegK"
    _si_clear_button = "//span[text()='Clear']/parent::button"
    _si_Add_to_order_button = "shippingItems_LTL1-276_button_2"
    _si_save_and_confirm_button = "//span[text()='Save & Confirm']/parent::button"
    _si_save_button = "shippingItems_LTL1-276_button_2"

    _si_hazmat_code_field = "itemEditor_LTL1-276_inputField_9"
    _si_chemical_name_field = "itemEditor_LTL1-276_inputField_10"
    _si_emercontact_no_field = "itemEditor_LTL1-276_inputField_11"

    # _si_hazmat_group_icon = _si_hazmat_grp_text + "/following-sibling:: div //div[contains(@class,'css-1wy')]"
    # _si_hazmat_group_icon = _si_hazmat_grp_text + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_hazmat_group_icon = "itemEditor_LTL1-276_select_5"

    # _si_hazmat_group_list = _si_hazmat_grp_text + "/following-sibling:: div //div[contains(@class,'-menu')] //div"
    _si_hazmat_group_list = "//div[@id='itemEditor_LTL1-276_select_5'] //div[contains(@class,'-menu')] //div"

    # _si_get_hazmat_group = "//input[@id='react-select-16-input']/preceding-sibling::div"
    # _si_get_hazmat_group = "//input[@id='itemEditor_LTL1-276_select_5']/preceding-sibling::div"
    # _si_get_hazmat_group = _si_hazmat_grp_text + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_get_hazmat_group = "itemEditor_LTL1-276_select_5"

    # _si_hazmat_class_icon = _si_hazmat_class + "/following-sibling:: div //div[contains(@class,'css-1wy')]"
    # _si_hazmat_class_icon = _si_hazmat_class + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_hazmat_class_icon = "itemEditor_LTL1-276_select_6"

    _si_hazmat_class_list = "//div[@id= 'itemEditor_LTL1-276_select_6'] //div[contains(@class,'-menu')] //div"

    # _si_get_hazmat_class = "//input[@id='react-select-17-input']/preceding-sibling::div"
    # _si_get_hazmat_class = "//input[@id='itemEditor_LTL1-276_select_6']/preceding-sibling::div"
    # _si_get_hazmat_class = _si_hazmat_class + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_get_hazmat_class = "itemEditor_LTL1-276_select_6"

    # _si_hazmat_prefix_icon = _si_prefix + "/following-sibling:: div //div[contains(@class,'css-1wy')]"
    # _si_hazmat_prefix_icon = _si_prefix + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_hazmat_prefix_icon = "itemEditor_LTL1-276_select_7"

    _si_hazmat_prefix_list = "//div[@id='itemEditor_LTL1-276_select_7'] //div[contains(@class,'-menu')] //div"

    # _si_get_hazmat_prefix = "//input[@id='react-select-18-input']/preceding-sibling::div"
    # _si_get_hazmat_prefix = "//input[@id='itemEditor_LTL1-276_select_7']/preceding-sibling::div"
    # _si_get_hazmat_prefix = _si_prefix + "/following-sibling:: div //div[contains(@class,'css-16ii')]"
    _si_get_hazmat_prefix = "itemEditor_LTL1-276_select_7"

    _si_row_list = "//div[@id='itemEditor_LTL1-276_wrapper']/div"

    # --------------------------SO Items Locators--------------------------------------------------
    _summarized_item_section = "div[id='CreateQuote-right-container'] div[class*='iPckzq']"
    # _summarized_item_section = "div[class*='appstyle__Aside'] div[class*='cpVmJf']"
    # _itemized_details_list = "//div[contains(@class,'sales-order-itemsstyle__TextWrapper')]"
    _itemized_details_list = "//div[contains(@class,'SalesOrderItemsstyle__TextWrapper')]"
    _salesOrder_item_summary = "//div[@id = 'salesOrder_LTL1-277_summaryWrapper']"
    _restricted_link = "//div[contains(@class,'sales-orderstyle__RestrictedText')]/a"
    _total_shipment_value = "salesOrder_LTL1-277_inputField"
    _volume_base_locator = _salesOrder_item_summary + "/div[1]"
    _weight_base_locator = _salesOrder_item_summary + "/div[3]"
    _density_base_locator = _salesOrder_item_summary + "/div[2]"
    _linear_feet_base_locator = _salesOrder_item_summary + "/div[4]"

    _salesorder_edit_button = "//button[contains(@id,'salesOrder_LTL1-2820_items_editButton')]"
    _salesorder_delete_button = "//button[contains(@id,'salesOrder_LTL1-2820_items_deleteButton')]"
    _shippingitem_cancelbutton = "shippingItems_LTL1-276_button_1"

    # ----------------------------------Add Product ----------------------------------------------------------------

    product_book_icon = "//input[@id = 'itemEditor_LTL1-276_inputField_1']/following-sibling::div"
    add_product_button = "div[id = 'productBook_LTL1-276_productBookContainer'] div[class*='AddOrderButton']"
    product_book_container = "div[id ='productBook_LTL1-276_productBookContainer'] div[class*='AddProductContainer']"
    product_commodity = "productBook_LTL1-276_inputField_2"
    product_nfmc = "productBook_LTL1-276_inputField_3"

    # product_class = "//input[@id='productBook_LTL1-276_inputField_3']/ancestor::div[2]/following-sibling::div " \
    #                 "//div[contains(@class,'css-16iim7i')]"
    product_class = "productBook_LTL1-276_select_1"
    product_class_list = "//div[@id='productBook_LTL1-276_select_1'] //div[contains(@class,'-menu')] //div"
    product_unit = "productBook_LTL1-276_inputField_4"
    product_piece = "productBook_LTL1-276_inputField_5"

    # product_unitType = "//input[@id='productBook_LTL1-276_inputField_4']/ancestor::div[2]/preceding-sibling::div " \
    #                    "//div[contains(@class,'css-16iim7i')]"
    product_unitType = "productBook_LTL1-276_select_2"
    product_unitType_list = "//div[@id='productBook_LTL1-276_select_2'] //div[contains(@class,'-menu')] //div"
    product_weight = "productBook_LTL1-276_inputField_6"

    # product_weight_units = "//input[@id='productBook_LTL1-276_inputField_6']/ancestor::div[2]/following-sibling::div
    # //div[contains(@class,'css-16iim7i')]"
    product_weight_unit = "productBook_LTL1-276_select_3"
    product_weight_unit_list = "//div[@id='productBook_LTL1-276_select_3'] //div[contains(@class,'-menu')] //div"
    product_length = "productBook_LTL1-276_inputField_7"
    product_width = "productBook_LTL1-276_inputField_8"
    product_height = "productBook_LTL1-276_inputField_9"

    # product_dim = "//input[@id='productBook_LTL1-276_inputField_9']/ancestor::div[2]/following-sibling::div " \
    #               "//div[contains(@class,'css-16iim7i')]"
    product_dim = "productBook_LTL1-276_select_4"
    product_dim_list = "//div[@id='productBook_LTL1-276_select_4'] //div[contains(@class,'-menu')] //div"

    product_stackable = "//input[@id='productBook_LTL1-276_checkbox_1']/following-sibling::div"
    product_stackable_check = "//input[@id='productBook_LTL1-276_checkbox_1']/following-sibling::div/child::*"
    product_hazmat = "//input[@id='productBook_LTL1-276_checkbox_2']/following-sibling::div"
    product_hazmat_check = "//input[@id='productBook_LTL1-276_checkbox_2']/following-sibling::div/child::*"
    product_cancel = "//span[text()='Cancel']/parent::button[@id='productBook_LTL1-276_button_1']"
    product_save = "(//button[@id='productBook_LTL1-276_button_1'])[2]"

    # product_popup = "//div[@id='productBook_LTL1-276_productBookContainer']"
    product_popup = "productBook_LTL1-276_productBookContainer"
    product_close = "//div[text()='Product Book']/following-sibling::div"

    # product_haz_group = \
    #     product_popup + " //label[text()='Hazmat Group']/following-sibling::div //div[contains(@class,'css-16iim7i')]"
    product_haz_group = "productBook_LTL1-276_select_5"
    product_haz_group_list = "//div[@id='productBook_LTL1-276_select_5'] //div[contains(@class,'-menu')] //div"

    # product_haz_class = \
    #     product_popup + " //label[text()='Hazmat Class']/following-sibling::div //div[contains(@class,'css-16iim7i')]"
    product_haz_class = "productBook_LTL1-276_select_6"
    product_haz_class_list = "//div[@id='productBook_LTL1-276_select_6'] //div[contains(@class,'-menu')] //div"

    # product_prefix = \
    #     product_popup + "//label[text()='Prefix']/following-sibling::div //div[contains(@class,'css-16iim7i')]"
    product_hazmat_prefix = "productBook_LTL1-276_select_7"
    product_hazmat_prefix_list = "//div[@id= 'productBook_LTL1-276_select_7'] //div[contains(@class,'-menu')] //div"
    product_haz_code = "productBook_LTL1-276_inputField_10"
    product_haz_chemical_name = "productBook_LTL1-276_inputField_11"
    product_haz_emergency_no = "productBook_LTL1-276_inputField_12"
    addproduct_spinner = "div[id ='productBook_LTL1-276_productBookContainer'] div[class*='SpinnerOverlay']"
    product_list = "div[id ='productBook_LTL1-276_productBookContainer'] div[class*='SearchProductContainer']"
    search_product = "productBook_LTL1-276_inputField_1"
    product_item_list = "div[id*='productBook_LTL1-276_commodityItem']"
    product_book_loader = "div[class*='product-bookstyles__Loader']"

    carrier_spinner = "//div[contains(@class,'spinnerstyle__SpinnerLoader')]"

    # Message
    _si_linear_feet_message = \
        "*For shipments larger than 12 linear ft. a spot quote is required. Please call for an accurate Quote."

    # ---------------------------------Shipping Items section method------------------------------------

    def si_EnterTextOnField(self, fieldName, value):
        """
        In shipping items section for text field entered passed value.
        :param fieldName: for the field want to enter the value.
        :param value: the want to enter.
        """
        self.log.info("Entering Text on shipping Items Field")
        try:
            if "commodity".lower() == fieldName.lower():
                self.log.info("commodity entered")
                self.sendKeys(value, self._si_commodities_field, locatorType="id")
            elif "Piece".lower() == fieldName.lower():
                self.log.info("Piece entered")
                self.sendKeys(value, self._si_piece_field, locatorType="id")
            elif "weight".lower() == fieldName.lower():
                self.log.info("Weight entered")
                self.sendKeys(value, self._si_weight_field, locatorType="id")
            elif "nfmc".lower() == fieldName.lower():
                self.log.info("NFMC entered ")
                self.sendKeys(value, self._si_nmfc_field, locatorType="id")
            elif "unitcount".lower() == fieldName.lower():
                self.log.info("Unit count entered")
                self.sendKeys(value, self._si_huc_field, locatorType="id")
            elif "length".lower() == fieldName.lower():
                self.log.info("Length entered")
                self.sendKeys(value, self._si_length_field, locatorType="id")
            elif "width".lower() == fieldName.lower():
                self.log.info("Width entered")
                self.sendKeys(value, self._si_width_field, locatorType="id")
            elif "height".lower() == fieldName.lower():
                self.log.info("height entered")
                self.sendKeys(value, self._si_height_field, locatorType="id")
            elif "HazmatCode".lower() == fieldName.lower():
                self.log.info("Hazmat code enter")
                if not self.si_VerifySelectedCheckBox("hazmat"):
                    self.si_selectCheckBox("hazmat")
                self.sendKeys(value, self._si_hazmat_code_field, locatorType="id")
            elif "ChemicalName".lower() == fieldName.lower():
                self.log.info("Chemical Name Entered")
                if not self.si_VerifySelectedCheckBox("hazmat"):
                    self.si_selectCheckBox("hazmat")
                self.sendKeys(value, self._si_chemical_name_field, locatorType="id")
            elif "emergencyContact".lower() == fieldName.lower():
                self.log.info("Emergency Contact Number entered")
                if not self.si_VerifySelectedCheckBox("hazmat"):
                    self.si_selectCheckBox("hazmat")
                self.sendKeys(value, self._si_emercontact_no_field, locatorType="id")
            else:
                self.log.info(
                    "In si_EnterTextOnField method incorrect field name passed, field Name : " + str(fieldName))
        except Exception as E:
            self.log.error("In si_EnterTextOnField method Either passed Locator and locator Type incorrect or element "
                           "got updated for Field Name :" + str(fieldName))
            self.log.error(f"Exception: {str(E)}")

    def getShippingItemsData(self, fieldName):
        """
        Getting the selected or entered value
        :param fieldName: the field Name
        :return: FieldValue
        """
        self.log.info("Inside getShippingItemsData Method, Passed field Name is : " + str(fieldName))
        try:
            if "commodity".lower() == fieldName.lower():
                return self.getAttribute(self._si_commodities_field, "id", "value")
            elif "Piece".lower() == fieldName.lower():
                return self.getAttribute(self._si_piece_field, "id", "value")
            elif "weight".lower() == fieldName.lower():
                return self.getAttribute(self._si_weight_field, "id", "value")
            elif "nfmc".lower() == fieldName.lower():
                return self.getAttribute(self._si_nmfc_field, "id", "value")
            elif "unitcount".lower() == fieldName.lower():
                return self.getAttribute(self._si_huc_field, "id", "value")
            elif "length".lower() == fieldName.lower():
                return self.getAttribute(self._si_length_field, "id", "value")
            elif "width".lower() == fieldName.lower():
                return self.getAttribute(self._si_width_field, "id", "value")
            elif "height".lower() == fieldName.lower():
                return self.getAttribute(self._si_height_field, "id", "value")
            elif "HazmatCode".lower() == fieldName.lower():
                return self.getAttribute(self._si_hazmat_code_field, "id", "value")
            elif "ChemicalName".lower() == fieldName.lower():
                return self.getAttribute(self._si_chemical_name_field, "id", "value")
            elif "emergencyContact".lower() == fieldName.lower():
                return self.getAttribute(self._si_emercontact_no_field, "id", "value").replace("(", "").\
                    replace(")", "").replace("-", "").replace("x", "")
            elif "UnitType".lower() == fieldName.lower():
                return self.getText(self._si_get_unit_type, "id")
            elif "className".lower() == fieldName.lower():
                return self.getText(self._si_get_class, "id")
            elif "weightUnits".lower() == fieldName.lower():
                return self.getText(self._si_get_weight_units, "id")
            elif "Dim".lower() == fieldName.lower():
                return self.getText(self._si_get_dim, "id")
            elif "HazmatGroup".lower() == fieldName.lower():
                return self.getText(self._si_get_hazmat_group, "id")
            elif "HazmatClass".lower() == fieldName.lower():
                return self.getText(self._si_get_hazmat_class, "id")
            elif "prefix".lower() == fieldName.lower():
                return self.getText(self._si_get_hazmat_prefix, "id")
            else:
                self.log.info(
                    "In getShippingItemsData Method incorrect field Name passed. Passed field Name is :" + str(
                        fieldName))
        except Exception as E:
            self.log.error("In getShippingItemsData Method Either passed Locator and locator Type incorrect or "
                           "element got updated for fieldName : " + str(fieldName))
            self.log.error(f"Exception: {str(E)}")

    def si_verifyTextOnField(self, fieldName, ExpectedValue="", productBook="no"):
        """
        Verify value in text field and compare with expected value.
        :param productBook:
        :param fieldName: for the field want to validate
        :param ExpectedValue: expected value
        :return: if matched than True else False
        """
        self.log.info("Getting Text from shipping Items Field")
        if fieldName.lower() in ["commodity", "piece", "weight", "nfmc", "unitcount", "height", "hazmatcode",
                                 "chemicalname", "emergencycontact"]:
            actualText = self.getShippingItemsData(fieldName)
            return self.validators.verify_text_match(actualText, ExpectedValue)
        elif fieldName.lower() == "length" and productBook.lower() == "yes":
            actualText = self.getShippingItemsData(fieldName)
            if self.getText(self._si_get_unit_type, "id") == "Pallets(40x48)":
                ExpectedValue = "40"
            return self.validators.verify_text_match(actualText, ExpectedValue)
        elif fieldName.lower() == "length" and productBook.lower() == "no":
            actualText = self.getShippingItemsData(fieldName)
            if self.getText(self._si_get_unit_type, "id") == "Pallets(40x48)":
                ExpectedValue = "40"
            return self.validators.verify_text_match(actualText, ExpectedValue)
        elif fieldName.lower() == "width" and productBook.lower() == "yes":
            actualText = self.getShippingItemsData(fieldName)
            if self.getText(self._si_get_unit_type, "id") == "Pallets(40x48)":
                ExpectedValue = "48"
            return self.validators.verify_text_match(actualText, ExpectedValue)
        elif fieldName.lower() == "width" and productBook.lower() == "no":
            actualText = self.getShippingItemsData(fieldName)
            if self.getText(self._si_get_unit_type, "id") == "Pallets(40x48)":
                ExpectedValue = "48"
            return self.validators.verify_text_match(actualText, ExpectedValue)
        else:
            self.log.info(
                "In si_verifyTextOnField Passed field Name incorrect, Please check. Passed Field Name is : " + str(
                    fieldName))

    def si_SelectFromDropDownByName(self, fieldName, selectValue):
        """
        In shipping items section for drop down field select the element based on passed value
        :param fieldName: for field want to perform
        :param selectValue: the value want to select in drop down
        :return: true if it selected same value else false
        """
        self.log.info("Shipping Item section selecting value from drop down based on passed Name")
        try:
            if "UnitType".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_unit_type_drop_icon, "id")
                self.elementClick(self._si_unit_type_drop_icon, "id")
                self.waitForElement(self._si_unit_type_drop_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_unit_type_drop_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            elif "className".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_class_drop_icon, "id")
                self.elementClick(self._si_class_drop_icon, "id")
                self.waitForElement(self._si_class_drop_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_class_drop_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            elif "weightUnits".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_weight_drop_icon, "id")
                self.elementClick(self._si_weight_drop_icon, "id")
                self.waitForElement(self._si_weight_drop_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_weight_drop_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            elif "Dim".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_dim_drop_icon, "id")
                self.elementClick(self._si_dim_drop_icon, "id")
                self.waitForElement(self._si_dim_drop_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_dim_drop_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            elif "HazmatGroup".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_hazmat_group_icon, "id")
                self.elementClick(self._si_hazmat_group_icon, "id")
                self.waitForElement(self._si_hazmat_group_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_hazmat_group_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            elif "HazmatClass".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_hazmat_class_icon, "id")
                self.elementClick(self._si_hazmat_class_icon, "id")
                self.waitForElement(self._si_hazmat_class_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_hazmat_class_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            elif "prefix".lower() == fieldName.lower():
                self.pageVerticalScroll(self._si_hazmat_prefix_icon, "id")
                self.elementClick(self._si_hazmat_prefix_icon, "id")
                self.waitForElement(self._si_hazmat_prefix_list, "xpath")
                selectedName = self.selectBasedOnText(self._si_hazmat_prefix_list, "xpath", selectValue)
                self.log.info("Name returned : " + str(selectedName))
                return selectedName.lower() == selectValue.lower()
            else:
                self.log.info(
                    "Incorrect Field Name passed for 'si_SelectFromDropDownByName' method for field Name :" + str(
                        fieldName))
        except Exception as E:
            self.log.error("In si_SelectFromDropDownByName Method Passed locator either updated or incorrect for"
                           " field Name : " + str(fieldName))
            self.log.error(f"Exception: {str(E)}")

    def si_verifyDDValue(self, fieldName, ExpectedValue=""):
        """
        Validating based on selected text and expected text is matched or not for Drop down field
        :param fieldName: for the field want to validate in shipping items
        :param ExpectedValue: expected text
        :return: if matched than True else False
        """
        self.log.info("Shipping Item section Validating the value selected in Drop Down")
        if fieldName.lower() in ["unittype", "classname", "weightunits", "dim", "hazmatgroup", "hazmatclass",
                                 "prefix"]:
            selectedValue = self.getShippingItemsData(fieldName)
            self.log.info("selectedValue : " + str(selectedValue.lower()))
            self.log.info("ExpectedValue : " + ExpectedValue.lower())
            return selectedValue.lower() == ExpectedValue.lower()
        else:
            self.log.info(
                "Incorrect Field Name passed in 'si_verifyDDValue' method, Field Name is : " + str(fieldName) +
                "for si_verifyDDValue Method")

    def si_selectCheckBox(self, fieldName, checked="Yes"):
        """
        select the check box in shipping item section based on passed field name.
        :param checked: if want to check than Yes else No, Here checked "Yes" means want to check else uncheck
        :param fieldName the want to select.
        :param fieldName: the field want to select.
        """
        self.log.info("Check/Uncheck the CheckBox in shipping items section. passed field Name : " + str(fieldName) +
                      " and checked Value is :" + str(checked))
        try:
            if checked.lower() == "yes":
                if not self.si_VerifySelectedCheckBox(fieldName):
                    if "stackable".lower() == fieldName.lower():
                        self.log.info("Stackable Checkbox checked")
                        self.pageVerticalScroll(self._si_stackable_check, "xpath")
                        self.elementClick(self._si_stackable_check, "xpath")
                    elif "hazmat".lower() == fieldName.lower():
                        self.log.info("hazmat Checkbox checked")
                        self.pageVerticalScroll(self._si_hazmat_check, "xpath")
                        self.elementClick(self._si_hazmat_check, "xpath")
                    else:
                        self.log.info(
                            "Incorrect Field Name passed, Field Name is : " + str(fieldName) + "for si_selectCheckBox "
                                                                                               "Method")
                else:
                    self.log.info("Checkbox already checked for field Name :" + str(fieldName))
            else:
                if self.si_VerifySelectedCheckBox(fieldName):
                    if "stackable".lower() == fieldName.lower():
                        self.log.info("Stackable Checkbox unchecked")
                        self.pageVerticalScroll(self._si_stackable_check, "xpath")
                        self.elementClick(self._si_stackable_check, "xpath")
                    elif "hazmat".lower() == fieldName.lower():
                        self.log.info("hazmat Checkbox unchecked")
                        self.pageVerticalScroll(self._si_hazmat_check, "xpath")
                        self.elementClick(self._si_hazmat_check, "xpath")
                    else:
                        self.log.info(
                            "Incorrect Field Name passed, Field Name is : " + str(fieldName) + "for si_selectCheckBox "
                                                                                               "Method")
                else:
                    self.log.info("Checkbox already Unchecked for field Name :" + str(fieldName))
        except Exception as E:
            self.log.error(
                "Passed locator incorrect or element got updated with respect to field name : " + str(fieldName) +
                " for si_selectCheckBox method")
            self.log.error(f"Exception: {str(E)}")

    def si_VerifySelectedCheckBox(self, fieldName, checked="Yes"):
        """
        Method will return based on the checkbox check or uncheck and checked Value, if checked value is Yes than as per
        the selection it will return else reverse will return
        :param checked: if expectation is chcekbox is checked than pass Yes else No
        :param fieldName: fieldName - the field want to validate
        :return: True if check box selected else False
        """
        self.log.info(
            "Inside verify selected Check Box Method, for field Name : " + str(fieldName) + " ,checked val is:" +
            str(checked))
        try:
            if "stackable".lower() == fieldName.lower():
                elList = self.getElementList(self._si_stackable_check_list, "xpath")
                if len(elList) > 0:
                    return True
                else:
                    return False
            elif "hazmat".lower() == fieldName.lower():
                elList = self.getElementList(self._si_hazmat_check_list, "xpath")
                if len(elList) > 0:
                    hazmatCheck = True
                else:
                    hazmatCheck = False

                visibleHzText = self.is_element_present(locator=(By.XPATH, self._si_hazmat_grp_text))

                siRowCount = self.getElementList(self._si_row_list, "xpath")
                self.log.info("count : " + str(len(siRowCount)))
                if len(siRowCount) > 3:
                    hazmatRow = True
                else:
                    hazmatRow = False

                self.log.info("Hazmat check box valid, hazmatCheck -" + str(hazmatCheck) + " ,hazmatRow-" + str(
                    hazmatRow) + ", visibleHzText-" + str(visibleHzText))
                if hazmatCheck and hazmatRow and visibleHzText:
                    return True
                else:
                    return False
            else:
                self.log.info("Incorrect Field Name passed, field name is : " + str(fieldName) +
                              "for si_VerifySelectedCheckBox Method")
        except Exception as E:
            self.log.error(
                "passed locator incorrect or element get updated with respect to field name : " + str(fieldName) +
                "for si_VerifySelectedCheckBox method")
            self.log.error(f"Exception: {str(E)}")

    def verifyLinearFeetMessage(self):
        """
        Validating the linear feet message which showing in shipping item section
        :return: True or False
        """
        try:
            linearfeetmsg = self.getText(self._si_linear_feet_text, "css")
            return self.validators.verify_text_match(linearfeetmsg, self._si_linear_feet_message)
        except Exception as E:
            self.log.error("passed locator incorrect or element get updated for verifyLinearFeetMessage Method.")
            self.log.error(f"Exception: {str(E)}")

    def clearShippingItemsButton(self):
        """
        Clicking Clear Button in Shipping Item section
        """
        try:
            self.log.info("Inside clearShippingItemButton Method")
            self.elementClick(self._si_clear_button, "xpath")
        except Exception as E:
            self.log.error("Passed locator either incorrect or updated for ClearShippingItemsButton Method.")
            self.log.error(f"Exception: {str(E)}")

    def verifyShippingItemsDefaultState(self):
        """
        validating default view of shipping items
        :return: True if all shipping items field in Default State else False.
        """
        try:
            self.log.info("Inside verifyShippingItemsDefaultState Method")
            commodity = self.si_verifyTextOnField("commodity")
            piece = self.si_verifyTextOnField("Piece")
            weight = self.si_verifyTextOnField("weight")
            NMFC = self.si_verifyTextOnField("nfmc")
            unitCount = self.si_verifyTextOnField("unitcount")
            length = self.si_verifyTextOnField("length", "40")
            width = self.si_verifyTextOnField("width", "48")
            height = self.si_verifyTextOnField("height")
            self.log.info("Shipping items text field return, commodity-" + str(commodity) + " ,piece-" + str(piece) +
                          " ,weight-" + str(weight) + " ,NMFC-" + str(NMFC) + ", unitCount-" + str(unitCount) +
                          " ,length-" + str(length) + " ,width-" + str(width) + " ,height-" + str(height))
            if commodity and piece and weight and NMFC and unitCount and length and width and height:
                siTextValue = True
            else:
                siTextValue = False

            Utype = self.si_verifyDDValue("UnitType", "Pallets(40x48)")
            cName = self.si_verifyDDValue("className", "Select Class")
            wgtUnit = self.si_verifyDDValue("weightUnits", "lbs")
            DimUnit = self.si_verifyDDValue("Dim", "Inches")
            self.log.info("Shipping items Drop Down Value, UnitType-" + str(Utype) + ",cName-" +
                          str(cName) + " ,wgtUnit-" + str(wgtUnit) + ",DimUnit-" + str(DimUnit))
            if Utype and cName and wgtUnit and DimUnit:
                siDropdown = True
            else:
                siDropdown = False
            stackable = self.si_VerifySelectedCheckBox("stackable")
            hazmat = self.si_VerifySelectedCheckBox("hazmat")

            self.log.info(
                "Validating the checkbox value should uncheck, stackable-" + str(not stackable) + " hazmat-" + str(
                    not hazmat))

            if not stackable and not hazmat:
                checkbox = True
            else:
                checkbox = False

            self.log.info("verifyDefaultShippingItemsField method, siTextValue-" + str(siTextValue) + " ,siDropdown-" +
                          str(siDropdown) + ",checkbox-" + str(checkbox))

            if siTextValue and siDropdown and checkbox:
                return True
            else:
                return False

        except Exception as e:
            self.log.error("Method verifyShippingItemsDefaultState : Verification of default state of the "
                           "shipping items section failed.")
            self.log.error("Exception" + str(e))

    def dropDownListCompare(self, fieldName):
        """
        Note : Here passed column name same as field name
        :param fieldName:
        :return: if for the field default list match than true else False
        """
        try:
            elementList = []
            list_data = self.csvRowDataToList(fieldName, "defaultlist.csv")
            if fieldName == "weightunits":
                self.pageVerticalScroll(self._si_weight_drop_icon, "id")
                self.elementClick(self._si_weight_drop_icon, "id")
                elementList = self.getElementList(self._si_weight_drop_list, "xpath")
            elif fieldName == "unittype":
                self.pageVerticalScroll(self._si_unit_type_drop_icon, "id")
                self.elementClick(self._si_unit_type_drop_icon, "id")
                elementList = self.getElementList(self._si_unit_type_drop_list, "xpath")
            elif fieldName == "dim":
                self.pageVerticalScroll(self._si_dim_drop_icon, "id")
                self.elementClick(self._si_dim_drop_icon, "id")
                elementList = self.getElementList(self._si_dim_drop_list, "xpath")
            elif fieldName == "classname":
                self.pageVerticalScroll(self._si_class_drop_icon, "id")
                self.elementClick(self._si_class_drop_icon, "id")
                elementList = self.getElementList(self._si_class_drop_list, "xpath")
            elif fieldName == "hazmatgroup":
                if not self.si_VerifySelectedCheckBox("hazmat"):
                    self.si_selectCheckBox("hazmat")
                self.pageVerticalScroll(self._si_hazmat_group_icon, "id")
                self.elementClick(self._si_hazmat_group_icon, "id")
                elementList = self.getElementList(self._si_hazmat_group_list, "xpath")
            elif fieldName == "hazmatclass":
                if not self.si_VerifySelectedCheckBox("hazmat"):
                    self.si_selectCheckBox("hazmat")
                self.pageVerticalScroll(self._si_hazmat_class_icon, "id")
                self.elementClick(self._si_hazmat_class_icon, "id")
                elementList = self.getElementList(self._si_hazmat_class_list, "xpath")
            elif fieldName == "prefix":
                if not self.si_VerifySelectedCheckBox("hazmat"):
                    self.si_selectCheckBox("hazmat")
                self.pageVerticalScroll(self._si_hazmat_prefix_icon, "id")
                self.elementClick(self._si_hazmat_prefix_icon, "id")
                elementList = self.getElementList(self._si_hazmat_prefix_list, "xpath")
            getListData = []
            for i in elementList[1:]:
                textValue = i.text
                self.log.info("element Text : " + str(textValue))
                getListData.append(textValue.strip())
            self.log.info("expected : " + str(list_data))
            self.log.info("Actual :" + str(getListData))
            return self.validators.verify_list_match(list_data, getListData)
        except Exception as E:
            self.log.error("Passed locator incorrect or element get updated, Method called for field name "
                           ": " + str(fieldName))
            self.log.error(f"Exception: {str(E)}")

    def enterHazmatData(self, hzCode, chemName, emergNo, hzGrp, hzCls, prefix):
        """
            Enter Hazmat Data in Shipping Items
        """
        self.log.info("Entering Hazmat data to hazmat fields")
        if not self.si_VerifySelectedCheckBox("hazmat"):
            self.si_selectCheckBox("hazmat")
        self.si_EnterTextOnField("HazmatCode", hzCode)
        self.si_EnterTextOnField("ChemicalName", chemName)
        self.si_EnterTextOnField("emergencyContact", emergNo)
        self.si_SelectFromDropDownByName("HazmatGroup", hzGrp)
        self.si_SelectFromDropDownByName("HazmatClass", hzCls)
        if not prefix == "":
            self.si_SelectFromDropDownByName("prefix", prefix)

    def verifyHazmatData(self, hzCode, chemName, emergNo, hzGrp, hzCls, prefix):
        """
        Validate the Hazmat Data
        :return: if entered value is correct than True else False
        """
        self.log.info("validating Hazmat data to hazmat fields")
        validHzGrp = self.si_verifyDDValue("hazmatgroup", hzGrp)
        validHzCls = self.si_verifyDDValue("hazmatclass", hzCls)
        validPrefix = self.si_verifyDDValue("prefix", prefix)
        validHzCode = self.si_verifyTextOnField("hazmatcode", hzCode)
        validChemName = self.si_verifyTextOnField("ChemicalName", chemName)
        validEmergencyNo = self.si_verifyTextOnField("emergencycontact", emergNo)

        self.log.info("HazmatCode:" + str(validHzCode) + " ,ChemicalName" + str(validChemName) + " ,EmergencyContact" +
                      str(validEmergencyNo) + " ,hazmatgroup" + str(validHzGrp) + " ,hazmatclass" + str(validHzCls) +
                      " ,prefix" + str(validPrefix))
        if validHzCode and validChemName and validEmergencyNo and validHzCode and validHzCls and validPrefix:
            return True
        else:
            return False

    def entryShippingItemsData(self, commodity, piece, weight, unitCount, unitType, className, nfmc="",
                               length="", width="", height="", wtunits="", dim="", stackable="no", hazmat="no",
                               hazgrp="", hzclass="", prefix="", hzcode="", chemName="", emergNo=""):
        """
        enter passed shipping items data to the required field
        :return:
        """
        try:
            self.log.info("Inside entryShippingItemsData Method")
            # self.sendKeys(Keys.PAGE_DOWN, self._si_commodities_field, locatorType="id")
            if not commodity == "":
                self.si_EnterTextOnField("commodity", commodity)
            if not piece == "":
                self.si_EnterTextOnField("Piece", piece)
            if not weight == "":
                self.si_EnterTextOnField("weight", weight)
            if not unitType == "":
                self.waitForElement(self.carrier_spinner, "xpath", event="notdisplay", timeout=25)
                self.si_SelectFromDropDownByName("UnitType", unitType)
            if not unitCount == "":
                self.si_EnterTextOnField("unitcount", unitCount)
            if not nfmc == "":
                self.si_EnterTextOnField("nfmc", nfmc)
            if not length == "":
                self.si_EnterTextOnField("length", length)
            if not width == "":
                self.si_EnterTextOnField("width", width)
            if not height == "":
                self.si_EnterTextOnField("height", height)
            if not className == "":
                self.waitForElement(self.carrier_spinner, "xpath", event="notdisplay", timeout=25)
                self.si_SelectFromDropDownByName("className", className)
            if not wtunits == "":
                self.si_SelectFromDropDownByName("weightUnits", wtunits)
            if not dim == "":
                self.si_SelectFromDropDownByName("Dim", dim)
            self.si_selectCheckBox("stackable", stackable)
            self.si_selectCheckBox("hazmat", hazmat)
            if hazmat.lower() == "yes":
                self.enterHazmatData(hzcode, chemName, emergNo, hazgrp, hzclass, prefix)

            self.log.info(
                "Passed Value in entryShippingItemsData, commodity: " + str(commodity) + " ,piece:" + str(piece) +
                ",weight:" + str(weight) + ",unitCount: " + str(unitCount) + " ,nfmc: " + str(nfmc) +
                ",length: " + str(length) + ",width:" + str(width) + ",height:" + str(
                    height) + ",unitType:" + unitType +
                ",className:" + str(className) + ",wightUnits:" + str(wtunits) + ".Dim:" + str(dim))

        except Exception as E:
            self.log.error("Method entryShippingItemsData : ShippingItems entry in the respective fields got failed.")
            self.log.error("Exception : " + str(E))

    def verifyCombinedShippingItemData(self, commodity, piece, weight, unitCount, unitType, className, nfmc="",
                                       length="", width="", height="", wtunits="", dim="", stackable="no", hazmat="no",
                                       hzmcode="", chemName="", emergNo="", hzGrp="", hzclass="", prefix="",
                                       productBook="no"):
        """
        verifying passed shipping item data
        :return: True if all value matched with passed value else False
        """
        self.log.info("Inside verifyCombinedShippingItemData Method")
        try:
            commonFlag = True
            if not commodity == "":
                validcom = self.si_verifyTextOnField("commodity", str(commodity))
                commonFlag = commonFlag and validcom
                self.log.info("commodity comparision : " + str(commonFlag))
            if not piece == "":
                # validpiece = self.si_verifyTextOnField("piece", str(int(float(piece))))
                # validpiece = self.si_verifyTextOnField("piece", piece.replace(".", ""))
                validpiece = self.si_verifyTextOnField("piece", str(round(float(piece))))
                commonFlag = commonFlag and validpiece
                self.log.info("Pieces comparision : " + str(commonFlag))
            if not weight == "":
                # validwt = self.si_verifyTextOnField("weight", str(int(float(weight))))
                # validwt = self.si_verifyTextOnField("weight", weight.replace(".", ""))
                validwt = self.si_verifyTextOnField("weight", str(round(float(weight))))
                commonFlag = commonFlag and validwt
                self.log.info("Weight comparision : " + str(commonFlag))
            if not nfmc == "":
                # validnfmc = self.si_verifyTextOnField("nfmc", str(int(float(nfmc))))
                validnfmc = self.si_verifyTextOnField("nfmc", str(nfmc).replace(".", ""))
                commonFlag = commonFlag and validnfmc
                self.log.info("NFMC# comparision : " + str(commonFlag))
            if not unitCount == "":
                # validUnitCount = self.si_verifyTextOnField("unitcount", str(int(float(unitCount))))
                # validUnitCount = self.si_verifyTextOnField("unitcount", unitCount.replace(".", ""))
                validUnitCount = self.si_verifyTextOnField("unitcount", str(round(float(unitCount))))
                commonFlag = commonFlag and validUnitCount
                self.log.info("UnitCount comparision : " + str(commonFlag))
            if not unitType == "":
                validUnitType = self.si_verifyDDValue("UnitType", unitType)
                commonFlag = commonFlag and validUnitType
                self.log.info("UnitType comparision : " + str(commonFlag))
            if not length == "":
                validLength = self.si_verifyTextOnField("length", str(int(float(length))), productBook)
                # validLength = self.si_verifyTextOnField("length", length.replace(".", ""), productBook)
                commonFlag = commonFlag and validLength
                self.log.info("Length comparision : " + str(commonFlag))
            if not width == "":
                validWidth = self.si_verifyTextOnField("width", str(int(float(width))), productBook)
                # validWidth = self.si_verifyTextOnField("width", width.replace(".", ""), productBook)
                commonFlag = commonFlag and validWidth
                self.log.info("Width comparision : " + str(commonFlag))
            if not height == "":
                validHeight = self.si_verifyTextOnField("height", str(int(float(height))))
                # validHeight = self.si_verifyTextOnField("height", height.replace(".", ""))
                commonFlag = commonFlag and validHeight
                self.log.info("Height comparision : " + str(commonFlag))
            if not className == "":
                validClass = self.si_verifyDDValue("className", str(className))
                commonFlag = commonFlag and validClass
                self.log.info("ClassName comparision : " + str(commonFlag))
            if not wtunits == "":
                validWt = self.si_verifyDDValue("weightUnits", wtunits)
                commonFlag = commonFlag and validWt
                self.log.info("weightUnits comparision : " + str(commonFlag))
            if not dim == "":
                validDim = self.si_verifyDDValue("Dim", dim)
                commonFlag = commonFlag and validDim
                self.log.info("Dimension comparision : " + str(commonFlag))

            if stackable.lower() == "yes":
                stackbaleVal = self.si_VerifySelectedCheckBox("stackable")
                self.log.info("stackable value :" + str(stackbaleVal))
                commonFlag = commonFlag and stackbaleVal
            if hazmat.lower() == "yes":
                hazmatVal = self.si_VerifySelectedCheckBox("hazmat")
                self.log.info("Hazmat check Box : " + str(hazmatVal))
                commonFlag = commonFlag and hazmatVal

                if hazmatVal:
                    hazmatDataVal = self.verifyHazmatData(hzCode=hzmcode, chemName=chemName,
                                                          emergNo=emergNo, hzGrp=hzGrp,
                                                          hzCls=hzclass, prefix=prefix)
                    self.log.info("Hazmat Data Value : " + str(hazmatDataVal))
                    commonFlag = commonFlag and hazmatDataVal
                else:
                    self.log.error("Hazmat check box value not selected showing value : " + str(hazmatVal))
                    commonFlag = commonFlag and False

            self.log.info("commonFlag :" + str(commonFlag))
            return commonFlag
        except Exception as E:
            self.log.error("Passed Value incorrect or not acceptable as per required.")
            self.log.error("Exception : " + str(E))
            return False

    def clickAddToOrderButton(self):
        """
        Clicking Add to Order Button in shipping item section
        """
        try:
            self.log.info("Clicking Add to Order Button")
            self.pageVerticalScroll(self._si_Add_to_order_button, "id")
            self.elementClick(self._si_Add_to_order_button, "id")
            self.waitForElement(self._summarized_item_section, "css", "notdisplay")
        except Exception as E:
            self.log.error("Passed locator incorrect or element get updated")
            self.log.error("Exception : " + str(E))

    def verifyButtonEnabled(self, ButtonName):
        """
        Validating the Button in enabled or not
        :param ButtonName: locator of Button
        :return: if enabled than True else False
        """
        try:
            self.log.info("Inside verifyButtonEnabled Method, Passed Button Name is :" + str(ButtonName))
            if ButtonName == "AddToOrder":
                if "false" == self.getAttribute(self._si_Add_to_order_button, "id", "data-disabled"):
                    return True
                else:
                    return False
            else:
                self.log.error("Passed Button Name incorrect, Button Name :" + str(ButtonName))
        except Exception as E:
            self.log.error("Passed locator incorrect or element get updated, Button Name :" + str(ButtonName))
            self.log.error(f"Exception: {str(E)}")

    def verifyDefaultShippingItemData(self):
        """
        Validating Default value on field like drop down list all values
        and field accepted values
        :return: True if all field validation as expected else False
        """
        try:
            self.log.info("Inside verifyDefaultShippingItemData Method Required Field Validation")
            # Validating the Required fields in shipping item section
            requiredFlag = True
            self.clickAddToOrderButton()
            commodity = self.getAttribute(self._si_commodities_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(commodity, "Required")
            self.log.info("Commodity : " + str(requiredFlag))

            piece = self.getAttribute(self._si_piece_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(piece, "Required")
            self.log.info("Piece : " + str(requiredFlag))

            weight = self.getAttribute(self._si_weight_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(weight, "Required")
            self.log.info("weight : " + str(requiredFlag))

            nfmc = self.getAttribute(self._si_nmfc_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(nfmc, "Required")
            self.log.info("NFMC# : " + str(requiredFlag))

            unitcount = self.getAttribute(self._si_huc_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(unitcount, "Required")
            self.log.info("Unit Count : " + str(requiredFlag))

            classval = self.getText(self._si_get_class, "id")
            requiredFlag = requiredFlag and self.validators.verify_text_match(classval, "Required")
            self.log.info("Class Field : " + str(requiredFlag))

            heightBfore = self.getAttribute(self._si_height_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(heightBfore, "Enter Height")
            self.log.info("height before mandatory Field : " + str(requiredFlag))
            self.si_selectCheckBox("stackable", "yes")
            self.clickAddToOrderButton()
            heightAfter = self.getAttribute(self._si_height_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(heightAfter, "Required")
            self.log.info("height before mandatory Field : " + str(requiredFlag))

            # Default Value validation for Drop Down
            self.log.info("Inside verifyDefaultShippingItemData Method")
            wtUnitsDD = self.dropDownListCompare("weightunits")
            unitTypeDD = self.dropDownListCompare("unittype")
            dimDD = self.dropDownListCompare("dim")
            classNameDD = self.dropDownListCompare("classname")
            hazmatGroupDD = self.dropDownListCompare("hazmatgroup")
            hazmatClassDD = self.dropDownListCompare("hazmatclass")

            # field validation for unacceptable value(- ive scenario)
            self.si_EnterTextOnField("nfmc", "test")
            nfmc = self.si_verifyTextOnField("nfmc", "")
            self.si_EnterTextOnField("emergencyContact", "testf4")
            emergency_contact = \
                self.si_verifyTextOnField("emergencyContact", ''.join([i for i in "testf4" if i.isdigit()]))

            self.si_EnterTextOnField("length", "test")
            length = self.si_verifyTextOnField("length", "")
            self.si_EnterTextOnField("width", "test")
            width = self.si_verifyTextOnField("width", "")
            self.si_EnterTextOnField("height", "test")
            height = self.si_verifyTextOnField("height", "")
            self.si_EnterTextOnField("unitcount", "test")
            alphaunitcount = self.si_verifyTextOnField("unitcount", "")
            self.si_EnterTextOnField("Piece", "testing")
            alphaPieceCount = self.si_verifyTextOnField("Piece", "")
            dropdown = wtUnitsDD and unitTypeDD and dimDD and classNameDD and hazmatGroupDD and hazmatClassDD
            textfield1 = nfmc and height and length and width and height
            textfield2 = alphaunitcount and alphaPieceCount and emergency_contact
            textfield = textfield1 and textfield2
            self.log.info("dropdown : " + str(dropdown) + ",textfield : " + str(textfield) + ",Required Field :" +
                          str(requiredFlag))
            if dropdown and textfield and requiredFlag:
                return True
            else:
                return False
        except Exception as e:
            self.log.error(
                "Method verifyDefaultShippingItemData : Verification of default shipping items data got failed.")
            self.log.error("Exception" + str(e))

    def validate_error_message_in_shipping_items(self, field_name, place_holder_value):
        """
        Method to validate the place holder values of the fields in shipping items section.
        :param field_name: fields at different sections in shipping items section.
        :param place_holder_value: value to validate against the placeholder value from UI.
        :return: if passed value is same as of the placeholder value from UI then True else False
        """
        try:
            self.log.info("Inside validate_error_message_in_shipping_items Method and the field name is " +
                          str(field_name) + ", place holder value is  " + str(place_holder_value))
            get_place_holder_value = ""
            if field_name.lower().replace(" ", "") == "hazmatcode":
                get_place_holder_value = self.getAttribute(self._si_hazmat_code_field, "id", "placeholder")
            else:
                self.log.error("Incorrect field name passed.")

            if get_place_holder_value == place_holder_value:
                return True
            else:
                return False

        except Exception as E:
            self.log.error("Inside method validate_error_message_in_shipping_items and the exception is " + str(E))

    # ----------------------------------Add Product Method-------------------------------------------------------------

    def closeProductPopUp(self):
        """
        Closing the Product Popup
        :return: True if clicked success else False
        """
        try:
            if self.isElementDisplayed(self.product_popup, "id"):
                self.elementClick(self._si_piece_field, "id")
                self.elementClick(self.product_close, "xpath")
            return True
        except Exception as E:
            self.log.error("Passed Locator incorrect or Get updated")
            self.log.error("Exception : " + str(E))
            return False

    def productDropDownListCompare(self, fieldName):
        """
        Note : Here passed column name same as field name
        :param fieldName:
        :return: if for the field default list match than true else False
        """
        try:
            self.log.info("Inside productDropDownListCompare Method")
            elementList = []
            list_data = self.csvRowDataToList(fieldName, "defaultlist.csv")
            if fieldName == "weightunits":
                self.elementClick(self.product_weight_unit, "id")
                elementList = self.getElementList(self.product_weight_unit_list, "xpath")
            elif fieldName == "unittype":
                self.elementClick(self.product_unitType, "id")
                elementList = self.getElementList(self.product_unitType_list, "xpath")
            elif fieldName == "dim":
                self.pageVerticalScroll(self.product_dim, "id")
                self.elementClick(self.product_dim, "id")
                elementList = self.getElementList(self.product_dim_list, "xpath")
            elif fieldName == "classname":
                self.elementClick(self.product_class, "id")
                elementList = self.getElementList(self.product_class_list, "xpath")
            elif fieldName == "hazmatgroup":
                if not len(self.getElementList(self.product_hazmat_check, "xpath")) > 0:
                    self.elementClick(self.product_hazmat, "xpath")
                self.elementClick(self.product_haz_group, "id")
                elementList = self.getElementList(self.product_haz_group_list, "xpath")
            elif fieldName == "hazmatclass":
                if not len(self.getElementList(self.product_hazmat_check, "xpath")) > 0:
                    self.elementClick(self.product_hazmat, "xpath")
                self.elementClick(self.product_haz_class, "id")
                elementList = self.getElementList(self.product_haz_class_list, "xpath")
            elif fieldName == "prefix":
                if not len(self.getElementList(self.product_hazmat_check, "xpath")) > 0:
                    self.elementClick(self.product_hazmat, "xpath")
                self.elementClick(self.product_hazmat_prefix, "id")
                elementList = self.getElementList(self.product_hazmat_prefix_list, "xpath")
            getListData = []
            for i in elementList[1:]:
                textValue = i.text
                self.log.info("element Text : " + str(textValue))
                getListData.append(textValue.strip())
            self.log.info("expected : " + str(list_data))
            self.log.info("Actual :" + str(getListData))
            return self.validators.verify_list_match(list_data, getListData)
        except Exception as E:
            self.log.error("Passed locator incorrect or element get updated, Method called for field name "
                           ": " + str(fieldName))
            self.log.error("Exception Message : " + str(E))
            return False

    def searchAndSelectProduct(self, productName):
        """
        Search and Select product from List
        :param productName: Search product name
        :return: True If found else False
        """
        try:
            self.log.info("Inside searchAndSelectProduct Method")
            self.sendKeys(productName, self.search_product, "id")
            self.waitForElement(self.product_book_loader, "css")
            itemList = self.getElementList(self.product_item_list, "css")
            self.log.info("itemList : " + str(len(itemList)))

            if len(itemList) == 0:
                self.log.error("As per search value list not found")
                return False
            else:
                self.selectBasedOnValuePassed(self.product_item_list, "css", productName)
                self.waitForElement(self.product_popup, "id", "notdisplay")
                # self.log.info("Commodity Text :"+str(self.getAttribute(self._si_commodities_field, "id", "value")))
                return True
        except Exception as E:
            self.log.error("Passed Locator incorrect or element get Updated")
            self.log.error("Exception : " + str(E))
            return False

    def addNewProducts(self, commodity, nfmc, classval, unitType="", unitcount="", piece="", weight="", weightmom="",
                       length="", width="", height="", dimmom="", stackable="no", hazmat="no", hazgrp="", hzclass="",
                       prefix="", hzcode="", chemName="", emergNo=""):
        """
        While Adding new Product the field want to enter pass in method. In this method for unique commodity trying
        5 times if got unique than execute else failed
        :return: If product Added Successfully thaan True else False
        """
        try:
            commonFlag = True
            self.log.info("Inside addProducts Method")
            if not self.isElementDisplayed(self.product_popup, "id"):
                self.elementClick(self.product_book_icon, "xpath")
            self.elementClick(self.add_product_button, "css")
            self.waitForElement(self.product_book_container, "css", "display")

            self.sendKeys(commodity, self.product_commodity, "id")

            # self.sendKeys(int(float(nfmc)), self.product_nfmc, "id")
            self.sendKeys(nfmc.replace(".", ""), self.product_nfmc,
                          "id")  # Once NMFC issue will fix thn replace has to remove
            # Product Class
            # self.waitForElement(self.carrier_spinner, "xpath", "display")
            self.waitForElement(self.carrier_spinner, "xpath", event="notdisplay", timeout=15)
            self.elementClick(self.product_class, "id")
            self.waitForElement(self.product_class_list, "xpath")

            className = self.selectBasedOnText(self.product_class_list, "xpath", classval)
            self.log.info("class Name : " + str(className))
            self.log.info("class selected")

            if not unitType == "":
                self.log.info("Unit type Text : " + str(self.getText(self.product_unitType, "id")))
                self.elementClick(self.product_unitType, "id")
                self.waitForElement(self.product_unitType_list, "xpath", "display")
                self.selectBasedOnText(self.product_unitType_list, "xpath", unitType)
            if not unitcount == "":
                self.sendKeys(unitcount, self.product_unit, "id")
            if not piece == "":
                self.sendKeys(piece, self.product_piece, "id")
            if not weight == "":
                self.sendKeys(weight, self.product_weight, "id")
            if not weightmom == "":
                self.elementClick(self.product_weight_unit, "id")
                self.waitForElement(self.product_weight_unit_list, "xpath", "display")
                self.selectBasedOnText(self.product_weight_unit_list, "xpath", weightmom)
            if not length == "":
                self.sendKeys(length, self.product_length, "id")
            if not width == "":
                self.sendKeys(width, self.product_width, "id")
            if not height == "":
                self.sendKeys(height, self.product_height, "id")
            if not dimmom == "":
                self.elementClick(self.product_dim, "id")
                self.waitForElement(self.product_dim_list, "xpath", "display")
                self.selectBasedOnText(self.product_dim_list, "xpath", dimmom)
            if stackable.lower() == "yes":
                if not len(self.getElementList(self.product_stackable_check, "xpath")) > 0:
                    self.elementClick(self.product_stackable, "xpath")
                else:
                    if len(self.getElementList(self.product_stackable_check, "xpath")) > 0:
                        self.elementClick(self.product_stackable, "xpath")

            if hazmat.lower() == "yes":
                self.log.info("list count : " + str(len(self.getElementList(self.product_hazmat_check, "xpath"))))
                if not len(self.getElementList(self.product_hazmat_check, "xpath")) > 0:
                    self.elementClick(self.product_hazmat, "xpath")
                self.log.info("Inside Hazmat Loop and Hazgrp : " + str(hazgrp))
                if not hazgrp == "":
                    self.elementClick(self.product_haz_group, "id")
                    self.waitForElement(self.product_haz_group_list, "xpath", "display")
                    self.selectBasedOnText(self.product_haz_group_list, "xpath", hazgrp)
                if not hzclass == "":
                    self.elementClick(self.product_haz_class, "id")
                    self.waitForElement(self.product_haz_class_list, "xpath", "display")
                    self.selectBasedOnText(self.product_haz_class_list, "xpath", hzclass)
                if not prefix == "":
                    self.elementClick(self.product_hazmat_prefix, "id")
                    self.waitForElement(self.product_hazmat_prefix_list, "xpath", "display")
                    self.selectBasedOnText(self.product_hazmat_prefix_list, "xpath", prefix)
                if not hzcode == "":
                    self.sendKeys(hzcode, self.product_haz_code, "id")
                if not chemName == "":
                    self.sendKeys(chemName, self.product_haz_chemical_name, "id")
                if not emergNo == "":
                    self.sendKeys(emergNo, self.product_haz_emergency_no, "id")
            else:
                if len(self.getElementList(self.product_hazmat_check, "xpath")) > 0:
                    self.elementClick(self.product_hazmat, "xpath")

            commodityValue = self.getAttribute(self.product_commodity, "id", "value")
            # +++++==========================================================================
            self.pageVerticalScroll(self.product_save, "xpath")
            self.elementClick(self.product_save, "xpath")

            self.waitForElement(self.addproduct_spinner, "css", "notdisplay")
            i = 0
            while self.isElementDisplayed(self.product_save, "xpath"):
                self.log.info("inside while loop")
                newCommodity = commodity + str(random.randrange(99999))
                self.sendKeys(newCommodity, self.product_commodity, "id")
                commodityValue = self.getAttribute(self.product_commodity, "id", "value")
                # self.log.info("intermediate commodity Value : " + str(commodityValue))
                self.pageVerticalScroll(self.product_save, "xpath")
                self.elementClick(self.product_save, "xpath")
                self.waitForElement(self.addproduct_spinner, "css", "notdisplay")
                i += 1
                if i == 5:
                    self.log.error("Did not get Unique commodity Name")
                    commonFlag = commonFlag and False
                    break
            if self.isElementDisplayed(self.product_list, "css"):
                commonFlag = commonFlag and True
                self.log.info("True")
            else:
                commonFlag = commonFlag and False
                self.log.info("False")

            self.log.info("Final commodity Value : " + str(commodityValue))
            return commonFlag, commodityValue

        except Exception as E:
            self.log.error("Passed Locator incorrect or got updated in addNewProducts Method")
            self.log.error("Exception : " + str(E))
            return False

    def validateDefaultProductCommodities(self):
        """
        Adding Default list and validation in Add Product commodities
        :return: True if all validation get success else false
        """
        try:
            self.log.info("Inside validateDefaultProductCommodities Method")
            self.elementClick(self._si_commodities_field, "id")
            self.log.info("Element Display : " + str(self.isElementDisplayed(self.product_popup, "id")))
            self.log.info("Add Product Button : " + str(self.isElementDisplayed(self.add_product_button, "css")))
            if not self.isElementDisplayed(self.product_popup, "id"):
                self.elementClick(self.product_book_icon, "xpath")

            if self.isElementDisplayed(self.product_popup, "id"):
                self.log.info("Product Book popup Displaying")
                self.elementClick(self.add_product_button, "css")
                commonFlag = True
                if self.isElementDisplayed(self.product_book_container, "css"):
                    commonFlag = commonFlag and True
                    self.log.info("Product Book Container : " + str(commonFlag))
                    self.pageVerticalScroll(self.product_save, "xpath")
                    self.elementClick(self.product_save, "xpath")
                    commodity = self.validators.verify_text_match(
                        self.getAttribute(self.product_commodity, "id", "placeholder"),
                        "Required")
                    self.log.info("commodity required : " + str(commodity))
                    commonFlag = commonFlag and commodity

                    classvalue = self.validators.verify_text_match(self.getText(self.product_class, "id"), "Required")
                    self.log.info("Class required : " + str(classvalue))
                    commonFlag = commonFlag and classvalue

                    nfmc = self.validators.verify_text_match(self.getAttribute(self.product_nfmc, "id", "placeholder"),
                                                             "Required")
                    self.log.info("NFMC field required : " + str(nfmc))
                    commonFlag = commonFlag and nfmc

                    self.elementClick(self.product_hazmat, "xpath")
                    self.pageVerticalScroll(self.product_save, "xpath")
                    self.elementClick(self.product_save, "xpath")
                    hzgroup = self.validators.verify_text_match(self.getText(self.product_haz_group, "id"), "Required")
                    self.log.info("Hazmat Group field required : " + str(hzgroup))
                    commonFlag = commonFlag and hzgroup

                    hzcode = self.validators.verify_text_match(
                        self.getAttribute(self.product_haz_code, "id", "placeholder"),
                        "Required")
                    self.log.info("Hazmat Code field required : " + str(hzcode))
                    commonFlag = commonFlag and hzcode

                    # Drop Down List Validation
                    dimDD = self.productDropDownListCompare("dim")
                    self.log.info("Add Product Dimension Unit Compare result : " + str(dimDD))
                    commonFlag = commonFlag and dimDD
                    wtUnitsDD = self.productDropDownListCompare("weightunits")
                    self.log.info("Add Product Weight Unit Compare result : " + str(wtUnitsDD))
                    commonFlag = commonFlag and wtUnitsDD

                    unitTypeDD = self.productDropDownListCompare("unittype")
                    self.log.info("Add Product Unit Type Compare result : " + str(unitTypeDD))
                    commonFlag = commonFlag and unitTypeDD
                    classNameDD = self.productDropDownListCompare("classname")
                    self.log.info("Add Product Class List Compare result : " + str(classNameDD))
                    commonFlag = commonFlag and classNameDD
                    hazmatGroupDD = self.productDropDownListCompare("hazmatgroup")
                    self.log.info("Add Product Hazmat group list Compare result : " + str(hazmatGroupDD))
                    commonFlag = commonFlag and hazmatGroupDD
                    hazmatClassDD = self.productDropDownListCompare("hazmatclass")
                    self.log.info("Add Product Hazmat class list Compare result : " + str(hazmatClassDD))
                    commonFlag = commonFlag and hazmatClassDD
                    hazmatPrefix = self.productDropDownListCompare("prefix")
                    self.log.info("Add Product Hazmat prefix list Compare result : " + str(hazmatPrefix))
                    commonFlag = commonFlag and hazmatPrefix

                    return commonFlag
                else:
                    commonFlag = commonFlag and False
                    return commonFlag
        except Exception as E:
            self.log.error("Passed Locator incorrect or updated")
            self.log.error(E)
            return False

    # -------------------------------SO Item Details Section---------------------------------------------------------
    def verifyDisplaySOItems(self, olderItemCount):
        """
        Validating After click Add to Order Button SO Items summary should show.
        :return: True if visible else False
        """
        self.log.info("Inside verifyDisplaySOItems Method")
        try:
            self.waitForElement("(" + self._itemized_details_list + ")[" + str(int(olderItemCount) + 1) + "]", "xpath",
                                event="display")
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            if len(itemCount) > 0 and len(itemCount) == (olderItemCount + 1):
                return True
            else:
                self.log.debug("Sales order Items not showing in create quote page")
                return False
        except Exception as E:
            self.log.error("Passed Locator incorrect or element get updated")
            self.log.error(f"Exception: {str(E)}")

    def getSOItemCount(self):
        self.log.info("Inside getSOItemCount Method")
        try:
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            return len(itemCount)
        except Exception as E:
            self.log.error("Passed Locator incorrect or element get updated")
            self.log.error(f"Exception: {str(E)}")

    def validateSOEachItemData(self, ExpectedValueList):
        """
        Validating the SO each line Items data after added from Shipping items
        :param ExpectedValueList: List of Expected Value
        :return: True if matched else False
        """
        self.log.info("Inside validateSOItemData Method")
        try:
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            if len(itemCount) > 0:
                commonFlag = True
                self.log.info("(" + self._itemized_details_list + ")[" + str(len(itemCount)) + "]")
                BaseSOItemLocator = "(" + self._itemized_details_list + ")[" + str(len(itemCount)) + "]"

                # Commodity Section
                commodity = self.getText(BaseSOItemLocator + "/div[1]", "xpath")
                self.log.info("Commodity Value is :" + str(commodity))
                self.log.info("ExpectedCommodity :" + str(ExpectedValueList[0]))
                commonFlag = commonFlag and self.validators.verify_text_match(commodity, str(ExpectedValueList[0]))
                self.log.info("Commodity result : " + str(commonFlag))

                # Package Section
                package = self.getText(BaseSOItemLocator + "/div[2]", "xpath")
                self.log.info("Package Data :" + str(package))
                self.log.info("ExpectedPackage : " + str(ExpectedValueList[1]))
                commonFlag = commonFlag and self.validators.verify_text_match(package, ExpectedValueList[1])
                self.log.info("Package result : " + str(commonFlag))

                # Dimension Section
                dimension = self.getText(BaseSOItemLocator + "/div[3]", "xpath")
                self.log.info("Dimension Data :" + str(dimension))
                self.log.info("Expected Dimension : " + str(ExpectedValueList[2]))
                commonFlag = commonFlag and self.validators.verify_text_match(dimension, ExpectedValueList[2])
                self.log.info("dimension result : " + str(commonFlag))

                calculatedSectionBaseLocator = BaseSOItemLocator + "/div[4]"

                # Volume Section
                volume = self.getText(calculatedSectionBaseLocator + "/div[1]", "xpath")
                volume = volume.split("\n")[1]
                self.log.info("Actual Volume :" + str(volume))
                expectedVolume = "{:.2f}".format(ExpectedValueList[3])
                self.log.info("Expected Volume :" + str(expectedVolume))
                commonFlag = commonFlag and (volume == expectedVolume)
                self.log.info("Volume result : " + str(commonFlag))

                # Density Section
                density = self.getText(calculatedSectionBaseLocator + "/div[2]", "xpath")
                density = density.split("\n")[1]
                self.log.info("Actual density : " + str(density))
                expectedDensity = "{:.2f}".format(ExpectedValueList[4])
                self.log.info("Expected Density :" + str(expectedDensity))
                commonFlag = commonFlag and (density == expectedDensity)
                self.log.info("Density result : " + str(commonFlag))

                # Weight Section
                weight = self.getText(calculatedSectionBaseLocator + "/div[3]", "xpath")
                weight = weight.split("\n")[1]
                self.log.info("Actual weight : " + str(weight))
                expectedWeight = "{:.2f}".format(ExpectedValueList[5])
                self.log.info("Expected Weight : " + str(expectedWeight))
                commonFlag = commonFlag and (weight == expectedWeight)
                self.log.info("Weight result : " + str(commonFlag))

                return commonFlag
            else:
                self.log.error("No Element list found with locator, Item Details Locator.")
                return False
        except Exception as E:
            self.log.error("Passed Locator not correct or element get updated.")
            self.log.error(f"Exception: {str(E)}")

    def getEachShippingItemDataForSOItem(self):
        """
        Returning the Shipping items Data required for SO Item Validation
        :return: List of Values
        """
        self.log.info("Inside getEachShippingItemDataForSOItem Method")
        listOfData = []
        listOfData.append(self.getShippingItemsData("commodity"))

        listOfData.append(self.getShippingItemsData("unitcount") + " " + self.getShippingItemsData("UnitType"))
        listOfData.append(self.getShippingItemsData("length") + " x " + self.getShippingItemsData("width") + " x " +
                          self.getShippingItemsData("height"))
        listOfData.append(self.calculateVolume())
        listOfData.append(self.calculateDensity())
        listOfData.append(self.calculateWeight())
        listOfData.append(self.calculateLinearFeet())
        return listOfData

    def calculateDensity(self):
        """
        Calculating the Density Value Based on Shipping Data
        :return: Calculated Density Value
        """
        self.log.info("Inside calculateDensity Method")
        try:
            weight = self.getShippingItemsData("weight")
            length = self.getShippingItemsData("length")
            width = self.getShippingItemsData("width")
            height = self.getShippingItemsData("height")
            unitCount = self.getShippingItemsData("unitcount")
            dimUnit = self.getShippingItemsData("Dim")
            weightUnit = self.getShippingItemsData("weightUnits")
            self.log.info(
                "weight: " + str(weight) + ",length: " + str(length) + ",width: " + str(width) +
                ",height: " + str(height) + ",UnitCount: " + str(unitCount) + ",dimensionUnit: " + str(dimUnit) +
                ",wtUnit:" + str(weightUnit))

            # Getting Dimension Unit
            if dimUnit.lower() == "inches":
                dim = 1728
            elif dimUnit.lower() == "feet":
                dim = 1
            else:
                self.log.error("Correct dimension value not passed, passed dimension unit :" + str(dimUnit))
                dim = 0

            # Getting Weight Unit
            if weightUnit.lower() == "kg":
                wtUnit = 2.20462
            elif weightUnit.lower() == "lbs":
                wtUnit = 1
            else:
                self.log.error("Correct weight Unit value not passed, passed Weight unit :" + str(weightUnit))
                wtUnit = 0

            density = (int(weight) * float(wtUnit) * dim) / (int(length) * int(width) * int(height) * int(unitCount))
            self.log.info("From calculateDensity method for each line item Calculated density : " + str(density))
            return density
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for calculateDensity Method")
            self.log.error(f"Exception: {str(E)}")

    def calculateWeight(self):
        """
        Calculating the weight for SO Items in Lbs
        :return: Calculated Weight Value
        """
        self.log.info("Inside calculateWeight Method")
        try:
            weight = self.getShippingItemsData("weight")
            weightUnit = self.getShippingItemsData("weightUnits")
            self.log.info(
                "weight: " + str(weight) + ",wtUnit:" + str(weightUnit))
            if weightUnit.lower() == "kg":
                wtUnit = 2.20462
            elif weightUnit.lower() == "lbs":
                wtUnit = 1
            else:
                self.log.error("Correct weight Unit value not passed, passed Weight unit :" + str(weightUnit))
                wtUnit = 0

            TotalWeight = int(weight) * wtUnit
            self.log.info("From calculateWeight Method for each line items Calculated Weight   : " + str(TotalWeight))
            return TotalWeight
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for calculateWeight Method")
            self.log.error(f"Exception: {str(E)}")

    def calculateVolume(self):
        """
        Calculating Volume for SO Item based on shipping Data
        :return: Calculated Volume value
        """
        self.log.info("Inside calculateVolume Method")
        try:
            length = self.getShippingItemsData("length")
            width = self.getShippingItemsData("width")
            height = self.getShippingItemsData("height")
            unitCount = self.getShippingItemsData("unitcount")

            dimunit = self.getShippingItemsData("Dim")
            if dimunit.lower() == "inches":
                dim = 1728
            elif dimunit.lower() == "feet":
                dim = 1
            else:
                self.log.error("Correct dimension value not passed, passed dimension unit :" + str(dimunit))
                dim = 0

            volume = ((int(length) * int(width) * int(height)) / dim) * int(unitCount)
            self.log.info("from calculateVolume Method for each line item Calculated Volume  : " + str(volume))
            return volume
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for calculateVolume Method")
            self.log.error(f"Exception: {str(E)}")

    def calculateLinearFeet(self):
        self.log.info("Inside calculateLinearFeet Method")
        trailer = 1943
        control = 1.1
        factor = 28
        weightValue = self.getShippingItemsData("weight")
        weightUnit = self.getShippingItemsData("weightUnits")
        classValue = self.getShippingItemsData("className")

        self.log.info("Class Value in Calculate Linear Feet Method : " + str(classValue))
        self.log.info(type(classValue))
        weightPerCubicFeet = 0
        if classValue == "50":
            weightPerCubicFeet = 50
        elif classValue == "55":
            weightPerCubicFeet = 35
        elif classValue == "60":
            weightPerCubicFeet = 30
        elif classValue == "65":
            weightPerCubicFeet = 22.5
        elif classValue == "70":
            weightPerCubicFeet = 15
        elif classValue == "77":
            weightPerCubicFeet = 13.5
        elif classValue == "85":
            weightPerCubicFeet = 12
        elif classValue == "92":
            weightPerCubicFeet = 10.5
        elif classValue == "100":
            weightPerCubicFeet = 9
        elif classValue == "110":
            weightPerCubicFeet = 8
        elif classValue == "125":
            weightPerCubicFeet = 7
        elif classValue == "150":
            weightPerCubicFeet = 4.7
        elif classValue == "175":
            weightPerCubicFeet = 4.5
        elif classValue == "200":
            weightPerCubicFeet = 4
        elif classValue == "250":
            weightPerCubicFeet = 3
        elif classValue == "300":
            weightPerCubicFeet = 2
        elif classValue == "400":
            weightPerCubicFeet = 1
        elif classValue == "500":
            weightPerCubicFeet = 0.9

        if weightUnit.lower() == "kg":
            wtUnit = 2.20462
        elif weightUnit.lower() == "lbs":
            wtUnit = 1
        else:
            self.log.error("Correct weight Unit value not passed, passed Weight unit :" + str(weightUnit))
            wtUnit = 0
        self.log.info("In Calculate Linear Feet Method weight Unit Value is : " + str(wtUnit))

        cubicFeet = (int(weightValue) * wtUnit) / float(weightPerCubicFeet)
        linearFeet = (float(cubicFeet) / float(trailer)) * float(factor) * float(control)
        self.log.info("From calculateLinearFeet Method Calculated Linear Feet Value  : " + str(linearFeet))
        return linearFeet

    def calculateTotalSOItemValue(self, listOfData):
        """
        Calculate the aggregate Value (for volume, Density, Weight) based on the entered Data in Shipping items
        List of data getting from Method "getEachShippingItemDataForSOItem".
        :return: List of Values (Total Volume, Total Weight, Total Density)
        """
        self.log.info("Inside calculateTotalSOItemValue Method")
        try:
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            if len(itemCount) > 0:
                finalVolume = 0
                finalWeight = 0
                finalDensity = 0
                finalLinearFeet = 0
                self.log.info("Expected List : " + str(listOfData))
                if len(listOfData) == len(itemCount):
                    for j in listOfData:
                        self.log.info("from External Volume :" + str(j[3]))
                        self.log.info("from External Weight :" + str(j[5]))
                        self.log.info("from External Density :" + str(j[4]))
                        finalVolume = finalVolume + float(j[3])
                        finalWeight = finalWeight + float(j[5])
                        # finalDensity = finalDensity + float(j[4])
                        self.log.info("final Linear Feet : " + str(j[6]))
                        finalLinearFeet = finalLinearFeet + float(j[6])

                    finalDensity = (finalWeight / finalVolume)
                    finalDensity = "{:.2f}".format(finalDensity)
                    finalVolume = "{:.2f}".format(finalVolume)
                    finalWeight = "{:.2f}".format(finalWeight)
                    finalLinearFeet = "{:.2f}".format(finalLinearFeet)

                    self.log.info("Final final Data Volume:" + str(finalVolume) + ",density : " + str(finalDensity) +
                                  ", Weight:" + str(finalWeight) + ", Linear Feet : " + str(finalLinearFeet))
                else:
                    self.log.error("Passed list of Data and no of line items not matching in "
                                   "calculateTotalSOItemValue Method.")
                return finalVolume, finalWeight, finalDensity, finalLinearFeet
            else:
                self.log.error("Element list not found with passed locator in calculateTotalSOItemValue Method. "
                               "Locator :" + str(self._itemized_details_list))
        except Exception as e:
            self.log.error("Passed locator incorrect or updated the element for calculateTotalSOItemValue Method")
            self.log.error("Exception : " + str(e))

    def verifySOItemSummary(self, actualList):
        """
        Validating the SO Item total summary display and calculated Value
        :param actualList: list of calculated data which used for comparison get from calculateTotalSOItemValue method
        :return: True if all match else false
        """
        self.log.info("Inside verifySOItemSummary Method, and actualList : " + str(actualList))
        try:
            commonFlag = True

            volumeValue = self.getText(self._volume_base_locator + "/div[2]", "xpath")
            self.log.info("Expected total volume Value : " + str(volumeValue))
            self.log.info("Actual total volume Value : " + str(actualList[0]))

            commonFlag = commonFlag and (float(volumeValue) == float(actualList[0]))
            self.log.info(float(volumeValue) == float(actualList[0]))
            self.log.info("Total volume Value Comparision : " + str(commonFlag))

            weightValue = self.getText(self._weight_base_locator + "/div[2]", "xpath")
            self.log.info("Expected total weight Value : " + str(weightValue))
            self.log.info("Actual total Weight Value : " + str(actualList[1]))
            commonFlag = commonFlag and (float(weightValue) == float(actualList[1]))
            self.log.info(float(weightValue) == float(actualList[1]))
            self.log.info("Total Weight Value Comparision : " + str(commonFlag))

            densityValue = self.getText(self._density_base_locator + "/div[2]", "xpath")
            self.log.info("Expected total density Value : " + str(densityValue))
            self.log.info("Actual total Density Value : " + str(actualList[2]))
            commonFlag = commonFlag and (float(densityValue) == float(actualList[2]))
            self.log.info(float(densityValue) == float(actualList[2]))
            self.log.info("Total Density Value Comparision : " + str(commonFlag))

            # linearFeetValue = self.getText(self._linear_feet_base_locator + "/div[2]", "xpath")
            # self.log.info("Expected(from application) Estimated Linear Feet Value : " + str(linearFeetValue))
            # self.log.info("Actual Estimated Linear Feet Value : " + str(actualList[3]))
            # commonFlag = commonFlag and (float(linearFeetValue) == float(actualList[3]))
            # self.log.info((float(linearFeetValue) == float(actualList[3])))
            # self.log.info("Total Est. Linear feet Value comparision :" + str(commonFlag))

            volumeString = self.getText(self._volume_base_locator + "/div[1]", "xpath")
            commonFlag = commonFlag and self.validators.verify_text_match(volumeString, "Total Volume (cf ft)")
            self.log.info("Total Volume string : " + str(commonFlag))

            weightString = self.getText(self._weight_base_locator + "/div[1]", "xpath")
            commonFlag = commonFlag and self.validators.verify_text_match(weightString, "Total Weight (lbs)")
            self.log.info("Total Weight string : " + str(commonFlag))

            densityString = self.getText(self._density_base_locator + "/div[1]", "xpath")
            commonFlag = commonFlag and self.validators.verify_text_match(densityString, "Total Density")
            self.log.info("Total Density string : " + str(commonFlag))

            linearFeetString = self.getText(self._linear_feet_base_locator + "/div[1]", "xpath")
            commonFlag = commonFlag and self.validators.verify_text_match(linearFeetString,
                                                                          "Estimated Linear Feet (ft)")
            self.log.info("Estimated Linear Feet string : " + str(commonFlag))

            return commonFlag
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for verifySOItemSummary Method")
            self.log.error(f"Exception: {str(E)}")
            return False

    def validateLinearFeet(self):
        """
        Validating the Linear Feet, as we don't have formula so we are validating only 0 value should not be shown
        :return: True if match else false
        """
        try:
            linearFeetValue = self.getText(self._linear_feet_base_locator + "/div[2]", "xpath")
            self.log.info("Actual Value of Linear Feet (From application) : " + str(linearFeetValue))
            if "0.00" == linearFeetValue:
                self.log.debug("Linear Feet Value is showing as 0")
            else:
                self.log.debug("Display Linear Feet Value showing equal to 0 due to dimension more than 96 inches.")
            return True
        except Exception as E:
            self.log.error("Exception occurred in validateLinearFeet Method and exception is :" + str(E))
            return False

    def verifyDensityValue(self):
        """
        validating the Density Value
        :return: True if display in shipping items and calculated value match else False
        """
        try:
            self.log.info("inside verifyDensityValue Method")
            ActualDensity = "{:.2f}".format(self.calculateDensity())
            ActualDensity = float(ActualDensity)
            self.log.info("Actual Density Value : " + str(ActualDensity))
            ExpectedDensity = self.getAttribute(self._si_density_value, "id", "value")
            ExpectedDensity = float(ExpectedDensity)
            self.log.info("Expected density Value :" + str(ExpectedDensity))
            if ActualDensity == ExpectedDensity:
                self.log.info("Density value matched")
                return True
            else:
                self.log.info("Density value not matched")
                return False
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for verifyDensityValue Method")
            self.log.error(f"Exception: {str(E)}")

    def enterTotalShipmentValue(self, enterValue):
        """
        Entering Total Shipment Value
        :param enterValue: Value to be enter
        """
        try:
            self.log.info("Inside enterTotalShipmentValue Method and enter Value : " + str(enterValue))
            if not enterValue == "":
                self.sendKeys(enterValue, self._total_shipment_value, "id")
                self.elementClick(self._si_piece_field, "id")
                shipment_value = self.getAttribute(self._total_shipment_value, "id", attributeType="value")
                self.log.info("shipment value : " + str(shipment_value))
                if int(enterValue) < 100:
                    self.log.info("Inside less than condition")
                    verified_result = self.validators.verify_text_match(shipment_value, "100")
                else:
                    self.log.info("Inside else block")
                    verified_result = self.validators.verify_text_match(shipment_value, enterValue)
            else:
                self.log.info("passed value is empty so didn't enter in field")
                return False

            return verified_result
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for enterTotalShipmentValue Method "
                           "and Exception is :" + str(E))

    def verifyTotalShipmentValue(self, expectedValue):
        """
        comparing Total shipment Value
        :param expectedValue: expected Value
        :return: True if both value matched else False
        """
        self.log.info("Inside verifyTotalShipmentValue Method and passed expected Value : " + str(expectedValue))
        actualValue = self.getTotalShipmentValue()
        if int(actualValue) == int(expectedValue):
            return True
        else:
            self.log.error("Actual and expected Value not matching in verifyTotalShipmentValue Method.")
            return False

    def getTotalShipmentValue(self):
        """
        getting Total shipment Value
        :return: return the Value
        """
        try:
            self.log.info("Inside getTotalShipmentValue Method")
            shipmentValue = self.getAttribute(self._total_shipment_value, "id", "value")
            return shipmentValue
        except Exception as E:
            self.log.error("Passed locator incorrect or updated the element for getTotalShipmentValue Method")
            self.log.error(f"Exception: {str(E)}")

    def verifyProhibitedCommoditiesLink(self):
        """
        Validating the Prohibited Link Name and file Name.
        :return: True if correct else false
        """
        try:
            self.log.info("Inside verifyProhibitedCommoditiesLink Method")
            linkText = self.getText(self._restricted_link, "xpath")
            textResult = self.validators.verify_text_match(linkText, "Restricted/Prohibited Commodities")

            value = self.getAttribute(self._restricted_link, "xpath", "href")
            if "restrictedOrExcludedCommodities.pdf" in value:
                linkName = True
            else:
                linkName = False

            if textResult and linkName:
                return True
            else:
                return False
        except Exception as E:
            self.log.error("Passed locator incorrect or update, inside verifyProhibitedCommoditiesLink Method")
            self.log.error(str(E))

    def editShippingItem(self, index=1):
        """
        Click edit the Shipping items based on passed index
        """
        self.log.info("Inside editShippingItem Method")
        self.pageVerticalScroll("(" + self._salesorder_edit_button + ")[" + str(index) + "]", "xpath")
        self.elementClick("(" + self._salesorder_edit_button + ")[" + str(index) + "]", "xpath")
        self.waitForElement("(" + self._salesorder_edit_button + ")[" + str(index) + "]", "xpath", "notdisplay")

    def deleteShippingItem(self, index=1):
        """
        click delete the selected shipping items based on passed index
        :param index: which index value has to delete
        """
        try:
            self.log.info("Inside deleteShippingItem Method and index value is :" + str(index))
            self.pageVerticalScroll("(" + self._salesorder_delete_button + ")[" + str(index) + "]", "xpath")
            self.elementClick("(" + self._salesorder_delete_button + ")[" + str(index) + "]", "xpath")
        except Exception as e:
            self.log.error("Passed Locator not found or updated for getShippingItemIndexBasedOnCommodity Method")
            self.log.error(e)

    def clickButton(self, button):
        """
        Clicking Button in the Shipping Item section.
        """
        try:
            if button.lower() == "saveandconfirm":
                self.log.info("Clicking Save & Confirm Button")
                self.pageVerticalScroll(self._si_save_and_confirm_button, locatorType="xpath")
                self.elementClick(self._si_save_and_confirm_button, "xpath")
                self.waitForElement(self._summarized_item_section, "css", "notdisplay")
            elif button.lower() == "save":
                self.log.info("Clicking Save Button")
                self.pageVerticalScroll(self._si_save_button, locatorType="id")
                self.elementClick(self._si_save_button, "id")
                self.waitForElement(self._summarized_item_section, "css", "notdisplay")
            elif button.lower() == "addtoorder":
                self.log.info("Clicking Add to Order Button")
                self.elementClick(self._si_Add_to_order_button, "id")
                self.waitForElement(self._summarized_item_section, "css", "notdisplay")
            else:
                self.log.error("Incorrect Button passed " + str(button))
        except Exception as E:
            self.log.error("Passed button is incorrect or element get updated")
            self.log.error("Exception : " + str(E))

    def getShippingItemIndexBasedOnCommodity(self, commodityName):

        self.log.info("Inside getShippingItemIndexBasedOnCommodity Method")
        try:
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            if len(itemCount) > 0:
                for i in range(len(itemCount)):
                    self.log.info("(" + self._itemized_details_list + ")[" + str(i + 1) + "]" + "/div[1]")
                    if commodityName.lower() == self.getText(
                            "(" + self._itemized_details_list + ")[" + str(i + 1) + "]" + "/div[1]",
                            "xpath").lower():
                        return i + 1
            else:
                self.log.error(
                    "Element list not found with passed Locator in getShippingItemIndexBasedOnCommodity Method")
        except Exception as E:
            self.log.error("Passed Locator not found or updated for getShippingItemIndexBasedOnCommodity Method")
            self.log.error(str(E))

    def verifyDisplayItemByCommodity(self, commodityName):
        self.log.info("Inside verifyDisplayItemByCommodity Method, passed commodity :" + str(commodityName))
        try:
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            if len(itemCount) > 0:
                for i in range(len(itemCount)):
                    if commodityName.lower() == self.getText(
                            "(" + self._itemized_details_list + ")[" + str(i + 1) + "]" + "/div[1]", "xpath").lower():
                        return True
            else:
                self.log.error(
                    "Element list not found with passed Locator in getShippingItemIndexBasedOnCommodity Method")
                return False
        except Exception as E:
            self.log.error("Passed Locator not found or updated for getShippingItemIndexBasedOnCommodity Method")
            self.log.error(str(E))

        # ----------- Cost Only Flow ----------------------------------------

    def verifyDefaultShippingItemDataCostOnly(self):
        """
        Validating Default value on field like drop down list all values
        and field accepted values
        :return: True if all field validation as expected else False
        """
        try:
            self.log.info("Inside verifyDefaultShippingItemDataCostOnly Method Required Field Validation")
            # Validating the Required fields in shipping item section
            requiredFlag = True
            self.clickAddToOrderButton()
            commodity = self.getAttribute(self._si_commodities_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(commodity, "Enter description")
            self.log.info("Commodity : " + str(requiredFlag))

            piece = self.getAttribute(self._si_piece_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(piece, "Enter Piece Count")
            self.log.info("Piece : " + str(requiredFlag))

            weight = self.getAttribute(self._si_weight_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(weight, "Required")
            self.log.info("weight : " + str(requiredFlag))

            nfmc = self.getAttribute(self._si_nmfc_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(nfmc, "Enter NMFC#")
            self.log.info("NFMC# : " + str(requiredFlag))

            unitcount = self.getAttribute(self._si_huc_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(unitcount, "Required")
            self.log.info("Unit Count : " + str(requiredFlag))

            classval = self.getText(self._si_get_class, "id")
            requiredFlag = requiredFlag and self.validators.verify_text_match(classval, "Required")
            self.log.info("Class Field : " + str(requiredFlag))

            heightBfore = self.getAttribute(self._si_height_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(heightBfore, "Enter Height")
            self.log.info("height before mandatory Field : " + str(requiredFlag))
            self.si_selectCheckBox("stackable", "yes")
            self.clickAddToOrderButton()
            heightAfter = self.getAttribute(self._si_height_field, "id", "placeholder")
            requiredFlag = requiredFlag and self.validators.verify_text_match(heightAfter, "Enter Height")
            self.log.info("height after checking stackable Field : " + str(requiredFlag))

            # Default Value validation for Drop Down
            self.log.info("Inside verifyDefaultShippingItemData Method")
            wtUnitsDD = self.dropDownListCompare("weightunits")
            unitTypeDD = self.dropDownListCompare("unittype")
            dimDD = self.dropDownListCompare("dim")
            classNameDD = self.dropDownListCompare("classname")
            hazmatGroupDD = self.dropDownListCompare("hazmatgroup")
            hazmatClassDD = self.dropDownListCompare("hazmatclass")

            # field validation for unacceptable value(- ive scenario)
            self.si_EnterTextOnField("nfmc", "test")
            nfmc = self.si_verifyTextOnField("nfmc", "")
            self.si_EnterTextOnField("emergencyContact", "testf4")
            height = self.si_verifyTextOnField("emergencyContact", ''.join([i for i in "testf4" if i.isdigit()]))
            self.si_EnterTextOnField("length", "test")
            length = self.si_verifyTextOnField("length", "")
            self.si_EnterTextOnField("width", "test")
            width = self.si_verifyTextOnField("width", "")
            self.si_EnterTextOnField("height", "test")
            height = self.si_verifyTextOnField("height", "")
            self.si_EnterTextOnField("unitcount", "test")
            alphaunitcount = self.si_verifyTextOnField("unitcount", "")
            self.si_EnterTextOnField("Piece", "testing")
            alphaPieceCount = self.si_verifyTextOnField("Piece", "")
            dropdown = wtUnitsDD and unitTypeDD and dimDD and classNameDD and hazmatGroupDD and hazmatClassDD
            textfield = nfmc and height and length and width and height and alphaunitcount and alphaPieceCount

            productBookIconStatusResult = self.isElementDisplayed(self.product_book_icon, locatorType="id")
            if not productBookIconStatusResult:
                self.log.info("Product book is not present.")
                productBookIconStatusResult = True
            else:
                self.log.info("Product book is present.")
                productBookIconStatusResult = False

            self.log.info(
                "dropdown : " + str(dropdown) + ",textfield : " + str(textfield) +
                ",Required Field :" + str(requiredFlag) + ",productBookIconStatusResult :" + str(
                    productBookIconStatusResult))
            if dropdown and textfield and requiredFlag and productBookIconStatusResult:
                return True
            else:
                return False

        except Exception as E:
            self.log.error(
                "Method verifyDefaultShippingItemDataCostOnly : Passed locator incorrect or element get updated")
            self.log.error("Exception : " + str(E))

    # --------- Shipping Items validation methods added here -----------

    def clickShippingItemCancelButton(self):
        """ under this method, clicking cancel button when the shipping items are in edited mode """
        self.log.info("Inside clickCancelButton method")
        self.elementClick(self._shippingitem_cancelbutton, "id")

    def validateSOEditItemData(self, ExpectedValueList, commodity):
        """
        Validating the SO each line Items data after added from Shipping items
        :param commodity:
        :param ExpectedValueList: List of Expected Value
        :return: True if matched else False
        """
        self.log.info("Inside validateSOEditItemData Method")
        try:
            itemCount = self.getElementList(self._itemized_details_list, "xpath")
            self.log.info("Total element Count :" + str(len(itemCount)))
            if len(itemCount) > 0:
                for item in range(0, len(itemCount)):
                    if commodity.lower() == self.getText("(" + self._itemized_details_list + ")[" + str(
                            item + 1) + "]" + "//div[contains(@class, 'CommodityText')]", "xpath").lower():
                        self.log.info("Inside if ------ commodity found at index :" + str(item + 1))
                        commonFlag = True
                        self.log.info("(" + self._itemized_details_list + ")[" + str(item + 1) + "]")
                        BaseSOItemLocator = "(" + self._itemized_details_list + ")[" + str(item + 1) + "]"

                        # Commodity Section
                        commodity = self.getText(BaseSOItemLocator + "/div[1]", "xpath")
                        self.log.info("Commodity Value is :" + str(commodity))
                        self.log.info("ExpectedCommodity :" + str(ExpectedValueList[0]))
                        commonFlag = commonFlag and self.validators.verify_text_match(commodity,
                                                                                      str(ExpectedValueList[0]))
                        self.log.info("Commodity result : " + str(commonFlag))

                        # Package Section
                        package = self.getText(BaseSOItemLocator + "/div[2]", "xpath")
                        self.log.info("Package Data :" + str(package))
                        self.log.info("ExpectedPackage : " + str(ExpectedValueList[1]))
                        commonFlag = commonFlag and self.validators.verify_text_match(package, ExpectedValueList[1])
                        self.log.info("Package result : " + str(commonFlag))

                        # Dimension Section
                        dimension = self.getText(BaseSOItemLocator + "/div[3]", "xpath")
                        self.log.info("Dimension Data :" + str(dimension))
                        self.log.info("Expected Dimension : " + str(ExpectedValueList[2]))
                        commonFlag = commonFlag and self.validators.verify_text_match(dimension, ExpectedValueList[2])
                        self.log.info("dimension result : " + str(commonFlag))

                        calculatedSectionBaseLocator = BaseSOItemLocator + "/div[4]"

                        # Volume Section
                        volume = self.getText(calculatedSectionBaseLocator + "/div[1]", "xpath")
                        volume = volume.split("\n")[1]
                        self.log.info("Actual Volume :" + str(volume))
                        expectedVolume = "{:.2f}".format(ExpectedValueList[3])
                        self.log.info("Expected Volume :" + str(expectedVolume))
                        commonFlag = commonFlag and (volume == expectedVolume)
                        self.log.info("Volume result : " + str(commonFlag))

                        # Density Section
                        density = self.getText(calculatedSectionBaseLocator + "/div[2]", "xpath")
                        density = density.split("\n")[1]
                        self.log.info("Actual density : " + str(density))
                        expectedDensity = "{:.2f}".format(ExpectedValueList[4])
                        self.log.info("Expected Density :" + str(expectedDensity))
                        commonFlag = commonFlag and (density == expectedDensity)
                        self.log.info("Density result : " + str(commonFlag))

                        # Weight Section
                        weight = self.getText(calculatedSectionBaseLocator + "/div[3]", "xpath")
                        weight = weight.split("\n")[1]
                        self.log.info("Actual weight : " + str(weight))
                        expectedWeight = "{:.2f}".format(ExpectedValueList[5])
                        self.log.info("Expected Weight : " + str(expectedWeight))
                        commonFlag = commonFlag and (weight == expectedWeight)
                        self.log.info("Weight result : " + str(commonFlag))

                        return commonFlag
            else:
                self.log.error("No Element list found with locator, Item Details Locator.")
                return False
        except Exception as E:
            self.log.error("Method validateSOEditItemData : Passed locator incorrect or element get updated")
            self.log.error("Exception : " + str(E))

    @property
    def si_save_button(self):
        return self._si_save_button
