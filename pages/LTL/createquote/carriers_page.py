from selenium.webdriver.common.keys import Keys
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.basepage import BasePage
from utilities.read_data import read_csv_data


class CarriersPage(BasePage):
    reRateData = read_csv_data("rerate_details.csv")

    def __init__(self, driver, log):
        super().__init__(driver, log)
        self.reqop = CreateQuotePageLTL(self.driver, log)
        self.rerdp = RouteDetailsPage(self.driver, log)
        self.resi = ShippingItemsPage(self.driver, log)

    # Locators
    # ---------------------- Carrier Section Locators--------------------------
    # _pageloadwait_listingcarriers = "//div[contains(@class,'spinnerstyle__SpinnerLoader-sc-9xnc59-0')]"
    _carrier_email = "div[class*='CarrierHeaderIcon-CreateQuote']"
    _pageloadwait_listingcarriers = "//div[contains(@class,'Spinnerstyle__SpinnerLoader')]"
    _search_carrier_txtbox = "carrierSearchField"
    _carrier_cell = "//div[contains(@id,'carriers_LTL1-278_carrierItemWrapper_')]"
    _carrier_checkbox = "//div[@id='carriers_LTL1-278_carrierItemWrapper_0'] //div[contains(@class,'Checkbox')]"
    _carrier_downloadicon = "carriers_LTL1-278_carrierHeaderContainer"
    _carrier_select_buttons = "carriers_LTL1-278_button_"
    _carrier_row = "//div[@id='carriers_LTL1-278_carrierItemWrapper_"
    _carrierrow_name = "']//label/following-sibling::div"
    _carrierrow_costonly_name = "']/div/div[1]"
    _carrier_fastest = "//div[@class='carrier-itemstyles__CarrierItemWrapper-CreateQuote__sappoo-0 dtlJUB'][1]"
    _carrier_fastest_class_value = "carrier-itemstyles__CarrierItemWrapper-CreateQuote__sappoo-0 dtlJUB"
    _carrier_cheapest = "//div[@class='carrier-itemstyles__CarrierItemWrapper-CreateQuote__sappoo-0 iKgkTO'][1]"
    _carrier_normal_firstoccurance = \
        "//div[@class='carrier-itemstyles__CarrierItemWrapper-CreateQuote__sappoo-0 ckBBRO'][1]"
    _carrier_costpopup = "costCard_LTL1-51_popup"
    _carrier_withinsuranceradiobutton = "//label[contains(text(),'Rev (Incl. Insurance)')]"
    _carrier_withoutinsuranceradiobutton = "//label[contains(text(),'Rev (without insurance)')]"
    _carrier_insurancecostcolumn = "carriers_LTL1-278_insuranceCostColumn_"
    _carrier_costwithinsurance = "//section[@id='carriers_LTL1-278_insuranceCostColumn_"
    _carrier_costlink = "//div[contains(@class,'carrier-itemstyles__InsuranceCostLink')]"
    _carrier_revenuelink = "//div[contains(@class,'carrier-itemstyles__InsuranceCostText')]"
    _carrier_transitdays = "//div[contains(text(),'Point')]/following-sibling::div[2]"
    _carrier_cost_only_transit_days = "//div[contains(@class,'ShipmentDetailsHeader')])[3]/following-sibling::div[2]"
    _carrier_estimated_delivery_date = "//div[contains(text(),'Point')]/following-sibling::div[1]"
    _carrier_cost_only_estimated_delivery_date = \
        "//div[contains(@class,'ShipmentDetailsHeader')])[3]/following-sibling::div[1]"
    _carrier_guaranteedcheckbox = "//input[@id='carriers_LTL1-278_checkbox_2']/following-sibling::div"
    _carrier_guaranteedtitleincarrier = "//div[@id='carriers_LTL1-278_guaranteedLTLWrapper_"
    _carrier_costlabeltitle_count = \
        "//div[contains(@class,'cost-cardstyles__BodyRow')][2]//div[contains(@class,'cost-cardstyles__CostLabelTitle')]"
    _totalamt_popupsub_section = "//div[contains(@class,'cost-cardstyles__BodyRow')]"
    _totalamt_popup_row1 = "//div[contains(@class,'cost-cardstyles__BodyRow')][1]"
    _totalamt_popup_row2 = "//div[contains(@class,'cost-cardstyles__BodyRow')][2]"
    _totalamt_popup_row3 = "//div[contains(@class,'cost-cardstyles__BodyRow')][3]"
    _totalamt_popup_col = "//div[contains(@class,'cost-cardstyles__BodyColumn')]"
    _totalamt_popup_col1 = "//div[contains(@class,'cost-cardstyles__BodyColumn')][1]"
    _totalamt_popup_col2 = "//div[contains(@class,'cost-cardstyles__BodyColumn')][2]"
    _totalamt_popup_col3 = "//div[contains(@class,'cost-cardstyles__BodyColumn')][3]"
    _cost_label = "//div[contains(@class,'cost-cardstyles__CostLabelTitle')]"
    _costlabel_amt = "//div[contains(@class,'cost-cardstyles__CostLabelAmount')]"
    _carrier_guaranteed_amt = "//div[@name='deliveryTimes']//div[1]//label"
    _carrier_guaranteedradiobtn = "//div[@name='deliveryTimes']//div[1]//label[contains(text(),'M - $')]"
    _rerate_origin = "Tolleson, Arizona, 85353"
    _rerate_destination = "Los Angeles, CA, 90001"
    # _carrier_appointment_dates =
    # "//div[contains(@class,'carrier-itemstyles__DirectPointText-sc-1nmnyaj-23 fMnTXN')][1]"
    _carrier_appointment_dates = "//div[contains(text(),'Point')]/following-sibling::div[1]"
    _nocarriers_element = "carriers_LTL1-278_carriersNoResultsFound"

    _carrier_email_popup = "div[class*='ReactModal__Content--after-open']"
    _email_subject = "//div[contains(@class,'SubjectText')]"
    _email_to = "(//div[contains(@class, 'TextFieldHeaderDiv')]/following-sibling::div//input)[1]"
    _email_field = "(//div[contains(@class, 'TextFieldHeaderDiv')]/following-sibling::div)[1]"
    _cc_field = "(//div[contains(@class, 'TextFieldHeaderDiv')]/following-sibling::div)[2]"
    _recipient_text = "//div[@for='recipient']"
    _copy_to_text = "//div[@for='copyTo']"
    _subject_text = "div[for='subject']"
    _email_text = "div[for='emailBody']"
    _email_cc = "(//div[contains(@class, 'TextFieldHeaderDiv')]/following-sibling::div//input)[2]"
    _email_send_button = "//span[text()='SEND']/parent::button"
    _email_cancel_button = "//div[contains(@class,'EmailPopupMainstyles')] //span[text()='Cancel']/parent::button"
    _email_spinner = "//div[text()='Loading']"
    _email_contact_list_header_value = "//div[@id='LTL1-628_email_contact_list']/div[@data-testid='header']/div[1]"
    _email_contact_label_part = "(//div[@id='LTL1-628_email_contact_list']//label[@data-testid='text-field-label'])["
    _cancel_button_email_contact = "//div[@id='LTL1-628_email_contact_list']//span[text()='Cancel']/parent::button"
    _select_button_email_contact = "//div[@id='LTL1-628_email_contact_list']//span[text()='Select']/parent::button"
    _email_customer_list = "//div[contains(@class,'ShowCustomerName')]"
    _email_pop_email_list = "//div[contains(@class,'ShowEmail')]"
    _customer_select_email_popup = \
        "//input[@id='LTL1-628_email_contact_list_address_checkbox_0']/following-sibling::div"
    _selected_email_address = "//div[@for='recipient']/parent::div/following-sibling::div"
    _selected_copy_to_address = "//div[@for='copyTo']/parent::div/following-sibling::div"
    # ---------  Adjust Rate Feature Locators ----------------------#

    _adjustrate_hyperlink = "carriers_LTL1-278_carrierLiabilityLink_"
    _adjustrate_defaultwindow = "carriers_LTL1-278_adjustSellRate_"
    _adjustrate_cancelbtn = "//div[contains(@class,'adjust-sell-ratestyle__ActionGroup')]//button[1]"
    _adjustrate_adjustbtn = "//div[contains(@class,'adjust-sell-ratestyle__ActionGroup')]//button[2]"
    _adjustrate_orisellrate_value = "//div[text()='Original Sell Rate']/following-sibling::div"
    _adjustrate_oriprofitmarginpercent_value = "//div[text()='Original Profit Margin %']/following-sibling::div"
    _adjustrate_newsellrate_value = "//div[text()='New Sell Rate']/following-sibling::div//input"
    _adjustrate_newprofitmarginpercent_value = "//div[text()='New Profit Margin %']/following-sibling::div//input"
    _adjustrate_threshold_errormsg = "//article[contains(@class,'adjust-sell-ratestyle__ErrorMessage')]"
    _adjustrate_thresholdvalue_errormsg = "The sell rate is below threshold value."

    # ---------------------------------Carriers section method------------------------------------

    def clearSearchCarrierField(self):
        """
        Method to clear the text in the search field textbox.
        """
        self.log.info("Inside searchCarrierByNameandSelect Method")
        self.sendKeys(" ", self._search_carrier_txtbox, "name")
        self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")

    def searchCarrier(self, carrierName):
        """
        Method to search the carrier from the list of carriers displayed.
        :param carrierName: Name of the carrier
        """
        self.log.info("Inside searchCarrier Method")
        try:
            FinalsearchStatusResult = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")
            self.sendKeys(carrierName, self._search_carrier_txtbox, "name")
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))
            for i in range(noOfCarriers):
                matchingCarrier = self._carrier_row + str(i) + self._carrierrow_name
                carrierDisplayed = self.getText(matchingCarrier, "xpath")
                if carrierName.lower() in carrierDisplayed.lower():
                    self.log.info(
                        "The passed text " + str(carrierName) + " is matching in the carrier " + str(carrierDisplayed))
                    initialSearchStatusResult = True
                else:
                    self.log.info("The passed text " + str(carrierName) + " is not matching in the carrier " + str(
                        carrierDisplayed))
                    initialSearchStatusResult = False
                FinalsearchStatusResult = initialSearchStatusResult and initialSearchStatusResult
            if FinalsearchStatusResult:
                self.log.info("The matching carriers are found in carrier list")
                return True
            else:
                self.log.info("The matching carriers are not found in carrier list")
                return False

        except Exception as e:
            self.log.error("Method searchCarrier : Passed carrier name " + str(
                carrierName) + " is not found from the matching carrier list.")
            self.log.error("Exception" + str(e))

    def selectCarrierByFilter(self, carrierFilter, isInsurancePresent, selectIndex=0):
        """
        Method to select a carrier by its filter(Cheapest,Fastest,Guaranteed).
        :param selectIndex: select the carrier index value
        :param carrierFilter: Carrier Filter
        :param isInsurancePresent : selecting the respective filter carriers with/without insurance based on the
        parameter value
        """
        # carrierSelect = ""
        self.log.info("Inside selectCarrierByFilter Method")
        try:
            # carrierList = []
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")

            if "cheapest".lower() == carrierFilter.lower():
                self.log.info("Inside SelectCheapestCarrier")
                cheapestCarrierSectionIdValue = self.getAttribute(self._carrier_cheapest, locatorType="xpath",
                                                                  attributeType="id")
                self.log.info("Attribute value, value :" + str(cheapestCarrierSectionIdValue))
                index = cheapestCarrierSectionIdValue.replace("carriers_LTL1-278_carrierItemWrapper_", "")
                startIndex = int(index)
                carrierNameFromIndex = self.getText(self._carrier_row + str(startIndex) +
                                                    self._carrierrow_name, "xpath")
                self.log.info("Carrier Name from the Index :" + str(carrierNameFromIndex))
                finalList = self.totalAmountBreakup(startIndex, isInsurancePresent)
                self.waitForElement(self._carrier_cheapest, locatorType="xpath")
                self.pageVerticalScroll(self._carrier_cheapest, "xpath")
                self.elementClick(self._carrier_cheapest, locatorType="xpath")
                finalList.append(carrierNameFromIndex)
                self.log.info("Final List " + str(finalList))
                return finalList

            elif "fastest".lower() == carrierFilter.lower():
                self.log.info("Inside SelectFastestCarrier")
                fastestCarrierSectionIdValue = self.getAttribute(self._carrier_fastest, locatorType="xpath",
                                                                 attributeType="id")
                self.log.info("Attribute value, value :" + str(fastestCarrierSectionIdValue))
                index = fastestCarrierSectionIdValue.replace("carriers_LTL1-278_carrierItemWrapper_", "")
                startIndex = int(index)
                carrierNameFromIndex = self.getText(self._carrier_row + str(startIndex) +
                                                    self._carrierrow_name, "xpath")
                finalList = self.totalAmountBreakup(startIndex, isInsurancePresent)
                self.waitForElement(self._carrier_fastest, locatorType="xpath")
                self.pageVerticalScroll(self._carrier_fastest, "xpath")
                self.elementClick(self._carrier_fastest, locatorType="xpath")
                finalList.append(carrierNameFromIndex)
                self.log.info("Final List " + str(finalList))
                return finalList
            elif "guaranteed".lower() == carrierFilter.lower():
                self.log.info("Inside SelectGuaranteedCarrier")
                guaranteedTitleStatus = True
                self.elementClick(self._carrier_guaranteedcheckbox, locatorType="xpath")
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")
                elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
                noOfCarriers = len(elementList)
                self.log.info("Total no. of Guaranteed Carriers in the carrier list :" + str(noOfCarriers))
                if noOfCarriers == 0:
                    guaranteedTitleStatus = False

                for x in range(noOfCarriers):
                    guaranteedTitle = self.getText(self._carrier_guaranteedtitleincarrier + str(x) + "']/div[1]",
                                                   locatorType="xpath")
                    if guaranteedTitle == "Guaranteed LTL":
                        titleStatus = True
                    else:
                        titleStatus = False
                    guaranteedTitleStatus = titleStatus and guaranteedTitleStatus

                if guaranteedTitleStatus:
                    self.log.info("All the carriers are guaranteed carriers")
                    self.elementClick(self._carrier_row + str(selectIndex) + "']" + self._carrier_guaranteedradiobtn,
                                      locatorType="xpath")
                    carrierNameFromIndex = self.getText(self._carrier_row + str(selectIndex) +
                                                        self._carrierrow_name, "xpath")
                    finalList = self.totalAmountBreakup(selectIndex, isInsurancePresent)
                    carrierSelect = self._carrier_select_buttons + str(selectIndex)
                    self.waitForElement(carrierSelect, locatorType="id")
                    self.elementClick(carrierSelect, locatorType="id")
                    finalList.append(carrierNameFromIndex)
                    self.log.info("Final List " + str(finalList))
                    return finalList
                else:
                    self.log.error("All the carriers are not guaranteed carriers/no carriers displayed.")

            else:
                self.log.info("The passed carrier filter " + str(carrierFilter) + " is not present")

            createOrderButtonEnabledStatus = self.reqop.verifyCreateOrderButtonEnabled()
            if createOrderButtonEnabledStatus:
                self.log.info("The Create Order Button is Enabled")
            else:
                self.log.error("The Create Order Button is Disabled")

        except Exception as e:
            self.log.error("Method selectCarrierByFilter :Respective carrier is not select " + str(
                carrierFilter) + " or passed locator may got updated/wrong or "
                                 "clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def totalAmountBreakup(self, carrierIndex, isInsurancePresent):
        """
        Method to return the cost breakup in a list based on the parameter whether with insurance or without insurance.
        :param isInsurancePresent: Yes if insurance is present and No if not.
        :param carrierIndex:
        :return Total Amt breakup charge along with Validation status in list based on the insurance parameter.
        """
        self.log.info("Inside totalAmountBreakup Method")
        try:
            costLabelListUI = []
            totalAmtListUI = []
            costAmtListUI = []
            revenueAmtListUI = []
            finalTotalAmtPopupList = []
            validationResult = True

            if isInsurancePresent.lower() == "yes":
                self.log.info("If Block : IsInsurancePresent Value is : " + str(isInsurancePresent))
                self.log.info("If Block : CarrierIndex Value is : " + str(carrierIndex))

                clickCostWithInsurance = self._carrier_costwithinsurance + str(
                    carrierIndex) + "'][1]" + self._carrier_costlink
                clickRadioButtonWithInsurance = self._carrier_row + str(
                    carrierIndex) + "']" + self._carrier_withinsuranceradiobutton
                self.pageVerticalScroll(clickRadioButtonWithInsurance, "xpath")
                costHyperLink = self.getText(clickCostWithInsurance, "xpath")
                costHyperLinkUI = costHyperLink[5:-4]
                clickRevenueWithInsurance = self._carrier_costwithinsurance + str(
                    carrierIndex) + "'][1]" + self._carrier_revenuelink
                revenueHyperLink = self.getText(clickRevenueWithInsurance, "xpath")
                revenueHyperLinkUI = revenueHyperLink[:-4]

                self.log.info("Total Cost from display(Hyperlink) : " + str(costHyperLinkUI))
                self.log.info("Total Revenue from display(Hyperlink)  : " + str(revenueHyperLinkUI))

                self.elementClick(clickRadioButtonWithInsurance, locatorType="xpath")
                self.elementClick(clickCostWithInsurance, locatorType="xpath")
                self.waitForElement(self._carrier_costpopup, locatorType="id", event="display", timeout=20)
                self.pageVerticalScroll(self._totalamt_popup_row3, "xpath")

                costLabelList = self.getElementList(self._totalamt_popup_row2 + self._cost_label, locatorType="xpath")
                for i in costLabelList:
                    costLabel = i.text
                    costLabelListUI.append(costLabel)

                totalAmtList = self.getElementList(
                    self._totalamt_popup_row3 + self._totalamt_popup_col + self._cost_label, locatorType="xpath")
                for i in totalAmtList[1:]:
                    totalAmt = i.text[:-4]
                    totalAmtListUI.append(totalAmt)

                costLabelAmtList = self.getElementList(self._totalamt_popup_col2 + self._costlabel_amt,
                                                       locatorType="xpath")
                for i in costLabelAmtList:
                    costLabelAmt = i.text
                    costAmtListUI.append(costLabelAmt)

                revenueLabelAmtList = self.getElementList(self._totalamt_popup_col3 + self._costlabel_amt,
                                                          locatorType="xpath")
                for i in revenueLabelAmtList:
                    revenueLabelAmt = i.text
                    revenueAmtListUI.append(revenueLabelAmt)

                totalCostAmt = totalAmtListUI[0]
                totalRevenueAmt = totalAmtListUI[1]

                self.log.info("Total Cost from display(Hyperlink) : " + str(costHyperLinkUI))
                self.log.info("Total Cost from breakup popup  : " + str(totalCostAmt))
                self.log.info("Total Revenue from display(Hyperlink) : " + str(revenueHyperLinkUI))
                self.log.info("Total Revenue from breakup popup  : " + str(totalRevenueAmt))

                if costHyperLinkUI == totalCostAmt and revenueHyperLinkUI == totalRevenueAmt:
                    self.log.info(f"Cost & Revenue hyperlink value in UI is matching with Total Cost & Revenue "
                                  f"present in Total Amount Breakup Popup at with insurance rate")
                    validationResult = validationResult and True
                    self.log.info("totalAmountBreakup at with insurance: validationResult " + str(validationResult))
                else:
                    self.log.error(f"Cost/Revenue hyperlink value in UI is not matching with Total Cost & Revenue "
                                   f"present n Total Amount Breakup Popup at with insurance rate")
                    validationResult = validationResult and False
                    self.log.info("totalAmountBreakup at with insurance: validationResult " + str(validationResult))

                if "Insurance" in costLabelListUI:
                    self.log.info("Insurance breakup is present in with insurance rates.")
                    validationResult = validationResult and True
                    self.log.info("totalAmountBreakup at with insurance: validationResult " + str(validationResult))
                else:
                    self.log.error("Insurance breakup is not present in with insurance rates.")
                    validationResult = validationResult and False
                    self.log.info("totalAmountBreakup at with insurance: validationResult " + str(validationResult))

                self.log.info("Total Cost Amt from UI : " + str(totalCostAmt))
                finalTotalAmtPopupList.append(totalCostAmt)
                self.log.info("Total Revenue Amt from UI : " + str(totalRevenueAmt))
                finalTotalAmtPopupList.append(totalRevenueAmt)
                self.log.info("Breakup Charges Label List : " + str(costLabelListUI))
                finalTotalAmtPopupList.append(costLabelListUI)
                self.log.info("Cost Breakup charges List : " + str(costAmtListUI))
                finalTotalAmtPopupList.append(costAmtListUI)
                self.log.info("Revenue Breakup Charges List : " + str(revenueAmtListUI))
                finalTotalAmtPopupList.append(revenueAmtListUI)
                self.log.info("Cost And Revenue Hyperlink and Insurance Validation Status Result : " +
                              str(validationResult))
                finalTotalAmtPopupList.append(validationResult)

                self.log.info("Final List of Total Amount Popup from UI : " + str(finalTotalAmtPopupList))
                # finalTotalAmtPopupList : Total Cost,Total revenue,Breakup Name,Cost Breakup,
                # ...Revenue Breakup,Cost&Revenue HyperLink & Insurance Validation Status
                self.pageVerticalScroll(self._carrier_row + str(carrierIndex) + "']" + self._carrier_transitdays,
                                        "xpath")
                self.elementClick(self._carrier_row + str(carrierIndex) + "']" + self._carrier_transitdays, "xpath")
                return finalTotalAmtPopupList

            elif isInsurancePresent.lower() == "guaranteed":
                self.log.info("elif Block : IsInsurancePresent Value is : " + str(isInsurancePresent))
                self.log.info("elif Block : CarrierIndex Value is : " + str(carrierIndex))
                guaranteedAmtUI = self.getText(
                    self._carrier_row + str(carrierIndex) + "']" + self._carrier_guaranteed_amt, locatorType="xpath")
                self.log.info("guaranteedAmtUI" + str(guaranteedAmtUI))
                guaranteedAmt = guaranteedAmtUI[11:]
                self.log.info("guaranteedAmtAmt" + str(guaranteedAmt))
                return guaranteedAmt

            elif isInsurancePresent.lower() == "no":
                self.log.info("Else Block : IsInsurancePresent Value is : " + str(isInsurancePresent))
                self.log.info("Else Block : CarrierIndex Value is : " + str(carrierIndex))

                clickCostWithoutInsurance = self._carrier_costwithinsurance + str(
                    carrierIndex) + "'][2]" + self._carrier_costlink
                clickRadioButtonWithoutInsurance = self._carrier_row + str(
                    carrierIndex) + "']" + self._carrier_withoutinsuranceradiobutton
                self.pageVerticalScroll(clickRadioButtonWithoutInsurance, "xpath")
                costHyperLink = self.getText(clickCostWithoutInsurance, "xpath")
                costHyperLinkUI = costHyperLink[5:-4]
                clickRevenueWithInsurance = self._carrier_costwithinsurance + str(
                    carrierIndex) + "'][2]" + self._carrier_revenuelink
                revenueHyperLink = self.getText(clickRevenueWithInsurance, "xpath")
                revenueHyperLinkUI = revenueHyperLink[:-4]

                self.log.info("Total Cost from display(Hyperlink) : " + str(costHyperLinkUI))
                self.log.info("Total Revenue from display(Hyperlink) : " + str(revenueHyperLinkUI))

                self.elementClick(clickRadioButtonWithoutInsurance, locatorType="xpath")
                self.elementClick(clickCostWithoutInsurance, locatorType="xpath")
                self.waitForElement(self._carrier_costpopup, locatorType="id", event="display", timeout=20)
                self.pageVerticalScroll(self._totalamt_popup_row3, "xpath")

                costLabelList = self.getElementList(self._totalamt_popup_row2 + self._cost_label, locatorType="xpath")
                for i in costLabelList:
                    costLabel = i.text
                    costLabelListUI.append(costLabel)

                totalAmtList = self.getElementList(
                    self._totalamt_popup_row3 + self._totalamt_popup_col + self._cost_label, locatorType="xpath")
                for i in totalAmtList[1:]:
                    totalAmt = i.text[:-4]
                    totalAmtListUI.append(totalAmt)

                costLabelAmtList = self.getElementList(self._totalamt_popup_col2 + self._costlabel_amt,
                                                       locatorType="xpath")
                for i in costLabelAmtList:
                    costLabelAmt = i.text
                    costAmtListUI.append(costLabelAmt)

                revenueLabelAmtList = self.getElementList(self._totalamt_popup_col3 + self._costlabel_amt,
                                                          locatorType="xpath")
                for i in revenueLabelAmtList:
                    revenueLabelAmt = i.text
                    revenueAmtListUI.append(revenueLabelAmt)

                totalCostAmt = totalAmtListUI[0]
                totalRevenueAmt = totalAmtListUI[1]

                self.log.info("Total Cost from display(Hyperlink) : " + str(costHyperLinkUI))
                self.log.info("Total Cost from breakup popup  : " + str(totalCostAmt))
                self.log.info("Total Revenue from display(Hyperlink) : " + str(revenueHyperLinkUI))
                self.log.info("Total Revenue from breakup popup  : " + str(totalRevenueAmt))

                if costHyperLinkUI == totalCostAmt and revenueHyperLinkUI == totalRevenueAmt:
                    self.log.info(f"Cost & Revenue hyperlink value in UI is matching with Total Cost & Revenue "
                                  f"present in Total Amount Breakup Popup at without insurance rate")
                    validationResult = validationResult and True
                    self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))
                else:
                    self.log.error(f"Cost/Revenue hyperlink value in UI is not matching with Total Cost & Revenue "
                                   f"present n Total Amount Breakup Popup at without insurance rate")
                    validationResult = validationResult and False
                    self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))

                if "Insurance" not in costLabelListUI:
                    self.log.info("Insurance breakup is not present in without insurance rates.")
                    validationResult = validationResult and True
                    self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))
                else:
                    self.log.error("Insurance breakup is present in without insurance rates.")
                    validationResult = validationResult and False
                    self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))

                self.log.info("Total Cost Amt from UI : " + str(totalCostAmt))
                finalTotalAmtPopupList.append(totalCostAmt)
                self.log.info("Total Revenue Amt from UI : " + str(totalRevenueAmt))
                finalTotalAmtPopupList.append(totalRevenueAmt)
                self.log.info("Breakup Charges Label List : " + str(costLabelListUI))
                finalTotalAmtPopupList.append(costLabelListUI)
                self.log.info("Cost Breakup charges List : " + str(costAmtListUI))
                finalTotalAmtPopupList.append(costAmtListUI)
                self.log.info("Revenue Breakup Charges List : " + str(revenueAmtListUI))
                finalTotalAmtPopupList.append(revenueAmtListUI)
                self.log.info("Cost And Revenue Hyperlink and Insurance Validation Status Result : " +
                              str(validationResult))
                finalTotalAmtPopupList.append(validationResult)

                self.log.info("Final List of Total Amount Popup from UI : " + str(finalTotalAmtPopupList))
                # finalTotalAmtPopupList : Total Cost,Total revenue,Breakup Name,Cost Breakup,
                # ...Revenue Breakup,Cost&Revenue HyperLink & Insurance Breakup Validation Status
                self.pageVerticalScroll(self._carrier_row + str(carrierIndex) + "']" + self._carrier_transitdays,
                                        "xpath")
                self.elementClick(self._carrier_row + str(carrierIndex) + "']" + self._carrier_transitdays, "xpath")
                return finalTotalAmtPopupList

            else:
                self.log.info("Passed parameter " + str(isInsurancePresent) + " is not matching with the conditions")

        except Exception as e:
            self.log.error("Method totalAmountBreakup :Not able to validate amount breakup charges or passed locator "
                           "may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def validateAllCarriersBreakupCharges(self):
        """
        Method to validate All carriers Total Amount breakup charges(Cost & Revenue) With & Without Insurance
        """
        self.log.info("Inside validateAllBreakupCharges Method")
        try:
            totalAmtValidationHavingInsuranceResult = True
            totalAmtValidationWithoutInsuranceResult = True
            setFlag = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
                if not self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath") \
                        and not self.isElementPresent(self._nocarriers_element, "id"):
                    setFlag = setFlag and True
                else:
                    setFlag = setFlag and False
            elif self.isElementPresent(self._nocarriers_element, "id"):
                self.log.debug("No carriers/results found")
                setFlag = setFlag and False

            self.log.info("setflag value : " + str(setFlag))
            if setFlag:

                elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
                noOfCarriers = len(elementList)
                self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

                if int(noOfCarriers) > 0:
                    # Validating All carriers Total Amount breakup charges(Cost & Revenue) with & without Insurance
                    for startIndex in range(noOfCarriers):
                        # Total Amount Breakup with insurance
                        finalTotalAmountPopupList = self.totalAmountBreakup(startIndex, "yes")
                        totalAmtValidationHavingInsuranceResultInitial = self.validateTotalCostAndRevenue(
                            finalTotalAmountPopupList)
                        totalAmtValidationHavingInsuranceResult = \
                            totalAmtValidationHavingInsuranceResult and totalAmtValidationHavingInsuranceResultInitial

                        # Total Amount Breakup without insurance
                        finalTotalAmountPopupList = self.totalAmountBreakup(startIndex, "no")
                        totalAmtValidationWithoutInsuranceResultInitial = self.validateTotalCostAndRevenue(
                            finalTotalAmountPopupList)
                        totalAmtValidationWithoutInsuranceResult = \
                            totalAmtValidationWithoutInsuranceResult and totalAmtValidationWithoutInsuranceResultInitial

                    self.log.info("Total amount breakup charge with insurance status: " +
                                  str(totalAmtValidationHavingInsuranceResult))
                    self.log.info("Total amount breakup charge without insurance status: " +
                                  str(totalAmtValidationWithoutInsuranceResult))

                    if totalAmtValidationHavingInsuranceResult and totalAmtValidationWithoutInsuranceResult:
                        self.log.info("Total amount breakup charges are validated for amount with insurance and "
                                      "without insurance")
                        return True
                    else:
                        self.log.error("Total amount breakup charges are not correct for amount with/without insurance")
                        return False
                else:
                    self.log.error("No Carrier(s) found")
                    return False
            else:
                self.log.error("No carrier(s) found in the carrier section, setFlag value : " + str(setFlag))
                return False
        except Exception as e:
            self.log.error(f"Method validateAllCarriersBreakupCharges : Not able to validate all amounts breakup "
                           f"charges or passed locator may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def validateTotalCostAndRevenue(self, referenceList):
        """
        Method to validate total cost with all the breakup cost's and total revenue with all the breakup revenue's
        :param referenceList: List containing all the details in Total Amount popup in the order
        Total Cost,Total revenue,Breakup charges Label,Breakup charges Cost,Breakup charges Revenue,hyperlink &
        Insurance breakup status
        """
        self.log.info("Inside validateTotalCostAndRevenue Method")
        try:

            # referenceList = [tccost,trevenue,label,cost,revenue,cost & revenue hyperlink & Insurance breakup status]
            costAmtListUI = [float(i) for i in referenceList[3]]
            summedCost = sum(costAmtListUI)
            totalCost = round(summedCost, 2)
            revenueAmtListUI = [float(i) for i in referenceList[4]]
            summedRevenue = sum(revenueAmtListUI)
            totalRevenue = round(summedRevenue, 2)
            self.log.info("Total Cost : " + str(totalCost))
            self.log.info("Total Revenue : " + str(totalRevenue))
            self.log.info("Total Cost from Total Amount Breakup Popup : " + str(referenceList[0]))
            self.log.info("Total Revenue from Total Amount Breakup Popup  : " + str(referenceList[1]))

            if totalCost == float(referenceList[0]) and totalRevenue == float(referenceList[1]):
                self.log.info("Cost and Revenue total is correct")
                sumTotalCostAndRevenue = True
            else:
                self.log.error("Cost/Revenue total is not correct")
                sumTotalCostAndRevenue = False

            self.log.info("sumTotalCostAndRevenue Status : " + str(sumTotalCostAndRevenue))
            self.log.info("Total Cost & Revenue from Total Amount Breakup Popup with Cost & Revenue HyperLink Value "
                          "Validation Status: " + str(referenceList[5]))

            if sumTotalCostAndRevenue and referenceList[5]:
                self.log.info("Cost and Revenue total is correct,Insurance status is correct and"
                              " hyperlink cost & revenue are matching with total cost & revenue in amt breakup")
                return True
            else:
                self.log.error("Cost/Revenue total is not correct or Insurance status is not correct or"
                               " hyperlink cost/revenue are not matching with total cost & revenue in amt breakup")
                return False

        except Exception as e:
            self.log.error("Method validateTotalCostAndRevenue : Not able to validate all amounts breakup charges "
                           "or passed locator may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def validateNormalCarrierOrdering(self, flow="CustomerRates"):
        """
        Method to validate normal carrier ordering and it is in ascending order.
        :return True if the normal carrier ordering is correct and in ascending order.
        """
        self.log.info(
            "Inside validateNormalCarrierOrdering Method and the passed argument Create Quote Flow is " + str(flow))
        try:
            costList = []
            breakFlag = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

            if flow.lower() == "CustomerRates".lower():
                self.log.info("Inside If ,Flow : CustomerRates")
                carrierCostWithoutInsuranceSection = 2
                carrierRowName = self._carrierrow_name
            else:
                self.log.info("Inside else ,Flow : CostOnly")
                carrierCostWithoutInsuranceSection = 1
                carrierRowName = self._carrierrow_costonly_name

            # attribute
            normalCarrierSectionIdValue = self.getAttribute(self._carrier_normal_firstoccurance, locatorType="xpath",
                                                            attributeType="id")
            self.log.info("Attribute value, value :" + str(normalCarrierSectionIdValue))
            index = normalCarrierSectionIdValue.replace("carriers_LTL1-278_carrierItemWrapper_", "")
            startIndex = int(index)
            for x in range(startIndex, noOfCarriers):
                carrierCostFromUI = self.getText(self._carrier_costwithinsurance + str(x) + "'][" + str(
                    carrierCostWithoutInsuranceSection) + "]" + self._carrier_costlink, "xpath")
                carrierCost = carrierCostFromUI[5:-4]
                costList.append(carrierCost)
            self.log.info("CostList from the UI costList: " + str(costList))
            costList = [float(i) for i in costList]
            for i in range(len(costList)):
                for j in range(i + 1, len(costList)):
                    if costList[i] > costList[j]:
                        breakFlag = False
                        wrongOrderCarrierName = self.getText(
                            self._carrier_row + str(i + startIndex) + str(carrierRowName), "xpath")
                        self.log.info("cost order wrong for carrier name" + str(wrongOrderCarrierName))
                        break

            if not breakFlag:
                self.log.info("Normal carriers ordering is not correct")
                return False
            else:
                self.log.info("Normal carriers ordering is correct and its in ascending order")
                return True

        except Exception as e:
            self.log.error("Method validateNormalCarrierOrdering : Not able to find the normal carrier ordering or "
                           "passed locator may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def validateFastestCarrier(self, flow="CustomerRates"):
        """
        Method to validate the fastest carrier among the carrier list.
        :return True if the carrier is the fastest.
        """
        self.log.info("Inside validateFastestCarrier Method and the passed argument Create Quote Flow is " + str(flow))
        try:
            fastest_carriers_list_from_class_index = []
            fastest_carriers_list_from_ui = []

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")
            element_list = self.getElementList(self._carrier_cell, locatorType="xpath")
            no_of_carriers = len(element_list)
            self.log.info("Total no. of Carriers in the carrier list :" + str(no_of_carriers))

            if flow.lower() == "CustomerRates".lower():
                self.log.info("Inside If ,Flow : CustomerRates")
                carrier_row_name = self._carrierrow_name
            else:
                self.log.info("Inside else ,Flow : CostOnly")
                carrier_row_name = self._carrierrow_costonly_name

            # Finding Fastest Carrier starting index using class and finding its transit days & estimate delivery date
            # Finding Fastest Carrier starting index using class
            fastest_carrier_section_id_value = self.getAttribute(self._carrier_fastest, locatorType="xpath",
                                                                 attributeType="id")
            self.log.info("Attribute value, value :" + str(fastest_carrier_section_id_value))
            index = fastest_carrier_section_id_value.replace("carriers_LTL1-278_carrierItemWrapper_", "")
            start_index = int(index)
            start_index_ui = int(index)
            # finding first occurrence of fastest carrier's transit days & estimate delivery date
            if flow.lower() == "CustomerRates".lower():
                transit_days_from_ui_fastest_first_occurrence = \
                    self.getText(self._carrier_row + str(start_index) + "']" + self._carrier_transitdays,
                                 locatorType="xpath")
                estimated_delivery_date_from_ui_fastest_first_occurrence = \
                    self.getText(self._carrier_row + str(start_index) + "']" + self._carrier_estimated_delivery_date,
                                 locatorType="xpath")
            else:
                transit_days_from_ui_fastest_first_occurrence = \
                    self.getText("(" + self._carrier_row + str(start_index) + "']" +
                                 self._carrier_cost_only_transit_days, locatorType="xpath")
                estimated_delivery_date_from_ui_fastest_first_occurrence = \
                    self.getText("(" + self._carrier_row + str(start_index) + "']" +
                                 self._carrier_cost_only_estimated_delivery_date, locatorType="xpath")
            if transit_days_from_ui_fastest_first_occurrence == "1 Day":
                transit_days_from_ui_fastest_first_occurrence_after_trim = \
                    transit_days_from_ui_fastest_first_occurrence[:-4]
            else:
                transit_days_from_ui_fastest_first_occurrence_after_trim = \
                    transit_days_from_ui_fastest_first_occurrence[:-5]

            if '-' in transit_days_from_ui_fastest_first_occurrence_after_trim:
                transit_days_first_occurrence = transit_days_from_ui_fastest_first_occurrence_after_trim.split("-")[0]
            else:
                transit_days_first_occurrence = transit_days_from_ui_fastest_first_occurrence_after_trim

            self.log.info("transit_days_first_occurrence : " + str(transit_days_first_occurrence))
            self.log.info("estimated_delivery_date_from_ui_fastest_first_occurrence : " +
                          str(estimated_delivery_date_from_ui_fastest_first_occurrence))

            # Preparing Fastest Carrier List from the Start Index of Fastest Carrier
            # class to till Remaining Carrier's list End
            while start_index < no_of_carriers:
                fastest_carrier_class_from_ui = self.getAttribute("carriers_LTL1-278_carrierItemWrapper_" +
                                                                  str(start_index), locatorType="id",
                                                                  attributeType="class")

                if fastest_carrier_class_from_ui == self._carrier_fastest_class_value:
                    self.log.info("The carrier is the fastest carrier")
                    carrier_name_from_index = self.getText(self._carrier_row + str(start_index) + str(carrier_row_name),
                                                           "xpath")
                    self.log.info("Fastest Carrier Name :" + str(carrier_name_from_index))
                    fastest_carriers_list_from_class_index.append(carrier_name_from_index)
                start_index += 1

            # Prepare Fastest Carrier list from the whole carrier(s) displayed
            for index in range(start_index_ui, no_of_carriers):
                if flow.lower() == "CustomerRates".lower():
                    transit_days_from_ui = \
                        self.getText(self._carrier_row + str(index) + "']" + self._carrier_transitdays,
                                     locatorType="xpath")
                    estimated_delivery_date_from_ui = \
                        self.getText(self._carrier_row + str(index) + "']" + self._carrier_estimated_delivery_date,
                                     locatorType="xpath")
                else:
                    transit_days_from_ui = \
                        self.getText("(" + self._carrier_row + str(index) + "']" +
                                     self._carrier_cost_only_transit_days, locatorType="xpath")
                    estimated_delivery_date_from_ui = \
                        self.getText("(" + self._carrier_row + str(index) + "']" +
                                     self._carrier_cost_only_estimated_delivery_date, locatorType="xpath")
                if transit_days_from_ui == "1 Day":
                    transit_days_from_ui_after_trim = transit_days_from_ui[:-4]
                else:
                    transit_days_from_ui_after_trim = transit_days_from_ui[:-5]

                if '-' in transit_days_from_ui_after_trim:
                    transit_days_occurrence_from_ui = transit_days_from_ui_after_trim.split("-")[0]
                else:
                    transit_days_occurrence_from_ui = transit_days_from_ui_after_trim

                self.log.info("transit_days_first_occurrence : " + str(transit_days_first_occurrence) +
                              " , transit_days_occurrence_from_ui : " + str(transit_days_occurrence_from_ui) +
                              " and estimated_delivery_date_from_ui_fastest_first_occurrence : " +
                              str(estimated_delivery_date_from_ui_fastest_first_occurrence) +
                              " , estimated_delivery_date_from_ui : " +
                              str(estimated_delivery_date_from_ui))

                if transit_days_first_occurrence == transit_days_occurrence_from_ui and \
                        estimated_delivery_date_from_ui_fastest_first_occurrence == estimated_delivery_date_from_ui:
                    self.log.info("The carrier is the fastest carrier")
                    carrier_name_from_index = self.getText(self._carrier_row + str(index) + str(carrier_row_name),
                                                           "xpath")
                    self.log.info("Fastest Carrier Name :" + str(carrier_name_from_index))
                    fastest_carriers_list_from_ui.append(carrier_name_from_index)

            self.log.info("Fastest Carriers List Using class:" + str(fastest_carriers_list_from_class_index))
            self.log.info(
                "Fastest Carriers List  from the carriers List displayed from UI :" +
                str(fastest_carriers_list_from_ui))

            if fastest_carriers_list_from_ui == fastest_carriers_list_from_class_index:
                self.log.info("Fastest carrier(s) ordering is correct and Fastest Carriers List : " +
                              str(fastest_carriers_list_from_class_index))
                return True
            else:
                self.log.error("Fastest carrier(s) ordering is not correct")
                return False

        except Exception as e:
            self.log.error("Method validateFastestCarrier : Not able to find the fastest carrier or passed locator "
                           "may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def validateCheapestCarrier(self, flow="CustomerRates"):
        """
        Method to validate the cheapest carrier among the carrier list.
        :return True if the carrier is the cheapest.
        """
        self.log.info("Inside validateCheapestCarrier Method and the passed argument Create Quote Flow is " + str(flow))
        try:
            costList = []
            cheapestCarriers = []
            cheapestCarriersFromTopOfTheCarrierList = []
            cheapestCarrierStatus = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay")
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

            if flow.lower() == "CustomerRates".lower():
                self.log.info("Inside If ,Flow : CustomerRates")
                carrierCostWithoutInsuranceSection = 2
                carrierRowName = self._carrierrow_name
            else:
                self.log.info("Inside else ,Flow : CostOnly")
                carrierCostWithoutInsuranceSection = 1
                carrierRowName = self._carrierrow_costonly_name

            for i in range(noOfCarriers):
                carrierCostFromUI = self.getText(self._carrier_costwithinsurance + str(i) + "'][" + str(
                    carrierCostWithoutInsuranceSection) + "]" + self._carrier_costlink, "xpath")
                carrierCost = carrierCostFromUI[5:-4]
                costList.append(carrierCost)

            self.log.info("CostList after for loop : " + str(costList))
            sortedCostList = [float(i) for i in costList]
            sortedCostList.sort()
            self.log.info("CostList after sort : " + str(sortedCostList))
            noOfOccurance = sortedCostList.count(sortedCostList[0])
            # Cheapest Carriers from the UI from the Carrier List from the top of the list
            for startIndex in range(noOfOccurance):
                cheapestCarrierName = self.getText(self._carrier_row + str(startIndex) + str(carrierRowName), "xpath")
                cheapestCarriersFromTopOfTheCarrierList.append(cheapestCarrierName)
            self.log.info("Cheapest Carriers List  from the carriers List from UI :" + str(
                cheapestCarriersFromTopOfTheCarrierList))

            cheapestCarrierSectionIdValue = self.getAttribute(self._carrier_cheapest, locatorType="xpath",
                                                              attributeType="id")
            self.log.info("Attribute value, value :" + str(cheapestCarrierSectionIdValue))
            index = cheapestCarrierSectionIdValue.replace("carriers_LTL1-278_carrierItemWrapper_", "")
            startIndex = int(index)
            startIndexCondition = startIndex
            # Cheapest Carriers list from the cheapest section and verifying the lowest cost
            while startIndex < startIndexCondition + noOfOccurance:
                cheapestCarrierCostFromUI = self.getText(
                    self._carrier_costwithinsurance + str(startIndex) + "'][" + str(
                        carrierCostWithoutInsuranceSection) + "]" + self._carrier_costlink, "xpath")
                self.log.info("cheapestCarrierCostFromUI : " + str(cheapestCarrierCostFromUI))
                cheapestCarrierCost = cheapestCarrierCostFromUI[5:-4]
                self.log.info("cheapestCarrierCost : " + str(cheapestCarrierCost))

                if sortedCostList[0] == float(cheapestCarrierCost):
                    cheapestCarrierName = self.getText(self._carrier_row + str(startIndex) + str(carrierRowName),
                                                       "xpath")
                    cheapestCarriers.append(cheapestCarrierName)
                    cheapestCarrierStatus0 = True
                else:
                    cheapestCarrierStatus0 = False
                cheapestCarrierStatus = cheapestCarrierStatus and cheapestCarrierStatus0
                startIndex += 1

            self.log.info(
                "cheapestCarrierStatus for the carrier(s) based on the lowest cost : " + str(cheapestCarrierStatus))
            self.log.info("Cheapest Carriers List Using class:" + str(cheapestCarriers))
            self.log.info("Cheapest Carriers List  from the carriers List from UI :" + str(
                cheapestCarriersFromTopOfTheCarrierList))

            if cheapestCarriersFromTopOfTheCarrierList == cheapestCarriers:
                self.log.info("Cheapest carrier(s) ordering is correct ")
                self.log.info("Cheapest Carriers List : " + str(cheapestCarriers))
                return cheapestCarrierStatus and True
            else:
                self.log.info("Cheapest carrier(s) ordering is not correct")
                return cheapestCarrierStatus and False

        except Exception as e:
            self.log.error("Method validateCheapestCarrier : Not able to find the cheapest carrier or passed locator "
                           "may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def selectCarrierByNameOrIndex(self, carrierNameOrIndex, isInsurancePresent):
        """
        Method to search the carrier and click on the carrier either by name  or index.
        :param isInsurancePresent: it represent whether want to select insurance rate or without insurance rate
        :param carrierNameOrIndex: Name of the carrier or Index
        """
        self.log.info("Inside selectCarrierByNameOrIndex Method")
        try:
            setFlag = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=10)

            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
                if not self.isElementPresent(
                        self._pageloadwait_listingcarriers, locatorType="xpath") and not \
                        self.isElementPresent(self._nocarriers_element, "id"):
                    setFlag = setFlag and True
                else:
                    setFlag = setFlag and False
            elif self.isElementPresent(self._nocarriers_element, "id"):
                self.log.debug("Carrier List not found, display message as No carriers found")
                setFlag = setFlag and False

            self.log.info("setflag value : " + str(setFlag))

            self.log.info("Parameter (carrierNameOrIndex) Value: " + str(carrierNameOrIndex))
            self.log.info("Parameter (isInsurancePresent) Value: " + str(isInsurancePresent))

            if setFlag:
                parameterStatus = isinstance(carrierNameOrIndex, str)
                self.log.info("parameterStatus: " + str(parameterStatus))

                finalParameterStatus = True
                if parameterStatus:
                    finalParameterStatus = carrierNameOrIndex.lstrip("-").isnumeric()

                self.log.info("finalParameterStatus: " + str(finalParameterStatus))
                if not finalParameterStatus:
                    self.log.info("Inside SelectCarrierByName Method")

                    elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
                    noOfCarriers = len(elementList)
                    self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))
                    lastCarrierIndex = int(noOfCarriers) - 1

                    if noOfCarriers != 0:
                        matchingCarrierValidationStatus = False
                        for i in range(noOfCarriers):
                            matchingCarrier = self._carrier_row + str(i) + self._carrierrow_name
                            carrierDisplayed = self.getText(matchingCarrier, "xpath")

                            if carrierDisplayed.lower() == carrierNameOrIndex.lower():
                                matchingCarrierValidationStatus = True
                                self.log.info(
                                    "matchingCarrierValidationStatus inside" + str(matchingCarrierValidationStatus))
                                self.log.info(
                                    "The carrier " + str(carrierNameOrIndex) + " is present in the carrier list")
                                transit_days = self.getText(
                                    self._carrier_row + str(i) + "']" + self._carrier_transitdays,
                                    locatorType="xpath")
                                appointment_dates = self.getText(
                                    self._carrier_row + str(i) + "']" + self._carrier_appointment_dates,
                                    locatorType="xpath")
                                finalList = self.totalAmountBreakup(i, isInsurancePresent)
                                self.log.info("The carrier index to click SELECT : " + str(i))
                                carrierSelect = self._carrier_select_buttons + str(i)
                                self.waitForElement(carrierSelect, locatorType="id")
                                self.pageVerticalScroll(carrierSelect, "id")
                                self.elementClick(carrierSelect, locatorType="id")
                                createOrderButtonEnabledStatus = self.reqop.verifyCreateOrderButtonEnabled()
                                if createOrderButtonEnabledStatus:
                                    self.log.info("The Create Order Button is Enabled")
                                else:
                                    self.log.debug("The Create Order Button is Disabled")

                                finalList.append(carrierNameOrIndex)
                                finalList.append(transit_days)
                                finalList.append(appointment_dates)
                                self.log.info("Final List " + str(finalList))
                                return finalList

                        self.log.info(
                            "matchingCarrierValidationStatus after for loop" + str(matchingCarrierValidationStatus))
                        if not matchingCarrierValidationStatus:
                            carrierSelect = self._carrier_select_buttons + str(lastCarrierIndex)
                            self.waitForElement(carrierSelect, locatorType="id")
                            carrierNameFromIndex = self.getText(self._carrier_row + str(lastCarrierIndex) +
                                                                self._carrierrow_name, "xpath")
                            self.log.info("Carrier Name from the Index :" + str(carrierNameFromIndex))
                            transit_days = self.getText(
                                self._carrier_row + str(lastCarrierIndex) + "']" + self._carrier_transitdays,
                                locatorType="xpath")
                            appointment_dates = self.getText(
                                self._carrier_row + str(lastCarrierIndex) + "']" + self._carrier_appointment_dates,
                                locatorType="xpath")
                            finalList = self.totalAmountBreakup(lastCarrierIndex, isInsurancePresent)
                            self.pageVerticalScroll(carrierSelect, "id")
                            self.elementClick(carrierSelect, locatorType="id")
                            createOrderButtonEnabledStatus = self.reqop.verifyCreateOrderButtonEnabled()
                            if createOrderButtonEnabledStatus:
                                self.log.info("The Create Order Button is Enabled")
                            else:
                                self.log.debug("The Create Order Button is Disabled")
                            finalList.append(carrierNameFromIndex)
                            finalList.append(transit_days)
                            finalList.append(appointment_dates)
                            self.log.info("Final List " + str(finalList))
                            return finalList

                    else:
                        self.log.error("No carrier(s) found in the carrier section.")

                else:
                    self.log.info("Inside SelectCarrierByIndex Method")
                    elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
                    noOfCarriers = len(elementList)
                    self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))
                    carrierIndexNumber = int(carrierNameOrIndex)
                    lastCarrierIndex = int(noOfCarriers) - 1

                    if noOfCarriers != 0:
                        if 0 <= carrierIndexNumber <= lastCarrierIndex:
                            self.log.info("Select the carrier based on the positive index")
                            carrierSelect = self._carrier_select_buttons + str(carrierNameOrIndex)
                            self.waitForElement(carrierSelect, locatorType="id")
                            carrierNameFromIndex = self.getText(self._carrier_row + str(carrierNameOrIndex) +
                                                                self._carrierrow_name, "xpath")
                            self.log.info("Carrier Name from the Index :" + str(carrierNameFromIndex))
                            transit_days = self.getText(
                                self._carrier_row + str(carrierNameOrIndex) + "']" + self._carrier_transitdays,
                                locatorType="xpath")
                            appointment_dates = self.getText(
                                self._carrier_row + str(carrierNameOrIndex) + "']" + self._carrier_appointment_dates,
                                locatorType="xpath")
                            finalList = self.totalAmountBreakup(carrierNameOrIndex, isInsurancePresent)
                            self.pageVerticalScroll(carrierSelect, "id")
                            self.elementClick(carrierSelect, locatorType="id")
                            createOrderButtonEnabledStatus = self.reqop.verifyCreateOrderButtonEnabled()
                            if createOrderButtonEnabledStatus:
                                self.log.info("The Create Order Button is Enabled")
                            else:
                                self.log.debug("The Create Order Button is Disabled")
                            finalList.append(carrierNameFromIndex)
                            finalList.append(transit_days)
                            finalList.append(appointment_dates)
                            self.log.info("Final List " + str(finalList))
                            return finalList
                        elif carrierIndexNumber < 0:
                            self.log.info("Select the carrier based on the negative index")
                            self.log.info("Index passed : " + str(carrierNameOrIndex))
                            negativeCarrierIndex = int(lastCarrierIndex) + int(carrierNameOrIndex)
                            carrierSelect = self._carrier_select_buttons + str(negativeCarrierIndex)
                            self.waitForElement(carrierSelect, locatorType="id")
                            carrierNameFromIndex = self.getText(self._carrier_row + str(negativeCarrierIndex) +
                                                                self._carrierrow_name, "xpath")
                            self.log.info("Carrier Name from the Index :" + str(carrierNameFromIndex))
                            transit_days = self.getText(
                                self._carrier_row + str(negativeCarrierIndex) + "']" + self._carrier_transitdays,
                                locatorType="xpath")
                            appointment_dates = self.getText(
                                self._carrier_row + str(negativeCarrierIndex) + "']" + self._carrier_appointment_dates,
                                locatorType="xpath")
                            finalList = self.totalAmountBreakup(carrierNameOrIndex, isInsurancePresent)
                            self.pageVerticalScroll(carrierSelect, "id")
                            self.elementClick(carrierSelect, locatorType="id")
                            createOrderButtonEnabledStatus = self.reqop.verifyCreateOrderButtonEnabled()
                            if createOrderButtonEnabledStatus:
                                self.log.info("The Create Order Button is Enabled")
                            else:
                                self.log.debug("The Create Order Button is Disabled")
                            finalList.append(carrierNameFromIndex)
                            finalList.append(transit_days)
                            finalList.append(appointment_dates)
                            self.log.info("Final List " + str(finalList))
                            return finalList
                        else:
                            self.log.info("Select the last carrier from the carrier list.")
                            self.log.info("Index passed : " + str(carrierNameOrIndex))
                            carrierSelect = self._carrier_select_buttons + str(lastCarrierIndex)
                            self.waitForElement(carrierSelect, locatorType="id")
                            carrierNameFromIndex = self.getText(self._carrier_row + str(carrierNameOrIndex) +
                                                                self._carrierrow_name, "xpath")
                            self.log.info("Carrier Name from the Index :" + str(carrierNameFromIndex))
                            transit_days = self.getText(
                                self._carrier_row + str(carrierNameOrIndex) + "']" + self._carrier_transitdays,
                                locatorType="xpath")
                            appointment_dates = self.getText(
                                self._carrier_row + str(carrierNameOrIndex) + "']" + self._carrier_appointment_dates,
                                locatorType="xpath")
                            finalList = self.totalAmountBreakup(carrierNameOrIndex, isInsurancePresent)
                            self.pageVerticalScroll(carrierSelect, "id")
                            self.elementClick(carrierSelect, locatorType="id")
                            createOrderButtonEnabledStatus = self.reqop.verifyCreateOrderButtonEnabled()
                            if createOrderButtonEnabledStatus:
                                self.log.info("The Create Order Button is Enabled")
                            else:
                                self.log.debug("The Create Order Button is Disabled")
                            finalList.append(carrierNameFromIndex)
                            finalList.append(transit_days)
                            finalList.append(appointment_dates)
                            self.log.info("Final List " + str(finalList))
                            return finalList
                    else:
                        self.log.error("No carrier(s) found in the carrier section.")
            else:
                self.log.error("No carrier(s) found in the carrier section, setFlag value : " + str(setFlag))

        except Exception as e:
            self.log.error("Method selectCarrierByNameOrIndex : The passed carrier name/index " + str(
                carrierNameOrIndex) + " is wrong")
            self.log.error("Exception" + str(e))

    def validateReRateWithSiteType(self, originSiteType, destinationSiteType, originSiteTypeBreakup,
                                   destinationSiteTypeBreakup):
        """
        Method to validate rerate functionality with site type section.
        """
        self.log.info("Inside validateReRateWithSiteType Method")
        try:
            createOrderButtonStatusResult = True
            totalAmtVadtnWithInsuranceResult = True
            totalAmtVadtnWithoutInsuranceResult = True
            siteTypeStatusResult = True
            siteTypeStatus = True
            siteTypePairValidationList = []
            siteTypeNotPresentList = []
            siteTypeNotPresentListWithoutInsurance = []

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            self.log.info("origin SiteType : " + str(originSiteType))
            self.log.info("destination SiteType : " + str(destinationSiteType))
            if not originSiteTypeBreakup == "":
                siteTypePairValidationList.append(originSiteTypeBreakup)
            if not destinationSiteTypeBreakup == "":
                siteTypePairValidationList.append(destinationSiteTypeBreakup)
            self.log.info("siteTypePairValidationList : " + str(siteTypePairValidationList))

            # Making to precondition.
            selectedAccessorials = self.reqop.selectedAccessorials(True)
            self.log.info("Inside validateReRate Method : selectedAccessorials " + str(selectedAccessorials))
            if len(selectedAccessorials) > 0:
                for accessorialfromlist in selectedAccessorials:
                    self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display",
                                    timeout=15)
                if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                    self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                    # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                    #                     timeout=50)
                    self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                        timeout=180)

            # Rearte 1 with Site Type Combination
            self.log.info("Rerate: Selecting Site Type Pair")
            self.rerdp.selectSiteType(originSiteType, "origin")
            self.rerdp.selectSiteType(destinationSiteType, "destination")
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # Re-rate 1 CreateOrder Button Validation
            createOrderButtonStatus = self.reqop.verifyCreateOrderButtonEnabled()
            createOrderButtonStatusResult = createOrderButtonStatusResult and not createOrderButtonStatus

            # Re-rate 1 List preparation for Validation.
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list After Rerate:" + str(noOfCarriers))

            if noOfCarriers > 0:
                # Validating breakup accessorials ,breakup sum of cost & revenue for all carriers with & without
                # insurance (rerate 1)
                for startIndex in range(noOfCarriers):
                    # Total Amount Breakup with insurance
                    totalAmtPopupListWithInsurance = self.totalAmountBreakup(startIndex, "yes")
                    # Total Amount Breakup without insurance
                    totalAmtPopupListWithoutInsurance = self.totalAmountBreakup(startIndex, "no")

                    for siteTypeFromList in siteTypePairValidationList:
                        if siteTypeFromList in totalAmtPopupListWithInsurance[2]:
                            siteTypeStatus = siteTypeStatus and True
                        else:
                            siteTypeStatus = siteTypeStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_name, "xpath")
                            self.log.warning("The following site type " + str(
                                siteTypeFromList) + " was not present at the Index " + str(
                                startIndex) + " for the carrier " + str(
                                carrierNameFromIndex) + " with insurance popup.")
                            # site type List
                            siteTypeNotPresentList.append(siteTypeFromList)

                        if siteTypeFromList in totalAmtPopupListWithoutInsurance[2]:
                            siteTypeStatus = siteTypeStatus and True
                        else:
                            siteTypeStatus = siteTypeStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_name, "xpath")
                            self.log.warning("The following site type " + str(
                                siteTypeFromList) + " was not present at the Index " + str(
                                startIndex) + " for the carrier " + str(
                                carrierNameFromIndex) + " without insurance popup.")
                            # site type List
                            siteTypeNotPresentListWithoutInsurance.append(siteTypeFromList)

                    totalAmtVadtnWithInsurance = self.validateTotalCostAndRevenue(totalAmtPopupListWithInsurance)
                    totalAmtVadtnWithInsuranceResult = totalAmtVadtnWithInsuranceResult and totalAmtVadtnWithInsurance

                    totalAmtVadtnWithoutInsurance = self.validateTotalCostAndRevenue(totalAmtPopupListWithoutInsurance)
                    totalAmtVadtnWithoutInsuranceResult = \
                        totalAmtVadtnWithoutInsuranceResult and totalAmtVadtnWithoutInsurance
                    siteTypeStatusResult = siteTypeStatusResult and siteTypeStatus
                    self.log.info("siteTypeStatusResult Status for the carrier : " + str(siteTypeStatusResult))

                self.log.info("After Re-rate : totalAmtVadtnWithInsuranceResult for all carriers " +
                              str(totalAmtVadtnWithInsuranceResult))
                self.log.info("After Re-rate : totalAmtVadtnWithoutInsuranceResult for all carriers " +
                              str(totalAmtVadtnWithoutInsuranceResult))
                totalAmtVadtnStatusResult = totalAmtVadtnWithInsuranceResult and totalAmtVadtnWithoutInsuranceResult

                self.log.info("Total no. of Carriers in the carrier list After rerate:" + str(noOfCarriers))
                if not siteTypeStatusResult:
                    self.log.info(
                        "All Site Type List which is not present in the breakup of with insurance : " + str(
                            siteTypeNotPresentList))
                    self.log.info(
                        "All Site Type List which is not present in the breakup of without insurance : " + str(
                            siteTypeNotPresentListWithoutInsurance))

                    if siteTypeNotPresentList == siteTypeNotPresentListWithoutInsurance:
                        uniqueSiteType = set(siteTypeNotPresentList)
                        self.log.info(
                            "Unique Site Type List which is not present in the breakup : " + str(uniqueSiteType))

                        siteTypeStatusResult = not siteTypeStatusResult
                        for siteTypeFromList in uniqueSiteType:
                            count = siteTypeNotPresentList.count(siteTypeFromList)
                            self.log.info("The no. of times the site type " + str(
                                siteTypeFromList) + " not present in the breakup is : " + str(count))

                            if count == noOfCarriers:
                                siteTypeStatusResult = siteTypeStatusResult and False
                                self.log.info("Status siteTypeStatusResult: " + str(siteTypeStatusResult))
                            else:
                                siteTypeStatusResult = siteTypeStatusResult and True
                                self.log.info("Status siteTypeStatusResult: " + str(siteTypeStatusResult))

                self.log.info("Status Final totalAmtValidation: " + str(totalAmtVadtnStatusResult))
                self.log.info("Status Final createOrderButtonStatusResult: " + str(createOrderButtonStatusResult))
                self.log.info("Status Final siteTypeStatus: " + str(siteTypeStatusResult))

                # Final Validation of rerate for site type
                if totalAmtVadtnStatusResult and createOrderButtonStatusResult and siteTypeStatusResult:
                    self.log.info("Rerate happened with site type .")
                    return True
                else:
                    self.log.error("Rerate not happened with site types,Origin : " + str(originSiteType) +
                                   " and Destination : " + str(destinationSiteType))
                    return False
            else:
                self.log.error("Rerate not happened with site types,Origin : " + str(originSiteType) +
                               " and Destination : " + str(destinationSiteType) + " and the no. of carriers was zero.")
                return False

        except Exception as e:
            self.log.error("Method validateReRateWithSiteType : Rerate didnot happened based on the site type.")
            self.log.error("Exception" + str(e))

    def validateReRateWithAccessorials(self):
        """
        Method to validate rerate functionality with accessorials section.
        """
        self.log.info("Inside validateReRateWithAccessorials Method")
        try:
            createOrderButtonStatusResult = True
            reRateStatusResult = True
            _ssaccessorialList = []
            _oaccessorialList = []
            _daccessorialList = []

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # Making to precondition.
            selectedAccessorials = self.reqop.selectedAccessorials(True)
            self.log.info("Inside validateReRate Method : selectedAccessorials " + str(selectedAccessorials))
            self.pageVerticalScroll(self.reqop.acc_destination, "id")
            if len(selectedAccessorials) > 0:
                for accessorialfromlist in selectedAccessorials:
                    self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)

            # self.pageVerticalScroll(self.rerdp._origin_siteType_Business , "xpath")
            self.rerdp.selectSiteType("business", "origin")
            self.rerdp.selectSiteType("business", "destination")
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            _allAccessorialList = self.csvRowDataToList("allaccessorials", "defaultlist.csv")
            for accessorial in _allAccessorialList:
                if "origin" not in accessorial and "destination" not in accessorial:
                    _ssaccessorialList.append(accessorial)
                elif "origin" in accessorial and "destination" not in accessorial:
                    _oaccessorialList.append(accessorial)
                elif "origin" not in accessorial and "destination" in accessorial:
                    _daccessorialList.append(accessorial)

            _ssaccessorialValidationList = self.accessorialBreakupValidationList(_ssaccessorialList)
            _oriaccessorialValidationList = self.accessorialBreakupValidationList(_oaccessorialList)
            _desaccessorialValidationList = self.accessorialBreakupValidationList(_daccessorialList)

            _accessorialList = _oaccessorialList + _daccessorialList
            self.log.info("ss : _accessorialList " + str(_accessorialList))
            _accessorialList.remove('destinationnotifypriortoarrival')
            _ssaccessorialNotValidationList = self.accessorialBreakupValidationList(_accessorialList)

            _accessorialList = _ssaccessorialList + _daccessorialList
            self.log.info("ori : _accessorialList " + str(_accessorialList))
            _accessorialList.remove('destinationnotifypriortoarrival')
            _oaccessorialNotValidationList = self.accessorialBreakupValidationList(_accessorialList)

            _accessorialList = _ssaccessorialList + _oaccessorialList
            self.log.info("des : _accessorialList " + str(_accessorialList))
            _daccessorialNotValidationList = self.accessorialBreakupValidationList(_accessorialList)

            self.log.info("_allAccessorialList " + str(_allAccessorialList))

            self.log.info("_ssaccessorialclickingList " + str(_ssaccessorialList))
            self.log.info("_ssaccessorialValidationList " + str(_ssaccessorialValidationList))
            self.log.info("_ssaccessorialNotValidationList " + str(_ssaccessorialNotValidationList))

            self.log.info("_oaccessorialclickingList " + str(_oaccessorialList))
            self.log.info("_oriaccessorialValidationList " + str(_oriaccessorialValidationList))
            self.log.info("_oaccessorialNotValidationList " + str(_oaccessorialNotValidationList))

            self.log.info("_daccessorialclickingList " + str(_daccessorialList))
            self.log.info("_desaccessorialValidationList " + str(_desaccessorialValidationList))
            self.log.info("_daccessorialNotValidationList " + str(_daccessorialNotValidationList))

            # Rerate 1 with Special Services
            self.log.info("Rerate 1: Clicking Special Services Accessorials")
            for accessorialfromlist in _ssaccessorialList:
                self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # ReRate 1 Create Order Button Validation
            createOrderButtonStatus = self.reqop.verifyCreateOrderButtonEnabled()
            createOrderButtonStatusResult = createOrderButtonStatusResult and not createOrderButtonStatus

            # Rearate 1 List preparation for Validation.
            rerateStatus = self.reRateListValidationOfAccessorial(_ssaccessorialValidationList,
                                                                  _ssaccessorialNotValidationList)
            self.log.info("Status After 1st Re-rate " + str(rerateStatus))
            reRateStatusResult = reRateStatusResult and rerateStatus

            # Unchecking the special services accessorials - Pre-condition for Rerate 2
            if len(_ssaccessorialList) > 0:
                for accessorialfromlist in _ssaccessorialList:
                    self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)

            self.pageVerticalScroll(self.reqop.acc_origin, "id")
            # Rerate 2 with Origin Accessorials
            self.log.info("Re-rate 2: Clicking Origin Accessorials")
            for accessorialfromlist in _oaccessorialList:
                self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # ReRate 2 Create Order Button Validation
            createOrderButtonStatus = self.reqop.verifyCreateOrderButtonEnabled()
            createOrderButtonStatusResult = createOrderButtonStatusResult and not createOrderButtonStatus

            # Rearate 2 List preparation for Validation.
            rerateStatus = self.reRateListValidationOfAccessorial(_oriaccessorialValidationList,
                                                                  _oaccessorialNotValidationList)
            self.log.info("Status After 2nd Rerate " + str(rerateStatus))
            reRateStatusResult = reRateStatusResult and rerateStatus

            self.pageVerticalScroll(self.reqop.acc_origin, "id")
            # Unchecking the origin accessorials - Pre-condition for Rerate 3
            if len(_oaccessorialList) > 0:
                for accessorialfromlist in _oaccessorialList:
                    self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)

            self.pageVerticalScroll(self.reqop.acc_destination, "id")
            # Rerate 3 with Destination Accessorials
            self.log.info("Rerate 3: Clicking Destination Accessorials")
            # Once rates are coming with tradeshow accessorial below line of code will get removed.
            _daccessorialList.remove('destinationtradeshowdelivery')
            for accessorialfromlist in _daccessorialList:
                self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # ReRate 3 Create Order Button Validation
            createOrderButtonStatus = self.reqop.verifyCreateOrderButtonEnabled()
            createOrderButtonStatusResult = createOrderButtonStatusResult and not createOrderButtonStatus

            # Rearate 3 List preparation for Validation.
            # Once rates are coming with tradeshow accessorial below line of code will get removed.
            _desaccessorialValidationList.remove('Tradeshow Delivery')
            rerateStatus = self.reRateListValidationOfAccessorial(_desaccessorialValidationList,
                                                                  _daccessorialNotValidationList)
            self.log.info("Return Status After 3rd Rerate " + str(rerateStatus))
            reRateStatusResult = reRateStatusResult and rerateStatus

            self.pageVerticalScroll(self.reqop.acc_destination, "id")
            # Unchecking the destination accessorials
            if len(_daccessorialList) > 0:
                for accessorialfromlist in _daccessorialList:
                    self.reqop.clickingAccessorialsCheckbox(accessorialfromlist)

            # Final Validation of rerate for accessorials
            if reRateStatusResult and createOrderButtonStatusResult:
                self.log.info("Rerate happened with accessorials .")
                return True
            else:
                self.log.error("Rerate not happened with accessorials.")
                return False

        except Exception as e:
            self.log.error("Method validateReRateWithAccessorials : Rerate didnot happened based on the accessorials.")
            self.log.error("Exception" + str(e))

    def reRateListValidationOfAccessorial(self, accessorialValidationList, accessorialNotValidationList):
        """
        Method to .validate accessorials break up are present or not and validating total cost & revenue in the popup
        :param accessorialValidationList accessorial breakup list to check in the total amount popup.
        :param accessorialNotValidationList accessorial breakup should not present in the total amount popup.
        """
        self.log.info("Inside reRateListValidationOfAccessorial Method")
        try:
            totalAmtVadtnWithInsuranceResult = True
            totalAmtVadtnWithoutInsuranceResult = True
            accPresentStatus = True
            accNotPresentStatus = True
            accPresentStatusResult = True
            accNotPresentStatusResult = True
            accNotPresentList = []
            accNotPresentListWithoutInsurance = []

            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriersAfterReRate = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list After rerate:" + str(noOfCarriersAfterReRate))

            if noOfCarriersAfterReRate > 0:
                # Validating breakup accessorials ,breakup sum of cost & revenue for all carriers with & without
                # insurance (rerate 2)
                for startIndex in range(noOfCarriersAfterReRate):
                    # Total Amount Breakup with insurance
                    totalAmtPopupListWithInsurance = self.totalAmountBreakup(startIndex, "yes")
                    # Total Amount Breakup without insurance
                    totalAmtPopupListWithoutInsurance = self.totalAmountBreakup(startIndex, "no")

                    for accessorialfromlist in accessorialValidationList:
                        if accessorialfromlist in totalAmtPopupListWithInsurance[2]:
                            accPresentStatus = accPresentStatus and True
                        else:
                            accPresentStatus = accPresentStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_name, "xpath")
                            self.log.warning("The following accessorial " + str(
                                accessorialfromlist) + " was not present at the Index " + str(
                                startIndex) + " after Rerate at with insurance for the carrier " + str(
                                carrierNameFromIndex))
                            # accessorial List
                            accNotPresentList.append(accessorialfromlist)

                        if accessorialfromlist in totalAmtPopupListWithoutInsurance[2]:
                            accPresentStatus = accPresentStatus and True
                        else:
                            accPresentStatus = accPresentStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_name, "xpath")
                            self.log.warning("The following accessorial " + str(
                                accessorialfromlist) + " was not present at the Index " + str(
                                startIndex) + " after Rerate at with insurance for the carrier " + str(
                                carrierNameFromIndex))
                            # accessorial List
                            accNotPresentListWithoutInsurance.append(accessorialfromlist)

                    for accessorialfromlist in accessorialNotValidationList:
                        if accessorialfromlist not in totalAmtPopupListWithInsurance[2]:
                            accNotPresentStatus = accNotPresentStatus and True
                        else:
                            accNotPresentStatus = accNotPresentStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_name, "xpath")
                            self.log.error("The following accessorial " + str(
                                accessorialfromlist) + " is  present at the Index " + str(
                                startIndex) + " after Re-rate at with insurance for the carrier " + str(
                                carrierNameFromIndex))

                        if accessorialfromlist not in totalAmtPopupListWithoutInsurance[2]:
                            accNotPresentStatus = accNotPresentStatus and True
                        else:
                            accNotPresentStatus = accNotPresentStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_name, "xpath")
                            self.log.error("The following accessorial " + str(
                                accessorialfromlist) + " is  present at the Index " + str(
                                startIndex) + " after Re-rate at with insurance for the carrier " + str(
                                carrierNameFromIndex))

                    totalAmtVadtnWithInsurance = self.validateTotalCostAndRevenue(totalAmtPopupListWithInsurance)
                    totalAmtVadtnWithInsuranceResult = totalAmtVadtnWithInsuranceResult and totalAmtVadtnWithInsurance

                    totalAmtVadtnWithoutInsurance = self.validateTotalCostAndRevenue(totalAmtPopupListWithoutInsurance)
                    totalAmtVadtnWithoutInsuranceResult = \
                        totalAmtVadtnWithoutInsuranceResult and totalAmtVadtnWithoutInsurance

                    accPresentStatusResult = accPresentStatusResult and accPresentStatus
                    self.log.info("AccPresent StatusAfterRerate " + str(accPresentStatusResult))

                    accNotPresentStatusResult = accNotPresentStatusResult and accNotPresentStatus
                    self.log.info("AccNotPresent StatusAfterRerate " + str(accNotPresentStatusResult))

                self.log.info("Total no. of Carriers in the carrier list After rerate:" + str(noOfCarriersAfterReRate))
                if not accPresentStatusResult:
                    self.log.info("All Accessorial List which is not present in the breakup of with insurance: " +
                                  str(accNotPresentList))
                    self.log.info("All Accessorial List which is not present in the breakup of without insurance : " +
                                  str(accNotPresentListWithoutInsurance))

                    if accNotPresentList == accNotPresentListWithoutInsurance:
                        uniqueAcc = set(accNotPresentList)
                        self.log.info("Unique Acessorial List which is not present in the breakup : " + str(uniqueAcc))

                        accPresentStatusResult = not accPresentStatusResult
                        for accessorialfromlist in uniqueAcc:
                            count = accNotPresentList.count(accessorialfromlist)
                            self.log.info("The no. of times the accessorial " + str(
                                accessorialfromlist) + " not present in the breakup is : " + str(count))

                            if count == noOfCarriersAfterReRate:
                                accPresentStatusResult = accPresentStatusResult and False
                                self.log.info("Status accPresentStatusResult : " + str(accPresentStatusResult))
                            else:
                                accPresentStatusResult = accPresentStatusResult and True
                                self.log.info("Status accPresentStatusResult : " + str(accPresentStatusResult))

                self.log.info("After Re-rate : totalAmtVadtnWithInsuranceResult for all carriers " +
                              str(totalAmtVadtnWithInsuranceResult))
                self.log.info("After Re-rate : totalAmtVadtnWithoutInsuranceResult for all carriers " +
                              str(totalAmtVadtnWithoutInsuranceResult))
                self.log.info("After Re-rate (accPresentStatusResult): FinalStatus of accessorials present for"
                              " all carriers " + str(accPresentStatusResult))
                self.log.info("After Re-rate (accNotPresentStatusResult): FinalStatus of accessorials not present for"
                              " all carriers " + str(accNotPresentStatusResult))

                if totalAmtVadtnWithInsuranceResult and totalAmtVadtnWithoutInsuranceResult and accPresentStatusResult \
                        and accNotPresentStatusResult:
                    return True
                else:
                    return False
            else:
                self.log.error("No. of carriers is zero after Re-Rate.")
                return False

        except Exception as e:
            self.log.error(
                "Method reRateListValidationOfAccessorial : Rerate validation for accessorials didnot happened.")
            self.log.error("Exception" + str(e))

    def validateReRateWithZipCodes(self):
        """
        Method to validate rerate functionality with zip codes section.
        """
        self.log.info("Inside validateReRateWithZipCodes Method")
        try:
            createOrderButtonStatusResult = True
            CostOrRevenueChangedStatusResult = True

            allCarriersTotalCostListWithInsuranceBeforeReRate = []
            allCarriersTotalRevenueListWithInsuranceBeforeReRate = []
            allCarriersTotalCostListWithoutInsuranceBeforeReRate = []
            allCarriersTotalRevenueListWithoutInsuranceBeforeReRate = []

            allCarriersTotalCostListWithInsuranceAfterReRate = []
            allCarriersTotalRevenueListWithInsuranceAfterReRate = []
            allCarriersTotalCostListWithoutInsuranceAfterReRate = []
            allCarriersTotalRevenueListWithoutInsuranceAfterReRate = []

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list before rerate:" + str(noOfCarriers))

            # Validating breakup sum of cost & revenue for all carriers with & without insurance
            for startIndex in range(noOfCarriers):
                # Total Amount Breakup with insurance
                totalAmtPopupListWithInsurance = self.totalAmountBreakup(startIndex, "yes")
                allCarriersTotalCostListWithInsuranceBeforeReRate.append(totalAmtPopupListWithInsurance[0])
                allCarriersTotalRevenueListWithInsuranceBeforeReRate.append(totalAmtPopupListWithInsurance[1])

                # Total Amount Breakup without insurance
                totalAmtPopupListWithoutInsurance = self.totalAmountBreakup(startIndex, "no")
                allCarriersTotalCostListWithoutInsuranceBeforeReRate.append(totalAmtPopupListWithoutInsurance[0])
                allCarriersTotalRevenueListWithoutInsuranceBeforeReRate.append(totalAmtPopupListWithoutInsurance[1])

            # ReRate happening (Rerate 1)
            selected_textValue = self.getText(self.rerdp.origin_textfield_value, locatorType="id")
            self.log.info("Value in origin input field" + str(selected_textValue))
            row = self.reRateData[0]
            if selected_textValue != self._rerate_origin:
                self.rerdp.directSearchAndSelectAddress(row["OriginZip"], "origin")
            else:
                self.rerdp.directSearchAndSelectAddress(row["DestinationZip"], "origin")

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # Create Order Button Validation (Rerate 1)
            createOrderButtonStatus = self.reqop.verifyCreateOrderButtonEnabled()
            createOrderButtonStatusResult = createOrderButtonStatusResult and not createOrderButtonStatus

            # Validation of Total Cost,Total Revenue,Breakup Sum of Cost,Breakup Sum of Revenue for
            # with & without insurance for all carriers(Rerate)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriersAfterReRate = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list After rerate: " + str(noOfCarriersAfterReRate))

            if noOfCarriersAfterReRate > 0:
                for startIndex in range(noOfCarriersAfterReRate):
                    # Total Amount Breakup with insurance
                    totalAmtPopupListWithInsurance = self.totalAmountBreakup(startIndex, "yes")
                    allCarriersTotalCostListWithInsuranceAfterReRate.append(totalAmtPopupListWithInsurance[0])
                    allCarriersTotalRevenueListWithInsuranceAfterReRate.append(totalAmtPopupListWithInsurance[1])

                    # Total Amount Breakup without insurance
                    totalAmtPopupListWithoutInsurance = self.totalAmountBreakup(startIndex, "no")
                    allCarriersTotalCostListWithoutInsuranceAfterReRate.append(totalAmtPopupListWithoutInsurance[0])
                    allCarriersTotalRevenueListWithoutInsuranceAfterReRate.append(totalAmtPopupListWithoutInsurance[1])

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalCostListWithInsuranceBeforeReRate,
                    allCarriersTotalCostListWithInsuranceAfterReRate)
                self.log.info("After Re-rate : Cost With Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalRevenueListWithInsuranceBeforeReRate,
                    allCarriersTotalRevenueListWithInsuranceAfterReRate)
                self.log.info(
                    "After Re-rate : Revenue With Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalCostListWithoutInsuranceBeforeReRate,
                    allCarriersTotalCostListWithoutInsuranceAfterReRate)
                self.log.info(
                    "After Re-rate : Cost Without Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalRevenueListWithoutInsuranceBeforeReRate,
                    allCarriersTotalRevenueListWithoutInsuranceAfterReRate)
                self.log.info(
                    "After Re-rate : Revenue Without Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                self.log.info("Status Final createOrderButtonStatusResult: " + str(createOrderButtonStatusResult))
                self.log.info("Status Final CostOrRevenueChangedStatus: " + str(CostOrRevenueChangedStatusResult))

                if createOrderButtonStatusResult and CostOrRevenueChangedStatusResult:
                    self.log.info("Re-rate happened with zip codes.")
                    return True
                else:
                    self.log.info("Re-rate didnot happened with zip codes.")
                    return False
            else:
                self.log.info(
                    "Re-rate didnot happened with zip codes and the no.of carrier's listed after rerate was zero.")
                return False

        except Exception as e:
            self.log.error("Method validateReRateWithZipCodes : Rerate didnot happened.")
            self.log.error("Exception" + str(e))

    def validateReRateWithShippingItems(self):
        """
        Method to validate rerate functionality with Shipping Items section.
        """
        self.log.info("Inside validateReRateWithShippingItems Method")
        try:

            createOrderButtonStatusResult = True
            CostOrRevenueChangedStatusResult = True

            allCarriersTotalCostListWithInsuranceBeforeReRate = []
            allCarriersTotalRevenueListWithInsuranceBeforeReRate = []
            allCarriersTotalCostListWithoutInsuranceBeforeReRate = []
            allCarriersTotalRevenueListWithoutInsuranceBeforeReRate = []

            allCarriersTotalCostListWithInsuranceAfterReRate = []
            allCarriersTotalRevenueListWithInsuranceAfterReRate = []
            allCarriersTotalCostListWithoutInsuranceAfterReRate = []
            allCarriersTotalRevenueListWithoutInsuranceAfterReRate = []

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementDisplayed(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list before rerate:" + str(noOfCarriers))

            # Validating breakup sum of cost & revenue for all carriers with & without insurance
            for startIndex in range(noOfCarriers):
                # Total Amount Breakup with insurance
                totalAmtPopupListWithInsurance = self.totalAmountBreakup(startIndex, "yes")
                allCarriersTotalCostListWithInsuranceBeforeReRate.append(totalAmtPopupListWithInsurance[0])
                allCarriersTotalRevenueListWithInsuranceBeforeReRate.append(totalAmtPopupListWithInsurance[1])

                # Total Amount Breakup without insurance
                totalAmtPopupListWithoutInsurance = self.totalAmountBreakup(startIndex, "no")
                allCarriersTotalCostListWithoutInsuranceBeforeReRate.append(totalAmtPopupListWithoutInsurance[0])
                allCarriersTotalRevenueListWithoutInsuranceBeforeReRate.append(totalAmtPopupListWithoutInsurance[1])

            # ReRate happening (Rerate 1)
            self.resi.editShippingItem(1)
            self.waitForElement(self.resi.si_save_button, "id", "display")
            pieceValueInUI = self.resi.getShippingItemsData("Piece")
            weightValueInUI = self.resi.getShippingItemsData("weight")
            self.log.info("Piece from UI " + str(pieceValueInUI))
            self.log.info("Weight from UI " + str(weightValueInUI))

            reRatePieceValueInUI = int(pieceValueInUI) * 5
            reRateWeightValueInUI = int(weightValueInUI) * 5
            self.resi.si_EnterTextOnField("Piece", reRatePieceValueInUI)
            self.resi.si_EnterTextOnField("weight", reRateWeightValueInUI)
            self.log.info("Piece entered in UI " + str(reRatePieceValueInUI))
            self.log.info("Weight entered in UI " + str(reRateWeightValueInUI))

            self.pageVerticalScroll(self.resi.si_save_button, "id")
            self.resi.clickButton("save")
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)

            # Create Order Button Validation (Rerate 1)
            createOrderButtonStatus = self.reqop.verifyCreateOrderButtonEnabled()
            createOrderButtonStatusResult = createOrderButtonStatusResult and not createOrderButtonStatus

            # Validation of Total Cost,Total Revenue,Breakup Sum of Cost,Breakup Sum of Revenue for
            # with & without insurance for all carriers(Rerate)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriersAfterReRate = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list After rerate:" + str(noOfCarriersAfterReRate))

            if noOfCarriersAfterReRate > 0:
                for startIndex in range(noOfCarriersAfterReRate):
                    # Total Amount Breakup with insurance
                    totalAmtPopupListWithInsurance = self.totalAmountBreakup(startIndex, "yes")
                    allCarriersTotalCostListWithInsuranceAfterReRate.append(totalAmtPopupListWithInsurance[0])
                    allCarriersTotalRevenueListWithInsuranceAfterReRate.append(totalAmtPopupListWithInsurance[1])

                    # Total Amount Breakup without insurance
                    totalAmtPopupListWithoutInsurance = self.totalAmountBreakup(startIndex, "no")
                    allCarriersTotalCostListWithoutInsuranceAfterReRate.append(totalAmtPopupListWithoutInsurance[0])
                    allCarriersTotalRevenueListWithoutInsuranceAfterReRate.append(totalAmtPopupListWithoutInsurance[1])

                # Validating total cost' & total revenue's before and after rerate for all carriers.

                CostOrRevenueChangedStatus = \
                    self.validators.verify_list_match(allCarriersTotalCostListWithInsuranceBeforeReRate,
                                                      allCarriersTotalCostListWithInsuranceAfterReRate)
                self.log.info("After Re-rate : Cost With Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalRevenueListWithInsuranceBeforeReRate,
                    allCarriersTotalRevenueListWithInsuranceAfterReRate)
                self.log.info(
                    "After Re-rate : Revenue With Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalCostListWithoutInsuranceBeforeReRate,
                    allCarriersTotalCostListWithoutInsuranceAfterReRate)
                self.log.info(
                    "After Re-rate : Cost Without Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                CostOrRevenueChangedStatus = self.validators.verify_list_match(
                    allCarriersTotalRevenueListWithoutInsuranceBeforeReRate,
                    allCarriersTotalRevenueListWithoutInsuranceAfterReRate)
                self.log.info(
                    "After Re-rate : Revenue Without Insurance Changed Status " + str(CostOrRevenueChangedStatus))
                CostOrRevenueChangedStatusResult = CostOrRevenueChangedStatusResult and not CostOrRevenueChangedStatus

                self.log.info("Status Final createOrderButtonStatusResult: " + str(createOrderButtonStatusResult))
                self.log.info("Status Final CostOrRevenueChangedStatus: " + str(CostOrRevenueChangedStatusResult))

                if createOrderButtonStatusResult and CostOrRevenueChangedStatusResult:
                    self.log.info("Re-rate happened based on the inputs at Shipping Items section.")
                    return True
                else:
                    self.log.info("Re-rate did not based on the inputs at Shipping Items section.")
                    return False
            else:
                self.log.info("Re-rate didnot happened based on the Shipping Items section and no. of carriers "
                              "after rerate was zero.")
                return False

        except Exception as e:
            self.log.error(
                "Method validateReRateWithShippingItems : Rerate didnot happened based on the shipping items.")
            self.log.error("Exception" + str(e))

    def validateCarrierDownloadIcon(self):
        """
        Method to Validate carrier download icon is enabled or disabled.
        :return: Return True if carrier download is enabled or disabled.
        """
        self.log.info("Inside validateCarrierDownloadIcon Method")
        value = self.isElementPresent(self._carrier_downloadicon, locatorType="id")
        if value:
            self.log.info("Carrier Download Icon is enabled")
            return True
        else:
            self.log.info("Carrier Download Icon is disabled")
            return False

    def verifyCarrierDownloadIcon(self):
        """
        Method to carrier download icon is enabling and disabling.
        :return: Return True if carrier download icon behaviour is correct.
        """
        self.log.info("Inside verifyCarrierDownloadIcon Method")
        try:
            self.pageVerticalScroll(self._carrier_checkbox, "xpath")
            # Disabled
            carrierDownloadIconStatusResult0 = self.validateCarrierDownloadIcon()
            self.elementClick(self._carrier_checkbox, locatorType="xpath")
            # Enabled
            carrierDownloadIconStatusResult1 = self.validateCarrierDownloadIcon()
            self.elementClick(self._carrier_checkbox, locatorType="xpath")
            # Disabled
            carrierDownloadIconStatusResult2 = self.validateCarrierDownloadIcon()
            if not carrierDownloadIconStatusResult0 and carrierDownloadIconStatusResult1 and \
                    not carrierDownloadIconStatusResult2:
                self.log.info("Carrier Download Icon is getting enabled and disabled")
                return True
            else:
                self.log.info("Carrier Download Icon behaviour is wrong")
                return False

        except Exception as e:
            self.log.error("Method validateCarrierDownloadIcon : Carrier Download Icon is not clicked.")
            self.log.error("Exception" + str(e))

    def getSelectedCarrierData(self, noOfCarriersToSelect, indexToFetchCarrierData="0", insuranceSelection="yes"):
        """
        Method to click carrier checkboxes and fetch carrier data
        :param : noOfCarriersToSelect: no. of carriers in which we have to select the checkbox.
        :indexToFetchCarrierData : index from which carrier data to fetch.
        :insuranceSelection : to fetch the data of carrier with or without insurance
        :return : Carrier data if its fetched otherwise empty list if no carriers found.
        """
        self.log.info(f"Inside getSelectedCarrierData Method and the passed noOfCarriersToSelect = "
                      f"{str(noOfCarriersToSelect)} , indexToFetchCarrierData = {str(indexToFetchCarrierData)}, "
                      f"insuranceSelection = {str(insuranceSelection)}")
        try:
            carrierData = []
            # Selecting carriers checkboxes
            selectionResult = self.selectCarrierCheckbox(noOfCarriersToSelect, indexToFetchCarrierData,
                                                         insuranceSelection)
            self.log.info("selectionResult :" + str(selectionResult))

            if not selectionResult[0]:
                totalCostFromUI = ""
                totalRevenueFromUI = ""
                if selectionResult[1]:
                    if insuranceSelection.lower() == "yes":
                        totalCostFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][1]" + self._carrier_costlink
                        totalRevenueFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][1]" + self._carrier_revenuelink
                        radioButtonWithInsurance = self._carrier_row + str(
                            indexToFetchCarrierData) + "']" + self._carrier_withinsuranceradiobutton
                        self.pageVerticalScroll(radioButtonWithInsurance, "xpath")
                    elif insuranceSelection.lower() == "no":
                        totalCostFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][2]" + self._carrier_costlink
                        totalRevenueFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][2]" + self._carrier_revenuelink
                        radioButtonWithoutInsurance = self._carrier_row + str(
                            indexToFetchCarrierData) + "']" + self._carrier_withoutinsuranceradiobutton
                        self.pageVerticalScroll(radioButtonWithoutInsurance, "xpath")
                    else:
                        self.log.info(
                            "Passed parameter " + str(insuranceSelection) + " is not matching with the conditions")
                else:
                    self.log.info(
                        "Inside else block:Sufficient number carriers is not displayed,hence fetch first carrier data")
                    indexToFetchCarrierData = 0
                    if insuranceSelection.lower() == "yes":
                        totalCostFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][1]" + self._carrier_costlink
                        totalRevenueFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][1]" + self._carrier_revenuelink

                    elif insuranceSelection.lower() == "no":
                        totalCostFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][2]" + self._carrier_costlink
                        totalRevenueFromUI = self._carrier_costwithinsurance + str(
                            indexToFetchCarrierData) + "'][2]" + self._carrier_revenuelink
                    else:
                        self.log.info("Passed parameter " + str(insuranceSelection) + " is not matching with the "
                                                                                      "conditions")

                costHyperLink = self.getText(totalCostFromUI, "xpath")
                costHyperLinkUI = costHyperLink[5:-4]
                revenueHyperLink = self.getText(totalRevenueFromUI, "xpath")
                revenueHyperLinkUI = revenueHyperLink[:-4]
                carrierName = self.getText(self._carrier_row + str(indexToFetchCarrierData) + self._carrierrow_name,
                                           "xpath")
                appointment_dates = self.getText(self._carrier_row + str(indexToFetchCarrierData) + "']" +
                                                 self._carrier_appointment_dates, locatorType="xpath")

                carrierData.append(costHyperLinkUI)
                carrierData.append(revenueHyperLinkUI)
                carrierData.append(carrierName)
                carrierData.append(appointment_dates)
                carrierData.append(selectionResult[2])
            else:
                self.log.error("No Carrier(s) found.")
            self.log.info("carrierData for Saved Quotes : " + str(carrierData))
            return carrierData

        except Exception as e:
            self.log.error("Method getSelectedCarrierData and the exception is " + str(e))

    def selectCarrierCheckbox(self, noOfCarriersToSelect, indexToFetchCarrierData="0", insuranceSelection="yes"):
        """
        Method to click no. of carrier checkboxes based on the argument and from which index.
        :param : noOfCarriersToSelect: no. of carriers in which we have to select the checkbox.
        :indexToFetchCarrierData : index from which carrier data to fetch.
        :insuranceSelection : to fetch the data of carrier with or without insurance
        :return: nocarriers : Return True if no carriers found else False
        :return: carrierCondition : Return True if condition passes else False
        :return: carrierCheckboxSelectionIndexes : Return list containing the indexes of the carrier for which
        checkbox is selected.
        """
        self.log.info(f"Inside selectCarrierCheckbox Method and the passed noOfCarriersToSelect = "
                      f"{str(noOfCarriersToSelect)}, indexToFetchCarrierData = {str(indexToFetchCarrierData)}, "
                      f"insuranceSelection = {str(insuranceSelection)}")
        try:
            noCarriersDisplayed = False
            carrierCheckboxSelectionFlag = False
            selectedCarrierIndexValue = []
            setFlag = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display")
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=50)
                if not self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath") \
                        and not self.isElementPresent(self._nocarriers_element, "id"):
                    setFlag = True
                    self.log.debug("carrier(s) loaded within the timeout successfully.")
                else:
                    self.log.debug("carrier(s) did not load within the timeout.")
                    setFlag = False
            elif self.isElementPresent(self._nocarriers_element, "id"):
                self.log.debug("No carriers/results found")
                setFlag = False
            self.log.info("Final setFlag value : " + str(setFlag))

            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

            if setFlag:
                if noOfCarriers >= int(noOfCarriersToSelect) and noOfCarriers >= (int(indexToFetchCarrierData) + 1) \
                        and noOfCarriers >= (int(indexToFetchCarrierData) + int(noOfCarriersToSelect)):
                    self.pageVerticalScroll(self._carrier_checkbox, "xpath")
                    for index in range(int(noOfCarriersToSelect)):
                        self.elementClick(self._carrier_row + str(int(indexToFetchCarrierData) + index) +
                                          "'] //div[contains(@class,'Checkbox')]", locatorType="xpath")
                        selectedCarrierIndexValue.append(str(int(indexToFetchCarrierData) + index))
                    carrierCheckboxSelectionFlag = True
                else:
                    self.log.info("Inside else block:Sufficient number of carriers is not displayed,hence fetch "
                                  "first carrier data")
                    self.pageVerticalScroll(self._carrier_checkbox, "xpath")
                    self.elementClick(self._carrier_row + "0'] //div[contains(@class,'Checkbox')]", locatorType="xpath")
                    selectedCarrierIndexValue.append("0")
            else:
                self.log.error("No Carrier(s) found. or carriers did not load within the timeout.")
                noCarriersDisplayed = True

            return noCarriersDisplayed, carrierCheckboxSelectionFlag, selectedCarrierIndexValue
        except Exception as e:
            self.log.error("Method selectCarrierCheckbox and the exception is " + str(e))

    def carrierEmailLinkClick(self):
        """Clicking Carrier Email icon """
        try:
            self.log.info("Inside carrierEmailLinkClick Method")
            self.pageVerticalScroll(self._carrier_email, "css")
            self.elementClick(self._carrier_email, "css")
            self.wait_for_email_popup()
        except Exception as e:
            self.log.error("Exception occurred in carrierEmailLinkClick Method and Exception is : " + str(e))

    def wait_for_email_popup(self, timeout=10, action="display"):
        try:
            self.log.info("Inside wait_for_email_popup Method")
            self.waitForElement(self._carrier_email_popup, "css", event=action, timeout=timeout)
        except Exception as e:
            self.log.error("Exception occurred in Method and Exception is :" + str(e))

    def click_email_pop_up_button(self, buttonName):
        """Clicking the Button based on passed Argument"""
        try:
            self.log.info("Inside click_email_pop_up_button Method")
            if buttonName.lower() == "cancel":
                self.elementClick(self._email_cancel_button, "xpath")
                self.wait_for_email_popup(timeout=10, action="notdisplay")
                if not self.isElementPresent(self._carrier_email_popup, "css"):
                    return True
                else:
                    return False
            elif buttonName.lower() == "send":
                self.elementClick(self._email_send_button, "xpath")
                # Added time out due to popup not closed within time
                self.wait_for_email_popup(timeout=15, action="notdisplay")
                if not self.isElementPresent(self._carrier_email_popup, "css"):
                    return True
                else:
                    return False
        except Exception as e:
            self.log.error("Exception occurred in click_email_pop_up_button Method and Exception is : " + str(e))

    def enterEmailId(self, toField="uiAutomation.v09jltbo@mailosaur.io;", ccField=""):
        """Entering email to the email Field"""
        try:
            self.log.info("Inside enterEmailId Method")
            self.sendKeys(toField, self._email_to, "xpath")
            if not ccField == "":
                self.sendKeys(ccField, self._email_cc, "xpath")
        except Exception as e:
            self.log.error("Exception occurred in enterEmailId Method and Exception is : " + str(e))

    def validate_email_popup(self):
        """Validating the Email Popup"""
        try:
            self.log.info("Inside validate_email_popup Method")
            self.wait_for_email_popup()
            recipient_text = self.getText(self._recipient_text, "xpath")
            compare_value_1 = self.validators.verify_text_match(recipient_text, "To")
            copy_to_text = self.getText(self._copy_to_text, "xpath")
            compare_value_2 = self.validators.verify_text_match(copy_to_text, "Copy To")
            subject_text = self.getText(self._subject_text, "css")
            compare_value_3 = self.validators.verify_text_match(subject_text, "Subject")
            email_text = self.getText(self._email_text, "css")
            compare_value_4 = self.validators.verify_text_match(email_text, "Email Body")
            send_button_enabled = self.getAttribute(self._email_send_button, "xpath", "data-disabled")
            compare_value_5 = self.validators.verify_text_match(send_button_enabled, "false")
            cancel_button_enabled = self.getAttribute(self._email_cancel_button, "xpath", "data-disabled")
            compare_value_6 = self.validators.verify_text_match(cancel_button_enabled, "false")

            self.log.info("Compare result : for recipient_text: " + str(compare_value_1) + " for copy_to_text: " +
                          str(compare_value_2) + " for subject_text: " + str(compare_value_3) + " for email_text" +
                          str(compare_value_4) + " for send_button_enabled" + str(compare_value_5) +
                          "for cancel_button_enabled" + str(compare_value_6))
            if compare_value_1 and compare_value_2 and compare_value_3 and compare_value_4 and compare_value_5 and \
                    compare_value_6:
                return True
            else:
                return False

        except Exception as e:
            self.log.error("Exception occurred in validate_email_popup Method and exception is :" + str(e))

    def email_field_validation(self):
        """Validating the Email Field"""
        try:
            self.log.info("Inside email_field_validation Method")

            KeyCombination = Keys.CONTROL, "a", Keys.BACK_SPACE
            self.sendKeys(KeyCombination, self._email_to, "xpath")
            self.sendKeys(" ", self._email_to, "xpath")
            self.sendKeys("test.com", self._email_cc, "xpath")

            email_class = self.getAttribute(self._email_field, "xpath", "class")
            cc_class = self.getAttribute(self._cc_field, "xpath", "class")
            self.log.info("email class : " + str(email_class))
            self.log.info("CC class : " + str(cc_class))

            if "kstfjW" in email_class and cc_class:
                return True
            else:
                self.log.info("False")
                return False

        except Exception as e:
            self.log.error("Exception occurred in email_field_validation Method and exception is " + str(e))

    def click_and_validate_select_contact_pop_up(self, customer_name):
        """Clicking and Validating the Select Email Popup"""
        try:
            self.log.info("Inside click_and_validate_select_contact_pop_up Method")
            self.elementClick(self._recipient_text + "/following-sibling::div", "xpath")
            self.waitForElement(self._email_spinner, "xpath", "display")
            self.waitForElement(self._email_spinner, "xpath", "notdisplay", timeout=20)

            expectedText = "Email Contact List"
            contact_list_flag = self.validators.verify_text_match(self.getText(self._email_contact_list_header_value,
                                                                               "xpath"), expectedText)
            contact_flag = self.validators.verify_text_match(self.getText(self._email_contact_label_part + "1]",
                                                                          "xpath"), "Selected Contacts")
            recent_flag = self.validators.verify_text_match(self.getText(self._email_contact_label_part + "2]",
                                                                         "xpath"), "Recent Contacts")
            add_book_flag = self.validators.verify_text_match(self.getText(self._email_contact_label_part + "3]",
                                                                           "xpath"), "Address Book Contacts")

            # Validating contact list for customer
            customer_list_validation = self.validate_contact_list_customer_name(customer_name=customer_name)

            # Validating the selected email cancel Scenario
            self.elementClick(self._customer_select_email_popup, "xpath")

            self.elementClick(self._cancel_button_email_contact, "xpath")
            self.waitForElement(self._email_contact_list_header_value, "xpath", "notdisplay")

            get_email_value = self.getText(self._selected_email_address, "xpath")
            self.log.info("get Value : " + get_email_value)
            cancel_selected_customer = \
                self.validators.verify_text_match(get_email_value, "Please enter an email recipient")

            select_address_flag = self.select_customer_email_from_address_book()

            self.log.info("Flag Value : contactList:" + str(contact_list_flag) + ",contactFlag: " + str(contact_flag) +
                          ",recentFlag:" + str(recent_flag) + ",addressBook:" + str(add_book_flag) +
                          ", list validation:" + str(customer_list_validation) + ",select address flag: " +
                          str(select_address_flag) + " Validating the selected email Cancel : " +
                          str(cancel_selected_customer))

            if contact_list_flag and contact_flag and recent_flag and add_book_flag and customer_list_validation and \
                    select_address_flag and cancel_selected_customer:
                return True
            else:
                return False

        except Exception as e:
            self.log.error("Exception occurred in click_and_validate_select_contact_pop_up Method and Exception is " +
                           str(e))
            return False

    def validate_contact_list_customer_name(self, customer_name):
        """Validating in the Address Book Contact list Customer Name"""
        try:
            self.log.info("Inside validate_contact_list_customer_name Method")
            if self.isElementPresent(self._email_contact_list_header_value, "xpath"):
                customer_list_element = self.getElementList(self._email_customer_list, "xpath")
                for i in customer_list_element:
                    if not customer_name == i.text:
                        self.log.debug("Customer name not matching with one of the list value.")
                        return False
                return True
            else:
                self.log.error("Email Contact Popup not opened, hence not able to validate.")
                return False
        except Exception as e:
            self.log.error("Exception occurred in validate_contact_list_customer_name Method and Exception is " +
                           str(e))

    def select_customer_email_from_address_book(self):
        """selecting Customer from addressBook"""
        try:
            self.log.info("Inside select_customer_email_from_address_book Method")
            self.elementClick(self._copy_to_text + "/following-sibling::div", "xpath")
            self.waitForElement(self._email_spinner, "xpath", "display")
            self.waitForElement(self._email_spinner, "xpath", "notdisplay", timeout=20)
            if self.isElementPresent(self._email_contact_list_header_value, "xpath"):
                email_value = self.getText(self._email_pop_email_list, "xpath")
                self.elementClick(self._customer_select_email_popup, "xpath")
                self.elementClick(self._select_button_email_contact, "xpath")
                self.waitForElement(self._email_contact_list_header_value, "xpath", "notdisplay")
                get_email_value = self.getText(self._selected_copy_to_address, "xpath")

                return self.validators.verify_text_match(get_email_value, email_value)
            else:
                self.log.error("Email Contact Popup not opened, hence not able to validate.")
        except Exception as e:
            self.log.error("Exception occurred in select_customer_email_from_address_book Method and Exception is " +
                           str(e))

    def validateEmailPopSubject(self, expectedText):
        """Validating the subject line in Email Popup"""
        try:
            self.log.info("Inside validateEmailPopSubject Method")
            getSubject = self.getText(self._email_subject, "xpath")
            return self.validators.verify_text_match(getSubject, expectedText)
        except Exception as e:
            self.log.error("Exception occurred in validateEmailPopSubject Method and Exception is " + str(e))

    def carrierCheckboxValidation(self, selectedCarrierCheckboxIndexes):
        """
        Method to validate carrier checkbox selection
        :param :selectedCarrierCheckboxIndexes : list containing the index to validate the carrier checkbox selection.
        :return : True if the checkbox(s) are in checked state else False
        """
        self.log.info("Inside carrierCheckboxValidation Method and the passed selectedCarrierCheckboxIndexes = " +
                      str(selectedCarrierCheckboxIndexes))
        try:
            selectionResult = True
            noOfCarriersCheckboxSelected = len(selectedCarrierCheckboxIndexes)
            self.log.info("noOfCarriersCheckboxSelected :" + str(noOfCarriersCheckboxSelected))
            for index in range(noOfCarriersCheckboxSelected):
                selectionResult = self.isElementPresent(self._carrier_row + str(selectedCarrierCheckboxIndexes[index]) +
                                                        "'] //div[contains(@class,'Checkbox')]/*", "xpath")

                if not selectionResult:
                    self.log.error("Carrier checkbox is not in checked state for the index " +
                                   str(selectedCarrierCheckboxIndexes[index]))

            self.log.info("Final selectionResult :" + str(selectionResult))
            return selectionResult
        except Exception as e:
            self.log.error("Method carrierCheckboxValidation and the exception is " + str(e))
            return False

    # ----------------------- Cost Only Section Method Starts ----------------------------------------------

    def validateAllCarriersCostBreakupCharges(self):
        """
        Method to validate All carriers Total Amount breakup charges(Cost)  Without Insurance
        """
        self.log.info("Inside validateAllBreakupCharges Method")
        try:
            totalAmtValidationWithoutInsuranceResult = True

            setFlag = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
                if not self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath") and \
                        not self.isElementPresent(self._nocarriers_element, "id"):
                    setFlag = setFlag and True
                else:
                    setFlag = setFlag and False
            elif self.isElementPresent(self._nocarriers_element, "id"):
                self.log.debug("No carriers/results found")
                setFlag = setFlag and False

            if setFlag:
                elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
                noOfCarriers = len(elementList)
                self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

                # Validating All carriers Total Amount breakup charges(Cost) Without Insurance
                for startIndex in range(noOfCarriers):
                    # Total Amount Breakup without insurance
                    finalTotalAmountPopupList = self.totalAmountBreakupCostOnly(startIndex)
                    totalAmtValidationWithoutInsuranceResultInitial = self.validateTotalCost(finalTotalAmountPopupList)
                    totalAmtValidationWithoutInsuranceResult = \
                        totalAmtValidationWithoutInsuranceResult and totalAmtValidationWithoutInsuranceResultInitial

                self.log.info("Total amount breakup charge without insurance status: " + str(
                    totalAmtValidationWithoutInsuranceResult))

                if totalAmtValidationWithoutInsuranceResult:
                    self.log.info("Total amount breakup charges are validated for amount without insurance")
                    return True
                else:
                    self.log.error("Total amount breakup charges are not correct for amount without insurance")
                    return False
            else:
                self.log.error("No Carrier(s) found, setflag value : " + str(setFlag))
                return False

        except Exception as e:
            self.log.error(f"Method validateAllCarriersCostBreakupCharges : Not able to validate all amounts breakup "
                           f"charges or passed locator may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def validateTotalCost(self, referenceList):
        """
        Method to validate total cost with all the breakup cost's  with all the breakup revenue's
        :param referenceList: List containing all the details in Total Amount popup in the order
        Total Cost,Breakup charges Label,Breakup charges Cost,hyperlink & Insurance breakup status
        """
        self.log.info("Inside validateTotalCost Method")
        try:

            # referenceList = [tccost,label,cost,cost hyperlink & Insurance breakup status]
            costAmtListUI = [float(i) for i in referenceList[2]]
            summedCost = sum(costAmtListUI)
            totalCost = round(summedCost, 2)

            self.log.info("Total Cost : " + str(totalCost))
            self.log.info("Total Cost from Total Amount Breakup Popup : " + str(referenceList[0]))

            if totalCost == float(referenceList[0]):
                self.log.info("Cost total is correct")
                sumTotalCost = True
            else:
                self.log.error("Cost total is not correct")
                sumTotalCost = False

            self.log.info("sumTotalCost Status : " + str(sumTotalCost))
            self.log.info("Total Cost from Total Amount Breakup Popup with Cost HyperLink Value "
                          "Validation Status: " + str(referenceList[3]))

            if sumTotalCost and referenceList[3]:
                self.log.info("Cost total is correct,Insurance status is correct and"
                              " hyperlink cost are matching with total cost in amt breakup")
                return True
            else:
                self.log.error("Cost total is not correct or Insurance status is not correct or"
                               " hyperlink cost are not matching with total cost in amt breakup")
                return False

        except Exception as e:
            self.log.error("Method validateTotalCost : Not able to validate all amounts breakup charges or passed "
                           "locator may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def totalAmountBreakupCostOnly(self, carrierIndex):
        """
        Method to return the cost breakup in a list based on the parameter whether with insurance or without insurance.
        :param carrierIndex:
        :return Total Amt breakup charge along with Validation status in list based on the insurance parameter.
        """
        self.log.info("Inside totalAmountBreakupCostOnly Method")
        try:
            costLabelListUI = []
            totalAmtListUI = []
            costAmtListUI = []
            finalTotalAmtPopupList = []
            validationResult = True

            self.log.info("CarrierIndex Value is : " + str(carrierIndex))

            # clickCostWithoutInsurance = self._carrier_costwithinsurance + str(carrierIndex) + "'][1]" + \
            #                             self._carrier_costlink
            clickCostWithoutInsurance = "//div[@id='" + self._carrier_insurancecostcolumn + str(carrierIndex) + "']"
            # self.pageVerticalScroll(clickCostWithoutInsurance, "xpath")
            carrierName = self._carrier_row + str(carrierIndex) + self._carrierrow_costonly_name
            self.pageVerticalScroll(carrierName, "xpath")
            costHyperLink = self.getText(clickCostWithoutInsurance, "xpath")
            costHyperLinkUI = costHyperLink[5:-4]
            self.log.info("Total Cost from display(Hyperlink) : " + str(costHyperLinkUI))

            costHyperlinkElementDispBeforeScrollResult = self.isElementDisplayed(clickCostWithoutInsurance, "xpath")
            self.log.info(
                "costHyperlinkElementDispBeforeScrollResult : " + str(costHyperlinkElementDispBeforeScrollResult))
            carrierSection = self._carrier_row + str(carrierIndex) + "']"
            self.pageVerticalScroll(carrierSection, "xpath")
            costHyperlinkElementDispAfterScrollResult = self.isElementDisplayed(clickCostWithoutInsurance, "xpath")
            self.log.info(
                "costHyperlinkElementDispAfterScrollResult : " + str(costHyperlinkElementDispAfterScrollResult))

            self.waitForElement(clickCostWithoutInsurance, locatorType="xpath")
            self.elementClick(clickCostWithoutInsurance, locatorType="xpath")
            self.waitForElement(self._carrier_costpopup, locatorType="id", event="display")
            self.pageVerticalScroll(self._totalamt_popup_row3, "xpath")

            costLabelList = self.getElementList(self._totalamt_popup_row2 + self._cost_label, locatorType="xpath")
            for i in costLabelList:
                costLabel = i.text
                costLabelListUI.append(costLabel)

            totalAmtList = self.getElementList(
                self._totalamt_popup_row3 + self._totalamt_popup_col + self._cost_label, locatorType="xpath")
            for i in totalAmtList[1:]:
                totalAmt = i.text[:-4]
                totalAmtListUI.append(totalAmt)

            costLabelAmtList = self.getElementList(self._totalamt_popup_col2 + self._costlabel_amt,
                                                   locatorType="xpath")
            for i in costLabelAmtList:
                costLabelAmt = i.text
                costAmtListUI.append(costLabelAmt)

            totalCostAmt = totalAmtListUI[0]
            self.log.info("Total Cost from display(Hyperlink) : " + str(costHyperLinkUI))
            self.log.info("Total Cost from breakup popup  : " + str(totalCostAmt))

            if costHyperLinkUI == totalCostAmt:
                self.log.info(
                    "Cost hyperlink value in UI is matching with Total Cost  present in Total Amount Breakup Popup at "
                    "without insurance rate")
                validationResult = validationResult and True
                self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))
            else:
                self.log.error(
                    "Cost hyperlink value in UI is not matching with Total Cost present n Total Amount Breakup Popup "
                    "at without insurance rate")
                validationResult = validationResult and False
                self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))

            if "Insurance" not in costLabelListUI:
                self.log.info("Insurance breakup is not present in without insurance rates.")
                validationResult = validationResult and True
                self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))
            else:
                self.log.error("Insurance breakup is present in without insurance rates.")
                validationResult = validationResult and False
                self.log.info("totalAmountBreakup at without insurance: validationResult " + str(validationResult))

            self.log.info("Total Cost Amt from UI : " + str(totalCostAmt))
            finalTotalAmtPopupList.append(totalCostAmt)
            self.log.info("Breakup Charges Label List : " + str(costLabelListUI))
            finalTotalAmtPopupList.append(costLabelListUI)
            self.log.info("Cost Breakup charges List : " + str(costAmtListUI))
            finalTotalAmtPopupList.append(costAmtListUI)
            self.log.info("Cost Hyperlink and Insurance Validation Status Result : " + str(validationResult))

            finalTotalAmtPopupList.append(validationResult)
            # finalTotalAmtPopupList : Total Cost,Breakup Name,Cost Breakup,Cost HyperLink & Insurance Breakup
            # Validation Status
            self.log.info("Final List of Total Amount Popup from UI : " + str(finalTotalAmtPopupList))

            self.elementClick(carrierName, "xpath")
            return finalTotalAmtPopupList

        except Exception as e:
            self.log.error("Method totalAmountBreakupCostOnly : Not able to validate amount breakup charges or passed "
                           "locator may got updated/wrong or clicked event didn't happened.")
            self.log.error("Exception" + str(e))

    def accessorialAndSiteTypeBreakupValidation(self):
        """
        Method to validate accessorials & site type break up are present or not
        :return If respective accessorials & site type are present return True otherwise False.
        """
        try:
            self.log.info("Inside accessorialAndSiteTypeBreakupValidation Method")
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=50)
            selectedAccessorials = self.reqop.selectedAccessorials(True)
            validationList = self.accessorialBreakupValidationList(selectedAccessorials)
            siteTypeValidationList = self.siteTypeBreakupValidationList()
            validationList.extend(siteTypeValidationList)
            self.log.info("Final List for validation(Accessorials & Site Type)." + str(validationList))
            accAndSiteTypeStatusResult = self.breakupValidation(validationList)
            return accAndSiteTypeStatusResult

        except Exception as e:
            self.log.error("Method accessorialAndSiteTypeBreakupValidation : Breakup validation for accessorials and "
                           "site type didnot happened.")
            self.log.error("Exception" + str(e))

    def siteTypeBreakupValidationList(self):
        """
        Method to create a validation list of site type for breakup comparison based on the selection.
        :return It will return a list containing the site type selected at origin & destination.
        """
        try:
            self.log.info("Inside siteTypeBreakupValidationList Method")
            _validationList = []
            originSiteType = self.rerdp.selectedSiteType("origin")
            self.log.info("originSiteType : " + str(originSiteType))
            destinationSiteType = self.rerdp.selectedSiteType("destination")
            self.log.info("destinationSiteType : " + str(destinationSiteType))

            if "RESIDENCE" == originSiteType:
                _validationList.append("Residential Pickup")
            elif "BUSINESS" == originSiteType:
                self.log.info("no need to append")
            else:
                originSiteTypeValue = originSiteType.title()
                _validationList.append(originSiteTypeValue)

            if "RESIDENCE" == destinationSiteType:
                _validationList.append("Residential Delivery")
            elif "BUSINESS" == destinationSiteType:
                self.log.info("no need to append")
            else:
                destinationSiteTypeValue = destinationSiteType.title()
                _validationList.append(destinationSiteTypeValue)

            self.log.info("Final Site Type Validation List : " + str(_validationList))
            return _validationList

        except Exception as e:
            self.log.error(
                "Method siteTypeBreakupValidationList : Validation List preparation of Site Type didnot happened.")
            self.log.error("Exception" + str(e))

    def accessorialBreakupValidationList(self, accessorialsBreakupLabel):
        """
        Method to create a validation list of accessorial for breakup comparison based on the selection.
        :return It will return a list containing the accessorial selected in the respective sections.
        """
        try:
            self.log.info("Inside accessorialBreakupValidationList Method")
            _validationList = []

            self.log.info("Accessorial Breakup conversion List : " + str(accessorialsBreakupLabel))
            for accessorial in accessorialsBreakupLabel:
                if "origin" not in accessorial and "destination" not in accessorial:

                    if "protectfromfreeze" == accessorial:
                        _validationList.append("Protect from freeze")
                    elif "sortandsegregate" == accessorial:
                        _validationList.append("Sort and Segregate")

                elif "origin" in accessorial and "destination" not in accessorial:
                    if "originappointment" == accessorial:
                        _validationList.append("Appointment Pickup")
                    elif "origininside" == accessorial:
                        _validationList.append("Inside Pickup")
                    elif "originliftgate" == accessorial:
                        _validationList.append("Liftgate Pickup")

                elif "origin" not in accessorial and "destination" in accessorial:
                    if "destinationnotifypriortoarrival" == accessorial:
                        _validationList.append("Notify prior to arrival")
                    elif "destinationappointment" == accessorial:
                        _validationList.append("Appointment Delivery")
                    elif "destinationinside" == accessorial:
                        _validationList.append("Inside Delivery")
                    elif "destinationliftgate" == accessorial:
                        _validationList.append("Liftgate Delivery")
                    elif "destinationtradeshowdelivery" == accessorial:
                        _validationList.append("Tradeshow Delivery")

            self.log.info("Final Accessorial Validation List : " + str(_validationList))
            return _validationList

        except Exception as e:
            self.log.error(
                "Method accessorialBreakupValidationList : Validation List preparation of Accessorial didnot happened.")
            self.log.error("Exception" + str(e))

    def breakupValidation(self, breakupValidationList):
        """
        Method to validate  break up are present or not based on the passed parameter
        :param breakupValidationList List containing breakup's needed to check in the breakup window.
        :return If all the breakup's are present based on the passed parameter then return True otherwise False.
        """
        self.log.info("Inside breakupValidation Method")
        try:
            breakupPresentStatus = True
            breakupPresentStatusResult = True
            breakupNotPresentList = []

            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

            if noOfCarriers > 0:
                # Validating breakup accessorials & sitetype for all carriers without insurance
                for startIndex in range(noOfCarriers):
                    # Total Amount Breakup without insurance
                    totalAmtPopupListWithoutInsurance = self.totalAmountBreakupCostOnly(startIndex)

                    for breakupfromlist in breakupValidationList:
                        if breakupfromlist in totalAmtPopupListWithoutInsurance[1]:
                            breakupPresentStatus = breakupPresentStatus and True
                        else:
                            breakupPresentStatus = breakupPresentStatus and False
                            carrierNameFromIndex = self.getText(
                                self._carrier_row + str(startIndex) + self._carrierrow_costonly_name, "xpath")
                            self.log.warning("The following breakup " + str(
                                breakupfromlist) + " was not present at the Index " + str(
                                startIndex) + " after Rereate at without Insurance for the carrier " + str(
                                carrierNameFromIndex))
                            # accessorial or site type not present  List
                            breakupNotPresentList.append(breakupfromlist)

                    breakupPresentStatusResult = breakupPresentStatusResult and breakupPresentStatus
                    self.log.info("Breakup Present Status " + str(breakupPresentStatusResult))

                self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))
                if not breakupPresentStatusResult:
                    self.log.info(
                        "All Breakup's List which is not present in the breakup : " + str(breakupNotPresentList))
                    uniqueAccOrSiteType = set(breakupNotPresentList)
                    self.log.info("Unique Acessorial/Site  Type List which is not present in the breakup : " + str(
                        uniqueAccOrSiteType))

                    breakupPresentStatusResult = not breakupPresentStatusResult
                    for breakupFromList in uniqueAccOrSiteType:
                        count = breakupNotPresentList.count(breakupFromList)
                        self.log.info("The no. of times the accessorial " + str(
                            breakupFromList) + " not present in the breakup is : " + str(count))

                        if count == noOfCarriers:
                            breakupPresentStatusResult = breakupPresentStatusResult and False
                            self.log.info("Status breakupPresentStatusResult: " + str(breakupPresentStatusResult))
                        else:
                            breakupPresentStatusResult = breakupPresentStatusResult and True
                            self.log.info("Status breakupPresentStatusResult: " + str(breakupPresentStatusResult))

                self.log.info("FinalStatus of breakup present for all carriers " + str(breakupPresentStatusResult))
                return breakupPresentStatusResult
            else:
                self.log.error("No. of carriers is zero.")
                return False

        except Exception as e:
            self.log.error("Method breakupValidation : Breakup validation didnot happened.")
            self.log.error("Exception" + str(e))

    # ---------  Adjust Rate Feature Method ----------------------#

    def defaultAdjustSellRateWindowForCarriers(self, validationRange=""):
        """
        Method to validate the default adjust sell rate window for carrier's based on the argument.
        :param validationRange Its the range of validation
        :return Return True if all the validations are correct otherwise False.
        """
        self.log.info("Inside defaultAdjustSellRateWindowForCarriers Method")
        try:

            defaultAdjustRateWindowWithInsuranceResult = True
            defaultAdjustRateWindowWithoutInsuranceResult = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))
            if validationRange == "":
                validationRange = noOfCarriers
            self.log.info("validationRange : " + str(validationRange))

            if noOfCarriers <= int(validationRange) and validationRange != "":
                validationRange = noOfCarriers
            if noOfCarriers > 0:
                for index in range(int(validationRange)):
                    # With Insurance Section
                    clickRadioButtonWithInsurance = self._carrier_row + str(
                        index) + "']" + self._carrier_withinsuranceradiobutton
                    self.pageVerticalScroll(clickRadioButtonWithInsurance, "xpath")
                    self.elementClick(clickRadioButtonWithInsurance, locatorType="xpath")

                    totalCostAndRevenueList = self.totalCostAndRevenueHyperlinkData(index)
                    clickAdjustRateHyperlink = "(//a[@id='" + self._adjustrate_hyperlink + str(index) + "'])[2]"
                    self.elementClick(clickAdjustRateHyperlink, locatorType="xpath")

                    # Commented due to bug LTL1-3216
                    defaultAdjustRateWindowWithInsurance = \
                        self.validators.verify_text_match(self.getText(self._adjustrate_orisellrate_value, "xpath").
                                                          replace("$", ""), totalCostAndRevenueList[1]) \
                        and self.validators.verify_text_match(self.getAttribute(self._adjustrate_newsellrate_value,
                                                                                "xpath", "value"), "0") \
                        and self.validators.verify_text_match(self.getAttribute(
                            self._adjustrate_newprofitmarginpercent_value, "xpath", "value"), "0")

                    self.elementClick(self._adjustrate_adjustbtn, locatorType="xpath")
                    defaultAdjustRateWindowWithInsurance = \
                        defaultAdjustRateWindowWithInsurance and not self.isElementPresent(
                            self._adjustrate_defaultwindow + str(index), "id")
                    # Commented due to bug LTL1-3216
                    # defaultAdjustRateWindowWithInsurance = \
                    #     defaultAdjustRateWindowWithInsurance and \
                    #     self.validators.verify_text_match(self.getText(self._adjustrate_threshold_errormsg, "xpath"),
                    #                                       self._adjustrate_thresholdvalue_errormsg)
                    defaultAdjustRateWindowWithInsuranceResult = \
                        defaultAdjustRateWindowWithInsuranceResult and defaultAdjustRateWindowWithInsurance
                    # adjustRateCloseBtn = "//div[@id='" + self._adjustrate_defaultwindow + str(
                    #     index) + "']//span/parent::div"
                    # self.elementClick(adjustRateCloseBtn, locatorType="xpath")

                    # Without Insurance Section
                    clickRadioButtonWithoutInsurance = self._carrier_row + str(
                        index) + "']" + self._carrier_withoutinsuranceradiobutton
                    self.pageVerticalScroll(clickRadioButtonWithoutInsurance, "xpath")
                    self.elementClick(clickRadioButtonWithoutInsurance, locatorType="xpath")
                    self.elementClick(clickAdjustRateHyperlink, locatorType="xpath")

                    # Commented due to bug LTL1-3216
                    # defaultAdjustRateWindowWithoutInsurance = self.validators.verify_text_match(
                    #     self.getText(self._adjustrate_orisellrate_value, "xpath").replace("$", ""),
                    #     totalCostAndRevenueList[3]) \
                    #                                           and self.validators.verify_text_match(
                    #     self.getAttribute(self._adjustrate_newsellrate_value, "xpath", "value"), "0") \
                    #                                           and self.validators.verify_text_match(
                    #     self.getAttribute(self._adjustrate_newprofitmarginpercent_value, "xpath", "value"), "0")

                    defaultAdjustRateWindowWithoutInsurance = \
                        self.validators.verify_text_match(
                            self.getAttribute(self._adjustrate_newsellrate_value, "xpath", "value"),
                            "0") and \
                        self.validators.verify_text_match(
                            self.getAttribute(self._adjustrate_newprofitmarginpercent_value, "xpath", "value"),
                            "0")
                    self.elementClick(self._adjustrate_adjustbtn, locatorType="xpath")

                    defaultAdjustRateWindowWithoutInsurance = \
                        defaultAdjustRateWindowWithoutInsurance and not self.isElementPresent(
                            self._adjustrate_defaultwindow + str(index), "id")
                    # Commented due to bug LTL1-3216
                    # defaultAdjustRateWindowWithoutInsurance = \
                    #     defaultAdjustRateWindowWithoutInsurance and \
                    #     self.validators.verify_text_match(self.getText(self._adjustrate_threshold_errormsg, "xpath"),
                    #                                       self._adjustrate_thresholdvalue_errormsg)
                    defaultAdjustRateWindowWithoutInsuranceResult = \
                        defaultAdjustRateWindowWithoutInsuranceResult \
                        and defaultAdjustRateWindowWithoutInsurance

                    # self.elementClick(self._adjustrate_cancelbtn, locatorType="xpath")
                    self.waitForElement(self._adjustrate_defaultwindow, locatorType="id", event="notdisplay")

                self.log.info("Default Adjust Rate Window with insurance status: " + str(
                    defaultAdjustRateWindowWithInsuranceResult))
                self.log.info("Default Adjust Rate Window without insurance status: " + str(
                    defaultAdjustRateWindowWithoutInsuranceResult))

                if defaultAdjustRateWindowWithInsuranceResult and defaultAdjustRateWindowWithoutInsuranceResult:
                    self.log.info(
                        "Default Adjust Rate Window Fields data are validated with insurance and without insurance")
                    return True
                else:
                    self.log.error("Default Adjust Rate Window Fields data are not correct with/without insurance")
                    return False
            else:
                self.log.error("No. of carrier's displayed is zero.")
                return False

        except Exception as e:
            self.log.error(f"Method defaultAdjustSellRateWindowForCarriers : Default Adjust Sell Rate Window Validation"
                           f" didnot happened and the Exception : {str(e)}")

    def validateAdjustRate(self, carrierIndex, radioButtonSelection="yes"):
        """
        Method to validate adjust rate for a carrier based on the selection of revenue.
        :param carrierIndex : index of the carrier.
        :param radioButtonSelection : radio button selection parameter.
        :return Return True if the validations are correct otherwise False.
        """
        self.log.info("Inside validateAdjustRate Method and the carrier Index is " + str(
            carrierIndex) + " and radiobutton selection is " + str(radioButtonSelection))
        try:
            validateAdjustRateStatus = True
            diffAllowedForProfitMarginPercentage = 0.05
            diff_allowed_for_sell_rate = 0.5

            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=50)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))

            # Following carrier hyperlink data's before Adjust Rate.
            if (noOfCarriers - 1) == carrierIndex:
                followingCarrierDataResultBeforeAdjustRate = self.totalCostAndRevenueHyperlinkData(carrierIndex - 1)
            else:
                followingCarrierDataResultBeforeAdjustRate = self.totalCostAndRevenueHyperlinkData(carrierIndex + 1)

            clickAdjustRateHyperlink = "(//a[@id='" + self._adjustrate_hyperlink + str(carrierIndex) + "'])[2]"
            self.pageVerticalScroll(clickAdjustRateHyperlink, "xpath")
            self.elementClick(clickAdjustRateHyperlink, locatorType="xpath")
            self.waitForElement(self._adjustrate_defaultwindow + str(carrierIndex), locatorType="id", event="display")

            orisellrate = self.getText(self._adjustrate_orisellrate_value, "xpath").replace("$", "")
            oriprofitmarginpercentage = self.getText(self._adjustrate_oriprofitmarginpercent_value, "xpath")
            actualCostResult = self.actualCostCalculation(orisellrate, oriprofitmarginpercentage)
            self.log.info("actualCostResult : " + str(actualCostResult))

            self.log.info("currentOriginalSellRate : " + str(orisellrate))
            enteredNewSellRate = round((float(orisellrate) + (float(orisellrate) / 2)), 0)
            self.log.info("enteredNewSellRate : " + str(enteredNewSellRate))
            self.sendKeys(str(enteredNewSellRate), self._adjustrate_newsellrate_value, locatorType="xpath")
            profitMarginPercentageBasedOnEnteredNewSellRate = self.getAttribute(
                self._adjustrate_newprofitmarginpercent_value, "xpath", "value")
            actualProfitMarginPercentage = self.profitMarginPercentageCalculation(actualCostResult, enteredNewSellRate)
            profitMarginPercentageBasedOnCostCalculation = round(actualProfitMarginPercentage, 2)

            differenceProfitMarginPercentage = round(float(profitMarginPercentageBasedOnEnteredNewSellRate) - float(
                profitMarginPercentageBasedOnCostCalculation), 2)
            self.log.info("profitMarginPercentageBasedOnEnteredNewSellRate : " + str(
                profitMarginPercentageBasedOnEnteredNewSellRate))
            self.log.info(
                "profitMarginPercentageBasedOnCostCalculation : " + str(profitMarginPercentageBasedOnCostCalculation))
            self.log.info("profit margin percentage variation > differenceProfitMarginPercentage : " + str(
                differenceProfitMarginPercentage))

            # Validating the Sell Rate Based on Update Margin Percentage
            self.log.info("New Sell Rate : " + str(enteredNewSellRate) + "Profit Margin : " +
                          str(profitMarginPercentageBasedOnEnteredNewSellRate))
            self.sendKeys(profitMarginPercentageBasedOnEnteredNewSellRate,
                          self._adjustrate_newprofitmarginpercent_value, "xpath")

            get_new_sell_rate = self.getAttribute(self._adjustrate_newsellrate_value, "xpath", "value")
            self.log.info("New updated Sell Rate : " + str(get_new_sell_rate))

            diff_sell_rate = float(get_new_sell_rate) - float(enteredNewSellRate)
            self.log.debug("diffSellRate : " + str(diff_sell_rate))
            if float(-diff_allowed_for_sell_rate) <= diff_sell_rate <= float(diff_allowed_for_sell_rate):
                self.log.debug("After Update Margin percentage Sell Rate value showing same as earlier")
                validate_margin_value_status = True
            else:
                self.log.error("After Update Margin percentage Sell Rate value not showing same as earlier, Expected "
                               "sell rate : " + str(float(enteredNewSellRate)) + ", actual sell rate : " +
                               str(float(get_new_sell_rate)))
                validate_margin_value_status = False

            enteredNewSellRate = get_new_sell_rate

            if float(profitMarginPercentageBasedOnEnteredNewSellRate) == float(
                    profitMarginPercentageBasedOnCostCalculation):
                validateAdjustRateStatus = validateAdjustRateStatus and True
                self.log.info("New Profit Margin Percentage is correct and validateAdjustRateStatus is " + str(
                    validateAdjustRateStatus))
            else:
                if float(-diffAllowedForProfitMarginPercentage) <= differenceProfitMarginPercentage <= float(
                        diffAllowedForProfitMarginPercentage):
                    validateAdjustRateStatus = validateAdjustRateStatus and True
                    self.log.info("New Profit Margin Percentage is correct(including variation) and "
                                  "validateAdjustRateStatus is " + str(validateAdjustRateStatus))
                else:
                    validateAdjustRateStatus = validateAdjustRateStatus and False
                    self.log.error("New Profit Margin Percentage is wrong and validateAdjustRateStatus is " + str(
                        validateAdjustRateStatus))

            # Clicking on the Adjust Rate Button
            self.elementClick(self._adjustrate_adjustbtn, locatorType="xpath")
            self.waitForElement(self._adjustrate_defaultwindow + str(carrierIndex), locatorType="id",
                                event="notdisplay")
            # Click Adjust rate HyperLink again
            self.elementClick(clickAdjustRateHyperlink, locatorType="xpath")
            self.waitForElement(self._adjustrate_defaultwindow + str(carrierIndex), locatorType="id", event="display")

            # Original Sell Rate & Profit Margin Validation after Adjust Rate
            originalSellRateInWindowAfterAdjustRate = self.getText(self._adjustrate_orisellrate_value, "xpath").replace(
                "$", "")
            originalProfitMarginPercentageInWindowAfterAdjustRate = self.getText(
                self._adjustrate_oriprofitmarginpercent_value, "xpath")

            if float(originalSellRateInWindowAfterAdjustRate) == \
                    float(enteredNewSellRate) and float(originalProfitMarginPercentageInWindowAfterAdjustRate) == \
                    float(profitMarginPercentageBasedOnEnteredNewSellRate):
                validateAdjustRateStatus = validateAdjustRateStatus and True
                self.log.info("Original Sell Rate & Original Profit Margin Percentage In Window After Adjust Rate is "
                              "correct and validateAdjustRateStatus is " + str(validateAdjustRateStatus))
            else:
                validateAdjustRateStatus = validateAdjustRateStatus and False
                self.log.error("Original Sell Rate or Original Profit Margin PercentageIn Window After Adjust Rate is "
                               "wrong and validateAdjustRateStatus is " + str(validateAdjustRateStatus))

            # Click Cancel button in the Adjust Rate Window
            self.elementClick(self._adjustrate_cancelbtn, locatorType="xpath")
            self.waitForElement(self._adjustrate_defaultwindow, locatorType="id", event="notdisplay")

            totalAmtPopupListWithInsuranceAfterAdjustRate = self.totalAmountBreakup(carrierIndex, "yes")
            totalAmtValidationWithInsuranceResult = \
                self.validateTotalCostAndRevenue(totalAmtPopupListWithInsuranceAfterAdjustRate)

            getIndex = totalAmtPopupListWithInsuranceAfterAdjustRate[2].index("Insurance")
            insurancevalue = totalAmtPopupListWithInsuranceAfterAdjustRate[4][getIndex]

            totalAmtPopupListWithoutInsuranceAfterAdjustRate = self.totalAmountBreakup(carrierIndex, "no")
            totalAmtValidationWithoutInsuranceResult = \
                self.validateTotalCostAndRevenue(totalAmtPopupListWithoutInsuranceAfterAdjustRate)

            self.log.info("enteredNewSellRate : " + str(enteredNewSellRate))
            self.log.info("Revenue WithInsurance AfterAdjustRate : " +
                          str(totalAmtPopupListWithInsuranceAfterAdjustRate[1]))
            self.log.info("totalAmtValidationWithInsuranceResult : " + str(totalAmtValidationWithInsuranceResult))
            self.log.info("Revenue WithoutInsurance AfterAdjustRate : " + str(
                totalAmtPopupListWithoutInsuranceAfterAdjustRate[1]))
            self.log.info("totalAmtValidationWithoutInsuranceResult : " + str(totalAmtValidationWithoutInsuranceResult))

            if radioButtonSelection.lower() == "yes".lower():
                self.log.info("Radio Button Selection is With Insurance")
                # when radio button of with insurance was selected during the time of Adjust Rate
                revenuewithoutinsurane = float(enteredNewSellRate) - float(insurancevalue)
                self.log.info("revenuewithoutinsurane : " + str(round(revenuewithoutinsurane, 2)))
                if (float(enteredNewSellRate) == float(totalAmtPopupListWithInsuranceAfterAdjustRate[1])) and \
                        totalAmtPopupListWithInsuranceAfterAdjustRate[5] and totalAmtValidationWithInsuranceResult:
                    validateAdjustRateStatus = validateAdjustRateStatus and True
                    self.log.info(f"New Sell Rate entered in Adjust Rate Window got updated in the revenue hyper link "
                                  f"of with insurance,Sum of Cost & Revenue also correct and validateAdjustRateStatus "
                                  f"is {str(validateAdjustRateStatus)}")
                else:
                    validateAdjustRateStatus = validateAdjustRateStatus and False
                    self.log.error(f"New Sell Rate entered in Adjust Rate Window didnot got updated in the revenue "
                                   f"hyper link of with insurance or ,Sum of Cost/Revenue is wrong correct and "
                                   f"validateAdjustRateStatus is {str(validateAdjustRateStatus)}")

                if (float(totalAmtPopupListWithoutInsuranceAfterAdjustRate[1]) == float(round(revenuewithoutinsurane, 2))) and \
                        totalAmtPopupListWithoutInsuranceAfterAdjustRate[
                            5] and totalAmtValidationWithoutInsuranceResult:
                    validateAdjustRateStatus = validateAdjustRateStatus and True
                    self.log.info("Verified revenue got updated & not change in cost without insurance and "
                                  "validateAdjustRateStatus is " + str(validateAdjustRateStatus))
                else:
                    validateAdjustRateStatus = validateAdjustRateStatus and False
                    self.log.error("Verified revenue may not updated or  cost got changed without insurance and "
                                   "validateAdjustRateStatus is " + str(validateAdjustRateStatus))
            else:
                self.log.info("Radio Button Selection is Without Insurance")
                # when radio button of without insurance was selected during the time of Adjust Rate
                revenuewithinsurane = float(enteredNewSellRate) + float(insurancevalue)
                self.log.info("revenuewithinsurane : " + str(revenuewithinsurane))
                if (float(revenuewithinsurane) == float(totalAmtPopupListWithInsuranceAfterAdjustRate[1])) and \
                        totalAmtPopupListWithInsuranceAfterAdjustRate[5] and totalAmtValidationWithInsuranceResult:
                    validateAdjustRateStatus = validateAdjustRateStatus and True
                    self.log.info(f"New Sell Rate entered in Adjust Rate Window got updated in the revenue hyper link "
                                  f"of with insurance,Sum of Cost & Revenue also correct and validateAdjustRateStatus "
                                  f"is {str(validateAdjustRateStatus)}")
                else:
                    validateAdjustRateStatus = validateAdjustRateStatus and False
                    self.log.error(f"New Sell Rate entered in Adjust Rate Window didnot got updated in the revenue "
                                   f"hyper link of with insuranceor ,Sum of Cost/Revenue is wrong correct and "
                                   f"validateAdjustRateStatus is {str(validateAdjustRateStatus)}")

                if (float(totalAmtPopupListWithoutInsuranceAfterAdjustRate[1]) == float(enteredNewSellRate)) and \
                        totalAmtPopupListWithoutInsuranceAfterAdjustRate[
                            5] and totalAmtValidationWithoutInsuranceResult:
                    validateAdjustRateStatus = validateAdjustRateStatus and True
                    self.log.info("Verified revenue got updated & no change in cost without insurance and "
                                  "validateAdjustRateStatus is " + str(validateAdjustRateStatus))
                else:
                    validateAdjustRateStatus = validateAdjustRateStatus and False
                    self.log.error("Verified revenue may not updated or  cost got changed without insurance and "
                                   "validateAdjustRateStatus is " + str(validateAdjustRateStatus))

            # Following carrier hyperlink data's After Adjust Rate.
            if (noOfCarriers - 1) == carrierIndex:
                followingCarrierDataResultAfterAdjustRate = self.totalCostAndRevenueHyperlinkData(carrierIndex - 1)
            else:
                followingCarrierDataResultAfterAdjustRate = self.totalCostAndRevenueHyperlinkData(carrierIndex + 1)

            self.log.info("followingCarrierDataResultBeforeAdjustRate " +
                          str(followingCarrierDataResultBeforeAdjustRate))
            self.log.info("followingCarrierDataResultAfterAdjustRate " + str(followingCarrierDataResultAfterAdjustRate))
            followingCarrierDataResult = self.validators.verify_list_match(followingCarrierDataResultBeforeAdjustRate,
                                                                           followingCarrierDataResultAfterAdjustRate)

            # Following carrier's Data did not affected validation After Adjust Rate.
            if followingCarrierDataResult:
                validateAdjustRateStatus = validateAdjustRateStatus and True
                self.log.info("Following carrier data do not  got changed after adjust rate and "
                              "validateAdjustRateStatus is " + str(validateAdjustRateStatus))
            else:
                validateAdjustRateStatus = validateAdjustRateStatus and False
                self.log.error("Following carrier data got changed after adjust rate and validateAdjustRateStatus is " +
                               str(validateAdjustRateStatus))

            if validateAdjustRateStatus and validate_margin_value_status:
                self.log.info("All the validations related to adjust rate feature are correct.")
                return True
            else:
                self.log.error("All/Some validations related to adjust rate feature are incorrect.")
                return False

        except Exception as e:
            self.log.error("Method validateAdjustRate : Default Adjust Sell Rate Window Validation didn't happened "
                           "and the Exception : " + str(e))

    def validateAdjustRateForCarriers(self, validationRange=""):
        """
        Method to Validate Adjust Rate for carriers.
        :param validationRange Its the range of validation
        :return Return True if adjust rate validations are correct otherwise False.
        """
        self.log.info(
            "Inside validateAdjustRateForCarriers Method and the validation range passed is " + str(validationRange))
        try:

            validateAdjustRateStatusResult = True
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                # self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                #                     timeout=50)
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=180)
            elementList = self.getElementList(self._carrier_cell, locatorType="xpath")
            noOfCarriers = len(elementList)
            self.log.info("Total no. of Carriers in the carrier list :" + str(noOfCarriers))
            if validationRange == "":
                validationRange = noOfCarriers
            self.log.info("validationRange : " + str(validationRange))
            if noOfCarriers <= int(validationRange) and validationRange != "":
                validationRange = noOfCarriers
            if noOfCarriers > 0:
                for index in range(int(validationRange)):
                    # Radio Button Selection
                    # Radio Button Selection (with Insurance)
                    clickRadioButtonWithInsurance = \
                        self._carrier_row + str(index) + "']" + self._carrier_withinsuranceradiobutton
                    self.pageVerticalScroll(clickRadioButtonWithInsurance, "xpath")
                    self.elementClick(clickRadioButtonWithInsurance, locatorType="xpath")
                    validateAdjustRateWithInsuranceStatus = self.validateAdjustRate(index, "yes")
                    self.log.info("Adjust Rate Validation Status with Insurance for a carrier : " + str(
                        validateAdjustRateWithInsuranceStatus))

                    # Radio Button Selection (without Insurance)
                    clickRadioButtonWithoutInsurance = \
                        self._carrier_row + str(index) + "']" + self._carrier_withoutinsuranceradiobutton
                    self.pageVerticalScroll(clickRadioButtonWithoutInsurance, "xpath")
                    self.elementClick(clickRadioButtonWithoutInsurance, locatorType="xpath")
                    validateAdjustRateWithoutInsuranceStatus = self.validateAdjustRate(index, "no")
                    self.log.info("Adjust Rate Validation Status without Insurance for a carrier : " +
                                  str(validateAdjustRateWithoutInsuranceStatus))

                    validateAdjustRateStatusResult = \
                        validateAdjustRateStatusResult and validateAdjustRateWithInsuranceStatus and \
                        validateAdjustRateWithoutInsuranceStatus
                    self.log.info("Adjust Rate Validation Status with & without Insurance for a carrier : " +
                                  str(validateAdjustRateStatusResult))

                self.log.info("Final Adjust Rate Validation Status with & without Insurance : " +
                              str(validateAdjustRateStatusResult))
                if validateAdjustRateStatusResult:
                    self.log.info("Adjust Rate feature is validated for with and without insurance sections.")
                    return True
                else:
                    self.log.error("Adjust Rate feature validations  failed for  with or without insurance section(s).")
                    return False
            else:
                self.log.error("No. of carrier's displayed is zero.")
                return False

        except Exception as e:
            self.log.error(
                "Method validateAdjustRateForCarriers : Adjust Rate Validation didn't happened for carriers and the "
                "Exception : " + str(e))

    def actualCostCalculation(self, ori_sell_rate, ori_profit_margin_percentage):
        """
        Method to calulate the original cost returned by the carrier.
        :return actualCost : original cost value.
        """
        self.log.info("Inside actualCostCalculation Method and the passed original sellrate is " + str(
            ori_sell_rate) + " and original profit margin percentage is " + str(ori_profit_margin_percentage))
        try:
            mulOriProftPercentAndOriSellRate = float(ori_profit_margin_percentage) * float(ori_sell_rate)
            mulOriProftPercentAndOriSellRateDivByhundred = mulOriProftPercentAndOriSellRate / 100
            actualCost = float(ori_sell_rate) - mulOriProftPercentAndOriSellRateDivByhundred
            self.log.info("ori_sell_rate : " + str(ori_sell_rate) + ", ori_profit_margin_percentage : " +
                          str(ori_profit_margin_percentage) + ", mulOriProftPercentAndOriSellRate : " +
                          str(mulOriProftPercentAndOriSellRate) + ", mulOriProftPercentAndOriSellRateDivByhundred : " +
                          str(mulOriProftPercentAndOriSellRateDivByhundred) + ", Actual Cost " + str(actualCost))
            return actualCost

        except Exception as e:
            self.log.error(
                "Method actualCostCalculation : Passed locators is wrong/updated and the Exception : " + str(e))

    def profitMarginPercentageCalculation(self, cost, new_sell_rate):
        """
        Method to calculate New profit margin percentage.
        :param new_sell_rate:
        :param cost: original cost returned by the carrier.
        :return profitMarginPercentage : New profit margin percentage.
        """
        self.log.info("Inside profitMarginPercentageCalculation Method and the original cost is " + str(cost) +
                      " and new_sell_rate is " + str(new_sell_rate))
        try:
            subNewSellrateAndCost = float(new_sell_rate) - float(cost)
            subNewSellrateAndCostMulByHundred = subNewSellrateAndCost * 100
            actualProfitMarginPercentage = subNewSellrateAndCostMulByHundred / float(new_sell_rate)
            self.log.info("new_sell_rate : " + str(new_sell_rate) + ", subNewSellrateAndCost : " +
                          str(subNewSellrateAndCost) + ", subNewSellrateAndCostMulByHundred : " +
                          str(subNewSellrateAndCostMulByHundred) + ", actualProfitMarginPercentage before rounding : " +
                          str(actualProfitMarginPercentage))
            return actualProfitMarginPercentage

        except Exception as e:
            self.log.error(
                "Method profitMarginPercentageCalculation : Passed locators is wrong/updated and the Exception : " +
                str(e))

    def totalCostAndRevenueHyperlinkData(self, carrierIndex):
        """
        Method to return the total cost & revenue of with insurance and without insurance.
        :param carrierIndex: index of the carrier.
        :return Total cost & revenue of with insurance and without insurance list.
        List(CostHyperlink WithInsurance,RevenueHyperlink WithInsurance,CostHyperlink WithoutInsurance,RevenueHyperlink
         WithoutInsurance)
        """
        self.log.info("Inside totalCostAndRevenueHyperlinkData Method and the passed index is " + str(carrierIndex))
        try:

            totalCostAndRevenueList = []

            # With Insurance Section
            costWithInsurance = self._carrier_costwithinsurance + str(carrierIndex) + "'][1]" + self._carrier_costlink
            radioButtonWithInsurance = self._carrier_row + str(
                carrierIndex) + "']" + self._carrier_withinsuranceradiobutton
            self.pageVerticalScroll(radioButtonWithInsurance, "xpath")
            costHyperLink = self.getText(costWithInsurance, "xpath")
            costHyperLinkUI = costHyperLink[5:-4]
            revenueWithInsurance = self._carrier_costwithinsurance + str(
                carrierIndex) + "'][1]" + self._carrier_revenuelink
            revenueHyperLink = self.getText(revenueWithInsurance, "xpath")
            revenueHyperLinkUI = revenueHyperLink[:-4]
            self.log.info("Total Cost from display(Hyperlink) of with insurance section : " + str(costHyperLinkUI))
            totalCostAndRevenueList.append(costHyperLinkUI)
            self.log.info(
                "Total Revenue from display(Hyperlink) of with insurance section : " + str(revenueHyperLinkUI))
            totalCostAndRevenueList.append(revenueHyperLinkUI)

            # Without Insurance Section
            costWithoutInsurance = self._carrier_costwithinsurance + str(
                carrierIndex) + "'][2]" + self._carrier_costlink
            radioButtonWithoutInsurance = self._carrier_row + str(
                carrierIndex) + "']" + self._carrier_withoutinsuranceradiobutton
            self.pageVerticalScroll(radioButtonWithoutInsurance, "xpath")
            costHyperLink = self.getText(costWithoutInsurance, "xpath")
            costHyperLinkUI = costHyperLink[5:-4]
            revenueWithoutInsurance = self._carrier_costwithinsurance + str(
                carrierIndex) + "'][2]" + self._carrier_revenuelink
            revenueHyperLink = self.getText(revenueWithoutInsurance, "xpath")
            revenueHyperLinkUI = revenueHyperLink[:-4]
            self.log.info("Total Cost from display(Hyperlink) of without insurance section : " + str(costHyperLinkUI))
            totalCostAndRevenueList.append(costHyperLinkUI)
            self.log.info(
                "Total Revenue from display(Hyperlink) of without insurance section : " + str(revenueHyperLinkUI))
            totalCostAndRevenueList.append(revenueHyperLinkUI)

            # totalCostAndRevenueList(CostHyperlink WithInsurance,RevenueHyperlink WithInsurance,
            # CostHyperlink WithoutInsurance,RevenueHyperlink WithoutInsurance)
            self.log.info(
                "totalCostAndRevenueHyperlinkData with and without Insurance  : " + str(totalCostAndRevenueList))
            return totalCostAndRevenueList

        except Exception as e:
            self.log.error("Method totalCostAndRevenueHyperlinkData : passed locator may got updated/wrong and the "
                           "Exception : " + str(e))

    def createQuotePageLoadWhenNavFromQuotesTab(self):
        self.log.info("Inside createQuotePageLoadWhenNavFromQuotesTab Method")
        try:
            self.waitForElement(self.reqop.create_quote_base_container + self.reqop.quote_page_spinner, "xpath",
                                event="display")
            self.waitForElement(self.reqop.create_quote_base_container + self.reqop.quote_page_spinner, "xpath",
                                event="notdisplay", timeout=20)
            self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="display", timeout=15)
            if self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath"):
                self.pageVerticalScroll(self._pageloadwait_listingcarriers, locatorType="xpath")
                self.waitForElement(self._pageloadwait_listingcarriers, locatorType="xpath", event="notdisplay",
                                    timeout=50)

            if not self.isElementPresent(self._pageloadwait_listingcarriers, locatorType="xpath") and \
                    not self.isElementPresent(self._nocarriers_element, "id"):
                self.log.debug("Create quote page loaded & carrier(s) loaded within the timeout successfully.")
                return True
            else:
                self.log.error("Create quote page didnot loaded or carrier(s) did not load within the timeout.")
                return False

        except Exception as E:
            self.log.error("Exception occurred in createQuotePageLoadWhenNavFromQuotesTab Method and Exception is : " +
                           str(E))
