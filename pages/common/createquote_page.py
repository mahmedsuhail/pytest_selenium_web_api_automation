import time

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from pages.basepage import BasePage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.customer_builder import CustomerBuilder
from pages.common.models.customer import Customer

CREATE_QUOTE_CONTAINER = (By.ID, "createQuote_LTL1-279_createQuoteContainer")
CREATE_QUOTE_CONTAINER_HEADER = \
    (By.XPATH, "//div[@id='createQuote_LTL1-279_createQuoteContainer']"
               "//div[contains(@class,'CardLightViewstyles__Header-ui')]")
QUOTE_ORDER_ICON = \
    (By.XPATH, "//nav[contains(@class,'side-navstyle__LeftBarNav')] //a[@href='/create-quote']")
ACC_SECTION = (By.ID, "accessorialNSpecialServices_TL-680_Card-Title")
ACC_CHECKBOX_LABEL = (
    By.CSS_SELECTOR, "#accessorialNSpecialServices_TL-680_Card-Title label[class*='CheckboxContainer']")
CUSTOMER_SPINNER = \
    (By.XPATH, "//div[@id='createQuote_LTL1-279_select_2'] //div[contains(@class,'css-1wy0on6')]/child::*")
SERVICE_TYPE_DROPDOWN = (
    By.ID, "createQuote_LTL1-279_select_3")
SERVICE_TYPE_DROPDOWN_LTL = (
    By.XPATH, "//div[@id='createQuote_LTL1-279_select_3']//div[contains(@id,'select') and text()='LTL']")
SERVICE_TYPE_DROPDOWN_TL = (
    By.XPATH, "//div[@id='createQuote_LTL1-279_select_3']//div[contains(@id,'select') and text()='TL']")
SERVICE_TYPE_FIELD = (By.ID, "createQuote_LTL1-279_select_3")
CUSTOMER_NAME_FIELD = (By.CSS_SELECTOR, "#createQuote_LTL1-279_select_2 input")
CUSTOMER_RATES_RADIOBUTTON = (By.ID, "createQuote_LTL1-279_createQuoteRadios-0")
COST_RADIOBUTTON = (By.ID, "createQuote_LTL1-279_createQuoteRadios-1")
CURRENCY_DROPDOWN = (By.ID, "createQuote_LTL1-279_select_1")
SERVICE_DROPDOWN = (By.ID, "createQuote_TL-688_selectService")
CUSTOMER_SEARCH_BOX_ELEMENT = (By.ID, "createQuote_LTL1-279_select_2")
CUSTOMER_SEARCH_BOX = (
    By.CSS_SELECTOR, "#createQuote_LTL1-279_select_2 input")
CUSTOMER_SEARCH_BOX_RESULT_LIST = (
    By.CSS_SELECTOR, "#createQuote_LTL1-279_select_2 div[class*='menu'] div[id*='react-select']")
SELECTED_CUSTOMER = (
    By.XPATH, "//div[@id='createQuote_LTL1-279_select_2']//div[contains(@class, 'singleValue')]")
CUSTOMER_SEARCH_BOX_PLACE_HOLDER = (
    By.XPATH, "//div[@id='createQuote_LTL1-279_select_2']//div[contains(@class, 'placeholder')]")

CUSTOMER_LISTS = "//div[@id='createQuote_LTL1-279_select_2'] //div[contains(@class,'-menu')] //div[contains(@class,'itemstyles__Header')]"
CUSTOMER_CONTACT_LIST = "//div[@id='createQuote_LTL1-279_select_2'] //div[contains(@class,'-menu')] //div[contains(@class,'itemstyles__Detail')]"
MIDDLE_SECTION_DISABLED_ELEMENTS = (By.CSS_SELECTOR, "div[subscript = 'center'] div[class*= 'hGYFfd']")


class CreateQuotePage(BasePage):

    def __init__(self, driver, log):
        super().__init__(driver, log)

    def get_url_create_quote(self):
        return "/create-quote"

    def open_page(self):
        self.driver.get(self.get_base_url() + self.get_url_create_quote())

    def click_quote_order_icon(self):
        """
            Clicking Quote Icon from side bar and waiting till quote page is not display.
        :return:
        """
        self.wait_for_element(QUOTE_ORDER_ICON, "display")
        self.element_click(element=self.get_element(locator=QUOTE_ORDER_ICON))
        self.wait_for_element_visible(locator=CREATE_QUOTE_CONTAINER, timeout=40)
        self.wait_for_element(locator=QUOTE_ORDER_ICON, event="display")

    def validate_quote_order_icon(self):
        """
            Validating whether icon highlighted or not.
        :return: if highlight than true else false
        """
        value = self.get_attribute(locator=QUOTE_ORDER_ICON, attribute_type="class")
        if value == "active":
            return True
        else:
            return False

    def select_service_type(self, service_type, timeout=30):
        end_time = time.time() + timeout
        while time.time() < end_time:
            try:
                self.wait_for_element_clickable(locator=SERVICE_TYPE_DROPDOWN)
                self.element_click_by_locator(locator=SERVICE_TYPE_FIELD)
                service_type_dropdown = self.basic_types_factory.get_dropdown(locator=SERVICE_TYPE_DROPDOWN)

                if service_type == ServiceType.TL:
                    self.wait_for_element_clickable(locator=SERVICE_TYPE_DROPDOWN_TL)
                    service_type_dropdown.select_option_by_value(value=service_type.name.upper())

                elif service_type == ServiceType.LTL:
                    self.wait_for_element_clickable(locator=SERVICE_TYPE_DROPDOWN_LTL)
                    service_type_dropdown.select_option_by_value(value=service_type.name.upper())

                self.wait_for_element_has_text(locator=SERVICE_TYPE_FIELD, text_value=service_type.name.upper(),
                                               timeout=3)

                if not service_type_dropdown.is_dropdown_open() and self.get_text(
                        locator=SERVICE_TYPE_FIELD) == service_type.name.upper():
                    break

            except Exception as E:
                self.log.error("Method selectServiceType : Passed locator incorrect or element get updated")
                self.log.error("Exception : " + str(E))

            if time.time() > end_time:
                self.log.error("Was not able to select correct service type - timeout")
                break

    def get_service_type_dropdown_options(self) -> [str]:
        dropdown = self.basic_types_factory.get_dropdown(locator=SERVICE_TYPE_DROPDOWN)
        return dropdown.get_available_options_values()

    def search_customer(self, search_string=""):
        """
        Based on the passed text search the Data in customer list field and wait till drop down not present.
        :param search_string: Customer Name
        :return: None
        """
        if self.get_text(locator=CUSTOMER_SEARCH_BOX_ELEMENT) != 'Enter customer name':
            self.log.info("customer Name Display as selected")
            self.element_click_by_locator(CUSTOMER_SEARCH_BOX)
            self.wait_for_element(locator=CUSTOMER_SPINNER, event="notdisplay")
            key_combination = Keys.CONTROL, "a", Keys.BACK_SPACE
            self.log.info("variable :" + str(key_combination))
            self.send_keys(data=key_combination, locator=CUSTOMER_SEARCH_BOX)
        self.send_keys(data=search_string, locator=CUSTOMER_SEARCH_BOX)
        self.wait_for_element(locator=CUSTOMER_SPINNER, event="notdisplay")  # wait for spinner
        self.wait_for_customer_search_result()

    def select_customer_from_list(self, index=1, timeout=60):
        """
        Selecting customer from List based on the pass index value and return the Selected customer name
        :param index: index value which has to select if want to select last element than pass 0 2nd last -1 and
        1st element 1
        :return: Selected Customer Name
        """
        end_time = time.time() + timeout
        while True:
            try:
                customer_lists = self.get_element_list(locator=CUSTOMER_SEARCH_BOX_RESULT_LIST)
                list_count = len(customer_lists)
                self.log.info("Customer list count : " + str(list_count) + " and index value is " + str(index))

                if index <= 0:
                    customer_name = self.getText("(" + CUSTOMER_LISTS + ")[" + str(list_count + index) + "]", "xpath")
                    customer_contact_name = self.getText(
                        "(" + CUSTOMER_CONTACT_LIST + ")[" + str(list_count + index) + "]",
                        "xpath")
                    self.elementClick("(" + CUSTOMER_LISTS + ")[" + str(list_count + index) + "]", "xpath")
                    self.wait_for_element(locator=CUSTOMER_SPINNER, event="notdisplay")
                elif index > 0:
                    customer_name = self.getText("(" + CUSTOMER_LISTS + ")[" + str(index) + "]", "xpath")
                    customer_contact_name = self.getText("(" + CUSTOMER_CONTACT_LIST + ")[" + str(index) + "]", "xpath")
                    self.elementClick("(" + CUSTOMER_LISTS + ")[" + str(index) + "]", "xpath")
                    self.wait_for_element(locator=CUSTOMER_SPINNER, event="notdisplay")
                else:
                    self.log.info("Did not get proper customer Index, It should be Number.")
                    return None

                customer_id = customer_name[customer_name.rfind("-") + 1:].strip()
                self.log.info("Customer ID: " + str(customer_id) + ", contact Name : " + str(
                    customer_contact_name) + ", and customer Name : " + str(customer_name))
                exact_customer_name = customer_name[:customer_name.rfind("-")].strip()
                self.log.info("Exact Customer Name : " + str(exact_customer_name))
                self.wait_for_element(locator=CUSTOMER_SEARCH_BOX_ELEMENT, event="text", text_value=exact_customer_name,
                                      timeout=20)

                return exact_customer_name, customer_id, customer_contact_name
            except Exception as E:
                if time.time() > end_time:
                    time.sleep(1)
                    self.log.info(f"Error during select_customer_from_list, exception:{str(E)} ")
                    self.log.error(str(E))
                    break

    def search_and_select_customer_by_name(self, search_string, customer_name) -> Customer:

        self.search_customer(search_string=search_string)

        elements = self.get_element_list(locator=CUSTOMER_SEARCH_BOX_RESULT_LIST)
        selected_customer = None

        for element in elements:
            if element.text.splitlines()[0] == customer_name:
                self.scroll_into_view(element=element)
                selected_customer = CustomerBuilder() \
                    .with_full_name(element.text.splitlines()[0]) \
                    .with_contact(element.text.splitlines()[1]).build()
                self.element_click(element=element)
                break

        return selected_customer

    def search_and_select_customer_by_index(self, search_string, index=1, timeout=60):
        end_time = time.time() + timeout
        while True:
            try:
                self.search_customer(search_string=search_string)
                customer_name = self.getText("(" + CUSTOMER_LISTS + ")[" + str(index) + "]", "xpath")
                customer_contact_name = self.getText("(" + CUSTOMER_CONTACT_LIST + ")[" + str(index) + "]", "xpath")
                customer_id = customer_name[customer_name.rfind("-") + 1:].strip()
                self.elementClick("(" + CUSTOMER_LISTS + ")[" + str(index) + "]", "xpath")
                self.wait_for_element(locator=CUSTOMER_SPINNER, event="notdisplay")
                return customer_name, customer_contact_name, customer_id
            except Exception as E:
                if time.time() > end_time:
                    time.sleep(1)
                    self.log.info(f"Error during search_and_select_customer_by_index, exception:{str(E)} ")
                    self.log.error(str(E))
                    break

    def get_customer_with_contacts_list(self):
        customers = []
        contacts = []

        elements = self.get_element_list(locator=CUSTOMER_SEARCH_BOX_RESULT_LIST)

        for element in elements:
            customers.append(element.text.splitlines()[0])
            contacts.append(element.text.splitlines()[1])

        customers_with_contacts = []
        for i in range(len(customers)):
            customers_with_contacts.append([customers[i], contacts[i]])

        self.log.info(f"Retrieved a list of customers with contacts: {customers_with_contacts}")

        return customers_with_contacts

    def get_accessorials_container_visible(self):
        return self.is_element_present(ACC_SECTION)

    def get_create_quote_container_visibility(self) -> bool:
        self.wait_for_element(locator=CREATE_QUOTE_CONTAINER)
        return self.is_element_present(locator=CREATE_QUOTE_CONTAINER)

    def get_customer_rates_radiobutton_visibility(self) -> bool:
        return self.is_element_present(locator=CUSTOMER_RATES_RADIOBUTTON)

    def get_customer_rates_radiobutton_checked_status(self) -> bool:
        checked = self.get_element(locator=CUSTOMER_RATES_RADIOBUTTON).get_property(name="checked")
        return checked is not None

    def get_cost_radiobutton_visibility(self) -> bool:
        return self.is_element_present(locator=COST_RADIOBUTTON)

    def get_customer_search_box_value(self) -> str:
        return self.get_text(locator=SELECTED_CUSTOMER)

    def get_customer_search_box_place_holder_value(self) -> str:
        return self.get_text(locator=CUSTOMER_SEARCH_BOX_PLACE_HOLDER)

    def get_service_type_dropdown_value(self):
        return self.get_text(locator=SERVICE_TYPE_DROPDOWN)

    def get_service_dropdown_value(self):
        return self.get_text(locator=SERVICE_DROPDOWN)

    def get_currency_dropdown_value(self):
        return self.get_text(locator=CURRENCY_DROPDOWN)

    def clear_customer_search_field(self):
        self.clear_field(locator=CUSTOMER_NAME_FIELD)
        self.log.info(f"Cleared customer search field")

    def wait_for_customer_search_result(self):
        self.wait_for_element(locator=CUSTOMER_SEARCH_BOX_RESULT_LIST, timeout=40)

    def is_create_quote_container_header_highlighted(self) -> bool:
        is_highlighted = False
        element = self.get_element(locator=CREATE_QUOTE_CONTAINER_HEADER)
        value = element.get_attribute("class")
        value = self.get_attribute(locator=CREATE_QUOTE_CONTAINER_HEADER, attribute_type="class")
        if "fiUmjF" in value:
            is_highlighted = True
        return is_highlighted

    # TODO: review and decide how proceed with verifications within page object.
    def verify_quote_page_is_disabled(self):
        """
            Validating the Quote Page whether disabled or not. accordingly returning values

        :return: if disabled than return True
        """
        self.log.info(self.is_element_present(locator=MIDDLE_SECTION_DISABLED_ELEMENTS))
        if self.is_element_present(locator=MIDDLE_SECTION_DISABLED_ELEMENTS):
            elements = self.get_element_list(locator=MIDDLE_SECTION_DISABLED_ELEMENTS)
            self.log.info("Length of elements : " + str(len(elements)))
            if len(elements) == 3:
                return True
            else:
                return False
        else:
            self.log.debug("verifyQuotePageDisable method : Element not present, hence executing else block")
            return True
