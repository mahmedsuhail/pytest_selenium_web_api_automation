import random
import string

from pages.common.models.dispatcher import Dispatcher
from utilities.util import Util


class DispatcherBuilder:

    def __init__(self):
        self.dispatcher = Dispatcher()

    def with_name(self, name):
        self.dispatcher.name = name
        return self

    def with_phone(self, phone):
        self.dispatcher.phone = phone
        return self

    def with_email(self, email):
        self.dispatcher.email = email
        return self

    def with_fax(self, fax):
        self.dispatcher.fax = fax
        return self

    def with_random_name(self):
        self.dispatcher.name = Util.get_random_alpha_numeric(length=10)
        return self

    def with_random_email(self):
        self.dispatcher.email = Util.get_random_alpha_numeric(length=10) + "@mail.com"
        return self

    def with_random_phone(self):
        self.dispatcher.phone = Util.get_random_alpha_numeric(length=10, characters_type="digits")
        return self

    def with_random_fax(self):
        self.dispatcher.fax = Util.get_random_alpha_numeric(length=10, characters_type="digits")
        return self

    def build(self):
        return self.dispatcher
