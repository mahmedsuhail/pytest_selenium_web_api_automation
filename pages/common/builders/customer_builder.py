from pages.common.models.customer import Customer


class CustomerBuilder:

    def __init__(self):
        self.customer = Customer()

    def with_full_name(self, full_name):
        self.customer.full_name = full_name
        return self

    def with_contact(self, contact):
        self.customer.contact = contact
        return self

    def with_short_name(self, short_name):
        self.customer.short_name = short_name
        return self

    def build(self):
        return self.customer
