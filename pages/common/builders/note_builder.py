from pages.common.models.note import Note


class NoteBuilder:
    def __init__(self):
        self.note = Note()

    def with_note_type(self, note_type):
        self.note.note_type = note_type
        return self

    def with_note_text(self, note_text):
        self.note.note_text = note_text
        return self

    def with_username(self, username):
        self.note.username = username
        return self

    def with_timestamp(self, timestamp):
        self.note.timestamp = timestamp
        return self

    def build(self):
        return self.note
