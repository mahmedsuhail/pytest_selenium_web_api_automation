from pages.common.models.driver import Driver
from utilities.util import Util


class DriverBuilder:

    def __init__(self):
        self.driver = Driver()

    def with_name(self, name):
        self.driver.name = name
        return self

    def with_phone(self, phone):
        self.driver.phone = phone
        return self

    def with_random_name(self):
        self.driver.name = Util.get_random_alpha_numeric(length=10)
        return self

    def with_random_phone(self):
        self.driver.phone = Util.get_random_alpha_numeric(length=10, characters_type="digits")
        return self

    def build(self):
        return self.driver
