from pages.common.models.shipping_item import ShippingItem


class ShippingItemBuilder:
    def __init__(self):
        self.shipping_item = ShippingItem()

    def with_commodity_description(self, commodity_description):
        self.shipping_item.commodity_description = commodity_description
        return self

    def with_piece_count(self, piece_count):
        self.shipping_item.piece_count = piece_count
        return self

    def with_weight(self, weight):
        self.shipping_item.weight = weight
        return self

    def with_weight_units(self, weight_units):
        self.shipping_item.weight_units = weight_units
        return self

    def with_pallet_count(self, pallet_count):
        self.shipping_item.pallet_count = pallet_count
        return self

    def with_length(self, length):
        self.shipping_item.length = length
        return self

    def with_width(self, width):
        self.shipping_item.width = width
        return self

    def with_height(self, height):
        self.shipping_item.height = height
        return self

    def with_dim_units(self, dim_units):
        self.shipping_item.dim_units = dim_units
        return self

    def with_stackable(self):
        self.shipping_item.is_stackable = True
        return self

    def with_hazmat(self):
        self.shipping_item.is_hazmat = True
        return self

    # up to 4 digits
    def with_hazmat_code(self, code):
        self.shipping_item.hazmat_code = code
        return self

    def with_hazmat_chemical_name(self, chemical_name):
        self.shipping_item.hazmat_chemical_name = chemical_name
        return self

    # values are hardcoded, check on page
    def with_hazmat_class(self, class_name):
        self.shipping_item.hazmat_class = class_name
        return self

    def with_hazmat_emergency_contact(self, contact_number):
        self.shipping_item.hazmat_emergency_contact = contact_number
        return self

    # values are hardcoded, check on page
    def with_hazmat_group(self, group):
        self.shipping_item.hazmat_group = group
        return self

    # values are hardcoded, check on page
    def with_hazmat_prefix(self, prefix):
        self.shipping_item.hazmat_prefix = prefix
        return self

    def build(self):
        return self.shipping_item
