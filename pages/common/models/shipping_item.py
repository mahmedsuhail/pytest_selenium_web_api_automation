class ShippingItem:
    def __init__(self):
        self.commodity_description = ''
        self.piece_count = ''
        self.weight = ''
        self.weight_units = ''
        self.pallet_count = ''
        self.length = ''
        self.width = ''
        self.height = ''
        self.dim_units = ''
        self.is_hazmat = False
        self.hazmat_group = ''
        self.hazmat_class = ''
        self.hazmat_prefix = ''
        self.hazmat_code = ''
        self.hazmat_chemical_name = ''
        self.hazmat_emergency_contact = ''
        self.is_stackable = False

    def __str__(self):
        return "Commodity Description: " + self.commodity_description + " Piece Count: " + self.piece_count \
               + " Weight: " + self.weight + " " + " weight_units: " + self.weight_units + " pallet_count: " \
               + self.pallet_count + " length: " + self.length + " width: " + self.width + " height: " \
               + self.height + " dim_units: " + self.dim_units

    def __eq__(self, other):

        return \
            self.commodity_description == other.commodity_description and \
            self.piece_count == other.piece_count and \
            self.weight == other.weight and \
            self.weight_units == other.weight_units and \
            self.pallet_count == other.pallet_count and \
            self.length == other.length and \
            self.width == other.width and \
            self.height == other.height and \
            self.dim_units == other.dim_units and \
            self.is_hazmat == other.is_hazmat and \
            self.hazmat_group == other.hazmat_group and \
            self.hazmat_class == other.hazmat_class and \
            self.hazmat_prefix == other.hazmat_prefix and \
            self.hazmat_code == other.hazmat_code and \
            self.hazmat_chemical_name == other.hazmat_chemical_name and \
            self.hazmat_emergency_contact.replace('(', '').replace(')', '').replace(' ', '') == \
            other.hazmat_emergency_contact.replace('(', '').replace(')', '').replace(' ', '') and \
            self.is_stackable == other.is_stackable

    def get_clear_template(self):
        tmp = ShippingItem()
        tmp.commodity_description = ''
        tmp.dim_units = 'Inches'
        tmp.hazmat_chemical_name = ''
        tmp.hazmat_class = ''
        tmp.hazmat_code = ''
        tmp.hazmat_emergency_contact = ''
        tmp.hazmat_group = ''
        tmp.hazmat_prefix = ''
        tmp.height = ''
        tmp.is_hazmat = False
        tmp.is_stackable = False
        tmp.length = ''
        tmp.pallet_count = ''
        tmp.piece_count = ''
        tmp.weight = ''
        tmp.weight_units = 'lbs'
        tmp.width = ''

        return tmp

    def get_required_placeholders(self, is_hazmat, is_stackable):
        tmp = ShippingItem()
        tmp.commodity_description = 'Required'
        tmp.dim_units = ''
        tmp.is_hazmat = is_hazmat
        if tmp.is_hazmat:
            tmp.hazmat_chemical_name = 'Enter Name'
            tmp.hazmat_class = 'Required'
            tmp.hazmat_code = 'Required'
            tmp.hazmat_emergency_contact = 'Valid phone is required'
            tmp.hazmat_group = 'Required'
            tmp.hazmat_prefix = ''
        tmp.is_stackable = is_stackable
        if tmp.is_stackable:
            tmp.height = 'Req'
        else:
            tmp.height = 'Enter'
        tmp.length = 'Enter'
        tmp.pallet_count = 'Required'
        tmp.piece_count = 'Required'
        tmp.weight = 'Required'
        tmp.weight_units = ''
        tmp.width = 'Enter'

        return tmp
