class DataHolder:
    """
    Encapsulates dictionary like data and provides methods to compare with another
    """
    def __init__(self, key_values):
        self.key_values = key_values

    def compare(self, val2, keys_to_ignore=None) -> (bool, str):
        """
        Compare two DataHolder objects
        Parameters:
            val2 - DataHolder object to compare
            keys_to_ignore - list of keys ignored in comparison
        Returns:
            1 comparison status (True, False)
            2 list of non equal keys in a concatenated string
        """
        if keys_to_ignore is None:
            keys_to_ignore = []
        result = ""
        for key in self.key_values:
            if key not in keys_to_ignore:
                result += self.__values_equal(val1=self.key_values[key], val2=val2.key_values[key], name=key)

        if len(result) > 0:
            return False, result
        return True, result

    # noinspection PyMethodMayBeStatic
    def __values_equal(self, val1, val2, name) -> str:
        if val1 != val2:
            return f"{name}: {val1} != {val2}\n"
        return ""

    def __getitem__(self, key):
        return self.key_values[key]

    def __setitem__(self, key, value):
        self.key_values[key] = value
