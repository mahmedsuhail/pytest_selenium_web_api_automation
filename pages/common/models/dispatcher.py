class Dispatcher:
    def __init__(self):
        self.name = ""
        self.phone = ""
        self.email = ""
        self.fax = ""

    def get_formatted_phone_number(self) -> str:
        return f"({self.phone[:3]}) {self.phone[3:6]}-{self.phone[6:10]}"
