from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.common.models.sales_order import SalesOrder
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util
from utilities.validators import Validators
import pytest


class SalesOrderCreator:

    def __init__(self, driver, log):
        self.driver = driver
        self.log = log
        self.rdp = RouteDetailsPage(self.driver, self.log)
        self.si = ShippingItemsPage(self.driver, self.log)
        self.car = CarriersPage(self.driver, self.log)
        self.qop = CreateQuotePageLTL(self.driver, self.log)
        self.util = Util(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    def create_orders(self, sales_order_data, order_address_book, order_shipping) -> (ReportTestStatus, [SalesOrder]):

        sales_orders = []

        self.qop.click_quote_order_icon()

        for row in sales_order_data:

            sales_order = SalesOrder()

            selected_customer_result = self.qop.searchAndSelectCustomer(row["customername"], 1)
            dictionary = {}
            if selected_customer_result is not None:
                self.ts.mark(selected_customer_result[0], "Validating the search customer selected")
                self.util.add_to_dictionary("customerid", selected_customer_result[2], dictionary)
                self.util.add_to_dictionary("customercontactname", selected_customer_result[3], dictionary)

            else:
                self.ts.mark(False, "Validating the search customer selected")

            # Validate Freight Date is set to given Date
            select_date = int(row["selectdate"])
            validate_next_date_selected_result = self.rdp.dateSelection(select_date)
            self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

            if row["addressbook"].lower() == "yes":
                self.log.info("Address Book value passed as Yes")
                ori_acc_list = []
                dest_acc_list = []
                add_index = row["sno"]
                self.log.info("get Index : " + str(add_index))
                for r_add in order_address_book:
                    if int(add_index) >= int(r_add["sno"]):
                        # self.log.info("sno : "+str(row["sno"]))
                        if int(r_add["sno"]) == int(row["sno"]):
                            if r_add["addresssection"] == "origin":
                                # Origin address book and site type validation
                                self.rdp.searchAndSelectAddressFromAddressBook(r_add["company_name"],
                                                                               r_add["company_name"],
                                                                               field=r_add["addresssection"])
                                zip_value = r_add["address"][r_add["address"].rfind(",") + 1:].strip()
                                self.log.info("zip_value : " + str(zip_value))
                                validate_selected_add = self.rdp.verifySelectedAddressInTextField(zip_value,
                                                                                                  field=r_add[
                                                                                                      "addresssection"])
                                self.ts.mark(validate_selected_add,
                                             "Validating the selected Address for Origin from Address Book")
                                # Verify site type for selected Address
                                selected_site_type_result = self.rdp.verifySiteTypeSelected(r_add["site_type"],
                                                                                            r_add["addresssection"])
                                self.ts.mark(selected_site_type_result,
                                             "Validating the Site type for shipper Address Book.")
                                ori_acc_list = self.qop.getAccessorialsList(r_add["origin_accessorials"],
                                                                            r_add["addresssection"])

                            elif r_add["addresssection"] == "destination":
                                # Destination address book and site type and accessorial
                                self.rdp.searchAndSelectAddressFromAddressBook(r_add["company_name"],
                                                                               r_add["company_name"],
                                                                               field=r_add["addresssection"])
                                zip_value = r_add["address"][r_add["address"].rfind(",") + 1:].strip()
                                self.log.info("zip_value : " + str(zip_value))

                                validate_selected_add = self.rdp.verifySelectedAddressInTextField(zip_value,
                                                                                                  field=r_add[
                                                                                                      "addresssection"])
                                self.ts.mark(validate_selected_add,
                                             "Validating the selected Address for Destination from Address Book")
                                # Verify site type for selected Address
                                selected_site_type_result = self.rdp.verifySiteTypeSelected(r_add["site_type"],
                                                                                            r_add["addresssection"])
                                self.ts.mark(selected_site_type_result,
                                             "Validating the Site type for consignee Address Book.")
                                dest_acc_list = self.qop.getAccessorialsList(r_add["destination_accessorials"],
                                                                             r_add["addresssection"])

                            else:
                                self.log.error("Incorrect address type passed")
                                self.ts.mark(False, "Incorrect address type passed to select address Book method")
                    else:
                        self.log.info("out of address Book selection index")
                        break

                # Accessorial Verification
                self.log.info("Origin Accessorial List : " + str(ori_acc_list) + " and Destination accessrial list: " +
                              str(dest_acc_list))
                complete_acc_list = ori_acc_list + dest_acc_list
                self.log.info("Complete accessorial List : " + str(complete_acc_list))

                selected_acc_app = self.qop.selectedAccessorials("True")
                self.log.info("selected accessorial from Application : " + str(selected_acc_app))
                compare_acc_list = Validators(logger=self.log).verify_list_match(complete_acc_list, selected_acc_app)
                self.ts.mark(compare_acc_list, "Validating accessorial list display after select from address Book.")
            else:
                self.log.info("Address Book value passed as No")
                zip_value = row["orizip"][row["orizip"].rfind(",") + 1:].strip()
                self.log.info("zip_value : " + str(zip_value))

                origin_select_result = self.rdp.directSearchAndSelectAddress(zip_value, "origin")
                self.ts.mark(origin_select_result, "Validating the passed and selected address for origin.")
                zipdest = row["destzip"][row["destzip"].rfind(",") + 1:].strip()
                destination_select_result = self.rdp.directSearchAndSelectAddress(zipdest, "destination")
                self.ts.mark(destination_select_result, "Validating the passed and selected address for destination.")

                # Select Site type
                self.rdp.selectSiteType(row["originsitetype"], "origin")
                verify_selected_site_type_ori = self.rdp.verifySiteTypeSelected(row["originsitetype"], "origin")
                self.ts.mark(verify_selected_site_type_ori, "selected site type for Origin")

                self.rdp.selectSiteType(row["destinationsitetype"], "destination")
                verify_selected_site_type_dest = self.rdp.verifySiteTypeSelected(row["destinationsitetype"],
                                                                                 "destination")
                self.ts.mark(verify_selected_site_type_dest, "selected site type for Destination")

                # Accessorial Selection
                select_ori_acc = self.qop.selectAccessorial(row["originacc"],
                                                            self.util.get_key_for_value(row["originacc"], row))
                self.ts.mark(select_ori_acc, "Accessorial selected for Origin section")
                select_dest_acc = self.qop.selectAccessorial(row["destinationacc"],
                                                             self.util.get_key_for_value(row["destinationacc"], row))
                self.ts.mark(select_dest_acc, "Validating selected accessorial for destination")
                selected_acc_display_list = self.qop.selectedAccessorials("True")
                self.log.info("Display selected accessorial List from Application : " + str(selected_acc_display_list))

            # adding Shipping Data
            shipping_index = row["sno"]
            self.log.info("index value : " + str(shipping_index))
            for s_row in order_shipping:
                self.log.info("adding shipping Items")
                self.log.info("Number : " + str(s_row["sno"]))
                if int(shipping_index) >= int(s_row["sno"]):
                    if int(row["sno"]) == int(s_row["sno"]):
                        self.log.info("Inside Product book")
                        if s_row["productbook"].lower() == "yes":
                            self.log.info("Inside Add Product")
                            added_product_result = self.si.addNewProducts(s_row["commodity"], s_row["nfmc"],
                                                                          s_row["className"],
                                                                          s_row["UnitType"],
                                                                          unitcount=s_row["unitcount"],
                                                                          piece=s_row["piece"], weight=s_row["weight"],
                                                                          length=s_row["length"], width=s_row["width"],
                                                                          height=s_row["height"],
                                                                          # weightmom=s_row["weightUnits"], dimmom=s_row["Dim"],
                                                                          stackable=s_row["stackable"],
                                                                          hazmat=s_row["hazmat"],
                                                                          hazgrp=s_row["HazmatGroup"],
                                                                          hzclass=s_row["HazmatClass"],
                                                                          prefix=s_row["prefix"],
                                                                          hzcode=s_row["HazmatCode"],
                                                                          chemName=s_row["ChemicalName"],
                                                                          emergNo=s_row["emergencyContact"])

                            self.ts.mark(added_product_result[0], "Added Product Result.")
                            self.log.info("Returned Product Value : " + str(added_product_result[1]))
                            if added_product_result[0]:
                                selected_product = self.si.searchAndSelectProduct(added_product_result[1])
                                self.ts.mark(selected_product, "Validating search and able to select product")
                                # Validating the shipping Data as its selected from Product Book
                                valid_all_shipping_data = self.si.verifyCombinedShippingItemData(
                                    added_product_result[1],
                                    s_row["piece"],
                                    s_row["weight"],
                                    s_row["unitcount"],
                                    s_row["UnitType"],
                                    s_row["className"],
                                    s_row["nfmc"],
                                    s_row["length"],
                                    s_row["width"],
                                    s_row["height"],
                                    # s_row["weightUnits"],s_row["Dim"],
                                    stackable=s_row["stackable"],
                                    hazmat=s_row["hazmat"],
                                    hzmcode=s_row["HazmatCode"],
                                    chemName=s_row["ChemicalName"],
                                    emergNo=s_row["emergencyContact"],
                                    hzGrp=s_row["HazmatGroup"],
                                    hzclass=s_row["HazmatClass"],
                                    prefix=s_row["prefix"])
                                self.log.info("shipping Data : " + str(valid_all_shipping_data))
                                self.ts.mark(valid_all_shipping_data, "Validating product data in shipping items")
                                # Clicking Add to Order Button
                                self.si.clickAddToOrderButton()
                                get_shipping_dict = self.util.add_to_dictionary(
                                    self.util.get_key_for_value(s_row["newcommodity"], s_row),
                                    added_product_result[1])
                                self.log.info("Final shipping Dict Object : " + str(get_shipping_dict))
                                # writeCSVData("salesorder_shipping.csv", get_shipping_dict, s_row)
                                sales_order.sales_order_shipping.append({**s_row, **get_shipping_dict})
                            else:
                                closing_popup_result = self.si.closeProductPopUp()
                                self.ts.mark(closing_popup_result,
                                             "Validating the popup should get closed with out any error")

                        elif s_row["productbook"].lower() == "no":
                            self.log.info("Product Book value No")
                            self.si.entryShippingItemsData(s_row["commodity"], s_row["piece"], s_row["weight"],
                                                           s_row["unitcount"],
                                                           s_row["UnitType"], s_row["className"], s_row["nfmc"],
                                                           s_row["length"],
                                                           s_row["width"],
                                                           s_row["height"], s_row["weightUnits"], s_row["Dim"],
                                                           stackable=s_row["stackable"], hazmat=s_row["hazmat"],
                                                           hazgrp=s_row["HazmatGroup"], hzclass=s_row["HazmatClass"],
                                                           # prefix=s_row["prefix"],
                                                           hzcode=s_row["HazmatCode"], chemName=s_row["ChemicalName"],
                                                           emergNo=s_row["emergencyContact"])

                            # Clicking Add to Order Button
                            self.si.clickAddToOrderButton()
                            sales_order.sales_order_shipping.append(s_row)
                else:
                    self.log.info("Out of product index Condition")
                    break

            # Entering Total shipment Value
            self.si.enterTotalShipmentValue(row["shipmentvalue"])

            # Adding Notes
            adding_notes_result = self.qop.addAndCancelNote("submit", row["notetype"], content=row["notes"])
            self.ts.mark(adding_notes_result, "Validating the Added notes in Notes Section")

            # Entering Data into Origin and Destination Container
            if row["addressbook"].lower() == "yes":
                self.log.info("Address Book value is Yes, Only address Book Validation")
                # Validating Code Here as address Book Already populated from Address Book
                address_index = row["sno"]
                for r_add in order_address_book:
                    if int(address_index) >= int(r_add["sno"]):
                        if int(r_add["sno"]) == int(row["sno"]):
                            addr = r_add["address"]
                            self.log.info("Printing address : " + str(addr))
                            # Extracting city and Zip code
                            city_name = addr[:addr.rfind(",")]
                            zipcode = addr[addr.rfind(",") + 1:].strip()
                            self.log.info("city Name : " + str(city_name) + ", zip code : " + str(zipcode))
                            if r_add["addresssection"] == "origin":
                                self.log.info("Origin section ")
                                validate_ori_details = self.rdp.validateAddressContainerFields(r_add["addresssection"],
                                                                                               city_name, zipcode,
                                                                                               r_add['readytime'],
                                                                                               r_add['closetime'],
                                                                                               r_add['company_name'],
                                                                                               r_add['contact_name'],
                                                                                               r_add['contact_phone'],
                                                                                               r_add['addressline1'],
                                                                                               r_add['addressline2'],
                                                                                               r_add['remarks'])
                                self.ts.mark(validate_ori_details, "Verify Origin Address details after select from "
                                                                   "address book")
                            elif r_add["addresssection"] == "destination":
                                self.log.info("Destination section")
                                validate_dest_details = self.rdp.validateAddressContainerFields(r_add["addresssection"],
                                                                                                city_name, zipcode,
                                                                                                r_add['readytime'],
                                                                                                r_add['closetime'],
                                                                                                r_add['company_name'],
                                                                                                r_add['contact_name'],
                                                                                                r_add['contact_phone'],
                                                                                                r_add['addressline1'],
                                                                                                r_add['addressline2'],
                                                                                                r_add['remarks'])
                                self.ts.mark(validate_dest_details, "Verify Destination Address details after select "
                                                                    "from address book")
                            else:
                                self.log.error("Incorrect Address type passed in Method for address Book")
                                self.ts.mark(False, "Incorrect Address type passed for Origin/Destination container")
                    else:
                        self.log.info("Out of Address Book index Condition")
                        break
            else:
                self.log.info("Address Book value is No , Manual Entry")
                self.log.info("Passed Address Book Value is " + str(row["addressbook"]))
                self.rdp.enterRequiredAddressContainerDetails(
                    row['oricompanyName'],
                    row['oricontactName'],
                    row['oriphone'],
                    addressline1=row['oriaddline1'],
                    addressline2=row["oriaddline2"],
                    readytime=row["orireadytime"],
                    closetime=row["oriclosetime"],
                    remarks=row['pickupremarks'],
                    field="origin")
                self.rdp.enterRequiredAddressContainerDetails(
                    row['destcompanyname'],
                    row['destcontactname'],
                    row['destphone'],
                    addressline1=row['destaddline1'],
                    addressline2=row["destaddline2"],
                    readytime=row["destreadytime"],
                    closetime=row["destclosetime"],
                    remarks=row['deliveryremarks'],
                    field="destination")

            # # selecting Carrier
            carrier_data = self.car.selectCarrierByNameOrIndex(row["carriers"], row["selectcarrierrate"])
            bol_number = ""
            if carrier_data is not None:
                self.ts.mark(carrier_data[5], "Passed carrier result selection and break up correct.")
                self.log.info("Getting carrier Data : " + str(carrier_data))

                quote_value = self.qop.getIdentifierValue("quote")
                self.log.debug("Get Quote Value : " + str(quote_value))
                self.util.add_to_dictionary("quoteid", quote_value, dictionary)

                # dictionary = self.util.add_to_dictonary("totalcost", carrier_data[0])
                self.util.add_to_dictionary("totalcost", carrier_data[0], dictionary)
                self.util.add_to_dictionary("totalrevenue", carrier_data[1], dictionary)
                self.util.add_to_dictionary("selectedcarriername", carrier_data[6], dictionary)
                self.util.add_to_dictionary("transitdays", carrier_data[7], dictionary)
                appointmentdates = carrier_data[8].split("-")
                self.util.add_to_dictionary("originpickup", appointmentdates[0], dictionary)
                self.util.add_to_dictionary("etadelivery", appointmentdates[1], dictionary)

                del carrier_data[5:]
                del carrier_data[:2]

                self.util.add_to_dictionary("carrierbreakup", carrier_data, dictionary)
                self.log.info("Dict : " + str(dictionary))

                if row["addressbook"].lower() == "yes":
                    DefaultTime = True
                else:
                    DefaultTime = self.rdp.validateDefaultTimeInContainer()

                # Creating Order
                create_order_result = self.qop.createOrder(DefaultTime)
                self.ts.mark(create_order_result, "Validating the Create Order Result")
                self.qop.waitForOrderPopup()
                validate_message = self.qop.validateCreateOrderMessage()
                self.ts.mark(validate_message, "Validating the Order Creation display Message.")
                bol_number = self.qop.getBolNumber()
                self.log.debug("Return Bol NUmber : " + str(bol_number))
                order_popup_status = self.qop.orderPopupClick("close")
                self.ts.mark(order_popup_status, "Verify the Order popup status")

                get_dict = self.util.add_to_dictionary("bolnumber", bol_number, dictionary)
                self.log.info("Final Dict Object : " + str(get_dict))

                sales_order.sales_order_data = {**row, **get_dict}
                sales_orders.append(sales_order)

                # Writing to Dictionary
                # writeCSVData("salesOrderData.csv", get_dict, row)
            else:
                self.ts.mark(False, "Carrier list not populated so not able to create shipment")

                # get_dict = self.util.add_to_dictonary(self.util.get_key_for_value(row["bolnumber"], row), BolNumber, dictionary)
                # self.log.info("Final Dict Object : " + str(get_dict))
                #
                # # Writing to Dictionary
                # writeCSVData("salesOrderData.csv", get_dict, row)

            # self.lop.logout()
            # logoutResult = self.lop.verifyLogoutSuccessful()
            # self.ts.mark(logoutResult, "Validating the LogOut")
        return self.ts, sales_orders

    # def create_orders(self, sales_order_data, order_address_book, order_shipping) -> (TestStatus, [SalesOrder]):
    #
    #     sales_orders = []
    #
    #     self.createquote.clickQuoteOrderIcon()
    #
    #     for row in sales_order_data:
    #
    #         sales_order = SalesOrder()
    #
    #         selected_customer_result = self.createquote.searchAndSelectCustomer(row["customername"], 1)
    #         dictionary = {}
    #         if selected_customer_result is not None:
    #             self.test_status.mark(selected_customer_result[0], "Validating the search customer selected")
    #             self.util.add_to_dictonary("customerid", selected_customer_result[2], dictionary)
    #             self.util.add_to_dictonary("customercontactname", selected_customer_result[3], dictionary)
    #
    #         else:
    #             self.test_status.mark(False, "Validating the search customer selected")
    #
    #         # Validate Freight Date is set to given Date
    #         select_date = int(row["selectdate"])
    #         validate_next_date_selected_result = self.route_details_page.dateSelection(select_date)
    #         self.test_status.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
    #
    #         if row["addressbook"].lower() == "yes":
    #             self.__handle_address_book(row, order_address_book)
    #         else:
    #             self.log.info("Address Book value passed as No")
    #             zip_value = row["orizip"][row["orizip"].rfind(",") + 1:].strip()
    #             self.log.info("zip_value : " + str(zip_value))
    #
    #             origin_select_result = self.route_details_page.directSearchAndSelectAddress(zip_value, "origin")
    #             self.test_status.mark(origin_select_result, "Validating the passed and selected address for origin.")
    #             zip_dest = row["destzip"][row["destzip"].rfind(",") + 1:].strip()
    #             destination_select_result = self.route_details_page.directSearchAndSelectAddress(zip_dest, "destination")
    #             self.test_status.mark(destination_select_result,
    #                                   "Validating the passed and selected address for destination.")
    #
    #             # # Select Site type
    #             self.__select_site_type(row)
    #
    #             # Accessorial Selection
    #             self.__select_accessorials(row)
    #
    #         sales_order = self.__add_shipping_data(sales_order_data_row=row, order_shipping=order_shipping,
    #                                                sales_order=sales_order)
    #
    #         self.shipping_items_page.enterTotalShipmentValue(row["shipmentvalue"])
    #
    #         # Adding Notes
    #         adding_notes_result = self.createquote.addAndCancelNote("submit", row["notetype"], content=row["notes"])
    #         self.test_status.mark(adding_notes_result, "Validating the Added notes in Notes Section")
    #
    #         # Entering Data into Origin and Destination Container
    #         self.__select_origin_destination(row, order_address_book)
    #
    #         # selecting Carrier
    #         carrier_data = self.carriers_page.selectCarrierByNameOrIndex(row["carriers"], row["selectcarrierrate"])
    #         bol_number = ""
    #         if carrier_data is not None:
    #             dictionary = self.__select_carrier(carrier_data, dictionary)
    #
    #             if row["addressbook"].lower() == "yes":
    #                 default_time = True
    #             else:
    #                 default_time = self.route_details_page.validateDefaultTimeInContainer()
    #
    #             # Creating Order
    #             create_order_result = self.createquote.createOrder(default_time)
    #             self.test_status.mark(create_order_result, "Validating the Create Order Result")
    #             self.createquote.waitForOrderPopup()
    #             validate_message = self.createquote.validateCreateOrderMessage()
    #             self.test_status.mark(validate_message, "Validating the Order Creation display Message.")
    #             bol_number = self.createquote.getBolNumber()
    #             self.log.debug("Return Bol NUmber : " + str(bol_number))
    #             order_popup_status = self.createquote.orderPopupClick("close")
    #             self.test_status.mark(order_popup_status, "Verify the Order popup status")
    #
    #             get_dict = self.util.add_to_dictonary("bolnumber", bol_number, dictionary)
    #             self.log.info("Final Dict Object : " + str(get_dict))
    #
    #             #TODO - remove after refactoring, we don't modify CSV files anymore
    #             # Writing to Dictionary
    #             #writeCSVData("salesOrderData.csv", get_dict, row)
    #
    #             sales_order.sales_order_data = {**row, **get_dict}
    #             sales_orders.append(sales_order)
    #
    #         else:
    #             self.test_status.mark(False, "Carrier list not populated so not able to create shipment")
    #
    #             # getDict = self.util.add_to_dictonary(self.util.get_key_for_value(row["bolnumber"], row), BolNumber, dictionary)
    #             # self.log.info("Final Dict Object : " + str(getDict))
    #             #
    #             # # Writing to Dictionary
    #             # writeCSVData("salesOrderData.csv", getDict, row)
    #
    #         # self.lop.logout()
    #         # logoutResult = self.lop.verifyLogoutSuccessful()
    #         # test_setup_status.mark(logoutResult, "Validating the LogOut")
    #
    #     return self.test_status, sales_orders
    #
    # def __add_shipping_data(self, sales_order_data_row, order_shipping, sales_order) -> TestStatus:
    #     shipping_index = sales_order_data_row["sno"]
    #     self.log.info("index value : " + str(shipping_index))
    #
    #     for s_row in order_shipping:
    #         self.log.info("adding shipping Items")
    #         self.log.info("Number : " + str(s_row["sno"]))
    #         if int(shipping_index) >= int(s_row["sno"]):
    #             if int(sales_order_data_row["sno"]) == int(s_row["sno"]):
    #                 self.log.info("Inside Product book")
    #
    #                 if s_row["productbook"].lower() == "yes":
    #                     self.log.info("Inside Add Product")
    #                     added_product_result = self.shipping_items_page.addNewProducts(s_row["commodity"], s_row["nfmc"],
    #                                                                                  s_row["className"],
    #                                                                                  s_row["UnitType"],
    #                                                                                  unitcount=s_row["unitcount"],
    #                                                                                  piece=s_row["piece"],
    #                                                                                  weight=s_row["weight"],
    #                                                                                  length=s_row["length"],
    #                                                                                  width=s_row["width"],
    #                                                                                  height=s_row["height"],
    #                                                                                  # weightmom=s_row["weightUnits"], dimmom=s_row["Dim"],
    #                                                                                  stackable=s_row["stackable"],
    #                                                                                  hazmat=s_row["hazmat"],
    #                                                                                  hazgrp=s_row["HazmatGroup"],
    #                                                                                  hzclass=s_row["HazmatClass"],
    #                                                                                  prefix=s_row["prefix"],
    #                                                                                  hzcode=s_row["HazmatCode"],
    #                                                                                  chemName=s_row["ChemicalName"],
    #                                                                                  emergNo=s_row["emergencyContact"])
    #
    #                     self.test_status.mark(added_product_result[0], "Added Product Result.")
    #                     self.log.info("Returned Product Value : " + str(added_product_result[1]))
    #
    #                     if added_product_result[0]:
    #                         selected_product = self.shipping_items_page.searchAndSelectProduct(added_product_result[1])
    #                         self.test_status.mark(selected_product, "Validating search and able to select product")
    #                         # Validating the shipping Data as its selected from Product Book
    #                         valid_all_shipping_data = self.shipping_items_page.verifyCombinedShippingItemData(
    #                             added_product_result[1],
    #                             s_row["piece"],
    #                             s_row["weight"],
    #                             s_row["unitcount"],
    #                             s_row["UnitType"],
    #                             s_row["className"],
    #                             s_row["nfmc"],
    #                             s_row["length"],
    #                             s_row["width"],
    #                             s_row["height"],
    #                             # s_row["weightUnits"],s_row["Dim"],
    #                             stackable=s_row[
    #                                 "stackable"]
    #                             , hazmat=s_row[
    #                                 "hazmat"],
    #                             hzmcode=s_row[
    #                                 "HazmatCode"],
    #                             chemName=s_row[
    #                                 "ChemicalName"],
    #                             emergNo=s_row[
    #                                 "emergencyContact"],
    #                             hzGrp=s_row[
    #                                 "HazmatGroup"],
    #                             hzclass=s_row[
    #                                 "HazmatClass"],
    #                             prefix=s_row[
    #                                 "prefix"])
    #                         self.log.info("shipping Data : " + str(valid_all_shipping_data))
    #                         self.test_status.mark(valid_all_shipping_data,
    #                                               "Validating product data in shipping items")
    #                         # Clicking Add to Order Button
    #                         self.shipping_items_page.clickAddToOrderButton()
    #                         get_shipping_dict = self.util.add_to_dictonary(
    #                             self.util.get_key_for_value(s_row["newcommodity"], s_row),
    #                             added_product_result[1])
    #                         self.log.info("Final shipping Dict Object : " + str(get_shipping_dict))
    #
    #                         # TODO - remove after refactoring, we shouldn't write to csv files anymore
    #                         #writeCSVData("salesorder_shipping.csv", get_shipping_dict, s_row)
    #
    #                         sales_order.sales_order_shipping.append({**s_row, **get_shipping_dict})
    #
    #                     else:
    #                         closing_popup_result = self.shipping_items_page.closeProductPopUp()
    #                         self.test_status.mark(closing_popup_result,
    #                                               "Validating the popup should get closed with out any error")
    #
    #                 elif s_row["productbook"].lower() == "no":
    #                     self.log.info("Product Book value No")
    #                     self.shipping_items_page.entryShippingItemsData(s_row["commodity"], s_row["piece"],
    #                                                                     s_row["weight"],
    #                                                                     s_row["unitcount"],
    #                                                                     s_row["UnitType"], s_row["className"],
    #                                                                     s_row["nfmc"],
    #                                                                     s_row["length"],
    #                                                                     s_row["width"],
    #                                                                     s_row["height"], s_row["weightUnits"],
    #                                                                     s_row["Dim"],
    #                                                                     stackable=s_row["stackable"],
    #                                                                     hazmat=s_row["hazmat"],
    #                                                                     hazgrp=s_row["HazmatGroup"],
    #                                                                     hzclass=s_row["HazmatClass"],
    #                                                                     # prefix=s_row["prefix"],
    #                                                                     hzcode=s_row["HazmatCode"],
    #                                                                     chemName=s_row["ChemicalName"],
    #                                                                     emergNo=s_row["emergencyContact"])
    #
    #                     # Clicking Add to Order Button
    #                     self.shipping_items_page.clickAddToOrderButton()
    #                     sales_order.sales_order_shipping.append(s_row)
    #
    #         else:
    #             self.log.info("Out of product index Condition")
    #             break
    #
    #     return sales_order
    #
    # def __select_carrier(self, carrier_data, dictionary) -> dict:
    #     self.test_status.mark(carrier_data[5], "Passed carrier result selection and break up correct.")
    #     self.log.info("Getting carrier Data : " + str(carrier_data))
    #
    #     quote_value = self.createquote.getIdentifierValue("quote")
    #     self.log.debug("Get Quote Value : " + str(quote_value))
    #     self.util.add_to_dictonary("quoteid", quote_value, dictionary)
    #
    #     # dictionary = self.util.add_to_dictonary("totalcost", carrierData[0])
    #     self.util.add_to_dictonary("totalcost", carrier_data[0], dictionary)
    #     self.util.add_to_dictonary("totalrevenue", carrier_data[1], dictionary)
    #     self.util.add_to_dictonary("selectedcarriername", carrier_data[6], dictionary)
    #     self.util.add_to_dictonary("transitdays", carrier_data[7], dictionary)
    #     appointmentdates = carrier_data[8].split("-")
    #     self.util.add_to_dictonary("originpickup", appointmentdates[0], dictionary)
    #     self.util.add_to_dictonary("etadelivery", appointmentdates[1], dictionary)
    #
    #     del carrier_data[5:]
    #     del carrier_data[:2]
    #
    #     self.util.add_to_dictonary("carrierbreakup", carrier_data, dictionary)
    #     self.log.info("Dict : " + str(dictionary))
    #
    #     return dictionary
    #
    # def __select_origin_destination(self, row, order_address_book):
    #     if row["addressbook"].lower() == "yes":
    #         self.log.info("Address Book value is Yes, Only address Book Validation")
    #         # Validating Code Here as address Book Already populated from Address Book
    #         address_index = row["sno"]
    #         for r_add in order_address_book:
    #             if int(address_index) >= int(r_add["sno"]):
    #                 if int(r_add["sno"]) == int(row["sno"]):
    #                     addr = r_add["address"]
    #                     self.log.info("Printing address : " + str(addr))
    #                     # Extracting city and Zip code
    #                     city_name = addr[:addr.rfind(",")]
    #                     zipcode = addr[addr.rfind(",") + 1:].strip()
    #                     self.log.info("city Name : " + str(city_name) + ", zip code : " + str(zipcode))
    #                     if r_add["addresssection"] == "origin":
    #                         self.log.info("Origin section ")
    #                         validate_ori_details = self.route_details_page.validateAddressContainerFields(
    #                             r_add["addresssection"],
    #                             city_name, zipcode,
    #                             r_add['readytime'],
    #                             r_add['closetime'],
    #                             r_add['company_name'],
    #                             r_add['contact_name'],
    #                             r_add['contact_phone'],
    #                             r_add['addressline1'],
    #                             r_add['addressline2'],
    #                             r_add['remarks'])
    #                         self.test_status.mark(validate_ori_details,
    #                                               "Verify Origin Address details after select from "
    #                                               "address book")
    #                     elif r_add["addresssection"] == "destination":
    #                         self.log.info("Destination section")
    #                         validate_dest_details = self.route_details_page.validateAddressContainerFields(
    #                             r_add["addresssection"],
    #                             city_name, zipcode,
    #                             r_add['readytime'],
    #                             r_add['closetime'],
    #                             r_add['company_name'],
    #                             r_add['contact_name'],
    #                             r_add['contact_phone'],
    #                             r_add['addressline1'],
    #                             r_add['addressline2'],
    #                             r_add['remarks'])
    #                         self.test_status.mark(validate_dest_details,
    #                                               "Verify Destination Address details after select "
    #                                               "from address book")
    #                     else:
    #                         self.log.error("Incorrect Address type passed in Method for address Book")
    #                         self.test_status.mark(False,
    #                                               "Incorrect Address type passed for Origin/Destination container")
    #             else:
    #                 self.log.info("Out of Address Book index Condition")
    #                 break
    #     else:
    #         self.log.info("Address Book value is No , Manual Entry")
    #         self.log.info("Passed Address Book Value is " + str(row["addressbook"]))
    #         self.route_details_page.enterRequiredAddressContainerDetails(row['oricompanyName'], row['oricontactName'],
    #                                                                      row['oriphone'],
    #                                                                      addressline1=row['oriaddline1'],
    #                                                                      addressline2=row["oriaddline2"],
    #                                                                      readytime=row["orireadytime"],
    #                                                                      closetime=row["oriclosetime"],
    #                                                                      remarks=row['pickupremarks']
    #                                                                      , field="origin")
    #         self.route_details_page.enterRequiredAddressContainerDetails(row['destcompanyname'], row['destcontactname'],
    #                                                                      row['destphone'],
    #                                                                      addressline1=row['destaddline1'],
    #                                                                      addressline2=row["destaddline2"],
    #                                                                      readytime=row["destreadytime"],
    #                                                                      closetime=row["destclosetime"],
    #                                                                      remarks=row['deliveryremarks'],
    #                                                                      field="destination")
    #
    # def __handle_address_book(self, row, order_address_book):
    #     self.log.info("Address Book value passed as Yes")
    #     ori_acc_list = []
    #     dest_acc_list = []
    #     add_index = row["sno"]
    #     self.log.info("get Index : " + str(add_index))
    #     for r_add in order_address_book:
    #         if int(add_index) >= int(r_add["sno"]):
    #             # self.log.info("sno : "+str(row["sno"]))
    #             if int(r_add["sno"]) == int(row["sno"]):
    #                 if r_add["addresssection"] == "origin":
    #                     # Origin address book and site type validation
    #                     self.route_details_page.searchAndSelectAddressFromAddressBook(r_add["company_name"],
    #                                                                                   r_add["company_name"],
    #                                                                                   field=r_add[
    #                                                                                       "addresssection"])
    #                     zip_value = r_add["address"][r_add["address"].rfind(",") + 1:].strip()
    #                     self.log.info("zipValue : " + str(zip_value))
    #                     validate_selected_add = self.route_details_page.verifySelectedAddressInTextField(zip_value,
    #                                                                                                    field=
    #                                                                                                    r_add[
    #                                                                                                        "addresssection"])
    #                     self.test_status.mark(validate_selected_add,
    #                                           "Validating the selected Address for Origin from Address Book")
    #                     # Verify site type for selected Address
    #                     selected_site_type_result = self.route_details_page.verifySiteTypeSelected(
    #                         r_add["site_type"],
    #                         r_add[
    #                             "addresssection"])
    #                     self.test_status.mark(selected_site_type_result,
    #                                           "Validating the Site type for shipper Address Book.")
    #                     ori_acc_list = self.createquote.getAccessorialsList(r_add["origin_accessorials"],
    #                                                                               r_add["addresssection"])
    #
    #                 elif r_add["addresssection"] == "destination":
    #                     # Destination address book and site type and accessorial
    #                     self.route_details_page.searchAndSelectAddressFromAddressBook(r_add["company_name"],
    #                                                                                   r_add["company_name"],
    #                                                                                   field=r_add[
    #                                                                                       "addresssection"])
    #                     zip_value = r_add["address"][r_add["address"].rfind(",") + 1:].strip()
    #                     self.log.info("zipValue : " + str(zip_value))
    #
    #                     validate_selected_add = self.route_details_page.verifySelectedAddressInTextField(zip_value,
    #                                                                                                    field=
    #                                                                                                    r_add[
    #                                                                                                        "addresssection"])
    #                     self.test_status.mark(validate_selected_add,
    #                                           "Validating the selected Address for Destination from Address Book")
    #                     # Verify site type for selected Address
    #                     selected_site_type_result = self.route_details_page.verifySiteTypeSelected(
    #                         r_add["site_type"],
    #                         r_add[
    #                             "addresssection"])
    #                     self.test_status.mark(selected_site_type_result,
    #                                           "Validating the Site type for consignee Address Book.")
    #                     dest_acc_list = self.createquote.getAccessorialsList(
    #                         r_add["destination_accessorials"],
    #                         r_add["addresssection"])
    #
    #                 else:
    #                     self.log.error("Incorrect address type passed")
    #                     self.test_status.mark(False,
    #                                           "Incorrect address type passed to select address Book method")
    #         else:
    #             self.log.info("out of address Book selection index")
    #             break
    #
    #     # Accessorial Verification
    #     self.log.info(
    #         "Origin Accessorial List : " + str(ori_acc_list) + " and Destination accessrial list: "
    #         + str(dest_acc_list))
    #     complete_acc_list = ori_acc_list + dest_acc_list
    #     self.log.info("Complete accessorial List : " + str(complete_acc_list))
    #
    #     selected_acc_app = self.createquote.selectedAccessorials("True")
    #     self.log.info("selected accessorial from Application : " + str(selected_acc_app))
    #     compare_acc_list = self.util.verifyListMatch(complete_acc_list, selected_acc_app)
    #     self.test_status.mark(compare_acc_list,
    #                           "Validating accessorial list display after select from address Book.")
    #
    # def __select_site_type(self, row):
    #     # Select Site type
    #     self.route_details_page.selectSiteType(row["originsitetype"], "origin")
    #     verify_selected_site_type_ori = self.route_details_page.verifySiteTypeSelected(row["originsitetype"],
    #                                                                                 "origin")
    #     self.test_status.mark(verify_selected_site_type_ori, "selected site type for Origin")
    #
    #     self.route_details_page.selectSiteType(row["destinationsitetype"], "destination")
    #     verify_selected_site_type_dest = self.route_details_page.verifySiteTypeSelected(row["destinationsitetype"],
    #                                                                                  "destination")
    #     self.test_status.mark(verify_selected_site_type_dest, "selected site type for Destination")
    #
    # def __select_accessorials(self, row):
    #     select_ori_acc = self.createquote.selectAccessorial(row["originacc"],
    #                                                               self.createquote.get_key_for_value(
    #                                                                   row["originacc"],
    #                                                                   row))
    #     self.test_status.mark(select_ori_acc, "Accessorial selected for Origin section")
    #     select_dest_acc = self.createquote.selectAccessorial(row["destinationacc"],
    #                                                                self.util.get_key_for_value(row["destinationacc"],
    #                                                                                      row))
    #     self.test_status.mark(select_dest_acc, "Validating selected accessorial for destination")
    #     selected_acc_display_list = self.createquote.selectedAccessorials("True")
    #     self.log.info(
    #         "Display selected accessorial List from Application : " + str(selected_acc_display_list))
