"""
@package base

Base Page class implementation
It implements methods which are common to all the pages throughout the application

This class needs to be inherited by all the page classes
This should not be used by creating object instances

Example:
    Class LoginPage(BasePage)
"""
import calendar
from datetime import datetime
from traceback import print_stack

from selenium.webdriver.common.by import By

from base.selenium_driver import SeleniumDriver
from pages.basictypes.basic_types_factory import BasicTypesFactory
from utilities.read_data import read_csv_data
from utilities.util import Util
from utilities.validators import Validators

QUOTE_PAGE_SPINNER = (By.XPATH, "//div[text()='Loading']")


class BasePage(SeleniumDriver):

    def __init__(self, driver, log):
        """
        Inits BasePage class

        Returns:
            None
        """
        super(BasePage, self).__init__(driver, log)
        self.util = Util(logger=log)
        self.validators = Validators(logger=log)
        self.basic_types_factory = BasicTypesFactory(driver=driver, log=log)

    # ========================Locators==========================================
    _datepicker = "']//div[@class='react-datepicker__input-container']//input"
    _datepicker_monthValue = "//div[@class='react-datepicker']//div[@class='react-datepicker__month-container']//div[" \
                             "contains(@class, 'current-month')] "
    _nextmonth_button = "//div[@class='react-datepicker']//button[@aria-label='Next Month']"
    _previousmonth_button = "//div[@class='react-datepicker']//button[@aria-label='Previous Month']"
    _dateTobeSelected_part1 = "//div[@class='react-datepicker']//div[contains(@class,'"
    _dateTobeSelected_part2 = "') and contains(@aria-label, '"

    def verify_page_title(self, title_to_verify):
        """
        Verify the page Title

        Parameters:
            title_to_verify: Title on the page that needs to be verified
        """
        try:
            actual_title = self.get_title()
            return Validators(logger=self.log).verify_text_contains(actual_title, title_to_verify)
        except Exception as E:
            self.log.error("Failed to get page title")
            self.log.error(f"Exception: {str(E)}")
            print_stack()
            return False

    def selectBasedOnText(self, locator, locatorType, selectValue):
        """
        this method select from drop down based on passed Text
        :param locator: Locator (webelement)
        :param locatorType: Locator Type
        :param selectValue: The value want to select in Drop down
        :return: selected Name
        """
        self.log.info(
            "Inside 'selectBasedOnText' Method passed value for this method is : " + str(selectValue))
        try:
            dropDownList = self.getElementList(locator, locatorType)
            # self.log.info("Drop down list : "+str(dropDownList))
            IndexValue = None
            for dropDownIndex in dropDownList:
                # self.log.info("...."+str(dropDownIndex.text))
                if dropDownIndex.text.isdigit() and selectValue.isdigit():
                    # self.log.info("inside Number compare")
                    if int(dropDownIndex.text) == int(selectValue):
                        indexName = dropDownIndex.text
                        self.log.info("element Text :" + str(indexName))
                        self.driver.execute_script("arguments[0].scrollIntoView();", dropDownIndex)
                        dropDownIndex.click()
                        IndexValue = indexName
                        # self.log.info("Digit Index Name :"+str(IndexValue))
                        return indexName
                else:
                    # self.log.info("inside String compare")
                    if dropDownIndex.text.lower() == selectValue.lower():
                        indexName = dropDownIndex.text
                        self.log.info("element Text :" + str(indexName))
                        self.driver.execute_script("arguments[0].scrollIntoView();", dropDownIndex)
                        dropDownIndex.click()
                        IndexValue = indexName
                        # self.log.info("String Index Name :" + str(IndexValue))
                        return indexName

            if IndexValue is None:
                self.log.error(
                    "From selectFromDDBasedOnText Method, Either passed text not found in drop down or passed"
                    " locator or locator type incorrect")
        except Exception as E:
            self.log.error("Exception occurred in selectBasedOnText Method and Exception is : " + str(E))

    def csvRowDataToList(self, columnName, fileName):
        """
        converting passed csv row data into list
        :return: row data as List
        """
        try:
            lists = []
            dataObj = read_csv_data(fileName)
            for row in dataObj:
                if not row[columnName] == "":
                    lists.append(row[columnName].strip())
            return lists
        except Exception as E:
            self.log.error("Error found, Either correct file name or column name not correct")
            self.log.error(E)

    def csvRowDataToList_SearchByColumnValues(self, columnName, searchValue, fileName):
        """
        converting passed csv row data into list. Get rows by searchValue in selected columnName
        :return: row data as List
        """
        try:
            lists = []
            dataObj = read_csv_data(fileName)
            for row in dataObj:
                if row[columnName] == searchValue:
                    lists.append(row)
            return lists
        except Exception as E:
            self.log.error("Error found, Either correct file name or column name not correct")
            self.log.error(E)

    def selectBasedOnValuePassed(self, locator, locatorType, selectValue):
        print("Inside selectBasedOnValuePassed method")
        try:
            indexName = ""
            self.log.info(
                "Inside 'selectBasedOnValuePassed' Method passed value for this method is : " + str(selectValue))
            print("Inside 'selectBasedOnValuePassed' Method passed value for this method is : " + str(selectValue))
            dropDownList = self.getElementList(locator, locatorType)
            self.log.info("Drop down list count : " + str(len(dropDownList)))
            self.log.info("Drop down list : " + str(dropDownList))
            for dropDownIndex in dropDownList:
                self.log.info("...." + str(dropDownIndex.text))
                if dropDownIndex.text.isdigit():
                    if selectValue in dropDownIndex.text.lower():
                        self.log.info(dropDownIndex.text)
                        # print("...." + str(dropDownIndex.text))
                        indexName = dropDownIndex.text
                        dropDownIndex.click()
                        break
                else:
                    if selectValue.lower() in dropDownIndex.text.lower():
                        self.log.info(dropDownIndex.text)
                        # print("...." + str(dropDownIndex.text))
                        indexName = dropDownIndex.text
                        dropDownIndex.click()
                        break
            return indexName
        except Exception as E:
            self.log.error(
                "From selectBasedOnValuePassed Method, passed text not found in drop down")
            self.log.error(E)

    def selectDateBasedOnPassedValue(self, no_of_days="", passedData="", dateFormat="%m/%d/%Y"):
        try:
            self.log.info("Inside selectDateBasedOnPassedValue Method, passed No if Days : " + str(no_of_days) +
                          ", Passed Data : " + str(passedData))
            if not no_of_days == "":
                if type(no_of_days) is str:
                    no_of_days = int(no_of_days)
                date_to_be_selected = self.util.get_date_based_on_holiday(no_of_days)
            elif not passedData == "":
                date_value = datetime.strptime(passedData, dateFormat)
                date_to_be_selected = date_value.strftime('%m/%d/%Y')
            self.log.info("Date value : " + str(date_to_be_selected))

            DateValue = str(date_to_be_selected).split('/')
            month_value = DateValue[0]
            day_value = DateValue[1]
            year_value = DateValue[2]
            month_name = calendar.month_name[int(month_value)]
            selected_month = calendar.month_name[int(month_value)] + " " + year_value

            while selected_month.lower() != self.getText(self._datepicker_monthValue, "xpath").lower():
                displayMonthYearValue = self.getText(self._datepicker_monthValue, "xpath")
                dispalyMonthYearArray = displayMonthYearValue.split(" ")
                displayMonth_name = dispalyMonthYearArray[0]
                displayYear_value = dispalyMonthYearArray[1]
                displayMonth_value = datetime.strptime(displayMonth_name, '%B').month
                self.log.info("selected month : " + str(selected_month) + ", displayMonthYearValue : " +
                              str(displayMonthYearValue) + ", month_value : " + str(month_value) +
                              ", displayMonth_value : " + str(displayMonth_value) + ", year_value : " +
                              str(year_value) + ", displayYear_value : " + str(displayYear_value))

                if (int(month_value) > int(displayMonth_value) and int(year_value) >= int(displayYear_value)) \
                        or (int(month_value) < int(displayMonth_value) and int(year_value) > int(displayYear_value)):
                    self.log.info("Next month selection")
                    self.elementClick(self._nextmonth_button, "xpath")
                elif int(month_value) < int(displayMonth_value) and int(year_value) <= int(displayYear_value):
                    self.log.info("previous month selection")
                    self.elementClick(self._previousmonth_button, "xpath")
                else:
                    self.log.info("Previous month selection")
                    self.elementClick(self._previousmonth_button, "xpath")

            chooseday = "0" + str(day_value)
            self.elementClick(
                self._dateTobeSelected_part1 + chooseday + self._dateTobeSelected_part2 + month_name + "')]",
                "xpath")

            return date_to_be_selected

        except Exception as E:
            self.log.error("Exception occurred in se Method and Exception is : " + str(E))

    def wait_for_page_loader_to_show_and_disappear(self, show_timeout=10, disappear_timeout=30):
        """
        Waiting for page loader first shows up and then disappear
        :return:
        """
        try:
            self.wait_for_element(locator=QUOTE_PAGE_SPINNER, event="display", timeout=show_timeout)
            self.wait_for_element(locator=QUOTE_PAGE_SPINNER, event="notdisplay", timeout=disappear_timeout)

        except Exception as E:
            self.log.error(f"Exception in wait_for_page_loader_to_show_and_disappear Method and Exception is: {str(E)}")
