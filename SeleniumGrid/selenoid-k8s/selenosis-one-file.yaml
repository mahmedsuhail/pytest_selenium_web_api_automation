apiVersion: v1
kind: Namespace
metadata:
  name: selenosis
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  namespace: selenosis
  name: selenosis
rules:
- apiGroups: [""]
  resources: ["*"]
  verbs: ["*"]
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: selenosis
  namespace: selenosis
roleRef:
  kind: ClusterRole
  name: selenosis
  apiGroup: rbac.authorization.k8s.io
subjects:
- kind: ServiceAccount
  namespace: selenosis
  name: default
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: selenosis-config
  namespace: selenosis
data:
  browsers.json: |
    {
    "chrome": {
        "defaultVersion": "89.0",
        "path": "/",
        "spec": {
          "env": [
            {
              "name": "SCREEN_RESOLUTION",
              "value": "1920x1080x24"
            },
            {
              "name": "ENABLE_VNC",
              "value": "true"
            }
          ]
        },
        "versions": {
            "89.0": {
                "image": "selenoid/vnc:chrome_89.0"
            },
            "88.0": {
                "image": "selenoid/vnc:chrome_88.0"
            },
            "87.0": {
                "image": "selenoid/vnc:chrome_87.0"
            }
        }
    }
    }
---
apiVersion: v1
kind: Service
metadata:
  name: selenosis
  namespace: selenosis
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: selenium
    port: 4444
    protocol: TCP
    targetPort: 4444
    nodePort: 31000
  selector:
    app: selenosis
  sessionAffinity: None
  type: LoadBalancer
status:
  loadBalancer: {}
---
apiVersion: v1
kind: Service
metadata:
  name: seleniferous
  namespace: selenosis
spec:
  selector:
    type: browser
  clusterIP: None
  publishNotReadyAddresses: true
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: selenosis
  namespace: selenosis
spec:
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
  replicas: 2
  selector:
    matchLabels:
      app: selenosis
  template:
    metadata:
      labels:
        app: selenosis
      namespace: selenosis
    spec:
      containers:
      - args: ["/selenosis", "--browsers-config", "/config/browsers.json", "--namespace", "selenosis", "--service-name", "seleniferous", "--browser-limit", "20", "--proxy-image", "alcounit/seleniferous:v0.0.2-develop"]
        image: alcounit/selenosis:v0.0.10-develop
        name: selenosis
        ports:
        - containerPort: 4444
          name: selenium
          protocol: TCP
        volumeMounts:
        - mountPath: /config
          name: browsers-config
        imagePullPolicy: IfNotPresent
        readinessProbe:
          httpGet:
            path: /healthz
            port: 4444
          periodSeconds: 2
          initialDelaySeconds: 3
        livenessProbe:
          httpGet:
            path: /healthz
            port: 4444
          periodSeconds: 2
          initialDelaySeconds: 3
      volumes:
      - name: browsers-config
        configMap:
          name: selenosis-config
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: selenoid-ui
  namespace: selenosis
spec:
  replicas: 1
  strategy:
    rollingUpdate:
      maxUnavailable: 0
      maxSurge: 1
  selector:
    matchLabels:
      app: selenoid-ui
  template:
    metadata:
      labels:
        app: selenoid-ui
      namespace: selenosis
    spec:
      containers:
        - args: ["--status-uri", "http://localhost:4444", "-webdriver-uri", "http://selenosis:4444"]
          name: selenoid-ui
          imagePullPolicy: IfNotPresent
          image: aerokube/selenoid-ui:1.10.1
          ports:
            - containerPort: 8080
              name: http
              protocol: TCP
        - args: ["--port", ":4444", "--selenosis-url", "http://selenosis:4444"]
          name: selenoid-ui-adapter
          imagePullPolicy: IfNotPresent
          image: alcounit/adaptee:v0.0.1-develop
      dnsPolicy: ClusterFirst
      restartPolicy: Always
      terminationGracePeriodSeconds: 0
---
apiVersion: v1
kind: Service
metadata:
  name: selenoid-ui
  namespace: selenosis
spec:
  externalTrafficPolicy: Cluster
  ports:
    - name: selenoid-ui
      port: 8080
      targetPort: 8080
      nodePort: 32000
      protocol: TCP
  selector:
    app: selenoid-ui
  sessionAffinity: None
  type: LoadBalancer
status:
  loadBalancer: {}