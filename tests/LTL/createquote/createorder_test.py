import sys

import pytest
import unittest2

import utilities.custom_logger as cl
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class CreateOrderTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    order = read_csv_data("salesOrderData.csv")
    order_shipping = read_csv_data("salesorder_shipping.csv")
    order_address_book = read_csv_data("salesorder_addressbook.csv")

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.lp = TMSLoginPage(self.driver, self.log)
        self.qop = CreateQuotePageLTL(self.driver, self.log)
        self.lop = TMSLogoutPage(self.driver, self.log)
        self.ts = ReportTestStatus(self.driver, self.log)
        self.si = ShippingItemsPage(self.driver, self.log)
        self.rdp = RouteDetailsPage(self.driver, self.log)
        self.car = CarriersPage(self.driver, self.log)
        self.sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)

    def createOrder(self):

        self.log.info("Inside test_createOrder Method")
        self.log.info("argument list : " + str(sys.argv))
        self.log.info("count : " + str(len(self.order)))
        if "smoke" in sys.argv:
            count = 1
        else:
            count = len(self.order)
        self.log.info("getting count : " + str(count))
        # -------------------- Added Login here due to logout bug ---------------------
        login = self.order[0]
        self.lp.login(login["username"], login["password"])
        self.lp.verifyLoginSuccessful()

        create_orders_result = self.sales_order_creator.create_orders(
            sales_order_data=self.order[:count],
            order_shipping=self.order_shipping,
            order_address_book=self.order_address_book)
        self.ts = create_orders_result[0]

        self.ts.mark_final("test_createOrder", True, "Validating the Create Order Test")
