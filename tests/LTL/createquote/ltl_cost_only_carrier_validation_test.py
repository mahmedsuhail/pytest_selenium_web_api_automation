import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage

from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.reportteststatus import ReportTestStatus
import unittest2
import pytest
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class LTLCostOnlyCarriersTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")
    login_data = read_csv_data("login_details.csv")
    shipping_data = read_csv_data("shippingItems_testing_data.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).login_page = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)

        self._login()
        self._select_cost_only()
        self._set_freight_date()
        self._select_and_verify_address()
        self._enter_shipping_data()

    def test_cost_only_carrier_section(self):
        self.log.info("Inside test_cost_only_carrier_section Method")

        all_carriers_breakup_charges_result = self.car.validateAllCarriersCostBreakupCharges()
        self.ts.mark(all_carriers_breakup_charges_result,
                     "Validated All carriers amount breakup charges without insurances")

        cheapest_carrier_result = self.car.validateCheapestCarrier("CostOnly")
        self.ts.mark(cheapest_carrier_result, "Validated Cheapest carrier")

        normal_carrier_ordering_result = self.car.validateNormalCarrierOrdering("CostOnly")
        self.ts.mark(normal_carrier_ordering_result, "Validated normal carrier ordering in ascending")

        fastest_carrier_result = self.car.validateFastestCarrier("CostOnly")
        self.ts.mark(fastest_carrier_result, "Validated fastest carrier")

        accessorial_and_site_type_breakup_result = self.car.accessorialAndSiteTypeBreakupValidation()
        self.ts.mark(accessorial_and_site_type_breakup_result, "Validated accessorial & site type breakup.")

        self.ts.mark_final("test_cost_only_carrier_section", True, "Validated carrier section for Cost Only Flow")

    def _login(self):
        credential = self.login_data[0]
        self.login_page.login(credential["userName"], credential["password"])
        self.login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")

    def _select_cost_only(self):
        self.log.info("Selecting Cost Only radio Button")
        self.qop.click_quote_order_icon()
        self.qop.clickCostOnlyRadio()

    def _set_freight_date(self):
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

    def _select_and_verify_address(self):
        self.log.info("Selecting address Method")
        for row in self.address_data:
            if row == self.address_data[0]:
                verifiedOriginTextResult = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'], "origin")
                self.ts.mark(verifiedOriginTextResult, "Populated origin text is same as selected from dropdown")

                # Get the site type for the selected origin address and validate it
                verify_origin_SiteTypeResult = self.rdp.verifySiteTypeSelected("business", "origin")
                self.ts.mark(verify_origin_SiteTypeResult, "site type should be business by default for direct "
                                                           "search of origin address")

                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                verifyOriginSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
                self.ts.mark(verifyOriginSiteTypeSelected, "Site Type selected is same as given for origin address")

            if row == self.address_data[1]:
                verifiedDestinationTextResult = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                      "destination")
                self.ts.mark(verifiedDestinationTextResult, "Populated destination text is same as selected from "
                                                            "dropdown")

                # Get the site type for the selected destination address and validate it
                verify_dest_SiteTypeResult = self.rdp.verifySiteTypeSelected("business", "destination")
                self.ts.mark(verify_dest_SiteTypeResult, "site type should be business by default for direct "
                                                         "search "
                                                         "of destination address")
                self.rdp.selectSiteType(row["destination_site_type"], "destination")
                verifyDestSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                             "destination")
                self.ts.mark(verifyDestSiteTypeSelected, "Site Type selected is same as given for destination "
                                                         "address")

    def _enter_shipping_data(self):
        self.log.info("Enter shipping Data into cost only Page")
        for row in self.shipping_data[:1]:
            self.log.info(row)
            self.log.info("inside for loop")
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])

            self.log.info("Inserted data in shipping items")

            # Add to Order Button Validation
            addToOrderButton = self.si.verifyButtonEnabled("AddToOrder")
            self.log.info("Add to Order Button Status : " + str(addToOrderButton))
            self.ts.mark(addToOrderButton, "Validating the Button Enabled or Not")
            # Clicking Add to Order Button
            self.si.clickAddToOrderButton()
