import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class RouteDetailsAddressTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    login_data = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):

        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).addressData = read_csv_data("address_details.csv")
        self.__login()
        self.__search_and_select_customer()

    def __login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __search_and_select_customer(self):
        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.addressData[0]["customer_name"], 1)

    def test_direct_address_search(self):
        self.log.info("Inside test_validate_direct_address_search method")
        for row in self.addressData:
            if row != self.addressData[2]:
                # Validate the searched address found in all the matching address
                origin_matching_list_result = self.rdp.validateMatchingAddressForDirectSearch(row["search_value"],
                                                                                              "origin")
                self.ts.mark(origin_matching_list_result, "searched address found in all the matching address")

                dest_matching_list_result = self.rdp.validateMatchingAddressForDirectSearch(row["search_value"],
                                                                                            "destination")
                self.ts.mark(dest_matching_list_result, "searched address found in all the matching address")

        for row in self.addressData:
            if row == self.addressData[0]:
                # Validate direct address search in origin textfield and select the required address from
                # dropdown list
                verified_origin_text_result = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'],
                                                                                    "origin")
                self.ts.mark(verified_origin_text_result, "Populated origin text is same as selected from dropdown")

                # Get the site type for the selected origin address and validate it
                verify_origin_Sitetype_result = self.rdp.verifySiteTypeSelected("business", "origin")
                self.ts.mark(verify_origin_Sitetype_result, "site type should be business by default for direct "
                                                            "search of origin address")

                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                verify_origin_sitetype_Selected = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
                self.ts.mark(verify_origin_sitetype_Selected, "Site Type selected is same as given for origin address")

                # Get the accessorials list for the selected origin address and validate it
                Origin_selectedAccessorialsList = self.qop.selectedAccessorials("True")

                originaccessorials_selected_result = \
                    self.rdp.validateAccessorialsForDirectSearch(Origin_selectedAccessorialsList)
                self.ts.mark(originaccessorials_selected_result, "accessorials should not be selected for direct "
                                                                 "search of origin address")

            if row == self.addressData[1]:
                # Validate direct address search in destination textfield and select the required address from
                # dropdown list
                verified_destination_text_result = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                         "destination")
                self.ts.mark(verified_destination_text_result, "Populated destination text is same as selected from "
                                                               "dropdown")

                # Get the site type for the selected destination address and validate it
                verify_dest_sitetype_result = self.rdp.verifySiteTypeSelected("business", "destination")
                self.ts.mark(verify_dest_sitetype_result, "site type should be business by default for direct "
                                                          "search of destination address")
                self.rdp.selectSiteType(row["destination_site_type"], "destination")
                verify_dest_sitetype_selected = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                                "destination")
                self.ts.mark(verify_dest_sitetype_selected, "Site Type selected is same as given for destination "
                                                            "address")

                # Get the accessorials list for the selected destination address and validate it
                Dest_selectedAccessorialsList = self.qop.selectedAccessorials("True")
                destAccessorials_selected_result = \
                    self.rdp.validateAccessorialsForDirectSearch(Dest_selectedAccessorialsList)
                self.ts.mark(destAccessorials_selected_result, "accessorials should not be selected for direct "
                                                               "search of destination address")

        # Validate Freight Date is set to System Date

        validate_date_result = self.rdp.validateSelectedFreightDate()
        self.ts.mark(validate_date_result, "Default Freight date is set to System Date")

        # Validate Freight Date is set to given Date
        validate_next_date_selected_result = self.rdp.dateSelection(3)
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

        # Validate Previous Date is Disabled
        validate_previous_date_disabled_result = self.rdp.checkDateIsDisabled(-2)
        self.ts.mark(validate_previous_date_disabled_result, "Previous Date is disabled")

        self.rdp.checkDatepickerClosed()

        self.ts.mark_final("test_validate_direct_address_search", True, "verified and validated direct search for "
                                                                        "origin and destination field")

    @pytest.mark.jira("LTL1-3851")
    @pytest.mark.slowtest
    def test_search_and_select_address_from_address_book(self):
        self.log.info("Inside test_search_and_select_address_from_address_book method")
        for row in self.addressData:
            # Validate address passed in origin address book is found in all the matching address list
            origin_matching_address_found = self.rdp.validateMatchingAddressFromAddressBook(row["search_value"],
                                                                                            row["city_name"], "origin")
            self.ts.mark(origin_matching_address_found, "Matching city name found in origin address list")

            # Search and select address from origin address book
            self.rdp.searchAndSelectAddressFromAddressBook(row["search_value"], row["company_name"], "origin")

            # Verify selected address from origin address book is populated in origin textfield
            origin_selected_textfield_result = self.rdp.verifySelectedAddressInTextField(row["city_name"], "origin")
            self.ts.mark(origin_selected_textfield_result, "Address populated in the origin textfield is same as "
                                                           "selected from origin address book")

            # Verify site type for selected origin address
            origin_selected_sitetype_result = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
            self.ts.mark(origin_selected_sitetype_result, "Site Type selected is same as given for origin address")

            origin_accessorials_list = self.qop.getAccessorialsList(row["origin_accessorials"], "origin")

            # Validate address passed in destination address book is found in all the matching address list
            destination_matching_address_found = self.rdp.validateMatchingAddressFromAddressBook(row["search_value"],
                                                                                                 row["city_name"],
                                                                                                 "destination")
            self.ts.mark(destination_matching_address_found, "Matching city name found in destination address list")

            # Search and select address from destination address book
            self.rdp.searchAndSelectAddressFromAddressBook(row["search_value"], row["company_name"], "destination")

            # Verify selected address from destination address book is populated in destination textfield
            destination_selected_textfield_result = self.rdp.verifySelectedAddressInTextField(row["city_name"],
                                                                                              "destination")
            self.ts.mark(destination_selected_textfield_result, "Address populated in the destination textfield is "
                                                                "same as selected from destination address book")

            # Verify site type for selected destination address
            destination_selected_sitetype_result = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                                   "destination")
            self.ts.mark(destination_selected_sitetype_result, "Site Type selected is same as given for destination "
                                                               "address")

            dest_accessorials_list = self.qop.getAccessorialsList(row["destination_accessorials"], "destination")

            final_accessorials_selected = self.qop.selectedAccessorials("True")
            origin_dest_accessorials = list(set().union(origin_accessorials_list, dest_accessorials_list))
            accessorialselected_result = self.rdp.validateAccessorials(origin_dest_accessorials,
                                                                       final_accessorials_selected)
            self.ts.mark(accessorialselected_result, "Accessorials selected for origin and destination is matched and "
                                                     "displayed correctly")

        self.ts.mark_final("test_validate_search_and_select_address_from_address_book", True,
                           "searchAndSelectAddressFromAddressBook is verified for origin and destination fields")
