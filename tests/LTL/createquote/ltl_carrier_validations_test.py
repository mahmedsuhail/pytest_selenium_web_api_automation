import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup")
class LTLCarrierValidationsTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    shipping_data = read_csv_data("shippingItems_testing_data.csv")
    address_data = read_csv_data("address_details.csv")
    util = Util(log)

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)

        self.login_data = read_csv_data("login_details.csv")

        self.__login()
        self.__navigate_to_create_quote_and_select_customer()
        self._set_freight_date()
        self.__select_route_details_and_accessorials()
        self.__enter_shipping_data()

    @pytest.mark.slowtest
    def test_ltl_carrier_validations(self):
        self.log.info("Inside test_ltl_carrier_validations Method")
        self.is_insurance_present = self.car.csvRowDataToList("isInsurancePresent", "carrier_details.csv")
        all_carriers_breakup_charges_result = self.car.validateAllCarriersBreakupCharges()
        self.ts.mark(all_carriers_breakup_charges_result,
                     "Validated All carriers amount breakup charges with & without insurances")

        cheapest_carrier_result = self.car.validateCheapestCarrier()
        self.ts.mark(cheapest_carrier_result, "Validated Cheapest carrier")

        normal_carrier_ordering_result = self.car.validateNormalCarrierOrdering()
        self.ts.mark(normal_carrier_ordering_result, "Validated normal carrier ordering in ascending")

        fastest_carrier_result = self.car.validateFastestCarrier()
        self.ts.mark(fastest_carrier_result, "Validated fastest carrier")

        carrier_search_result = self.car.searchCarrier("Freight")
        self.ts.mark(carrier_search_result, "Validated Search functionality")
        self.car.clearSearchCarrierField()

        select_carrier_by_index_result = self.car.selectCarrierByNameOrIndex(2, self.is_insurance_present[0])
        self.ts.mark(select_carrier_by_index_result[5], "Validated selection of carrier by index")

        carrier_download_icon_status_result = self.car.verifyCarrierDownloadIcon()
        self.ts.mark(carrier_download_icon_status_result, "Validated all the behaviour of the download carrier icon.")

        self.ts.mark_final("test_ltl_carrier_validations", True,
                           "Validated LTL carrier section in create quote page.")

    def __login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __navigate_to_create_quote_and_select_customer(self):
        self.qop.click_quote_order_icon()
        # Search and select customer
        self.qop.searchAndSelectCustomer(self.address_data[2]["customer_name"], 1)

    def __select_route_details_and_accessorials(self):
        # Data Entry in Route Details & Accessorial Section
        for row in self.address_data:
            if row == self.address_data[2]:
                # Select Zip code
                verified_origin_text_result = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'],
                                                                                    "origin")
                self.ts.mark(verified_origin_text_result, "Populated origin text is same as selected from dropdown")

                verified_destination_text_result = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                         "destination")

                self.ts.mark(verified_destination_text_result, "Populated destination text is same as "
                                                               "selected from dropdown")

                # Select Site type
                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                self.rdp.selectSiteType(row["destination_site_type"], "destination")

                # Select Accessorial
                self.qop.selectAccessorial(row["origin_accessorials"],
                                           self.util.get_key_for_value(row["origin_accessorials"], row))

    def __enter_shipping_data(self):
        # Data Entry in Shipping Items Section
        for row in self.shipping_data[:2]:
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])
            self.si.clickAddToOrderButton()

    def _set_freight_date(self):
        # current_date = self.qop.getCurrentDate()
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
