import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup_orders")
class CreateQuoteBasicFeatureTests(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    login_data = read_csv_data("login_details.csv")
    addressData = read_csv_data("address_details.csv")
    quoteData = read_csv_data("quote_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).login_page = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).lop = TMSLogoutPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.__login()

    def __login(self):
        credential = self.login_data[0]
        self.login_page.login(credential["userName"], credential["password"])
        self.login_page.verifyLoginSuccessful()

    def test_create_quote_icon_presence(self):
        self.log.info("Inside test_create_quote_icon_presence Method")
        self.qop.click_quote_order_icon()
        page_disabled = self.qop.verify_quote_page_is_disabled()
        self.ts.mark(page_disabled, "Quote page access.")
        result_2 = self.qop.validateQuoteOrderIcon()
        self.ts.mark(result_2, "Create Quote Icon is active")
        result_3 = self.qop.verifyQuotePage()
        self.ts.mark_final("test_create_quote_icon_presence", result_3, "Create Quote Board is displayed")

    def test_navigate_sales_order_page(self):
        self.log.info("Inside test_navigate_sales_order_page Method")
        for i in self.quoteData:
            if i == self.quoteData[1]:
                self.qop.searchAndSelectCustomer(i["customerName"])
                # Navigate to Sales Order Page
                self.sop.clickSalesOrderIcon()
                verify_navigation_msg = self.qop.verifyNavigationConfirmationText()
                self.ts.mark(verify_navigation_msg, "Validating Navigation confirmation Message")
                verify_cancel_navigation = self.qop.clickButtonInQuote("navigationcancel")
                self.ts.mark(verify_cancel_navigation, "Validating Navigation Cancel Button")
                self.sop.clickSalesOrderIcon()
                verify_confirm_navigation = self.qop.clickButtonInQuote("navigationconfirm")
                self.ts.mark(verify_confirm_navigation, "Validating Navigation Confirm Button")
                validate_so_page = self.login_page.verifyLoginSuccessful()
                self.ts.mark(validate_so_page, "Sales order board is displayed")

                # Navigate to Quote Order Page and validate the customer field and it should be empty
                self.qop.click_quote_order_icon()
                result_5 = self.qop.validateQuoteOrderIcon()
                self.ts.mark(result_5, "Create Quote Icon is active")

                result_6 = self.qop.verifyQuotePage()
                self.ts.mark(result_6, "Quote Board is displayed")

                empty_quote = self.qop.validateCreateQuoteContainerFields()
                self.ts.mark(empty_quote, "Data should present in Quote Container")
        self.ts.mark_final("test_navigate_sales_order_page", True, "Validating the Navigation feature from Quote Page.")

    def test_page_access_based_on_customer_search(self):
        self.log.info("Inside test_page_access_based_on_customer_search")

        self.qop.click_quote_order_icon()
        self.qop.search_customer(self.quoteData[1]["customerName"])
        self.qop.select_customer_from_list()
        self.qop.selectServiceType()
        page_enabled = self.qop.verify_quote_page_is_disabled()
        self.ts.mark(not page_enabled, "Until customer not selected page should disabled")

        # Validating all the accessorials are editable
        accessorial_result = self.qop.validateAllAccessorialsBasedOnSections()
        self.ts.mark(accessorial_result, "All the Accessorials names are correct based on the section")
        accessorial_editable_result = self.qop.validateAllAccessorialsEditable()
        self.ts.mark(accessorial_editable_result, "Validating all accessorial clickable or not.")
        self.ts.mark_final("test_page_access_based_on_customer_search", True,
                           "Validating the Page enabled only after customer select.")

    def test_customer_search_verification(self):
        self.log.info("inside test_customer_search_verification")

        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.addressData[0]["customer_name"], 1)
        self.qop.selectServiceType()

        index = 0
        for i in self.quoteData:
            self.log.info("Passed customer Name : " + i["customerName"])

            self.qop.search_customer(i["customerName"])
            search_list_result = self.qop.verifyCustomerListBasedOnSearchData(i["customerName"])
            self.ts.mark(search_list_result, "Verify the Search List Based on the customer name passed")

            customer_search_and_select_result = self.qop.select_customer_from_list(index)
            index += 2
            if customer_search_and_select_result is not None:
                self.ts.mark(customer_search_and_select_result[0],
                             "Verify passed customer name based on passed index able to select.")
            else:
                self.ts.mark(False, "Verify passed customer name based on passed index able to select")

        self.ts.mark_final("test_customer_search_verification", True,
                           "Verify Based on customer search data should populate and get selected.")
