import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup_orders")
class SavedQuoteTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    order = read_csv_data("salesOrderData.csv")
    order_shipping = read_csv_data("salesorder_shipping.csv")
    order_address_book = read_csv_data("salesorder_addressbook.csv")
    login_data = read_csv_data("login_details.csv")
    validators = Validators(logger=log)
    util = Util(logger=log)

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).login_page = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        self.__login()

    def __login(self):
        credential = self.login_data[0]
        self.login_page.login(credential["userName"], credential["password"])
        self.login_page.verifyLoginSuccessful()

    @pytest.mark.slowtest
    @pytest.mark.jira("LTL1-3487")
    def test_saved_quote(self):
        self.log.info("Inside test_saved_quote Method")
        self.qop.click_quote_order_icon()
        quote_navigation = False
        saved_quote_reference = ""
        quote_value = ""

        for row in self.order[:1]:

            selected_customer_result = self.qop.searchAndSelectCustomer(row["customername"], 1)
            self.log.info("selected_customer_result : " + str(selected_customer_result))

            # Validate Freight Date is set to given Date
            select_date = int(row["selectdate"])
            validate_next_date_selected_result = self.rdp.dateSelection(select_date)
            self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
            get_selected_freight_date_result = self.rdp.getSelectedFreightDate()
            self.log.info("get_selected_freight_date_result : " + str(get_selected_freight_date_result))

            if row["addressbook"].lower() == "yes":
                self.log.info("Address Book value passed as Yes")
                add_index = row["sno"]
                self.log.info("get Index : " + str(add_index))
                for r_add in self.order_address_book:
                    if int(add_index) >= int(r_add["sno"]):
                        if int(r_add["sno"]) == int(row["sno"]):
                            if r_add["addresssection"] == "origin":
                                # Origin address book and site type validation
                                self.rdp.searchAndSelectAddressFromAddressBook(r_add["company_name"],
                                                                               r_add["company_name"],
                                                                               field=r_add["addresssection"])
                            elif r_add["addresssection"] == "destination":
                                # Destination address book and site type and accessorial
                                self.rdp.searchAndSelectAddressFromAddressBook(r_add["company_name"],
                                                                               r_add["company_name"],
                                                                               field=r_add["addresssection"])
                            else:
                                self.log.error("Incorrect address type passed")
                                self.ts.mark(False, "Incorrect address type passed to select address Book method")
                    else:
                        self.log.info("out of address Book selection index")
                        break

                complete_acc_list = self.qop.selectedAccessorials("True")
                self.log.info("Accessorial List Selected from address book : " + str(complete_acc_list))
            else:
                self.log.info("Address Book value passed as No")
                zip_value = row["orizip"][row["orizip"].rfind(",") + 1:].strip()
                self.log.info("zip_value : " + str(zip_value))

                origin_select_result = self.rdp.directSearchAndSelectAddress(zip_value, "origin")
                self.ts.mark(origin_select_result, "Validating the passed and selected address for origin.")
                zip_dest = row["destzip"][row["destzip"].rfind(",") + 1:].strip()
                destination_select_result = self.rdp.directSearchAndSelectAddress(zip_dest, "destination")
                self.ts.mark(destination_select_result, "Validating the passed and selected address for destination.")

                # Select Site type
                self.rdp.selectSiteType(row["originsitetype"], "origin")
                self.rdp.selectSiteType(row["destinationsitetype"], "destination")

                # Accessorial Selection
                select_ori_acc = self.qop.selectAccessorial(row["originacc"],
                                                            self.util.get_key_for_value(row["originacc"], row))
                self.ts.mark(select_ori_acc, "Accessorial selected for Origin section")
                select_dest_acc = self.qop.selectAccessorial(row["destinationacc"],
                                                             self.util.get_key_for_value(row["destinationacc"], row))
                self.ts.mark(select_dest_acc, "Validating selected accessorial for destination")

                complete_acc_list = self.qop.selectedAccessorials("True")
                self.log.info("Accessorial List Selected : " + str(complete_acc_list))

            # adding Shipping Data
            shipping_index = row["sno"]
            self.log.info("index value : " + str(shipping_index))
            for s_row in self.order_shipping:
                self.log.info("adding shipping Items")
                self.log.info("Number : " + str(s_row["sno"]))
                if int(shipping_index) >= int(s_row["sno"]):
                    if int(row["sno"]) == int(s_row["sno"]):
                        self.si.entryShippingItemsData(s_row["commodity"], s_row["piece"], s_row["weight"],
                                                       s_row["unitcount"],
                                                       s_row["UnitType"], s_row["className"], s_row["nfmc"],
                                                       s_row["length"],
                                                       s_row["width"],
                                                       s_row["height"], s_row["weightUnits"], s_row["Dim"],
                                                       stackable=s_row["stackable"], hazmat=s_row["hazmat"],
                                                       hazgrp=s_row["HazmatGroup"], hzclass=s_row["HazmatClass"],
                                                       hzcode=s_row["HazmatCode"], chemName=s_row["ChemicalName"],
                                                       emergNo=s_row["emergencyContact"])
                        self.si.clickAddToOrderButton()
                else:
                    self.log.info("out of shipping index condition")
                    break
            carrier_data = self.car.getSelectedCarrierData("2", row["carriers"], row["selectcarrierrate"])
            if carrier_data is not None:
                quote_value = self.qop.getIdentifierValue("quote")
                self.log.debug("Get Quote Value : " + str(quote_value))
                saved_quote_reference = "SQREF" + str(quote_value)
                self.log.info("Saved Quote Reference : " + str(saved_quote_reference))
                save_order_button_status = self.qop.verifySaveOrderButtonEnabled()
                self.ts.mark(save_order_button_status, "Save Order Button is in enabled state.")
                self.qop.defaultValidationsSaveOrder()
                self.qop.enterReferenceForSaveOrder(saved_quote_reference)
                quote_navigation = True
            else:
                self.ts.mark(False, "Carrier list not populated")

            if quote_navigation:
                self.sop.clickSalesOrderIcon()
                verify_confirm_navigation = self.qop.clickButtonInQuote("navigationconfirm")
                self.ts.mark(verify_confirm_navigation, "Validating Navigation Confirm Button")
                date_value = self.util.get_current_date(time_zone="US/Central")
                self.log.info("Get Current Date : " + str(date_value))
                self.sop.soBoardLoadWait()
                filter_result = self.sop.clickAndValidateShipmentStatusFilter("Quotes")
                self.ts.mark(filter_result,
                             "SO Board LTL Filter Tab Quotes is clicked and the same is in selected state.")

                save_quote_ref_status_in_filter = self.sop.applyFilterBasedOnColumnName("referenceNumber",
                                                                                        saved_quote_reference)
                self.ts.mark(save_quote_ref_status_in_filter,
                             "Saved quote reference number present in quotes tab filter.")

                validated_carrier_in_quotes_tab_in_so_board = self.sop.columnValueCompare("carrier", carrier_data[2],
                                                                                          quote_value)
                self.ts.mark(validated_carrier_in_quotes_tab_in_so_board, "Validated carrier name in quotes tab in so "
                                                                          "board.")

                piece_value = 0
                pallet_value = 0
                weight_value = 0
                shipping_index = row["sno"]
                for s_row in self.order_shipping:
                    self.log.info("Number : " + str(s_row["sno"]))
                    if int(shipping_index) >= int(s_row["sno"]):
                        if int(row["sno"]) == int(s_row["sno"]):
                            piece_value = piece_value + round(float(s_row["piece"]))
                            pallet_value = pallet_value + round(float(s_row["unitcount"]))
                            get_weight_value = self.sop.getConvertedWeightValue(s_row["weight"], s_row["weightUnits"])
                            self.log.info(
                                "weight_value : " + str(weight_value) + ",get_weight_value : " + str(get_weight_value))
                            weight_value = weight_value + round(get_weight_value)
                    else:
                        break

                self.log.info("Total Pcs Value : " + str(piece_value))
                self.log.info("Total Unitcount Value : " + str(pallet_value))
                self.log.info("Weight Value : " + str(weight_value))

                validated_cus_contactname_in_quotes_tab_in_so_board = self.sop. \
                    columnValueCompare("contactName", selected_customer_result[3], quote_value)
                self.ts.mark(validated_cus_contactname_in_quotes_tab_in_so_board,
                             "Validated customer contact name in quotes tab in so board.")

                # commented due to bug LTL1-2932 validated_cost_in_quotes_tab_in_so_board =
                # self.sop.columnValueCompare("cost", carrierData[0], quote_value) self.ts.mark(
                # validated_cost_in_quotes_tab_in_so_board, "Validated cost in quotes tab in so board.")

                validated_customer_in_quotes_tab_in_so_board = self.sop.columnValueCompare("customer",
                                                                                           selected_customer_result[1],
                                                                                           quote_value)
                self.ts.mark(validated_customer_in_quotes_tab_in_so_board,
                             "Validated customer name in quotes tab in so board.")

                if row["addressbook"].lower() == "yes":
                    self.log.info("Address Book value passed as Yes")
                    add_index = row["sno"]
                    self.log.info("get Index : " + str(add_index))
                    for r_add in self.order_address_book:
                        if int(add_index) >= int(r_add["sno"]):
                            if int(r_add["sno"]) == int(row["sno"]):
                                if r_add["addresssection"] == "origin":
                                    origin = r_add["address"].split(", ")
                                    validated_ori_city_in_quotes_tab_in_so_board = self.sop.columnValueCompare(
                                        "originCity",
                                        origin[0],
                                        quote_value)
                                    self.ts.mark(validated_ori_city_in_quotes_tab_in_so_board,
                                                 "Validated origin city in quotes tab in so board.")

                                    validated_ori_state_in_quotes_tab_in_so_board = self.sop.columnValueCompare(
                                        "originState",
                                        origin[1],
                                        quote_value)
                                    self.ts.mark(validated_ori_state_in_quotes_tab_in_so_board,
                                                 "Validated origin state in quotes tab in so board.")

                                    validated_ori_zip_in_quotes_tab_in_so_board = self.sop.columnValueCompare(
                                        "originZip",
                                        origin[2],
                                        quote_value)
                                    self.ts.mark(validated_ori_zip_in_quotes_tab_in_so_board,
                                                 "Validated origin zip in quotes tab in so board.")

                                elif r_add["addresssection"] == "destination":
                                    destination = r_add["address"].split(", ")
                                    validated_des_city_in_quotes_tab_in_so_board = self.sop.columnValueCompare(
                                        "destinationCity", destination[0], quote_value)
                                    self.ts.mark(validated_des_city_in_quotes_tab_in_so_board,
                                                 "Validated destination city in quotes tab in so board.")

                                    validated_des_state_in_quotes_tab_in_so_board = self.sop.columnValueCompare(
                                        "destinationState", destination[1], quote_value)
                                    self.ts.mark(validated_des_state_in_quotes_tab_in_so_board,
                                                 "Validated destination state in quotes tab in so board.")

                                    validated_des_zip_in_quotes_tab_in_so_board = self.sop.columnValueCompare(
                                        "destinationZip",
                                        destination[2],
                                        quote_value)
                                    self.ts.mark(validated_des_zip_in_quotes_tab_in_so_board,
                                                 "Validated destination zip in quotes tab in so board.")

                                else:
                                    self.log.error("Incorrect address type passed")
                                    self.ts.mark(False, "Incorrect address type passed to select address Book method")
                        else:
                            self.log.info("out of address Book selection index")
                            break
                else:
                    destination = row["destzip"].split(", ")
                    origin = row["orizip"].split(", ")
                    validated_des_city_in_quotes_tab_in_so_board = self.sop.columnValueCompare("destinationCity",
                                                                                               destination[0],
                                                                                               quote_value)
                    self.ts.mark(validated_des_city_in_quotes_tab_in_so_board,
                                 "Validated destination city in quotes tab in so board.")

                    validated_des_state_in_quotes_tab_in_so_board = self.sop.columnValueCompare("destinationState",
                                                                                                destination[1],
                                                                                                quote_value)
                    self.ts.mark(validated_des_state_in_quotes_tab_in_so_board,
                                 "Validated destination state in quotes tab in so board.")

                    validated_des_zip_in_quotes_tab_in_so_board = self.sop.columnValueCompare("destinationZip",
                                                                                              destination[2],
                                                                                              quote_value)
                    self.ts.mark(validated_des_zip_in_quotes_tab_in_so_board,
                                 "Validated destination zip in quotes tab in so board.")

                    validated_ori_city_in_quotes_tab_in_so_board = self.sop.columnValueCompare("originCity", origin[0],
                                                                                               quote_value)
                    self.ts.mark(validated_ori_city_in_quotes_tab_in_so_board,
                                 "Validated origin city in quotes tab in so board.")

                    validated_ori_state_in_quotes_tab_in_so_board = self.sop.columnValueCompare("originState",
                                                                                                origin[1],
                                                                                                quote_value)
                    self.ts.mark(validated_ori_state_in_quotes_tab_in_so_board,
                                 "Validated origin state in quotes tab in so board.")

                    validated_ori_zip_in_quotes_tab_in_so_board = self.sop.columnValueCompare("originZip", origin[2],
                                                                                              quote_value)
                    self.ts.mark(validated_ori_zip_in_quotes_tab_in_so_board,
                                 "Validated origin zip in quotes tab in so board.")

                calculated_expiry_date = self.util.getDateValueWithoutHolidayAndWeekends(7, date_value)
                self.log.info("calculated_expiry_date : " + str(calculated_expiry_date))
                validated_expiry_date_in_quotes_tab_in_so_board = self.sop.columnValueCompare("expireDate",
                                                                                              calculated_expiry_date,
                                                                                              quote_value)
                self.ts.mark(validated_expiry_date_in_quotes_tab_in_so_board,
                             "Validated expiry date in quotes tab in so board.")

                validated_generated_on_date_in_quotes_tab_in_so_board = self.sop.columnValueCompare("generatedOn",
                                                                                                    date_value,
                                                                                                    quote_value)
                self.ts.mark(validated_generated_on_date_in_quotes_tab_in_so_board,
                             "Validated generated on date in quotes tab in so board.")

                validated_mode_in_quotes_tab_in_so_board = self.sop.columnValueCompare("mode", "LTL", quote_value)
                self.ts.mark(validated_mode_in_quotes_tab_in_so_board,
                             "Validated Mode of shipment in quotes tab in so board.")

                validated_pallet_count_in_tab_in_so_board = self.sop.columnValueCompare("palletCount", pallet_value,
                                                                                        quote_value)
                self.ts.mark(validated_pallet_count_in_tab_in_so_board,
                             "Validated pallet count in quotes tab in so board.")

                validated_pieces_count_in_tab_in_so_board = self.sop.columnValueCompare("pieceCount", piece_value,
                                                                                        quote_value)
                self.ts.mark(validated_pieces_count_in_tab_in_so_board,
                             "Validated piece count in quotes tab in so board.")

                validated_quote_num_in_quotes_tab_in_so_board = self.sop.columnValueCompare("quoteBK", quote_value,
                                                                                            quote_value)
                self.ts.mark(validated_quote_num_in_quotes_tab_in_so_board,
                             "Validated quote number in quotes tab in so board.")

                validated_quote_date_in_quotes_tab_in_so_board = self.sop.columnValueCompare("quoteDate", date_value,
                                                                                             quote_value)
                self.ts.mark(validated_quote_date_in_quotes_tab_in_so_board,
                             "Validated quote date in quotes tab in so board.")

                # commented due to bug LTL1-2932 validated_revenue_in_quotes_tab_in_so_board =
                # self.sop.columnValueCompare("revenue", carrierData[1], quote_value) self.ts.mark(
                # validated_revenue_in_quotes_tab_in_so_board, "Validated revenue in quotes tab in so board.")

                validated_weight_in_quotes_tab_in_so_board = self.sop.columnValueCompare("weight", weight_value,
                                                                                         quote_value)
                self.ts.mark(validated_weight_in_quotes_tab_in_so_board, "Validated weight in quotes tab in so board.")

                # Navigating to Saved Quote Popup and its Validations
                open_saved_quote_popup_result = self.sop.openSavedQuotePopup(quote_value)
                self.ts.mark(open_saved_quote_popup_result, "Validated the navigation to saved quote popup")

                saved_quote_popup_pickup_date_result = self.sop.savedQuotePopupPickupDateValidation(
                    get_selected_freight_date_result)
                self.ts.mark(saved_quote_popup_pickup_date_result, "Validated pickup date in saved quote popup.")

                saved_quote_popup_customer_contactname_result = self.sop.savedQuotePopupCustomerContactNameValidation(
                    selected_customer_result[3])
                self.ts.mark(saved_quote_popup_customer_contactname_result,
                             "Validated customer contact name in saved quote popup.")

                shipping_val = True
                for si_row in self.order_shipping:
                    self.log.info("row value si :" + str(si_row["sno"]) + " ,main row : " + str(row["sno"]))
                    if int(si_row["sno"]) <= int(row["sno"]):
                        self.log.info("shipping row No. : " + str(si_row["sno"]))
                        if int(si_row["sno"]) == int(row["sno"]):
                            validate_shipping_data = self.sop.validateShippingItemDataInSavedQuotePopup(
                                si_row["commodity"],
                                si_row["weight"],
                                si_row["unitcount"],
                                si_row["piece"],
                                si_row["length"],
                                si_row["width"],
                                si_row["height"],
                                si_row["hazmat"],
                                si_row["weightUnits"],
                                si_row["UnitType"],
                                si_row["Dim"])

                            self.log.info("Shipping Data Value : " + str(validate_shipping_data))
                            shipping_val = shipping_val and validate_shipping_data
                        self.log.info("Reading Logs")
                    else:
                        self.log.debug("Out side expected condition")
                        break
                self.ts.mark(shipping_val, "Validated Shipping Line Item Data in Saved Quote Popup.")

                self.sop.clickButtonInSavedQuotePopup("BOOKIT")
                quote_page_displayed_result = self.car.createQuotePageLoadWhenNavFromQuotesTab()
                self.ts.mark(quote_page_displayed_result, "Validated quote page is displayed and carriers are loaded.")

                if quote_page_displayed_result:
                    # Data validation in create quote page
                    origin_zip_value = ""
                    destination_zip_value = ""
                    origin_city_value = ""
                    destination_city_value = ""
                    if row["addressbook"].lower() == "yes":
                        self.log.info("Address Book value passed as Yes")
                        add_index = row["sno"]
                        self.log.info("get Index : " + str(add_index))
                        for r_add in self.order_address_book:
                            if int(add_index) >= int(r_add["sno"]):
                                if int(r_add["sno"]) == int(row["sno"]):
                                    if r_add["addresssection"] == "origin":
                                        origin_zip_value = r_add["address"][r_add["address"].rfind(",") + 2:]
                                        origin_city_value = r_add["address"][:r_add["address"].rfind(",")]

                                        # Site Type Validation (Origin)
                                        selected_site_type_result = self.rdp. \
                                            verifySiteTypeSelected(r_add["site_type"], r_add["addresssection"])
                                        self.ts.mark(selected_site_type_result,
                                                     "Validated the Site type for shipper Address Book.")

                                        # Zip Validation (Origin)
                                        validate_selected_add = self.rdp.verifySelectedAddressInTextField(
                                            r_add["address"], field=r_add["addresssection"])
                                        self.ts.mark(validate_selected_add,
                                                     "Validated the selected Address for Origin from Address Book")

                                    elif r_add["addresssection"] == "destination":
                                        destination_zip_value = r_add["address"][r_add["address"].rfind(",") + 2:]
                                        destination_city_value = r_add["address"][:r_add["address"].rfind(",")]

                                        # Site Type Validation (Destination)
                                        selected_site_type_result = self.rdp. \
                                            verifySiteTypeSelected(r_add["site_type"], r_add["addresssection"])
                                        self.ts.mark(selected_site_type_result,
                                                     "Validated the Site type for consignee Address Book.")

                                        # Zip Validation (Destination)
                                        validate_selected_add = self.rdp.verifySelectedAddressInTextField(
                                            r_add["address"], field=r_add["addresssection"])
                                        self.ts.mark(validate_selected_add,
                                                     "Validated the selected Address for Destination from Address Book")
                                    else:
                                        self.log.error("Incorrect address type passed")
                                        self.ts.mark(False,
                                                     "Incorrect address type passed to select address Book method")
                            else:
                                self.log.info("out of address Book selection index")
                                break

                        selected_acc_inquotepagenavfromquotetab = self.qop.selectedAccessorials("True")
                        self.log.info(
                            "selected accessorial from Application in create quote page when navigated from Quote "
                            "page: " + str(
                                selected_acc_inquotepagenavfromquotetab))
                        compare_acc_list = self.validators.verify_list_match(complete_acc_list,
                                                                             selected_acc_inquotepagenavfromquotetab)
                        self.ts.mark(compare_acc_list,
                                     "Validated accessorial list display after navigated to create quote page from "
                                     "Quotes Tab from Address Book.")
                    else:
                        self.log.info("Address Book value passed as No")
                        origin_zip_value = row["orizip"][row["orizip"].rfind(",") + 2:]
                        destination_zip_value = row["destzip"][row["destzip"].rfind(",") + 2:]
                        origin_city_value = row["orizip"][:row["orizip"].rfind(",")]
                        destination_city_value = row["destzip"][:row["destzip"].rfind(",")]

                        # Site Type Validation (Origin & Destination)
                        verify_selected_sitetype_ori = self.rdp.verifySiteTypeSelected(row["originsitetype"], "origin")
                        self.ts.mark(verify_selected_sitetype_ori, "Validated selected site type for Origin")
                        verify_selected_sitetype_dest = self.rdp.verifySiteTypeSelected(row["destinationsitetype"],
                                                                                        "destination")
                        self.ts.mark(verify_selected_sitetype_dest, "Validated selected site type for Destination")

                        # Zip Code Validation (Origin & Destination)
                        self.log.info("Origin Zip Value : " + str(origin_zip_value) + "Destination Zip Value : " + str(
                            destination_zip_value))
                        validate_selected_add = self.rdp.verifySelectedAddressInTextField(origin_zip_value, "origin")
                        self.ts.mark(validate_selected_add,
                                     "Validated the selected zip code for Origin from direct entry.")
                        validate_selected_add = self.rdp.verifySelectedAddressInTextField(destination_zip_value,
                                                                                          "destination")
                        self.ts.mark(validate_selected_add,
                                     "Validated the selected zip code for Destination from direct entry.")

                        # Accessorial List Validation
                        selected_acc_inquotepagenavfromquotetab = self.qop.selectedAccessorials("True")
                        self.log.info(
                            "selected accessorial from Application in create quote page when navigated from Quote "
                            "page: " + str(
                                selected_acc_inquotepagenavfromquotetab))
                        compare_acc_list = self.validators.verify_list_match(complete_acc_list,
                                                                             selected_acc_inquotepagenavfromquotetab)
                        self.ts.mark(compare_acc_list,
                                     "Validated accessorial list display after navigated to create quote page from "
                                     "Quotes Tab.")

                        # Freight Date validation
                    selected_freight_date_result = \
                        self.rdp.validateSelectedFreightDate(get_selected_freight_date_result)
                    self.ts.mark(selected_freight_date_result, "Validated freight date is current date.")

                    # Saved Quote Reference validation in identifier section
                    saved_quote_reference_identifier = self.qop.getIdentifierValue("reference")
                    validate_ref_identifier_result = self.validators.verify_text_match(saved_quote_reference_identifier,
                                                                                       saved_quote_reference)
                    self.ts.mark(validate_ref_identifier_result,
                                 "Validated the Saved quote Reference Number in Identifier section in Create quote "
                                 "page.")

                    self.log.info("origin_city_value : " + str(origin_city_value) + "origin_zip_value : " + str(
                        origin_zip_value) + "destination_city_value : " + str(
                        destination_city_value) + "destination_zip_value : " + str(destination_zip_value))
                    # Origin Container Validation
                    validated_origin_container_result = self.rdp.validateAddressContainerFields("origin",
                                                                                                origin_city_value,
                                                                                                origin_zip_value, "",
                                                                                                "",
                                                                                                "",
                                                                                                "", "", "", "", "")
                    self.ts.mark(validated_origin_container_result,
                                 "Validated Origin Address Container fields are empty ")

                    # Destination Container Validation
                    validated_destination_container_result = self.rdp. \
                        validateAddressContainerFields("destination", destination_city_value, destination_zip_value, "",
                                                       "", "", "", "", "", "", "")
                    self.ts.mark(validated_destination_container_result,
                                 "Validated Destination Address Container fields are empty ")

                    # Shipping line items validation & SO Container Validation
                    # # Commented due to bug LTL1-3055
                    # if row["selectcarrierrate"].lower() == "no":
                    #     shipmentValue = "4000"
                    # else:
                    #     shipmentValue = row["shipmentvalue"]
                    shipment_value = "4000"
                    validated_shipment_value_result = self.si.verifyTotalShipmentValue(shipment_value)
                    self.ts.mark(validated_shipment_value_result,
                                 "Validated shipment value in create quote page is same as during saved quote after "
                                 "navigating from Quote Tab.")

                    listof_calculated_data = []
                    shipping_index = row["sno"]
                    self.log.info("index value : " + str(shipping_index))
                    for s_row in self.order_shipping:
                        self.log.info("Number : " + str(s_row["sno"]))
                        if int(shipping_index) >= int(s_row["sno"]):
                            if int(row["sno"]) == int(s_row["sno"]):
                                commodity = s_row["commodity"]
                                index_value_based_on_commodity = self.si.getShippingItemIndexBasedOnCommodity(commodity)
                                self.log.info("index_value_based_on_commodity : " + str(index_value_based_on_commodity))
                                self.si.editShippingItem(index_value_based_on_commodity)
                                # preparing the data to validate SO Items
                                so_prepared_data = self.si.getEachShippingItemDataForSOItem()
                                listof_calculated_data.append(so_prepared_data)
                                self.log.info("getItemDataFromShippingItem : " + str(so_prepared_data))
                                get_weight_value = self.sop.getConvertedWeightValue(s_row["weight"],
                                                                                    s_row["weightUnits"])
                                if s_row["Dim"] == "feet":
                                    length_value = str(round(float(s_row["length"])) * 12)
                                    width_value = str(round(float(s_row["width"])) * 12)
                                    height_value = str(round(float(s_row["height"])) * 12)
                                else:
                                    length_value = str(round(float(s_row["length"])))
                                    width_value = str(round(float(s_row["width"])))
                                    height_value = str(round(float(s_row["height"])))
                                valid_all_shipping_data = self.si.verifyCombinedShippingItemData(s_row["commodity"],
                                                                                                 s_row["piece"],
                                                                                                 str(get_weight_value),
                                                                                                 s_row["unitcount"],
                                                                                                 s_row["UnitType"],
                                                                                                 s_row["className"],
                                                                                                 s_row["nfmc"],
                                                                                                 length_value,
                                                                                                 width_value,
                                                                                                 height_value,
                                                                                                 "lbs",
                                                                                                 "Inches",
                                                                                                 stackable="No",
                                                                                                 hazmat="No",
                                                                                                 # given as no,
                                                                                                 # Commented due to
                                                                                                 # bug LTL1-3016
                                                                                                 hzmcode=s_row[
                                                                                                     "HazmatCode"],
                                                                                                 chemName=s_row[
                                                                                                     "ChemicalName"],
                                                                                                 emergNo=s_row[
                                                                                                     "emergencyContact"],
                                                                                                 hzGrp=s_row[
                                                                                                     "HazmatGroup"],
                                                                                                 hzclass=s_row[
                                                                                                     "HazmatClass"],
                                                                                                 prefix=s_row["prefix"])
                                self.ts.mark(valid_all_shipping_data,
                                             "Validated all shipping line items data in Create quote page after "
                                             "navigating from Quotes Tab.")
                        else:
                            self.log.debug("Out of index condition")
                            break

                    calculated_value_from_so_item = self.si.calculateTotalSOItemValue(listof_calculated_data)
                    summary_result = self.si.verifySOItemSummary(calculated_value_from_so_item)
                    self.ts.mark(summary_result,
                                 "Validated Sales Order Container Summary in Create quote page after navigating from "
                                 "Quotes Tab.")

                    # # Commented due to bug LTL1-3008
                    # # Carrier checkbox validation
                    # carrierCheckboxSelectionResult = self.car.carrierCheckboxValidation(carrierData[4])
                    # self.ts.mark(carrierCheckboxSelectionResult, "Validated carrier checkbox selection")
                    #
                    # # Commented due to bug LTL1-3009
                    # # Save Order Button in enabled state validation
                    # saveOrderButtonStatus = self.qop.verifySaveOrderButtonEnabled()
                    # self.ts.mark(saveOrderButtonStatus, "Validated Save Order Button is in enabled state.")
                else:
                    self.log.error("create quote page didnot loaded when navigated from Quotes tab.")
            else:
                self.log.error("Saved Quote didnot happened,carriers did not populated.")

        self.ts.mark_final("test_saved_quote", True, "Validated Saved Quote Feature.")
