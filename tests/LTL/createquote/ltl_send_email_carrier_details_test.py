import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.LTL.sendingMail.sendMail_page import SendMailPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class LTLSendEmailCarrierDetailsTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).email = SendMailPage(self.driver, self.log)
        self.rdp = RouteDetailsPage(self.driver, self.log)
        type(self).shipping_data = read_csv_data("shippingItems_testing_data.csv")
        self.login_data = read_csv_data("login_details.csv")

        self._login()
        self._select_customer()
        self._select_and_verify_address()
        self._enter_shipping_data()

    @pytest.mark.jira("LTL1-3496")
    def test_send_email_rates(self):
        self.log.info("Inside test_send_email_rates Method")

        # Selecting carrier checkbox and validating the email popup
        self.car.selectCarrierCheckbox("2")
        quote_value = self.qop.getIdentifierValue("quote")
        self.log.debug("Get Quote # in Quote Page : " + str(quote_value))

        origin = self.rdp.getSelectedAddressInTextField("origin")
        origin_value = origin[:origin.find(",") + 1] + origin[origin.find(",") + 1:].strip().replace(",", "")
        dest = self.rdp.getSelectedAddressInTextField("destination")
        destination_value = dest[:dest.find(",") + 1] + dest[dest.find(",") + 1:].strip().replace(",", "")

        prepared_subject = "[" + origin_value + "] - [" + destination_value + "] Quote # " + quote_value

        self.car.carrierEmailLinkClick()
        validated_subject_result = self.car.validateEmailPopSubject(prepared_subject)
        self.ts.mark(validated_subject_result, "Validating the Subject Line in Email Popup")

        validate_cancel_result = self.car.click_email_pop_up_button("Cancel")
        self.ts.mark(validate_cancel_result, "Validating the cancel Email Button Feature")
        self.car.carrierEmailLinkClick()

        # Entering mail id in Email Popup
        self.car.enterEmailId()
        validating_send_result = self.car.click_email_pop_up_button("Send")
        self.ts.mark(validating_send_result, "Validating the Send Email Button Feature")

        # Opening Mail in new Tab and login to the mail server
        self.email.openMailPage()
        self.email.loginMailServer()
        self.email.emailPageRefresh()

        email_received_result = self.email.searchAndSelectEmail(prepared_subject)
        self.ts.mark(email_received_result, "Validating the Email Receive in recipient mail id.")
        validate_attachment = self.email.verifyRateQuoteAttachment()
        self.ts.mark(validate_attachment, " Validating the emailRates Attachment in recipient mail id.")

        self.ts.mark_final("test_send_email_rates", True, "Validating the Sending EMail Rates Feature")

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")

    def _select_customer(self):
        self.log.info("Selecting Customer")
        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.address_data[0]["customer_name"], 1)

    def _select_and_verify_address(self):
        self.log.info("Selecting address Method")
        for row in self.address_data:
            if row == self.address_data[0]:
                # Validate direct address search in origin textfield and select the required address from
                # dropdown list
                verifiedOriginTextResult = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'], "origin")
                self.ts.mark(verifiedOriginTextResult, "Populated origin text is same as selected from dropdown")

                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                verifyOriginSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
                self.ts.mark(verifyOriginSiteTypeSelected, "Site Type selected is same as given for origin address")

            if row == self.address_data[1]:
                # Validate direct address search in destination textfield and select the required address from
                # dropdown list
                verifiedDestinationTextResult = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                      "destination")
                self.ts.mark(verifiedDestinationTextResult, "Populated destination text is same as selected from "
                                                            "dropdown")

                self.rdp.selectSiteType(row["destination_site_type"], "destination")
                verifyDestSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                             "destination")
                self.ts.mark(verifyDestSiteTypeSelected, "Site Type selected is same as given for destination "
                                                         "address")

        # Validate Freight Date is set to System Date
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

    def _enter_shipping_data(self):
        for row in self.shipping_data[:1]:
            self.log.info(row)
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           # prefix=row["prefix"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])

            self.log.info("Inserted data in shipping items")

            self.si.clickAddToOrderButton()
