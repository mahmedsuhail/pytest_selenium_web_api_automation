import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup")
class LTLCarrierReRateTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    login_data = read_csv_data("login_details.csv")
    shipping_data = read_csv_data("shippingItems_testing_data.csv")
    address_data = read_csv_data("address_details.csv")
    re_rate_site_type_data = read_csv_data("rerate_sitetype.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)

        self.__login()
        self.__navigate_to_create_quote_and_select_customer()
        self._set_freight_date()
        self.__select_route_details_and_accessorials()
        self.__enter_shipping_data()

    @pytest.mark.jira("LTL1-3851")
    @pytest.mark.slowtest
    def test_ltl_carrier_re_rate(self):
        self.log.info("Inside test_ltl_carrier_re_rate Method")
        re_rate_with_zip_codes_result = self.car.validateReRateWithZipCodes()
        self.ts.mark(re_rate_with_zip_codes_result, "Validated re-rate functionality with zip codes")

        re_rate_with_shipping_items_result = self.car.validateReRateWithShippingItems()
        self.ts.mark(re_rate_with_shipping_items_result, "Validated re-rate functionality with shipping items")

        re_rate_with_accessorials_result = self.car.validateReRateWithAccessorials()
        self.ts.mark(re_rate_with_accessorials_result, "Validating re-rate functionality  with accessorials")

        re_rate_with_site_type_result = True
        for row in self.re_rate_site_type_data:
            self.log.info(row)
            self.log.info("inside for loop")
            re_rate_with_site_type = self.car.validateReRateWithSiteType(row["originsitetype"],
                                                                         row["destinationsitetype"],
                                                                         row["originsitetypebreakup"],
                                                                         row["destinationsitetypebreakup"])
            re_rate_with_site_type_result = re_rate_with_site_type_result and re_rate_with_site_type
        self.ts.mark(re_rate_with_site_type_result, "Validating re-rate functionality  with site type")

        self.ts.mark_final("test_ltl_carrier_re_rate", True,
                           "Validated Re-Rate behaviour of carriers in create quote page.")

    def __login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __navigate_to_create_quote_and_select_customer(self):
        self.qop.click_quote_order_icon()
        # Search and select customer
        self.qop.searchAndSelectCustomer(self.address_data[2]["customer_name"], 1)

    def __select_route_details_and_accessorials(self):
        # Data Entry in Route Details & Accessorial Section
        for row in self.address_data:
            if row == self.address_data[2]:
                # Search and select address from origin address book
                self.rdp.searchAndSelectAddressFromAddressBook(row["search_value"], row["company_name"], "origin")

                # Search and select address from destination address book
                self.rdp.searchAndSelectAddressFromAddressBook(row["search_value"], row["company_name"], "destination")

    def __enter_shipping_data(self):
        # Data Entry in Shipping Items Section
        for row in self.shipping_data[:1]:
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])
            self.si.clickAddToOrderButton()

    def _set_freight_date(self):
        # current_date = self.qop.getCurrentDate()
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
