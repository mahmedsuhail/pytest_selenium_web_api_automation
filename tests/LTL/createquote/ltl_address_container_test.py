import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup_orders")
class AddressContainerTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    login_data = read_csv_data("login_details.csv")
    address_data = read_csv_data("address_details.csv")
    shipping_data = read_csv_data("shippingItems_testing_data.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).lop = TMSLogoutPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).isInsurancePresent = self.car.csvRowDataToList("isInsurancePresent", "carrier_details.csv")
        self.__login()
        self.__search_and_select_customer()
        self._set_freight_date()

    def __login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __search_and_select_customer(self):
        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.address_data[0]["customer_name"], 1)

    def _set_freight_date(self):
        # current_date = self.qop.getCurrentDate()
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

    def __enter_shipping_data(self):
        # Data Entry in Shipping Items Section
        for row in self.shipping_data[:2]:
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])
            self.si.clickAddToOrderButton()

    def test_direct_search_address_container_validation(self):
        self.log.info("Inside test_direct_search_address_container_validation method")
        for row in self.address_data:
            if row == self.address_data[0]:
                # Validate direct address search in origin textfield and select the required address from
                # dropdown list
                verified_origin_text_result = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'],
                                                                                    "origin")
                self.ts.mark(verified_origin_text_result, "Populated origin text is same as selected from dropdown")

            if row == self.address_data[1]:
                # Validate direct address search in destination textfield and select the required address from
                # dropdown list
                verified_destination_text_result = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                         "destination")
                self.ts.mark(verified_destination_text_result, "Populated destination text is same as selected from "
                                                               "dropdown")

        address_container_visibility = self.qop.addressContainerPresenceCheck()
        self.log.info("address_container_visibility : " + str(address_container_visibility))
        if address_container_visibility:
            self.log.info("Inside if not")
            self.__enter_shipping_data()

        for row in self.address_data:
            if row == self.address_data[0]:
                validated_origin_result = self.rdp.validateAddressContainerFields("origin",
                                                                                  row['random_origin_cityname'],
                                                                                  row['random_origin_zipcode'],
                                                                                  row['default_origin_readytime'],
                                                                                  row['default_origin_closetime'])
                self.ts.mark(validated_origin_result, "Verified Origin Address Container fields after direct search")

            if row == self.address_data[1]:
                validated_dest_result = self.rdp.validateAddressContainerFields("destination",
                                                                                row['random_dest_cityname'],
                                                                                row['random_dest_zipcode'],
                                                                                row['default_dest_readytime'],
                                                                                row['default_dest_closetime'])
                self.ts.mark(validated_dest_result, "Verified Destination Address Container fields after direct search")

        self.car.selectCarrierByNameOrIndex(1, self.isInsurancePresent[0])
        validated_mandatory_fields_result = self.rdp.validateMandatoryFields()
        self.ts.mark(validated_mandatory_fields_result, "Verified Mandatory Fields are required for Origin and "
                                                        "Destination Address")
        validated_default_origin_and_destination_time = self.rdp.validateDefaultTime()
        self.ts.mark(validated_default_origin_and_destination_time, "Verified Default origin and destination ready "
                                                                    "time and close time")

        for row in self.address_data:
            if row == self.address_data[2]:
                # Enter details in Origin Address container and validate entered data
                self.rdp.enterRequiredAddressContainerDetails(row['company_name'], row['contact_name'],
                                                              row['contact_phone'],
                                                              row['addressline1'], row['addressline2'],
                                                              row['origin_ready_time'],
                                                              row['origin_close_time'], row['pickup_marks'], "origin")
                validated_selected_origin_result = \
                    self.rdp.validateAddressContainerFields("origin", row['random_origin_cityname'],
                                                            row['random_origin_zipcode'], row['origin_ready_time'],
                                                            row['origin_close_time'], row['company_name'],
                                                            row['contact_name'], row['contact_phone'],
                                                            row['addressline1'], row['addressline2'],
                                                            row['pickup_marks'])
                self.ts.mark(validated_selected_origin_result, "Verified Origin Address Container fields after "
                                                               "entering data")

                # Enter details in Destination Address container and validate entered data
                self.rdp.enterRequiredAddressContainerDetails(row['company_name'], row['contact_name'],
                                                              row['contact_phone'],
                                                              row['addressline1'], row['addressline2'],
                                                              row['dest_ready_time'],
                                                              row['dest_close_time'], row['delivery_marks'],
                                                              "destination")
                validated_selected_dest_result = self.rdp.validateAddressContainerFields("destination",
                                                                                         row['random_dest_cityname'],
                                                                                         row['random_dest_zipcode'],
                                                                                         row['dest_ready_time'],
                                                                                         row['dest_close_time'],
                                                                                         row['company_name'],
                                                                                         row['contact_name'],
                                                                                         row['contact_phone'],
                                                                                         row['addressline1'],
                                                                                         row['addressline2'],
                                                                                         row['delivery_marks'])
                self.ts.mark(validated_selected_dest_result,
                             "Verified destination Address Container fields after entering")

        validated_time_result = self.rdp.selectTimeAndValidate(self.address_data[0]['ready_time'])
        self.ts.mark(validated_time_result, "Verified for the given ready time, close time is set properly in origin "
                                            "and destination containers")

        # Validation origin/dest ready time on selecting ready time earlier than 9 AM
        validated_time_result1 = self.rdp.selectTimeAndValidate("6:00 AM")
        self.ts.mark(validated_time_result1, "Verified origin and destinatio ready time can be "
                                             "entered earlier than 9:00 AM")

        self.ts.mark_final("test_direct_search_address_container_validation", True,
                           "Verified address containers on direct serach address")

    @pytest.mark.jira("LTL1-3851")
    @pytest.mark.slowtest
    def test_search_address_book_container_validation_on_selection(self):
        self.log.info("Inside test_search_address_book_container_validation_on_selection method")
        for row in self.address_data:
            # Search and select address from origin address book
            self.rdp.searchAndSelectAddressFromAddressBook(row["search_value"], row["company_name"], "origin")

            # Verify selected address from origin address book is populated in origin textfield
            origin_selected_textfield_result = self.rdp.verifySelectedAddressInTextField(row["city_name"], "origin")
            self.ts.mark(origin_selected_textfield_result, "Address populated in the origin textfield is same as "
                                                           "selected from origin address book")

            # Search and select address from destination address book
            self.rdp.searchAndSelectAddressFromAddressBook(row["search_value"], row["company_name"], "destination")

            # Verify selected address from destination address book is populated in destination textfield
            destination_selected_textfield_result = self.rdp.verifySelectedAddressInTextField(row["city_name"],
                                                                                              "destination")
            self.ts.mark(destination_selected_textfield_result, "Address populated in the destination textfield is "
                                                                "same as selected from destination address book")

            address_container_visibility = self.qop.addressContainerPresenceCheck()
            self.log.info("address_container_visibility : " + str(address_container_visibility))
            if address_container_visibility:
                self.log.info("Inside if not")
                self.__enter_shipping_data()

            validated_origin_container_result = self.rdp.validateAddressContainerFields("origin", row['city_name'],
                                                                                        row['zip_code'],
                                                                                        row['origin_ready_time'],
                                                                                        row['origin_close_time'],
                                                                                        row['company_name'],
                                                                                        row['contact_name'],
                                                                                        row['contact_phone'],
                                                                                        row['addressline1'],
                                                                                        row['addressline2'],
                                                                                        row['pickup_marks']
                                                                                        )
            self.ts.mark(validated_origin_container_result, "Verified Origin Address Container fields after selecting "
                                                            "from address book")

            validated_dest_container_result = self.rdp.validateAddressContainerFields("destination", row['city_name'],
                                                                                      row['zip_code'],
                                                                                      row['dest_ready_time'],
                                                                                      row['dest_close_time'],
                                                                                      row['company_name'],
                                                                                      row['contact_name'],
                                                                                      row['contact_phone'],
                                                                                      row['addressline1'],
                                                                                      row['addressline2'],
                                                                                      row['delivery_marks']
                                                                                      )
            self.ts.mark(validated_dest_container_result, "Verified Destination Address Container fields after "
                                                          "selecting from address book")

        self.ts.mark_final("test_search_address_book_container_validation_on_selection", True,
                           "Verified address containers on address book selection")
