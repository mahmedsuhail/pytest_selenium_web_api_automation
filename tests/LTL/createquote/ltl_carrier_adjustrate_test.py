import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup")
class LTLCarrierAdjustRateTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    shipping_data = read_csv_data("shippingItems_testing_data.csv")
    address_data = read_csv_data("address_details.csv")
    login_data = read_csv_data("login_details.csv")
    util = Util(log)

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.__login()
        self.__navigate_to_create_quote_and_select_customer()
        self._set_freight_date()
        self.__select_route_details_and_accessorials()
        self.__enter_shipping_data()

    @pytest.mark.slowtest
    def test_ltl_carrier_adjust_rate(self):
        self.log.info("Inside test_ltl_carrier_adjust_rate Method")
        # Adjust Rate feature validation in carrier section.

        validate_adjust_rate_for_carriers_result = self.car.validateAdjustRateForCarriers()
        self.ts.mark(validate_adjust_rate_for_carriers_result, "Validated Adjust Rate for carriers.")

        default_adjust_sell_rate_window_for_carriers_result = self.car.defaultAdjustSellRateWindowForCarriers()
        self.ts.mark(default_adjust_sell_rate_window_for_carriers_result,
                     "Validated Default Adjust Sell Rate Page for carriers.")

        self.ts.mark_final("test_ltl_carrier_adjust_rate", True,
                           "Validated adjust rate feature in carrier section in LTL create quote page.")

    def __login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __navigate_to_create_quote_and_select_customer(self):
        self.qop.click_quote_order_icon()
        # Search and select customer
        self.qop.searchAndSelectCustomer(self.address_data[2]["customer_name"], 1)

    def __select_route_details_and_accessorials(self):
        # Data Entry in Route Details & Accessorial Section
        for row in self.address_data:
            if row == self.address_data[2]:
                # Select Zip code
                verified_origin_text_result = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'],
                                                                                    "origin")
                self.ts.mark(verified_origin_text_result, "Populated origin text is same as selected from dropdown")

                verified_destination_text_result = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                         "destination")

                self.ts.mark(verified_destination_text_result, "Populated destination text is same as "
                                                               "selected from dropdown")

                # Select Site type
                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                self.rdp.selectSiteType(row["destination_site_type"], "destination")

                # Select Accessorial
                self.qop.selectAccessorial(row["origin_accessorials"],
                                           self.util.get_key_for_value(row["origin_accessorials"], row))

    def __enter_shipping_data(self):
        # Data Entry in Shipping Items Section
        for row in self.shipping_data[:2]:
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])
            self.si.clickAddToOrderButton()

    def _set_freight_date(self):
        # current_date = self.qop.getCurrentDate()
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
