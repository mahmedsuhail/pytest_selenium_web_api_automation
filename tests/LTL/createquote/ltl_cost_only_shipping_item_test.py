import utilities.custom_logger as cl

from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from utilities.reportteststatus import ReportTestStatus
import unittest2
import pytest
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class LTLCostOnlyShippingItemTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")
    login_data = read_csv_data("login_details.csv")
    shipping_data = read_csv_data("shippingItems_testing_data.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).login_page = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)

        self._login()

    def test_default_cost_only_shipping_items(self):
        self.log.info("Inside test_default_cost_only_shipping_items Method")

        self._select_customer_rates()
        self._select_cost_only()

        # Default & mandatory fields validation at Shipping Items section.
        self.si.clearShippingItemsButton()
        clearResult = self.si.verifyShippingItemsDefaultState()
        self.ts.mark(clearResult, "Validating the Default Status of Shipping items")
        for row in self.shipping_data:
            if row == self.shipping_data[0]:
                self.log.info("inside onetime loop")
                default_shipping_items_validation = self.si.verifyDefaultShippingItemDataCostOnly()
                self.ts.mark(default_shipping_items_validation, " Validating the default shipping items data")
                # Linear Feet Message Validation
                linear_feet_msg = self.si.verifyLinearFeetMessage()
                self.ts.mark(linear_feet_msg, "Validating Linear feet Message")
                # Origin & Destination Validation
                default_route_details_validation = self.rdp.verifyDefaultRouteDetailsCostOnly()
                self.ts.mark(default_route_details_validation, " Validating the default Route Details")

                self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                               row["UnitType"], row["className"], row["nfmc"], row["length"],
                                               row["width"],
                                               row["height"], row["weightUnits"], row["Dim"],
                                               stackable=row["stackable"],
                                               hazmat=row["hazmat"], hazgrp=row["HazmatGroup"],
                                               hzclass=row["HazmatClass"],
                                               prefix=row["prefix"], hzcode=row["HazmatCode"],
                                               chemName=row["ChemicalName"],
                                               emergNo=row["emergencyContact"])

                valid_all_shipping_data = self.si.verifyCombinedShippingItemData(row["commodity"], row["piece"],
                                                                                 row["weight"],
                                                                                 row["unitcount"], row["UnitType"],
                                                                                 row["className"], row["nfmc"],
                                                                                 row["length"],
                                                                                 row["width"], row["height"],
                                                                                 row["weightUnits"], row["Dim"],
                                                                                 stackable=row["stackable"],
                                                                                 hazmat=row["hazmat"],
                                                                                 hzmcode=row["HazmatCode"],
                                                                                 chemName=row["ChemicalName"],
                                                                                 emergNo=row["emergencyContact"],
                                                                                 hzGrp=row["HazmatGroup"],
                                                                                 hzclass=row["HazmatClass"],
                                                                                 prefix=row["prefix"])
                self.ts.mark(valid_all_shipping_data, "Validating the All shipping Data")

                self.si.clearShippingItemsButton()
                clear_result = self.si.verifyShippingItemsDefaultState()
                self.ts.mark(clear_result, "Validating the field after click Clear Field")

                self.sop.clickSalesOrderIcon()
                verify_confirm_navigation = self.qop.clickButtonInQuote("navigationconfirm")
                self.ts.mark(verify_confirm_navigation, "Validating Navigation Confirm Button")
        self.ts.mark_final("test_default_cost_only_shipping_items", True,
                           "Validating the Default data in shipping item section for cost only")

    def test_cost_only_add_shipping_items_data(self):

        self.log.info("Inside test_cost_only_add_shipping_items_data Method")
        list_of_calculated_data = []

        self._select_cost_only()
        self._select_and_verify_address()

        for row in self.shipping_data[:2]:
            self.log.info(row)
            self.log.info("inside for loop")
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])

            self.log.info("Inserted data in shipping items")

            # Add to Order Button Validation
            add_to_order_button = self.si.verifyButtonEnabled("AddToOrder")
            self.log.info("Add to Order Button Status : " + str(add_to_order_button))
            self.ts.mark(add_to_order_button, "Validating the Button Enabled or Not")

            # # preparing the data to validate SO Items
            so_prepared_data = self.si.getEachShippingItemDataForSOItem()
            list_of_calculated_data.append(so_prepared_data)
            self.log.info("getItemDataFromShippingItem : " + str(so_prepared_data))

            # Validating the Density value in shipping item section
            density_compare_result = self.si.verifyDensityValue()
            self.ts.mark(density_compare_result, "Validating the display Density Value.")

            # supplement method for verifyDisplaySOItems
            current_so_item_count = self.si.getSOItemCount()
            # Clicking Add to Order Button
            self.si.clickAddToOrderButton()
            so_item = self.si.verifyDisplaySOItems(current_so_item_count)
            self.ts.mark(so_item, "Validating after click AddToOrder button Item Summary should display.")

            # Validating the SO Items Details
            validation_so_item_result = self.si.validateSOEachItemData(so_prepared_data)
            self.ts.mark(validation_so_item_result, "Validating the display SO Item")
            if row == self.shipping_data[len(self.shipping_data) - 1]:
                self.log.info("inside condition")
                calculated_value_from_so_item = self.si.calculateTotalSOItemValue(list_of_calculated_data)

                summary_result = self.si.verifySOItemSummary(calculated_value_from_so_item)
                self.ts.mark(summary_result, "Summary Result Validation")
                prohibited_link_result = self.si.verifyProhibitedCommoditiesLink()
                self.ts.mark(prohibited_link_result, "Validate the Prohibited Link text and file Name")
                Shipment_value_result = self.si.verifyTotalShipmentValue(4000)
                self.ts.mark(Shipment_value_result, "Validating the Total Shipment Value")
                self.si.enterTotalShipmentValue(300)
                Shipment_value_result = self.si.verifyTotalShipmentValue(300)
                self.ts.mark(Shipment_value_result, "Validating for the updated Total Shipment Value.")
                index_value = self.si.getShippingItemIndexBasedOnCommodity(row["commodity"])
                self.si.deleteShippingItem(index_value)
                item_result = self.si.verifyDisplayItemByCommodity(row["commodity"])
                self.ts.mark(not item_result, "Validate the item should not display")
        self.ts.mark_final("test_cost_only_add_shipping_items_data", True,
                           "Validating the Shipping items Add to Order functionality for cost only feature.")

    def _login(self):
        credential = self.login_data[0]
        self.login_page.login(credential["userName"], credential["password"])
        self.login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")

    def _select_cost_only(self):
        self.log.info("Selecting Cost Only radio Button")
        self.qop.click_quote_order_icon()
        self.qop.clickCostOnlyRadio()

    # -- Adding method as patch fix --
    def _select_customer_rates(self):
        self.log.info("Selecting customer radio Button")
        self.qop.clickCustomerRatesRadio()

    def _select_and_verify_address(self):
        self.log.info("Selecting address Method")
        for row in self.address_data:
            if row == self.address_data[0]:
                verifiedOriginTextResult = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'], "origin")
                self.ts.mark(verifiedOriginTextResult, "Populated origin text is same as selected from dropdown")

                # Get the site type for the selected origin address and validate it
                verify_origin_SiteTypeResult = self.rdp.verifySiteTypeSelected("business", "origin")
                self.ts.mark(verify_origin_SiteTypeResult, "site type should be business by default for direct "
                                                           "search of origin address")

                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                verifyOriginSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
                self.ts.mark(verifyOriginSiteTypeSelected, "Site Type selected is same as given for origin address")

            if row == self.address_data[1]:
                verifiedDestinationTextResult = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                      "destination")
                self.ts.mark(verifiedDestinationTextResult, "Populated destination text is same as selected from "
                                                            "dropdown")

                # Get the site type for the selected destination address and validate it
                verify_dest_SiteTypeResult = self.rdp.verifySiteTypeSelected("business", "destination")
                self.ts.mark(verify_dest_SiteTypeResult, "site type should be business by default for direct "
                                                         "search "
                                                         "of destination address")
                self.rdp.selectSiteType(row["destination_site_type"], "destination")
                verifyDestSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                             "destination")
                self.ts.mark(verifyDestSiteTypeSelected, "Site Type selected is same as given for destination "
                                                         "address")
