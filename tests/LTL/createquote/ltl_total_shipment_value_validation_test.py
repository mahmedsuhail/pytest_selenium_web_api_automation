import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class LTLTotalShipmentValueValidationTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).shipping_data = read_csv_data("shippingItems_testing_data.csv")
        self.login_data = read_csv_data("login_details.csv")

        self._login()
        self._select_customer()
        self._set_freight_date()
        self._select_address()
        self._enter_shipping_data()

    def test_validate_shipment_value(self):
        self.log.info("inside test_validate_shipment_value Method")

        self.car.selectCarrierByNameOrIndex("1", "yes")
        button_enabled = self.qop.verifyCreateOrderButtonEnabled()
        self.ts.mark(button_enabled, "Validating the Create Order Button Enabled")

        entered_value = self.si.enterTotalShipmentValue(99)
        self.ts.mark(entered_value, "Validated entered value is set to 100 as default value")
        self.car.selectCarrierByNameOrIndex("1", "yes")
        button_enabled = self.qop.verifyCreateOrderButtonEnabled()
        self.ts.mark_final("test_validate_shipment_value", button_enabled,
                           "Validating the Create Order Button enabeled if shipment value greater than 100")

    def test_validate_shipment_value_on_save_order(self):
        self.log.info("Inside test_validate_shipment_value_on_save_order method")
        self.si.enterTotalShipmentValue(6000)
        self.car.selectCarrierCheckbox("1")
        quote_value = self.qop.getIdentifierValue("quote")
        self.log.debug("Get Quote Value : " + str(quote_value))
        saved_quote_reference = "SQREF" + str(quote_value)
        self.qop.enterReferenceForSaveOrder(saved_quote_reference)
        # Select customer and enter all the mandatory details
        self.qop.clickCostOnlyRadio()
        self.qop.clickCustomerRatesRadio()
        self._select_customer()
        self._set_freight_date()
        self._select_address()
        self._enter_shipping_data()
        # Validate the shipment value is set to default value (4000) on saving previous order
        validated_shipment_value = self.si.verifyTotalShipmentValue("4000")
        self.ts.mark_final("test_validate_shipment_value_on_save_order", validated_shipment_value,
                           "Validated total shipment value on saving previous order.")

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")

    def _select_customer(self):
        self.log.info("Selecting Customer")
        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.address_data[0]["customer_name"], 1)

    def _enter_shipping_data(self):
        # Data Entry in Shipping Items Section
        for row in self.shipping_data[:1]:
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])
            self.si.clickAddToOrderButton()

    def _set_freight_date(self):
        # current_date = self.qop.getCurrentDate()
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

    def _select_address(self):
        # Selecting origin and destination address
        for row in self.address_data:
            if row == self.address_data[0]:
                verifiedOriginTextResult = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'], "origin")
                self.ts.mark(verifiedOriginTextResult, "Populated origin text is same as selected from dropdown")

            if row == self.address_data[1]:
                verifiedDestinationTextResult = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                      "destination")
                self.ts.mark(verifiedDestinationTextResult, "Populated destination text is same as selected "
                                                            "from dropdown")
