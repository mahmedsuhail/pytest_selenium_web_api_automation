import utilities.custom_logger as cl

from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from utilities.reportteststatus import ReportTestStatus
import unittest2
import pytest
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup_orders")
class LTLCostOnlyNavigationTests(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    shipping_data = read_csv_data("shippingItems_testing_data.csv")
    login_data = read_csv_data("login_details.csv")
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).login_page = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        self.__login()

    def test_cost_only_login_create_order_icon_so_navigation(self):
        self.log.info("Inside test_cost_only_login_create_order_icon_so_navigation method")

        self._select_cost_only()

        # Navigate to Sales Order Page
        self.sop.clickSalesOrderIcon()
        verify_navigation_msg = self.qop.verifyNavigationConfirmationText()
        self.ts.mark(verify_navigation_msg, "Validating Navigation confirmation Message")
        verify_cancel_navigation = self.qop.clickButtonInQuote("navigationcancel")
        self.ts.mark(verify_cancel_navigation, "Validating Navigation Cancel Button")
        self.sop.clickSalesOrderIcon()
        verify_confirm_navigation = self.qop.clickButtonInQuote("navigationconfirm")
        self.ts.mark(verify_confirm_navigation, "Validating Navigation Confirm Button")
        validate_so_page = self.login_page.verifyLoginSuccessful()
        self.ts.mark(validate_so_page, "Sales order board is displayed")

        self.ts.mark_final("test_cost_only_login_create_order_icon_so_navigation", True,
                           "Validated Login,Create Order Icon & the Navigation feature from Quote Page")

    def test_default_cost_only_page(self):
        self.log.info("Inside test_default_cost_only_page Method")
        self._select_cost_only()

        currency_dropdown_result = self.qop.dropDownListCompareCostSection("currency")
        self.ts.mark(currency_dropdown_result, "Validated Currency Drop down")
        self.qop.waitForQuotePageEnable()
        accessorial_result = self.qop.validateAllAccessorialsBasedOnSections()
        self.ts.mark(accessorial_result, "All the accessorial names are correct based on the section")
        self.sop.clickSalesOrderIcon()
        self.qop.clickButtonInQuote("navigationconfirm")
        self.ts.mark_final("test_default_cost_only_page", True, "Validated Default Cost Page Status.")

    def test_cost_only_route_details_and_site_type(self):
        self.log.info("Inside test_costOnlyRouteDetailsAndSiteType method")

        self._select_cost_only()

        self.__select_and_verify_address()
        self.qop.clickingAccessorialsCheckbox("origininside")
        self.qop.clickingAccessorialsCheckbox("destinationliftgate")

        # Validate Freight Date is set to given Date
        validate_next_date_selected_result = self.rdp.dateSelection(3)
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
        self.rdp.checkDatepickerClosed()
        self.sop.clickSalesOrderIcon()
        self.qop.clickButtonInQuote("navigationconfirm")
        self.ts.mark_final("test_costOnlyRouteDetailsAndSiteType", True,
                           "verified and validated direct search for origin and destination field")

    def __login(self):
        credential = self.login_data[0]
        self.login_page.login(credential["userName"], credential["password"])
        self.login_page.verifyLoginSuccessful()

    def _select_cost_only(self):
        self.log.info("Selecting Cost Only radio Button")
        self.qop.click_quote_order_icon()
        self.qop.clickCostOnlyRadio()

    def __select_and_verify_address(self):
        for row in self.address_data:
            if row == self.address_data[0]:
                # Validate direct address search in origin textfield and select the required address from
                # dropdown list
                verified_origin_text_result = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'],
                                                                                    "origin")
                self.ts.mark(verified_origin_text_result, "Populated origin text is same as selected from drop down")

                # Get the accessorials list for the selected origin address and validate it
                Origin_selected_accessorial_list = self.qop.selectedAccessorials("True")

                origin_accessorial_selected_result = \
                    self.rdp.validateAccessorialsForDirectSearch(Origin_selected_accessorial_list)
                self.ts.mark(origin_accessorial_selected_result,
                             "accessorial should not be selected for direct search of origin address")

                # Get the site type for the selected origin address and validate it
                verify_origin_site_type_result = self.rdp.verifySiteTypeSelected("business", "origin")
                self.ts.mark(verify_origin_site_type_result,
                             "site type should be business by default for direct search of origin address")

                # self.rdp.selectSiteType(row["origin_site_type"], "origin")
                self.rdp.selectSiteType("Country Club Pickup", "origin")

                verify_origin_site_type_selected = self.rdp.verifySiteTypeSelected("Country Club Pickup", "origin")
                self.ts.mark(verify_origin_site_type_selected,
                             "Site Type selected is same as given for origin address")

            if row == self.address_data[1]:
                # Validate direct address search in destination textfield and select the required address from
                # dropdown list
                verified_destination_text_result = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                         "destination")
                self.ts.mark(verified_destination_text_result,
                             "Populated destination text is same as selected from dropdown")

                # Get the accessorials list for the selected destination address and validate it
                dest_selected_accessorial_list = self.qop.selectedAccessorials("True")
                dest_accessorial_selected_result = \
                    self.rdp.validateAccessorialsForDirectSearch(dest_selected_accessorial_list)
                self.ts.mark(dest_accessorial_selected_result,
                             "accessorial should not be selected for direct search of destination address")

                # Get the site type for the selected destination address and validate it
                verify_dest_site_type_result = self.rdp.verifySiteTypeSelected("business", "destination")
                self.ts.mark(verify_dest_site_type_result,
                             "site type should be business by default for direct search of destination address")

                self.rdp.selectSiteType("residence", "destination")

                verify_dest_site_type_selected = self.rdp.verifySiteTypeSelected("residence", "destination")
                self.ts.mark(verify_dest_site_type_selected,
                             "Site Type selected is same as given for destination address")
