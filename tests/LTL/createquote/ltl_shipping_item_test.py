import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class LTLShippingItemTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        self.rdp = RouteDetailsPage(self.driver, self.log)
        type(self).shipping_data = read_csv_data("shippingItems_testing_data.csv")
        self.login_data = read_csv_data("login_details.csv")

        self._login()
        self._select_customer()
        self._set_freight_date()
        self._select_and_verify_address()

    @pytest.mark.slowtest
    def test_default_shipping_items_and_clear_feature(self):
        self.log.info("inside test_default_shipping_items_and_clear_feature Method")

        self.si.clearShippingItemsButton()

        for row in self.shipping_data[:2]:
            clear_result = self.si.verifyShippingItemsDefaultState()
            self.ts.mark(clear_result, "Validating the Default Status of Shipping items")
            if row == self.shipping_data[0]:
                self.log.info("inside onetime loop")
                default_shipping_items_validation = self.si.verifyDefaultShippingItemData()
                self.ts.mark(default_shipping_items_validation, " Validating the default shipping items data")
                # Linear Feet Message Validation
                linear_feet_msg = self.si.verifyLinearFeetMessage()
                self.ts.mark(linear_feet_msg, "Validating Linear feet Message")

            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           prefix=row["prefix"], hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])

            valid_all_shipping_data = self.si.verifyCombinedShippingItemData(row["commodity"], row["piece"],
                                                                             row["weight"], row["unitcount"],
                                                                             row["UnitType"], row["className"],
                                                                             row["nfmc"], row["length"],
                                                                             row["width"], row["height"],
                                                                             row["weightUnits"], row["Dim"],
                                                                             stackable=row["stackable"],
                                                                             hazmat=row["hazmat"],
                                                                             hzmcode=row["HazmatCode"],
                                                                             chemName=row["ChemicalName"],
                                                                             emergNo=row["emergencyContact"],
                                                                             hzGrp=row["HazmatGroup"],
                                                                             hzclass=row["HazmatClass"],
                                                                             prefix=row["prefix"])
            self.ts.mark(valid_all_shipping_data, "Validating the All shipping Data")

            self.si.clearShippingItemsButton()
            clear_result = self.si.verifyShippingItemsDefaultState()
            self.ts.mark(clear_result, "Validating the field after click Clear Field")
        self.ts.mark_final("test_default_shipping_items_and_clear_feature", True,
                           "Completed Validation for test_default_shipping_items_and_clear_feature")

    def test_add_and_populate_product_feature(self):
        self.log.info("Inside test_add_and_populate_product_feature Method")

        self.si.clearShippingItemsButton()
        for row in self.shipping_data[:2]:
            if row == self.shipping_data[0]:
                product_default = self.si.validateDefaultProductCommodities()
                self.ts.mark(product_default, "Validating the basic validation in Add Product section")

            # Below condition will get removed once NFMC # issue get resolved for 2nd row
            if row == self.shipping_data[0]:
                added_product_result = self.si.addNewProducts(row["commodity"], row["nfmc"], row["className"],
                                                              row["UnitType"], unitcount=row["unitcount"],
                                                              piece=row["piece"], weight=row["weight"],
                                                              length=row["length"], width=row["width"],
                                                              height=row["height"], weightmom=row["weightUnits"],
                                                              dimmom=row["Dim"], stackable=row["stackable"],
                                                              hazmat=row["hazmat"], hazgrp=row["HazmatGroup"],
                                                              hzclass=row["HazmatClass"], prefix=row["prefix"],
                                                              hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                                              emergNo=row["emergencyContact"])
                self.ts.mark(added_product_result[0], "Validating added Product.")
                self.log.info("returned Value : " + str(added_product_result[1]))
                if added_product_result[0]:
                    selected_product = self.si.searchAndSelectProduct(added_product_result[1])
                    self.ts.mark(selected_product, "Validating search and able to select product")
                    # Due to Bug LTL1-3628, unit count field validating as empty
                    valid_all_shipping_data = self.si.verifyCombinedShippingItemData(added_product_result[1],
                                                                                     row["piece"], row["weight"],
                                                                                     # row["unitcount"],
                                                                                     "",
                                                                                     row["UnitType"],
                                                                                     row["className"], row["nfmc"],
                                                                                     row["length"], row["width"],
                                                                                     row["height"], row["weightUnits"],
                                                                                     row["Dim"],
                                                                                     stackable=row["stackable"],
                                                                                     hazmat=row["hazmat"],
                                                                                     hzmcode=row["HazmatCode"],
                                                                                     chemName=row["ChemicalName"],
                                                                                     # emergNo=row["emergencyContact"],
                                                                                     emergNo="",
                                                                                     hzGrp=row["HazmatGroup"],
                                                                                     hzclass=row["HazmatClass"],
                                                                                     prefix=row["prefix"],
                                                                                     productBook="yes")
                    self.log.info("shipping Data : " + str(valid_all_shipping_data))
                    self.ts.mark(valid_all_shipping_data, "Validating product data in shipping items")

                else:
                    closing_popup_result = self.si.closeProductPopUp()
                    self.ts.mark(closing_popup_result, "Validating the popup should get closed with out any error")

        self.ts.mark_final("test_add_and_populate_product_feature", True,
                           "Completed Validation for Add Product Validation")

    def test_add_shipping_items_data(self):
        self.log.info("Inside test_add_shipping_items_data Method")

        self.si.clearShippingItemsButton()
        list_of_calculated_data = []
        for row in self.shipping_data[:2]:
            self.log.info(row)
            self.log.info("inside for loop")
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           # prefix=row["prefix"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])

            self.log.info("Inserted data in shipping items")

            # Add to Order Button Validation
            add_to_order_button = self.si.verifyButtonEnabled("AddToOrder")
            self.log.info("Add to Order Button Status : " + str(add_to_order_button))
            self.ts.mark(add_to_order_button, "Validating the Button Enabled or Not")

            # preparing the data to validate SO Items
            so_prepared_data = self.si.getEachShippingItemDataForSOItem()
            list_of_calculated_data.append(so_prepared_data)
            self.log.info("getItemDataFromShippingItem : " + str(so_prepared_data))

            # Validating the Density value in shipping item section
            density_compare_result = self.si.verifyDensityValue()
            self.ts.mark(density_compare_result, "Validating the display Density Value.")

            # supplement method for verifyDisplaySOItems
            current_so_item_count = self.si.getSOItemCount()
            # Clicking Add to Order Button
            self.si.clickAddToOrderButton()
            so_item = self.si.verifyDisplaySOItems(current_so_item_count)
            self.ts.mark(so_item, "Validating after click AddToOrder button Item Summary should display.")

            # Validating the SO Items Details
            validation_so_item_result = self.si.validateSOEachItemData(so_prepared_data)
            self.ts.mark(validation_so_item_result, "Validating the display SO Item")
            if row == self.shipping_data[len(self.shipping_data) - 1]:
                self.log.info("inside condition")
                calculated_value_from_so_item = self.si.calculateTotalSOItemValue(list_of_calculated_data)

                # Below Method Linear Feet Validation commented due to Bug
                summary_result = self.si.verifySOItemSummary(calculated_value_from_so_item)
                self.ts.mark(summary_result, "Summary Result Validation")
                validate_linear_feet = self.si.validateLinearFeet()
                self.ts.mark(validate_linear_feet, "Validating the Linear Feet")
                prohibited_link_result = self.si.verifyProhibitedCommoditiesLink()
                self.ts.mark(prohibited_link_result, "Validate the Prohibited Link text and file Name")
                shipment_value_result = self.si.verifyTotalShipmentValue(4000)
                self.ts.mark(shipment_value_result, "Validating the Total Shipment Value")
                self.si.enterTotalShipmentValue(300)
                shipment_value_result = self.si.verifyTotalShipmentValue(300)
                self.ts.mark(shipment_value_result, "Validating for the updated Total Shipment Value.")
                index_value = self.si.getShippingItemIndexBasedOnCommodity(row["commodity"])
                self.si.deleteShippingItem(index_value)
                item_result = self.si.verifyDisplayItemByCommodity(row["commodity"])
                self.ts.mark(not item_result, "Validate the item should not display")

        # Notes Section Validation
        default_notes_verification_result = self.qop.defaultNoteValidation()
        self.ts.mark(default_notes_verification_result, "Default Note type Verification result.")

        cancel_result = self.qop.addAndCancelNote("cancel", content="This is for Testing Notes")
        self.ts.mark(cancel_result, "Verify the Cancel Note functionality.")
        notes_section_result = self.qop.verifyNoteSection("User")
        self.ts.mark(notes_section_result, "Verify the Notes Section after clicking cancel Notes")

        adding_notes_result = self.qop.addAndCancelNote("submit", "User", verify="no")
        self.ts.mark(adding_notes_result, "Validating the Empty Notes")
        validate_message_result = self.qop.verifyNotesValidation()
        self.ts.mark(validate_message_result, "Validating the message if no text is entered.")
        adding_notes_result = self.qop.addAndCancelNote("submit", "User", content="Testing Notes")
        self.ts.mark(adding_notes_result, "Validating the Added notes in Notes Section")

        self.ts.mark_final("test_add_shipping_items_data", True,
                           "Validating the Shipping items Add to Order functionality.")

    def test_validate_added_product_from_product_book(self):
        self.log.info("Inside test_validate_added_product_from_product_book Method")

        self.si.clearShippingItemsButton()
        total_item_count = self.si.getSOItemCount()
        self.log.info("Total item count - " + str(total_item_count))
        for index in range(int(total_item_count), 0, -1):
            self.log.info("Index value : " + str(index))
            self.si.deleteShippingItem(index)

        for row in self.shipping_data[:2]:
            if row == self.shipping_data[0]:
                product_default = self.si.validateDefaultProductCommodities()
                self.ts.mark(product_default, "Validating the basic validation in Add Product section")

            # Below condition will get removed once NFMC # issue get resolved for 2nd row
            if row == self.shipping_data[0]:
                added_product_result = self.si.addNewProducts(row["commodity"], row["nfmc"], row["className"],
                                                              row["UnitType"], unitcount=row["unitcount"],
                                                              piece=row["piece"], weight=row["weight"],
                                                              length=row["length"], width=row["width"],
                                                              height=row["height"], weightmom=row["weightUnits"],
                                                              dimmom=row["Dim"], stackable=row["stackable"],
                                                              hazmat=row["hazmat"], hazgrp=row["HazmatGroup"],
                                                              hzclass=row["HazmatClass"], prefix=row["prefix"],
                                                              hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                                              emergNo=row["emergencyContact"])
                self.ts.mark(added_product_result[0], "Validating added Product.")
                self.log.info("returned Value : " + str(added_product_result[1]))
                if added_product_result[0]:
                    selected_product = self.si.searchAndSelectProduct(added_product_result[1])
                    self.ts.mark(selected_product, "Validating search and able to select product")
                    # Due to Bug LTL1-3628, unit count field validating as empty
                    if row["UnitType"].lower() == "pallets(40x48)":
                        unit_count = ""
                    else:
                        unit_count = row["unitcount"]
                    valid_all_shipping_data = self.si.verifyCombinedShippingItemData(added_product_result[1],
                                                                                     row["piece"], row["weight"],
                                                                                     unit_count,
                                                                                     row["UnitType"],
                                                                                     row["className"], row["nfmc"],
                                                                                     row["length"], row["width"],
                                                                                     row["height"], row["weightUnits"],
                                                                                     row["Dim"],
                                                                                     stackable=row["stackable"],
                                                                                     hazmat=row["hazmat"],
                                                                                     hzmcode=row["HazmatCode"],
                                                                                     chemName=row["ChemicalName"],
                                                                                     # emergNo=row["emergencyContact"],
                                                                                     emergNo="",
                                                                                     hzGrp=row["HazmatGroup"],
                                                                                     hzclass=row["HazmatClass"],
                                                                                     prefix=row["prefix"],
                                                                                     productBook="yes")
                    self.log.info("shipping Data : " + str(valid_all_shipping_data))
                    self.ts.mark(valid_all_shipping_data, "Validating product data in shipping items")

                    # preparing the data to validate SO Items

                    # Entering unit count, since it is mandatory - Bug LTL1-3628
                    self.si.si_EnterTextOnField("unitcount", "3")

                    list_of_calculated_data = []
                    so_prepared_data = self.si.getEachShippingItemDataForSOItem()
                    list_of_calculated_data.append(so_prepared_data)
                    self.log.info("getItemDataFromShippingItem : " + str(so_prepared_data))
                    # Emergency contact is mandatory, hence entering contact num
                    self.si.si_EnterTextOnField("emergencyContact", "5457452148")
                    self.si.clickAddToOrderButton()

                    # Validating the SO Items Details on adding shipping item from product book
                    validation_so_item_result = self.si.validateSOEachItemData(so_prepared_data)
                    self.ts.mark(validation_so_item_result, "Validating the display SO Item on adding shipping "
                                                            "items from product book")
                else:
                    closing_popup_result = self.si.closeProductPopUp()
                    self.ts.mark(closing_popup_result, "Validating the popup should get closed with out any error")

        self.ts.mark_final("test_validate_added_product_from_product_book", True,
                           "Completed Validation for Added Product Validation")

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")

    def _select_customer(self):
        self.log.info("Selecting Customer")
        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.address_data[0]["customer_name"], 1)

    def _select_and_verify_address(self):
        self.log.info("Selecting address Method")
        for row in self.address_data:
            if row == self.address_data[0]:
                verifiedOriginTextResult = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'], "origin")
                self.ts.mark(verifiedOriginTextResult, "Populated origin text is same as selected from dropdown")

                # Get the site type for the selected origin address and validate it
                verify_origin_SiteTypeResult = self.rdp.verifySiteTypeSelected("business", "origin")
                self.ts.mark(verify_origin_SiteTypeResult, "site type should be business by default for direct "
                                                           "search of origin address")

                self.rdp.selectSiteType(row["origin_site_type"], "origin")
                verifyOriginSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
                self.ts.mark(verifyOriginSiteTypeSelected, "Site Type selected is same as given for origin address")

            if row == self.address_data[1]:
                verifiedDestinationTextResult = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                      "destination")
                self.ts.mark(verifiedDestinationTextResult, "Populated destination text is same as selected from "
                                                            "dropdown")

                # Get the site type for the selected destination address and validate it
                verify_dest_SiteTypeResult = self.rdp.verifySiteTypeSelected("business", "destination")
                self.ts.mark(verify_dest_SiteTypeResult, "site type should be business by default for direct "
                                                         "search "
                                                         "of destination address")
                self.rdp.selectSiteType(row["destination_site_type"], "destination")
                verifyDestSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                             "destination")
                self.ts.mark(verifyDestSiteTypeSelected, "Site Type selected is same as given for destination "
                                                         "address")

    def _set_freight_date(self):
        # current_date = self.qop.getCurrentDate()
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")
