import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.sendingMail.sendMail_page import SendMailPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup_orders")
class SavedQuoteSendEmailTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")
    shipping_data = read_csv_data("shippingItems_testing_data.csv")
    login_data = read_csv_data("login_details.csv")[0]
    validators = Validators(logger=log)
    util = Util(logger=log)

    saved_quote_reference = ""
    quote_value = ""
    quote_navigation = False
    origin_value = ""
    destination_value = ""

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).login_page = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).si = ShippingItemsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).email = SendMailPage(self.driver, self.log)
        type(self).quote_navigation = False
        self._login()
        self._select_customer()
        self._select_and_verify_address()
        self._enter_shipping_data()
        self._select_carrier()

    def test_saved_quote_send_email_feature(self):
        self.log.info("Inside test_saved_quote_send_email_feature Method")

        if self.quote_navigation:
            verify_confirm_navigation = self.qop.clickButtonInQuote("navigationconfirm")
            self.ts.mark(verify_confirm_navigation, "Validating Navigation Confirm Button")

            self.sop.soBoardLoadWait()
            filter_result = self.sop.clickAndValidateShipmentStatusFilter("Quotes")
            self.ts.mark(filter_result,
                         "SO Board LTL Filter Tab Quotes is clicked and the same is in selected state.")

            save_quote_ref_status_in_filter = self.sop.applyFilterBasedOnColumnName("referenceNumber",
                                                                                    self.saved_quote_reference)
            self.ts.mark(save_quote_ref_status_in_filter,
                         "Saved quote reference number present in quotes tab filter.")

            # Navigating to Saved Quote Popup and its Validations
            open_saved_quote_popup_result = self.sop.openSavedQuotePopup(self.quote_value)
            self.ts.mark(open_saved_quote_popup_result, "Validated the navigation to saved quote popup")

            # Validation of Saved quote Email popup
            self.sop.clickButtonInSavedQuotePopup("sendemail")
            self.car.wait_for_email_popup()
            self.log.debug("Get Quote # in Quote Page : " + str(self.quote_value))

            prepared_subject = \
                "[" + self.origin_value + "] - [" + self.destination_value + "] Quote # " + self.quote_value
            subject_line_validation = self.car.validateEmailPopSubject(prepared_subject)
            self.ts.mark(subject_line_validation, "Validating the Subject Line")

            popup_validation = self.car.validate_email_popup()
            self.ts.mark(popup_validation, "Validating the Email Popup")
            field_validation = self.car.email_field_validation()
            self.ts.mark(field_validation, "Validating the Field Validation in Email Popup")

            # Validating the Contact Popup fields and flow:
            validate_contact_popup = \
                self.car.click_and_validate_select_contact_pop_up(self.address_data[0]["customer_name"])
            self.ts.mark(validate_contact_popup, "Validating the ContactList and field value in Popup")

            cancel_flow_check = self.car.click_email_pop_up_button(buttonName="cancel")
            self.ts.mark(cancel_flow_check, "Validating after click cancel popup should get closed")

            # Validating the Sending Email Feature
            self.sop.clickButtonInSavedQuotePopup("sendemail")
            self.car.wait_for_email_popup()

            # Entering mail id in Email Popup
            self.car.enterEmailId()

            validating_send_result = self.car.click_email_pop_up_button("Send")
            self.ts.mark(validating_send_result, "Validating the Send Email Button Feature")

            # Opening Mail in new Tab and login to the mail server
            self.email.openMailPage()
            self.email.loginMailServer()
            self.email.emailPageRefresh()

            email_received_result = self.email.searchAndSelectEmail(prepared_subject)
            self.ts.mark(email_received_result, "Validating the Email Receive in recipient mail id.")
            validate_attachment = self.email.verifyRateQuoteAttachment()
            self.ts.mark(validate_attachment, " Validating the emailRates Attachment in recipient mail id.")

            self.ts.mark_final("test_saved_quote_send_email_feature", True,
                               "Validating the Saved Quote send Email feature")

    def _login(self):
        self.log.info("Login to the Application")
        self.login_page.login(self.login_data["userName"], self.login_data["password"])
        self.login_page.verifyLoginSuccessful()

    def _select_customer(self):
        self.log.info("Selecting Customer")
        self.qop.click_quote_order_icon()
        self.qop.searchAndSelectCustomer(self.address_data[0]["customer_name"], 1)

    def _select_and_verify_address(self):
        self.log.info("Selecting address Method")
        for row in self.address_data:
            if row == self.address_data[0]:
                # Validate direct address search in origin textfield and select the required address from
                # dropdown list
                verifiedOriginTextResult = self.rdp.directSearchAndSelectAddress(row['random_origin_zipcode'], "origin")
                self.ts.mark(verifiedOriginTextResult, "Populated origin text is same as selected from dropdown")

                self.rdp.selectSiteType(row["origin_site_type"], "origin")

                verifyOriginSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["origin_site_type"], "origin")
                self.ts.mark(verifyOriginSiteTypeSelected, "Site Type selected is same as given for origin address")

            if row == self.address_data[1]:
                # Validate direct address search in destination textfield and select the required address from
                # dropdown list
                verifiedDestinationTextResult = self.rdp.directSearchAndSelectAddress(row['random_dest_zipcode'],
                                                                                      "destination")
                self.ts.mark(verifiedDestinationTextResult, "Populated destination text is same as selected from "
                                                            "dropdown")

                self.rdp.selectSiteType(row["destination_site_type"], "destination")
                verifyDestSiteTypeSelected = self.rdp.verifySiteTypeSelected(row["destination_site_type"],
                                                                             "destination")
                self.ts.mark(verifyDestSiteTypeSelected, "Site Type selected is same as given for destination "
                                                         "address")

        # Validate Freight Date is set to System Date
        validate_next_date_selected_result = self.rdp.dateSelection()
        self.ts.mark(validate_next_date_selected_result, "Freight date is set to passed Date")

    def _enter_shipping_data(self):
        for row in self.shipping_data[:1]:
            self.log.info(row)
            self.si.entryShippingItemsData(row["commodity"], row["piece"], row["weight"], row["unitcount"],
                                           row["UnitType"], row["className"], row["nfmc"], row["length"], row["width"],
                                           row["height"], row["weightUnits"], row["Dim"], stackable=row["stackable"],
                                           hazmat=row["hazmat"], hazgrp=row["HazmatGroup"], hzclass=row["HazmatClass"],
                                           # prefix=row["prefix"],
                                           hzcode=row["HazmatCode"], chemName=row["ChemicalName"],
                                           emergNo=row["emergencyContact"])

            self.log.info("Inserted data in shipping items")

            self.si.clickAddToOrderButton()

    def _select_carrier(self):
        self.log.info("Inside Select Carrier Method")
        # Get Origin and Destination field Value
        origin = self.rdp.getSelectedAddressInTextField("origin")
        self.origin_value = origin[:origin.find(",") + 1] + origin[origin.find(",") + 1:].strip().replace(",", "")
        dest = self.rdp.getSelectedAddressInTextField("destination")
        self.destination_value = dest[:dest.find(",") + 1] + dest[dest.find(",") + 1:].strip().replace(",", "")

        carrier_data = self.car.getSelectedCarrierData("2", "0", "yes")
        if carrier_data is not None:
            # Saving Quote Data with Reference #
            self.quote_value = self.qop.getIdentifierValue("quote")
            self.log.debug("Get Quote Value : " + str(self.quote_value))

            self.saved_quote_reference = "SQREF" + str(self.quote_value)
            self.log.info("Saved Quote Reference : " + str(self.saved_quote_reference))

            self.qop.enterReferenceForSaveOrder(self.saved_quote_reference)
            self.sop.clickSalesOrderIcon()
            self.quote_navigation = True
        else:
            self.ts.mark(False, "Carrier list not populated")
