import utilities.custom_logger as cl
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from pages.home.TMSuser_page import TMSUserPage
from utilities.reportteststatus import ReportTestStatus
import unittest2
import pytest
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUp2", "setUp")
class TMSUserTests(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")
    loginData2 = read_csv_data("lockedUser_details.csv")

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp2):
        self.lp = TMSLoginPage(self.driver, self.log)
        self.lop = TMSLogoutPage(self.driver, self.log)
        self.ts = ReportTestStatus(self.driver, self.log)
        self.up = TMSUserPage(self.driver, self.log)
        self.user_valid_cred = self.loginData2[0]
        self.master_valid_credential = self.loginData[3]
        self.url = self.url
        self.log.info("url : " + str(self.url))

    def test_unlock_locked_user(self):
        self.log.info("Inside test_unlock_locked_user Method")

        self._lock_user(5)
        title_verify = self.lp.verifyTitle()
        self.ts.mark(title_verify, "Title Verified")
        self.lp.login(self.master_valid_credential["userName"], self.master_valid_credential["password"])
        verify_admin_page = self.up.verifyListUserLogin()
        self.ts.mark(verify_admin_page, "User Management Login Validation completed for Valid credential")
        unlockProcessStatus = self.up.unlockUser(self.user_valid_cred["userName"])
        self.ts.mark_final("test_valid_unlockUser", unlockProcessStatus, "Validating the Unlock process.")

        self.up.navigate_to_url(self.url)
        self.lp.verifyLoginSuccessful()
        self._logout()
        verify_title = self.lp.verifyTitle()
        self.ts.mark(verify_title, "Title Verified")
        self.lp.login(self.user_valid_cred["userName"], self.user_valid_cred["password"])
        verify_login = self.lp.verifyLoginSuccessful()
        self.ts.mark_final("test_unlock_locked_user", verify_login, "Login Validation completed for Valid credential")

    def _logout(self):
        self.lop.logout()
        logoutResult = self.lop.verifyLogoutSuccessful(self.url)
        self.ts.mark_final("test_logout", logoutResult, "Validating the LogOut")

    def _lock_user(self, iteration):
        self.log.info("Inside _lock_user Method")

        for i in range(iteration):
            self.log.info("Value of i : " + str(i))
            self.lp.login(self.user_valid_cred["userName"], "password")
            result1 = self.lp.verifyDisplayErrorMessage("Invalid username or password")
            self.ts.mark(result1, "Validating correct userName and incorrect password validation Message")
