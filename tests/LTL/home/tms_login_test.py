import pytest
import unittest2

import utilities.custom_logger as cl
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class TMSLoginTests(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True)
    def class_setup(self, oneTimeSetUp):
        self.lp = TMSLoginPage(self.driver, self.log)
        self.lop = TMSLogoutPage(self.driver, self.log)
        self.ts = ReportTestStatus(self.driver, self.log)
        self.credential = self.loginData[0]
        self.url = self.url

    def test_empty_user_name_password_login(self):
        self.log.info("Inside test_empty_user_name_password_login Method")
        self.lp.waitToLoadLoginPage()
        self.lp.clickLoginButton()
        empty_login_result = self.lp.verifyDisplayErrorMessage("Please enter a username and password")
        self.ts.mark(empty_login_result, "Empty UserName Password field validation Message verified")

        self.lp.enterPassword("userName")
        self.lp.clickLoginButton()
        empty_user_name_result = self.lp.verifyDisplayErrorMessage("Please enter a username and password")
        self.ts.mark(empty_user_name_result, "Empty UserName field validation Message verified")

        self.lp.enterUserName("userName")
        self.lp.enterPassword("")
        self.lp.clickLoginButton()
        empty_password_result = self.lp.verifyDisplayErrorMessage("Please enter a username and password")
        self.ts.mark_final("test_empty_user_name_password_login", empty_password_result,
                           "Login Validation Completed for Empty Credential validation Message.")

    def test_invalid_user_name_login(self):
        self.log.info("Inside test_invalid_user_name_login Method")

        title_verify_result = self.lp.verifyTitle()
        self.ts.mark(title_verify_result, "Title Verified")
        self.lp.login(self.credential["userName"], "abcabcabc")
        invalid_cred_result = self.lp.verifyDisplayErrorMessage("Invalid username or password")
        self.ts.mark(invalid_cred_result, "Validating correct userName and incorrect password validation Message")
        self.lp.login("user1", self.credential["password"])
        invalid_cred_result2 = self.lp.verifyDisplayErrorMessage("Invalid username or password")

        self.ts.mark_final("test_invalid_user_name_login", invalid_cred_result2,
                           "Login Validation completed for incorrect Credential Validation Message.")

    def test_valid_login(self):
        self.log.info("Inside test_valid_login method and the UserName is " + str(
            self.credential["userName"]) + " ,password is : " + self.credential["password"])

        title_verify_result = self.lp.verifyTitle()
        self.ts.mark(title_verify_result, "Title Verified")
        self.lp.login(self.credential["userName"], self.credential["password"])
        validate_successful_login_result = self.lp.verifyLoginSuccessful()
        self._logout()

        self.ts.mark_final("test_valid_login", validate_successful_login_result,
                           "Login Validation completed for Valid credential")

    def _logout(self):
        self.lop.logout()
        logoutResult = self.lop.verifyLogoutSuccessful(self.url)
        self.ts.mark_final("test_logout", logoutResult, "Validating the LogOut")
