import utilities.custom_logger as cl

from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.ordersummarysodetails_page import OrderSummarySODetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorderfinancialdetails_page import SalesorderFinancialDetailsPage
from pages.LTL.salesorderboard.salesorderroutedetails_page import SalesorderRouteDetailsPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class NavigateToSOPageFromSOBoardUpdateManualDispatchPopupTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).osp = OrderSummarySODetailsPage(self.driver, self.log)
        type(self).srf = SalesorderFinancialDetailsPage(self.driver, self.log)
        type(self).srd = SalesorderRouteDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[0:1]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def test_navigate_to_so_page_from_so_board_update_manual_dispatch_popup(self):
        self.log.info("Inside test_navigate_to_so_page_from_so_board_update_manual_dispatch_popup Method")

        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        for order in orders:
            row = order.sales_order_data

            filter_result = self.sop.clickAndValidateShipmentStatusFilter("Pending")
            self.ts.mark(filter_result,
                         "Shipment Status Filter Pending is clicked and the same is in selected state.")
            bol_status_in_pending_filter = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if bol_status_in_pending_filter:
                status_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                self.ts.mark(status_result, "Validating status Column value is Pending.")
                # Manual Dispatch popup should open up
                self.sop.clickSOBoardActionButton(row["bolnumber"], "ManualDispatch")
                # Clicking Sales Order Link in the Manual Dispatch popup and there by navigating to
                # respective BOL's sales order page
                self.sop.click_sales_order_link_in_so_board_update_popup(row["bolnumber"])
                so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                self.ts.mark(so_validation_result, "Validate in SO Details page passed BOL should open and display.")
                # Validate Network Info Section
                validate_nw_container_text = self.sdp.ValidateNetworkSection()
                self.ts.mark(validate_nw_container_text, "Validating Network container Text Value")
                validate_customer_name = self.sdp.validateNWSectionBasedOnPassedValue("customer", row["customername"])
                self.ts.mark(validate_customer_name, "Validating the Customer Name in SO Page")
                validate_service_type = self.sdp.validateNWSectionBasedOnPassedValue("servicetype", "LTL")
                self.ts.mark(validate_service_type, "Validating the Service Type in SO Details Page")
                validate_application_name = self.sdp.validateNWSectionBasedOnPassedValue("generatedon")
                self.ts.mark(validate_application_name, "Validating the application name display in SO Details Page")
                validate_contact_name = self.sdp.validateNWSectionBasedOnPassedValue("customercontact",
                                                                                     row["customercontactname"])
                self.ts.mark(validate_contact_name, "Validating the Contact Name display in SO Details Page")
                validate_rating_method = self.sdp.validateNWSectionBasedOnPassedValue("ratingmethod", "Standard Tariff")
                self.ts.mark(validate_rating_method, "Validating the Rating method in SO Details Page")
                validate_quote_request = self.sdp.validateNWSectionBasedOnPassedValue("quoterequest", row["quoteid"])
                self.ts.mark(validate_quote_request, "Validating the Quote request Id in SO Details Page")

                self.sdp.soOrderBoardButton()

            else:
                self.log.error(
                    "After apply filter BOL not found in Sales Order Board for BOL Number : " + str(row["bolnumber"]))
        self.ts.mark_final("test_navigate_to_so_page_from_so_board_update_manual_dispatch_popup", True,
                           "Navigated to Sales Order page upon click on the sales order hyperlink in "
                           "SO board update Manual Dispatch Popup")
