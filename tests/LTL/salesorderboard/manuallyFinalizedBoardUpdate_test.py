import utilities.custom_logger as cl

from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.home.TMSlogout_page import TMSLogoutPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class SalesOrderManualFinalizeTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).lop = TMSLogoutPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        order_shipping = read_csv_data("salesorder_shipping.csv")
        self.__login()

        order_data = read_csv_data("salesOrderData.csv")[4:5]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    @pytest.mark.slowtest
    def test_validate_manual_finalize_from_so_board(self):
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        manually_finalize = False
        for order in orders:
            row = order.sales_order_data
            self.log.info("Printing Row Data : " + str(row))
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol and not manually_finalize:
                manually_finalize = True
                button_status = self.sop.getButtonEnabled(row["bolnumber"], "Finalize")
                self.ts.mark(button_status, "Validating the Manual Finalize Button Status")
                if button_status:
                    self.log.info("inside click event")
                    self.sop.clickSOBoardActionButton(row["bolnumber"], "finalize")
                    bol_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"], "finalize",
                                                                                   "BOL #", row["bolnumber"])
                    self.ts.mark(bol_identifier_result, "Validating the SO Board BOl Data Identifier Result")
                    po_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                  "finalize", "PO#", "")
                    self.ts.mark(po_identifier_result, "Validating the SO Board Data Identifier Result")
                    ref_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                   "finalize", "REF#", "")
                    self.ts.mark(ref_identifier_result, "Validating the SO Board Data Identifier Result")
                    exp_date_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                        "finalize", "Exp. Date")
                    self.ts.mark(exp_date_identifier_result, "Validating the SO Board Data Identifier Result in "
                                                             " Finalize Popup")

                    carrier_result = self.sop.validateBaseDataInSOBoardPopup(row["bolnumber"], "finalize",
                                                                             "Carrier Name",
                                                                             row["selectedcarriername"])
                    self.ts.mark(carrier_result, "Validating the Display Carrier Name in SO Board Action popup.")

                    customer_result = self.sop.validateBaseDataInSOBoardPopup(row["bolnumber"], "finalize",
                                                                              "Customer Name",
                                                                              row["customername"])
                    self.ts.mark(customer_result,
                                 "Validating the Display Customer Name in SO Board Action popup For Finalize.")

                    validate_cons_add = False
                    validate_ship_add = False
                    origin_accessorial = []
                    destination_accessorial = []

                    if row["addressbook"].lower() == "yes":
                        self.log.info("Address Book value is Yes, validate from Address Book")
                        address_index = row["sno"]
                        for r_add in self.order_address_book:
                            phone = r_add["contact_phone"].replace("(", "").replace(")", "").replace("-",
                                                                                                     "").replace(
                                " ", "")
                            if int(address_index) >= int(r_add["sno"]):
                                if int(r_add["sno"]) == int(row["sno"]):
                                    if r_add["addresssection"] == "origin":
                                        self.log.info("Inside Address book origin condiiton")
                                        validate_ship_add = self.sop.validateAddressDataInSOBoard(
                                            "origin",
                                            r_add["company_name"],
                                            r_add["addressline1"],
                                            r_add["addressline2"],
                                            r_add["address"],
                                            r_add["contact_name"],
                                            phone)
                                        origin_accessorial = self.qop.getAccessorialsList(
                                            r_add["origin_accessorials"], "origin")

                                    elif r_add["addresssection"] == "destination":
                                        self.log.info("Inside Address book Destination condiiton")
                                        validate_cons_add = self.sop.validateAddressDataInSOBoard(
                                            "destination",
                                            r_add["company_name"],
                                            r_add["addressline1"],
                                            r_add["addressline2"],
                                            r_add["address"],
                                            r_add["contact_name"],
                                            phone)
                                        destination_accessorial = self.qop.getAccessorialsList(
                                            r_add["destination_accessorials"], "destination")
                                    else:
                                        self.log.error("Incorrect address type passed")
                                        self.ts.mark(False,
                                                     "Incorrect address type passed in Finalize")
                            else:
                                self.log.info("out of address Book selection index")
                                break
                    else:
                        self.log.info("Inside address book no condition")
                        validate_ship_add = self.sop.validateAddressDataInSOBoard("origin", row["oricompanyName"],
                                                                                  row["oriaddline1"],
                                                                                  row["oriaddline2"],
                                                                                  row["orizip"],
                                                                                  row["oricontactName"],
                                                                                  row["oriphone"])
                        validate_cons_add = self.sop.validateAddressDataInSOBoard("destination",
                                                                                  row["destcompanyname"],
                                                                                  row["destaddline1"],
                                                                                  row["destaddline2"],
                                                                                  row["destzip"],
                                                                                  row["destcontactname"],
                                                                                  row["destphone"])
                        origin_accessorial = self.qop.getAccessorialsList(row["originacc"], "origin")
                        destination_accessorial = self.qop.getAccessorialsList(row["destinationacc"],
                                                                               "destination")
                    self.ts.mark(validate_ship_add, "Validating the Shipper Address display in SO Board Popup "
                                                    "for Finalize")
                    self.ts.mark(validate_cons_add, "Validating the Consignee Address display in SO Board Popup "
                                                    "for Finalize")

                    shipping_val = True
                    for si_row in order.sales_order_shipping:
                        self.log.info("row value si :" + str(si_row["sno"]) + " ,main row : " + str(row["sno"]))
                        if int(si_row["sno"]) <= int(row["sno"]):
                            self.log.info("shipping row No. : " + str(si_row["sno"]))
                            if int(si_row["sno"]) == int(row["sno"]):
                                if si_row["productbook"].lower() == "no":
                                    self.log.info("Inside Product book no condition")
                                    validate_shipping_data = self.sop.validateShippingItemData(si_row["commodity"],
                                                                                               si_row["weight"],
                                                                                               si_row["weightUnits"],
                                                                                               si_row["UnitType"],
                                                                                               si_row["unitcount"],
                                                                                               si_row["piece"],
                                                                                               si_row["length"],
                                                                                               si_row["width"],
                                                                                               si_row["height"],
                                                                                               si_row["Dim"],
                                                                                               si_row["className"],
                                                                                               si_row["hazmat"])
                                else:
                                    self.log.info("Inside Product book Yes condition")
                                    validate_shipping_data = self.sop.validateShippingItemData(
                                        si_row["newcommodity"], si_row["weight"],
                                        si_row["weightUnits"], si_row["UnitType"],
                                        si_row["unitcount"], si_row["piece"],
                                        si_row["length"], si_row["width"],
                                        si_row["height"], si_row["Dim"],
                                        si_row["className"], si_row["hazmat"])

                                self.log.info("Shipping Data Value : " + str(validate_shipping_data))
                                shipping_val = shipping_val and validate_shipping_data
                            self.log.info("Reading Logs")
                        else:
                            self.log.debug("Out side expected condition")
                            break

                    self.ts.mark(shipping_val, "Validating the Shipping Data in SO Board Popup for Finalize.")

                    complete_acc_list = origin_accessorial + destination_accessorial
                    self.log.info("Complete accessorial List : " + str(complete_acc_list))

                    acc_breakup_value = self.car.accessorialBreakupValidationList(complete_acc_list)
                    self.log.info("Accessorial breakup Value : " + str(acc_breakup_value))

                    break_up_value_list = self.sop.validateAndAddAccessorial(acc_breakup_value,
                                                                             row["carrierbreakup"])
                    self.log.info("Final List : " + str(break_up_value_list))

                    accessorial_result = self.sop.validateAccessorialList(break_up_value_list)
                    self.ts.mark(accessorial_result, "Validating the Display Accessorial in SO Board Popup for "
                                                     "Finalize.")
                    proNo = "pro" + row["bolnumber"]
                    validate_finalize = self.sop.clickOnActionButtonOnSOBoardPopup("finalize", proNumber=proNo,
                                                                                   pickupDate="1")
                    self.ts.mark(validate_finalize[0],
                                 "Validating the Finalize Action in SO Board popup for Finalize")

                    status_result = self.sop.validateColumnValue("status", "Manually Finalized", row["bolnumber"])
                    self.ts.mark(status_result, "Validating the updated Status in SO Board for Finalize.")

                    pro_number_result = self.sop.validateColumnValue("pro", proNo, row["bolnumber"])
                    self.ts.mark(pro_number_result, "Validating the updated PRO Number while doing Finalize.")

                    pickup_date_result = self.sop.validateColumnValue("pickupDate", validate_finalize[1],
                                                                      row["bolnumber"])
                    self.ts.mark(pickup_date_result, "Validating the updated Pickup Date while doing Finalize.")

                    button_status = self.sop.getButtonEnabled(row["bolnumber"], "Finalize")
                    self.ts.mark(not button_status, "Validated the Manual Finalize circle Button is in greyed "
                                                    "state.")

                    # Validating Finalize will not happen again once Finalize is done
                    if validate_finalize[0]:
                        self.sop.clickSOBoardActionButton(row["bolnumber"], "finalize")
                        finalize_button_status = self.sop.getButtonEnabledInPopups("Finalize")
                        self.ts.mark(not finalize_button_status, "Validated the Finalize Button is in greyed state in"
                                                                 " finalize popup.")
                        add_note_button_status = self.sop.getButtonEnabledInPopups("addnote")
                        self.ts.mark(not add_note_button_status, "Validated the Add Note Button is in greyed state in"
                                                                 " finalize popup.")
                    else:
                        self.log.error("Finalize operation did not happened.")

                    self.sop.navigateToSOPage(row["bolnumber"])
                    self.sdp.soPageLoadWait(25)
                    validate_pro_identifier = self.sdp.getIdentifierValue("PRO #", proNo)
                    self.ts.mark(validate_pro_identifier,
                                 "Validating the Added PRO in SO Details Page after finalize"
                                 " order.")
                    # Status Section Validation
                    validate_process_status = self.sdp.validateStatusSectionBasedOnPassedValue("process",
                                                                                               "Manual Finalized")
                    self.ts.mark(validate_process_status,
                                 "Validating the Process status value in Status section after Manually Finalize")

                    validate_invoice_status = self.sdp.validateStatusSectionBasedOnPassedValue("invoice",
                                                                                               "Pending")
                    self.ts.mark(validate_invoice_status,
                                 "Validating the Invoice status in Status Section after Manually Finalize")

                    validate_vendor_status = self.sdp.validateStatusSectionBasedOnPassedValue("Vendorbill",
                                                                                              "None")
                    self.ts.mark(validate_vendor_status,
                                 "Validating the vendor Bill status in Status section after Manually Finalize")

                    validate_edi_status = self.sdp.validateStatusSectionBasedOnPassedValue("edi", "")
                    self.ts.mark(validate_edi_status,
                                 "Validating the EDI Status in Status Section after Manually Finalize")

                    validate_tracking_status = self.sdp.validateStatusSectionBasedOnPassedValue("tracking",
                                                                                                "IN TRANSIT")
                    self.ts.mark(validate_tracking_status,
                                 "Validating the Tracking status in Status Section after Manually Finalize")

                    self.sdp.soOrderBoardButton()
                    self.sop.soBoardLoadWait()

        if not manually_finalize:
            self.log.error(
                "None of the passed BOL Has Pending/Dispatch Status To verify Manually Finalize Flow from "
                "SO Board, hence Failing the case.")
            self.ts.mark(False, "Manually Finalize from SO Board Failed as not found any SO with proper status")

        self.ts.mark_final(
            "test_validate_manual_finalize_from_so_board",
            True,
            "Validating the SO Board Data Update on Manually Finalize from SO Board.")

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
