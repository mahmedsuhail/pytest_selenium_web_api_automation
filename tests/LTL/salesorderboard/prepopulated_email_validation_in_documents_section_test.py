import utilities.custom_logger as cl
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class PrepopulatedEmailValidationInDocumentSectionTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).order_data = read_csv_data("salesOrderData.csv")[0:1]
        type(self).ordershipping = read_csv_data("salesorder_shipping.csv")
        type(self).orderAddressBook = read_csv_data("salesorder_addressbook.csv")
        type(self).ts = ReportTestStatus(self.driver, self.log)
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        self.__login()
        type(self).create_orders_result = sales_order_creator.create_orders(self.order_data, self.orderAddressBook,
                                                                            self.ordershipping)
        self.__navigate_to_sales_order_board()

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __navigate_to_sales_order_board(self):
        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

    def test_validate_email_popup_prepopulated_with_email_address(self):
        self.log.info("Inside test_validate_email_popup_prepopulated_with_email_address Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")

                        self.snd.navigate_to_email_document_popup("bolandlabel")
                        email_popup_with_prepopulated_email_result_for_bol_and_label = \
                            self.snd.validate_email_popup_with_prepopulated_email()
                        self.ts.mark(email_popup_with_prepopulated_email_result_for_bol_and_label,
                                     "Validated To field is prepopulated with email address in Email Document popup of "
                                     "BOL & Label in Document section in Sales Order page.")

                        self.snd.navigate_to_email_document_popup("billoflading")
                        email_popup_with_prepopulated_email_result_for_bill_of_lading = \
                            self.snd.validate_email_popup_with_prepopulated_email()
                        self.ts.mark(email_popup_with_prepopulated_email_result_for_bill_of_lading,
                                     "Validated To field is prepopulated with email address in Email Document popup of "
                                     "Bill of Lading in Document section in Sales Order page.")

                        self.sdp.soOrderBoardButton()
                        self.sop.soBoardLoadWait()
                    else:
                        self.log.error("After apply filter BOL not found in Sales Order Board for BOL Number : " +
                                       str(row["bolnumber"]))

        self.ts.mark_final("test_validate_email_popup_prepopulated_with_email_address", True,
                           "Validated To field is prepopulated with email address in Email Document popup of"
                           "BOL & Label and Bill of Lading in Document section in Sales Order page.")
