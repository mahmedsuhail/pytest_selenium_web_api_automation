import utilities.custom_logger as cl

from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class ManualDispatchFromSalesOrderPageTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[3:4]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    @pytest.mark.slowtest
    def test_manual_dispatch_flow_from_sales_order_page(self):
        self.log.info("Inside test_manual_dispatch_flow_from_sales_order_page Method")

        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")
        manual_Dispatch_flag = False
        for order in orders:
            row = order.sales_order_data

            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")

                        if not manual_Dispatch_flag:
                            manual_Dispatch_flag = True
                            dispatch_status = self.sdp.clickAndValidateManualDispatch()
                            self.ts.mark(dispatch_status, "Validating the Dispatch flow from SO Details Page")

                            # Status Section Validation
                            validate_process_status = \
                                self.sdp.validateStatusSectionBasedOnPassedValue("process", "Manual Dispatch")
                            self.ts.mark(validate_process_status,
                                         "Validating the Process status value in Status section after Manual Dispatch")

                            validate_invoice_status = self.sdp.validateStatusSectionBasedOnPassedValue("invoice",
                                                                                                       "Pending")
                            self.ts.mark(validate_invoice_status,
                                         "Validating the Invoice status in Status Section after Manual Dispatch")

                            validate_vendor_status = self.sdp.validateStatusSectionBasedOnPassedValue("Vendorbill",
                                                                                                      "None")
                            self.ts.mark(validate_vendor_status,
                                         "Validating the vendor Bill status in Status section after Manual Dispatch")

                            validate_EDI_status = self.sdp.validateStatusSectionBasedOnPassedValue("edi", "")
                            self.ts.mark(validate_EDI_status,
                                         "Validating the EDI Status in Status Section after Manual Dispatch")

                            validate_tracking_status = self.sdp.validateStatusSectionBasedOnPassedValue("tracking",
                                                                                                        "Dispatched")
                            self.ts.mark(validate_tracking_status,
                                         "Validating the Tracking status in Status Section after Manual Dispatch")

                            self.sdp.soOrderBoardButton()
                            Filter_result = self.sop.clickAndValidateShipmentStatusFilter("Dispatched")
                            self.ts.mark(Filter_result,
                                         "Shipment Status Filter Dispatched is clicked and the same is in selected"
                                         " state.")

                            bol_status_in_dispatched_filter = self.sop.applyFilterBasedOnColumnName("billOfLading",
                                                                                                    row["bolnumber"])
                            self.ts.mark(bol_status_in_dispatched_filter, "BOL is present in Dispatch Filter.")
                            status_result = self.sop.validateColumnValue("status", "Manual Dispatch", row["bolnumber"])
                            self.ts.mark(status_result, "Validating the updated Status in SO Board for Manual "
                                                        "Dispatch.")

                            # Validation of BOL not present in other filters
                            self.sop.clickAndValidateShipmentStatusFilter("intransit")
                            self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                            bol_status_in_intransit_filter = self.sop.validateNoRowsToShow()
                            self.ts.mark(bol_status_in_intransit_filter,
                                         "Validated Dispatched Status BOL is not present in InTransit Filter.")
                            self.sop.clickAndValidateShipmentStatusFilter("delivered")
                            self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                            bol_status_in_delivered_filter = self.sop.validateNoRowsToShow()
                            self.ts.mark(bol_status_in_delivered_filter,
                                         "Validated Dispatched Status BOL is not present in Delivered Filter.")
                            self.sop.clickAndValidateShipmentStatusFilter("pending")
                            self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                            bol_status_in_pending_filter = self.sop.validateNoRowsToShow()
                            self.ts.mark(bol_status_in_pending_filter,
                                         "Validated Dispatched Status BOL is not present in Pending Filter.")
        if not manual_Dispatch_flag:
            self.log.error("None of the passed BOL Has Pending Status To verify Manual Dispatch Flow hence Failing "
                           "the case.")
            self.ts.mark(False, "Manual Dispatch from SO Details Page Failed as not found any SO with proper status")
        self.ts.mark_final(
            "test_manual_dispatch_flow_from_sales_order_page",
            True,
            "Validating the Manual Dispatch flow from SO Details Page")
