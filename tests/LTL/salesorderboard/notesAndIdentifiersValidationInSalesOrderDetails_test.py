import utilities.custom_logger as cl
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class NotesAndIdentifiersValidationInSODetailsTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).order_data = read_csv_data("salesOrderData.csv")[0:1]
        type(self).ordershipping = read_csv_data("salesorder_shipping.csv")
        type(self).orderAddressBook = read_csv_data("salesorder_addressbook.csv")
        type(self).ts = ReportTestStatus(self.driver, self.log)
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        self.__login()
        type(self).create_orders_result = sales_order_creator.create_orders(self.order_data, self.orderAddressBook,
                                                                            self.ordershipping)
        self.__navigate_to_sales_order_board()

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __navigate_to_sales_order_board(self):
        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

    @pytest.mark.slowtest
    def test_validate_notes_in_sales_order_page(self):
        self.log.info("Inside test_validate_notes_in_sales_order_page Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")

                        # Notes Section Edit
                        self.log.info("SalesOrder Page Notes Section Edit")
                        default_so_page_notes_container_result = self.snd.defaultSOPageNotesContainerValidation()
                        self.ts.mark(default_so_page_notes_container_result,
                                     "Default SalesOrder Page Notes Container Section Verified.")
                        cancel_result = self.snd.addAndCancelNoteInSOPage(
                            "cancel", "POD Signed By", content="This is for Testing Notes")
                        self.ts.mark(cancel_result, "Verify the Cancel Note functionality.")
                        adding_notes_result = self.snd.addAndCancelNoteInSOPage("submit", "User", verify="no")
                        self.ts.mark(adding_notes_result, "Validating the Empty Notes.")
                        validateMessageResult = self.snd.verifyNotesValidationInSOPage()
                        self.ts.mark(validateMessageResult, "Validating the message if no text is entered.")

                        #  Due to Bug below method for adding notes get commented
                        adding_notes_result = True
                        adding_notes_result = adding_notes_result and self.snd.addAndCancelNoteInSOPage(
                            "submit", "POD Signed By", content="Testing POD Signed By Notes in SO Page")
                        adding_notes_result = adding_notes_result and self.snd.addAndCancelNoteInSOPage(
                            "submit", "Financial", content="Testing Financial Notes in SO Page")
                        self.ts.mark(adding_notes_result, "Validated the Added notes in SO Notes Section.")

                        # Saved Notes validation
                        saved_notes_result = True
                        saved_notes_result = saved_notes_result and self.snd.validateSavedNotesInSOPage(
                            "POD Signed By", passedContent="Testing POD Signed By Notes in SO Page")
                        saved_notes_result = saved_notes_result and self.snd.validateSavedNotesInSOPage(
                            "Financial", passedContent="Testing Financial Notes in SO Page")
                        self.ts.mark(saved_notes_result, "Validated the Added notes in SO Notes Section "
                                                         "after navigating back to SO Page.")

                        self.sdp.soOrderBoardButton()
                        self.sop.soBoardLoadWait()
            else:
                self.log.error("After apply filter BOL not found in Sales Order Board for BOL Number : " +
                               str(row["bolnumber"]))
        self.ts.mark_final("test_validate_notes_in_sales_order_page", True,
                           "Validated Notes Functionality in Sales Order page.")

    @pytest.mark.slowtest
    def test_validate_identifier_in_sales_order_page(self):
        self.log.info("Inside test_validate_identifier_in_sales_order_page Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        for order in orders:
            row = order.sales_order_data

            po_num = "PO" + row["bolnumber"]
            ref = "REF" + row["bolnumber"]
            pu_num = "PU" + row["bolnumber"]
            pro = row["bolnumber"]
            custom_bol = "CU" + row["bolnumber"]
            self.log.info(
                "Identifier Value : " + str(po_num) + " ," + ref + " ," + pu_num + " ," + pro + " ," + custom_bol)

            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")

                        # Identifier Edit Section Validations
                        default_identifier_edit_validations_result = self.sdp.defaultIdentifierEditValidations()
                        self.ts.mark(default_identifier_edit_validations_result,
                                     "Validating the default Edit Identifier Section in SO Page")

                        save_identifier_po_result = \
                            self.sdp.identifierEditSectionButtonBehaviour("P/O #", "saveandconfirm", po_num)
                        self.ts.mark(save_identifier_po_result,
                                     "Validating the saveandconfirm(P/O #) behaviour in Edit Identifier Section"
                                     " in SO Page")

                        save_identifier_ref_result = \
                            self.sdp.identifierEditSectionButtonBehaviour("Reference #", "saveandconfirm", ref)
                        self.ts.mark(save_identifier_ref_result,
                                     "Validating the saveandconfirm(Ref #) behaviour in Edit Identifier Section"
                                     " in SO Page")

                        save_identifier_pu_result = \
                            self.sdp.identifierEditSectionButtonBehaviour("P/U #", "saveandconfirm", pu_num)
                        self.ts.mark(save_identifier_pu_result,
                                     "Validating the saveandconfirm(P/U #) behaviour in Edit Identifier Section"
                                     " in SO Page")

                        save_identifier_pro_result = \
                            self.sdp.identifierEditSectionButtonBehaviour("PRO #", "saveandconfirm", pro)
                        self.ts.mark(save_identifier_pro_result,
                                     "Validating the saveandconfirm(PRO #) behaviour in Edit Identifier Section"
                                     " in SO Page")

                        save_identifier_cbol_result = \
                            self.sdp.identifierEditSectionButtonBehaviour("Customer BOL #", "saveandconfirm",
                                                                          custom_bol)
                        self.ts.mark(save_identifier_cbol_result,
                                     "Validating the saveandconfirm(Customer BOL #) behaviour in "
                                     "Edit Identifier Section in SO Page")
                        self.sdp.soOrderBoardButton()
                        self.sop.soBoardLoadWait()
                    else:
                        self.log.error("After apply filter BOL not found in Sales Order Board for BOL Number : " +
                                       str(row["bolnumber"]))

        self.ts.mark_final("test_validate_identifier_in_sales_order_page", True,
                           "Validated Identifier Functionality in Sales Order page.")

    @pytest.mark.slowtest
    def test_validate_request_vendor_document_in_sales_order_page(self):
        self.log.info("Inside test_validate_request_vendor_document_in_sales_order_page Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")

                        # Request Vendor Doc validation
                        doc_request_behaviour_result = self.snd.requestVendorDocumentsBehaviour("submit")
                        self.ts.mark(doc_request_behaviour_result,
                                     "Validated successful request for Request Document.")
                        self.sdp.soOrderBoardButton()
                        self.sop.soBoardLoadWait()
                    else:
                        self.log.error("After apply filter BOL not found in Sales Order Board for BOL Number : " +
                                       str(row["bolnumber"]))

        self.ts.mark_final("test_validate_request_vendor_document_in_sales_order_page", True,
                           "Validated request vendor document  Functionality in Sales Order page.")

    @pytest.mark.slowtest
    def test_validate_create_new_order_redirection_from_sales_order_page(self):
        self.log.info("Inside test_validate_create_new_order_redirection_from_sales_order_page Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")

                        create_new_order_button_status_result = self.sdp.getButtonEnabled("createneworder")
                        self.ts.mark(create_new_order_button_status_result,
                                     "Validated Create New Order Button Status when BOL status is pending")
                        self.sdp.clickSalesOrderPageActionButton("createneworder")
                        create_new_order_nav_result = self.qop.verifyQuotePage()
                        self.ts.mark(create_new_order_nav_result,
                                     "Create New Order : Validated the navigation to Create Quote Page from SO page")
                        self.sop.clickSalesOrderIcon()
                        self.sop.soBoardLoadWait()
                    else:
                        self.log.error("After apply filter BOL not found in Sales Order Board for BOL Number : " +
                                       str(row["bolnumber"]))

        self.ts.mark_final("test_validate_create_new_order_redirection_from_sales_order_page", True,
                           "Validated new order redirection from Sales Order page.")
