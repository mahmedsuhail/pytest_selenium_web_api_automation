import utilities.custom_logger as cl
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from utilities.reportteststatus import ReportTestStatus
import unittest2
import pytest
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class SalesOrderDefaultTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.lp = TMSLoginPage(self.driver, self.log)
        self.sop = SalesorderPage(self.driver, self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.mark.regression
    @pytest.mark.smoke
    def test_default_sales_order_board_validation(self):
        self.lp.waitToLoadLoginPage()
        credential = self.loginData[0]
        self.lp.login(credential["userName"], credential["password"])
        result_1 = self.lp.verifyLoginSuccessful()
        self.ts.mark(result_1, "Successfully Logged In")
        result_2 = self.sop.validateSalesOrderIcon()
        self.ts.mark(result_2, "Sales order Icon is active")
        salesorder_headersresult = self.sop.validateSalesBoardColumnHeaders()
        self.ts.mark(salesorder_headersresult, "Verified Sales Order Board Column Headers")
        validated_default_board_result = self.sop.validateSalesOrderBoardByDefault()
        self.ts.mark_final(
            "test_default_sales_order_board_validation",
            validated_default_board_result,
            "Verified Default Sales Order Board")
