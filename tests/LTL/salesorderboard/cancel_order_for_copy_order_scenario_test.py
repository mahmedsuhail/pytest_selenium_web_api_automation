import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.ordersummarysodetails_page import OrderSummarySODetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorderfinancialdetails_page import SalesorderFinancialDetailsPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
from pages.LTL.salesorderboard.salesorderroutedetails_page import SalesorderRouteDetailsPage
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util

""" Copy order feature is not working, since copy feature is major scenario to validate data,skipping this test case"""


@pytest.mark.usefixtures("oneTimeSetUp", "login_and_setup_orders")
class CancelOrderForCopyOrderScenarioTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")
    util = Util(logger=log)

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).osp = OrderSummarySODetailsPage(self.driver, self.log)
        type(self).srf = SalesorderFinancialDetailsPage(self.driver, self.log)
        type(self).srd = SalesorderRouteDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[0:1]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    @pytest.mark.jira("LTL1-3732")
    def test_cancel_order_scenario_copy_order(self):
        self.log.info("Inside test_cancel_order_scenario_copy_order Method")

        copy_order = False
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                if navigation_result and so_validation_result:
                    if not copy_order:
                        copy_order = True

                        copy_order_result = self.sdp.clickAndValidateCopyOrderButton()
                        self.ts.mark(copy_order_result[0], "Validate the Copy Order Feature in SO Details Page")

                        validate_nw_container_text = self.sdp.ValidateNetworkSection()
                        self.ts.mark(validate_nw_container_text, "Validating Network container Text Value")

                        validate_customer_name = self.sdp.validateNWSectionBasedOnPassedValue("customer",
                                                                                              row["customername"])
                        self.ts.mark(validate_customer_name, "Validating the Customer Name in SO Page")

                        self.log.info("Insurance Value : " + str(row["selectcarrierrate"]))
                        result_cancel_by_customer = self.sdp.clickAndValidateCancelFlow(self.loginData[0]["userName"],
                                                                                        "no",
                                                                                        "By customer")

                        self.ts.mark(result_cancel_by_customer, "Validating the Cancel By Customer Result")
                        self.sdp.soOrderBoardButton()

        if not copy_order:
            self.log.error("BOL Not found or Not able to redirect to SO Details Page")
            self.ts.mark(False, "BOL Not found or Not able to redirect to SO Details Page")

        self.ts.mark_final("test_cancel_order_scenario_copy_order", True, "Validating the Cancel Order Flow for "
                                                                          "Copy Order.")
