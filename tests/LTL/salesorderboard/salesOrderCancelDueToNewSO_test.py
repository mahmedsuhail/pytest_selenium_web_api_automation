import utilities.custom_logger as cl

from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class SalesOrderCancelDueToNewSOFlowTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[2:3]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    @pytest.mark.slowtest
    def test_sales_order_cancel_due_to_new_so_flow(self):
        self.log.info("Inside test_sales_order_cancel_due_to_new_so_flow Method")

        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        cancel_new_so_flag = False
        accepted_statuses = ["Pending", "Auto Dispatch", "Manual Dispatch"]
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", accepted_statuses, row["bolnumber"])
                if value_compare_result and not cancel_new_so_flag:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:
                        self.sdp.soPageLoadWait(15)
                        cancel_new_so_flag = True

                        self.log.info("Insurance Value : " + str(row["selectcarrierrate"]))
                        result_cancel_new_so = self.sdp.clickAndValidateCancelFlow(self.loginData[0]["userName"],
                                                                                   row["selectcarrierrate"], "newso")
                        self.ts.mark(result_cancel_new_so, "Validating the Cancel Due to New SO Result")
                        self.sdp.soOrderBoardButton()

                        self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                        status_result = self.sop.validateColumnValue("status", "Canceled", row["bolnumber"])
                        self.ts.mark(status_result, "Validating the updated Status in SO Board for canceling Order")

        if not cancel_new_so_flag:
            self.log.error("None of the passed BOL Has proper Status To verify Cancel Flow with New SO option "
                           "hence Failing the case.")
            self.ts.mark(False, "verify Cancel Flow with New SO option from SO Details Page Failed as not found "
                                "any SO with proper status")
        self.ts.mark_final(
            "test_sales_order_cancel_due_to_new_so_flow",
            True,
            "Validating the cancel Sales order By Customer flow in SO Details Page")
