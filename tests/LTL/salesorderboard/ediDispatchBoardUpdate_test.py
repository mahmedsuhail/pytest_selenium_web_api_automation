import utilities.custom_logger as cl

from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.home.TMSlogout_page import TMSLogoutPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class EdiDispatchBoardUpdateTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).lop = TMSLogoutPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).lop = TMSLogoutPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[3:4]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    @pytest.mark.slowtest
    def test_validate_edi_dispatch_from_so_board(self):
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        edi_dispatch = False
        for order in orders:
            row = order.sales_order_data
            self.log.info("Printing Row Data : " + str(row))
            if not edi_dispatch:
                applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                if applied_bol and not edi_dispatch:
                    value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                    if value_compare_result:
                        edi_dispatch = True
                        button_status = self.sop.getButtonEnabled(row["bolnumber"], "EDIDispatch")
                        self.ts.mark(button_status, "Validating the Manual Dispatch Button Status")
                        if button_status:
                            self.log.info("inside click event")
                            self.sop.clickSOBoardActionButton(row["bolnumber"], "EDIDispatch")
                            bol_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                           "EDI Dispatch",
                                                                                           "BOL #", row["bolnumber"])
                            self.ts.mark(bol_identifier_result,
                                         "Validating the SO Board BOl Data Identifier Result for EDI Dispatch")
                            po_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                          "EDI Dispatch", "PO#", "")
                            self.ts.mark(po_identifier_result,
                                         "Validating the SO Board Data Identifier Result for EDI Dispatch")
                            ref_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                           "EDI Dispatch", "REF#", "")
                            self.ts.mark(ref_identifier_result,
                                         "Validating the SO Board Data Identifier Result for EDI Dispatch")
                            exp_date_identifier_result = self.sop.validateIdentifierDataSOBoard(row["bolnumber"],
                                                                                                "EDI Dispatch",
                                                                                                "Exp. Date")
                            self.ts.mark(exp_date_identifier_result,
                                         "Validating the SO Board Data Identifier Result for EDI Dispatch")

                            carrier_result = self.sop.validateBaseDataInSOBoardPopup(row["bolnumber"], "EDI Dispatch",
                                                                                     "Carrier Name",
                                                                                     row["selectedcarriername"])
                            self.ts.mark(carrier_result,
                                         "Validating the Carrier Name display in SO Board popup for EDI Dispatch.")

                            customer_result = self.sop.validateBaseDataInSOBoardPopup(row["bolnumber"], "EDI Dispatch",
                                                                                      "Customer Name",
                                                                                      row["customername"])
                            self.ts.mark(customer_result,
                                         "Validating the Customer Name display in SO Board popup for EDI Dispatch.")

                            validate_ship_add = True
                            validate_cons_add = True
                            origin_accessorial = []
                            destination_accessorial = []

                            if row["addressbook"].lower() == "yes":
                                self.log.info("Address Book value is Yes, validate from Address Book")
                                address_index = row["sno"]
                                for r_add in self.order_address_book:
                                    phone = r_add["contact_phone"].replace("(", "").replace(")", "")\
                                        .replace("-", "").replace(" ", "")
                                    if int(address_index) >= int(r_add["sno"]):
                                        if int(r_add["sno"]) == int(row["sno"]):
                                            if r_add["addresssection"] == "origin":
                                                self.log.info("Inside Address book origin condition")
                                                validate_ship_add = \
                                                    self.sop.validateAddressDataInSOBoard("origin",
                                                                                          r_add["company_name"],
                                                                                          r_add["addressline1"],
                                                                                          r_add["addressline2"],
                                                                                          r_add["address"],
                                                                                          r_add["contact_name"], phone)
                                                origin_accessorial = self.qop.getAccessorialsList(
                                                    r_add["origin_accessorials"], "origin")
                                            elif r_add["addresssection"] == "destination":
                                                self.log.info("Inside Address book Destination condiiton")
                                                validate_cons_add = \
                                                    self.sop.validateAddressDataInSOBoard("destination",
                                                                                          r_add["company_name"],
                                                                                          r_add["addressline1"],
                                                                                          r_add["addressline2"],
                                                                                          r_add["address"],
                                                                                          r_add["contact_name"],
                                                                                          phone)
                                                destination_accessorial = self.qop.getAccessorialsList(
                                                    r_add["destination_accessorials"], "destination")
                                            else:
                                                self.log.error("Incorrect address type passed")
                                                self.ts.mark(False,
                                                             "Incorrect address type passed to select address Book "
                                                             "method for EDI Dispatch")
                                    else:
                                        self.log.info("out of address Book selection index")
                                        break
                            else:
                                self.log.info("Inside address book no condition")
                                validate_ship_add = self.sop.validateAddressDataInSOBoard("origin",
                                                                                          row["oricompanyName"],
                                                                                          row["oriaddline1"],
                                                                                          row["oriaddline2"],
                                                                                          row["orizip"],
                                                                                          row["oricontactName"],
                                                                                          row["oriphone"])
                                validate_cons_add = self.sop.validateAddressDataInSOBoard("destination",
                                                                                          row["destcompanyname"],
                                                                                          row["destaddline1"],
                                                                                          row["destaddline2"],
                                                                                          row["destzip"],
                                                                                          row["destcontactname"],
                                                                                          row["destphone"])
                                origin_accessorial = self.qop.getAccessorialsList(row["originacc"], "origin")
                                destination_accessorial = self.qop.getAccessorialsList(row["destinationacc"],
                                                                                       "destination")
                            self.ts.mark(validate_ship_add, "Validating the Shipper Address display in SO Board Popup "
                                                            "for EDI Dispatch")
                            self.ts.mark(validate_cons_add, "Validating the Consignee Address display in SO Board "
                                                            "Popup for EDI Dispatch")

                            shipping_val = True
                            for si_row in order.sales_order_shipping:
                                self.log.info("row value si :" + str(si_row["sno"]) + " ,main row : " + str(row["sno"]))
                                if int(si_row["sno"]) <= int(row["sno"]):
                                    self.log.info("shipping row No. : " + str(si_row["sno"]))
                                    if int(si_row["sno"]) == int(row["sno"]):
                                        if si_row["productbook"].lower() == "no":
                                            self.log.info("Inside Product book no condition for EDI Dispatch")
                                            validate_shipping_data = \
                                                self.sop.validateShippingItemData(si_row["commodity"], si_row["weight"],
                                                                                  si_row["weightUnits"],
                                                                                  si_row["UnitType"],
                                                                                  si_row["unitcount"],
                                                                                  si_row["piece"],
                                                                                  si_row["length"],
                                                                                  si_row["width"],
                                                                                  si_row["height"],
                                                                                  si_row["Dim"],
                                                                                  si_row["className"],
                                                                                  si_row["hazmat"])
                                        else:
                                            self.log.info("Inside Product book Yes condition for EDI Dispatch")
                                            validate_shipping_data = self.sop.validateShippingItemData(
                                                si_row["newcommodity"], si_row["weight"],
                                                si_row["weightUnits"], si_row["UnitType"],
                                                si_row["unitcount"], si_row["piece"],
                                                si_row["length"], si_row["width"],
                                                si_row["height"], si_row["Dim"],
                                                si_row["className"], si_row["hazmat"])

                                        self.log.info("Shipping Data Value : " + str(validate_shipping_data))
                                        shipping_val = shipping_val and validate_shipping_data
                                    self.log.info("Reading Logs")
                                else:
                                    self.log.debug("Out side expected condition for EDI Dispatch")
                                    break

                            self.ts.mark(shipping_val,
                                         "Validating the Shipping Data in SO Board Popup for EDI Dispatch.")

                            complete_acc_list = origin_accessorial + destination_accessorial
                            self.log.info("Complete accessorial List : " + str(complete_acc_list))

                            acc_breakup_value = self.car.accessorialBreakupValidationList(complete_acc_list)
                            self.log.info("Accessorial breakup Value : " + str(acc_breakup_value))

                            break_up_value_list = self.sop.validateAndAddAccessorial(acc_breakup_value,
                                                                                     row["carrierbreakup"])
                            self.log.info("Final List : " + str(break_up_value_list))

                            accessorial_result = self.sop.validateAccessorialList(break_up_value_list)
                            self.ts.mark(accessorial_result, "Validating the Display Accessorial in SO Board Popup for "
                                                             "EDI Dispatch.")

                            pu_number = "pu" + row["bolnumber"]

                            # Once Bug will fix than will enable this line of Code
                            # if len(complete_acc_list)>0:
                            #     #Handle through popup
                            #     acclist = "yes"
                            #
                            #     # validateDispatch = self.sop.ediDispatchAccessorialConfirmationPopup()
                            # else:
                            #     puNumber = "pu" + row["bolnumber"]
                            #     acclist = "no"
                            #     # validateDispatch = self.sop.clickOnActionButtonOnSOBoardPopup("EDI Dispatch",
                            #                                                                  puNumber)
                            #
                            # validateDispatch = self.sop.clickOnActionButtonOnSOBoardPopup("EDI Dispatch", puNumber,
                            #                                                               acc=acclist)

                            validate_dispatch = self.sop.clickOnActionButtonOnSOBoardPopup("EDI Dispatch", pu_number)

                            self.ts.mark(validate_dispatch, "Validating the Dispatch Action Button in SO Board popup "
                                                            "for EDI Dispatch")

                            status_result = self.sop.validateColumnValue("status", "Auto Dispatch", row["bolnumber"])
                            self.ts.mark(status_result, "Validating the updated Status in SO Board for EDI Dispatch.")

                            # pickup number is not displayed in so board - bug created
                            punumber_result = self.sop.validateColumnValue("pickupNumber", pu_number, row["bolnumber"])
                            self.ts.mark(punumber_result,
                                         "Validating the updated Pickup Number while doing EDI Dispatch.")

                            self.sop.navigateToSOPage(row["bolnumber"])
                            self.sdp.soPageLoadWait(25)
                            # Commented due to bug
                            # if not puNumber == "":
                            #     # pickup number is not displayed in so page - bug created
                            #     validateProIdentifier = self.sdp.getIdentifierValue("P/U #", puNumber)
                            #     # validateProIdentifier = self.sdp.getIdentifierValue("P/U #", "")
                            #     self.ts.mark(validateProIdentifier, "Validating the Added Pickup Number in SO Details"
                            #                                         " Page after EDI Dispatch Order.")

                            # Status Section Validation
                            validate_process_status = self.sdp.validateStatusSectionBasedOnPassedValue("process",
                                                                                                       "Auto Dispatch")
                            self.ts.mark(validate_process_status,
                                         "Validating the Process status value in Status section after EDI Dispatch")

                            validate_invoice_status = self.sdp.validateStatusSectionBasedOnPassedValue("invoice",
                                                                                                       "Pending")
                            self.ts.mark(validate_invoice_status,
                                         "Validating the Invoice status in Status Section after EDI Dispatch")

                            validate_vendor_status = self.sdp.validateStatusSectionBasedOnPassedValue("Vendorbill",
                                                                                                      "None")
                            self.ts.mark(validate_vendor_status,
                                         "Validating the vendor Bill status in Status section after EDI Dispatch")

                            validate_edi_status = self.sdp.validateStatusSectionBasedOnPassedValue("edi",
                                                                                                   "waiting for 997")
                            self.ts.mark(validate_edi_status,
                                         "Validating the EDI Status in Status Section after EDI Dispatch")

                            validate_tracking_status = self.sdp.validateStatusSectionBasedOnPassedValue("tracking",
                                                                                                        "Dispatched")
                            self.ts.mark(validate_tracking_status,
                                         "Validating the Tracking status in Status Section after EDI Dispatch")

                            self.sdp.soOrderBoardButton()
                            self.sop.soBoardLoadWait()

        if not edi_dispatch:
            self.log.error("None of the passed BOL Has Pending Status To verify EDI Dispatch Flow from SO Board, "
                           "hence Failing the case.")
            self.ts.mark(False, "EDI Dispatch from SO Board Failed as not found any SO with proper status")
        self.ts.mark_final("test_validate_edi_dispatch_from_so_board", True, "Validating EDI Dispatch Flow in SO Board.")
