import utilities.custom_logger as cl

from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.ordersummarysodetails_page import OrderSummarySODetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorderfinancialdetails_page import SalesorderFinancialDetailsPage
from pages.LTL.salesorderboard.salesorderroutedetails_page import SalesorderRouteDetailsPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class ValidateSODetailsTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).osp = OrderSummarySODetailsPage(self.driver, self.log)
        type(self).srf = SalesorderFinancialDetailsPage(self.driver, self.log)
        type(self).srd = SalesorderRouteDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[0:2]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    @pytest.mark.smoke
    @pytest.mark.slowtest
    def test_validate_created_sales_order_page(self):
        self.log.info("Inside test_validate_created_sales_order_page Method")

        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        for order in orders:
            row = order.sales_order_data

            navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
            self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
            if navigation_result:
                so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                self.ts.mark(so_validation_result, "Validate in SO Details page passed BOL should open and display.")
                validate_nw_container_text = self.sdp.ValidateNetworkSection()
                self.ts.mark(validate_nw_container_text, "Validating Network container Text Value")
                validate_customer_name = self.sdp.validateNWSectionBasedOnPassedValue("customer", row["customername"])
                self.ts.mark(validate_customer_name, "Validating the Customer Name in SO Page")
                validate_service_type = self.sdp.validateNWSectionBasedOnPassedValue("servicetype", "LTL")
                self.ts.mark(validate_service_type, "Validating the Service Type in SO Details Page")
                validate_application_name = self.sdp.validateNWSectionBasedOnPassedValue("generatedon")
                self.ts.mark(validate_application_name, "Validating the application name display in SO Details Page")
                validate_contact_name = self.sdp.validateNWSectionBasedOnPassedValue("customercontact",
                                                                                     row["customercontactname"])
                self.ts.mark(validate_contact_name, "Validating the Contact Name display in SO Details Page")
                validate_rating_method = self.sdp.validateNWSectionBasedOnPassedValue("ratingmethod", "Standard Tariff")
                self.ts.mark(validate_rating_method, "Validating the Rating method in SO Details Page")
                validate_quote_request = self.sdp.validateNWSectionBasedOnPassedValue("quoterequest", row["quoteid"])
                self.ts.mark(validate_quote_request, "Validating the Quote request Id in SO Details Page")

                # SO Summary Section :
                validate_summary_container_text = self.osp.validateSalesOrderSummarySection()
                self.ts.mark(validate_summary_container_text, "Validating Sales order Summary Header Text Value")
                validate_carrier_name = self.osp.validateSalesOrderSummaryData("carrier", row["selectedcarriername"])
                self.ts.mark(validate_carrier_name, "Validating the carrier Name in SO Details Page")

                for si_row in order.sales_order_shipping:
                    self.log.info("row value si :" + str(si_row["sno"]) + " ,main row : " + str(row["sno"]))
                    if int(si_row["sno"]) <= int(row["sno"]):
                        if int(si_row["sno"]) == int(row["sno"]):
                            if si_row["productbook"].lower() == "no":
                                index_value = self.osp.getOrderSummaryIndexValue(si_row["commodity"])
                                validate_commodity = self.osp.validateSalesOrderSummaryData("commodity",
                                                                                            si_row["commodity"],
                                                                                            index_value)
                                self.ts.mark(validate_commodity, "Validating the Commodity Name in SO Details Page")

                                validate_shipping_result = self.srf.validateShippingItems(si_row["commodity"],
                                                                                          si_row["nfmc"],
                                                                                          si_row["className"],
                                                                                          si_row["weight"],
                                                                                          si_row["weightUnits"],
                                                                                          si_row["UnitType"],
                                                                                          si_row["unitcount"],
                                                                                          si_row["piece"],
                                                                                          si_row["length"],
                                                                                          si_row["width"],
                                                                                          si_row["height"],
                                                                                          si_row["Dim"],
                                                                                          si_row["hazmat"],
                                                                                          si_row["prefix"],
                                                                                          si_row["HazmatCode"],
                                                                                          si_row["HazmatGroup"],
                                                                                          si_row["HazmatClass"])

                                self.ts.mark(validate_shipping_result,
                                             "Validated shipping items in SO Page under Financial section")
                            else:
                                index_value = self.osp.getOrderSummaryIndexValue(si_row["newcommodity"])
                                validate_commodity = self.osp.validateSalesOrderSummaryData("commodity",
                                                                                            si_row["newcommodity"],
                                                                                            index_value)
                                self.ts.mark(validate_commodity, "Validating the Commodity Name in SO Details Page")

                                validate_shipping_result = self.srf.validateShippingItems(si_row["newcommodity"],
                                                                                          si_row["nfmc"],
                                                                                          si_row["className"],
                                                                                          si_row["weight"],
                                                                                          si_row["weightUnits"],
                                                                                          si_row["UnitType"],
                                                                                          si_row["unitcount"],
                                                                                          si_row["piece"],
                                                                                          si_row["length"],
                                                                                          si_row["width"],
                                                                                          si_row["height"],
                                                                                          si_row["Dim"],
                                                                                          si_row["hazmat"],
                                                                                          si_row["prefix"],
                                                                                          si_row["HazmatCode"],
                                                                                          si_row["HazmatGroup"],
                                                                                          si_row["HazmatClass"])
                                self.ts.mark(validate_shipping_result,
                                             "Validated shipping items in SO Page under Financial section")

                            # Commented due to bug LTL1-3177
                            if row["selectcarrierrate"].lower() == "yes":

                                validateShipmentValue = self.osp.validateSalesOrderSummaryData("shipmentvalue",
                                                                                               row["shipmentvalue"],
                                                                                               index_value)
                                self.ts.mark(validateShipmentValue,
                                             "Validating the Shipment Value in SO Details Page")
                            else:
                                validateShipmentValue = self.osp.validateSalesOrderSummaryData("shipmentvalue",
                                                                                               "0",
                                                                                               index_value)
                                self.ts.mark(validateShipmentValue,
                                             "Validating the Shipment Value in SO Details Page")
                            self.ts.mark(validateShipmentValue, "Validating the Shipment Value in SO Details Page")

                            # Unit Count Validation
                            validate_unit_value = self.osp.validateSalesOrderSummaryData("unitcount",
                                                                                         si_row["unitcount"],
                                                                                         index_value)
                            self.ts.mark(validate_unit_value, "Validating the Unit Value in SO Details Page")

                            validate_weight_value = self.osp.validateWeightSOSummaryData(si_row["weight"],
                                                                                         si_row["weightUnits"],
                                                                                         index_value)
                            self.ts.mark(validate_weight_value, "Validating the Weight Value in SO Details Page")

                    else:
                        self.log.debug(
                            "Index value out of range, si row id: " + str(si_row["sno"]) + " and row index : " + row[
                                "sno"])
                        break

                # Notes validation
                user_note_result = self.snd.validateSavedNotesInSOPage(row["notetype"], row["notes"])
                self.ts.mark(user_note_result, "Verify the Notes(User) in the SO page for the BOL when created.")

                # Route Details Section Validation :
                self.log.info("Route Details section validation")
                validate_rd_container_text = self.srd.ValidateRouteDetailsSection()
                self.ts.mark(validate_rd_container_text,
                             "Validating Route Details container Text Value in SO Details Page")

                validate_transit_days = self.srd.validateRDSectionBasedOnPassedValue(row["transitdays"], "transitdays")
                self.ts.mark(validate_transit_days, "Validating Transit Days Value in SO Details Page")

                if row["addressbook"].lower() == "yes":
                    self.log.info("Address Book value is Yes, Only address Book Validation")
                    address_index = row["sno"]
                    for r_add in self.order_address_book:
                        if int(address_index) >= int(r_add["sno"]):
                            if int(r_add["sno"]) == int(row["sno"]):
                                if r_add["addresssection"] == "origin":
                                    validate_origin_address = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add["addressline1"] +
                                                                                     ", " + r_add["address"],
                                                                                     "completeaddress",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_origin_address,
                                                 "Validated Origin address in SO Details Page under Route Detail "
                                                 "Section from address book")
                                    validate_origin_company_name = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add['company_name'],
                                                                                     "companyname",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_origin_company_name,
                                                 "Validated Origin company name in SO Details Page under Route Detail "
                                                 "Section from address book")
                                    validate_origin_phone_number = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add['contact_phone'],
                                                                                     "phonenumber",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_origin_phone_number,
                                                 "Validated Origin phone number in SO Details Page under Route Detail "
                                                 "Section from address book from address book")
                                    validate_origin_appointment = \
                                        self.srd.validateRDSectionBasedOnPassedValue(row["originpickup"],
                                                                                     "appointmentdate",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_origin_appointment,
                                                 "Validated Origin appointment Date in SO Details Page under Route "
                                                 "Detail Section from address book")

                                    validate_origin_timings = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add['readytime'] + " - " +
                                                                                     r_add['closetime'],
                                                                                     "appointmenttimings",
                                                                                     r_add["addresssection"])

                                    self.ts.mark(validate_origin_timings,
                                                 "Validated Origin Appointment Timings in SO Details Page under Route "
                                                 "Detail Section from address book")
                                    # Notes validation
                                    pickup_remark_note_result = self.snd.validateSavedNotesInSOPage("Pick up Remarks",
                                                                                                    r_add['remarks'])
                                    self.ts.mark(pickup_remark_note_result,
                                                 "Verify the Notes(Pick up Remarks) from address book in the SO page "
                                                 "for the BOL when created.")

                                elif r_add["addresssection"] == "destination":

                                    validate_dest_address = self.srd.validateRDSectionBasedOnPassedValue(
                                        r_add["addressline1"] + ", " +
                                        r_add["address"], "completeaddress", r_add["addresssection"])
                                    self.ts.mark(validate_dest_address,
                                                 "Validated Dest address in SO Details Page under Route Detail Section "
                                                 "from address book")
                                    validate_dest_company_name = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add['company_name'],
                                                                                     "companyname",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_dest_company_name,
                                                 "Validated Dest company name in SO Details Page under Route Detail "
                                                 "Section from address book")
                                    validate_dest_phone_number = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add['contact_phone'],
                                                                                     "phonenumber",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_dest_phone_number,
                                                 "Validated Dest phone number in SO Details "
                                                 "Page under Route Detail Section from address book")
                                    validate_dest_appointment = \
                                        self.srd.validateRDSectionBasedOnPassedValue(row["etadelivery"],
                                                                                     "appointmentdate",
                                                                                     r_add["addresssection"])
                                    self.ts.mark(validate_dest_appointment,
                                                 "Validated Dest appointment Date in SO "
                                                 "Details Page under Route Detail Section from address book")

                                    validate_dest_timings = \
                                        self.srd.validateRDSectionBasedOnPassedValue(r_add['readytime'] + " - " +
                                                                                     r_add['closetime'],
                                                                                     "appointmenttimings",
                                                                                     r_add["addresssection"])

                                    self.ts.mark(validate_dest_timings,
                                                 "Validated Dest appointment Timings in SO Details Page under Route "
                                                 "Detail Section from address book")
                                    # Notes validation
                                    delivery_remark_note_result = \
                                        self.snd.validateSavedNotesInSOPage("Delivery Remarks", r_add['remarks'])

                                    self.ts.mark(delivery_remark_note_result,
                                                 "Verify the Notes(Delivery Remarks) from address book in the SO page "
                                                 "for the BOL when created.")

                                else:
                                    self.log.error("Incorrect address type passed")
                                    self.ts.mark(False,
                                                 "Incorrect address type passed to select address Book method")
                        else:
                            self.log.info("out of address Book selection index")
                            break
                else:
                    validate_origin_address = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["oriaddline1"] + ", " + row["orizip"],
                                                                     "completeaddress", "origin")
                    self.ts.mark(validate_origin_address,
                                 "Validated Origin address in SO Details Page under Route Detail Section")
                    validate_origin_company_name = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["oricompanyName"], "companyname", "origin")
                    self.ts.mark(validate_origin_company_name,
                                 "Validated Origin company name in SO Details Page under Route Detail Section")
                    validate_origin_phone_number = self.srd.validateRDSectionBasedOnPassedValue(row["oriphone"],
                                                                                                "phonenumber",
                                                                                                "origin")
                    self.ts.mark(validate_origin_phone_number,
                                 "Validated Origin phone number in SO Details Page under Route Detail Section")
                    validate_origin_appointment = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["originpickup"], "appointmentdate", "origin")
                    self.ts.mark(validate_origin_appointment,
                                 "Validated Origin appointment date in SO Details Page under Route Detail Section")
                    validate_origin_timings = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["orireadytime"] + " - " + row["oriclosetime"],
                                                                     "appointmenttimings", "origin")
                    self.ts.mark(validate_origin_timings,
                                 "Validated Origin Appointment Timings in SO Details Page under Route Detail Section")

                    validate_dest_address = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["destaddline1"] + ", " + row["destzip"],
                                                                     "completeaddress", "destination")
                    self.ts.mark(validate_dest_address,
                                 "Validated Dest address in SO Details Page under Route Detail Section")
                    validate_dest_company_name = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["destcompanyname"], "companyname",
                                                                     "destination")
                    self.ts.mark(validate_dest_company_name,
                                 "Validated Dest company name in SO Details Page under Route Detail Section")
                    validate_dest_phone_number = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["destphone"], "phonenumber", "destination")
                    self.ts.mark(validate_dest_phone_number,
                                 "Validated Dest phone number in SO Details Page under Route "
                                 "Detail Section")
                    validate_dest_appointment = self.srd.validateRDSectionBasedOnPassedValue(row["etadelivery"],
                                                                                             "appointmentdate",
                                                                                             "destination")
                    self.ts.mark(validate_dest_appointment,
                                 "Validated Dest appointment date in SO Details Page under Route Detail Section")
                    validate_dest_timings = \
                        self.srd.validateRDSectionBasedOnPassedValue(row["destreadytime"] + " - " +
                                                                     row["destclosetime"], "appointmenttimings",
                                                                     "destination")

                    self.ts.mark(validate_dest_timings,
                                 "Validated Dest appointment Timings in SO Details Page under Route Detail Section")
                    # Notes validation
                    pickup_remark_note_result = self.snd.validateSavedNotesInSOPage("Pick up Remarks",
                                                                                    row['pickupremarks'])
                    self.ts.mark(pickup_remark_note_result,
                                 "Verify the Notes(Pick up Remarks) in the SO page for the BOL when created.")
                    delivery_remark_note_result = self.snd.validateSavedNotesInSOPage("Delivery Remarks",
                                                                                      row['deliveryremarks'])
                    self.ts.mark(delivery_remark_note_result,
                                 "Verify the Notes(Delivery Remarks) in the SO page for the BOL when created.")

                # Status Section Validation
                validate_process_status = self.sdp.validateStatusSectionBasedOnPassedValue("process", "Pending")
                self.ts.mark(validate_process_status, "Validating the Process status value in Status section")
                validate_invoice_status = self.sdp.validateStatusSectionBasedOnPassedValue("invoice", "Pending")
                self.ts.mark(validate_invoice_status, "Validating the Invoice status in Status Section")
                validate_vendor_status = self.sdp.validateStatusSectionBasedOnPassedValue("Vendorbill", "None")
                self.ts.mark(validate_vendor_status, "Validating the vendor Bill status in Status section")
                validate_edi_status = self.sdp.validateStatusSectionBasedOnPassedValue("edi", "")
                self.ts.mark(validate_edi_status, "Validating the EDI Status in Status Section")
                validate_tracking_status = self.sdp.validateStatusSectionBasedOnPassedValue("tracking", "Pending")
                self.ts.mark(validate_tracking_status, "Validating the Tracking status in Status Section")

                # Validate Identifier Section
                validate_bol_identifier = self.sdp.getIdentifierValue("BOL #", row["bolnumber"])
                self.ts.mark(validate_bol_identifier, "Validating the BOL Identifier in SO Page")
                validate_quote_identifier = self.sdp.getIdentifierValue("Quote #", row["quoteid"])
                self.ts.mark(validate_quote_identifier, "Validating the Quote Identifier in SO Page")

                # Validate Financial Section
                # Commented due to bug LTL1-3177 - bug got fixed
                validate_carrier_breakup_result = self.srf.validateCarrierBreakup(row["carrierbreakup"])
                self.ts.mark(validate_carrier_breakup_result, "Validated carrier breakup in Financial Section")

                validate_total_revenue_result = self.srf.validateTotalValue(row["totalrevenue"], "revenue")
                self.ts.mark(validate_total_revenue_result, "Validated Total Revenue in Financial Section")

                validate_default_document_section_result = \
                    self.snd.validateDefaultDocumentSection(row["selectcarrierrate"])

                self.ts.mark(validate_default_document_section_result,
                             "Validated Default Document Section of the BOL.")
                self.sdp.soOrderBoardButton()

            else:
                self.log.error(
                    "After apply filter BOL not found in Sales Order Board for BOL Number : " + str(row["bolnumber"]))
        self.ts.mark_final("test_validate_created_sales_order_page", True, "Validating the SO Page for Created SO Data")
