import utilities.custom_logger as cl
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderroutedetails_page import SalesorderRouteDetailsPage
import unittest2
import pytest
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "setUp")
class SalesOrderPageOrderStatusUpdateFlowTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).srd = SalesorderRouteDetailsPage(self.driver, self.log)
        type(self).order_data = read_csv_data("salesOrderData.csv")[3:5]
        type(self).order_shipping = read_csv_data("salesorder_shipping.csv")
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        type(self).ts = ReportTestStatus(self.driver, self.log)
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        self.__login()
        type(self).create_orders_result = sales_order_creator.create_orders(self.order_data, self.order_address_book,
                                                                            self.order_shipping)
        self.__navigate_to_sales_order_board()

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    def __navigate_to_sales_order_board(self):
        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

    @pytest.mark.slowtest
    def test_validate_dispatched_order_status_update_in_sales_order_page(self):
        self.log.info("Inside test_validate_dispatched_order_status_update_in_sales_order_page Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        dispatched_status_flag = False
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result and not dispatched_status_flag:
                    navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                    self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                    if navigation_result:

                        so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                        self.ts.mark(so_validation_result,
                                     "Validate in SO Details page passed BOL should open and display.")
                        dispatched_status_flag = True
                        validated_default_actual_date_messages = self.srd.verifyActualPickupAndDeliveryDateValidations()
                        self.ts.mark(validated_default_actual_date_messages,
                                     "Verified all the scenarios for actual pickup and delivery date selection")
                        validate_pending_status_result = self.sdp.validateStatusesSectionOnUpdate("pending")
                        self.ts.mark(validate_pending_status_result, "Validated Pending status")
                        validate_delivered_status_result = self.sdp.validateStatusesSectionOnUpdate("delivered")
                        self.ts.mark(validate_delivered_status_result, "Validated Delivered status after dispatched")

                        get_pickup_date = self.srd.get_actual_pickup_date()
                        self.log.info("Pickup Date Value : " + str(get_pickup_date))

                        create_new_order_button_status_result = self.sdp.getButtonEnabled("createneworder")
                        self.ts.mark(create_new_order_button_status_result,
                                     "Validated Create New Order Button Status when BOL status is delivered after "
                                     "dispatched.")
                        self.sdp.soOrderBoardButton()
                        self.sop.soBoardLoadWait()
                        self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                        requested_pick_date_cmp = self.sop.getCellData("pickupDate", row["bolnumber"])

                        if get_pickup_date in requested_pick_date_cmp:
                            self.ts.mark(True, "Validating the Updated Actual Pickup Date Value in SO Board")
                        else:
                            self.ts.mark(False, "Validating the Updated Actual Pickup Date Value in SO Board")
            else:
                self.log.error("After apply filter BOL not found in Sales Order Board for BOL Number : " +
                               str(row["bolnumber"]))

        if not dispatched_status_flag:
            self.log.error("None of the passed BOL Has proper Status To verify Dispatched Flow with SO status options "
                           "hence Failing the case.")
            self.ts.mark(False, "verify Dispatched Flow with SO status option from SO Details Page Failed as not found "
                                "any SO with proper status")

        self.ts.mark_final("test_validate_dispatched_order_status_update_in_sales_order_page", True,
                           "Validated dispatched order status update in sales order page")

    @pytest.mark.slowtest
    def test_validate_finalized_order_status_update_in_sales_order_page(self):
        self.log.info("Inside test_validate_finalized_order_status_update_in_sales_order_page Method")
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]
        finalize_status_flag = False
        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                value_compare_result = self.sop.validateColumnValue("status", "Pending", row["bolnumber"])
                if value_compare_result and not finalize_status_flag:
                    finalize_status_flag = True
                    self.sop.clickSOBoardActionButton(row["bolnumber"], "finalize")
                    pro_num = "pro" + row["bolnumber"]
                    self.sop.clickOnActionButtonOnSOBoardPopup("finalize", proNumber=pro_num, pickupDate="1")
                    filter_result = self.sop.clickAndValidateShipmentStatusFilter("In Transit")
                    self.ts.mark(filter_result,
                                 "Shipment Status Filter InTransit is clicked and the same is in selected state.")

                    # Validation of BOL not present in other filters
                    self.sop.clickAndValidateShipmentStatusFilter("pending")
                    self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                    bol_status_in_pending_filter = self.sop.validateNoRowsToShow()
                    self.ts.mark(bol_status_in_pending_filter,
                                 "Validated InTransit Status BOL is not present in Pending Filter.")
                    self.sop.clickAndValidateShipmentStatusFilter("Dispatched")
                    self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                    bol_status_in_dispatched_filter = self.sop.validateNoRowsToShow()
                    self.ts.mark(bol_status_in_dispatched_filter,
                                 "Validated InTransit Status BOL is not present in Dispatched Filter.")
                    self.sop.clickAndValidateShipmentStatusFilter("delivered")
                    self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                    bol_status_in_delivered_filter = self.sop.validateNoRowsToShow()
                    self.ts.mark(bol_status_in_delivered_filter,
                                 "Validated InTransit Status BOL is not present in Delivered Filter.")
                    self.sop.clickAndValidateShipmentStatusFilter("intransit")

                    self.sop.navigateToSOPage(row["bolnumber"])
                    self.sdp.soPageLoadWait(25)
                    validate_delivered_status_result = self.sdp.validateStatusesSectionOnUpdate("delivered")
                    self.ts.mark(validate_delivered_status_result, "Validated Delivered status after finalize")
                    self.sdp.soOrderBoardButton()
                    self.sop.soBoardLoadWait()
                    filter_result = self.sop.clickAndValidateShipmentStatusFilter("Delivered")
                    self.ts.mark(filter_result,
                                 "Shipment Status Filter Delivered is clicked and and the same is in selected state.")
                    bol_status_in_delivered_filter = self.sop.applyFilterBasedOnColumnName(
                        "billOfLading", row["bolnumber"])
                    self.ts.mark(bol_status_in_delivered_filter, "BOL is present in Delivered Filter.")
                    # Validation of BOL not present in other filters
                    self.sop.clickAndValidateShipmentStatusFilter("pending")
                    self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                    bol_status_in_pending_filter = self.sop.validateNoRowsToShow()
                    self.ts.mark(bol_status_in_pending_filter,
                                 "Validated Delivered Status BOL is not present in Pending Filter.")
                    self.sop.clickAndValidateShipmentStatusFilter("Dispatched")
                    self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                    bol_status_in_dispatched_filter = self.sop.validateNoRowsToShow()
                    self.ts.mark(bol_status_in_dispatched_filter,
                                 "Validated Delivered Status BOL is not present in Dispatched Filter.")
                    self.sop.clickAndValidateShipmentStatusFilter("intransit")
                    self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
                    bol_status_in_transit_filter = self.sop.validateNoRowsToShow()
                    self.ts.mark(bol_status_in_transit_filter,
                                 "Validated Delivered Status BOL is not present in InTransit Filter.")
                    self.sop.clickAndValidateShipmentStatusFilter("allorders")
            else:
                self.log.error(
                    "After apply filter BOL not found in Sales Order Board for BOL Number : " + str(row["bolnumber"]))

        if not finalize_status_flag:
            self.log.error("None of the passed BOL Has proper Status To verify finalizeStatus Flow with SO status "
                           "options hence Failing the case.")
            self.ts.mark(False, "verify finalizeStatus Flow with SO status option from SO Details Page Failed as not "
                                "found any SO with proper status")

        self.ts.mark_final("test_validate_finalized_order_status_update_in_sales_order_page", True,
                           "Validated finalized order status update in sales order page")
