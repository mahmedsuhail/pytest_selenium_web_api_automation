import pytest
import unittest2

import utilities.custom_logger as cl
from pages.LTL.createquote.carriers_page import CarriersPage
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.createquote.routedetails_page import RouteDetailsPage
from pages.LTL.createquote.shippingItem_page import ShippingItemsPage
from pages.LTL.salesorderboard.ordersummarysodetails_page import OrderSummarySODetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorderfinancialdetails_page import SalesorderFinancialDetailsPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
from pages.LTL.salesorderboard.salesorderroutedetails_page import SalesorderRouteDetailsPage
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class EditSalesOrderTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    login_data = read_csv_data("login_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):

        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).lop = TMSLogoutPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).srd = SalesorderRouteDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).osp = OrderSummarySODetailsPage(self.driver, self.log)
        type(self).srf = SalesorderFinancialDetailsPage(self.driver, self.log)
        type(self).rdp = RouteDetailsPage(self.driver, self.log)
        type(self).sip = ShippingItemsPage(self.driver, self.log)
        type(self).car = CarriersPage(self.driver, self.log)
        type(self).util = Util(self.log)
        type(self).order_data = read_csv_data("salesOrderData.csv")[0:1]
        type(self).order_shipping = read_csv_data("salesorder_shipping.csv")
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        self.__login()

        type(self).create_orders_result = sales_order_creator.create_orders(self.order_data, self.order_address_book,
                                                                            self.order_shipping)

    def test_validate_edit_sales_order(self):
        self.log.info("Inside test_validate_edit_sales_order method")
        update_sales_order_status_flag = False
        update_button_enabeled_status_flag = False
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        for order in orders:
            row = order.sales_order_data
            self.log.info("Printing Row Data : " + str(row))
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:

                # Edit sales order details and validate in SO Page
                navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                self.sdp.soPageLoadWait(15)
                if navigation_result and not update_sales_order_status_flag:
                    update_sales_order_status_flag = True
                    quote_page_displayed_result = False
                    validated_update_button_status_result = self.sdp.verifyEditButtonEnabledAndClick()
                    self.ts.mark(validated_update_button_status_result,
                                 "Verified update order button is enabeled and clicked")

                    if validated_update_button_status_result:
                        quote_page_displayed_result = self.qop.createQuotePageLoad()
                        self.ts.mark(quote_page_displayed_result, "verified quote page is displayed")
                    if quote_page_displayed_result:
                        updated_piece_value = "4"
                        updated_item_row_index_commodity = ""
                        updated_item_row_index = ""
                        if row["addressbook"].lower() == "yes":
                            self.log.info("Edit sales order : Address Book value is Yes, Only address Book Validation")
                            address_index = row["sno"]
                            self.log.info("address_index : " + str(address_index))
                            origin_accessorial_list = ""
                            destination_accessorial_list = ""
                            for r_add in self.order_address_book:
                                if int(address_index) >= int(r_add["sno"]):
                                    if int(r_add["sno"]) == int(row["sno"]):
                                        if r_add["origin_accessorials"] != "":
                                            origin_accessorial_list = r_add["origin_accessorials"]
                                        if r_add["destination_accessorials"] != "":
                                            destination_accessorial_list = r_add["destination_accessorials"]

                                        verified_city_name = self.rdp. \
                                            verifySelectedAddressInTextField(r_add["address"],
                                                                             r_add["addresssection"])
                                        self.ts.mark(verified_city_name, "Verified " + str(
                                            r_add["addresssection"]) + " city name selected same as given")

                                        verified_site_type = self.rdp.verifySiteTypeSelected(r_add["site_type"],
                                                                                             r_add["addresssection"])
                                        self.ts.mark(verified_site_type, "Verified " + str(
                                            r_add["addresssection"]) + " site type selected same as given")

                                        validated_address_container_result = self.rdp.validateAddressContainerFields(
                                            r_add["addresssection"],
                                            r_add['city_name'],
                                            r_add['zip_code'],
                                            r_add['readytime'],
                                            r_add['closetime'],
                                            r_add['company_name'],
                                            r_add['contact_name'],
                                            r_add['contact_phone'],
                                            r_add['addressline1'],
                                            r_add['addressline2'])

                                        self.ts.mark(validated_address_container_result, "Verified " +
                                                     str(r_add["addresssection"]) +
                                                     " Address Container fields after selecting from address book")

                                        # Notes validation commented due to bug
                                        user_note_result = self.qop.validateSavedNotesInQuotePage(row["notetype"],
                                                                                                  row["notes"])
                                        self.ts.mark(user_note_result,
                                                     "Edit sales order : verify the Notes(User) in the create quote "
                                                     "page.")

                                        if r_add["addresssection"].lower() == "origin":
                                            pickup_remark_note_result = self.qop.validateSavedNotesInQuotePage(
                                                "Pick up Remarks", r_add['remarks'])
                                            self.ts.mark(pickup_remark_note_result,
                                                         "Edit sales order : Verify the Notes(Pick up Remarks) in the "
                                                         "create quote page.")
                                        elif r_add["addresssection"].lower() == "destination":
                                            delivery_remark_note_result = self.qop.validateSavedNotesInQuotePage(
                                                "Delivery Remarks", r_add['remarks'])
                                            self.ts.mark(delivery_remark_note_result,
                                                         "Edit sales order : Verify the Notes(Delivery Remarks) in "
                                                         "the create quote page.")

                            origin_accessorials_list = self.qop.getAccessorialsList(origin_accessorial_list, "origin")
                            dest_accessorials_list = self.qop.getAccessorialsList(destination_accessorial_list,
                                                                                  "destination")

                            final_accessorials_selected = self.qop.selectedAccessorials("True")
                            self.log.info("final_accessorials_selected : " + str(final_accessorials_selected))
                            origin_dest_accessorials = list(
                                set().union(origin_accessorials_list, dest_accessorials_list))
                            self.log.info("Passed accessorials list" + str(origin_dest_accessorials))
                            accessorialselected_result = self.rdp.validateAccessorials(origin_dest_accessorials,
                                                                                       final_accessorials_selected)
                            self.log.info("accessorialselected_result : " + str(accessorialselected_result))
                            self.ts.mark(accessorialselected_result,
                                         "Edit sales order : Accessorials selected for origin and destination is "
                                         "matched and displayed correctly")
                        else:
                            verified_origin_city_name = self.rdp.verifySelectedAddressInTextField(row["orizip"],
                                                                                                  "origin")
                            self.ts.mark(verified_origin_city_name,
                                         "Edit sales order : Verified origin city name selected is same as given")
                            verified_dest_city_name = self.rdp.verifySelectedAddressInTextField(row["destzip"],
                                                                                                "destination")
                            self.ts.mark(verified_dest_city_name,
                                         "Edit sales order : Verified destination city name selected is same as given")
                            verified_origin_site_type = self.rdp.verifySiteTypeSelected(row["originsitetype"], "origin")
                            self.ts.mark(verified_origin_site_type,
                                         "Edit sales order : Verified Origin Site Type is selected same as given")
                            verified_dest_site_type = self.rdp.verifySiteTypeSelected(row["destinationsitetype"],
                                                                                      "destination")
                            self.ts.mark(verified_dest_site_type,
                                         "Edit sales order : Verified Destination Site Type is selected same as given")

                            origin_accessorials_list = self.qop.getAccessorialsList(row["originacc"], "origin")
                            dest_accessorials_list = self.qop.getAccessorialsList(row["destinationacc"], "destination")

                            final_accessorials_selected = self.qop.selectedAccessorials("True")
                            self.log.info("final_accessorials_selected : " + str(final_accessorials_selected))
                            origin_dest_accessorials = list(
                                set().union(origin_accessorials_list, dest_accessorials_list))
                            self.log.info("Passed accessorials list" + str(origin_dest_accessorials))
                            accessorialselected_result = self.rdp.validateAccessorials(origin_dest_accessorials,
                                                                                       final_accessorials_selected)
                            self.log.info("accessorialselected_result : " + str(accessorialselected_result))
                            self.ts.mark(accessorialselected_result,
                                         "Edit sales order : Accessorials selected for origin and destination is "
                                         "matched and displayed correctly")

                            # Preparing data as per requirements for validation purpose
                            orizip_code_array = row['orizip'].split(",")
                            orizip_code = orizip_code_array[2].replace(" ", "")
                            oricity_name = orizip_code_array[0] + "," + orizip_code_array[1]

                            destzip_code_array = row['destzip'].split(",")
                            destzip_code = destzip_code_array[2].replace(" ", "")
                            destcity_name = destzip_code_array[0] + "," + destzip_code_array[1]

                            validated_origin_address_container_result = self.rdp.validateAddressContainerFields(
                                "origin",
                                oricity_name,
                                orizip_code,
                                row['orireadytime'],
                                row['oriclosetime'],
                                row['oricompanyName'],
                                row['oricontactName'],
                                row['oriphone'],
                                row['oriaddline1'],
                                row['oriaddline2'])
                            self.ts.mark(validated_origin_address_container_result,
                                         "Edit sales order : Verified Origin Address Container fields are displayed "
                                         "same as given in create quote page.")
                            validated_dest_address_container_result = self.rdp.validateAddressContainerFields(
                                "destination",
                                destcity_name,
                                destzip_code,
                                row['destreadytime'],
                                row['destclosetime'],
                                row['destcompanyname'],
                                row['destcontactname'],
                                row['destphone'],
                                row['destaddline1'],
                                row['destaddline2'])
                            self.ts.mark(validated_dest_address_container_result,
                                         "Edit sales order : Verified Dest Address Container fields are displayed "
                                         "same as given in create quote page.")

                            # Notes validation
                            user_note_result = self.qop.validateSavedNotesInQuotePage(row["notetype"], row["notes"])
                            self.ts.mark(user_note_result,
                                         "Edit sales order : verify the Notes(User) in the create quote page")

                            pickup_remark_note_result = self.qop.validateSavedNotesInQuotePage("Pick up Remarks",
                                                                                               row['pickupremarks'])
                            self.ts.mark(pickup_remark_note_result,
                                         "Edit sales order : Verify the Notes(Pick up Remarks) in the create quote "
                                         "page.")
                            delivery_remark_note_result = self.qop.validateSavedNotesInQuotePage("Delivery Remarks",
                                                                                                 row['deliveryremarks'])
                            self.ts.mark(delivery_remark_note_result,
                                         "Edit sales order : Verify the Notes(Delivery Remarks) in the create quote "
                                         "page.")
                        # Commented due to bug LTL1 - 2904 - bug got fixed
                        if row["selectcarrierrate"].lower() == "no":
                            shipment_value = "4000"
                        else:
                            shipment_value = row["shipmentvalue"]

                        validated_shipment_value_result = self.sip.verifyTotalShipmentValue(shipment_value)
                        self.ts.mark(validated_shipment_value_result,
                                     "verified shipment value displayed is same as given in create quote page.")

                        # Freight date validation
                        freight_date_array = row["originpickup"].split("/")
                        monthnum = freight_date_array[0]
                        daynum = freight_date_array[1]
                        if int(monthnum) < 10 and int(daynum) < 10:
                            monthnum = "0" + monthnum
                            daynum = "0" + daynum
                        elif int(monthnum) < 10:
                            monthnum = "0" + monthnum
                        elif int(daynum) < 10:
                            daynum = "0" + daynum
                        freight_date = monthnum + "/" + daynum
                        validate_selected_freight_date = self.rdp.validateSelectedFreightDate(freight_date, "%m/%d")
                        self.ts.mark(validate_selected_freight_date,
                                     "Edit sales order : verified selected freight date is same as given in create "
                                     "quote page.")

                        # Shipping Items and SO Items Summary section validation
                        listof_calculated_data = []
                        edit_flag_status = False
                        commodity = ""
                        for si_row in order.sales_order_shipping:

                            if int(si_row["sno"]) <= int(row["sno"]):

                                if int(si_row["sno"]) == int(row["sno"]):

                                    if si_row["productbook"].lower() == "no":
                                        commodity = si_row["commodity"]
                                    elif si_row["productbook"].lower() == "yes":
                                        commodity = si_row["newcommodity"]

                                    index_value_based_on_commodity = self.sip.getShippingItemIndexBasedOnCommodity(
                                        commodity)
                                    self.log.info(
                                        "index_value_based_on_commodity : " + str(index_value_based_on_commodity))

                                    self.sip.editShippingItem(index_value_based_on_commodity)

                                    # preparing the data to validate SO Items
                                    so_prepared_data = self.sip.getEachShippingItemDataForSOItem()
                                    listof_calculated_data.append(so_prepared_data)
                                    self.log.info("getItemDataFromShippingItem : " + str(so_prepared_data))

                                    # Validating the SO Items Details - commented due bug volume not displayed
                                    # correct - bug fixed
                                    validation_so_item_result = self.sip.validateSOEditItemData(so_prepared_data,
                                                                                                commodity)
                                    self.ts.mark(validation_so_item_result, "Validating the display SO Item")

                                    weightvalue, weightunits, dimvalue, height, length, width = "", "", "", "", "", ""
                                    if si_row["UnitType"].lower() == "pallets(40x48)":
                                        length = "40"
                                        width = "48"

                                    if si_row["weightUnits"].lower() == "kg":
                                        weightunits = "lbs"
                                        weightvalue = int(float(si_row["weight"])) * 2.20462
                                    else:
                                        weightunits = si_row["weightUnits"]
                                        weightvalue = si_row["weight"]

                                    if si_row["Dim"].lower() == "feet":
                                        dimvalue = "inches"
                                        length = int(float(si_row["length"])) * 12
                                        width = int(float(si_row["width"])) * 12
                                        height = int(float(si_row["height"])) * 12
                                    else:
                                        length = si_row["length"]
                                        width = si_row["width"]
                                        height = si_row["height"]

                                    valid_all_shipping_data = \
                                        self.sip.verifyCombinedShippingItemData(commodity,
                                                                                si_row["piece"],
                                                                                str(weightvalue),
                                                                                si_row["unitcount"],
                                                                                si_row["UnitType"],
                                                                                si_row["className"],
                                                                                si_row["nfmc"],
                                                                                str(length),
                                                                                str(width),
                                                                                str(height),
                                                                                str(weightunits),
                                                                                dimvalue,
                                                                                # stackable=si_row["stackable"]
                                                                                stackable="no",
                                                                                hazmat=si_row[
                                                                                    "hazmat"],
                                                                                hzmcode=si_row[
                                                                                    "HazmatCode"],
                                                                                # chemicalname =
                                                                                # si_row[
                                                                                # "ChemicalName"]
                                                                                chemName="",
                                                                                emergNo=si_row[
                                                                                    "emergencyContact"],
                                                                                hzGrp=si_row[
                                                                                    "HazmatGroup"],
                                                                                hzclass=si_row[
                                                                                    "HazmatClass"],
                                                                                prefix=si_row[
                                                                                    "prefix"])
                                    self.ts.mark(valid_all_shipping_data,
                                                 "Edit sales order : Validating the All shipping Data in create quote "
                                                 "page.")
                                    if not edit_flag_status:
                                        self.log.info("Inside Edit details")
                                        edit_flag_status = True
                                        self.sip.si_EnterTextOnField("piece", updated_piece_value)

                                        # Since Chemical name is not displayed, entering chemical name since it is
                                        # mandatory field and have bug created for this
                                        # Entering Hazmat code for its negative scenario validation
                                        if si_row["hazmat"].lower() == "yes":
                                            self.sip.si_EnterTextOnField("ChemicalName", "highly inflammable")
                                            self.sip.si_EnterTextOnField("HazmatCode", "123456")
                                            self.sip.clickButton("save")
                                            place_holder_value_result = self.sip. \
                                                validate_error_message_in_shipping_items("HazmatCode",
                                                                                         "Invalid hazmat code(s)")
                                            self.ts.mark(place_holder_value_result,
                                                         "Hazmat Code cannot enter more than four characters")
                                            self.sip.si_EnterTextOnField("HazmatCode", si_row["HazmatCode"])

                                        self.sip.clickButton("save")

                                        updated_item_row_index_commodity = commodity
                                        updated_item_row_index = row["sno"]
                                        # select accessorial
                                        self.qop.clickingAccessorialsCheckbox("destinationappointment")
                                    else:
                                        self.sip.clickShippingItemCancelButton()
                            else:
                                # commented due to total volume not displayed correctly and have bug here - bug fixed
                                calculated_value_from_so_item = self.sip.calculateTotalSOItemValue(
                                    listof_calculated_data)
                                summary_result = self.sip.verifySOItemSummary(calculated_value_from_so_item)
                                self.ts.mark(summary_result, "Summary Result Validation")
                                break

                        dictionary = {}
                        getDict = {}
                        total_revenue_value = ""
                        self.log.info("updated_item_row_index : " + str(updated_item_row_index))
                        self.log.info("updated_item_row_index_commodity : " + str(updated_item_row_index_commodity))
                        carrier_data = self.car.selectCarrierByNameOrIndex(row["carriers"], row["selectcarrierrate"])

                        BolNumber = row["bolnumber"]
                        if carrier_data is not None:
                            self.ts.mark(carrier_data[5], "Passed carrier result selection and break up correct.")
                            self.log.info("Getting carrier Data : " + str(carrier_data))

                            self.util.add_to_dictionary("totalcost", carrier_data[0], dictionary)
                            self.util.add_to_dictionary("totalrevenue", carrier_data[1], dictionary)
                            self.util.add_to_dictionary("selectedcarriername", carrier_data[6], dictionary)
                            self.util.add_to_dictionary("transitdays", carrier_data[7], dictionary)
                            appointmentdates = carrier_data[8].split("-")
                            self.util.add_to_dictionary("originpickup", appointmentdates[0], dictionary)
                            self.util.add_to_dictionary("etadelivery", appointmentdates[1], dictionary)
                            carrier_data.pop(8)
                            carrier_data.pop(7)
                            carrier_data.pop(6)
                            carrier_data.pop(5)
                            carrier_data.pop(0)
                            carrier_data.pop(0)

                            self.util.add_to_dictionary("updatedcarrierbreakup", carrier_data, dictionary)

                            self.log.info("Dict : " + str(dictionary))
                            quote_value = self.qop.getIdentifierValue("quote")
                            self.log.debug("Get Quote Value : " + str(quote_value))
                            self.util.add_to_dictionary("quoteid", quote_value, dictionary)
                            getDict = self.util.add_to_dictionary("bolnumber", BolNumber, dictionary)
                            self.log.info("Final Dict Object : " + str(getDict))

                        update_order_button_enabeled_result = self.qop.verifyUpdateOrderButtonEnabled()
                        self.ts.mark(update_order_button_enabeled_result,
                                     "Edit sales order : verified update order button enabeled status")
                        if update_order_button_enabeled_result:
                            update_button_enabeled_status_flag = True
                            updated_order_status_result = self.qop.updateSalesOrderDetails()
                            self.ts.mark(updated_order_status_result, "Edit sales order : updated order successfully")
                            self.sdp.soPageLoadWait(25)

                            # Validating data in SO Page on update
                            piecevalue = ""
                            for order1 in orders:
                                row1 = order1.sales_order_data
                                self.log.info("row1 data : " + str(row1))
                                for si_row1 in order.sales_order_shipping:
                                    if int(si_row1["sno"]) <= int(row1["sno"]):
                                        if int(si_row1["sno"]) == int(row1["sno"]):
                                            self.log.info("Inside shipping data validation : " + str(si_row1["sno"]))
                                            if si_row1["productbook"].lower() == "no" and si_row1["commodity"].lower() \
                                                    == updated_item_row_index_commodity.lower():
                                                commodity = si_row1["commodity"]
                                                piecevalue = updated_piece_value
                                            elif si_row1["productbook"].lower() == "no" \
                                                    and not si_row1["commodity"].lower() == \
                                                    updated_item_row_index_commodity.lower():
                                                commodity = si_row1["commodity"]
                                                piecevalue = si_row1["piece"]
                                            elif si_row1["productbook"].lower() == "yes" \
                                                    and si_row1["commodity"].lower() == \
                                                    updated_item_row_index_commodity.lower():
                                                commodity = si_row1["newcommodity"]
                                                piecevalue = updated_piece_value
                                            elif si_row1["productbook"].lower() == "yes" \
                                                    and not si_row1["commodity"].lower() == \
                                                    updated_item_row_index_commodity.lower():
                                                commodity = si_row1["newcommodity"]
                                                piecevalue = si_row1["piece"]
                                            index_value = self.osp.getOrderSummaryIndexValue(commodity)
                                            validate_commodity = self.osp.validateSalesOrderSummaryData("commodity",
                                                                                                        commodity,
                                                                                                        index_value)
                                            self.ts.mark(validate_commodity,
                                                         "Edit sales order : Validating the Commodity Name in SO "
                                                         "Details Page under SO Summary section on update")
                                            validate_shipping_result = \
                                                self.srf.validateShippingItems(commodity, si_row1["nfmc"],
                                                                               si_row1["className"], si_row1["weight"],
                                                                               si_row1["weightUnits"],
                                                                               si_row1["UnitType"],
                                                                               si_row1["unitcount"], piecevalue,
                                                                               si_row1["length"], si_row1["width"],
                                                                               si_row1["height"], si_row1["Dim"],
                                                                               si_row1["hazmat"], si_row1["prefix"],
                                                                               si_row1["HazmatCode"],
                                                                               si_row1["HazmatGroup"],
                                                                               si_row1["HazmatClass"])
                                            self.ts.mark(validate_shipping_result,
                                                         "Edit sales order : Validated shipping items in SO Page "
                                                         "under Financial section on updating data")
                                    else:
                                        self.log.debug(
                                            "Index value out of range, si row id: " + str(
                                                si_row1["sno"]) + " and row index : " + row1["sno"])
                                        break

                                # Validate Financial Section
                                self.log.info("sl no of the updated row: " + str(updated_item_row_index))
                                validate_carrier_breakup_result = self.srf.validateCarrierBreakup(
                                    getDict["updatedcarrierbreakup"])
                                self.ts.mark(validate_carrier_breakup_result,
                                             "Edit sales order : Validated carrier breakup in Financial Section on "
                                             "update")

                                # commented code due to bug LTL1 - 3217
                                validate_total_revenue_result = self.srf.validateTotalValue(getDict["totalrevenue"],
                                                                                            "revenue")

                                self.ts.mark(validate_total_revenue_result,
                                             "Edit sales order : Validated Total Revenue in Financial Section on update"
                                             )

                                # SO Summary Section :
                                self.log.info("SO Summary Section validation on update")
                                validate_summary_container_text = self.osp.validateSalesOrderSummarySection()
                                self.ts.mark(validate_summary_container_text,
                                             "Edit sales order : Validating Sales order Summary Header Text Value on "
                                             "update")
                                validate_carrier_name = self.osp.validateSalesOrderSummaryData("carrier", getDict[
                                    "selectedcarriername"])
                                self.ts.mark(validate_carrier_name,
                                             "Edit sales order : Validating the carrier Name in SO Details Page on "
                                             "update")

                                # Network Section Validation
                                self.log.info("Network Section validation on update")
                                validate_nw_container_text = self.sdp.ValidateNetworkSection()
                                self.ts.mark(validate_nw_container_text,
                                             "Edit sales order : Validating Network container Text Value on update")
                                validate_customer_name = self.sdp.validateNWSectionBasedOnPassedValue("customer", row1[
                                    "customername"])
                                self.ts.mark(validate_customer_name,
                                             "Edit sales order : Validating the Customer Name in SO Page on update")
                                validate_service_type = self.sdp.validateNWSectionBasedOnPassedValue("servicetype",
                                                                                                     "LTL")
                                self.ts.mark(validate_service_type,
                                             "Edit sales order : Validating the Service Type in SO Details Page on "
                                             "update")
                                validate_application_name = self.sdp.validateNWSectionBasedOnPassedValue("generatedon")
                                self.ts.mark(validate_application_name,
                                             "Edit sales order : Validating the application name display in SO "
                                             "Details Page on update")
                                validate_contact_name = self.sdp. \
                                    validateNWSectionBasedOnPassedValue("customercontact", row1["customercontactname"])
                                self.ts.mark(validate_contact_name,
                                             "Edit sales order : Validating the Contact Name display in SO Details "
                                             "Page on update")
                                validate_rating_method = self.sdp.validateNWSectionBasedOnPassedValue("ratingmethod",
                                                                                                      "Standard Tariff")
                                self.ts.mark(validate_rating_method,
                                             "Edit sales order : Validating the Rating method in SO Details Page on "
                                             "update")
                                validate_quote_request = self.sdp.validateNWSectionBasedOnPassedValue("quoterequest",
                                                                                                      getDict[
                                                                                                          "quoteid"])
                                self.ts.mark(validate_quote_request,
                                             "Edit sales order : Validating the Quote request Id in SO Details Page "
                                             "on update")

                                # Route Details Section Validation :
                                self.log.info("Route Details section validation on update")
                                validate_rd_container_text = self.srd.ValidateRouteDetailsSection()
                                self.ts.mark(validate_rd_container_text,
                                             "Edit sales order : Validating Route Details container Text Value in SO "
                                             "Details Page")
                                validate_transit_days = self.srd.validateRDSectionBasedOnPassedValue(
                                    getDict["transitdays"],
                                    "transitdays")
                                self.ts.mark(validate_transit_days,
                                             "Edit sales order : Validating Transit Days Value in SO Details Page on "
                                             "update")
                                if row1["addressbook"].lower() == "yes":
                                    self.log.info(
                                        "Edit sales order : Address Book value is Yes, Only address Book Validation "
                                        "on update")
                                    address_index = row1["sno"]
                                    for r_add in self.order_address_book:
                                        if int(address_index) >= int(r_add["sno"]):
                                            if int(r_add["sno"]) == int(row1["sno"]):
                                                if r_add["addresssection"] == "origin":
                                                    validate_origin_address = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            r_add["addressline1"] + ", " + r_add["address"],
                                                            "completeaddress", r_add["addresssection"])
                                                    self.ts.mark(validate_origin_address,
                                                                 "Edit sales order : Validated Origin address in SO "
                                                                 "Details Page under Route Detail "
                                                                 "Section from address book on update")
                                                    validate_origin_company_name = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            r_add['company_name'], "companyname",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_origin_company_name,
                                                                 "Edit sales order : Validated Origin company name in "
                                                                 "SO Details Page under Route Detail "
                                                                 "Section from address book on update")
                                                    validate_origin_phone_number = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            r_add['contact_phone'], "phonenumber",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_origin_phone_number,
                                                                 "Edit sales order : Validated Origin phone number in "
                                                                 "SO Details Page under Route Detail "
                                                                 "Section from address book from address book on update"
                                                                 )
                                                    validate_origin_appointment = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            getDict["originpickup"], "appointmentdate",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_origin_appointment,
                                                                 "Edit sales order : Validated Origin appointment "
                                                                 "Date in SO Details Page under Route "
                                                                 "Detail Section from address book on update")

                                                    origin_timings = r_add['readytime'] + " - " + r_add['closetime']
                                                    validate_origin_timings = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            origin_timings, "appointmenttimings",
                                                            r_add["addresssection"])

                                                    self.ts.mark(validate_origin_timings,
                                                                 "Edit sales order : Validated Origin Appointment "
                                                                 "Timings in SO Details Page under Route "
                                                                 "Detail Section from address book on update")
                                                    # Notes validation
                                                    pickup_remark_note_result = self.snd.validateSavedNotesInSOPage(
                                                        "Pick up Remarks", r_add['remarks'])
                                                    self.ts.mark(pickup_remark_note_result,
                                                                 "Edit sales order : Verify the Notes(Pick up "
                                                                 "Remarks) from address book in the SO page on "
                                                                 "update.")

                                                elif r_add["addresssection"] == "destination":

                                                    validate_dest_address = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            r_add["addressline1"] + ", " +
                                                            r_add["address"], "completeaddress",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_dest_address,
                                                                 "Edit sales order : Validated Dest address in SO "
                                                                 "Details Page under Route Detail Section from "
                                                                 "address book on update")
                                                    validate_dest_company_name = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            r_add['company_name'], "companyname",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_dest_company_name,
                                                                 "Edit sales order : Validated Dest company name in "
                                                                 "SO Details Page under Route Detail "
                                                                 "Section from address book on update")
                                                    validate_dest_phone_number = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            r_add['contact_phone'], "phonenumber",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_dest_phone_number,
                                                                 "Edit sales order : Validated Dest phone number in "
                                                                 "SO Details Page under Route Detail Section from "
                                                                 "address book on update")
                                                    validate_dest_appointment = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            getDict["etadelivery"], "appointmentdate",
                                                            r_add["addresssection"])
                                                    self.ts.mark(validate_dest_appointment,
                                                                 "Edit sales order : Validated Dest appointment Date "
                                                                 "in SO "
                                                                 "Details Page under Route Detail Section from "
                                                                 "address book on update")

                                                    dest_timings = r_add['readytime'] + " - " + r_add['closetime']
                                                    validate_dest_timings = \
                                                        self.srd.validateRDSectionBasedOnPassedValue(
                                                            dest_timings, "appointmenttimings", r_add["addresssection"])

                                                    self.ts.mark(validate_dest_timings,
                                                                 "Edit sales order : Validated Dest appointment "
                                                                 "Timings in SO Details Page under Route "
                                                                 "Detail Section from address book on update")
                                                    # Notes validation
                                                    delivery_remark_note_result = self.snd.validateSavedNotesInSOPage(
                                                        "Delivery Remarks", r_add['remarks'])
                                                    self.ts.mark(delivery_remark_note_result,
                                                                 "Edit sales order : Verify the Notes(Delivery "
                                                                 "Remarks) from address book in the SO page on "
                                                                 "update.")

                                                else:
                                                    self.log.error("Incorrect address type passed")
                                                    self.ts.mark(False,
                                                                 "Edit sales order : Incorrect address type passed to "
                                                                 "select address Book method")

                                                user_note_result = self.snd.validateSavedNotesInSOPage(row1["notetype"],
                                                                                                       row1["notes"])
                                                self.ts.mark(user_note_result,
                                                             "Edit sales order : verify the Notes(User) in the SO "
                                                             "page on update.")
                                        else:
                                            self.log.info("Edit sales order : out of address Book selection index")
                                            break
                                else:
                                    validate_origin_address = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["oriaddline1"] + ", " + row1["orizip"], "completeaddress", "origin")
                                    self.ts.mark(validate_origin_address,
                                                 "Edit sales order : Validated Origin address in SO Details Page "
                                                 "under Route Detail Section on update")
                                    validate_origin_company_name = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["oricompanyName"], "companyname", "origin")
                                    self.ts.mark(validate_origin_company_name,
                                                 "Edit sales order : Validated Origin company name in SO Details Page "
                                                 "under Route Detail Section on update")
                                    validate_origin_phone_number = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["oriphone"], "phonenumber", "origin")
                                    self.ts.mark(validate_origin_phone_number,
                                                 "Edit sales order : Validated Origin phone number in SO Details Page "
                                                 "under Route Detail Section on update")
                                    validate_origin_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                                        getDict["originpickup"], "appointmentdate", "origin")
                                    self.ts.mark(validate_origin_appointment,
                                                 "Edit sales order : Validated Origin appointment date in SO Details "
                                                 "Page under Route Detail Section on update")
                                    validate_origin_timings = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["orireadytime"] + " - " + row1["oriclosetime"], "appointmenttimings",
                                        "origin")
                                    self.ts.mark(validate_origin_timings,
                                                 "Edit sales order : Validated Origin Appointment Timings in SO "
                                                 "Details Page under Route Detail Section on update")

                                    validate_dest_address = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["destaddline1"] + ", " + row1["destzip"], "completeaddress", "destination")
                                    self.ts.mark(validate_dest_address,
                                                 "Edit sales order : Validated Dest address in SO Details Page under "
                                                 "Route Detail Section on update")
                                    validate_dest_company_name = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["destcompanyname"], "companyname", "destination")
                                    self.ts.mark(validate_dest_company_name,
                                                 "Edit sales order : Validated Dest company name in SO Details Page "
                                                 "under Route Detail Section on update")
                                    validate_dest_phone_number = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["destphone"], "phonenumber", "destination")
                                    self.ts.mark(validate_dest_phone_number,
                                                 "Edit sales order : Validated Dest phone number in SO Details Page "
                                                 "under Route Detail Section on update")
                                    validate_dest_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                                        getDict["etadelivery"], "appointmentdate", "destination")
                                    self.ts.mark(validate_dest_appointment,
                                                 "Edit sales order : Validated Dest appointment date in SO Details "
                                                 "Page under Route Detail Section on update")
                                    validate_dest_timings = self.srd.validateRDSectionBasedOnPassedValue(
                                        row1["destreadytime"] + " - " + row1["destclosetime"], "appointmenttimings",
                                        "destination")

                                    self.ts.mark(validate_dest_timings,
                                                 "Edit sales order : Validated Dest appointment Timings in SO Details "
                                                 "Page under Route Detail Section on update")

                                    # Notes validation
                                    pickup_remark_note_result = self.snd. \
                                        validateSavedNotesInSOPage("Pick up Remarks", row1['pickupremarks'])
                                    self.ts.mark(pickup_remark_note_result,
                                                 "Edit sales order : Verify the Notes(Pick up Remarks) in the SO page "
                                                 "on update")
                                    delivery_remark_note_result = self.snd.validateSavedNotesInSOPage(
                                        "Delivery Remarks",
                                        row1[
                                            'deliveryremarks'])
                                    self.ts.mark(delivery_remark_note_result,
                                                 "Edit sales order : Verify the Notes(Delivery Remarks) in the SO "
                                                 "page on update")
                                    user_note_result = self.snd.validateSavedNotesInSOPage(row1["notetype"],
                                                                                           row1["notes"])
                                    self.ts.mark(user_note_result,
                                                 "Edit sales order : verify the Notes(User) in SO page on update.")

                                # Validate Order status
                                validate_process_status = self.sdp.validateStatusSectionBasedOnPassedValue("process",
                                                                                                           "Pending")
                                self.ts.mark(validate_process_status,
                                             "Edit sales order : Validating the Process status value in Status "
                                             "section on update")
                                validate_invoice_status = self.sdp.validateStatusSectionBasedOnPassedValue("invoice",
                                                                                                           "Pending")
                                self.ts.mark(validate_invoice_status,
                                             "Edit sales order : Validating the Invoice status in Status Section on "
                                             "update")
                                validate_vendor_status = self.sdp.validateStatusSectionBasedOnPassedValue("Vendorbill",
                                                                                                          "None")
                                self.ts.mark(validate_vendor_status,
                                             "Edit sales order : Validating the vendor Bill status in Status section "
                                             "on update")
                                validate_edi_status = self.sdp.validateStatusSectionBasedOnPassedValue("edi", "")
                                self.ts.mark(validate_edi_status,
                                             "Edit sales order : Validating the EDI Status in Status Section on update")
                                validate_tracking_status = self.sdp.validateStatusSectionBasedOnPassedValue("tracking",
                                                                                                            "Pending")
                                self.ts.mark(validate_tracking_status,
                                             "Edit sales order : Validating the Tracking status in Status Section on "
                                             "update")
                                self.sdp.soOrderBoardButton()
                                break
                    else:
                        self.log.error(
                            "Edit sales order : Create quote page is not displayed on clicking edit sales order button")
                    break

        if not update_sales_order_status_flag:
            self.log.error(
                "None of the passed BOL Has pending Status To verify edit sales order data Flow with SO status options "
                "hence Failing the case.")
            self.ts.mark(False,
                         "verify updateSalesOrderStatusFlag Flow with SO status option from SO Details Page Failed as "
                         "not found any SO with pending status")
        if not update_button_enabeled_status_flag:
            self.ts.mark(False, "Edit button is not enabled or found in SO Page")
            self.sdp.soOrderBoardButton()

        self.ts.mark_final("test_validate_edit_sales_order", True, "Validating the Edit Order Feature")

    @pytest.mark.slowtest
    def test_return_to_order(self):
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        for order in orders:
            row = order.sales_order_data
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                # Edit sales order details and validate in SO Page
                self.sop.navigateToSOPage(row["bolnumber"])
                self.sdp.soPageLoadWait(15)
                validated_update_button_status_result = self.sdp.verifyEditButtonEnabledAndClick()
                self.ts.mark(validated_update_button_status_result, "Verified update order button is enabeled and "
                                                                    "clicked")
                if validated_update_button_status_result:
                    self.qop.clickReturnToOrderButton("no")
                    return_order_button_displayed_result = self.qop.createQuotePagePresenceCheck()
                    self.ts.mark(return_order_button_displayed_result, "verified quote page is displayed on clicking "
                                                                       "return no button")
                    self.qop.clickReturnToOrderButton("yes")
                    self.sdp.soPageLoadWait(25)

        self.ts.mark_final("test_return_to_order", True, "Verified Return to order functionality")

    def __login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
