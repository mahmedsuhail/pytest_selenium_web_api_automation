import pytest
import unittest2
import datetime

import utilities.custom_logger as cl
from pages.LTL.createquote.createquote_page_ltl import CreateQuotePageLTL
from pages.LTL.salesorderboard.ordersummarysodetails_page import OrderSummarySODetailsPage
from pages.LTL.salesorderboard.salesorder_page import SalesorderPage
from pages.LTL.salesorderboard.salesorderdetails_page import SalesOrderDetailsPage
from pages.LTL.salesorderboard.salesorderfinancialdetails_page import SalesorderFinancialDetailsPage
from pages.LTL.salesorderboard.salesordernotesanddocuments_page import SalesorderNotesAndDocumentsPage
from pages.LTL.salesorderboard.salesorderroutedetails_page import SalesorderRouteDetailsPage
from pages.common.models.sales_order_creator import SalesOrderCreator
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util

""" Copy order feature is not working, since copy feature is major scenario to validate data,skipping this test case"""


@pytest.mark.usefixtures("oneTimeSetUp", "setUp", "login_and_setup_orders")
class CopySalesOrderTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    loginData = read_csv_data("login_details.csv")
    util = Util(logger=log)

    @pytest.fixture(autouse=True, scope="class")
    def login_and_setup_orders(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).sop = SalesorderPage(self.driver, self.log)
        type(self).sdp = SalesOrderDetailsPage(self.driver, self.log)
        type(self).qop = CreateQuotePageLTL(self.driver, self.log)
        type(self).osp = OrderSummarySODetailsPage(self.driver, self.log)
        type(self).srf = SalesorderFinancialDetailsPage(self.driver, self.log)
        type(self).srd = SalesorderRouteDetailsPage(self.driver, self.log)
        type(self).snd = SalesorderNotesAndDocumentsPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).order_address_book = read_csv_data("salesorder_addressbook.csv")
        self.__login()

        order_shipping = read_csv_data("salesorder_shipping.csv")
        order_data = read_csv_data("salesOrderData.csv")[0:1]
        sales_order_creator = SalesOrderCreator(driver=self.driver, log=self.log)
        type(self).create_orders_result = sales_order_creator.create_orders(order_data, self.order_address_book,
                                                                            order_shipping)

    def __login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

    @pytest.mark.slowtest
    @pytest.mark.jira("LTL1-3732")
    def test_validate_copy_sales_order(self):
        copy_order = False
        self.ts = self.create_orders_result[0]
        orders = self.create_orders_result[1]

        self.sop.clickSalesOrderIcon()
        if self.qop.confirmationPopDisplay():
            self.qop.clickButtonInQuote("navigationconfirm")

        for order in orders:
            row = order.sales_order_data
            ponum = "PO" + row["bolnumber"]
            ref = "REF" + row["bolnumber"]
            applied_bol = self.sop.applyFilterBasedOnColumnName("billOfLading", row["bolnumber"])
            if applied_bol:
                navigation_result = self.sop.navigateToSOPage(row["bolnumber"])
                self.ts.mark(navigation_result, "Validate the Navigation of BOL Number to SO Page")
                so_validation_result = self.sdp.validateSODetailsPage(row["bolnumber"])
                if navigation_result and so_validation_result:
                    if not copy_order:
                        copy_order = True
                        save_identifier_po_result = self.sdp.identifierEditSectionButtonBehaviour("P/O #",
                                                                                                  "saveandconfirm",
                                                                                                  ponum)
                        self.ts.mark(save_identifier_po_result,
                                     "Validating the saveandconfirm(P/O #) behaviour in Edit Identifier Section in SO "
                                     "Page")

                        save_identifier_ref_result = self.sdp.identifierEditSectionButtonBehaviour("Reference #",
                                                                                                   "saveandconfirm",
                                                                                                   ref)
                        self.ts.mark(save_identifier_ref_result,
                                     "Validating the saveandconfirm(Ref #) behaviour in Edit Identifier Section in SO "
                                     "Page")

                        copy_order_result = self.sdp.clickAndValidateCopyOrderButton()
                        self.ts.mark(copy_order_result[0], "Validate the Copy Order Feature in SO Details Page")

                        validate_nw_container_text = self.sdp.ValidateNetworkSection()
                        self.ts.mark(validate_nw_container_text, "Validating Network container Text Value")

                        validate_customer_name = self.sdp.validateNWSectionBasedOnPassedValue("customer",
                                                                                              row["customername"])
                        self.ts.mark(validate_customer_name, "Validating the Customer Name in SO Page")
                        validate_service_type = self.sdp.validateNWSectionBasedOnPassedValue("servicetype", "LTL")
                        self.ts.mark(validate_service_type, "Validating the Service Type in SO Details Page")
                        validate_application_name = self.sdp.validateNWSectionBasedOnPassedValue("generatedon")
                        self.ts.mark(validate_application_name,
                                     "Validating the application name display in SO Details Page")
                        validate_contact_name = self.sdp.validateNWSectionBasedOnPassedValue("customercontact",
                                                                                             row["customercontactname"])
                        self.ts.mark(validate_contact_name, "Validating the Contact Name display in SO Details Page")
                        validateRatingMethod = self.sdp.validateNWSectionBasedOnPassedValue("ratingmethod",
                                                                                            "Manually Rated")
                        self.ts.mark(validateRatingMethod, "Validating the Rating method in SO Details Page")
                        validate_quote_request = self.sdp.validateNWSectionBasedOnPassedValue("quoterequest", "0")
                        self.ts.mark(validate_quote_request, "Validating the Quote request Id in SO Details Page")

                        # SO Summary Section :
                        validate_summary_container_text = self.osp.validateSalesOrderSummarySection()
                        self.ts.mark(validate_summary_container_text,
                                     "Validating Sales order Summary Header Text Value")
                        validate_carrier_name = self.osp.validateSalesOrderSummaryData(
                            "carrier", row["selectedcarriername"])
                        self.ts.mark(validate_carrier_name, "Validating the carrier Name in SO Details Page")

                        for si_row in order.sales_order_shipping:
                            self.log.info("row value si :" + str(si_row["sno"]) + " ,main row : " + str(row["sno"]))
                            if int(si_row["sno"]) <= int(row["sno"]):
                                if int(si_row["sno"]) == int(row["sno"]):
                                    if si_row["productbook"].lower() == "no":
                                        index_value = self.osp.getOrderSummaryIndexValue(si_row["commodity"])
                                        validate_commodity = self.osp.validateSalesOrderSummaryData("commodity",
                                                                                                    si_row["commodity"],
                                                                                                    index_value)
                                        self.ts.mark(validate_commodity,
                                                     "Validating the Commodity Name in SO Details Page")

                                        validate_shipping_result = self.srf.validateShippingItems(si_row["commodity"],
                                                                                                  si_row["nfmc"],
                                                                                                  si_row["className"],
                                                                                                  si_row["weight"],
                                                                                                  si_row["weightUnits"],
                                                                                                  si_row["UnitType"],
                                                                                                  si_row["unitcount"],
                                                                                                  si_row["piece"],
                                                                                                  si_row["length"],
                                                                                                  si_row["width"],
                                                                                                  si_row["height"],
                                                                                                  si_row["Dim"],
                                                                                                  si_row["hazmat"],
                                                                                                  si_row["prefix"],
                                                                                                  si_row["HazmatCode"],
                                                                                                  si_row["HazmatGroup"],
                                                                                                  si_row["HazmatClass"])
                                        self.ts.mark(validate_shipping_result,
                                                     "Validated shipping items in SO Page under Financial section")
                                    else:
                                        index_value = self.osp.getOrderSummaryIndexValue(si_row["newcommodity"])
                                        validate_commodity = self.osp.validateSalesOrderSummaryData("commodity",
                                                                                                    si_row[
                                                                                                        "newcommodity"],
                                                                                                    index_value)
                                        self.ts.mark(validate_commodity,
                                                     "Validating the Commodity Name in SO Details Page")
                                        validate_shippingresult = self.srf.validateShippingItems(si_row["newcommodity"],
                                                                                                 si_row["nfmc"],
                                                                                                 si_row["className"],
                                                                                                 si_row["weight"],
                                                                                                 si_row["weightUnits"],
                                                                                                 si_row["UnitType"],
                                                                                                 si_row["unitcount"],
                                                                                                 si_row["piece"],
                                                                                                 si_row["length"],
                                                                                                 si_row["width"],
                                                                                                 si_row["height"],
                                                                                                 si_row["Dim"],
                                                                                                 si_row["hazmat"],
                                                                                                 si_row["prefix"],
                                                                                                 si_row["HazmatCode"],
                                                                                                 si_row["HazmatGroup"],
                                                                                                 si_row["HazmatClass"])
                                        self.ts.mark(validate_shippingresult,
                                                     "Validated shipping items in SO Page under Financial section")

                                    self.log.info(
                                        "carrier under : " + str(row["selectcarrierrate"]) + " ," +
                                        str(row["carriers"]) + " ," + str(row["addressbook"]) + " ," + str(row["sno"]))

                                    # Shipment value Validation (Have application Issue Here)
                                    validate_shipment_value = self.osp.validateSalesOrderSummaryData("shipmentvalue",
                                                                                                     "0",
                                                                                                     index_value)
                                    self.ts.mark(validate_shipment_value,
                                                 "Validating the Shipment Value in SO Details Page")

                                    # Unit Count Validation
                                    validate_unit_value = self.osp.validateSalesOrderSummaryData("unitcount",
                                                                                                 si_row["unitcount"],
                                                                                                 index_value)
                                    self.ts.mark(validate_unit_value, "Validating the Unit Value in SO Details Page")

                                    validate_weight_value = self.osp.validateWeightSOSummaryData(si_row["weight"],
                                                                                                 si_row["weightUnits"],
                                                                                                 index_value)
                                    self.ts.mark(validate_weight_value,
                                                 "Validating the Weight Value in SO Details Page")

                            else:
                                self.log.debug(
                                    "Index value out of range, si row id: " + str(si_row["sno"]) + " and row index : " +
                                    row["sno"])
                                break

                        # Route Details Section Validation :
                        self.log.info("Route Details section validation")
                        validate_rd_container_text = self.srd.ValidateRouteDetailsSection()
                        self.ts.mark(validate_rd_container_text,
                                     "Validating Route Details container Text Value in SO Details Page")
                        validate_transit_days = self.srd.validateRDSectionBasedOnPassedValue(row["transitdays"],
                                                                                             "transitdays")
                        self.ts.mark(validate_transit_days, "Validating Transit Days Value in SO Details Page")

                        date_value = self.util.get_current_date(time_zone="US/Arizona")
                        self.log.info("Get Current Date : " + str(date_value))

                        # Uncomment when bug LTL1-3492 is fixed
                        # get_days = row["transitdays"].split(" ")
                        # calculated_date = self.util.get_date_based_on_holiday(no_of_days=get_days[0],
                        #                                                       date_from=date_value)
                        # self.log.info("CalculatedDate : " + str(calculated_date))

                        # remove the following lines of code till calculateddate when the bug LTL1-3492 is fixed
                        self.log.info("originpickup : " + str(row["originpickup"]))
                        self.log.info("etadelivery : " + str(row["etadelivery"]))
                        delta = \
                            datetime.datetime.strptime(row["etadelivery"], "%m/%d") - \
                            datetime.datetime.strptime(row["originpickup"], "%m/%d")
                        get_calender_days = abs(delta.days)
                        self.log.info("get_calender_days : " + str(get_calender_days))
                        calculated_date = self.util.getDateValueWithoutHolidayAndWeekends(no_of_days=get_calender_days,
                                                                                          passedDate=str(date_value))
                        self.log.info("CalculatedDate :" + str(calculated_date))

                        if row["addressbook"].lower() == "yes":
                            self.log.info("Address Book value is Yes, Only address Book Validation")
                            address_index = row["sno"]
                            for r_add in self.order_address_book:
                                if int(address_index) >= int(r_add["sno"]):
                                    if int(r_add["sno"]) == int(row["sno"]):
                                        if r_add["addresssection"] == "origin":
                                            validate_origin_address = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add["addressline1"] + ", " + r_add["address"], "completeaddress",
                                                r_add["addresssection"])
                                            self.ts.mark(validate_origin_address,
                                                         "Validated Origin address in SO Details Page under Route "
                                                         "Detail "
                                                         "Section from address book")

                                            validate_origin_company_name = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add['company_name'], "companyname", r_add["addresssection"])
                                            self.ts.mark(validate_origin_company_name,
                                                         "Validated Origin company name in SO Details Page under "
                                                         "Route Detail "
                                                         "Section from address book")

                                            validate_origin_phone_number = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add['contact_phone'], "phonenumber", r_add["addresssection"])
                                            self.ts.mark(validate_origin_phone_number,
                                                         "Validated Origin phone number in SO Details Page under "
                                                         "Route Detail "
                                                         "Section from address book from address book")

                                            validate_origin_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                                                date_value, "appointmentdate", r_add["addresssection"])
                                            self.ts.mark(validate_origin_appointment,
                                                         "Validated Origin appointment Date in SO Details Page under "
                                                         "Route "
                                                         "Detail Section from address book")

                                            validate_origin_timings = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add['readytime'] + " - " + r_add['closetime'], "appointmenttimings",
                                                r_add["addresssection"])
                                            self.ts.mark(validate_origin_timings,
                                                         "Validated Origin Appointment Timings in SO Details Page "
                                                         "under Route "
                                                         "Detail Section from address book")

                                        elif r_add["addresssection"] == "destination":

                                            validate_dest_address = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add["addressline1"] + ", " +
                                                r_add["address"], "completeaddress", r_add["addresssection"])
                                            self.ts.mark(validate_dest_address,
                                                         "Validated Dest address in SO Details Page "
                                                         "under Route Detail Section from address book")
                                            validate_dest_company_name = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add['company_name'], "companyname", r_add["addresssection"])
                                            self.ts.mark(validate_dest_company_name,
                                                         "Validated Dest company name in SO Details Page under Route "
                                                         "Detail "
                                                         "Section from address book")
                                            validate_dest_phone_number = self.srd.validateRDSectionBasedOnPassedValue(
                                                r_add['contact_phone'], "phonenumber", r_add["addresssection"])
                                            self.ts.mark(validate_dest_phone_number,
                                                         "Validated Dest phone number in SO Details "
                                                         "Page under Route Detail Section from address book")

                                            validate_dest_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                                                calculated_date, "appointmentdate", "destination")
                                            # Bug LTL1-2858 --below code will remove once bug get fix
                                            # validate_dest_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                                            #     row["etadelivery"], "appointmentdate", r_add["addresssection"])

                                            self.ts.mark(validate_dest_appointment,
                                                         "Validated Dest appointment Date in SO "
                                                         "Details Page under Route Detail Section from address book")
                                            # Bug LTL1-2843
                                            # validateDestTimings = self.srd.validateRDSectionBasedOnPassedValue(
                                            #     r_add['readytime'] + " - " + r_add['closetime'], "appointmenttimings",
                                            #     r_add["addresssection"])

                                            validate_dest_timings = self.srd.validateRDSectionBasedOnPassedValue(
                                                "12:00 AM" + " - " + "12:00 AM", "appointmenttimings", "destination")
                                            self.ts.mark(validate_dest_timings,
                                                         "Validated Dest appointment Timings in SO Details Page under "
                                                         "Route "
                                                         "Detail Section from address book")
                                        else:
                                            self.log.error("Incorrect address type passed")
                                            self.ts.mark(False,
                                                         "Incorrect address type passed to select address Book method")
                                else:
                                    self.log.info("out of address Book selection index")
                                    break
                        else:
                            validate_origin_address = self.srd.validateRDSectionBasedOnPassedValue(
                                row["oriaddline1"] + ", " + row["orizip"], "completeaddress", "origin")
                            self.ts.mark(validate_origin_address,
                                         "Validated Origin address in SO Details Page under Route Detail Section")
                            validate_origin_company_name = self.srd.validateRDSectionBasedOnPassedValue(
                                row["oricompanyName"], "companyname", "origin")
                            self.ts.mark(validate_origin_company_name,
                                         "Validated Origin company name in SO Details Page under Route Detail Section")
                            validate_origin_phone_number = self.srd.validateRDSectionBasedOnPassedValue(row["oriphone"],
                                                                                                        "phonenumber",
                                                                                                        "origin")
                            self.ts.mark(validate_origin_phone_number,
                                         "Validated Origin phone number in SO Details Page under Route Detail Section")

                            validate_origin_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                                date_value, "appointmentdate", "origin")
                            self.ts.mark(validate_origin_appointment,
                                         "Validated Origin appointment date in SO Details Page under Route Detail "
                                         "Section")
                            validate_origin_timings = self.srd.validateRDSectionBasedOnPassedValue(
                                row["orireadytime"] + " - " + row["oriclosetime"], "appointmenttimings", "origin")
                            self.ts.mark(validate_origin_timings,
                                         "Validated Origin Appointment Timings in SO Details Page under Route Detail "
                                         "Section")

                            validate_dest_address = self.srd.validateRDSectionBasedOnPassedValue(
                                row["destaddline1"] + ", " + row["destzip"], "completeaddress", "destination")
                            self.ts.mark(validate_dest_address,
                                         "Validated Dest address in SO Details Page under Route Detail Section")
                            validate_dest_company_name = self.srd.validateRDSectionBasedOnPassedValue(
                                row["destcompanyname"], "companyname", "destination")
                            self.ts.mark(validate_dest_company_name,
                                         "Validated Dest company name in SO Details Page under Route Detail Section")
                            validate_dest_phone_number = self.srd.validateRDSectionBasedOnPassedValue(row["destphone"],
                                                                                                      "phonenumber",
                                                                                                      "destination")
                            self.ts.mark(validate_dest_phone_number,
                                         "Validated Dest phone number in SO Details Page under Route "
                                         "Detail Section")

                            validate_dest_appointment = \
                                self.srd.validateRDSectionBasedOnPassedValue(calculated_date, "appointmentdate",
                                                                             "destination")
                            # Bug  - LTL1-2858 -- will remove once bug get fix in jira
                            # validate_dest_appointment = self.srd.validateRDSectionBasedOnPassedValue(
                            #     row["etadelivery"], "appointmentdate", "destination")

                            self.ts.mark(validate_dest_appointment,
                                         "Validated Dest appointment date in SO Details Page under Route Detail "
                                         "Section")
                            # Due to Bug - LTL1-2843
                            # validate_dest_timings = self.srd.validateRDSectionBasedOnPassedValue(row[
                            #     "destreadytime"] + " - " + row["destclosetime"], "appointmenttimings", "destination")

                            validate_dest_timings = self.srd.validateRDSectionBasedOnPassedValue(
                                "12:00 AM" + " - " + "12:00 AM", "appointmenttimings", "destination")
                            self.ts.mark(validate_dest_timings,
                                         "Validated Dest appointment Timings in SO Details Page under Route Detail "
                                         "Section")

                            # Status Section Validation
                        validate_process_status = self.sdp.validateStatusSectionBasedOnPassedValue("process", "Pending")
                        self.ts.mark(validate_process_status, "Validating the Process status value in Status section")
                        validate_invoice_status = self.sdp.validateStatusSectionBasedOnPassedValue("invoice", "Pending")
                        self.ts.mark(validate_invoice_status, "Validating the Invoice status in Status Section")
                        validate_vendor_status = self.sdp.validateStatusSectionBasedOnPassedValue("Vendorbill", "None")
                        self.ts.mark(validate_vendor_status, "Validating the vendor Bill status in Status section")
                        validate_edi_status = self.sdp.validateStatusSectionBasedOnPassedValue("edi", "")
                        self.ts.mark(validate_edi_status, "Validating the EDI Status in Status Section")
                        validate_tracking_status = self.sdp.validateStatusSectionBasedOnPassedValue("tracking",
                                                                                                    "Pending")
                        self.ts.mark(validate_tracking_status, "Validating the Tracking status in Status Section")

                        # Validate Identifier Section
                        validate_bol_identifier = self.sdp.getIdentifierValue("BOL #", copy_order_result[1])
                        self.ts.mark(validate_bol_identifier, "Validating the BOL Identifier in copied order SO Page")

                        # Bug - LTL1-2817
                        # validateQuoteIdentifier = self.sdp.getIdentifierValue("Quote #", row["quoteid"])
                        # self.ts.mark(validateQuoteIdentifier, "Validating the Quote Identifier in SO Page")
                        validate_poi_identifier = self.sdp.getIdentifierValue("P/O #", ponum)
                        self.ts.mark(not validate_poi_identifier,
                                     "Validating the PO Number Identifier in copied order SO Page")

                        validate_ref_identifier = self.sdp.getIdentifierValue("Reference #", ref)
                        self.ts.mark(not validate_ref_identifier,
                                     "Validating the Reference Number Identifier in copied order SO Page")
                        # Validate Financial Section
                        copy_order_breakup_list = self.srf.removeLineItemFromBreakupList(row["carrierbreakup"],
                                                                                         "insurance")
                        validate_carrier_breakup_result = self.srf.validateCarrierBreakup(copy_order_breakup_list)
                        self.ts.mark(validate_carrier_breakup_result,
                                     "Validated Copy Order carrier breakup in Financial Section")

                        item_value = self.srf.getLineItemsBreakupValue(row["carrierbreakup"], "insurance", "revenue")
                        expected_total_revenue_after_copy_order = float(row["totalrevenue"]) - float(item_value)
                        self.log.info(
                            "expectedTotalRevenueAfterCopyOrder : " + str(expected_total_revenue_after_copy_order))

                        validate_total_revenue_result = self.srf.validateTotalValue(
                            expected_total_revenue_after_copy_order,
                            "revenue")
                        self.ts.mark(validate_total_revenue_result, "Validated Total Revenue in Financial Section")

                        # Document Section - Have bug here due to that below one is failing
                        validate_default_document_section_result = self.snd.validateDefaultDocumentSection("No")
                        self.ts.mark(validate_default_document_section_result,
                                     "Validated Default Document Section of the BOL.")

                        # Notes:
                        notes_count = self.snd.totalCountOfNoteList()
                        if notes_count == 0:
                            self.ts.mark(True, "Validating the Notes for Copy order scenario")
                        else:
                            self.log.error("Notes Found")
                            self.ts.mark(False, "Validating the Notes for Copy order scenario")

                        self.sdp.soOrderBoardButton()

        if not copy_order:
            self.log.error("BOL Not found or Not able to redirect to SO Details Page")
            self.ts.mark(False, "BOL Not found or Not able to redirect to SO Details Page")

        self.ts.mark_final("test_validate_copy_sales_order", True, "Validating the Copy Order Feature")
