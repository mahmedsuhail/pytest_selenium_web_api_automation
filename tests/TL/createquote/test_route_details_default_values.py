# import pytest
# from assertpy import assert_that
#
# import utilities.custom_logger as cl
# from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
# from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
# from pages.basictypes.service_type import ServiceType
# from utilities.read_data import read_csv_data
# from utilities.read_data import read_json_file
# from verifiers.createquote.create_quote_accessorial_validators import CreateQuoteAccessorialValidators
#
#
# @pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
# class TestRouteDetails:
#     log = cl.test_logger(filename=__qualname__)
#
#     @pytest.fixture(autouse=True)
#     def method_set_up(self, oneTimeSetUpWithAPI):
#         self.qop = CreateQuotePageTL(self.driver, self.log)
#         self._tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)
#         self.verifiers = CreateQuoteAccessorialValidators(self.log)
#         self.test_data = read_json_file("tl_create_quote_origin_destintation.json")
#
#     @pytest.fixture(scope="class")
#     @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
#     def login_and_go_to_tl(self):
#         qop = CreateQuotePageTL(self.driver, self.log)
#         qop.open_page()
#         quote_data = read_csv_data("quote_details.csv")
#         qop.search_customer(quote_data[1]["customerName"])
#         qop.select_customer_from_list()
#         qop.select_service_type(service_type=ServiceType.TL)
#         qop.wait_for_page_loader_to_show_and_disappear()
#
#     def test_route_details_is_empty_by_default(self):
#         origin_address = self._tl_route_details_page.get_origin_data()
#         assert_that(origin_address).is_equal_to("Enter Origin")
#
#     def test_destination_details_is_empty_by_default(self):
#         origin_address = self._tl_route_details_page.get_destination_data()
#         assert_that(origin_address).is_equal_to("Enter Destination")
