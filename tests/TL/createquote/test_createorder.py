from datetime import datetime

import pytest
import unittest2
from assertpy import assert_that, soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_saved_order_popup_page import CreateQuoteSavedOrderPopupPageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestCreateOrderTL(unittest2.TestCase):
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    test_data = read_json_file("tl_create_quote_smoke_test_data.json")
    log = cl.test_logger(filename=__qualname__)
    util = Util(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.create_order_popup = CreateQuoteSavedOrderPopupPageTL(self.driver, self.log)
        self.create_quote_page_tl = CreateQuotePageTL(self.driver, self.log)
        self.create_quote_sales_order_page_tl = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.route_details_page_tl = TLRouteDetailsPage(self.driver, self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-5642")
    def test_create_order_tl_items_not_duplicated(self):
        row_id = 0
        origin = self.test_data.get("origin")
        destination = self.test_data.get("destination")
        date_to_select_str = self.util.getDateValueWithoutHolidayAndWeekends(no_of_days=2)
        date_to_select = datetime.strptime(date_to_select_str, '%m/%d/%Y')
        selected_shipment_type = self.test_data["selectedShipmentType"]
        selected_equipment_type = self.test_data["selectedEquipmentType"]
        selected_equipment_length = self.test_data["selectedEquipmentLength"]

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        self.select_origin(origin=origin, date_to_select=date_to_select)
        self.select_destination(destination=destination, date_to_select=date_to_select)
        self.enter_shipping_item(shipping_item)
        self.set_financials_base(10, 10)
        self.fill_origin_and_destination_data(self.test_data)
        self.create_quote_page_tl.select_equipment_type(selected_equipment_type)
        self.create_quote_page_tl.select_shipment_type(selected_shipment_type)
        self.create_quote_page_tl.select_equipment_length(selected_equipment_length)

        self.create_quote_page_tl.click_create_order_footer_button()

        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()
        self.create_order_popup.wait_for_create_order_popup_visible()
        assert_that(self.create_order_popup.is_go_to_bol_button_displayed()).described_as(
            "Go to bol button should be visible").is_true()

        self.create_order_popup.click_go_to_bol()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_page.is_page_displayed()).described_as(
                "TL-5642 Sales Order Page Should be opened").is_true()

        self.sales_order_page.click_edit_order_footer_button()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        so_items = self.create_quote_sales_order_page_tl.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-5642 Verify Sales Order items count should be 1").is_equal_to(1)

    def select_origin(self, origin, date_to_select):
        self.route_details_page_tl.search_and_select_origin(search_string=origin[:3], expected_result=origin)
        self.route_details_page_tl.set_origin_pickup_date(date_to_set=date_to_select)
        self.route_details_page_tl.set_origin_open_time("08:00")
        self.route_details_page_tl.set_origin_close_time("10:00")

    def select_destination(self, destination, date_to_select):
        self.route_details_page_tl.search_and_select_destination(search_string=destination[:3],
                                                                 expected_result=destination)
        self.route_details_page_tl.set_destination_pickup_date(date_to_set=date_to_select)
        self.route_details_page_tl.set_destination_open_time("18:00")
        self.route_details_page_tl.set_destination_close_time("20:00")

    def enter_shipping_item(self, shipping_item):
        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

    def fill_origin_and_destination_data(self, test_data_from_json):
        self.create_quote_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self.create_quote_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)

        self.create_quote_origin_page.enter_company_name(
            company_name=test_data_from_json["originContainerData"].get("companyName"))
        self.create_quote_origin_page.enter_contact_name(
            contact_name=test_data_from_json["originContainerData"].get("contactName"))
        self.create_quote_origin_page.enter_contact_phone(
            contact_phone=test_data_from_json["originContainerData"].get("contactPhone"))
        self.create_quote_origin_page.type_to_address(
            addr1=test_data_from_json["originContainerData"].get("address"))

        self.create_quote_destination_page.enter_company_name(
            company_name=test_data_from_json["destinationContainerData"].get("companyName"))
        self.create_quote_destination_page.enter_contact_name(
            contact_name=test_data_from_json["destinationContainerData"].get("contactName"))
        self.create_quote_destination_page.enter_contact_phone(
            contact_phone=test_data_from_json["destinationContainerData"].get("contactPhone"))
        self.create_quote_destination_page.type_to_address(
            addr1=test_data_from_json["destinationContainerData"].get("address"))

    def set_financials_base(self, cost, revenue):
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()

        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_cost(cost)
        financials_charge_rows[0].set_revenue(revenue)

        financials_page.click_save()
