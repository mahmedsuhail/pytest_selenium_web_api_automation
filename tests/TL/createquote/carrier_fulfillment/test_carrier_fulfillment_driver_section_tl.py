import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_notes_popup_page import \
    CarrierFulfillmentNotesPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_popup_page import CarrierFulfillmentPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_section_page import CarrierFulfillmentSectionPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.basictypes.service_type import ServiceType
from pages.common.builders.driver_builder import DriverBuilder
from utilities.util import Util
from utilities.validators import Validators
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestCarrierFulfillmentDriverSection:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    test_data = read_json_file("tl_carrier_fulfillment_data.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        self.carrier_fulfillment_section = CarrierFulfillmentSectionPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_popup = CarrierFulfillmentPopupPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_notes_popup = CarrierFulfillmentNotesPopupPage(driver=self.driver, log=self.log)
        self.ts = ReportTestStatus(driver=self.driver, log=self.log)
        self.util = Util(logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")

        qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        qop.click_quote_order_icon()
        qop.search_and_select_customer_by_index(search_string=self.quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.fixture(scope="function", autouse=True)
    def close_fulfillment_popup(self):
        yield
        self.carrier_fulfillment_popup.close_popup_if_present()

    @pytest.mark.testcase("TL-1942")
    def test_select_driver_tl(self):
        carrier_name = self.test_data.get("carriers")[1].get("carrierName")
        driver1 = self.test_data.get("carriers")[1].get("drivers")[0]
        driver2 = self.test_data.get("carriers")[1].get("drivers")[1]

        self._search_and_select_carrier(carrier_name=carrier_name)

        expected_driver = DriverBuilder() \
            .with_name(driver1.get("driverName")) \
            .with_phone(driver1.get("driverPhone")) \
            .build()

        self._validate_driver(index=0, expected_driver=expected_driver)
        self.carrier_fulfillment_popup.select_driver(index=0)
        assert self.carrier_fulfillment_popup.driver_is_selected(index=0)

        expected_driver2 = DriverBuilder() \
            .with_name(driver2.get("driverName")) \
            .with_phone(driver2.get("driverPhone")) \
            .build()

        self._validate_driver(index=1, expected_driver=expected_driver2)
        self.carrier_fulfillment_popup.select_driver(index=1)
        assert self.carrier_fulfillment_popup.driver_is_selected(index=1)

        assert not self.carrier_fulfillment_popup.driver_is_selected(index=0), \
            "Previous driver should become unselected"

    @pytest.mark.testcase("TL-2015")
    def test_add_driver_required_fields_tl(self):
        carrier_name = self.test_data.get("carriers")[4].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        initial_drivers_count = self.carrier_fulfillment_popup.get_drivers_count()

        self.carrier_fulfillment_popup.click_add_driver_button()

        assert self.carrier_fulfillment_popup.verify_driver_name_is_required()
        assert self.carrier_fulfillment_popup.verify_driver_phone_is_required()
        self._verify_drivers_count_remains_the_same(initial_count=initial_drivers_count)

        driver = DriverBuilder().with_name("TestD").with_phone("(344) 454-5344").build()

        self.carrier_fulfillment_popup.enter_driver_name(name=driver.name, index=0)
        self.carrier_fulfillment_popup.click_save_driver(index=0)

        assert self.carrier_fulfillment_popup.verify_driver_phone_is_required()
        self._verify_drivers_count_remains_the_same(initial_count=initial_drivers_count)

        self.carrier_fulfillment_popup.click_cancel_button()
        self._search_and_select_carrier(carrier_name=carrier_name)

        self.carrier_fulfillment_popup.click_add_driver_button()
        self.carrier_fulfillment_popup.enter_driver_phone(phone=driver.phone, index=0)
        self.carrier_fulfillment_popup.click_save_driver(index=0)

        assert self.carrier_fulfillment_popup.verify_driver_name_is_required()
        self._verify_drivers_count_remains_the_same(initial_count=initial_drivers_count)

    @pytest.mark.testcase("TL-2017")
    def test_edit_driver_tl(self):
        carrier_name = self.test_data.get("carriers")[4].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        initial_drivers_count = self.carrier_fulfillment_popup.get_drivers_count()

        self.carrier_fulfillment_popup.click_edit_driver(index=0)
        assert self.carrier_fulfillment_popup.verify_save_driver_button_is_visible(index=0)

        driver = DriverBuilder().with_random_name().with_random_phone().build()

        self.carrier_fulfillment_popup.enter_driver_name(name=driver.name, index=0)
        self.carrier_fulfillment_popup.enter_driver_phone(phone=driver.phone, index=0)
        self.carrier_fulfillment_popup.click_save_driver(index=0)

        self._verify_drivers_count_remains_the_same(initial_count=initial_drivers_count)

        actual_driver = self.carrier_fulfillment_popup.get_driver_data(index=0)

        assert actual_driver.name == driver.name
        assert actual_driver.phone == driver.get_formatted_phone_number()

    @pytest.mark.testcase("TL-2018")
    def test_driver_is_not_required_field_tl(self):
        carrier_name = self.test_data.get("carriers")[2].get("carrierName")
        trailer_no = self.test_data.get("carriers")[2].get("trailerNo")
        erp_code = self.test_data.get("carriers")[2].get("erpCode")
        mc_no = self.test_data.get("carriers")[2].get("mcNo")
        truck_no = self.test_data.get("carriers")[2].get("truckNo")
        dot_no = self.test_data.get("carriers")[2].get("dotNumber")

        self._search_and_select_carrier(carrier_name=carrier_name)

        assert self.carrier_fulfillment_popup.get_drivers_count() == 0, \
            "Carrier should not have any assigned drivers"

        self.carrier_fulfillment_popup.enter_trailer_number_value(trailer_no=trailer_no)
        self.carrier_fulfillment_popup.click_save_and_assign_button()

        with soft_assertions():
            assert_that(self.carrier_fulfillment_section.get_carrier_name()). \
                described_as("Carrier Name not shown in Fulfillment Container section").is_equal_to(carrier_name)
            assert_that(self.carrier_fulfillment_section.get_trailer_no()). \
                described_as("Trailer No not shown in Fulfillment Container section").is_equal_to(trailer_no)
            assert_that(self.carrier_fulfillment_section.get_erp_code()). \
                described_as("ERP Code not shown in Fulfillment Container section").is_equal_to(erp_code)
            assert_that(self.carrier_fulfillment_section.get_mc_no()). \
                described_as("MC No not shown in Fulfillment Container section").is_equal_to(mc_no)
            assert_that(self.carrier_fulfillment_section.get_truck_no()). \
                described_as("Truck No not shown in Fulfillment Container section").is_equal_to(truck_no)
            assert_that(self.carrier_fulfillment_section.get_dot_no()). \
                described_as("Dot No not shown in Fulfillment Container section").is_equal_to(dot_no)
            assert_that(self.carrier_fulfillment_section.get_driver_name()). \
                described_as("Driver name is not empty").is_equal_to("")
            assert_that(self.carrier_fulfillment_section.get_driver_email()). \
                described_as("Driver email is not empty").is_equal_to("")
            assert_that(self.carrier_fulfillment_section.get_driver_phone()). \
                described_as("Driver phone is not empty").is_equal_to("")

        self.carrier_fulfillment_section.click_edit_carrier_button()
        self.carrier_fulfillment_popup.click_remove_carrier_button()
        self.carrier_fulfillment_section.click_close_carrier_search_box()

    @pytest.mark.testcase("TL-2118")
    @pytest.mark.testcase("TL-2123")
    def test_add_and_edit_driver_allowed_characters_driver_name_tl(self):
        carrier_name = self.test_data.get("carriers")[4].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        test_names = ["Test Driver Name", "123456", " !@#$%-+", "Driver 233 Name!@#"]

        self.carrier_fulfillment_popup.click_edit_driver(index=0)

        for data in test_names:
            self.carrier_fulfillment_popup.enter_driver_name(name=data, index=0)
            assert self.carrier_fulfillment_popup.get_driver_entered_name(index=0) == data

        self.carrier_fulfillment_popup.click_add_contact_button()

        for data in test_names:
            self.carrier_fulfillment_popup.enter_driver_name(name=data, index=0)
            assert self.carrier_fulfillment_popup.get_driver_entered_name(index=0) == data

    @pytest.mark.testcase("TL-2122")
    @pytest.mark.testcase("TL-2124")
    def test_add_and_edit_driver_valid_invalid_driver_phone_tl(self):
        carrier_name = self.test_data.get("carriers")[4].get("carrierName")
        driver = DriverBuilder().with_random_phone().build()

        self._search_and_select_carrier(carrier_name=carrier_name)
        invalid_phone_numbers = ['qwerty', ' !@#$%-+=']

        self.carrier_fulfillment_popup.click_edit_driver(index=0)

        self.carrier_fulfillment_popup.enter_driver_phone(phone=driver.phone, index=0)
        assert self.carrier_fulfillment_popup.get_driver_entered_phone(index=0) == driver.get_formatted_phone_number()

        for number in invalid_phone_numbers:
            self.carrier_fulfillment_popup.enter_driver_phone(phone=number, index=0)
            assert self.carrier_fulfillment_popup.get_driver_entered_phone(index=0) == ''

        self.carrier_fulfillment_popup.click_add_driver_button()

        self.carrier_fulfillment_popup.enter_driver_phone(phone=driver.phone, index=0)
        assert self.carrier_fulfillment_popup.get_driver_entered_phone(index=0) == driver.get_formatted_phone_number()

        for number in invalid_phone_numbers:
            self.carrier_fulfillment_popup.enter_driver_phone(phone=number, index=0)
            assert self.carrier_fulfillment_popup.get_driver_entered_phone(index=0) == ''

    def _validate_driver(self, index, expected_driver):
        assert self.carrier_fulfillment_popup.get_driver_name(index) == expected_driver.name, \
            "Driver name value should match"

        assert self.carrier_fulfillment_popup.get_driver_phone(index) == expected_driver.phone, \
            "Driver phone value should match"

    def _search_and_select_carrier(self, carrier_name):
        assert self.carrier_fulfillment_section.fulfillment_container_is_displayed(), \
            "Fulfillment section should be visible"

        self.carrier_fulfillment_section.click_select_carrier_button()

        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.search_and_select_carrier(
            search_string=carrier_name, carrier_name=carrier_name)

        assert self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(), \
            "Carrier fulfillment popup should be opened"

    def _search_for_customer_and_select_tl(self):
        self.qop.search_customer(search_string=self.quote_data[1]["customerName"])
        self.qop.select_customer_from_list()
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    def _verify_drivers_count_remains_the_same(self, initial_count):
        assert self.carrier_fulfillment_popup.get_drivers_count() == initial_count, \
            "Driver count should remain the same"
