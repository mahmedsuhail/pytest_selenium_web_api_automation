import pytest
from assertpy import assert_that, soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_notes_popup_page import \
    CarrierFulfillmentNotesPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_popup_page import CarrierFulfillmentPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_section_page import CarrierFulfillmentSectionPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.basictypes.service_type import ServiceType
from pages.common.builders.dispatcher_builder import DispatcherBuilder
from utilities.util import Util
from utilities.validators import Validators
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestCarrierFulfillmentDispatcherSection:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    test_data = read_json_file("tl_carrier_fulfillment_data.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        self.carrier_fulfillment_section = CarrierFulfillmentSectionPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_popup = CarrierFulfillmentPopupPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_notes_popup = CarrierFulfillmentNotesPopupPage(driver=self.driver, log=self.log)
        self.ts = ReportTestStatus(driver=self.driver, log=self.log)
        self.util = Util(logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")

        qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        qop.click_quote_order_icon()
        qop.search_and_select_customer_by_index(search_string=self.quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.fixture(scope="function", autouse=True)
    def close_fulfillment_popup(self):
        yield
        self.carrier_fulfillment_popup.close_popup_if_present()

    @pytest.mark.testcase("TL-1451")
    def test_select_dispatcher_tl(self):
        carrier_name = self.test_data.get("carriers")[1].get("carrierName")
        dispatcher1 = self.test_data.get("carriers")[1].get("dispatchers")[0]
        dispatcher2 = self.test_data.get("carriers")[1].get("dispatchers")[1]

        self._search_and_select_carrier(carrier_name=carrier_name)

        expected_dispatcher = DispatcherBuilder() \
            .with_name(dispatcher1.get("dispatcherName")) \
            .with_email(dispatcher1.get("dispatcherEmail")) \
            .with_phone(dispatcher1.get("dispatcherPhone")) \
            .with_fax(dispatcher1.get("dispatcherFax")) \
            .build()

        self._validate_dispatcher(index=0, expected_dispatcher=expected_dispatcher)
        assert self.carrier_fulfillment_popup.dispatcher_is_selected(index=0)

        expected_dispatcher2 = DispatcherBuilder() \
            .with_name(dispatcher2.get("dispatcherName")) \
            .with_email(dispatcher2.get("dispatcherEmail")) \
            .with_phone(dispatcher2.get("dispatcherPhone")) \
            .with_fax(dispatcher2.get("dispatcherFax")) \
            .build()

        self._validate_dispatcher(index=1, expected_dispatcher=expected_dispatcher2)
        self.carrier_fulfillment_popup.select_dispatcher(index=1)
        assert self.carrier_fulfillment_popup.dispatcher_is_selected(index=1)

        assert not self.carrier_fulfillment_popup.dispatcher_is_selected(index=0), \
            "Previous dispatcher should become unselected"

        assert self.carrier_fulfillment_popup.get_carrier_name() == carrier_name

    @pytest.mark.testcase("TL-1734")
    def test_empty_dispatcher_info_bar(self):
        carrier_name = self.test_data.get("carriers")[5].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        with soft_assertions():
            assert_that(self.carrier_fulfillment_popup.add_dispatcher_button_is_displayed()).described_as(
                "Add Contact Button should be displayed").is_true()
            assert_that(self.carrier_fulfillment_popup.get_dispatchers_count()) \
                .described_as("No dispatchers should be visible").is_equal_to(0)

    @pytest.mark.testcase("TL-1756")
    def test_add_dispatcher_required_fields_tl(self):
        carrier_name = self.test_data.get("carriers")[2].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        initial_dispatchers_count = self.carrier_fulfillment_popup.get_dispatchers_count()

        self.carrier_fulfillment_popup.click_add_contact_button()

        assert self.carrier_fulfillment_popup.verify_dispatcher_name_is_required()
        assert self.carrier_fulfillment_popup.verify_dispatcher_phone_is_required()

        dispatcher = DispatcherBuilder().with_email("testemail_mail@mail.com").with_fax("(344) 454-5344").build()

        self.carrier_fulfillment_popup.enter_dispatcher_email(email=dispatcher.email, index=0)
        self.carrier_fulfillment_popup.enter_dispatcher_fax(fax=dispatcher.fax, index=0)

        self.carrier_fulfillment_popup.click_save_dispatcher(index=0)

        assert self.carrier_fulfillment_popup.verify_dispatcher_name_is_required()
        assert self.carrier_fulfillment_popup.verify_dispatcher_phone_is_required()

        assert self.carrier_fulfillment_popup.get_dispatchers_count() == initial_dispatchers_count, \
            "Dispatcher shouldn't be added"

    @pytest.mark.testcase("TL-2115")
    @pytest.mark.testcase("TL-2126")
    def test_add_and_edit_dispatcher_allowed_characters_dispatcher_name_tl(self):
        carrier_name = self.test_data.get("carriers")[2].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        test_names = ["Dispatcher", "12345", " !@#$%-+", "Dispatcher 233 Name!@#"]

        self.carrier_fulfillment_popup.click_edit_dispatcher(index=0)

        for data in test_names:
            self.carrier_fulfillment_popup.enter_dispatcher_name(name=data, index=0)
            assert self.carrier_fulfillment_popup.get_dispatcher_entered_name(index=0) == data

        self.carrier_fulfillment_popup.click_add_contact_button()

        for data in test_names:
            self.carrier_fulfillment_popup.enter_dispatcher_name(name=data, index=0)
            assert self.carrier_fulfillment_popup.get_dispatcher_entered_name(index=0) == data

    @pytest.mark.testcase("TL-2125")
    @pytest.mark.testcase("TL-2116")
    def test_add_and_edit_dispatcher_valid_invalid_dispatcher_phone_tl(self):
        carrier_name = self.test_data.get("carriers")[2].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        dispatcher = DispatcherBuilder().with_random_phone().build()
        invalid_phone_numbers = ['qwerty', ' !@#$%-+=']

        self.carrier_fulfillment_popup.click_edit_dispatcher(index=0)

        self.carrier_fulfillment_popup.enter_dispatcher_phone(phone=dispatcher.phone, index=0)
        assert self.carrier_fulfillment_popup.get_dispatcher_entered_phone(
            index=0) == dispatcher.get_formatted_phone_number()

        for number in invalid_phone_numbers:
            self.carrier_fulfillment_popup.enter_dispatcher_phone(phone=number, index=0)
            assert self.carrier_fulfillment_popup.get_dispatcher_entered_phone(index=0) == ''

        self.carrier_fulfillment_popup.click_add_contact_button()

        self.carrier_fulfillment_popup.enter_dispatcher_phone(phone=dispatcher.phone, index=0)
        assert self.carrier_fulfillment_popup.get_dispatcher_entered_phone(
            index=0) == dispatcher.get_formatted_phone_number()

        for number in invalid_phone_numbers:
            self.carrier_fulfillment_popup.enter_dispatcher_phone(phone=number, index=0)
            assert self.carrier_fulfillment_popup.get_dispatcher_entered_phone(index=0) == ''

    @pytest.mark.testcase("TL-2115")
    @pytest.mark.testcase("TL-2126")
    def test_invalid_dispatcher_email_tl(self):
        carrier_name = self.test_data.get("carriers")[2].get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)
        test_emails = ['dispatchertest.com', 'dispatcher@@test.com', 'dispatcher@testcom', 'dispatcher_test.com']

        self.carrier_fulfillment_popup.click_edit_dispatcher(index=0)

        for data in test_emails:
            self.carrier_fulfillment_popup.enter_dispatcher_email(email=data, index=0)
            self.carrier_fulfillment_popup.click_save_dispatcher(index=0)
            assert self.carrier_fulfillment_popup.verify_entered_email_is_invalid(index=0)

        initial_dispatchers_count = self.carrier_fulfillment_popup.get_dispatchers_count()

        self.carrier_fulfillment_popup.click_add_contact_button()

        for data in test_emails:
            self.carrier_fulfillment_popup.enter_dispatcher_name(name="test", index=0)
            self.carrier_fulfillment_popup.enter_dispatcher_phone(phone="123456", index=0)
            self.carrier_fulfillment_popup.enter_dispatcher_email(email=data, index=0)
            self.carrier_fulfillment_popup.click_save_dispatcher(index=0)
            assert self.carrier_fulfillment_popup.verify_entered_email_is_invalid(index=0)

        assert self.carrier_fulfillment_popup.get_dispatchers_count() == initial_dispatchers_count, \
            "Dispatcher shouldn't be added"

    @pytest.mark.testcase("TL-1758")
    @pytest.mark.testcase("TL-2130")
    def test_edit_dispatcher_tl(self):
        carrier_name = self.test_data.get("carriers")[2].get("carrierName")

        assert self.carrier_fulfillment_section.fulfillment_container_is_displayed(), \
            "Fulfillment section should be visible"

        self.carrier_fulfillment_section.click_select_carrier_button()

        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.search_and_select_carrier(
            search_string=carrier_name, carrier_name=carrier_name)

        assert self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(), \
            "Carrier fulfillment popup should be opened"

        initial_dispatchers_count = self.carrier_fulfillment_popup.get_dispatchers_count()

        self.carrier_fulfillment_popup.click_edit_dispatcher(index=0)
        assert self.carrier_fulfillment_popup.verify_save_dispatcher_button_is_visible(index=0)

        dispatcher = DispatcherBuilder() \
            .with_random_name() \
            .with_random_phone() \
            .with_random_email() \
            .with_random_fax() \
            .build()

        self.carrier_fulfillment_popup.enter_dispatcher_data(dispatcher=dispatcher, index=0)
        self.carrier_fulfillment_popup.click_save_dispatcher(index=0)

        assert self.carrier_fulfillment_popup.get_dispatchers_count() == initial_dispatchers_count, \
            "Additional Dispatcher shouldn't be added"

        actual_dispatcher = self.carrier_fulfillment_popup.get_dispatcher_data(index=0)

        assert actual_dispatcher.name == dispatcher.name
        assert actual_dispatcher.phone == dispatcher.get_formatted_phone_number()
        assert actual_dispatcher.email == dispatcher.email
        assert actual_dispatcher.fax == dispatcher.fax

    def _validate_dispatcher(self, index, expected_dispatcher):
        assert self.carrier_fulfillment_popup.get_dispatcher_name(index) == expected_dispatcher.name, \
            "Dispatcher name value should match"

        assert self.carrier_fulfillment_popup.get_dispatcher_phone(index) == expected_dispatcher.phone, \
            "Dispatcher phone value should match"

        assert self.carrier_fulfillment_popup.get_dispatcher_email(index) == expected_dispatcher.email, \
            "Dispatcher email value should match"

        assert self.carrier_fulfillment_popup.get_dispatcher_fax(index) == expected_dispatcher.fax, \
            "Dispatcher fax value should match"

    def _search_and_select_carrier(self, carrier_name):
        assert self.carrier_fulfillment_section.fulfillment_container_is_displayed(), \
            "Fulfillment section should be visible"

        self.carrier_fulfillment_section.click_select_carrier_button()

        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.search_and_select_carrier(
            search_string=carrier_name, carrier_name=carrier_name)

        assert self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(), \
            "Carrier fulfillment popup should be opened"
