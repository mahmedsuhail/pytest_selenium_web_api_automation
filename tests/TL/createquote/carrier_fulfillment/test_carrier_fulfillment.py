import pytest

import utilities.custom_logger as cl
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_notes_popup_page import CarrierFulfillmentNotesPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_popup_page import CarrierFulfillmentPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_section_page import CarrierFulfillmentSectionPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.basictypes.service_type import ServiceType
from utilities.util import Util
from utilities.validators import Validators
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestCarrierFulfillment:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    test_data = read_json_file("tl_carrier_fulfillment_data.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        self.carrier_fulfillment_section = CarrierFulfillmentSectionPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_popup = CarrierFulfillmentPopupPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_notes_popup = CarrierFulfillmentNotesPopupPage(driver=self.driver, log=self.log)
        self.ts = ReportTestStatus(driver=self.driver, log=self.log)
        self.util = Util(logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")

        qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        qop.click_quote_order_icon()
        qop.search_and_select_customer_by_index(search_string=self.quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-1449")
    @pytest.mark.testcase("TL-2112")
    def test_add_carrier_additional_fields_tl(self):
        self.ts.mark(
            self.carrier_fulfillment_section.fulfillment_container_is_displayed(),
            "Fulfillment section should be visible")

        self.carrier_fulfillment_section.click_select_carrier_button()
        self.ts.mark(
            self.carrier_fulfillment_section.carrier_search_window_is_displayed(),
            "Carrier Fulfillment Popup Should be Displayed")

        search_string = "BLACK"
        carrier_name = self.test_data.get("carriers")[0].get("carrierName")

        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.search_and_select_carrier(
            search_string=search_string, carrier_name="BLACK & WHITE TRUCK  LLC")

        self.ts.mark(
            self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(),
            "Carrier fulfillment popup should be opened")

        # Validating Select Carrier Popup data
        self._validate_carrier_textbox_values()
        self._validate_carrier_textbox_editable_states()
        self._validate_dispatcher_section()
        self._validate_driver_section()
        assert self.carrier_fulfillment_popup.get_carrier_name() == carrier_name

        self.ts.mark(
            self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(),
            "Save and assign button should be displayed")

        # Deleting carrier and verifying
        self.carrier_fulfillment_popup.click_remove_carrier_button()
        self.carrier_fulfillment_section.click_close_carrier_search_box()
        self.ts.mark(
            not self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(),
            "Carrier fulfillment popup should be closed")
        self.ts.mark(
            self.carrier_fulfillment_section.select_carrier_button_is_displayed(),
            "Select carrier button should be displayed - no carrier should've been added")

        self.ts.mark_final(
            "test_add_carrier_additional_fields",
            True,
            "test_add_carrier_additional_fields")

    @pytest.mark.testcase("TL-2111")
    def test_carrier_assignment_notes_are_displayed_tl(self):

        carrier_name = self.test_data.get("carriers")[3].get("carrierName")

        assert self.carrier_fulfillment_section.fulfillment_container_is_displayed(), \
            "Fulfillment section should be visible"

        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.search_and_select_carrier(
            search_string=carrier_name, carrier_name=carrier_name)

        self._verify_notes_popup_message_and_close("CENTRAL ARIZONA FREIGHT, LLC Carrier Assighnment testing")
        self._verify_notes_popup_message_and_close("CENTRAL ARIZONA FREIGHT, LLC Carrier Assighnment CENTRAL ARIZONA FREIGHT")
        self._verify_notes_popup_message_and_close("CENTRAL ARIZONA FREIGHT, LLC Carrier Assighnment Note for testing pop-ups")

        assert self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(),\
            "Carrier fulfillment popup should be opened"
        assert self.carrier_fulfillment_popup.get_carrier_name() == carrier_name

        self.carrier_fulfillment_popup.click_cancel_button()

    def _verify_notes_popup_message_and_close(self, message):

        assert self.carrier_fulfillment_notes_popup.carrier_fulfillment_notes_popup_is_displayed(), \
            "Carrier fulfillment notes popup is not displayed"

        note_text = self.carrier_fulfillment_notes_popup.get_displayed_note_text_value()

        assert note_text == message

        self.carrier_fulfillment_notes_popup.click_ok_button()

    def _validate_carrier_textbox_values(self):
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_trailer_number_value(),
                self.test_data.get("carriers")[0].get("trailerNo")),
            "Trailer Number value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_truck_number_value(),
                self.test_data.get("carriers")[0].get("truckNo")),
            "Truck number value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_mc_number_value(),
                self.test_data.get("carriers")[0].get("mcNo")),
            "MC Number value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_erp_code_value(),
                self.test_data.get("carriers")[0].get("erpCode")),
            "ERP code value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_dot_number_value(),
                self.test_data.get("carriers")[0].get("dotNumber")),
            "Dot number value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_legal_name_value(),
                self.test_data.get("carriers")[0].get("legalName")),
            "Legal name value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_city_state_zip_value(),
                self.test_data.get("carriers")[0].get("cityStateZip")),
            "City/State/Zip value should match")

    def _validate_carrier_textbox_editable_states(self):
        self.ts.mark(
            self.carrier_fulfillment_popup.is_trailer_number_editable(),
            "Trailer Number should be editable")
        self.ts.mark(
            self.carrier_fulfillment_popup.is_truck_number_editable(),
            "Truck Number should be editable")
        self.ts.mark(
            not self.carrier_fulfillment_popup.is_mc_number_editable(),
            "MC Number should not be editable")
        self.ts.mark(
            not self.carrier_fulfillment_popup.is_erp_code_editable(),
            "ERP Code should not be editable")
        self.ts.mark(
            not self.carrier_fulfillment_popup.is_dot_number_editable(),
            "Dot number should not be editable")
        self.ts.mark(
            not self.carrier_fulfillment_popup.is_legal_name_editable(),
            "Legal name should not be editable")
        self.ts.mark(
            not self.carrier_fulfillment_popup.is_city_state_zip_editable(),
            "City/State/zip should not be editable")

    def _validate_dispatcher_section(self):
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_dispatcher_name(0),
                self.test_data.get("carriers")[0].get("dispatchers")[0].get("dispatcherName")),
            "Dispatcher name value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_dispatcher_phone(0),
                self.test_data.get("carriers")[0].get("dispatchers")[0].get("dispatcherPhone")),
            "Dispatcher phone value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_dispatcher_email(0),
                self.test_data.get("carriers")[0].get("dispatchers")[0].get("dispatcherEmail")),
            "Dispatcher email value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_dispatcher_fax(0),
                self.test_data.get("carriers")[0].get("dispatchers")[0].get("dispatcherFax")),
            "Dispatcher fax value should match")
        self.ts.mark(
            self.carrier_fulfillment_popup.add_dispatcher_button_is_displayed(),
            "Add Dispatcher button should be displayed")

    def _validate_driver_section(self):
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_driver_name(0),
                self.test_data.get("carriers")[0].get("drivers")[0].get("driverName")),
            "Driver name value should match")
        self.ts.mark(
            self.validators.verify_text_match(
                self.carrier_fulfillment_popup.get_driver_phone(0),
                self.test_data.get("carriers")[0].get("drivers")[0].get("driverPhone")),
            "Driver phone value should match")
        self.ts.mark(
            self.carrier_fulfillment_popup.add_driver_button_is_displayed(),
            "Add Driver button should be displayed")
