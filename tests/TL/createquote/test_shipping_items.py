import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from API.Models.product_commodity import ProductCommodity
from API.api_product import APIProduct
from API.api_product_tools import APIProductTools
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestShippingItems():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_testing_data.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        result = self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        type(self).selected_customer_id = result[2]
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    def __reopen_page_and_select_customer(self):
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        result = self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-1063")
    @pytest.mark.testcase("TL-1064")
    def test_si_user_can_see_product_book_and_close(self):
        # TL-1063
        product_book_page = self.shipping_items.open_product_book_popup()
        result = product_book_page.search_commodity_input_visibility()
        self.ts.mark(result, "'Commodity search box is visible")

        result = self.verifiers.validate_search_commodity_placeholder(
            product_book_page.get_search_commodity_input_placeholder())
        self.ts.mark(result, "Commodity search placeholder is proper")

        commodity_items = product_book_page.get_available_commodity_items()
        result = self.verifiers.validate_commodity_items_count_more_than_zero(commodity_items)
        self.ts.mark(result, "Commodity items exists")

        result = commodity_items[0].is_displayed()
        self.ts.mark(result, "Commodity items are visible")

        result = product_book_page.is_close_button_visible()
        self.ts.mark(result, "Closure element is visible")

        # TL-1064
        product_book_page.click_close()
        result = product_book_page.is_close_button_visible()
        self.ts.mark(not result, "Product book box is not visible after close")

        product_book_page = self.shipping_items.open_product_book_popup()
        result = product_book_page.search_commodity_input_visibility()
        self.ts.mark(result, "'Product book box is visible")

        product_book_page.click_close()
        result = product_book_page.is_close_button_visible()
        self.ts.mark(not result, "Product book box is not visible after close")

        self.ts.mark_final("test_si_user_can_see_product_book_and_close",
                           True,
                           "Validating that Product book is visible on Shipping Items after click button")

    @pytest.mark.testcase("TL-1068")
    def test_si_vertical_scroll_appears_on_more_than_10_commodities(self):
        product_book_page = self.shipping_items.open_product_book_popup()
        commodity_items = product_book_page.get_available_commodity_items()
        result = self.verifiers.validate_commodity_items_count_more_than_zero(commodity_items)
        self.ts.mark(result, "Commodity items exists before search")

        result = product_book_page.is_commodity_items_list_scrollable()
        self.ts.mark(result, "Commodity items list is scrollable")

        # Turns out that this step is not realistic, sometimes if there're multiline rows,
        # can be visible less than 10 commodities. Agreed with TC creator that this step is disabled
        # count_visible = product_book_page.get_amount_of_visible_commodities()
        # result = count_visible == 10

        product_book_page.click_close()
        self.ts.mark(result, "Verify if there's 10 visible commodities")
        self.ts.mark_final("test_si_vertical_scroll_appears_on_more_than_10_commodities",
                           True,
                           "Validating that vertical scroll appear on more than 10 commodities")

    @pytest.mark.testcase("TL-1065")
    def test_si_user_can_search_commodities_in_product_book(self):
        product_book_page = self.shipping_items.open_product_book_popup()
        commodity_items_before = product_book_page.get_available_commodity_items()
        result = self.verifiers.validate_commodity_items_count_more_than_zero(commodity_items_before)
        self.ts.mark(result, "Commodity items exists before search")

        product_book_page.search_for_commodity("Pr")
        commodity_items_after_first_search = product_book_page.get_available_commodity_items()
        result = self.verifiers.verify_list_match(commodity_items_before, commodity_items_after_first_search)
        self.ts.mark(result, "Commodity items after typing 2 signs is the same as before search")

        value_to_search = "Pro"
        product_book_page.search_for_commodity(value_to_search)
        commodity_items_after_second_search = product_book_page.get_available_commodity_items()
        result = self.verifiers.verify_list_match(commodity_items_before, commodity_items_after_second_search)
        self.ts.mark(not result, "Commodity items after typing 3 signs is different as before search")

        commodity_items_names_after_second_search = product_book_page.get_available_commodity_items_names()
        result = self.verifiers.validate_commodity_items_names_are_only_with_part_of_value(
            commodity_items_names_after_second_search, value_to_search)
        self.ts.mark(result, "Commodity items after search contains only values that contain search value")

        product_book_page.click_close()
        self.ts.mark_final("test_si_user_can_search_commodities_in_product_book",
                           True,
                           "Validating that user can search Commodities items in Product book")

    @pytest.mark.testcase("TL-1066")
    def test_si_user_sees_no_results_found_if_no_records_match_search(self):
        product_book_page = self.shipping_items.open_product_book_popup()
        commodity_items_before = product_book_page.get_available_commodity_items()
        result = self.verifiers.validate_commodity_items_count_more_than_zero(commodity_items_before)
        self.ts.mark(result, "Commodity items exists before search")

        product_book_page.search_for_commodity("qqqq")

        commodity_items_names = product_book_page.get_available_commodity_items_names()
        result = commodity_items_names[0] == "No results found"

        product_book_page.click_close()
        self.ts.mark(result, "If no Commodity found, message 'No results found' is printed")
        self.ts.mark_final("test_si_user_sees_no_results_found_if_no_records_match_search",
                           True,
                           "Validating that message 'No results found' is printed when user search not existing item")

    @pytest.mark.testcase("TL-1067")
    def test_si_shipping_item_prepopulated_with_selected_product(self):
        selected_product = "tl1067_product_t1"
        api_product = APIProductTools(APIProduct(self.api))
        product_if_empty = ProductCommodity(
            commodity_description=selected_product, weight_amount=23, weight_unit=21, piece_count=22,
            handling_unit_type_id=0, handling_unit_count=31, nmfc_number="9999", hazmat_flag=True, hazmat_class_id=10,
            hazmat_group_id=2, hazmat_prefix_id=0, hazmat_code="TST1", hazmat_chemical_name="ASD1",
            hazmat_emergency_number="(232)333-2223", stackable=True, dim_length=11, dim_width=12, dim_height=13,
            dim_unit_id=0, freight_class_id=55)

        result = api_product.check_if_product_exist_or_add_if_not(customer_id=self.selected_customer_id,
                                                                  product_name=selected_product,
                                                                  product_object_if_not_found=product_if_empty)
        if not result:
            raise Exception("There's no required product found. Tried to add but still missing, cannot proceed")

        product_book_page = self.shipping_items.open_product_book_popup()
        commodity_items = product_book_page.get_available_commodity_items()
        result = self.verifiers.validate_commodity_items_count_more_than_zero(commodity_items)
        self.ts.mark(result, "Commodity items exists before search")

        product_book_page.search_for_commodity(selected_product)
        commodity_items_after_first_search = product_book_page.get_available_commodity_items()
        product_book_page.select_item_from_list(commodity_items_after_first_search, selected_product)

        result = product_book_page.is_close_button_visible()
        self.ts.mark(not result, "Product book is closed after select")

        si_from_page = self.shipping_items.get_shipping_item_from_page()
        result = self.verifiers.validate_search_item_from_page_equal_to_expected(si_expected=product_if_empty,
                                                                                 si_from_page=si_from_page)
        self.ts.mark(result, "Shipping item prepopulated with selected product")
        self.ts.mark_final("test_si_shipping_item_prepopulated_with_selected_product",
                           True,
                           "Validating that shipping items is prepopulated with selected product")

    @pytest.mark.testcase("TL-1145")
    def test_si_all_inputs_clear_after_click_clear(self):
        self.__reopen_page_and_select_customer()
        row_id = 2
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["weightUnits"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .with_stackable() \
            .with_hazmat() \
            .with_hazmat_group(self.shipping_items_data[row_id]["HazmatGroup"]) \
            .with_hazmat_class(self.shipping_items_data[row_id]["HazmatClass"]) \
            .with_hazmat_prefix(self.shipping_items_data[row_id]["prefix"]) \
            .with_hazmat_code(self.shipping_items_data[row_id]["HazmatCode"]) \
            .with_hazmat_chemical_name(self.shipping_items_data[row_id]["ChemicalName"]) \
            .with_hazmat_emergency_contact(self.shipping_items_data[row_id]["HazmatCode"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_after_set = shipping_items_page.get_shipping_item_from_page()

        assert_that(shipping_items_after_set).is_equal_to(shipping_item)

        shipping_items_page.click_clear_button()
        shipping_items_after_clear = shipping_items_page.get_shipping_item_from_page()

        assert_that(shipping_items_after_clear).is_equal_to(shipping_item.get_clear_template())

    @pytest.mark.testcase("TL-1144")
    def test_si_not_added_to_order_if_required_fields_missing(self):
        self.__reopen_page_and_select_customer()
        row_id = 2
        shipping_item = ShippingItemBuilder() \
            .with_stackable() \
            .with_hazmat() \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        container_color_before = shipping_items_page.get_container_header_color()
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        placeholders = shipping_items_page.get_shipping_item_placeholders_from_page()

        placeholders_template = shipping_item.get_required_placeholders(is_hazmat=True, is_stackable=True)
        assert_that(placeholders).is_equal_to(placeholders_template)

        container_color_after = shipping_items_page.get_container_header_color()
        assert_that(container_color_before).is_not_equal_to(container_color_after)

    @pytest.mark.testcase("TL-1143")
    def test_si_added_to_order_if_all_required_fields_are_set(self):
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]

        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        row_id = 1
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.click_clear_button()
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        shipping_items_after_clear = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_clear).is_equal_to(shipping_item.get_clear_template())

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_items = sales_order_page.get_sale_item_rows()

        shipping_items_after_add = shipping_items_page.get_shipping_item_from_page()

        with soft_assertions():
            assert_that(shipping_items_after_add).is_equal_to(shipping_item.get_clear_template())
            assert_that(len(so_items)).described_as("Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("Total density").is_equal_to("1104.00")
            assert_that(sales_order_page.get_total_weight()).described_as("Total weight").is_equal_to("2024.00")
