import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.contactbook.contactbook_destination_page import ContactBookDestinationPage
from pages.TL.createquote.contactbook.contactbook_origin_page import ContactBookOriginPage
from pages.TL.createquote.contactbook.contactbook_page import ContactBookPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.route_details_popup_page import TLRouteDetailsPopupPage
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.models.data_holder import DataHolder
from utilities.read_data import read_csv_data
from utilities.read_data import read_json_file
from utilities.util import Util
from verifiers.createquote.create_quote_accessorial_validators import CreateQuoteAccessorialValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestAddressBookComponents:
    log = cl.test_logger(filename=__qualname__)
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self._rl_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self._rl_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)
        self._tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)
        self.verifiers = CreateQuoteAccessorialValidators(self.log)
        self.test_data = read_json_file("tl_add_contactbook_entry.json")

    @pytest.mark.testcase("TL-1926")
    @pytest.mark.testcase("TL-3643")
    @pytest.mark.testcase("TL-3659")
    @pytest.mark.testcase("TL-3661")
    @pytest.mark.testcase("TL-3662")
    @pytest.mark.testcase("TL-3664")
    def test_add_address_origin(self):
        contact_book = ContactBookOriginPage(self.driver, self.log)
        self.__perform_test_add_address(contact_book)

    @pytest.mark.testcase("TL-1926")
    @pytest.mark.testcase("TL-3643")
    @pytest.mark.testcase("TL-3659")
    @pytest.mark.testcase("TL-3661")
    @pytest.mark.testcase("TL-3662")
    @pytest.mark.testcase("TL-3664")
    @pytest.mark.jira("TL-8250")
    def test_add_address_destination(self):
        contact_book = ContactBookDestinationPage(self.driver, self.log)
        self.__perform_test_add_address(contact_book)

    @pytest.mark.testcase("TL-3652")
    def test_add_address_not_visible_on_additional_stops(self):
        # click add additional stop button
        self._tl_route_details_page.click_add_additional_stop_button()
        route_details_poppup_page = TLRouteDetailsPopupPage(self.driver, self.log)

        # add one stop
        route_details_poppup_page.add_stop()

        # there are 3 stops now, check all for the condition
        contactbook = route_details_poppup_page.open_stop_address_book(0)
        assert_that(contactbook.is_add_address_visible()) \
            .described_as("TL-3652 add address button not visible on add stop").is_equal_to(False)
        contactbook.close()

        contactbook = route_details_poppup_page.open_stop_address_book(1)
        assert_that(contactbook.is_add_address_visible()) \
            .described_as("TL-3652 add address button not visible on add stop").is_equal_to(False)
        contactbook.close()

        contactbook = route_details_poppup_page.open_stop_address_book(2)
        assert_that(contactbook.is_add_address_visible()) \
            .described_as("TL-3652 add address button not visible on add stop").is_equal_to(False)
        contactbook.close()

    @pytest.mark.testcase("TL-1927")
    @pytest.mark.testcase("TL-1928")
    @pytest.mark.testcase("TL-3694")
    @pytest.mark.testcase("TL-3697")
    @pytest.mark.testcase("TL-3689")
    @pytest.mark.jira("TL-9632")
    def test_user_is_able_to_edit_contact(self):
        contacbook = ContactBookOriginPage(self.driver, self.log)
        self.__perform_test_user_is_able_to_edit_contact(contacbook)

    def __perform_test_add_address(self, contact_book: ContactBookPage):
        data = DataHolder(self.test_data["customer1"]["contactbook_item"])
        data["company"] = data["company"] + Util(logger=self.log).get_unique_name(16)  # add random string to name

        # open address book
        contact_book.click_on_icon()

        assert_that(contact_book.is_add_address_visible()) \
            .is_equal_to(True) \
            .described_as(f"TL-3643 Add Address button is visible on opened Address Book pop-up for "
                          f"Origin/Destination on Route Details container")

        assert_that(contact_book.is_add_address_form_visible()) \
            .is_equal_to(True) \
            .described_as(f"TL-3659 Add Address form is displayed when user clicks Add Address button ")

        contact_book.add_address_click()

        # try to save empty form
        contact_book.save_button_click()

        with soft_assertions():
            assert_that(contact_book.is_company_name_required()) \
                .described_as("TL-3661 Validation messages for empty required... [company name]").is_equal_to(True)
            assert_that(contact_book.is_contact_name_required()) \
                .described_as("TL-3661 Validation messages for empty required... [contact name]").is_equal_to(True)
            assert_that(contact_book.is_street_address_required()) \
                .described_as("TL-3661 Validation messages for empty required... [street address 1]").is_equal_to(True)
            assert_that(contact_book.is_phone_number_required()) \
                .described_as("TL-3661 Validation messages for empty required... [phone]").is_equal_to(True)
            assert_that(contact_book.is_zip_required()) \
                .described_as("TL-3661 Validation messages for empty required... [zip, state]").is_equal_to(True)

        # save new contact data
        contact_book.add_address_click()
        contact_book.fill_form(
            company_name=data["company"],
            contact_name=data["contact_name"],
            phone_number=data["contact_phone"],
            email=data["contact_email"],
            street1=data["address1"],
            street2=data["address2"],
            zip_state_city=data["zip"])
        contact_book.save_button_click()
        contact_book.wait_until_saving_button_show_and_disappear()

        # find new address in search
        contact_book.set_search_text(data["company"])
        assert_that(contact_book.is_contact_found(data["company"])) \
            .described_as("TL-3664 Address successfully added and available to search").is_equal_to(True)

    def __perform_test_user_is_able_to_edit_contact(self, contactbook: ContactBookPage):
        data = DataHolder(self.test_data["customer1"]["contactbook_item"])

        # search for contacts
        contactbook.click_on_icon()
        contactbook.set_search_text(data["company"][:3])  # hopping to find more than 1

        # check if there are edit icons in results
        assert_that(contactbook.is_results_list_editable()) \
            .described_as("TL-3674 edit buttons are displayed in result list").is_equal_to(True)

        # check the address is editable

        contactbook.edit_first_result()

        # try to save empty form
        contactbook.clear_form()
        contactbook.save_button_click()

        with soft_assertions():
            assert_that(contactbook.is_company_name_required()) \
                .described_as("TL-3677 Validation messages for empty required... [company name]").is_equal_to(True)
            assert_that(contactbook.is_contact_name_required()) \
                .described_as("TL-3677 Validation messages for empty required... [contact name]").is_equal_to(True)
            assert_that(contactbook.is_street_address_required()) \
                .described_as("TL-3677 Validation messages for empty required... [street address 1]").is_equal_to(True)
            assert_that(contactbook.is_phone_number_required()) \
                .described_as("TL-3677 Validation messages for empty required... [phone]").is_equal_to(True)

        # prepare test random data
        data["company"] = data["company"] + Util(logger=self.log).get_unique_name(16)  # add random string to name
        data["contact_name"] = Util(logger=self.log).get_unique_name(10)

        # edit data
        contactbook.fill_form(
            company_name=data["company"],
            contact_name=data["contact_name"],
            phone_number=data["contact_phone"],
            email=data["contact_email"],
            street1=data["address1"],
            street2=data["address2"],
            zip_state_city=data["zip"])
        contactbook.set_business_hours("08:00", "18:00")
        contactbook.save_button_click()

        # find address in search
        contactbook.set_search_text(data["company"])
        contactbook.NO_ATTEMPTS = 3
        assert_that(contactbook.is_contact_found(data["company"])) \
            .described_as("TL-3664 Address successfully edited and available to search").is_equal_to(True)

        # edit again
        contactbook.edit_first_result()

        new_company_name = data["company"][:10] + Util(logger=self.log).get_unique_name(16)

        contactbook.fill_form(
            company_name=new_company_name,
            contact_name=data["contact_name"],
            phone_number=data["contact_phone"],
            email=data["contact_email"],
            street1=data["address1"],
            street2=data["address2"],
            zip_state_city=data["zip"])

        # cancel
        contactbook.cancel_button_click()

        # search for old name, should be present
        contactbook.set_search_text(data["company"])
        assert_that(contactbook.is_contact_found(data["company"])) \
            .described_as("TL-3678 Address edition canceled and old name found").is_equal_to(True)

        # search for old name, should not be present
        contactbook.set_search_text(new_company_name)
        assert_that(contactbook.is_contact_not_found(new_company_name)) \
            .described_as("TL-3678 Address edition canceled and new name not found").is_equal_to(True)
