import pytest
import unittest2
from parameterized import parameterized

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.basictypes.service_type import ServiceType
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class CreateQuoteCustomerSectionTests(unittest2.TestCase):
    quoteData = read_csv_data("quote_details.csv")
    equipment_length_data = read_csv_data("tl_equipment_length_data.csv")
    equipment_types_data = read_csv_data("tl_equipment_types_data.csv")
    service_types_data = read_csv_data("tl_service_types_data.csv")
    shipment_types_data = read_csv_data("tl_shipment_types_data.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUpWithAPI):
        self.create_quote_page = CreateQuotePageTL(self.driver, self.log)
        self.test_status = ReportTestStatus(self.driver, self.log)
        self.validators = Validators(self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUp")
    def login_and_go_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")
        create_quote_customer_page = CreateQuotePageTL(self.driver, self.log)
        create_quote_customer_page.click_quote_order_icon()
        create_quote_customer_page.clear_customer_search_field()
        create_quote_customer_page.search_and_select_customer_by_index(self.quoteData[1]["customerName"])
        create_quote_customer_page.select_service_type(service_type=ServiceType.TL)
        create_quote_customer_page.wait_for_page_loader_to_show_and_disappear()

    @pytest.fixture(scope="function", autouse=True)
    def clear_equipment_type_field(self):
        yield
        self.create_quote_page.clear_equipment_type_field()

    '''
    GIVEN User is logged in TMS and Create Quote page is opened. Truckload service type is selected
    WHEN User clicks on Service drop-down
    THEN The following dropdown options are displayed: Full (should be displayed selected by default), Partial, Volume
    '''

    @pytest.mark.testcase("TL-1199")
    def test_verify_user_can_select_tl_service_value(self):

        expected_service_values = self.service_types_data[0].get("serviceTypes").split(',')

        self.test_status.mark(self.create_quote_page.service_field_is_displayed(),
                              "Service field should be displayed in Create Quote section")

        self.test_status.mark(
            self.create_quote_page.verify_service_field_dropdown_values(expected_service_values),
            "Service drop down should have correct values populated")

        self.test_status.mark_final(
            "test_select_truckload_serviceType",
            True,
            "Validating Create Quote section after selecting TL service type")

    '''
    GIVEN User is logged in TMS and Create Quote page is opened. Truckload service type is selected
    WHEN User clicks on Equipment Type drop-down
    THEN The following dropdown options are displayed: Van, Flatbed, Temp Controlled (Frozen), Temp Controlled (Refrigerated), Power Only,
        RGN, Conestoga, Curtainside, Straight Truck, Double Drop, Cargo, Container, Sprinter Van, TOFC, COFC, Bulk, Cargo Van, Partial TL,
        Drayage, Stepdeck, Other
    '''

    @pytest.mark.testcase("TL-1200")
    def test_verify_user_can_select_tl_equipment_type(self):

        expected_equipment_types = self.equipment_types_data[0].get("equipmentTypes").split(',')

        assert self.create_quote_page.equipment_type_field_is_displayed(), \
            "Equipment field should be displayed in Create Quote section"

        assert self.validators.verify_unordered_list_match(self.create_quote_page.get_equipment_type_dropdown_values(),
                                                           expected_equipment_types), \
            "Equipment drop down should have correct values populated"

    '''
    GIVEN User is logged in TMS and Create Quote page is opened. Truckload service type is selected
    WHEN User clicks on Shipment drop-down
    THEN The following dropdown options are displayed: Spot Quote, Contract Ongoing
    '''

    @pytest.mark.testcase("TL-1201")
    def test_verify_user_can_select_tl_shipment_type(self):

        expected_shipment_types = self.shipment_types_data[0].get("shipmentTypes").split(',')

        self.test_status.mark(self.create_quote_page.shipment_type_field_is_displayed(),
                              "Shipment field should be displayed in Create Quote section")
        self.test_status.mark(
            self.create_quote_page.verify_shipment_type_dropdown_values(expected_shipment_types),
            "Shipment drop down should have correct values populated")

        self.test_status.mark_final(
            "test_verify_user_can_select_tl_shipment_type",
            True,
            "Validating Shipment Section after selecting TL service type")

    '''
    GIVEN User is logged in TMS and Create Quote page is opened. Truckload service type is selected
    WHEN User selects provided Equipment type
    THEN correct Equipment Length options are displayed
    '''

    @pytest.mark.testcase("TL-1208")
    @pytest.mark.testcase("TL-1209")
    @pytest.mark.testcase("TL-1210")
    @pytest.mark.testcase("TL-1211")
    @pytest.mark.testcase("TL-1214")
    @pytest.mark.testcase("TL-1217")
    @pytest.mark.testcase("TL-1218")
    @pytest.mark.testcase("TL-1221")
    @pytest.mark.testcase("TL-1222")
    @pytest.mark.testcase("TL-1223")
    @pytest.mark.testcase("TL-1224")
    @pytest.mark.testcase("TL-1225")
    @pytest.mark.testcase("TL-1226")
    @pytest.mark.testcase("TL-1227")
    @pytest.mark.testcase("TL-1228")
    @pytest.mark.testcase("TL-1229")
    @parameterized.expand([
        ["Van"],
        ["Flatbed"],
        ["Temp Controlled (Frozen)"],
        ["Temp Controlled (Refrigerated)"],
        ["Stepdeck"],
        ["Conestoga"],
        ["Curtainside"],
        ["Cargo"],
        ["Container"],
        ["Sprinter Van"],
        ["TOFC"],
        ["COFC"],
        ["Bulk"],
        ["Cargo Van"],
        ["Partial TL"],
        ["Drayage"],
    ])
    def test_verify_equipment_length_tl(self, selected_equipment_type):

        test_data = self.__get_equipment_data_by_key(self.equipment_length_data, selected_equipment_type)
        expected_equipment_length_options = test_data.get("equipmentLengthOptions").split(',')
        equipment_length_to_select = test_data.get("selectedEquipmentLength")

        self.test_status.mark(self.create_quote_page.equipment_type_field_is_displayed(),
                              "Equipment type field should be displayed in Create Quote section")
        self.create_quote_page.select_equipment_type(selected_equipment_type)

        self.test_status.mark(self.create_quote_page.equipment_length_field_is_displayed(),
                              "Equipment length field should be displayed in Create Quote section after selecting Equipment type")
        self.test_status.mark(
            self.create_quote_page.verify_equipment_length_field_value("Select Equipment Length"),
            "Equipment length field should not have a default value")

        self.test_status.mark(
            self.create_quote_page.verify_equipment_length_dropdown_values(expected_equipment_length_options),
            "Equipment length field should have correct values populated")

        self.create_quote_page.select_equipment_length(equipment_length_to_select)
        self.test_status.mark(
            self.create_quote_page.verify_equipment_length_field_value(equipment_length_to_select),
            "Equipment length field should be correctly populated after click")

        self.test_status.mark_final("test_verify_user_can_select_tl_equipment_length",
                                    True,
                                    "Validating Equipment length field after selecting Equipment type in TL")

    '''
     GIVEN User is logged in TMS and Create Quote page is opened. Truckload service type is selected
     WHEN User selects provided Equipment type
     THEN correct Equipment Length options are displayed
     '''

    @pytest.mark.testcase("TL-1213")
    @parameterized.expand([
        ["Temp Controlled (Frozen)"],
        ["Temp Controlled (Refrigerated)"],
    ])
    def test_verify_equipment_temperature_tl(self, selected_equipment_type):

        test_data = self.__get_equipment_data_by_key(self.equipment_length_data, selected_equipment_type)
        min_temp = test_data.get("minTemp")
        max_temp = test_data.get("maxTemp")

        self.create_quote_page.select_equipment_type(selected_equipment_type)

        self.test_status.mark(self.create_quote_page.verify_min_temp_field_is_displayed(),
                              "Min Temp field should be displayed")
        self.test_status.mark(self.create_quote_page.verify_max_temp_field_is_displayed(),
                              "Max Temp field should be displayed")
        self.create_quote_page.enter_min_temp(min_temp)
        self.create_quote_page.enter_max_temp(max_temp)

        self.test_status.mark(self.create_quote_page.verify_min_temp_field_value(min_temp),
                              "Min Temp field value should be correctly populated")
        self.test_status.mark(
            self.create_quote_page.verify_max_temp_field_value(max_temp),
            "Max Temp field value should be correctly populated")

        self.test_status.mark_final(
            "test_verify_equipment_temperature_tl",
            True,
            "Validating Equipment temperature fields after selecting Equipment type in TL")

    @pytest.mark.testcase("TL-1213")
    @pytest.mark.testcase("TL-1216")
    @pytest.mark.testcase("TL-1220")
    @pytest.mark.testcase("TL-1230")
    @parameterized.expand([
        ["RGN"],
        ["Double Drop"],
        ["Other"],
    ])
    def test_verify_equipment_width_height_length_options_tl(self, selected_equipment_type):

        test_data = self.__get_equipment_data_by_key(self.equipment_length_data, selected_equipment_type)
        length = test_data.get("length")
        width = test_data.get("width")
        height = test_data.get("height")

        self.create_quote_page.select_equipment_type(selected_equipment_type)

        self.test_status.mark(self.create_quote_page.verify_length_field_is_displayed(),
                              "Length field should be displayed")
        self.test_status.mark(self.create_quote_page.verify_width_field_is_displayed(),
                              "Width field should be displayed")
        self.test_status.mark(self.create_quote_page.verify_height_field_is_displayed(),
                              "Height field should be displayed")

        self.create_quote_page.enter_length(length)
        self.create_quote_page.enter_width(width)
        self.create_quote_page.enter_height(height)

        self.test_status.mark(self.create_quote_page.verify_length_value(length),
                              "Length value should be correctly populated")
        self.test_status.mark(self.create_quote_page.verify_width_value(width),
                              "Width value should be correctly populated")

        self.test_status.mark(self.create_quote_page.verify_height_value(height),
                              "Height value should be correctly populated")

        self.test_status.mark_final("test_verify_equipment_temperature_tl", True,
                                    "Validating Equipment temperature fields after selecting Equipment type in TL")

    @pytest.mark.testcase("TL-1215")
    def test_verify_power_only_equipment_has_no_width_height_length_options_tl(self):

        selected_equipment_type = "Power Only"

        self.create_quote_page.select_equipment_type(selected_equipment_type)

        self.test_status.mark(not self.create_quote_page.verify_length_field_is_displayed(),
                              "Length field should not be displayed")
        self.test_status.mark(not self.create_quote_page.verify_width_field_is_displayed(),
                              "Width field should not be displayed")
        self.test_status.mark(not self.create_quote_page.verify_height_field_is_displayed(),
                              "Height field should not be displayed")

        self.test_status.mark_final("test_verify_power_only_equipment_has_no_width_height_length_options_tl", True,
                                    "Validating Equipment temperature fields after selecting Equipment type in TL")

    def __get_equipment_data_by_key(self, equipment_length_data, equipment_type):
        for data in equipment_length_data:
            if data.get("equipmentType") == equipment_type:
                return data
