import pytest

import utilities.custom_logger as cl
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from utilities.validators import Validators
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_navigate_to_tl")
class TestSalesOrderContainerRestrictedCommoditiesLink:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_testing_data.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)
    util = Util(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        self.shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.ts = ReportTestStatus(driver=self.driver, log=self.log)
        self.validator = Validators(logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_navigate_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")
        qop = CreateQuotePageTL(self.driver, self.log)
        qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()
        self.enter_route_details()
        self.enter_shipping_items()

    def enter_route_details(self):
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]

        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

    def enter_shipping_items(self):
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[1]["commodity"]) \
            .with_piece_count(self.shipping_items_data[1]["piece"]) \
            .with_weight(self.shipping_items_data[1]["weight"]) \
            .with_weight_units(self.shipping_items_data[1]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[1]["unitcount"]) \
            .with_length(self.shipping_items_data[1]["length"]) \
            .with_width(self.shipping_items_data[1]["width"]) \
            .with_height(self.shipping_items_data[1]["height"]) \
            .with_dim_units(self.shipping_items_data[1]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

    @pytest.mark.testcase("TL-1460")
    @pytest.mark.testcase("TL-1440")
    def test_so_container_restricted_commodities_link(self):
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_commodities_link_text_and_hyper_link_url = sales_order_page.get_restricted_commodities_link()
        self.ts.mark(self.validator.verify_text_match(so_commodities_link_text_and_hyper_link_url[0],
                                                      "Restricted/Prohibited Commodities"),
                     "Validated Restricted or Prohibited Commodities link text in Sales Order Container")
        if "RestrictedOrExcludedCommodities.pdf" in so_commodities_link_text_and_hyper_link_url[1]:
            link_file_name = True
        else:
            link_file_name = False
        self.ts.mark(link_file_name,
                     "Validated Restricted or Prohibited Commodities link file name")
        self.ts.mark_final("test_so_container_restricted_commodities_link", True,
                           "test_so_container_restricted_commodities_link")
