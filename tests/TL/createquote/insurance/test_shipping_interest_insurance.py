import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestShippingInterestInsurance:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_customer(self.quote_data[1]["customerName"])
        result = self.qop.select_customer_from_list()
        type(self).selected_customer_id = result[1]
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-4515")
    @pytest.mark.testcase("TL-4518")
    @pytest.mark.testcase("TL-4642")
    @pytest.mark.testcase("TL-4700")
    def test_add_shippers_interests_insurance_happy_path_tl(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)

        # TL-4515
        with soft_assertions():
            assert_that(sales_order_page.add_shipper_interest_insurance_button_is_displayed()). \
                described_as("TL-4515 Add Shipper Interest Insurance Icon should be visible").is_true()
            assert_that(sales_order_page.get_shipper_interest_insurance_section_text()) \
                .described_as("TL-4515 Shipper Interests Insurance text").is_equal_to("Add Shipper Interests Insurance")

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.is_modal_visible()

        # TL-4518
        with soft_assertions():
            assert_that(add_shipper_insurance_page.get_handled_by_label_text()).described_as(
                "TL-4518 Handled By label").is_equal_to("Handled By*")

            # TL-4700
            assert_that(add_shipper_insurance_page.get_handled_by_dropdown_options()).described_as(
                "TL-4700 Handled By dropdown options").is_equal_to(
                ["GlobalTranz", "Carrier Direct"])

            assert_that(add_shipper_insurance_page.get_insurance_provider_label_text()).described_as(
                "TL-4518 Insurance Provider label").is_equal_to(
                "Insurance Provider*")
            assert_that(add_shipper_insurance_page.get_additional_insurance_amount_label_text()).described_as(
                "TL-4518 Insurance Provider text").is_equal_to(
                "Additional Insurance Amount*")
            assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_label_text()).described_as(
                "TL-4518 Cost to GTZ or Carrier label").is_equal_to(
                "Cost to GTZ or Carrier*")
            assert_that(add_shipper_insurance_page.get_charge_to_customer_label_text()).described_as(
                "TL-4518 Cost to GTZ or Carrier text").is_equal_to(
                "Charge to Customer (Rev)")
            assert_that(add_shipper_insurance_page.is_save_button_displayed()).described_as(
                "TL-4518 Save button").is_true()
            assert_that(add_shipper_insurance_page.is_cancel_button_displayed()).described_as(
                "TL-4518 Cancel button").is_true()

        self._fill_insurance_provider_form(add_shipper_insurance_page)

        # TL-4642
        with soft_assertions():
            assert_that(add_shipper_insurance_page.get_handled_by()).described_as(
                "TL-4642 Handled By Value").is_equal_to(
                "GlobalTranz")
            assert_that(add_shipper_insurance_page.get_insurance_provider()).described_as(
                "TL-4642 Insurance Provider").is_equal_to("Test")
            assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
                "TL-4642 Additional Insurance Amount").is_equal_to("100000")
            assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier()).described_as(
                "TL-4642 Cost to GTZ or carrier").is_equal_to("1000000")

        add_shipper_insurance_page.click_save()

        with soft_assertions():
            assert_that(add_shipper_insurance_page.is_modal_visible()).is_false()
            assert_that(sales_order_page.edit_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Edit Shippers Interest Insurance button should be displayed").is_true()
            assert_that(sales_order_page.remove_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Remove Shippers Interest Insurance button should be displayed").is_true()

    @pytest.mark.testcase("TL-4724")
    @pytest.mark.testcase("TL-4761")
    def test_add_shippers_interests_insurance_acceptable_field_values_tl(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        self._fill_insurance_provider_form(add_shipper_insurance_page)

        # Insurance Provider Field - Special Characters
        add_shipper_insurance_page.set_insurance_provider("!@#$%^")
        assert_that(add_shipper_insurance_page.get_insurance_provider()).described_as(
            "Insurance Provider value").is_equal_to("!@#$%^")

        # Validate that changes persist
        add_shipper_insurance_page.click_save()
        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        assert_that(add_shipper_insurance_page.get_insurance_provider()).described_as(
            "Insurance Provider value").is_equal_to("!@#$%^")

        # Insurance Provider Field - Letters
        add_shipper_insurance_page.set_insurance_provider("AaZz")
        assert_that(add_shipper_insurance_page.get_insurance_provider()).described_as(
            "Insurance Provider value").is_equal_to("AaZz")

        # Insurance Provider Field - DIGITS
        add_shipper_insurance_page.set_insurance_provider("123456")
        assert_that(add_shipper_insurance_page.get_insurance_provider()).described_as(
            "Insurance Provider value").is_equal_to("123456")

        # Additional Insurance Amount Field - valid digits & decimals
        add_shipper_insurance_page.set_additional_insurance_amount("123456")
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
            "Additional Insurance Amount Field value").is_equal_to("123456")

        add_shipper_insurance_page.set_additional_insurance_amount("0.01")
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
            "Additional Insurance Amount Field value").is_equal_to("0.01")

        # Additional Insurance Amount Field - invalid characters
        add_shipper_insurance_page.set_additional_insurance_amount("!@#$%^")
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
            "Additional Insurance Amount Field value").is_equal_to("")

        add_shipper_insurance_page.set_additional_insurance_amount("AaZz")
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
            "Additional Insurance Amount Field value").is_equal_to("")

    @pytest.mark.testcase("TL-4766")
    @pytest.mark.testcase("TL-4853")
    def test_financials_insurance_minimum_value_to_additional_insurance_amount(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        total_shipment_value = "100000"

        # TL-4766

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test provider")
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier("10")
        color_before = add_shipper_insurance_page.get_additional_insurance_amount_border_color()
        add_shipper_insurance_page.click_save()
        color_after = add_shipper_insurance_page.get_additional_insurance_amount_border_color()

        with soft_assertions():
            assert_that(color_before).described_as("TL-4766 color changed").is_not_equal_to(color_after)
            assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()).described_as(
                "TL-4766 placeholder").is_equal_to("Required")

        # TL-4766
        additional_insurance_amount = "0"
        add_shipper_insurance_page.set_additional_insurance_amount(additional_insurance_amount)
        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()).described_as(
            "TL-4766 placeholder").is_equal_to("Amount must be greater than or equal to the shipment value")

        # TL-4853
        additional_insurance_amount = "99999.99"
        add_shipper_insurance_page.set_additional_insurance_amount(additional_insurance_amount)
        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()).described_as(
            "TL-4766 placeholder").is_equal_to("Amount must be greater than or equal to the shipment value")

        additional_insurance_amount = total_shipment_value
        add_shipper_insurance_page.set_additional_insurance_amount(additional_insurance_amount)
        add_shipper_insurance_page.click_save()

        with soft_assertions():
            assert_that(add_shipper_insurance_page.is_modal_visible()).described_as("TL-3374 modal hidden").is_false()
            assert_that(sales_order_page.is_shipper_insurance_remove_button_visible()).described_as(
                "TL-3374 Shipper insurance is added").is_true()

    @pytest.mark.testcase("TL-4854")
    def test_user_can_change_total_shipment_value_to_be_greater_than_additional_insurance_amount(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        self._fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.click_save()

        sales_order_page.set_total_shipment_value_and_unfocus("100001")

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
            "Additional Insurance Amount").is_equal_to("100000")
        add_shipper_insurance_page.close_modal()

        assert_that(sales_order_page.get_total_shipment_value()).described_as(
            "Total Shipment Value").is_equal_to("100001")

    @pytest.mark.testcase("TL-4643")
    @pytest.mark.testcase("TL-4644")
    def test_cancel_adding_shippers_interests_insurance_tl(self):
        self.populate_route_details_and_add_shipping_item()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        self._fill_insurance_provider_form(add_shipper_insurance_page)

        add_shipper_insurance_page.click_cancel()

        with soft_assertions():
            assert_that(add_shipper_insurance_page.is_modal_visible()).is_false()
            assert_that(sales_order_page.add_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Add Shippers Interest Insurance button should be displayed").is_true()
            assert_that(sales_order_page.edit_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Edit Shippers Interest Insurance button shouldn't be displayed").is_false()
            assert_that(sales_order_page.remove_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Remove Shippers Interest Insurance button shouldn't be displayed").is_false()

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        self._fill_insurance_provider_form(add_shipper_insurance_page)

        add_shipper_insurance_page.close_modal()

        with soft_assertions():
            assert_that(add_shipper_insurance_page.is_modal_visible()).is_false()
            assert_that(sales_order_page.add_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Add Shippers Interest Insurance button should be displayed").is_true()
            assert_that(sales_order_page.edit_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Edit Shippers Interest Insurance button shouldn't be displayed").is_false()
            assert_that(sales_order_page.remove_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Remove Shippers Interest Insurance button shouldn't be displayed").is_false()

    @pytest.mark.testcase("TL-4645")
    @pytest.mark.testcase("TL-4646")
    def test_add_shippers_interest_insurance_invalid_or_missing_data_tl(self):
        self.populate_route_details_and_add_shipping_item()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        self._paritally_fill_insurance_provider_form(add_shipper_insurance_page)

        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_handled_by_placeholder()) \
            .described_as("Handled By placeholder").is_equal_to("Required")

        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.clear_insurance_provider_field()

        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_insurance_provider_placeholder()) \
            .described_as("Insurance provider placeholder").is_equal_to("Required")

        self._paritally_fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.clear_additional_insurance_amount_field()

        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()) \
            .described_as("Additional Insurance amount placeholder").is_equal_to(
            "Amount must be greater than or equal to the shipment value")

        self._paritally_fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.clear_cost_to_gtz_or_carrier_field()

        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()) \
            .described_as("Cost to GTZ or Carrier placeholder").is_equal_to("Required")

        # Entering Invalid Data
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test")
        add_shipper_insurance_page.set_additional_insurance_amount("0")
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier("0")
        add_shipper_insurance_page.set_charge_to_customer("0")

        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()) \
            .described_as("Additional Insurance amount placeholder").is_equal_to(
            "Amount must be greater than or equal to the shipment value")
        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()) \
            .described_as("Cost to GTZ or Carrier placeholder").is_equal_to("Minimal value 0.01")

    @pytest.mark.testcase("TL-4648")
    @pytest.mark.testcase("TL-4647")
    def test_delete_insurance_tl(self):
        self.populate_route_details_and_add_shipping_item()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        # User can cancel deleting insurance
        self._fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.click_save()

        popup = sales_order_page.click_shipper_insurance_remove_button()
        popup.wait_for_popup_visible()
        popup.click_no()

        self._verify_that_insurance_is_added(sales_order_page)

        # User can delete insurance
        popup = sales_order_page.click_shipper_insurance_remove_button()
        popup.wait_for_popup_visible()
        popup.click_yes()

        self._verify_that_insurance_is_not_added(sales_order_page)

    @pytest.mark.testcase("TL-4649")
    def test_edit_insurance_happy_path_tl(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        self._fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.click_save()

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        add_shipper_insurance_page.set_additional_insurance_amount("200001")
        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_false()
        self._verify_that_insurance_is_added(sales_order_page)

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()) \
            .described_as("Edited Additional Insurance Amount should be saved").is_equal_to("200001")

    @pytest.mark.testcase("TL-4650")
    @pytest.mark.testcase("TL-4651")
    def test_cancel_editing_insurance_tl(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        self._fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.click_save()

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        add_shipper_insurance_page.set_additional_insurance_amount("200001")
        add_shipper_insurance_page.click_cancel()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_false()
        self._verify_that_insurance_is_added(sales_order_page)

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()) \
            .described_as("Edited Additional Insurance Amount should not be changed").is_equal_to("100000")

        add_shipper_insurance_page.set_additional_insurance_amount("200001")
        add_shipper_insurance_page.close_modal()
        assert_that(add_shipper_insurance_page.is_modal_visible()).is_false()
        self._verify_that_insurance_is_added(sales_order_page)

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount()) \
            .described_as("Edited Additional Insurance Amount should not be changed").is_equal_to("100000")

    @pytest.mark.testcase("TL-4653")
    @pytest.mark.testcase("TL-4652")
    def test_edit_insurance_invalid_data_tl(self):
        self.populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()

        self._fill_insurance_provider_form(add_shipper_insurance_page)
        add_shipper_insurance_page.click_save()

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        self._fill_insurance_form_with_invalid_data(add_shipper_insurance_page)
        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()) \
            .described_as("Additional Insurance amount placeholder").is_equal_to(
            "Amount must be greater than or equal to the shipment value")
        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()) \
            .described_as("Cost to GTZ or Carrier placeholder").is_equal_to("Minimal value 0.01")

        add_shipper_insurance_page.click_cancel()
        self._verify_that_insurance_is_added(sales_order_page)

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        self._verify_that_insurance_form_has_correct_data_filled(add_shipper_insurance_page)

        self._clear_insurance_provider_form(add_shipper_insurance_page)

        add_shipper_insurance_page.click_save()
        assert_that(add_shipper_insurance_page.is_modal_visible()).is_true()
        assert_that(add_shipper_insurance_page.get_insurance_provider_placeholder()) \
            .described_as("Insurance provider placeholder").is_equal_to("Required")
        assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()) \
            .described_as("Additional Insurance amount placeholder").is_equal_to(
            "Amount must be greater than or equal to the shipment value")
        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()) \
            .described_as("Cost to GTZ or Carrier placeholder").is_equal_to("Required")

        add_shipper_insurance_page.click_cancel()
        self._verify_that_insurance_is_added(sales_order_page)

        add_shipper_insurance_page = sales_order_page.click_edit_shipper_insurance_button()
        self._verify_that_insurance_form_has_correct_data_filled(add_shipper_insurance_page)

    def populate_route_details_and_add_shipping_item(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)

    def _verify_that_insurance_is_added(self, sales_order_page):
        with soft_assertions():
            assert_that(sales_order_page.edit_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Edit Shippers Interest Insurance button should be displayed").is_true()
            assert_that(sales_order_page.remove_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Remove Shippers Interest Insurance button should be displayed").is_true()

    def _verify_that_insurance_is_not_added(self, sales_order_page):
        with soft_assertions():
            assert_that(sales_order_page.add_shipper_interest_insurance_button_is_displayed()). \
                described_as("Add Shipper Interest Insurance Icon should be visible").is_true()
            assert_that(sales_order_page.edit_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Edit Shippers Interest Insurance button should be displayed").is_false()
            assert_that(sales_order_page.remove_shipper_interest_insurance_button_is_displayed()) \
                .described_as("Remove Shippers Interest Insurance button should be displayed").is_false()

    def _fill_insurance_provider_form(self, add_shipper_insurance_page):
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test")
        add_shipper_insurance_page.set_additional_insurance_amount("100000")
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier("1000000")

    def _fill_insurance_form_with_invalid_data(self, add_shipper_insurance_page):
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test")
        add_shipper_insurance_page.set_additional_insurance_amount("0")
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier("0")
        add_shipper_insurance_page.set_charge_to_customer("0")

    def _paritally_fill_insurance_provider_form(self, add_shipper_insurance_page):
        add_shipper_insurance_page.set_insurance_provider("Test")
        add_shipper_insurance_page.set_additional_insurance_amount("100000")
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier("1000000")

    def _clear_insurance_provider_form(self, add_shipper_insurance_page):
        add_shipper_insurance_page.clear_insurance_provider_field()
        add_shipper_insurance_page.clear_additional_insurance_amount_field()
        add_shipper_insurance_page.clear_cost_to_gtz_or_carrier_field()
        add_shipper_insurance_page.clear_charge_to_customer_field()

    def _verify_that_insurance_form_has_correct_data_filled(self, add_shipper_insurance_page):
        with soft_assertions():
            assert_that(add_shipper_insurance_page.get_handled_by()).described_as("Handled By Value").is_equal_to(
                "GlobalTranz")
            assert_that(add_shipper_insurance_page.get_insurance_provider()).described_as(
                "Insurance Provider").is_equal_to("Test")
            assert_that(add_shipper_insurance_page.get_additional_insurance_amount()).described_as(
                "Additional Insurance Amount").is_equal_to("100000")
            assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier()).described_as(
                "Cost to GTZ or carrier").is_equal_to("1000000")
