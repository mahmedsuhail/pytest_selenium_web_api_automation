import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_popup_page import CarrierFulfillmentPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_section_page import CarrierFulfillmentSectionPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestCarrierInsurance:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    carrier_insurance_data = read_json_file("tl_carrier_insurance_data.json")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.carrier_fulfillment_section = CarrierFulfillmentSectionPage(driver=self.driver, log=self.log)
        self.carrier_fulfillment_popup = CarrierFulfillmentPopupPage(driver=self.driver, log=self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_customer(self.quote_data[1]["customerName"])
        result = self.qop.select_customer_from_list()
        type(self).selected_customer_id = result[1]
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-4353")
    @pytest.mark.testcase("TL-4355")
    def test_insurance_options_carrier_not_selected(self):
        self._populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)

        # TL-4353
        with soft_assertions():
            assert_that(sales_order_page.add_shipper_interest_insurance_button_is_displayed()). \
                described_as("TL-4353 Add Shipper Interest Insurance Icon should be visible").is_true()
            assert_that(sales_order_page.get_shipper_interest_insurance_section_text()) \
                .described_as("TL-4353 Shipper Interests Insurance text").is_equal_to("Add Shipper Interests Insurance")
            assert_that(sales_order_page.get_carrier_insurance_section_label()). \
                described_as("TL-4353 Carrier Insurance Label").is_equal_to("Carrier Insurance Coverage")

            # TL-4355
            assert_that(sales_order_page.get_carrier_insurance_section_text()) \
                .described_as("TL-4355 Carrier Insurance value").is_equal_to("Carrier not selected")

    @pytest.mark.testcase("TL-4356")
    def test_insurance_options_carrier_is_selected(self):
        self._populate_route_details_and_add_shipping_item()
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        carrier_name = self.carrier_insurance_data.get("carrierName")
        carrier_insurance_value = self.carrier_insurance_data.get("carrierInsuranceValue")

        self._search_and_select_carrier(carrier_name=carrier_name)

        with soft_assertions():
            assert_that(sales_order_page.add_shipper_interest_insurance_button_is_displayed()). \
                described_as("TL-4356 Add Shipper Interest Insurance Icon should be visible").is_true()
            assert_that(sales_order_page.get_shipper_interest_insurance_section_text()) \
                .described_as("TL-4356 Shipper Interests Insurance text").is_equal_to("Add Shipper Interests Insurance")
            assert_that(sales_order_page.get_carrier_insurance_section_label()). \
                described_as("TL-4356 Carrier Insurance Label").is_equal_to("Carrier Insurance Coverage")
            assert_that(sales_order_page.get_carrier_insurance_section_text()) \
                .described_as("TL-4356 Carrier Insurance value").is_equal_to(carrier_insurance_value)

    def _populate_route_details_and_add_shipping_item(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)

    def _search_and_select_carrier(self, carrier_name):
        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.click_select_carrier_button()
        assert self.carrier_fulfillment_section.carrier_search_window_is_displayed(), \
            "Carrier Fulfillment Popup Should be Displayed"

        self.carrier_fulfillment_section.search_and_select_carrier(
            search_string=carrier_name, carrier_name=carrier_name)
        assert self.carrier_fulfillment_popup.save_and_assign_button_is_displayed(), \
            "Carrier fulfillment popup should be opened"

        self.carrier_fulfillment_popup.click_save_and_assign_button()
