import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestFinancialsInsurance():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())
        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-3372")
    @pytest.mark.testcase("TL-4764")
    def test_financials_insurance_user_can_enter_only_numerical_to_charge(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        # TL-3372
        charge = "12345"
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.set_charge_to_customer(charge)

        assert_that(add_shipper_insurance_page.get_charge_to_customer()).described_as(
            "TL-3372 Charge cost numeric").is_equal_to(charge)

        # TL-4764
        charge = "0.01"
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.set_charge_to_customer(charge)

        assert_that(add_shipper_insurance_page.get_charge_to_customer()).described_as(
            "TL-4764 Charge cost decimal").is_equal_to(charge)

        charge = "Qq"
        add_shipper_insurance_page.set_charge_to_customer(charge)
        assert_that(add_shipper_insurance_page.get_charge_to_customer()).described_as(
            "TL-3372 Charge cost text").is_equal_to("")

        charge = "!@#$%"
        add_shipper_insurance_page.set_charge_to_customer(charge)
        assert_that(add_shipper_insurance_page.get_charge_to_customer()).described_as(
            "TL-3372 Charge cost special chars").is_equal_to("")

    @pytest.mark.testcase("TL-3373")
    @pytest.mark.testcase("TL-4763")
    def test_financials_insurance_user_can_enter_only_numerical_to_cost(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        # TL-3373
        charge = "12345"
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(charge)

        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier()).described_as(
            "TL-3372 Charge cost numeric").is_equal_to(charge)

        # TL-4763
        charge = "0.01"
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(charge)

        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier()).described_as(
            "TL-4763 Charge cost decimal").is_equal_to(charge)

        charge = "Qq"
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(charge)
        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier()).described_as(
            "TL-3372 Charge cost text").is_equal_to("")

        charge = "!@#$%"
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(charge)
        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier()).described_as(
            "TL-3372 Charge cost special chars").is_equal_to("")

    @pytest.mark.testcase("TL-3374")
    @pytest.mark.testcase("TL-4765")
    def test_financials_insurance_minimum_value_to_cost(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        # TL-3374

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test provider")
        add_shipper_insurance_page.set_additional_insurance_amount(total_shipment_value)
        color_before = add_shipper_insurance_page.get_cost_to_gtz_or_carrier_border_color()
        add_shipper_insurance_page.click_save()
        color_after = add_shipper_insurance_page.get_cost_to_gtz_or_carrier_border_color()

        with soft_assertions():
            assert_that(color_before).described_as("TL-3374 color changed").is_not_equal_to(color_after)
            assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()).described_as(
                "TL-3374 placeholder").is_equal_to("Required")

        # TL-4765
        cost = "0"
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(cost)
        add_shipper_insurance_page.click_save()

        assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()).described_as(
            "TL-4765 placeholder").is_equal_to("Minimal value 0.01")

        cost = "0.01"
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(cost)
        add_shipper_insurance_page.click_save()

        with soft_assertions():
            assert_that(add_shipper_insurance_page.is_modal_visible()).described_as("TL-3374 modal hidden").is_false()
            assert_that(sales_order_page.is_shipper_insurance_remove_button_visible()).described_as(
                "TL-3374 Shipper insurance is added").is_true()

    @pytest.mark.testcase("TL-3376")
    def test_financials_insurance_charge_to_customer_is_not_required(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        # TL-3376
        cost = "0.01"

        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test provider")
        add_shipper_insurance_page.set_additional_insurance_amount(total_shipment_value)
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(cost)
        add_shipper_insurance_page.click_save()

        with soft_assertions():
            assert_that(add_shipper_insurance_page.is_modal_visible()).described_as("TL-3376 modal hidden").is_false()
            assert_that(sales_order_page.is_shipper_insurance_remove_button_visible()).described_as(
                "TL-3376"
                " Shipper insurance is added").is_true()

    @pytest.mark.testcase("TL-3028")
    def test_financials_insurance_user_can_delete_insurance(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        cost = "100"
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.select_handled_by("GlobalTranz")
        add_shipper_insurance_page.set_insurance_provider("Test provider")
        add_shipper_insurance_page.set_additional_insurance_amount(total_shipment_value)
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(cost)
        add_shipper_insurance_page.click_save()

        # TL-3028
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()

        with soft_assertions():
            assert_that(len(financials_charge_rows)).described_as("TL-3028 theres row with insurance").is_equal_to(2)
            assert_that(financials_page.get_actual_cost()).described_as("TL-3028 actual cost").is_equal_to("$100.00")
            assert_that(financials_page.get_revenue()).described_as("TL-3028 revenue").is_equal_to("$0.00")
            assert_that(financials_page.get_margin()).described_as("TL-3028 margin").is_equal_to("$-100.00")
            assert_that(financials_page.get_margin_percentage()).described_as("TL-3028 margin percentage").is_equal_to(
                "0.00")

        popup = sales_order_page.click_shipper_insurance_remove_button()
        popup.wait_for_popup_visible()
        popup.click_yes()
        financials_charge_rows = financials_page.get_charge_table_rows()

        with soft_assertions():
            assert_that(len(financials_charge_rows)).described_as("TL-3028 row with insurance is removed").is_equal_to(
                1)
            assert_that(financials_page.get_actual_cost()).described_as("TL-3028 actual cost").is_equal_to("$0.00")
            assert_that(financials_page.get_revenue()).described_as("TL-3028 revenue").is_equal_to("$0.00")
            assert_that(financials_page.get_margin()).described_as("TL-3028 margin").is_equal_to("$0.00")
            assert_that(financials_page.get_margin_percentage()).described_as("TL-3028 margin percentage").is_equal_to(
                "0.00")

        financials_charge_rows[0].set_revenue(1)
        financials_page.click_save()

        assert_that(financials_page.is_save_button_visible()).described_as(
            "TL-3028 in view mode").is_false()

    @pytest.mark.testcase("TL-4178")
    @pytest.mark.testcase("TL-4852")
    def test_financials_insurance_required_fields_validation(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        # TL-4178
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        handled_color_before = add_shipper_insurance_page.get_handled_by_border_color()
        provider_color_before = add_shipper_insurance_page.get_insurance_provider_border_color()
        insurance_color_before = add_shipper_insurance_page.get_additional_insurance_amount_border_color()
        cost_color_before = add_shipper_insurance_page.get_cost_to_gtz_or_carrier_border_color()

        add_shipper_insurance_page.click_save()

        handled_color_after = add_shipper_insurance_page.get_handled_by_border_color()
        provider_color_after = add_shipper_insurance_page.get_insurance_provider_border_color()
        insurance_color_after = add_shipper_insurance_page.get_additional_insurance_amount_border_color()
        cost_color_after = add_shipper_insurance_page.get_cost_to_gtz_or_carrier_border_color()

        with soft_assertions():
            assert_that(handled_color_before).described_as("TL-4178 Handled by color").is_not_equal_to(
                handled_color_after)
            assert_that(add_shipper_insurance_page.get_handled_by_placeholder()).described_as(
                "TL-4178 placeholder").is_equal_to("Required")

            assert_that(provider_color_before).described_as("TL-4178 Insurance provider color").is_not_equal_to(
                provider_color_after)
            assert_that(add_shipper_insurance_page.get_insurance_provider_placeholder()).described_as(
                "TL-4178 placeholder").is_equal_to("Required")

            assert_that(insurance_color_before).described_as(
                "TL-4178 Additional Insurance Amount color").is_not_equal_to(insurance_color_after)
            assert_that(add_shipper_insurance_page.get_additional_insurance_amount_placeholder()).described_as(
                "TL-4178 placeholder").is_equal_to("Required")

            assert_that(cost_color_before).described_as("TL-4178 Cost to GTZ or Carrier color").is_not_equal_to(
                cost_color_after)
            assert_that(add_shipper_insurance_page.get_cost_to_gtz_or_carrier_placeholder()).described_as(
                "TL-4178 placeholder").is_equal_to("Required")
