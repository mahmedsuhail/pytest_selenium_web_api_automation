import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_saved_order_popup_page import CreateQuoteSavedOrderPopupPageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestFinancials():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-2807")
    @pytest.mark.testcase("TL-2808")
    @pytest.mark.testcase("TL-2809")
    @pytest.mark.testcase("TL-2927")
    def test_financials_basic(self):
        # TL-2807
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.wait_for_add_charge_button_visible()
        financials_charge_rows = financials_page.get_charge_table_rows()

        with soft_assertions():
            assert_that(len(financials_charge_rows)).described_as("TL-2807 Basic cost line visible").is_equal_to(1)
            assert_that(financials_page.is_add_charge_button_visible()).described_as(
                "TL-2807 Add charge button visibility").is_true()

        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[1].set_cost(100)
        financials_charge_rows[1].set_revenue(200)
        financials_charge_rows[1].select_description(value="Discount")

        with soft_assertions():
            # TL-2808
            assert_that(financials_page.is_charge_header_description_visible()).described_as(
                "TL-2808 Description header").is_true()
            assert_that(financials_page.is_charge_header_cost_visible()).described_as(
                "TL-2808 Cost header").is_true()
            assert_that(financials_page.is_charge_header_revenue_visible()).described_as(
                "TL-2808 Revenue header").is_true()
            # TL-2809
            assert_that(financials_charge_rows[1].get_cost()).described_as("TL-2809 Cost input").is_equal_to("100.00")
            assert_that(financials_charge_rows[1].get_revenue()).described_as("TL-2809 Revenue input").is_equal_to(
                "200.00")
            # TL-2927
            assert_that(financials_charge_rows[1].get_description()).described_as("TL-2927 Selected description") \
                .is_equal_to("Discount")

    @pytest.mark.testcase("TL-2293")
    @pytest.mark.testcase("TL-2693")
    @pytest.mark.testcase("TL-2703")
    @pytest.mark.testcase("TL-2670")
    @pytest.mark.testcase("TL-2694")
    @pytest.mark.testcase("TL-2702")
    def test_financials_inputs_validation(self):
        # Test data
        max_buy_positive, max_buy_positive_to_verify, max_buy_negative, max_buy_negative_to_verify = \
            "2000", "2,000.00", "-100", "100.00"
        target_cost_positive, target_cost_positive_to_verify, target_cost_negative, target_cost_negative_to_verify = \
            "2001", "2,001.00", "-101", "101.00"
        price_to_beat_positive, price_to_beat_positive_to_verify, price_to_beat_negative,\
            price_to_beat_negative_to_verify = "2002", "2,002.00", "-102", "102.00"

        # TEST
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.wait_for_add_charge_button_visible()

        with soft_assertions():
            assert_that(financials_page.get_max_buy()).described_as("TL-2293 Max Buy").is_equal_to("0.00")
            assert_that(financials_page.get_target_cost()).described_as("TL-2693 Target Cost").is_equal_to("0.00")
            assert_that(financials_page.get_price_to_beat()).described_as("TL-2703 Price to Beat").is_equal_to("0.00")

        financials_page.set_max_buy(max_buy_positive)
        financials_page.set_target_cost(target_cost_positive)
        financials_page.set_price_to_beat(price_to_beat_positive)
        financials_page.click_add_charge()

        with soft_assertions():
            assert_that(financials_page.get_max_buy()).described_as("TL-2293 Max Buy set proper").is_equal_to(
                max_buy_positive_to_verify)
            assert_that(financials_page.get_target_cost()).described_as("TL-2693 Target Cost set proper").is_equal_to(
                target_cost_positive_to_verify)
            assert_that(financials_page.get_price_to_beat()).described_as("TL-2703 Target Price to Beat proper") \
                .is_equal_to(price_to_beat_positive_to_verify)

        # validate negative cases

        financials_page.set_max_buy(max_buy_negative)
        financials_page.set_target_cost(target_cost_negative)
        financials_page.set_price_to_beat(price_to_beat_negative)
        financials_page.click_add_charge()

        with soft_assertions():
            assert_that(financials_page.get_max_buy()).described_as("TL-2670 Max Buy set wrong").is_equal_to(
                max_buy_negative_to_verify)
            assert_that(financials_page.get_target_cost()).described_as("TL-2694 Target Cost set wrong").is_equal_to(
                target_cost_negative_to_verify)
            assert_that(financials_page.get_price_to_beat()).described_as("TL-2702 Price to Beat set wrong") \
                .is_equal_to(price_to_beat_negative_to_verify)

    @pytest.mark.testcase("TL-3035")
    @pytest.mark.testcase("TL-3038")
    @pytest.mark.testcase("TL-3039")
    @pytest.mark.testcase("TL-3040")
    def test_financials_edit_base_rate_row(self):
        # TL-2932
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.wait_for_add_charge_button_visible()
        financials_charge_rows = financials_page.get_charge_table_rows()
        description_editable = financials_charge_rows[0].is_description_editable()

        with soft_assertions():
            # TL-3035
            assert_that(financials_charge_rows[0].get_description()).described_as("TL-3035 Description").is_equal_to(
                "Base Rate")
            assert_that(financials_charge_rows[0].get_cost()).described_as("TL-3035 Cost").is_equal_to(
                "0.00")
            assert_that(financials_charge_rows[0].get_revenue()).described_as("TL-3035 Cost").is_equal_to(
                "0.00")

        financials_charge_rows[0].set_cost(100)
        financials_charge_rows[0].set_revenue(200)
        financials_page.click_add_charge()  # click to let app to format value
        financials_charge_rows = financials_page.get_charge_table_rows()

        with soft_assertions():
            # TL-3038
            assert_that(description_editable).described_as("TL-3038 Description not editable for Base Rate").is_false()
            # TL-3039
            assert_that(financials_charge_rows[0].is_remove_button_exist()).described_as(
                "TL-3039 Remove button not exist for Base Rate row").is_false()
            # TL-3040
            assert_that(financials_charge_rows[0].get_cost()).described_as("TL-3040 Cost input").is_equal_to("100.00")
            assert_that(financials_charge_rows[0].get_revenue()).described_as("TL-3040 Revenue input").is_equal_to(
                "200.00")

    @pytest.mark.testcase("TL-2932")
    def test_financials_user_cannot_save_line_with_missing_descriptor(self):
        # TL-2932
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        color_before = financials_charge_rows[1].get_description_color()

        assert_that(financials_charge_rows[1].get_description()).is_equal_to("Select...")
        financials_page.click_save()
        color_after = financials_charge_rows[1].get_description_color()

        with soft_assertions():
            assert_that(color_before).described_as(
                "TL-2932 Placeholder color is different to show that field is necessary to be filled").is_not_equal_to(
                color_after)
            assert_that(financials_page.is_add_charge_button_visible()).described_as("TL-2932 is still editable") \
                .is_true()

    @pytest.mark.testcase("TL-2878")
    @pytest.mark.testcase("TL-2888")
    def test_financials_user_cannot_add_letter_can_set_minus_to_discount(self):
        # TL-2878
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()

        financials_charge_rows[1].set_cost(value="Q", clear=False)
        assert_that(financials_charge_rows[1].get_cost()).described_as("TL-2878 Cost input not changed") \
            .is_equal_to("0.00")
        financials_charge_rows[1].set_cost(value="@", clear=False)
        assert_that(financials_charge_rows[1].get_cost()).described_as("TL-2878 Cost input not changed") \
            .is_equal_to("0.00")

        financials_charge_rows[1].set_revenue(value="Q", clear=False)
        assert_that(financials_charge_rows[1].get_revenue()) \
            .described_as("TL-2878 Revenue input not changed").is_equal_to("0.00")
        financials_charge_rows[1].set_revenue(value="@", clear=False)
        assert_that(financials_charge_rows[1].get_revenue()) \
            .described_as("TL-2878 Revenue input not changed").is_equal_to("0.00")

        # TL-2888
        financials_charge_rows[1].select_description(value="Discount")
        financials_charge_rows[1].set_cost(value="-", clear=False)
        assert_that(financials_charge_rows[1].get_cost()).described_as("TL-2888 Revenue input not changed") \
            .is_equal_to("0.00-")

        financials_charge_rows[1].set_revenue(value="-", clear=False)
        assert_that(financials_charge_rows[1].get_revenue()) \
            .described_as("TL-2888 Revenue input not changed").is_equal_to("0.00-")

    @pytest.mark.testcase("TL-2917")
    @pytest.mark.slowtest
    def test_financials_user_cannot_add_minus_to_any_different_than_discount(self):
        # TL-2878
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()
        descriptions = financials_charge_rows[1].get_all_descriptions()

        with soft_assertions():
            for x in descriptions:
                if x != "Discount":
                    financials_charge_rows[1].clear_filter()
                    financials_charge_rows[1].select_description_quick(value=x)
                    financials_charge_rows[1].set_cost(value="-", clear=False)
                    assert_that(financials_charge_rows[1].get_cost()).described_as("TL-2917 Revenue input not changed") \
                        .is_equal_to("0.00")

                    financials_charge_rows[1].set_revenue(value="-", clear=False)
                    assert_that(financials_charge_rows[1].get_revenue()) \
                        .described_as("TL-2917 Revenue input not changed").is_equal_to("0.00")

    @pytest.mark.testcase("TL-2931")
    @pytest.mark.testcase("TL-2934")
    @pytest.mark.testcase("TL-2935")
    def test_financials_user_can_save_line_by_click_confirm_close(self):
        # TL-2931
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_revenue("1")

        financials_charge_rows[1].select_description_quick("Chassis Charge")
        financials_charge_rows[1].set_cost("100")
        financials_charge_rows[1].set_revenue("200")

        financials_page.click_save()

        with soft_assertions():
            assert_that(financials_page.is_save_button_visible()) \
                .described_as("TL-2931 Confirm and close button is invisible").is_false()
            assert_that(financials_page.is_add_charge_button_visible()) \
                .described_as("TL-2931 Add charge button is invisible").is_false()

        # TL-2934
        financials_page.enable()
        financials_page.wait_for_add_charge_button_visible()
        financials_charge_rows = financials_page.get_charge_table_rows()
        with soft_assertions():
            assert_that(financials_charge_rows[1].get_cost()).described_as("TL-2934 Cost input not changed") \
                .is_equal_to("100.00")
            assert_that(financials_charge_rows[1].get_revenue()) \
                .described_as("TL-2934 Revenue input not changed").is_equal_to("200.00")

        # TL-2935
        financials_charge_rows[1].click_remove()
        financials_page.wait_for_add_charge_button_visible()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)) \
            .described_as("TL-2935 After remove, only one, default, charge row is available").is_equal_to(1)

    @pytest.mark.testcase("TL-3030")
    @pytest.mark.testcase("TL-3028")
    def test_financials_line_added_when_add_shippers_insurance_to_sales_order(self):
        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        total_shipment_value = sales_order_page.get_total_shipment_value()

        cost = "20.00"
        add_shipper_insurance_page = sales_order_page.click_and_open_add_shipper_insurance()
        add_shipper_insurance_page.select_handled_by("Carrier Direct")
        add_shipper_insurance_page.set_insurance_provider("Test provider")
        add_shipper_insurance_page.set_additional_insurance_amount(total_shipment_value)
        add_shipper_insurance_page.set_cost_to_gtz_or_carrier(cost)
        add_shipper_insurance_page.click_save()

        # TL-3030
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()

        with soft_assertions():
            assert_that(len(financials_charge_rows)).described_as("TL-3028 theres row with insurance").is_equal_to(2)
            assert_that(financials_page.get_actual_cost()).described_as("TL-3028 actual cost").is_equal_to("$20.00")
            assert_that(financials_page.get_revenue()).described_as("TL-3028 revenue").is_equal_to("$0.00")
            assert_that(financials_charge_rows[1].get_description()).described_as(
                "TL-3030 financials contains row with Insurance").is_equal_to("Carrier Direct-Cargo Insurance")
            assert_that(financials_charge_rows[1].get_cost()).is_equal_to(cost)

        # TL-3028
        financials_charge_rows[0].set_revenue("1")
        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[1].click_remove()
        financials_charge_rows = financials_page.get_charge_table_rows()

        with soft_assertions():
            assert_that(len(financials_charge_rows)).described_as(
                "TL-3028 Insurance row removed from financials").is_equal_to(1)

        financials_page.click_save()
        with soft_assertions():
            assert_that(financials_page.is_save_button_visible()) \
                .described_as("TL-3028 Confirm and close button is invisible").is_false()
            assert_that(financials_page.is_add_charge_button_visible()) \
                .described_as("TL-3028 Add charge button is invisible").is_false()

    @pytest.mark.testcase("TL-3484")
    def test_financials_can_set_values_with_dot(self):
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[1].set_cost(value="1.01", clear=True)
        financials_charge_rows[1].set_revenue(value="222.05", clear=True)
        financials_page.click_add_charge()

        # TL-3484
        with soft_assertions():
            assert_that(financials_charge_rows[1].get_cost()).described_as("TL-3484 Cost input not changed") \
                .is_equal_to("1.01")
            assert_that(financials_charge_rows[1].get_revenue()).described_as("TL-3484 Revenue input not changed") \
                .is_equal_to("222.05")

    @pytest.mark.testcase("TL-3043")
    @pytest.mark.testcase("TL-3061")
    def test_financials_cannot_create_order_until_revenue_set_in_base_rate(self):
        # preconditions
        self.test_data = read_json_file("tl_create_quote_origin_destintation.json")
        selected_company_origin = self.test_data["origin_data2"]["company"]
        selected_equipment_type = "Van"
        selected_equipment_length = "53"
        selected_shipment_type = "Spot Quote"
        self.qop.select_equipment_type(selected_equipment_type)
        self.qop.select_shipment_type(selected_shipment_type)
        self.qop.select_equipment_length(selected_equipment_length)

        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        self._rl_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self._rl_origin_page.select_by_company(selected_company_origin)
        self._rl_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)
        self._rl_destination_page.select_by_company(selected_company_origin)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)

        financials_page = FinancialsPage(self.driver, self.log)
        header_color_before = financials_page.get_container_color()

        self.qop.click_create_order_footer_button()

        # TL-3043
        header_color_after = financials_page.get_container_color()

        assert_that(header_color_after).described_as("TL-3043 Header color changed").is_not_equal_to(
            header_color_before)

        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_revenue("0.01")
        financials_page.click_save()

        assert_that(financials_page.is_save_button_visible()).described_as(
            "TL-3043, TL-3061 Financials is in view mode").is_false()

        self.qop.click_create_order_footer_button()
        create_order_popup = CreateQuoteSavedOrderPopupPageTL(self.driver, self.log)
        create_order_popup.wait_for_create_order_popup_visible()

        assert_that(create_order_popup.is_popup_displayed).described_as("TL-3043, TL-3061 Order created").is_true()

    @pytest.mark.testcase("TL-2761")
    def test_financials_cost_prepopulated_based_on_items(self):
        # steps 1-3
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2761 new line is rendered").is_equal_to(2)

        # step 4
        financials_charge_rows[1].select_description_quick("BOL Correction")
        financials_charge_rows[1].set_cost("1000")
        financials_charge_rows[1].set_revenue("1100")
        assert_that(financials_page.get_actual_cost()).described_as("TL-2761 step 4").is_equal_to("$1,000.00")

        # steps 5,6
        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2761 new line is rendered").is_equal_to(3)

        financials_charge_rows[2].select_description_quick("Bobtail Fee")
        financials_charge_rows[2].set_cost("50")
        financials_charge_rows[2].set_revenue("110")
        assert_that(financials_page.get_actual_cost()).described_as("TL-2761 step 6").is_equal_to("$1,050.00")

        # step 7
        financials_charge_rows[1].click_remove()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(financials_page.get_actual_cost()).described_as("TL-2761 step 7").is_equal_to("$50.00")

        # step 8
        financials_charge_rows[1].set_cost("150")
        financials_charge_rows[1].set_revenue("110")
        assert_that(financials_page.get_actual_cost()).described_as("TL-2761 step 8").is_equal_to("$150.00")

    @pytest.mark.testcase("TL-2789")
    def test_financials_revenue_prepopulated_based_on_items(self):
        # steps 1,2
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2789 base line is rendered").is_equal_to(1)

        # step 3
        financials_charge_rows[0].set_revenue("100")
        assert_that(financials_page.get_revenue()).described_as("TL-2789 step 4").is_equal_to("$100.00")

        # steps 4,5
        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2789 new line is rendered").is_equal_to(2)

        financials_charge_rows[1].select_description_quick("Bobtail Fee")
        financials_charge_rows[1].set_cost("50")
        financials_charge_rows[1].set_revenue("100")
        assert_that(financials_page.get_revenue()).described_as("TL-2789 step 5").is_equal_to("$200.00")

        # step 7
        financials_charge_rows[1].click_remove()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(financials_page.get_revenue()).described_as("TL-2789 step 7").is_equal_to("$100.00")

    @pytest.mark.testcase("TL-2796")
    @pytest.mark.testcase("TL-2797")
    def test_financials_margin_calculated_based_on_items(self):
        # steps 1-3
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2789 base line is rendered").is_equal_to(2)

        # steps 4,5
        financials_charge_rows[1].select_description_quick("BOL Correction")
        financials_charge_rows[1].set_cost("1000")
        financials_charge_rows[1].set_revenue("1100")

        assert_that(financials_page.get_margin()).described_as("TL-2796 step 5").is_equal_to("$100.00")
        assert_that(financials_page.get_margin_percentage()).described_as("TL-2797 step 5").is_equal_to("9.09")

    @pytest.mark.testcase("TL-2936")
    def test_financials_data_change_when_rows_edit(self):
        # preconditions
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_cost("100")
        financials_charge_rows[0].set_revenue("200")

        financials_charge_rows[1].select_description_quick("BOL")
        financials_charge_rows[1].set_cost("50")
        financials_charge_rows[1].set_revenue("100")
        financials_page.click_save()

        financials_page.enable()

        # TL-2936 step 2
        self.tl_2936_assertion_helper(financials_page, "$150.00", "$300.00", "$150.00", "50.00")

        # step 3
        financials_page.click_add_charge()
        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[2].select_description_quick("Chassis Charge")
        financials_charge_rows[2].set_cost("130")
        financials_charge_rows[2].set_revenue("250")
        # step 4
        self.tl_2936_assertion_helper(financials_page, "$280.00", "$550.00", "$270.00", "49.09")

        # step 5
        financials_page.click_close()
        self.tl_2936_assertion_helper(financials_page, "$150.00", "$300.00", "$150.00", "50.00")

        # step 6
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2936 only 2 rows").is_equal_to(2)

        # step 7
        financials_charge_rows[1].click_remove()
        financials_charge_rows = financials_page.get_charge_table_rows()
        self.tl_2936_assertion_helper(financials_page, "$100.00", "$200.00", "$100.00", "50.00")

        # step 8
        financials_page.click_close()
        self.tl_2936_assertion_helper(financials_page, "$150.00", "$300.00", "$150.00", "50.00")

        # step 9
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2936 only 2 rows").is_equal_to(2)

        # step 10
        financials_charge_rows[1].select_description_quick("Chassis Charge", True)
        financials_charge_rows[1].set_cost("130")
        financials_charge_rows[1].set_revenue("250")
        self.tl_2936_assertion_helper(financials_page, "$230.00", "$450.00", "$220.00", "48.89")

        # step 11
        financials_page.click_close()
        self.tl_2936_assertion_helper(financials_page, "$150.00", "$300.00", "$150.00", "50.00")

        # step 12
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()
        assert_that(len(financials_charge_rows)).described_as("TL-2936 only 2 rows").is_equal_to(2)

        # step 13
        financials_page.set_max_buy("222")
        financials_page.set_target_cost("333")
        financials_page.set_price_to_beat("444")
        financials_page.click_add_charge()
        with soft_assertions():
            assert_that(financials_page.get_max_buy()).described_as("TL-2936 max buy").is_equal_to("222.00")
            assert_that(financials_page.get_target_cost()).described_as("TL-2936 target cost").is_equal_to("333.00")
            assert_that(financials_page.get_price_to_beat()).described_as("TL-2936 price to beat").is_equal_to("444.00")

        # step 14
        financials_page.click_close()
        with soft_assertions():
            assert_that(financials_page.get_max_buy()).described_as("TL-2936 max buy").is_equal_to("0.00")
            assert_that(financials_page.get_target_cost()).described_as("TL-2936 target cost").is_equal_to("0.00")
            assert_that(financials_page.get_price_to_beat()).described_as("TL-2936 price to beat").is_equal_to("0.00")

    @pytest.mark.testcase("TL-5208")
    @pytest.mark.testcase("TL-5210")
    def test_financials_user_narrow_line_select_by_search(self):
        # preconditions
        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()

        values_one = financials_charge_rows[1].get_all_descriptions_with_search("B", True)
        values_two = financials_charge_rows[1].get_all_descriptions_with_search("BO", True)
        values_three = financials_charge_rows[1].get_all_descriptions_with_search("BOL", True)

        with soft_assertions():
            for value in values_one:
                assert_that(value.lower()).described_as("TL-5208").contains("b")
            for value in values_two:
                assert_that(value.lower()).described_as("TL-5208").contains("bo")
            for value in values_three:
                assert_that(value.lower()).described_as("TL-5208").contains("bol")

        values_empty = financials_charge_rows[1].get_all_descriptions_with_search("BOLASDFGH", True)
        assert_that(len(values_empty)).described_as("TL-5210").is_equal_to(0)

    def tl_2936_assertion_helper(self, financials_page, actual_cost, revenue, margin, margin_percentage):
        with soft_assertions():
            assert_that(financials_page.get_actual_cost()).described_as("TL-2936 actual cost").is_equal_to(actual_cost)
            assert_that(financials_page.get_revenue()).described_as("TL-2936 revenue").is_equal_to(revenue)
            assert_that(financials_page.get_margin()).described_as("TL-2936 margin").is_equal_to(margin)
            assert_that(financials_page.get_margin_percentage()).described_as("TL-2936 margin percentage").is_equal_to(
                margin_percentage)
