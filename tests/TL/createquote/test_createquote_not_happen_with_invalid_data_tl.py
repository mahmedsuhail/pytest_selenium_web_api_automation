import datetime

import pytest
import unittest2
from assertpy import assert_that

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_saved_popup_page_tl import CreateQuoteSavedPopupPageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.customer_builder import CustomerBuilder
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from pages.common.createquote_page import CreateQuotePage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUp", "login")
class CreateQuoteWillNotHappenWithInvalidDataTL(unittest2.TestCase):
    test_data = read_json_file("tl_create_quote_smoke_test_data.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)
    util = Util(logger=log)

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.create_quote_page_tl = CreateQuotePageTL(self.driver, self.log)
        self.create_quote_saved_popup_tl = CreateQuoteSavedPopupPageTL(self.driver, self.log)
        self.route_details_page_tl = TLRouteDetailsPage(self.driver, self.log)
        self.shipping_items_page_tl = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.create_quote_sales_order_page_tl = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        self.test_status = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUp")
    def login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        create_quote_page = CreateQuotePage(self.driver, self.log)
        create_quote_page.click_quote_order_icon()

    @pytest.mark.testcase("TL-2134")
    @pytest.mark.testcase("TL-6286")
    @pytest.mark.testcase("TL-6288")
    @pytest.mark.jira("TL-8341")
    def test_createquote_not_happen_with_invalid_data_tl(self):

        # TEST DATA
        origin = self.test_data.get("origin")
        origin_short = self.test_data.get("originShort")
        destination = self.test_data.get("destination")
        destination_short = self.test_data.get("destinationShort")
        equipment_type_to_select = self.test_data.get("selectedEquipmentType")
        shipment_type_to_select = self.test_data.get("selectedShipmentType")

        customer_data = CustomerBuilder() \
            .with_full_name(self.test_data["fullCustomerName"]) \
            .with_short_name(self.test_data["shortCustomerName"]) \
            .with_contact(self.test_data["customerContact"]) \
            .build()

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.test_data["shippingItem"].get("commodityDescription")) \
            .with_piece_count(self.test_data["shippingItem"].get("pieceCount")) \
            .with_weight(self.test_data["shippingItem"].get("weight")) \
            .with_pallet_count(self.test_data["shippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        customer_search_string = customer_data.full_name[:3]

        assert self.create_quote_page_tl.get_create_quote_container_visibility(), \
            "Create Quote container should be visible"

        # Select Customer
        self._select_customer_and_verify(search_string=customer_search_string, customer_data=customer_data)

        # Select service type as TL
        self.select_tl()

        date_to_select_str = self.util.getDateValueWithoutHolidayAndWeekends(no_of_days=2)
        date_to_select = datetime.datetime.strptime(date_to_select_str, '%m/%d/%Y')

        # Select data in Route Details Sections
        self.select_origin_and_verify(origin=origin, origin_short=origin_short, date_to_select=date_to_select)
        self.select_destination_and_verify(destination=destination, destination_short=destination_short,
                                           date_to_select=date_to_select)
        self.test_status.mark(not self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be disabled")

        # Enter shipping data
        self.enter_shipping_item_and_verify(shipping_item=shipping_item)
        self.test_status.mark(self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be enabled")

        # Click on the Create Quote Button
        self.create_quote_page_tl.click_create_quote_footer_button()
        # Required fields Validations
        self.test_status.mark(self.create_quote_page_tl.verify_equipment_type_field_value("Required"),
                              "Equipment type field is not selected")
        self.test_status.mark(self.create_quote_page_tl.verify_shipment_type_field_value("Required"),
                              "Shipment type field is not selected")
        self.test_status.mark(self.create_quote_page_tl.is_create_quote_container_header_highlighted(),
                              "Create Quote container Header colour changed to orange")

        # Select Equipment Type from drop down list
        self.create_quote_page_tl.select_equipment_type(equipment_type=equipment_type_to_select)
        self.create_quote_page_tl.select_shipment_type(shipment_type=shipment_type_to_select)
        self.test_status.mark(self.create_quote_page_tl.equipment_length_field_is_displayed(),
                              "Equipment length field should be displayed in Create Quote "
                              "section after selecting Equipment type")

        # Click on the Create Quote Button
        self.create_quote_page_tl.click_create_quote_footer_button()
        # Required fields Validations After Selecting Equipment Type and clicking Create Quote Button Again
        self.test_status.mark(self.create_quote_page_tl.verify_equipment_type_field_value(equipment_type_to_select),
                              "Equipment type field is selected")
        self.test_status.mark(self.create_quote_page_tl.verify_equipment_length_field_value("Required"),
                              "Equipment type length field is not selected")
        self.test_status.mark(self.create_quote_page_tl.verify_shipment_type_field_value(shipment_type_to_select),
                              "Shipment type field is selected")
        self.test_status.mark(self.create_quote_page_tl.is_create_quote_container_header_highlighted(),
                              "Create Quote container Header colour changed to orange")

        self.test_status.mark(self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be enabled when necessary fields are entered")

        # Line item removed from the sales order container and validated Create Quote Button Behaviour
        so_items = self.create_quote_sales_order_page_tl.get_sale_item_rows()
        so_items[0].click_remove_item()
        self.test_status.mark(not self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be disabled after removing line item")

        # Enter shipping data
        self.enter_shipping_item_and_verify(shipping_item=shipping_item)
        self.test_status.mark(self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be enabled after adding line item")

        # Total Shipment value Removal and validated Create Quote Button Behaviour
        self.create_quote_sales_order_page_tl.clear_total_shipment_value()
        self.create_quote_sales_order_page_tl.enter_total_shipment_value(0)
        self.test_status.mark(not self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be disabled when the total shipment value is zero")

        self.test_status.mark_final("test_createquote_not_happen_with_invalid_data_tl", True,
                                    "Validated quote creation will not happen if atleast one field is not valid")

    def _select_customer_and_verify(self, search_string, customer_data):

        selected_customer = self.create_quote_page_tl.search_and_select_customer_by_name(
            search_string=search_string, customer_name=customer_data.full_name)

        assert selected_customer.full_name == customer_data.full_name
        assert selected_customer.contact == customer_data.contact

        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_customer_search_box_value(),
                                                                customer_data.short_name),
                              "Customer name does not match in customer search box")

    def select_tl(self):

        self.create_quote_page_tl.select_service_type(service_type=ServiceType.TL)
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        assert self.create_quote_page_tl.verify_quote_page_is_enabled_tl(), \
            "After selecting TL service type, create quote page should re-render"

    def select_origin_and_verify(self, origin, origin_short, date_to_select):
        self.route_details_page_tl.search_and_select_origin(search_string=origin[:3], expected_result=origin)

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_address_text(), origin_short),
            "Origin address did not match")

        self.route_details_page_tl.set_origin_pickup_date(date_to_set=date_to_select)
        self.route_details_page_tl.set_origin_open_time("08:00")
        self.route_details_page_tl.set_origin_close_time("10:00")

    def select_destination_and_verify(self, destination, destination_short, date_to_select):
        self.route_details_page_tl.search_and_select_destination(search_string=destination[:3],
                                                                 expected_result=destination)
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_address_text(),
                                              destination_short), "Destination address did not match")

        self.route_details_page_tl.set_destination_pickup_date(date_to_set=date_to_select)
        self.route_details_page_tl.set_destination_open_time("18:00")
        self.route_details_page_tl.set_destination_close_time("20:00")

    def enter_shipping_item_and_verify(self, shipping_item):
        assert_that(self.shipping_items_page_tl.is_page_displayed())\
            .is_equal_to(True), "Shipping items container should be displayed"

        self.test_status.mark(not self.shipping_items_page_tl.get_hazmat_checkbox_checked_status(),
                              "Hazmat checkbox should be unchecked by default")
        self.test_status.mark(not self.shipping_items_page_tl.get_stackable_checkbox_checked_status(),
                              "Stackable checkbox should be unchecked by default")

        self.shipping_items_page_tl.enter_shipping_item_data(shipping_item=shipping_item)
        added_shipping_item = self.shipping_items_page_tl.get_shipping_item_from_page()

        assert_that(added_shipping_item).is_equal_to(shipping_item), "Shipping item data was not populated correctly"

        self.shipping_items_page_tl.click_add_to_order_button()

        total_shipment_value = self.test_data.get("expectedTotalShipmentValue")

        so_items = self.create_quote_sales_order_page_tl.get_sale_item_rows()

        self.test_status.mark(
            self.validators.verify_text_match(so_items[0].get_commodity_text(), shipping_item.commodity_description),
            "Shipping item is not shown in Sales Order Page")

        self.test_status.mark(
            self.validators.verify_text_match(self.create_quote_sales_order_page_tl.get_total_shipment_value(),
                                              total_shipment_value),
            "Total Shipment value does not match")
