import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestSalesOrderShippingItems2():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()
        self.set_preconditions()

    def set_preconditions(self):
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()

        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_cost(10)
        financials_charge_rows[0].set_revenue(120)

        financials_page.click_save()

    @pytest.mark.testcase("TL-1375")
    def test_so_user_can_save_changes_in_line_when_all_required_fields_are_filled(self):
        # preconditions
        row_id = 3
        shipping_item_first = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .with_hazmat() \
            .with_hazmat_group(self.shipping_items_data[row_id]["HazmatGroup"]) \
            .with_hazmat_class(self.shipping_items_data[row_id]["HazmatClass"]) \
            .with_hazmat_prefix(self.shipping_items_data[row_id]["prefix"]) \
            .with_hazmat_code(self.shipping_items_data[row_id]["HazmatCode"]) \
            .with_hazmat_emergency_contact(self.shipping_items_data[row_id]["emergencyContact"]) \
            .build()

        row_id = 4
        shipping_item_second = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_first)
        shipping_items_page.click_add_to_order_button()

        # test
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 10)
        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-1375 Verify Sales Order item is added").is_equal_to(1)

        so_items[0].click_edit_item()
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_second, clear=True)
        shipping_items_page.click_add_to_order_button()

        so_items = sales_order_page.get_sale_item_rows()

        shipping_items_after = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after).is_equal_to(shipping_item_first.get_clear_template())
        so_items[0].click_edit_item()

        shipping_items_after_edit = shipping_items_page.get_shipping_item_from_page()
        with soft_assertions():
            assert_that(shipping_items_after_edit.commodity_description).described_as(
                "TL-1375 Shipping items fields are the same from second sales order item").is_equal_to(
                shipping_item_second.commodity_description)
            assert_that(shipping_items_after_edit.piece_count).described_as(
                "TL-1375 Shipping items fields are the same from second sales order item").is_equal_to(
                shipping_item_second.piece_count)
            assert_that(shipping_items_after_edit.pallet_count).described_as(
                "TL-1375 Shipping items fields are the same from second sales order item").is_equal_to(
                shipping_item_second.pallet_count)

    @pytest.mark.testcase("TL-1377")
    def test_so_user_cannot_save_changes_in_edited_item_when_required_field_is_empty(self):
        # preconditions
        row_id = 3
        shipping_item_first = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .with_hazmat() \
            .with_hazmat_group(self.shipping_items_data[row_id]["HazmatGroup"]) \
            .with_hazmat_class(self.shipping_items_data[row_id]["HazmatClass"]) \
            .with_hazmat_prefix(self.shipping_items_data[row_id]["prefix"]) \
            .with_hazmat_code(self.shipping_items_data[row_id]["HazmatCode"]) \
            .with_hazmat_emergency_contact(self.shipping_items_data[row_id]["emergencyContact"]) \
            .build()

        row_id = 4
        shipping_item_second = ShippingItemBuilder() \
            .with_commodity_description("true") \
            .with_piece_count("true") \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_first)
        shipping_items_page.click_add_to_order_button()

        # test
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 10)
        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-1377 Verify Sales Order item is added").is_equal_to(1)

        so_items[0].click_edit_item()
        container_color_before = shipping_items_page.get_container_header_color()
        shipping_items_page.clear_shipping_item_data(shipping_item=shipping_item_second)
        shipping_items_page.click_add_to_order_button()

        container_color_after = shipping_items_page.get_container_header_color()
        with soft_assertions():
            assert_that(container_color_before).described_as("TL-1377 header color changed") \
                .is_not_equal_to(container_color_after)
            assert_that(shipping_items_page.is_add_to_order_button_visible()).described_as(
                "TL-1377 Save and Confirm button still visible").is_true()

    @pytest.mark.testcase("TL-1378")
    def test_so_changes_not_saved_when_user_click_cancel_on_edited_item(self):
        # preconditions
        row_id = 0
        shipping_item_first = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 4
        shipping_item_second = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_first)
        shipping_items_page.click_add_to_order_button()

        # test
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 10)
        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-1378 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-1378 Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("TL-1378 Total density").is_equal_to(
                "110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-1378 Total weight").is_equal_to("202.00")

        so_items[0].click_edit_item()
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_second)
        shipping_items_page.click_cancel_button()

        so_items = sales_order_page.get_sale_item_rows()
        shipping_items_after = shipping_items_page.get_shipping_item_from_page()

        with soft_assertions():
            assert_that(shipping_items_after).is_equal_to(shipping_item_first.get_clear_template())
            assert_that(len(so_items)).described_as("TL-1378 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-1378 Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("TL-1378 Total density").is_equal_to(
                "110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-1378 Total weight").is_equal_to("202.00")
            assert_that(so_items[0].is_edit_item_button_visible()).described_as(
                "TL-1378 edit item button is visible").is_true()

    @pytest.mark.testcase("TL-1386")
    def test_so_scroll_appears_after_adding_4th_item(self):
        # preconditions
        row_id = 0
        shipping_item_first = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 1
        shipping_item_second = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 2
        shipping_item_third = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 3
        shipping_item_fourth = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)

        # 1
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_first)
        shipping_items_page.click_add_to_order_button()
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 10)

        # 2
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_second)
        shipping_items_page.click_add_to_order_button()
        sales_order_page.wait_for_sale_item_have_count_and_return(2, 10)

        # 3
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_third)
        shipping_items_page.click_add_to_order_button()
        sales_order_page.wait_for_sale_item_have_count_and_return(3, 10)

        # test
        assert_that(sales_order_page.is_scroll_visible_in_items_list()).described_as(
            "TL-1386 scroll is not visible when 3 items").is_false()

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_fourth)
        shipping_items_page.click_add_to_order_button()
        sales_order_page.wait_for_sale_item_have_count_and_return(4, 10)

        assert_that(sales_order_page.is_scroll_visible_in_items_list()).described_as(
            "TL-1386 scroll is visible when 4 or more items").is_true()

        so_items = sales_order_page.get_sale_item_rows()
        so_items[3].click_remove_item()

        assert_that(sales_order_page.is_scroll_visible_in_items_list()).described_as(
            "TL-1386 scroll is not visible when again 3 items").is_false()

    @pytest.mark.testcase("TL-1332")
    @pytest.mark.testcase("TL-1343")
    @pytest.mark.testcase("TL-1346")
    @pytest.mark.testcase("TL-4066")
    def test_so_calculator_recalculated_on_add_remove_item(self):
        # preconditions
        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_items = sales_order_page.get_sale_item_rows()
        shipping_items_after_add = shipping_items_page.get_shipping_item_from_page()

        with soft_assertions():
            assert_that(shipping_items_after_add).is_equal_to(shipping_item.get_clear_template())
            assert_that(len(so_items)).described_as(
                "TL-1332, TL-1343, TL-4066 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4066 Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4066 Total density").is_equal_to(
                "110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4066 Total weight").is_equal_to("202.00")

        row_id = 1
        shipping_item_to_edit = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        # add second shipping item
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_to_edit)
        shipping_items_page.click_add_to_order_button()
        so_items_second = sales_order_page.wait_for_sale_item_have_count_and_return(2, 30)

        with soft_assertions():
            assert_that(len(so_items_second)).described_as(
                "TL-1343, TL-4066 Verify Sales Order items count").is_equal_to(2)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4066 Total value").is_equal_to("1.90")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4066 Total density").is_equal_to(
                "265.85")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4066 Total weight").is_equal_to("504.00")

        # remove first item

        so_items_second[0].click_remove_item()
        so_items_third = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items_third)).described_as(
                "TL-1346, TL-4066 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4066 Total value").is_equal_to("0.06")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4066 Total density").is_equal_to(
                "4832.00")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4066 Total weight").is_equal_to("302.00")

        so_items_third[0].click_remove_item()
        so_items_fourth = sales_order_page.get_sale_item_rows()
        assert_that(len(so_items_fourth)).described_as("TL-1346 Verify Sales Order items count").is_equal_to(0)
