import re

import pytest

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.common.builders.note_builder import NoteBuilder
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.tl_notes_page import TlNotesPage
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from utilities.validators import Validators
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestNotes:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_testing_data.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    note_types_data = read_csv_data("tl_note_types.csv")
    log = cl.test_logger(filename=__qualname__)
    util = Util(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(driver=self.driver, log=self.log)
        self.shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.notes_page = TlNotesPage(self.driver, self.log)
        self.ts = ReportTestStatus(driver=self.driver, log=self.log)
        self.validator = Validators(logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")
        qop = CreateQuotePageTL(self.driver, self.log)
        qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]

        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[1]["commodity"]) \
            .with_piece_count(self.shipping_items_data[1]["piece"]) \
            .with_weight(self.shipping_items_data[1]["weight"]) \
            .with_weight_units(self.shipping_items_data[1]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[1]["unitcount"]) \
            .with_length(self.shipping_items_data[1]["length"]) \
            .with_width(self.shipping_items_data[1]["width"]) \
            .with_height(self.shipping_items_data[1]["height"]) \
            .with_dim_units(self.shipping_items_data[1]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

    @pytest.fixture(scope="function")
    def clean_notes_form(self):
        self.notes_page.click_clear_button()

    @pytest.mark.testcase("TL-1164")
    def test_notes_container_is_displayed_tl(self):

        self.ts.mark(self.notes_page.get_notes_section_visibility(),
                     "Notes section should be visible")
        self.ts.mark(self.validator.verify_text_match(self.notes_page.get_note_type_dropdown_value(),
                                                      self.note_types_data[0]["defaultNoteType"]),
                     "Type dropdown should not be selected by default")
        self.ts.mark(self.notes_page.get_cancel_button_visibility(),
                     "Cancel Button should be visible")
        self.ts.mark(self.notes_page.get_save_button_visibility(),
                     "Save Button should be visible")

        self.ts.mark_final("test_notes_container_is_displayed_tl", True,
                           "test_notes_container_is_displayed_tl")

    @pytest.mark.testcase("TL-1168")
    @pytest.mark.testcase("TL-1169")
    def test_note_type_and_text_are_required_fields_tl(self):

        initial_notes_count = self.notes_page.get_added_notes_count()

        actual_values = self.notes_page.get_note_type_dropdown_available_values()
        expected_values = self.note_types_data[0].get("noteTypes").split(',')

        self.ts.mark(self.validator.verify_unordered_list_match(actual_values, expected_values),
                     "Note type dropdown options do not match expected")

        self.notes_page.enter_note_text("Test Notes text")
        self.notes_page.click_save_button()

        self.ts.mark(self.notes_page.select_note_type_error_message_is_displayed(),
                     "Select note type error message is not displayed")

        self.ts.mark(initial_notes_count == self.notes_page.get_added_notes_count(), "Note should have not been added")

        self.notes_page.click_clear_button()
        self.notes_page.select_note_type("Quote")

        self.notes_page.click_save_button()

        self.ts.mark(self.notes_page.enter_note_text_error_message_is_displayed(),
                     "Enter note text error message is not displayed")

        self.ts.mark(initial_notes_count == self.notes_page.get_added_notes_count(), "Note should have not been added")

        self.notes_page.click_clear_button()
        self.notes_page.click_save_button()

        self.ts.mark(self.notes_page.enter_note_type_and_text_error_message_is_displayed(),
                     "Enter note text and type error message is not displayed")

        self.ts.mark(initial_notes_count == self.notes_page.get_added_notes_count(), "Note should have not been added")

        self.ts.mark_final("test_notes_container_is_displayed_tl", True,
                           "test_notes_container_is_displayed_tl")

    @pytest.mark.testcase("TL-1167")
    def test_user_can_cancel_note_creation_tl(self):

        note_text = "Test Notes text"

        initial_notes_count = self.notes_page.get_added_notes_count()

        self.notes_page.select_note_type("Internal")
        self.ts.mark(self.validator.verify_text_match(self.notes_page.get_note_type_dropdown_value(), "Internal"),
                     "Selected Note Type is not visible")

        self.notes_page.enter_note_text(note_text)
        actual_note_text = self.notes_page.get_note_textfield_text()
        self.ts.mark(self.validator.verify_text_match(actual_note_text, note_text),
                     "Entered note text should be visible in note text field")

        self.notes_page.click_clear_button()

        self.ts.mark(initial_notes_count == self.notes_page.get_added_notes_count(),
                     "Note should not be added after clicking Cancel")

        self.ts.mark(self.validator.verify_text_match(self.notes_page.get_note_textfield_text(), ""),
                     "Note textfield should clear after clicking Cancel")
        self.ts.mark(self.validator.verify_text_match(self.notes_page.get_note_type_dropdown_value(),
                                                      self.note_types_data[0]["defaultNoteType"]),
                     "Note type dropbox should clear after clicking Cancel")

        self.ts.mark_final("test_user_can_cancel_note_creation_tl", True,
                           "test_user_can_cancel_note_creation_tl")

    @pytest.mark.testcase("TL-1165")
    @pytest.mark.testcase("TL-1166")
    def test_added_note_is_visible_and_limited_to_500_characters_tl(self):

        note_text_to_enter = "Test Note text 500 symbols max Test Note text 500 symbols max Test Note text 500 symbols " \
                             "max Test Note text 500 symbols max Test Note text 500 symbols max Test Note text 500 " \
                             "symbols max Test Note text 500 symbols max Test Note text 500 symbols max Test Note text " \
                             "500 symbols max Test Note text 500 symbols max Test Note text 500 symbols max Test Note " \
                             "text 500 symbols max Test Note text 500 symbols max Test Note text 500 symbols max Test " \
                             "Note text 500 symbols max Test Note text 500 symbols max Testing"

        expected_note_type = "Internal"
        expected_username = self.login_data[0]["userName"]
        expected_note_text = "Test Note text 500 symbols max Test Note text 500 symbols max Test Note text 500 " \
                             "symbols max Test Note text 500 symbols max Test Note text 500 symbols max Test Note " \
                             "text 500 symbols max Test Note text 500 symbols max Test Note text 500 symbols max Test " \
                             "Note text 500 symbols max Test Note text 500 symbols max Test Note text 500 symbols max " \
                             "Test Note text 500 symbols max Test Note text 500 symbols max Test Note text 500 " \
                             "symbols max Test Note text 500 symbols max Test Note text 500 symbols max Test"

        initial_notes_count = self.notes_page.get_added_notes_count()

        self.notes_page.select_note_type(expected_note_type)
        self.ts.mark(
            self.validator.verify_text_match(self.notes_page.get_note_type_dropdown_value(), expected_note_type),
            "Selected Note Type is not visible")

        self.notes_page.enter_note_text(note_text_to_enter)

        self.notes_page.click_save_button()

        self.ts.mark(self.notes_page.get_added_notes_count() == initial_notes_count + 1,
                     "Note count should increase after note is added")

        added_note = self.notes_page.get_added_note_by_text(expected_note_text)

        self.ts.mark(added_note is not None, "Note should be visible in the list")

        if added_note is not None:
            self.ts.mark(self.validator.verify_text_match(added_note.note_type, expected_note_type),
                         f"Note Type should match expected. Expected:{expected_note_type} Actual: {added_note.note_type}")
            self.ts.mark(self.validator.verify_text_match(added_note.username, expected_username),
                         f"Note username should match expected. Expected:{expected_username} Actual: {added_note.username}")
            self.ts.mark(self.validator.verify_text_match(added_note.note_text, expected_note_text),
                         f"Note text should match expected. Expected:{expected_note_text} Actual: {added_note.note_text}")
            self.ts.mark(re.match(r"\d{1,2}:\d{1,2}\s+-\s+\d{1,2}/\d{1,2}/\d{4}", str(added_note.timestamp)),
                         "Note timestamp should be in correct format")

            self.ts.mark(self.validator.verify_text_match(self.notes_page.get_note_textfield_text(), ""),
                         "Note textfield should clear after adding a new note")
            self.ts.mark(self.validator.verify_text_match(self.notes_page.get_note_type_dropdown_value(),
                                                          self.note_types_data[0]["defaultNoteType"]),
                         "Note type dropbox should clear after adding a new note")

        self.notes_page.click_clear_button()

        self.ts.mark_final("test_added_note_is_visible", True,
                           "test_added_note_is_visible")

    @pytest.mark.testcase("TL-TL-1171")
    def test_note_list_is_sorted_by_created_date_tl(self):

        initial_notes_count = self.notes_page.get_added_notes_count()

        note_1 = NoteBuilder().with_note_type("Internal").with_note_text("Test Note 1").build()
        note_2 = NoteBuilder().with_note_type("Financial").with_note_text("Test Note 2").build()
        note_3 = NoteBuilder().with_note_type("Quote").with_note_text("Test Note 3").build()
        note_4 = NoteBuilder().with_note_type("Quote").with_note_text("Test Note 4").build()

        test_notes = [note_1, note_2, note_3, note_4]

        for note in test_notes:
            self.notes_page.add_note(note)

        self.ts.mark(self.notes_page.get_added_notes_count() == initial_notes_count + 4,
                     "Note count should increase after notes are added")

        added_notes = self.notes_page.get_all_notes()

        self.ts.mark(self.validator.verify_text_match(added_notes[0].note_text, "Test Note 4"),
                     "Note should be sorted by created date")
        self.ts.mark(self.validator.verify_text_match(added_notes[1].note_text, "Test Note 3"),
                     "Note should be sorted by created date")
        self.ts.mark(self.validator.verify_text_match(added_notes[2].note_text, "Test Note 2"),
                     "Note should be sorted by created date")
        self.ts.mark(self.validator.verify_text_match(added_notes[3].note_text, "Test Note 1"),
                     "Note should be sorted by created date")

        self.ts.mark_final("test_note_list_is_sorted_by_created_date_tl", True,
                           "test_note_list_is_sorted_by_created_date_tl")
