from datetime import date
from datetime import datetime

import pytest

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from utilities.read_data import read_csv_data
from utilities.read_data import read_json_file
from verifiers.createquote.create_quote_accessorial_validators import CreateQuoteAccessorialValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestRouteDetails:
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def method_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(self.driver, self.log)
        self._tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)
        self.verifiers = CreateQuoteAccessorialValidators(self.log)
        self.test_data = read_json_file("tl_create_quote_origin_destintation.json")

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        qop = CreateQuotePageTL(self.driver, self.log)
        qop.open_page()
        quote_data = read_csv_data("quote_details.csv")
        qop.search_and_select_customer_by_index(quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

    """
    Feature: Verify routing details

    Background:
        Given I am logged in as a Sales Rep
        And I enter Create Quote form for a sample customer
        And I choose 'TL' Service Type

    # TL-95
    Scenario: Check address search window appear
        GIVEN the form is empty
        WHEN I click on the origin and I type in a zip code or city or state name
        THEN the list of applicable results appears

    Scenario: check select address from address book
        GIVEN the form is empty
        WHEN I select an address from the Address book
        THEN it should show the City, State, and Zip on the Address line (the Address line 1 will show down in the Origin container)
    """

    @pytest.mark.testcase("TL-835")
    def test_check_address_search_appeared(self):
        # Arrange
        route_details_page = TLRouteDetailsPage(self.driver, self.log)

        # Act
        route_details_page.select_origin_by_zip_code_exact_match("76022")

        # Assert
        assert route_details_page.get_origin_address_text() == "Bedford, TX, 76022"

    @pytest.mark.skip("not implemented yet")
    def test_origin_date_and_time(self):
        self._tl_route_details_page.set_origin_pickup_date(date_to_set=date(month=1, day=13, year=2021))
        self._tl_route_details_page.set_origin_open_time("13:30")
        self._tl_route_details_page.set_origin_close_time("17:30")

        pass

    @pytest.mark.skip("not implemented yet")
    def test_destination_date_and_time(self):
        self._tl_route_details_page.set_destination_pickup_date(date_to_set=date(month=2, day=14, year=2021))
        self._tl_route_details_page.set_destination_open_time("14:00")
        self._tl_route_details_page.set_destination_close_time("18:00")

        pass
