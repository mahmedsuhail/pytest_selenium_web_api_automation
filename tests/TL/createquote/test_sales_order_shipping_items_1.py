import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_shipping_items_validators import CreateQuoteShippingItemsValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "open_page_and_select_customer")
class TestSalesOrderShippingItems1():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
    route_details_data = read_csv_data("tl_route_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.shipping_items = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.verifiers = CreateQuoteShippingItemsValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="function")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def open_page_and_select_customer(self):
        type(self).qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())

        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()
        self.set_preconditions()

    def set_preconditions(self):
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()

        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_cost(10)
        financials_charge_rows[0].set_revenue(120)

        financials_page.click_save()

    @pytest.mark.testcase("TL-1434")
    @pytest.mark.testcase("TL-1437")
    @pytest.mark.testcase("TL-1438")
    @pytest.mark.testcase("TL-1441")
    def test_so_container_displays_total_shipment_value(self):
        row_id = 0
        default_total_shipment_value = "100000"
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        shipping_items_after_clear = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_clear).is_equal_to(shipping_item.get_clear_template())

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 10)
        total_shipment_value = sales_order_page.get_total_shipment_value()
        assert_that(total_shipment_value).described_as("TL-1434 total shipment value has default").is_equal_to(
            default_total_shipment_value)

        value_to_test = "100010"
        sales_order_page.set_total_shipment_value(value_to_test, True)
        total_shipment_value = sales_order_page.get_total_shipment_value()
        assert_that(total_shipment_value).described_as(
            "TL-1437, TL-1438 total shipment value has changed value, field is editable").is_equal_to(value_to_test)

        value_to_test_letters = "asdf!@%&"
        sales_order_page.set_total_shipment_value(value_to_test_letters, False)
        total_shipment_value = sales_order_page.get_total_shipment_value()
        assert_that(total_shipment_value).described_as(
            "TL-1441 value is not changed after typing characters").is_equal_to(value_to_test)

    @pytest.mark.testcase("TL-4049")
    @pytest.mark.testcase("TL-4059")
    def test_so_total_value_calculator_values_adding_in_real_time(self):
        # TL-4049
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        shipping_items_after_clear = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_clear).is_equal_to(shipping_item.get_clear_template())

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-4049 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4049 Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4049Total density").is_equal_to("110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4049 Total weight").is_equal_to("202.00")

        # TL-4059
        so_items[0].click_remove_item()

        so_items_after_remove = sales_order_page.get_sale_item_rows()
        with soft_assertions():
            assert_that(len(so_items_after_remove)).described_as(
                "TL-4059 Verify Sales Order items count after remove item").is_equal_to(0)

            assert_that(sales_order_page.get_total_volume()).described_as("TL-4059 Total value").is_equal_to("0.00")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4059 Total density").is_equal_to("NaN")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4059 Total weight").is_equal_to("0.00")

    @pytest.mark.testcase("TL-4062")
    def test_so_total_value_calculator_values_recalculated_on_update__in_real_time(self):
        # preconditions
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_before_add = shipping_items_page.get_shipping_item_from_page()
        shipping_items_before_add.dim_units = shipping_items_before_add.dim_units.splitlines()[0]

        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_items = sales_order_page.get_sale_item_rows()

        shipping_items_after_add = shipping_items_page.get_shipping_item_from_page()

        with soft_assertions():
            assert_that(shipping_items_after_add).is_equal_to(shipping_item.get_clear_template())
            assert_that(len(so_items)).described_as("Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("Total density").is_equal_to("110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("Total weight").is_equal_to("202.00")

        # test
        so_items[0].click_edit_item()
        shipping_items_after_edit = shipping_items_page.get_shipping_item_from_page()
        shipping_items_after_edit.dim_units = shipping_items_after_edit.dim_units.splitlines()[0]
        assert_that(shipping_items_after_edit).described_as("Shipping items are the same like before add") \
            .is_equal_to(shipping_items_before_add)

        row_id = 1
        shipping_item_to_edit = ShippingItemBuilder() \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .build()

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_to_edit, clear=True)
        shipping_items_page.click_add_to_order_button()

        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-4062 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4062 Total value").is_equal_to("0.06")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4062 Total density").is_equal_to(
                "4832.00")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4062 Total weight").is_equal_to("302.00")

    @pytest.mark.testcase("TL-1364")
    @pytest.mark.testcase("TL-4067")
    def test_so_changes_not_applied_to_calculator_when_user_clicks_cancel(self):
        # preconditions
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_before_add = shipping_items_page.get_shipping_item_from_page()
        shipping_items_before_add.dim_units = shipping_items_before_add.dim_units.splitlines()[0]

        shipping_items_page.click_add_to_order_button()

        shipping_items_after_add = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_add).is_equal_to(shipping_item.get_clear_template())

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-4067 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4067 Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4067Total density").is_equal_to("110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4067 Total weight").is_equal_to("202.00")

        so_items[0].click_edit_item()
        assert_that(so_items[0].is_edit_item_button_visible()).described_as(
            "TL-1364 Edit button should not be visible").is_false()
        shipping_items_after_edit = shipping_items_page.get_shipping_item_from_page()
        shipping_items_after_edit.dim_units = shipping_items_after_edit.dim_units.splitlines()[0]
        assert_that(shipping_items_after_edit).described_as("Shipping items are the same like before add") \
            .is_equal_to(shipping_items_before_add)

        row_id = 1
        shipping_item_to_edit = ShippingItemBuilder() \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .build()

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_to_edit)

        # test
        shipping_items_page.click_clear_button()
        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-4067 Verify Sales Order items count").is_equal_to(1)
            assert_that(sales_order_page.get_total_volume()).described_as("TL-4067 Total value").is_equal_to("1.83")
            assert_that(sales_order_page.get_total_density()).described_as("TL-4067Total density").is_equal_to("110.18")
            assert_that(sales_order_page.get_total_weight()).described_as("TL-4067 Total weight").is_equal_to("202.00")

        so_items[0].click_remove_item()

    @pytest.mark.testcase("TL-1365")
    def test_so_info_in_shipping_items_is_lost_after_edit(self):
        # preconditions
        row_id = 0
        shipping_item_first = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 2
        shipping_item_second = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_first)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        so_items = sales_order_page.get_sale_item_rows()

        shipping_items_after_add = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_add).is_equal_to(shipping_item_first.get_clear_template())

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-1365 Verify Sales Order item is added").is_equal_to(1)
            assert_that(so_items[0].is_edit_item_button_visible()).described_as(
                "TL-1365 Edit button should be visible").is_true()

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_second)
        so_items[0].click_edit_item()

        shipping_items_after_edit = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_edit).described_as(
            "TL-1365 Shipping items fields are the same from sales order item").is_equal_to(shipping_item_first)

    @pytest.mark.testcase("TL-1366")
    def test_so_info_in_shipping_items_is_lost_after_edit_unsaved_editable(self):
        # preconditions
        row_id = 0
        shipping_item_first = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 1
        shipping_item_second = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        row_id = 2
        shipping_item_third = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_first)
        shipping_items_page.click_add_to_order_button()

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_second)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(2, 10)
        so_items = sales_order_page.get_sale_item_rows()

        with soft_assertions():
            assert_that(len(so_items)).described_as("TL-1366 Verify Sales Order items are added").is_equal_to(2)

        so_items[0].click_edit_item()
        shipping_items_after_edit = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_edit).described_as(
            "TL-1366 Shipping items fields are the same from first sales order item").is_equal_to(shipping_item_first)

        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item_third)
        so_items[1].click_edit_item()
        shipping_items_after_edit = shipping_items_page.get_shipping_item_from_page()
        assert_that(shipping_items_after_edit).described_as(
            "TL-1366 Shipping items fields are the same from second sales order item").is_equal_to(shipping_item_second)
