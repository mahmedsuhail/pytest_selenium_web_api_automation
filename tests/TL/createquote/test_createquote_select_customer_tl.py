import pytest
import unittest2

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.home.TMSlogin_page import TMSLoginPage
from pages.basictypes.service_type import ServiceType
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUp", "login")
class CreateQuoteCustomerSectionTests(unittest2.TestCase):
    quoteData = read_csv_data("quote_details.csv")
    equipment_types_data = read_csv_data("tl_equipment_types_data.csv")
    service_types_data = read_csv_data("tl_service_types_data.csv")
    shipment_types_data = read_csv_data("tl_shipment_types_data.csv")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.create_quote_customer_page = CreateQuotePageTL(self.driver, self.log)
        self.test_status = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUp")
    def login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        create_quote_customer_page = CreateQuotePageTL(self.driver, self.log)
        create_quote_customer_page.click_quote_order_icon()

    @pytest.fixture(autouse=False)
    def select_customer_and_go_to_tl(self):
        create_quote_customer_page = CreateQuotePageTL(self.driver, self.log)
        create_quote_customer_page.clear_customer_search_field()
        create_quote_customer_page.search_and_select_customer_by_index(self.quoteData[1]["customerName"])
        create_quote_customer_page.select_service_type(service_type=ServiceType.TL)
        create_quote_customer_page.wait_for_page_loader_to_show_and_disappear()

    '''
    GIVEN User is logged in TMS and Create Quote page is opened.
    WHEN In Create Quote container in "Customer" field enter 2 or less characters
    THEN Customer drop-down list is NOT opened. Searching in "Customer" field is not applied.
    '''

    @pytest.mark.testcase("TL-747")
    def test_create_quote_customer_search_2_or_less_characters_entered(self):
        self.log.info("inside test_create_quote_customer_search_2_or_less_characters_entered")

        incomplete_customer_name = self.quoteData[1]["customerName"][:2].lower()
        self.log.info("Passed customer Name : " + incomplete_customer_name)

        self.create_quote_customer_page.clear_customer_search_field()
        self.create_quote_customer_page.search_customer(incomplete_customer_name)
        customer_list = self.create_quote_customer_page.get_customer_with_contacts_list()
        search_list_result = len(customer_list) == 0

        self.test_status.mark(search_list_result, "Verify the Search List Based on the customer name passed")

        self.test_status.mark_final(
            "test_create_quote_customer_search_2_or_less_characters_entered",
            True,
            "Verify Based on customer search data should populate and get selected.")

    '''
    GIVEN User is logged in TMS and Create Quote page is opened.
    WHEN In Create Quote container in "Customer" field enter 2 or less characters
    THEN Customer drop-down list is NOT opened. Searching in "Customer" field is not applied.
    '''

    @pytest.mark.testcase("TL-746")
    def test_create_quote_customer_search_3_or_more_characters_entered(self):
        self.log.info("inside test_create_quote_customer_search_3_or_more_characters_entered")

        incomplete_customer_name = self.quoteData[1]["customerName"][:3].lower()

        self.log.info("Passed customer Name : " + incomplete_customer_name)

        self.create_quote_customer_page.clear_customer_search_field()
        self.create_quote_customer_page.search_customer(incomplete_customer_name)
        customers_with_contacts = self.create_quote_customer_page.get_customer_with_contacts_list()
        search_list_result = len(customers_with_contacts) != 0

        self.test_status.mark(search_list_result, "Verify the Search List Based on the customer name passed")

        correct_search_result = True

        for i in range(len(customers_with_contacts)):
            if not customers_with_contacts[i][0].lower().startswith(incomplete_customer_name) and not \
                    customers_with_contacts[i][1].lower().startswith(incomplete_customer_name):
                correct_search_result = False

        self.test_status.mark(correct_search_result,
                              "Verify that the search result is correctly filtered by customer name")

        self.test_status.mark_final(
            "test_create_quote_customer_search_3_or_more_characters_entered",
            True,
            "Verify Based on customer search data should populate and be correctly filtered")

    '''
    GIVEN User is logged in TMS and Create Quote page is opened. Customer is selected
    WHEN User selects TruckLoad service type
    THEN Create Quote page is re-rendered and additionally display Service field, Equipment Type field, Shipment Type field.
    AND THEN Customer Rates/ Cost only toggles are hidden
    '''

    @pytest.mark.testcase("TL-1198")
    @pytest.mark.testcase("TL-1197")
    @pytest.mark.testcase("TL-1196")
    def test_select_truckload_service_type_and_verify(self):

        expected_default_equipment_type = self.equipment_types_data[0].get("defaultEquipmentType")
        expected_default_shipment_type = self.shipment_types_data[0].get("defaultShipmentType")
        expected_default_service_value = self.service_types_data[0].get("defaultServiceType")

        self.log.info("inside test_select_truckload_serviceType")
        self.test_status.mark(self.create_quote_customer_page.verify_quote_page_is_disabled(), "Quote page should be disabled")

        self.create_quote_customer_page.clear_customer_search_field()
        self.create_quote_customer_page.search_customer(self.quoteData[1]["customerName"])
        self.create_quote_customer_page.select_customer_from_list()
        self.__select_truck_load()
        self.test_status.mark(self.create_quote_customer_page.verify_quote_page_is_enabled_tl(),
                              "After selecting TL service type, create quote page should re-render")

        self.test_status.mark(self.create_quote_customer_page.service_field_is_displayed(),
                              "Service field should be displayed in Create Quote section")
        self.test_status.mark(self.create_quote_customer_page.cost_currency_field_is_displayed(),
                              "Currency field should be displayed in Create Quote section")
        self.test_status.mark(self.create_quote_customer_page.equipment_type_field_is_displayed(),
                              "Equipment type field should be displayed in Create Quote section")
        self.test_status.mark(self.create_quote_customer_page.shipment_type_field_is_displayed(),
                              "Shipment type field should be displayed in Create Quote section")

        self.test_status.mark(not self.create_quote_customer_page.customer_rates_radiobutton_is_displayed(),
                              "Customer rates radiobutton should not be displayed in Create Quote section")
        self.test_status.mark(not self.create_quote_customer_page.cost_radiobutton_is_displayed(),
                              "Cost radiobutton should not be displayed in Create Quote section")

        self.test_status.mark(self.create_quote_customer_page.verify_cost_currency_field_value("USD"),
                              "Currency drop down should be correctly populated")
        self.test_status.mark(
            self.create_quote_customer_page.verify_service_field_value(expected_default_service_value),
            "Service drop down should be correctly populated")
        self.test_status.mark(
            self.create_quote_customer_page.verify_equipment_type_field_value(expected_default_equipment_type),
            "Equipment drop down should not have a default value")

        self.test_status.mark(
            self.create_quote_customer_page.verify_shipment_type_field_value(expected_default_shipment_type),
            "Shipment drop down should not have a default value")

        self.test_status.mark_final(
            "test_select_truckload_serviceType",
            True,
            "Validating Create Quote section after selecting TL service type")

    def __select_truck_load(self):
        self.create_quote_customer_page.select_service_type(service_type=ServiceType.TL)
        self.create_quote_customer_page.wait_for_page_loader_to_show_and_disappear()
