import pytest
from assertpy import assert_that
from assertpy import soft_assertions

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.basictypes.service_type import ServiceType
from pages.common.models.data_holder import DataHolder
from utilities.read_data import read_csv_data
from utilities.read_data import read_json_file
from utilities.util import Util
from verifiers.createquote.create_quote_accessorial_validators import CreateQuoteAccessorialValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestOriginDestinationDetails:
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def method_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(self.driver, self.log)
        self._rl_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self._rl_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)
        self._tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)
        self.verifiers = CreateQuoteAccessorialValidators(self.log)
        self.test_data = read_json_file("tl_create_quote_origin_destintation.json")
        self._rl_origin_page.clear_controls()
        self._rl_destination_page.clear_controls()

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        qop = CreateQuotePageTL(self.driver, self.log)
        qop.open_page()
        quote_data = read_csv_data("quote_details.csv")
        qop.search_and_select_customer_by_index(quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.testcase("TL-3742")
    @pytest.mark.testcase("TL-3596")
    def test_origin_details_container_is_prepopulated_from_route_details(self):
        # Arrange
        self.__select_route_details_and_wait_for_route_locations(
            company_name=self.test_data["customer1"]["input_data_text"],
            select_route_method=self._tl_route_details_page.select_origin_by_name_exact_match,
            route_locations_page=self._rl_origin_page
        )

        # Act
        self.__proceed_with_contact_details_container(self._rl_origin_page)

    @pytest.mark.testcase("TL-3746")
    @pytest.mark.testcase("TL-4151")
    def test_destination_details_container_is_prepopulated_from_route_details(self):
        # Arrange
        self.__select_route_details_and_wait_for_route_locations(
            company_name=self.test_data["customer1"]["input_data_text"],
            select_route_method=self._tl_route_details_page.select_destination_by_name_exact_match,
            route_locations_page=self._rl_destination_page
        )

        # Act
        self.__proceed_with_contact_details_container(self._rl_destination_page)

    @pytest.mark.testcase("TL-4152")
    def test_user_can_enter_up_to_500_characters_into_origin_pickup(self):
        self.__proceed_with_500_to_pickup(self._rl_origin_page)

    @pytest.mark.testcase("TL-4153")
    def test_user_can_enter_up_to_500_characters_into_destination_pickup(self):
        self.__proceed_with_500_to_pickup(self._rl_destination_page)

    @pytest.mark.testcase("TL-3842")
    @pytest.mark.jira("TL-9534")
    def test_user_is_able_to_manually_populate_the_address_on_origin_container(self):
        self.__proceed_with_route_location_manual_fill(self._rl_origin_page)

    @pytest.mark.testcase("TL-3836")
    @pytest.mark.jira("TL-9534")
    def test_user_is_able_to_manually_populate_the_address_on_destination_container(self):
        self.__proceed_with_route_location_manual_fill(self._rl_destination_page)

    @pytest.mark.testcase("TL-3748")
    @pytest.mark.testcase("TL-3830")
    def test_changes_reflected_in_origin_details_if_user_updates_route_details(self):
        self.__proceed_changes_in_route_location_to_reflect_in_route_details(
            route_location_page=self._rl_origin_page,
            get_address_method=self._tl_route_details_page.get_origin_address_text
        )

    @pytest.mark.testcase("TL-3825")
    @pytest.mark.testcase("TL-3831")
    def test_changes_reflected_in_destination_details_if_user_updates_route_details(self):
        self.__proceed_changes_in_route_location_to_reflect_in_route_details(
            route_location_page=self._rl_destination_page,
            get_address_method=self._tl_route_details_page.get_destination_address_text
        )

    @pytest.mark.testcase("TL-3827")
    def test_origin_details_becomes_empty_after_clearing_route_details(self):
        self.__proceed_with_clear_route_locations_with_route_details_clear(
            select_method=self._tl_route_details_page.select_origin_by_name_exact_match,
            clear_method=self._tl_route_details_page.clear_origin,
            route_locations_page=self._rl_origin_page
        )

    @pytest.mark.testcase("TL-3826")
    def test_destination_details_becomes_empty_after_clearing_route_details(self):
        self.__proceed_with_clear_route_locations_with_route_details_clear(
            select_method=self._tl_route_details_page.select_destination_by_name_exact_match,
            clear_method=self._tl_route_details_page.clear_destination,
            route_locations_page=self._rl_destination_page
        )

    def __proceed_with_clear_route_locations_with_route_details_clear(self, select_method, clear_method,
                                                                      route_locations_page):
        # Arrange
        ref_data = DataHolder({
            "company": "",
            "contact_name": "",
            "contact_email": "",
            "contact_phone": "",
            "pickup": "",
            "address1": "",
            "address2": "",
            "city": "City/State",
            "zip": "Zip",
            "open_time": None,
            "close_time": None
        })
        self.__select_route_details_and_wait_for_route_locations(
            company_name=self.test_data["customer1"]["input_data_text"],
            select_route_method=select_method,
            route_locations_page=route_locations_page
        )

        # Act
        self.__select_route_details_and_wait_for_route_locations(
            company_name=None,
            select_route_method=clear_method,
            route_locations_page=route_locations_page
        )

        # Assert
        current = self.__get_route_locations_data(route_locations_page=route_locations_page)
        result, message = current.compare(ref_data)
        assert_that(message).is_equal_to("")

    def __select_route_details_and_wait_for_route_locations(self, company_name, select_route_method,
                                                            route_locations_page):
        start_hash = route_locations_page.get_overal_hash()
        if company_name is None:
            select_route_method()
        else:
            select_route_method(company_name)
        route_locations_page.wait_until_changed(start_hash)

    def __proceed_with_contact_details_container(self, route_locations_page):
        ref_data = DataHolder(self.test_data["customer1"]["contactbook_item"])

        # Act
        current = self.__get_route_locations_data(route_locations_page=route_locations_page)

        # Assert
        result, description = ref_data.compare(current)
        assert_that(description).is_equal_to("")

    def __get_route_locations_data(self, route_locations_page) -> DataHolder:
        company = route_locations_page.get_company()
        name = route_locations_page.get_contact_name()
        email = route_locations_page.get_contact_email()
        phone = route_locations_page.get_contact_phone()
        pickup = route_locations_page.get_pickup_remarks()
        address1, address2, city, zip = route_locations_page.get_address()
        open_time = route_locations_page.get_open_time()
        close_time = route_locations_page.get_open_time()

        return DataHolder({
            "company": company,
            "contact_name": name,
            "contact_email": email,
            "contact_phone": phone,
            "pickup": pickup,
            "address1": address1,
            "address2": address2,
            "city": city,
            "zip": zip,
            "open_time": open_time,
            "close_time": close_time
        })

    def __proceed_with_500_to_pickup(self, route_locations_page):
        # Arrange

        with soft_assertions():
            # Act
            test_data_500_chars = Util(logger=self.log).get_random_alpha_numeric(length=500)
            route_locations_page.type_to_pickup(text=test_data_500_chars)

            # Assert
            assert_that(route_locations_page.get_pickup_remarks()).is_equal_to(test_data_500_chars)
            assert_that(len(route_locations_page.get_pickup_remarks())).is_equal_to(len(test_data_500_chars))

            # Act (type additional characters)
            route_locations_page.type_to_pickup(text="AAA")

            # Assert
            assert_that(route_locations_page.get_pickup_remarks()).is_equal_to(test_data_500_chars)
            assert_that(len(route_locations_page.get_pickup_remarks())).is_equal_to(len(test_data_500_chars))

    def __proceed_with_route_location_manual_fill(self, route_location_page):

        self._tl_route_details_page.clear_origin()
        self._tl_route_details_page.clear_destination()
        # Arrange
        addr1 = self.test_data["origin_data1"]["addr1"]
        addr2 = self.test_data["origin_data1"]["addr2"]
        zip = self.test_data["origin_data1"]["zip"]
        city = self.test_data["origin_data1"]["city"]

        # Act - fill zip only
        route_location_page.type_to_address(addr1=addr1, addr2=addr2, zip=zip)

        # Assert
        act_addr1, act_addr2, act_city, act_zip = route_location_page.get_address()
        with soft_assertions():
            assert_that(act_addr1, "address_line1").is_equal_to(addr1)
            assert_that(act_addr2, "address_line2").is_equal_to(addr2)
            assert_that(act_city, "city should be automatically loaded when zip only provided").is_equal_to(city)
            assert_that(act_zip, "zip").is_equal_to(zip)

        # Act - fill city only
        route_location_page.type_to_address(city=city)

        # Assert
        act_addr1, act_addr2, act_city, act_zip = route_location_page.get_address()
        with soft_assertions():
            assert_that(act_addr1, "address_line1").is_equal_to(addr1)
            assert_that(act_addr2, "address_line2").is_equal_to(addr2)
            assert_that(act_city, "city").is_equal_to(city)
            assert_that(act_zip, "zip should be automatically loaded when city only provided").is_equal_to(zip)

    def __proceed_changes_in_route_location_to_reflect_in_route_details(self, route_location_page, get_address_method):
        # Arrange
        company = self.test_data["origin_data2"]["company"]
        ref_data = self.test_data["origin_data2"]["expected_route_details_value"]
        start_value = get_address_method()

        # Act
        route_location_page.select_by_company(company)
        actual_value = get_address_method(before_change=start_value)

        # Assert
        assert_that(actual_value).is_equal_to(ref_data)
