import pytest
from parameterized import parameterized

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_accessorial_page import CreateQuoteAccessorialPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.basictypes.service_type import ServiceType
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus
from verifiers.createquote.create_quote_accessorial_validators import CreateQuoteAccessorialValidators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestAccessorialsAndSS():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.qop = CreateQuotePageTL(self.driver, self.log)
        self.accessorial_page = CreateQuoteAccessorialPage(self.driver, self.log)
        self.verifiers = CreateQuoteAccessorialValidators(self.log)
        self.ts = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        self.driver.get(self.api.base_url + "/create-quote")
        qop = CreateQuotePageTL(self.driver, self.log)
        qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        qop.select_service_type(service_type=ServiceType.TL)
        qop.wait_for_page_loader_to_show_and_disappear()

    @pytest.mark.run(order=101)
    @pytest.mark.testcase("TL-839")
    def test_acss_is_displayed_after_selecting_equipment_type(self):
        result1 = self.qop.get_accessorials_container_visible()
        self.ts.mark(not result1, "'Accessorials & SS' container is not displayed.")

        self.qop.select_equipment_type("Van")

        result2 = self.qop.get_accessorials_container_visible()
        self.ts.mark(result2, "'Accessorials & SS' container is displayed.")

    @pytest.mark.testcase("TL-851")
    @pytest.mark.testcase("TL-852")
    @pytest.mark.testcase("TL-853")
    @pytest.mark.testcase("TL-854")
    @pytest.mark.testcase("TL-855")
    @pytest.mark.testcase("TL-856")
    @pytest.mark.testcase("TL-857")
    @pytest.mark.testcase("TL-858")
    @pytest.mark.testcase("TL-859")
    @pytest.mark.testcase("TL-860")
    @pytest.mark.testcase("TL-861")
    @pytest.mark.testcase("TL-862")
    @pytest.mark.testcase("TL-863")
    @pytest.mark.testcase("TL-864")
    @pytest.mark.testcase("TL-865")
    @pytest.mark.testcase("TL-866")
    @pytest.mark.testcase("TL-867")
    @pytest.mark.testcase("TL-868")
    @pytest.mark.testcase("TL-869")
    @pytest.mark.testcase("TL-870")
    @pytest.mark.testcase("TL-871")
    @parameterized.expand([
        ["Van"],
        ["Flatbed"],
        ["Temp Controlled (Frozen)"],
        ["Temp Controlled (Refrigerated)"],
        ["Stepdeck"],
        ["Power Only"],
        ["RGN"],
        ["Conestoga"],
        ["Curtainside"],
        ["Double Drop"],
        ["Cargo"],
        ["Container"],
        ["Sprinter Van"],
        ["TOFC"],
        ["COFC"],
        ["Bulk"],
        ["Cargo Van"],
        ["Partial TL"],
        ["Drayage"],
        ["Other"]
    ])
    def test_acss_container_includes_options_for_equipment(self, equipment_type):
        self.qop.select_equipment_type(equipment_type=equipment_type)

        result1 = self.qop.get_accessorials_container_visible()
        self.ts.mark(result1, "'Accessorials & SS' container is displayed.")

        origin_accessorials_list = self.accessorial_page.get_all_accessorials_list_tl()
        result2 = self.verifiers.validate_accessorials_based_on_equipment_type_tl(equipment_type,
                                                                                  origin_accessorials_list)
        self.ts.mark(result2, f"Validate equipment of type {equipment_type}")
        self.ts.mark_final("test_acss_container_includes_options_for_equipment",
                           True,
                           "Validating that after changing the value of \"Equipment Type\" field - options in "
                           "\"Accessorials & SS\" container are changed")

    @pytest.mark.testcase("TL-872")
    @pytest.mark.testcase("TL-873")
    @pytest.mark.testcase("TL-875")
    @parameterized.expand([
        ["Pallet Exchange", "Van", "Piece Count"],
        ["Drop Trailer Shipper", "Van", "Length (Hours)"],
        ["Drop Trailer Consignee", "Van", "Length (Hours)"]
    ])
    def test_acss_container_checkbox_additional_field(self, checkbox_name, equipment_type, field_name):
        self._after_checkbox_field_verification(
            equipment_type, checkbox_name, field_name,
            f"test_acss_container_checkbox_additional_field_{field_name}")

    def _after_checkbox_field_verification(self, equipment_type, checkbox_name, field_name, testname):
        self.qop.select_equipment_type(equipment_type)

        result1 = self.qop.get_accessorials_container_visible()
        self.ts.mark(result1, "'Accessorials & SS' container is displayed.")

        origin_accessorials_list = self.accessorial_page.get_all_accessorials_list_tl()
        result2 = self.verifiers.validate_accessorials_based_on_equipment_type_tl(equipment_type,
                                                                                  origin_accessorials_list)
        self.ts.mark(result2, f"Validate equipment of type {equipment_type}")

        # Step 2
        checkbox = next(
            (x for x in origin_accessorials_list if x.name == checkbox_name), None)
        checkbox.check(scroll_to=True)

        result3 = self.accessorial_page.get_field_under_checkbox_label_displayed(checkbox_name)
        self.ts.mark(result3, f"Validate if field {field_name} label is visible")

        result4 = self.accessorial_page.get_field_under_checkbox_input_displayed(checkbox_name)
        self.ts.mark(result4, f"Validate if field {field_name} input is visible")

        result5 = self.verifiers.verify_text_match(
            self.accessorial_page.get_field_under_checkbox_input_value(checkbox_name), "")
        self.ts.mark(result5, f"Validate if field {field_name} input is empty")

        # Step 3
        self.accessorial_page.set_field_under_checkbox_input_value(checkbox_name, "Abc")
        result6 = self.verifiers.verify_text_match(
            self.accessorial_page.get_field_under_checkbox_input_value(checkbox_name), "")
        self.ts.mark(result6, f"Validate if field {field_name} input is empty after providing Abc")

        # Step 4
        self.accessorial_page.set_field_under_checkbox_input_value(checkbox_name, "\"!@#$%^&*()-=+_{}[];':\"<>?/|")
        result7 = self.verifiers.verify_text_match(
            self.accessorial_page.get_field_under_checkbox_input_value(checkbox_name), "")
        self.ts.mark(result7, f"Validate if field {field_name} input is empty after providing special signs")

        # Step 5
        self.accessorial_page.set_field_under_checkbox_input_value(checkbox_name, "12345")
        result8 = self.verifiers.verify_text_match(
            self.accessorial_page.get_field_under_checkbox_input_value(checkbox_name), "12345")
        self.ts.mark(result8, f"Validate if field {field_name} input contains values provided: 12345")

        # Step 6
        checkbox.uncheck(scroll_to=True)

        result9 = self.accessorial_page.get_field_under_checkbox_label_displayed(checkbox_name)
        self.ts.mark(not result9, f"Validate if field {field_name} label is not visible")
        result10 = self.accessorial_page.get_field_under_checkbox_input_displayed(checkbox_name)
        self.ts.mark(not result10, f"Validate if field {field_name} input is not visible")

        self.ts.mark_final(testname,
                           True,
                           "Validating that After selecting \"{checkbox_name}\" option, "
                           "additional field \"{field_name}\" appears and disappears after deselecting "
                           "\"{checkbox_name}\" option")

    @pytest.mark.testcase("TL-876")
    def test_acss_container_checkbox_tarps_tarp_size_field(self):
        # Step 1
        equipment_type = "Flatbed"
        checkbox_name = "Tarps"
        field_name = "Tarp size"

        self.qop.select_equipment_type(equipment_type)

        result1 = self.qop.get_accessorials_container_visible()
        self.ts.mark(result1, "'Accessorials & SS' container is displayed.")

        origin_accessorials_list = self.accessorial_page.get_all_accessorials_list_tl()
        result2 = self.verifiers.validate_accessorials_based_on_equipment_type_tl(equipment_type,
                                                                                  origin_accessorials_list)
        self.ts.mark(result2, f"Validate equipment of type {equipment_type}")

        # Step 2
        checkbox = next(
            (x for x in origin_accessorials_list if x.name == checkbox_name), None)
        checkbox.check(scroll_to=True)

        result3 = self.accessorial_page.get_field_under_checkbox_label_displayed(checkbox_name)
        self.ts.mark(result3, f"Validate if field {field_name} label is visible")

        result4 = self.accessorial_page.get_tarp_size_field_displayed()
        self.ts.mark(result4, f"Validate if field {field_name} input is visible")

        dropdown = self.accessorial_page.get_tarp_size_dropdown()

        result5 = self.verifiers.verify_text_match(dropdown.get_selected_value(), "Not Specified")
        self.ts.mark(result5, f"Validate if field {field_name} selected is equal to 'Not Specified'")

        # Step 3
        result6 = self.verifiers.verify_unordered_list_match(dropdown.get_available_options_values(),
                                                             self.accessorial_page.tarp_size_expected_fields)
        self.ts.mark(result6, f"Validate if options from dropdown are equal to expected")

        # Step 4
        dropdown.select_option_by_value("Smoke")
        result7 = dropdown.is_dropdown_open()
        self.ts.mark(not result7, f"Validate if options from dropdown are hidden after choosing option")

        result8 = self.verifiers.verify_text_match(dropdown.get_selected_value(), "Smoke")
        self.ts.mark(result8, f"Validate if selected option is are equal to selected from dropdown")
        self.ts.mark_final("test_acss_container_checkbox_tarps_tarp_size_field",
                           True,
                           "Validating that After selecting \"{checkbox_name}\" option, "
                           "additional dropdown \"{field_name}\" appears with required options: {requiredFields} ")
