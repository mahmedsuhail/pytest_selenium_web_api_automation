import datetime
import re

import pytest
import unittest2
from assertpy import assert_that

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_saved_order_popup_page import CreateQuoteSavedOrderPopupPageTL
from pages.TL.createquote.create_quote_saved_popup_page_tl import CreateQuoteSavedPopupPageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.TL.salesorder.so_copy_sales_order_popup_page_tl import SOCopySalesOrderPopupPage
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL, SalesOrderConfirmationPopup
from pages.TL.salesorder.so_equipment_page import SOEquipmentPageTL
from pages.TL.salesorder.so_network_info_page import SONetworkInfoPageTL
from pages.TL.salesorder.so_route_details_page_tl import SORouteDetailsPageTL
from pages.TL.salesorder.so_send_email_popup_page_tl import SOSendEmailPopupPage
from pages.TL.salesorder.so_carrier_fulfillment_page import SOCarrierFulfillmentPage
from pages.TL.salesorder.so_documents_page_tl import SODocumentsPageTL
from pages.TL.salesorder.so_financials_page import SOFinancialsPage
from pages.TL.salesorder.so_freight_details_page_tl import SOFreightDetailsPageTL
from pages.TL.salesorder.so_statuses_page import SOStatusesPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL, SalesOrderBoardTablePageTL
from pages.TL.salesorderboard.sales_order_board_pending_quote_details_page_tl import \
    SalesOrderBoardQuoteDetailsPageTL
from pages.basictypes.service_type import ServiceType
from pages.common.builders.customer_builder import CustomerBuilder
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from pages.common.createquote_page import CreateQuotePage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data, read_json_file
from utilities.reportteststatus import ReportTestStatus
from utilities.util import Util
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUp", "login")
class CreateQuoteSmokeTestTL(unittest2.TestCase):
    quoteData = read_csv_data("quote_details.csv")
    equipment_types_data = read_csv_data("tl_equipment_types_data.csv")
    service_types_data = read_csv_data("tl_service_types_data.csv")
    shipment_types_data = read_csv_data("tl_shipment_types_data.csv")
    test_data = read_json_file("tl_create_quote_smoke_test_data.json")
    carrier_data = read_json_file("tl_carrier_fulfillment_data.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)
    util = Util(logger=log)

    @pytest.fixture(autouse=True)
    def classSetUp(self, oneTimeSetUp):
        self.create_quote_page_tl = CreateQuotePageTL(self.driver, self.log)
        self.create_quote_saved_popup_tl = CreateQuoteSavedPopupPageTL(self.driver, self.log)
        self.sales_order_board_page_tl = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table_tl = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_board_quote_details_page_tl = SalesOrderBoardQuoteDetailsPageTL(self.driver,
                                                                                         self.log)
        self.route_details_page_tl = TLRouteDetailsPage(self.driver, self.log)
        self.shipping_items_page_tl = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.create_quote_sales_order_page_tl = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        self.create_quote_financials_page = FinancialsPage(self.driver, self.log)
        self.create_quote_sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        self.create_quote_shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.create_order_popup = CreateQuoteSavedOrderPopupPageTL(self.driver, self.log)
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_statuses_page = SOStatusesPage(self.driver, self.log)
        self.sales_order_network_info_page = SONetworkInfoPageTL(self.driver, self.log)
        self.sales_order_equipment_page = SOEquipmentPageTL(self.driver, self.log)
        self.sales_order_route_details_page = SORouteDetailsPageTL(self.driver, self.log)
        self.sales_order_carrier_fulfillment_page = SOCarrierFulfillmentPage(self.driver, self.log)
        self.sales_order_financials_page = SOFinancialsPage(self.driver, self.log)
        self.sales_order_freight_details_page = SOFreightDetailsPageTL(self.driver, self.log)
        self.sales_order_documents_page = SODocumentsPageTL(self.driver, self.log)
        self.send_email_popup = SOSendEmailPopupPage(self.driver, self.log)
        self.sales_order_page_confirmation_popup = SalesOrderConfirmationPopup(self.driver, self.log)
        self.copy_sales_order_popup = SOCopySalesOrderPopupPage(self.driver, self.log)
        self.test_status = ReportTestStatus(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUp")
    def login(self):
        login_data = read_csv_data("login_details.csv")
        credential = login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        create_quote_page = CreateQuotePage(self.driver, self.log)
        create_quote_page.click_quote_order_icon()

    @pytest.mark.testcase("TL-4843")
    @pytest.mark.testcase("TL-5660")
    @pytest.mark.testcase("TL-4960")
    @pytest.mark.smoke
    def test_create_quote_build_order_copy_order_smoke_tl(self):
        # TEST DATA
        expected_default_equipment_type = self.equipment_types_data[0].get("defaultEquipmentType")
        expected_default_shipment_type = self.shipment_types_data[0].get("defaultShipmentType")
        expected_default_service_value = self.service_types_data[0].get("defaultServiceType")
        expected_equipment_types = self.equipment_types_data[0].get("equipmentTypes").split(',')
        equipment_length_to_select = self.test_data.get("selectedEquipmentLength")
        expected_equipment_length_options = self.test_data.get("equipmentLengthOptions")
        expected_shipment_types = self.shipment_types_data[0].get("shipmentTypes").split(',')
        shipment_type_to_select = self.test_data.get("selectedShipmentType")
        origin = self.test_data.get("origin")
        origin_short = self.test_data.get("originShort")
        destination = self.test_data.get("destination")
        destination_short = self.test_data.get("destinationShort")

        customer_data = CustomerBuilder() \
            .with_full_name(self.test_data["fullCustomerName"]) \
            .with_short_name(self.test_data["shortCustomerName"]) \
            .with_contact(self.test_data["customerContact"]) \
            .build()

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.test_data["shippingItem"].get("commodityDescription")) \
            .with_piece_count(self.test_data["shippingItem"].get("pieceCount")) \
            .with_weight(self.test_data["shippingItem"].get("weight")) \
            .with_pallet_count(self.test_data["shippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        customer_search_string = customer_data.full_name[:3]

        assert self.create_quote_page_tl.get_create_quote_container_visibility(), \
            "Create Quote container should be visible after selecting a customer"

        # STEPS 1-3
        self._select_customer_and_verify(search_string=customer_search_string, customer_data=customer_data)

        # STEPS 4-6
        self.select_tl_and_verify_customer_section_default_values(expected_default_service_value,
                                                                  expected_default_equipment_type,
                                                                  expected_default_shipment_type,
                                                                  expected_equipment_types)

        # STEPS 7-11
        self.select_equipment_options_and_verify(expected_equipment_length_options, equipment_length_to_select,
                                                 expected_shipment_types, shipment_type_to_select)

        # STEP 12
        self.validate_route_details_section()

        date_to_select_str = self.util.getDateValueWithoutHolidayAndWeekends(no_of_days=2)
        date_to_select = datetime.datetime.strptime(date_to_select_str, '%m/%d/%Y')

        # STEPS 13-19
        self.select_origin_and_verify(origin=origin, origin_short=origin_short, date_to_select=date_to_select,
                                      date_to_select_str=date_to_select_str)

        # STEPS 20-26

        self.select_destination_and_verify(destination=destination, destination_short=destination_short,
                                           date_to_select=date_to_select, date_to_select_str=date_to_select_str)

        # STEP 27

        self.test_status.mark(not self.create_quote_page_tl.get_create_quote_footer_button_enabled_status(),
                              "Create Quote Footer button should be disabled")

        # STEPS 28 - 33
        self.enter_shipping_item_and_verify(shipping_item=shipping_item)

        assert_that(self.create_quote_page_tl.get_create_quote_footer_button_enabled_status()) \
            .is_equal_to(True), "Create Quote Footer button should be enabled after all required data was provided"

        # STEP 34
        quote_id = self.create_quote_and_verify()

        # STEP 35
        self.go_to_pending_quotes_and_verify_created_quote(quote_id=quote_id)

        # Quoting a Quote
        self._quote_a_quote_and_verify()

        # Going to Quoted & building a load
        self._go_to_quoted_and_verify(quote_id=quote_id)
        self.sales_order_board_quote_details_page_tl.click_build_load()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        assert_that(self.create_quote_page_tl.check_if_create_quote_page_is_opened()).described_as(
            "Create Quote Page should open after clicking 'Build Load'") \
            .is_true()

        self._add_another_shipping_item_and_verify()

        # Modifying Financials
        self._modify_financials_in_sales_order()

        # Filling Origin & Destination data & building an order
        self._fill_origin_and_destination_data_and_build_order()

        # Go to BOL and verify Sales order
        assert_that(self.create_order_popup.is_popup_displayed()).described_as(
            "Build order was not successful").is_true()
        self.create_order_popup.click_go_to_bol()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        assert_that(self.sales_order_page.is_page_displayed()).described_as(
            "Sales Order Page Should be opened after clicking 'Go to BOL#' button").is_true()

        # TL-5660 Verifying that user can change statuses
        self._verify_that_user_can_change_order_statuses()

        # TL-4960 - verifying Copy Order
        self._copy_order_and_verify_success()
        self._verify_copied_order_data()

        # TL-4960 - verifying Edit Order
        self._edit_sales_order_and_verify(order_date_str=date_to_select_str)

        self.test_status.mark_final("test_create_quote_build_order_copy_order_smoke_tl", True,
                                    "Smoke test for create quote and building an order in TL")

    @pytest.mark.smoke
    @pytest.mark.jira("TL-9039")
    def test_smoke_test_TODO_refactor_changing_statuses(self):
        # TODO - rework statuses according to https://globaltranz.atlassian.net/browse/TL-7536
        # search for this comment in the test
        assert False

    def _select_customer_and_verify(self, search_string, customer_data):
        self.test_status.mark(self.create_quote_page_tl.get_customer_rates_radiobutton_checked_status(),
                              "Customer Rates radiobutton should be selected by default")

        selected_customer = self.create_quote_page_tl.search_and_select_customer_by_name(search_string=search_string,
                                                                                         customer_name=customer_data.full_name)

        assert selected_customer.full_name == customer_data.full_name
        assert selected_customer.contact == customer_data.contact

        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_customer_search_box_value(),
                                                                customer_data.short_name),
                              "Customer name should match in customer search box")

    def select_tl_and_verify_customer_section_default_values(self, expected_default_service_value,
                                                             expected_default_equipment_type,
                                                             expected_default_shipment_type, expected_equipment_types):
        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_service_type_dropdown_value(), "LTL"),
                              "Default service type value does not match")
        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_currency_dropdown_value(), "USD"),
                              "Currency default value does not match")

        self.create_quote_page_tl.select_service_type(service_type=ServiceType.TL)
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        assert self.create_quote_page_tl.verify_quote_page_is_enabled_tl(), \
            "After selecting TL service type, create quote page should re-render"

        self.test_status.mark(self.create_quote_page_tl.service_field_is_displayed(),
                              "Service field should be displayed in Create Quote section")
        self.test_status.mark(self.create_quote_page_tl.cost_currency_field_is_displayed(),
                              "Currency field should be displayed in Create Quote section")
        self.test_status.mark(self.create_quote_page_tl.equipment_type_field_is_displayed(),
                              "Equipment type field should be displayed in Create Quote section")
        self.test_status.mark(self.create_quote_page_tl.shipment_type_field_is_displayed(),
                              "Shipment type field should be displayed in Create Quote section")

        self.test_status.mark(not self.create_quote_page_tl.customer_rates_radiobutton_is_displayed(),
                              "Customer rates radiobutton should not be displayed in Create Quote section")
        self.test_status.mark(not self.create_quote_page_tl.cost_radiobutton_is_displayed(),
                              "Cost radiobutton should not be displayed in Create Quote section")

        self.test_status.mark(self.create_quote_page_tl.verify_cost_currency_field_value("USD"),
                              "Currency drop down should be correctly populated")
        self.test_status.mark(
            self.create_quote_page_tl.verify_service_field_value(expected_default_service_value),
            "Service drop down should be correctly populated")
        self.test_status.mark(
            self.create_quote_page_tl.verify_equipment_type_field_value(expected_default_equipment_type),
            "Equipment drop down should not have a default value")

        self.test_status.mark(
            self.create_quote_page_tl.verify_shipment_type_field_value(expected_default_shipment_type),
            "Shipment drop down should not have a default value")

        self.test_status.mark(self.validators.verify_unordered_list_match(
            self.create_quote_page_tl.get_equipment_type_dropdown_values(),
            expected_equipment_types),
            "Equipment drop down should have correct values populated")

    def select_equipment_options_and_verify(self, expected_equipment_length_options, equipment_length_to_select,
                                            expected_shipment_types, shipment_type_to_select):
        # STEP 7
        selected_equipment_type = self.test_data["selectedEquipmentType"]
        self.create_quote_page_tl.select_equipment_type(selected_equipment_type)

        self.test_status.mark(self.create_quote_page_tl.equipment_length_field_is_displayed(),
                              "Equipment length field should be displayed in Create Quote section after selecting Equipment type")
        self.test_status.mark(
            self.create_quote_page_tl.verify_equipment_length_field_value("Select Equipment Length"),
            "Equipment length field should not have a default value")

        # STEP 8-9
        self.test_status.mark(
            self.create_quote_page_tl.verify_equipment_length_dropdown_values(expected_equipment_length_options),
            "Equipment length field should have correct values populated")

        self.create_quote_page_tl.select_equipment_length(equipment_length_to_select)

        # STEPS 10-11
        self.test_status.mark(
            self.create_quote_page_tl.verify_shipment_type_dropdown_values(expected_shipment_types),
            "Shipment drop down should have correct values populated")

        self.create_quote_page_tl.select_shipment_type(shipment_type=shipment_type_to_select)

        self.test_status.mark(
            self.create_quote_page_tl.verify_equipment_length_field_value(equipment_length_to_select),
            "Equipment length field should be correctly populated after click")
        self.test_status.mark(
            self.validators.verify_text_match(self.create_quote_page_tl.get_selected_shipment_type(),
                                              shipment_type_to_select),
            "Shipment drop down should have correct value selected")

    def validate_route_details_section(self):
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_address_text(), "Enter Origin"),
            "Origin should be empty by default when Creating a Quote")
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_address_text(),
                                              "Enter Destination"),
            "Destination should be empty by default when Creating a Quote")

        date_value = self.util.get_current_date()

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_pickup_date_value(),
                                              date_value),
            "Pickup Date value should match when Creating a Quote")
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_dropoff_date_value(),
                                              date_value),
            "Dropoff Date value should match when Creating a Quote")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_open_time_value(),
                                              "00:00"),
            "Origin open time value should match when Creating a Quote")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_close_time_value(),
                                              "00:00"),
            "Origin close time value should match when Creating a Quote")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_open_time_value(),
                                              "00:00"),
            "Destination open time value should match when Creating a Quote")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_close_time_value(),
                                              "00:00"),
            "Destination close time value should match when Creating a Quote")

        self.test_status.mark(
            not self.route_details_page_tl.get_appointment_required_checked_status(),
            "Appointment Required checkbox should be unselected by default when Creating a Quote")

        self.test_status.mark(
            not self.route_details_page_tl.get_appointment_confirmed_checked_status(),
            "Appointment Confirmed checkbox should be unselected by default when Creating a Quote")

    def select_origin_and_verify(self, origin, origin_short, date_to_select, date_to_select_str):
        self.route_details_page_tl.search_and_select_origin(search_string=origin[:3], expected_result=origin)

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_address_text(), origin_short),
            "Origin address did not match")

        self.route_details_page_tl.set_origin_pickup_date(date_to_set=date_to_select)

        self.route_details_page_tl.set_origin_open_time("08:00")
        self.route_details_page_tl.set_origin_close_time("10:00")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_pickup_date_value(), date_to_select_str),
            "Origin Pickup date does not match")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_open_time_value(), "08:00"),
            "Origin Open time value does not match")
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_close_time_value(), "10:00"),
            "Origin Close time value does not match")

    def select_destination_and_verify(self, destination, destination_short, date_to_select, date_to_select_str):
        self.route_details_page_tl.search_and_select_destination(search_string=destination[:3],
                                                                 expected_result=destination)
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_address_text(),
                                              destination_short),
            "Destination address did not match when Creating a Quote")

        self.route_details_page_tl.set_destination_pickup_date(date_to_set=date_to_select)

        self.route_details_page_tl.set_destination_open_time("18:00")
        self.route_details_page_tl.set_destination_close_time("20:00")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_dropoff_date_value(), date_to_select_str),
            "Destination Pickup date does not match expected when Creating a Quote")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_open_time_value(), "18:00"),
            "Destination Open time value does not match")
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_close_time_value(), "20:00"),
            "Destination Close time value does not match")

    def enter_shipping_item_and_verify(self, shipping_item):
        assert_that(self.shipping_items_page_tl.is_page_displayed()) \
            .is_equal_to(True), "Shipping items container should be displayed in Create Quote page"

        self.test_status.mark(not self.shipping_items_page_tl.get_hazmat_checkbox_checked_status(),
                              "Hazmat checkbox should be unchecked by default")
        self.test_status.mark(not self.shipping_items_page_tl.get_stackable_checkbox_checked_status(),
                              "Stackable checkbox should be unchecked by default")

        self.shipping_items_page_tl.enter_shipping_item_data(shipping_item=shipping_item)
        added_shipping_item = self.shipping_items_page_tl.get_shipping_item_from_page()

        self.test_status.mark(added_shipping_item == shipping_item,
                              "Shipping item data should be populated correctly when creating a quote")

        self.shipping_items_page_tl.click_add_to_order_button()

        total_shipment_value = self.test_data.get("expectedTotalShipmentValue")

        so_items = self.create_quote_sales_order_page_tl.get_sale_item_rows()

        self.test_status.mark(len(so_items) == 1, "Only one shipping item should be added when creating a quote")

        self.test_status.mark(
            self.validators.verify_text_match(so_items[0].get_commodity_text(), shipping_item.commodity_description),
            "Shipping item is not shown in Sales Order Page")

        self.test_status.mark(
            self.validators.verify_text_match(self.create_quote_sales_order_page_tl.get_total_shipment_value(),
                                              total_shipment_value),
            "Total Shipment value does not match")

    def create_quote_and_verify(self) -> str:
        self.create_quote_page_tl.click_create_quote_footer_button()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear(show_timeout=10, disappear_timeout=120)

        assert self.create_quote_saved_popup_tl.is_popup_displayed(), "Quote Created popup is not displayed"

        self.test_status.mark(self.create_quote_saved_popup_tl.is_close_button_visible(),
                              "Close button is not visible")
        self.test_status.mark(self.create_quote_saved_popup_tl.is_create_new_quote_button_displayed(),
                              "Create new quote button is not displayed")
        self.test_status.mark(self.create_quote_saved_popup_tl.is_go_to_pending_quotes_button_displayed(),
                              "Go to pending quotes button is not displayed")

        popup_message = self.create_quote_saved_popup_tl.get_popup_message()

        self.test_status.mark(re.match(r"Quote # \d+ Created!", popup_message),
                              "Create Quote success message is not correct")
        quote_id = re.findall(r"\d+", popup_message)[0]

        return quote_id

    def go_to_pending_quotes_and_verify_created_quote(self, quote_id):
        self.create_quote_saved_popup_tl.click_go_to_pending_quotes_button()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()
        self.sales_order_board_page_tl.clear_filters()
        self.sales_order_board_table_tl.wait_for_board_rows_to_be_loaded()

        self.test_status.mark(not self.create_quote_saved_popup_tl.is_popup_displayed(),
                              "Quote Created popup should be closed")
        assert self.sales_order_board_page_tl.is_pending_quotes_tab_displayed(), \
            "Pending Quotes tab should be opened after clicking  Go to Pending Quotes button"

        self.sales_order_board_table_tl.set_quote_id_column_filter(value=quote_id)

        assert self.sales_order_board_page_tl.check_if_created_quote_is_visible_in_table(
            quote_number=quote_id), \
            "Created Quote is not visible in Pending Quotes tab"

        self.sales_order_board_page_tl.open_quote_details(quote_number=quote_id)
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

        # Verifying Data in Pending Quote details view
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_customer_name(),
                                              self.test_data["pendingQuoteData"].get("customer")),
            "Customer Name should match in pending quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_service_type(),
                                              self.test_data["pendingQuoteData"].get("serviceType")),
            "serviceType should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_shipment_type(),
                                              self.test_data["pendingQuoteData"].get("shipmentType")),
            "shipmentType should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_origin(),
                                              self.test_data["pendingQuoteData"].get("origin")),
            "origin should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_destination(),
                                              self.test_data["pendingQuoteData"].get("destination")),
            "destination should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_commodity_description(),
                                              self.test_data["pendingQuoteData"].get("commodityDescription")),
            "commodityDescription should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_pieces(),
                                              self.test_data["pendingQuoteData"].get("pieceCount")),
            "pieceCount should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_weight(),
                                              self.test_data["pendingQuoteData"].get("weight")),
            "weight should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_pallets(),
                                              self.test_data["pendingQuoteData"].get("palletCount")),
            "palletCount should match in pending quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_handling_unit_type(),
                                              self.test_data["pendingQuoteData"].get("unitType")),
            "unitType should match in pending quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_service(),
                                              self.test_data["pendingQuoteData"].get("service")),
            "service should match in pending quote details view")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_equipment(),
                                              self.test_data["pendingQuoteData"].get("equipment")),
            "equipment should match in pending quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_length(),
                                              self.test_data["pendingQuoteData"].get("length")),
            "length should match in pending quote details view")

    def _quote_a_quote_and_verify(self):
        self.sales_order_board_quote_details_page_tl.set_sell_rate(self.test_data["financials"].get("sellRate"))
        self.sales_order_board_quote_details_page_tl.wait_for_revenue_change(
            expected_value=self.test_data["financialsSummary"].get("revenue"))

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_revenue_pending_quote(),
                                              self.test_data["financialsSummary"].get("revenue")),
            "After sell rate is provided in pending quote, revenue should be automatically populated")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_margin_pending_quote(),
                                              self.test_data["financialsSummary"].get("revenue")),
            "After sell rate is provided in pending quote, margin should be automatically populated")

        self.enter_note_page = self.sales_order_board_quote_details_page_tl.click_quote_it()
        self.enter_note_page.click_send_rate_button()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()
        self.send_email_popup.select_first_email_recipient_from_contacts()
        self.send_email_popup.click_send_button()
        self.test_status.mark(self.send_email_popup.is_email_sent_message_displayed(timeout=20),
                              "Email Sent success message should be shown after sending rate")
        self.send_email_popup.click_send_email_okay_button()

    def _go_to_quoted_and_verify(self, quote_id):
        self.sales_order_board_page_tl.click_on_quoted_tab()
        self.sales_order_board_page_tl.clear_filters()
        self.sales_order_board_table_tl.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table_tl.set_quote_id_column_filter(value=quote_id)
        self.sales_order_board_table_tl.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page_tl.open_quote_details(quote_number=quote_id)

        # Verifying Data in Quoted quote details view
        self.test_status.mark(self.sales_order_board_quote_details_page_tl.check_if_quoted_marker_is_visible(),
                              "Quoted Marker Should be visible in Quoted quote details view")
        self.test_status.mark(self.sales_order_board_quote_details_page_tl.check_if_quote_document_is_visible(),
                              "Quote Document Should be visible in Quoted quote details view")
        self.test_status.mark(self.sales_order_board_quote_details_page_tl.check_if_build_load_button_is_visible(),
                              "Build Load button Should be visible in Quoted quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_customer_name(),
                                              self.test_data["quotedQuoteViewData"].get("customer")),
            "Customer Name should match in Quoted quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_shipment_type(),
                                              self.test_data["quotedQuoteViewData"].get("shipmentType")),
            "shipmentType should match in Quoted quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_commodity_description(),
                                              self.test_data["quotedQuoteViewData"].get("commodityDescription")),
            "commodityDescription should match in Quoted quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_origin(),
                                              self.test_data["quotedQuoteViewData"].get("origin")),
            "origin should match in Quoted quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_destination(),
                                              self.test_data["quotedQuoteViewData"].get("destination")),
            "destination should match in Quoted quote details view")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_board_quote_details_page_tl.get_revenue(),
                                              self.test_data["financialsSummary"].get("revenue")),
            "revenue should match in Quoted quote details view")

    def _modify_financials_in_sales_order(self):
        self.create_quote_financials_page.enable()
        self.create_quote_financials_page.set_max_buy(self.test_data["financials"].get("maxBuy"))
        self.create_quote_financials_page.set_target_cost(self.test_data["financials"].get("targetCost"))
        self.create_quote_financials_page.click_save()

    def _add_another_shipping_item_and_verify(self):
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.test_data["additionalShippingItem"].get("commodityDescription")) \
            .with_piece_count(self.test_data["additionalShippingItem"].get("pieceCount")) \
            .with_weight(self.test_data["additionalShippingItem"].get("weight")) \
            .with_pallet_count(self.test_data["additionalShippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        self.create_quote_shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        self.create_quote_shipping_items_page.click_add_to_order_button()

        so_items = self.create_quote_sales_order_page.get_sale_item_rows()
        self.test_status.mark(len(so_items) == 2,
                              "Two shipping items should be visible in Sales order container after adding shipping item")
        self.test_status.mark(
            self.validators.verify_text_match(so_items[1].get_commodity_text(), shipping_item.commodity_description),
            "Commodity text should be properly displayed for added shipping item in Sales Order")

    def _fill_origin_and_destination_data_and_build_order(self):
        # TL-4862 Filling Origin & Destination data

        self.create_quote_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self.create_quote_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)

        self.create_quote_origin_page.enter_company_name(
            company_name=self.test_data["originContainerData"].get("companyName"))
        self.create_quote_origin_page.enter_contact_name(
            contact_name=self.test_data["originContainerData"].get("contactName"))
        self.create_quote_origin_page.enter_contact_phone(
            contact_phone=self.test_data["originContainerData"].get("contactPhone"))
        self.create_quote_origin_page.type_to_address(
            addr1=self.test_data["originContainerData"].get("address"))

        self.create_quote_destination_page.enter_company_name(
            company_name=self.test_data["destinationContainerData"].get("companyName"))
        self.create_quote_destination_page.enter_contact_name(
            contact_name=self.test_data["destinationContainerData"].get("contactName"))
        self.create_quote_destination_page.enter_contact_phone(
            contact_phone=self.test_data["destinationContainerData"].get("contactPhone"))
        self.create_quote_destination_page.type_to_address(
            addr1=self.test_data["destinationContainerData"].get("address"))

        # TL-4862 Building an order
        self.create_quote_page_tl.click_build_order_footer_button()
        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()

    def _search_and_select_carrier(self, carrier_name):
        self.sales_order_carrier_fulfillment_page.click_select_carrier_button()
        self.sales_order_carrier_fulfillment_page.search_and_select_carrier(search_string=carrier_name,
                                                                            carrier_name=carrier_name)
        self.sales_order_carrier_fulfillment_page.click_save_and_assign_button()

        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()
        self.sales_order_page.wait_for_order_to_be_loaded()

    def _select_status(self, status):
        if status == "CUSTOMER HOLD":
            self.sales_order_statuses_page.select_customer_hold_status()
        if status == "BOOKED":
            self.sales_order_statuses_page.select_booked_status()
        if status == "DISPATCH":
            self.sales_order_statuses_page.select_dispatch_status()
        if status == "AT SHIPPER":
            self.sales_order_statuses_page.select_at_shipper_status()
        if status == "IN TRANSIT":
            self.sales_order_statuses_page.select_in_transit_status()
        if status == "AT CONSIGNEE":
            self.sales_order_statuses_page.select_at_consignee_status()
        if status == "PAPERWORK":
            self.sales_order_statuses_page.select_paperwork_status()
        if status == "PROBLEM":
            self.sales_order_statuses_page.select_problem_status()
        if status == "DELIVERED":
            self.sales_order_statuses_page.select_delivered_status()
            self.sales_order_page_confirmation_popup.click_yes()

        self.create_quote_page_tl.wait_for_page_loader_to_show_and_disappear()
        self.sales_order_page.wait_for_order_to_be_loaded()

    def _verify_financials_in_sales_order(self):
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_revenue(),
                                              self.test_data["financialsSummary"].get("revenue")),
            "Revenue value should match in Sales Order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_margin(),
                                              self.test_data["financialsSummary"].get("margin")),
            "Margin value should match in Sales Order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_margin_percentage(),
                                              self.test_data["financialsSummary"].get("marginPercentage")),
            "Margin percentage value should match in Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_max_buy(),
                                              self.test_data["financialsSummary"].get("maxBuy")),
            "Max Buy value should match in Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_target_cost(),
                                              self.test_data["financialsSummary"].get("targetCost")),
            "Target cost value should match in Sales Order")

    def _verify_that_user_can_change_order_statuses(self):
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_vendor_bill_status(), "None"),
            "TL-4861 Vendor bill status should be None")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_invoice_status(), "Pending"),
            "TL-4861 Invoice status should be Pending")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_current_order_status(), "AVAILABLE"),
            "TL-4861 Order status should be AVAILABLE")

        self._select_status(status="CUSTOMER HOLD")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_current_order_status(),
                                              "CUSTOMER HOLD"),
            "Order status should successfully change from AVAILABLE to CUSTOMER HOLD")

        # Selecting a carrier and verifying pending status
        carrier_name = self.test_data.get("carrierName")
        self._search_and_select_carrier(carrier_name=carrier_name)

        # TODO - remove this workaround when TL-8594 is fixed
        # self.sales_order_page.refresh_page()

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_order_status_button_text(), "PENDING"),
            "TL-4861 Order status should change to PENDING after Carrier is assigned to sales order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_booked_by_field(),
                                              self.test_data.get("expectedBookedBy")),
            f"TL-4861 Booked by value should be correctly displayed after assigning a carrier ")
        self.test_status.mark(
            self.sales_order_carrier_fulfillment_page.check_if_carrier_is_assigned(
                carrier_name=carrier_name),
            "TL-5660 Carrier name should be visible in fulfillment section after assignment")

        self.sales_order_documents_page.click_on_rate_confirmation_document()
        self.sales_order_documents_page.click_send_email_button()
        self.send_email_popup.select_first_email_recipient_from_contacts()
        self.send_email_popup.click_send_button()
        self.test_status.mark(self.send_email_popup.is_email_sent_message_displayed(),
                              "Email Sent success message should be shown after sending rate confirmation document")
        self.send_email_popup.click_send_email_okay_button()
        self.sales_order_page.wait_for_page_loader_to_show_and_disappear()

        self.sales_order_statuses_page.wait_for_order_status_change(expected_status="BOOKED")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_current_order_status(), "BOOKED"),
            "Order status should successfully change to BOOKED after sending rate confirmation document")

        self._select_status(status="DISPATCH")
        self.sales_order_statuses_page.wait_for_order_status_change(expected_status="DISPATCH")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_current_order_status(), "DISPATCH"),
            "Order status should successfully change from BOOKED to DISPATCH")
        self.test_status.mark(
            self.validators.verify_unordered_list_match(
                self.sales_order_statuses_page.get_order_status_dropdown_options(),
                ["BOOKED"]),
            "TL-7536 Order status dropdown should have ['BOOKED'] option only")

        # TODO - rework statuses according to https://globaltranz.atlassian.net/browse/TL-7536
        # self.sales_order_page.click_view_stops_button()
        # self._select_status(status="AT SHIPPER")
        # self.test_status.mark(
        #     self.validators.verify_text_match(self.sales_order_page.get_current_order_status(), "AT SHIPPER"),
        #     "Order status should successfully change from DISPATCH to AT SHIPPER")
        #
        # self._select_status(status="IN TRANSIT")
        # self.test_status.mark(
        #     self.validators.verify_text_match(self.sales_order_page.get_current_order_status(), "IN TRANSIT"),
        #     "Order status should successfully change from AT SHIPPER to IN TRANSIT")
        #
        # self._select_status(status="AT CONSIGNEE")
        # self.test_status.mark(
        #     self.validators.verify_text_match(self.sales_order_page.get_current_order_status(), "AT CONSIGNEE"),
        #     "Order status did not successfully change from IN TRANSIT to AT CONSIGNEE")
        #
        # self._select_status(status="PAPERWORK")
        # self.test_status.mark(
        #     self.validators.verify_text_match(self.sales_order_page.get_current_order_status(), "PAPERWORK"),
        #     "Order status should successfully change from AT CONSIGNEE to PAPERWORK")
        #
        # self._select_status(status="PROBLEM")
        # self.test_status.mark(
        #     self.validators.verify_text_match(self.sales_order_page.get_current_order_status(), "PROBLEM"),
        #     "Order status should successfully change from PAPERWORK to PROBLEM")
        #
        # self._select_status(status="DELIVERED")
        # self.test_status.mark(
        #     self.validators.verify_text_match(self.sales_order_page.get_order_status_button_text(), "DELIVERED"),
        #     "Order status should successfully change from PROBLEM to DELIVERED")

    def _copy_order_and_verify_success(self):
        self.sales_order_page.click_copy_order_footer_button()
        self.test_status.mark(
            self.validators.verify_text_match(self.copy_sales_order_popup.copy_order_get_number_of_copies(), "1"),
            "Default number of order copies should be 1 when trying to copy an order")
        self.copy_sales_order_popup.click_copy_order_confirm_button()
        self.copy_sales_order_popup.wait_for_page_loader_to_show_and_disappear()
        assert_that(self.copy_sales_order_popup.check_copy_order_success()).described_as(
            "Copy Sales Order operation should be successful").is_true()
        self.test_status.mark(
            len(self.copy_sales_order_popup.get_go_to_links()) == 2,
            "Go to Sales Order Board and Go To BOL links should be displayed after Copy Order Success")
        self.copy_sales_order_popup.click_go_to_bol()
        self.copy_sales_order_popup.wait_for_page_loader_to_show_and_disappear()
        assert_that(self.sales_order_page.is_page_displayed()).described_as(
            "Copied Sales Order page should be shown after clicking BOL link").is_true()

    def _verify_copied_order_data(self):

        # Network Info container validation
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_customer_name_field(),
                                              self.test_data["copiedOrderData"].get("customer")),
            "Customer value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_booked_by_field(),
                                              self.test_data["copiedOrderData"].get("bookedBy")),
            "bookedBy value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_created_by_field(),
                                              self.test_data["copiedOrderData"].get("createdBy")),
            "createdBy value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_shipment_type_field(),
                                              self.test_data["copiedOrderData"].get("shipmentType")),
            "shipmentType value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_generated_on_field(),
                                              self.test_data["copiedOrderData"].get("generatedOn")),
            "generatedOn value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_network_info_page.get_rating_method_field(),
                                              self.test_data["copiedOrderData"].get("ratingMethod")),
            "ratingMethod value should match in copied order")

        # Equipment Section
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_equipment_page.get_equipment_type_field(),
                                              self.test_data["copiedOrderData"].get("equipmentType")),
            "equipmentType value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_equipment_page.get_equipment_length_field(),
                                              self.test_data["copiedOrderData"].get("equipmentLength")),
            "equipmentLength value should match in copied order")

        # Freight details container validation
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_total_shipment_value(),
                                              self.test_data["copiedOrderData"].get("totalShipmentValue")),
            "totalShipmentValue value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_commodity_description_value(),
                                              self.test_data["copiedOrderData"].get("commodityDescription")),
            "commodityDescription value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_handling_unit_count_value(),
                                              self.test_data["copiedOrderData"].get("handlingUnitCount")),
            "handlingUnitCount value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_pallets_value(),
                                              self.test_data["copiedOrderData"].get("pallets")),
            "pallets value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_pieces_value(),
                                              self.test_data["copiedOrderData"].get("pieces")),
            "pieces value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_carrier_insurance_value(),
                                              self.test_data["copiedOrderData"].get("carrierInsurance")),
            "carrierInsurance value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_freight_details_page.get_weight_value(),
                                              self.test_data["copiedOrderData"].get("weight")),
            "weight value should match in copied order")

        # Verifying Carrier Fulfillment data
        self.test_status.mark(self.sales_order_carrier_fulfillment_page.check_if_select_carrier_button_is_visible(),
                              "Carrier should not be selected in copied order")

        # Verifying Route Details

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_origin_field(),
                                              self.test_data["copiedOrderData"].get("origin")),
            "origin value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_origin_company_field(),
                                              self.test_data["copiedOrderData"].get("originCompany")),
            "originCompany value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_origin_contact_name_field(),
                                              self.test_data["copiedOrderData"].get("originContactName")),
            "originContactName value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_origin_phone_field(),
                                              self.test_data["copiedOrderData"].get("originPhone")),
            "originPhone value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_origin_check_in_field(),
                                              "Check In Pending"),
            "origin check in field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_origin_checkout_field(),
                                              "Check Out Pending"),
            "origin check out field should match in copied order")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_destination_field(),
                                              self.test_data["copiedOrderData"].get("destination")),
            "destination value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_destination_company_field(),
                                              self.test_data["copiedOrderData"].get("destinationCompany")),
            "destinationCompany value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_destination_contact_name_field(),
                                              self.test_data["copiedOrderData"].get("destinationContactName")),
            "destinationContactName value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_destination_phone_field(),
                                              self.test_data["copiedOrderData"].get("destinationPhone")),
            "destinationPhone value should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_destination_check_in_field(),
                                              "Check In Pending"),
            "destination check in field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_route_details_page.get_destination_checkout_field(),
                                              "Check Out Pending"),
            "destination check out field should match in copied order")

        # Verifying Statuses
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_vendor_bill_status(),
                                              self.test_data["copiedOrderData"].get("vendorBillStatus")),
            "vendorBillStatus field should match in copied order")

        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_invoice_status(),
                                              self.test_data["copiedOrderData"].get("invoiceStatus")),
            "invoiceStatus field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_statuses_page.get_current_order_status(),
                                              self.test_data["copiedOrderData"].get("shipmentStatus")),
            "shipmentStatus field should match in copied order - should be AVAILABLE")

        # Verifying Financials
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_actual_cost(),
                                              self.test_data["copiedOrderData"].get("actualCost")),
            "actualCost field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_revenue(),
                                              self.test_data["copiedOrderData"].get("revenue")),
            "revenue field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_margin(),
                                              self.test_data["copiedOrderData"].get("margin")),
            "margin field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_margin_percentage(),
                                              self.test_data["copiedOrderData"].get("marginPercentage")),
            "marginPercentage field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_max_buy(),
                                              self.test_data["copiedOrderData"].get("maxBuy")),
            "maxBuy field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_target_cost(),
                                              self.test_data["copiedOrderData"].get("targetCost")),
            "targetCost field should match in copied order")
        self.test_status.mark(
            self.validators.verify_text_match(self.sales_order_financials_page.get_price_to_beat(),
                                              self.test_data["copiedOrderData"].get("priceToBeat")),
            "priceToBeat field should match in copied order")

        # Verifying Notes
        self.test_status.mark(
            not self.sales_order_page.check_if_any_notes_are_added(),
            "no notes should be added in copied order")

        # Verifying Documents
        self.test_status.mark(
            self.sales_order_documents_page.is_quote_document_displayed(),
            "Quote Document should be present in copied order")
        self.test_status.mark(
            self.sales_order_documents_page.is_bill_of_landing_document_displayed(),
            "Bill of Landing Document should be present in copied order")

    def _edit_sales_order_and_verify(self, order_date_str):
        self.sales_order_page.click_edit_order_footer_button()
        self.sales_order_page.wait_for_page_loader_to_show_and_disappear(show_timeout=20, disappear_timeout=20)

        # Verifying Network Info Section

        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_customer_search_box_value(),
                                                                self.test_data["copiedOrderData"].get("customer")),
                              "Customer name should match in customer search box after EDIT Sales Order")

        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_selected_shipment_type(),
                                                                self.test_data.get("selectedShipmentType")),
                              "shipmentType should match in customer search box after EDIT Sales Order")
        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_selected_equipment_type(),
                                                                self.test_data["copiedOrderData"].get("equipmentType")),
                              "equipmentType should match in customer search box after EDIT Sales Order")
        self.test_status.mark(self.validators.verify_text_match(self.create_quote_page_tl.
                                                                get_selected_equipment_length(),
                                                                self.test_data["copiedOrderData"].get(
                                                                    "equipmentLength")),
                              "equipmentLength should match in customer search box after EDIT Sales Order")

        # Verifying Route Details Section
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_address_text(),
                                              self.test_data.get("originShort")),
            "Origin address did not match after EDIT Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_pickup_date_value(), order_date_str),
            "Origin Pickup date does not match after EDIT Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_open_time_value(), "08:00"),
            "Origin Open time value does not match after EDIT Sales Order")
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_origin_close_time_value(), "10:00"),
            "Origin Close time value does not match after EDIT Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_address_text(),
                                              self.test_data.get("destinationShort")),
            "Destination address did not match after EDIT Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_dropoff_date_value(), order_date_str),
            "Destination Pickup date does not match expected after EDIT Sales Order")

        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_open_time_value(), "18:00"),
            "Destination Open time value does not match after EDIT Sales Order")
        self.test_status.mark(
            self.validators.verify_text_match(self.route_details_page_tl.get_destination_close_time_value(), "20:00"),
            "Destination Close time value does not match after EDIT Sales Order")

        self.test_status.mark(
            not self.route_details_page_tl.get_appointment_required_checked_status(),
            "Appointment Required checkbox should be unselected after EDIT Sales Order")

        self.test_status.mark(
            not self.route_details_page_tl.get_appointment_confirmed_checked_status(),
            "Appointment Confirmed checkbox should be unselected after EDIT Sales Order")
