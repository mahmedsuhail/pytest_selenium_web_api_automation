import os
import time

import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from API.Helpers.create_order_helpers_api import CreateOrderHelpersAPI
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL, SalesOrderConfirmationPopup
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.TL.salesorderboard.sales_order_board_pending_quote_details_page_tl import \
    SalesOrderBoardQuoteDetailsPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from utilities.mail_api import MailAPI
from utilities.read_data import read_csv_data, read_json_file
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login")
class TestSOBPendingQuoteTests:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    create_tl_quote_api_data = read_json_file("API/order/create_tl_quote_model_with_email.json")
    create_tl_quote_api_data_credit_check_fails = read_json_file(
        "API/order/create_tl_quote_model_customer_credit_check_fails.json")
    create_tl_quote_documents_api_data = read_json_file("API/order/create_tl_quote_model_documents.json")
    pending_quoted_quote_data = read_json_file("TL/salesorderboard/tl_sob_pending_quoted_quote_data.json")
    quoted_quote_data_credit_check_fails = read_json_file(
        "TL/salesorderboard/tl_sob_quoted_quote_financials_customer_credit_check_fails.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.order_api = APIOrderAPI(self.api, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_page_confirmation_popup = SalesOrderConfirmationPopup(self.driver, self.log)
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_board_quote_details_page_tl = SalesOrderBoardQuoteDetailsPageTL(self.driver,
                                                                                         self.log)
        self.create_order_helpers_api = CreateOrderHelpersAPI(api=self.api, logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login(self):
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.sales_order_board_page.get_url_sales_order_board())

    @pytest.mark.testcase("TL-4271")
    @pytest.mark.testcase("TL-4273")
    @pytest.mark.testcase("TL-4274")
    @pytest.mark.testcase("TL-4275")
    def test_sob_pending_quote_it_button(self):
        text_to_input = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has " \
                        "been the industry's standard dummy text ever since the 1500s, when an unknown printer took " \
                        "a galley of type and scrambled it to make a type specimen book. It has survived not only " \
                        "five centuries, but also the leap into electronic typesetting, remaining essentially " \
                        "unchanged. It was popularised in the 1960s with the release of Letraset sheets containing" \
                        " Lorem Ipsum passages, and more recently with desktop pu"

        self.sales_order_board_page.open_page_tl()
        time.sleep(2)
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_page.clear_filters()

        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()

        self.sales_order_board_page.open_n_top_quote_details(n=1)
        # self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_quote_it_button_is_visible()) \
                .described_as("TL-4271 Quoted Marker Should be visible").is_true()

        self.sales_order_board_quote_details_page_tl.set_target_cost(1)
        self.sales_order_board_quote_details_page_tl.click_quote_it()  # click to go out from input, input is formatable
        self.enter_note_page = self.sales_order_board_quote_details_page_tl.click_quote_it()

        with soft_assertions():
            assert_that(self.enter_note_page.check_if_modal_is_opened()).described_as(
                "TL-4273 Enter note modal is opened").is_true()

        self.enter_note_page.set_text_in_area(text_to_input)

        with soft_assertions():
            assert_that(len(self.enter_note_page.get_text_from_area())).described_as(
                "TL-4275 Text is max 500 signs").is_equal_to(500)

        self.enter_note_page.click_cancel_button()

        with soft_assertions():
            assert_that(self.enter_note_page.check_if_modal_is_opened()).described_as(
                "TL-4274 Enter note modal is closed").is_false()

    @pytest.mark.testcase("TL-4276")
    def test_sob_pending_target_cost_field(self):
        self.sales_order_board_page.open_page_tl()
        time.sleep(2)
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.open_n_top_quote_details(n=1)

        test_value = 1234

        with soft_assertions():
            self.sales_order_board_quote_details_page_tl.set_target_cost(test_value)

            test_value = "1,234.00"
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
                "TL-4276 target cost is as set").is_equal_to(test_value)

            self.sales_order_board_quote_details_page_tl.set_target_cost("Qq", clear=False)
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
                "TL-4276 target cost is as set").is_equal_to(test_value)

            self.sales_order_board_quote_details_page_tl.set_target_cost("!@#$%", clear=False)
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
                "TL-4276 target cost is as set").is_equal_to(test_value)

    @pytest.mark.testcase("TL-4278")
    @pytest.mark.testcase("TL-4279")
    def test_sob_pending_click_quote_it_to_change_to_quoted(self):
        self.sales_order_board_page.open_page_tl()
        time.sleep(2)
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()
        self.quote_id = self.sales_order_board_page.open_n_top_quote_details(n=1)

        self.sales_order_board_quote_details_page_tl.check_if_status_is_visible()

        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_quote_it_button_is_visible()) \
                .described_as("TL-4271 Quoted Marker Should be visible").is_true()

            assert_that(self.sales_order_board_quote_details_page_tl.get_status()).described_as(
                "TL-4278 status is Pending Quote").is_equal_to("PENDING QUOTE")

        self.note_text = "asdfg"

        self.sales_order_board_quote_details_page_tl.set_target_cost(1)
        self.enter_note_page = self.sales_order_board_quote_details_page_tl.click_quote_it()
        self.enter_note_page.set_text_in_area(self.note_text)
        self.enter_note_page.click_send_rate_button()

        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()
        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.get_status()).described_as(
                "TL-4278 status is equal to Quoted").is_equal_to("QUOTED")
            assert_that(self.sales_order_board_quote_details_page_tl.get_notes_text()).described_as(
                "TL-4279 note added is visible").is_equal_to(self.note_text)

        self.sales_order_board_page.open_page_tl()
        self.sales_order_board_page.click_on_quoted_tab()
        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_quote_id_column_filter(value=self.quote_id)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        assert_that(
            self.sales_order_board_table.check_if_quote_is_present(quote_id=self.quote_id)).described_as(
            "TL-4278 order is now visible on Quoted tab").is_true()

        self.sales_order_board_page.open_page_tl()
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_table.set_quote_id_column_filter(value=self.quote_id)
        self.sales_order_board_table.wait_for_board_showing_no_rows_to_show()
        assert_that(
            self.sales_order_board_table.check_if_quote_is_present(quote_id=self.quote_id)).described_as(
            "TL-4278 order is not visible on Pending Quotes tab").is_false()

    @pytest.mark.testcase("TL-4359")
    @pytest.mark.testcase("TL-4360")
    @pytest.mark.testcase("TL-4361")
    @pytest.mark.testcase("TL-4363")
    @pytest.mark.testcase("TL-4365")
    @pytest.mark.testcase("TL-4370")
    @pytest.mark.testcase("TL-4277")
    def test_sob_pending_user_can_see_details(self):
        quote_bk = self._create_pending_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data,
                                                            target_cost=600)["quote_bk"]
        self.sales_order_board_page.open_page_tl()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_quote_id_column_filter(value=quote_bk)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.open_quote_details(quote_number=quote_bk)
        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_status_is_visible()).described_as(
                "TL-4359 status is visible").is_true()
            assert_that(self.sales_order_board_quote_details_page_tl.get_customer_name()).described_as(
                "TL-4359 customer name is equal").is_equal_to(self.pending_quoted_quote_data.get("customer"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_service_type()).described_as(
                "TL-4359 status is visible").is_equal_to(self.pending_quoted_quote_data.get("serviceType"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_shipment_type()).described_as(
                "TL-4359 status is visible").is_equal_to(self.pending_quoted_quote_data.get("shipmentType"))

            assert_that(self.sales_order_board_quote_details_page_tl.get_origin()).described_as(
                "TL-4359 origin should match").is_equal_to(self.pending_quoted_quote_data.get("origin"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_destination()).described_as(
                "TL-4359 destination should match").is_equal_to(self.pending_quoted_quote_data.get("destination"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_commodity_description()).described_as(
                "TL-4359 commodityDescription should match").is_equal_to(
                self.pending_quoted_quote_data.get("commodityDescription"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_pieces()).described_as(
                "TL-4359 pieceCount should match").is_equal_to(self.pending_quoted_quote_data.get("pieceCount"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_weight()).described_as(
                "TL-4359 weight should match").is_equal_to(self.pending_quoted_quote_data.get("weight"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_pallets()).described_as(
                "TL-4359 palletCount should match").is_equal_to(self.pending_quoted_quote_data.get("palletCount"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_handling_unit_type()).described_as(
                "TL-4359 unitType should match").is_equal_to(self.pending_quoted_quote_data.get("unitType"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_handling_unit_count()).described_as(
                "TL-4359 unitCount should match").is_equal_to(self.pending_quoted_quote_data.get("unitCount"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_service()).described_as(
                "TL-4359 service should match").is_equal_to(self.pending_quoted_quote_data.get("service"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_equipment()).described_as(
                "TL-4359 equipment should match").is_equal_to(self.pending_quoted_quote_data.get("equipment"))
            assert_that(self.sales_order_board_quote_details_page_tl.get_length()).described_as(
                "TL-4359 length should match").is_equal_to(self.pending_quoted_quote_data.get("length"))
            assert_that(sorted(self.sales_order_board_quote_details_page_tl.get_accessorials())).described_as(
                "TL-4359 accessorials should match").is_equal_to(
                sorted(self.pending_quoted_quote_data.get("accessorials")))

            assert_that(self.sales_order_board_quote_details_page_tl.is_max_buy_editable()).described_as(
                "TL-4360 is Max Buy editable").is_true()
            assert_that(self.sales_order_board_quote_details_page_tl.is_target_cost_editable()).described_as(
                "TL-4360 is Target Cost editable").is_true()
            assert_that(self.sales_order_board_quote_details_page_tl.is_price_to_beat_editable()).described_as(
                "TL-4360 is Price to Beat editable").is_true()

        self.sales_order_board_quote_details_page_tl.set_max_buy(100)
        self.sales_order_board_quote_details_page_tl.set_target_cost(100)
        self.sales_order_board_quote_details_page_tl.set_price_to_beat(100)

        self.sales_order_board_quote_details_page_tl.click_on_status()

        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.get_max_buy_input()).described_as(
                "TL-4361 Max buy have value as set").is_equal_to("100.00")
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
                "TL-4361 Target cost have value as set").is_equal_to("100.00")
            assert_that(self.sales_order_board_quote_details_page_tl.get_price_to_beat()).described_as(
                "TL-4361 Price to beat have value as set").is_equal_to("100.00")

        self.sales_order_board_quote_details_page_tl.set_max_buy("a#", False)
        self.sales_order_board_quote_details_page_tl.set_target_cost("a#", False)
        self.sales_order_board_quote_details_page_tl.set_price_to_beat("#a", False)

        self.sales_order_board_quote_details_page_tl.click_on_status()
        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.get_max_buy_input()).described_as(
                "TL-4363 Max buy cannot set different then digits").is_equal_to("100.00")
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
                "TL-4363 Target cost cannot set different then digits").is_equal_to("100.00")
            assert_that(self.sales_order_board_quote_details_page_tl.get_price_to_beat()).described_as(
                "TL-4363 Price to beat cannot set different then digits").is_equal_to("100.00")

        self.sales_order_board_quote_details_page_tl.set_max_buy("a", True)
        self.sales_order_board_quote_details_page_tl.set_target_cost("a", True)
        self.sales_order_board_quote_details_page_tl.set_price_to_beat("a", True)

        self.sales_order_board_quote_details_page_tl.click_on_status()
        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.get_max_buy_input()).described_as(
                "TL-4365 Max buy have default value after clean").is_equal_to("0.00")
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
                "TL-4365 Target cost have default value after clean").is_equal_to("0.00")
            assert_that(self.sales_order_board_quote_details_page_tl.get_price_to_beat()).described_as(
                "TL-4365 Price to beat have default value after clean").is_equal_to("0.00")

        self.sales_order_board_quote_details_page_tl.set_target_cost(-1)
        self.sales_order_board_quote_details_page_tl.click_on_status()

        assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost_input()).described_as(
            "TL-4277 Target cost cannot have minus value").is_equal_to("1.00")

        self.sales_order_board_page.close_quote_details(quote_number=quote_bk)
        assert_that(self.sales_order_board_quote_details_page_tl.check_if_status_is_visible()).described_as(
            "TL-4370 quote view is closed").is_false()

    @pytest.mark.testcase("TL-4280")
    @pytest.mark.testcase("TL-4281")
    def test_sob_pending_email_document_popup(self):
        result = self._create_pending_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data,
                                                          target_cost=600)
        email_copy_to_valid = self.config_holder['email']

        quote_bk = result["quote_bk"]
        self.sales_order_board_page.open_page_tl()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_quote_id_column_filter(value=quote_bk)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.open_quote_details(quote_number=quote_bk)
        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        self.note_text = "asdfg"

        self.sales_order_board_quote_details_page_tl.set_target_cost(1)
        self.enter_note_page = self.sales_order_board_quote_details_page_tl.click_quote_it()
        self.enter_note_page.set_text_in_area(self.note_text)
        self.email_modal = self.enter_note_page.click_send_rate_button()

        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        emails_to = self.email_modal.get_to_emails().split(os.linesep)
        emails_copy_to = self.email_modal.get_copy_to_emails().split(os.linesep)

        with soft_assertions():
            assert_that(self.email_modal.is_modal_opened()).described_as(
                "TL-4280 Email document modal is opened").is_true()
            assert_that(self.email_modal.is_pdf_checked()).described_as("TL-4280 PDF is checked by default").is_true()
            assert_that(self.email_modal.is_html_checked()).described_as("TL-4280 HTML is checked by default").is_true()
            assert_that(self.email_modal.get_email_subject()).is_equal_to(f"Truckload Quote ({quote_bk})")
            assert_that(emails_copy_to).described_as("TL-4280 emails copy to contains proper email").contains(
                email_copy_to_valid)
            assert_that(emails_to).described_as("TL-4280 emails to contains sales rep email").contains(
                result["sales_rep_email"])

        self.email_modal.click_cancel()
        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_status_is_visible()).described_as(
                "TL-4281 modal is closed, status is invisible").is_false()

    @pytest.mark.testcase("TL-4282")
    def test_sob_pending_email_document_is_sent(self):
        copy_to_email_address = "pendingq_email_copy_to.v09jltbo@mailosaur.io"
        to_email_address = "pendingq_email_to.v09jltbo@mailosaur.io"

        create_tl_quote_data = self.create_tl_quote_api_data
        create_tl_quote_data["quotedByInfo"]["email"] = copy_to_email_address
        create_tl_quote_data["bookedBy"]["email"] = copy_to_email_address
        result = self._create_pending_quoted_quote_in_api(quote_api_data=create_tl_quote_data,
                                                          target_cost=600)

        quote_bk = result["quote_bk"]
        self.sales_order_board_page.open_page_tl()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.click_on_pending_quotes()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_quote_id_column_filter(value=quote_bk)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.open_quote_details(quote_number=quote_bk)
        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        self.note_text = "asdfg"

        self.sales_order_board_quote_details_page_tl.set_target_cost(1)
        self.enter_note_page = self.sales_order_board_quote_details_page_tl.click_quote_it()
        self.enter_note_page.set_text_in_area(self.note_text)
        self.email_modal = self.enter_note_page.click_send_rate_button()

        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()
        self.email_copy_to_address = self.create_tl_quote_api_data["quotedByInfo"]["email"]
        text_to_send = f"Test email body for quote bk: {quote_bk}"
        required_subject = f"Truckload Quote ({quote_bk})"

        self.email_modal.set_email_body(text_to_send)
        self.email_modal.set_email_to(to_email_address)
        self.email_modal.set_email_copy_to(copy_to_email_address)

        self.email_modal.click_send()
        self.email_modal.wait_for_email_sent_status()
        self.email_modal.click_okay()

        self.sales_order_board_page.wait_for_page_loader_to_show_and_disappear()

        self.mail_api = MailAPI(self.log)
        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_status_is_visible()).described_as(
                "TL-4281 modal is closed, status is invisible").is_false()
            emails_from_email_to = self.mail_api.get_email(to_email_address)
            emails_from_email_copy_to = self.mail_api.get_email(copy_to_email_address)

            assert_that(emails_from_email_to.subject).described_as(
                "TL-4282 email subject equal as required in email to").is_equal_to(required_subject)
            assert_that(emails_from_email_copy_to.subject).described_as(
                "TL-4282 email subject equal as required in email copy to").is_equal_to(required_subject)

            assert_that(emails_from_email_to.html.body).described_as(
                "TL-4282 email body contains sent text in email to").contains(text_to_send)
            assert_that(emails_from_email_copy_to.html.body).described_as(
                "TL-4282 email body contains sent text in email copy to").contains(text_to_send)

    def _create_pending_quoted_quote_in_api(self, quote_api_data, target_cost):
        # Create a pending quote through API
        result = self.create_order_helpers_api.create_quote_through_api(quote_api_data['customerInfo'].get("name"),
                                                                        quote_api_data)
        return result
