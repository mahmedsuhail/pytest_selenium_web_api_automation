import time
from datetime import datetime

import pytest
from assertpy import assert_that

import utilities.custom_logger as cl
from API.Helpers.create_order_helpers_api import CreateOrderHelpersAPI
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL
from pages.TL.salesorder.so_statuses_page import SOStatusesPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.TL.salesorderboard.sales_order_board_pending_quote_details_page_tl import \
    SalesOrderBoardQuoteDetailsPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from utilities.read_data import read_csv_data, read_json_file
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login")
class TestSOBPendingTests:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    create_tl_order_api_data = read_json_file("API/order/create_tl_order_model_with_carrier.json")
    create_tl_order_item_api_data = read_json_file("API/order/create_tl_order_item_model.json")
    log = cl.test_logger(filename=__qualname__)
    util = Util(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.order_api = APIOrderAPI(self.api, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_board_quote_details_page_tl = SalesOrderBoardQuoteDetailsPageTL(self.driver,
                                                                                         self.log)
        self.sales_order_statuses_page = SOStatusesPage(self.driver, self.log)

        self.create_order_helpers_api = CreateOrderHelpersAPI(api=self.api, logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login(self):
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.sales_order_board_page.get_url_sales_order_board())

    @pytest.mark.testcase("TL-6619")
    def test_sob_pending_view_stop_save_change_status(self):
        bol_number, order_bk, customer_bk = self._create_order_through_api(
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            create_tl_order_api_data=self.create_tl_order_api_data)

        self.sales_order_board_page.open_page_tl()
        time.sleep(2)
        self.sales_order_board_page.click_on_pending_tab()
        self.sales_order_board_page.clear_filters()

        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()

        self.sales_order_board_table.open_order(order_bk)

        self.sales_order_page.wait_for_order_to_be_loaded()
        assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
            "TL-6619 Status should be pending").is_equal_to("PENDING")

        self.view_stops_page = self.sales_order_page.click_view_stops_button()
        self.view_stops_page.click_save()

        assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
            "TL-6619 Status should be pending still after save on View stops without any change").is_equal_to("PENDING")

        self.view_stops_page = self.sales_order_page.click_view_stops_button()

        row = self.view_stops_page.get_row_by_number(0)
        date_form = row.open_actual_date_form()
        date_form.set_date_from_current_to_plus_1_day()
        date_form.set_check_in("00:00")
        date_form.set_check_out("00:15")
        date_form.click_save_and_close()
        row.set_late_reason("Accident")
        self.view_stops_page.click_save()
        self.sales_order_page.wait_for_page_loader_to_show_and_disappear()

        assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
            "TL-6619 Status should be in transit").is_equal_to(
            "IN TRANSIT")

    @pytest.mark.testcase("TL-6774")
    def test_sob_pending_shouldnt_change_to_transit_by_changing_appointment_date(self):
        bol_number, order_bk, customer_bk = self._create_order_through_api(
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            create_tl_order_api_data=self.create_tl_order_api_data)

        self.sales_order_board_page.open_page_tl_sales_order(order_bk)

        self.sales_order_page.wait_for_order_to_be_loaded()
        assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
            "TL-6774 Status should be pending").is_equal_to("PENDING")

        self.view_stops_page = self.sales_order_page.click_view_stops_button()
        row = self.view_stops_page.get_row_by_number(0)

        date_to_select_str = self.util.getDateValueWithoutHolidayAndWeekends(no_of_days=2)
        date_to_select = datetime.strptime(date_to_select_str, '%m/%d/%Y')
        row.change_pickup_date(date_to_select)
        self.view_stops_page.click_save()

        self.sales_order_page.wait_for_page_loader_to_show_and_disappear()

        assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
            "TL-6774 Status should be pending still after save on View stops without any change").is_equal_to(
            "PENDING")

    def _create_order_through_api(self, create_tl_order_api_data, create_tl_order_item_api_data):
        bol_number, order_bk, customer_bk = self.create_order_helpers_api.create_order_through_api(
            customer_name=self.quote_data[1]["customerName"], create_tl_order_api_data=create_tl_order_api_data,
            create_tl_order_item_api_data=create_tl_order_item_api_data)

        return bol_number, order_bk, customer_bk
