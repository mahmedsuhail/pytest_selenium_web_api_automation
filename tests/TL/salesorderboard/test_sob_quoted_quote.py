import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from API.Helpers.create_order_helpers_api import CreateOrderHelpersAPI
from API.Types.order_statuses_dict import OrderStatusDict
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_popup_page import CarrierFulfillmentPopupPage
from pages.TL.createquote.carrier_fulfillment.carrier_fulfillment_section_page import CarrierFulfillmentSectionPage
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_saved_order_popup_page import CreateQuoteSavedOrderPopupPageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL, SalesOrderConfirmationPopup
from pages.TL.salesorder.so_statuses_page import SOStatusesPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.TL.salesorderboard.sales_order_board_pending_quote_details_page_tl import \
    SalesOrderBoardQuoteDetailsPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data, read_json_file
from utilities.validators import Validators


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login")
class TestSOBQuotedQuoteTests:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    create_tl_quote_api_data = read_json_file("API/order/create_tl_quote_model.json")
    create_tl_quote_api_data_credit_check_fails = read_json_file(
        "API/order/create_tl_quote_model_customer_credit_check_fails.json")
    create_tl_quote_documents_api_data = read_json_file("API/order/create_tl_quote_model_documents.json")
    quoted_quote_data = read_json_file("TL/salesorderboard/tl_sob_quoted_quote_data.json")
    quoted_quote_data_credit_check_fails = read_json_file(
        "TL/salesorderboard/tl_sob_quoted_quote_financials_customer_credit_check_fails.json")
    log = cl.test_logger(filename=__qualname__)
    validators = Validators(logger=log)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.order_api = APIOrderAPI(self.api, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_statuses_page = SOStatusesPage(self.driver, self.log)
        self.sales_order_page_confirmation_popup = SalesOrderConfirmationPopup(self.driver, self.log)
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_board_quote_details_page_tl = SalesOrderBoardQuoteDetailsPageTL(self.driver,
                                                                                         self.log)
        self.create_quote_page = CreateQuotePageTL(self.driver, self.log)
        self.create_quote_route_details_page = TLRouteDetailsPage(self.driver, self.log)
        self.create_quote_sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        self.create_quote_financials_page = FinancialsPage(self.driver, self.log)
        self.create_order_helpers_api = CreateOrderHelpersAPI(api=self.api, logger=self.log)
        self.create_quote_shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        self.create_quote_carrier_fulfillment_page = CarrierFulfillmentSectionPage(self.driver, self.log)
        self.create_quote_carrier_fulfillment_popup = CarrierFulfillmentPopupPage(self.driver, self.log)
        self.create_order_popup = CreateQuoteSavedOrderPopupPageTL(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login(self):
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.sales_order_board_page.get_url_sales_order_board())

    @pytest.mark.testcase("TL-3760")
    @pytest.mark.testcase("TL-3761")
    def test_sob_user_can_see_quoted_quote_view(self):
        self.sales_order_board_page.open_page_tl()
        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data, target_cost=600)

        self.sales_order_board_page.click_on_quoted_tab()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_quote_id_column_filter(value=quote_bk)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.open_quote_details(quote_number=quote_bk)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_quoted_marker_is_visible()) \
                .is_true(), "Quoted Marker Should be visible"
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_quote_document_is_visible()) \
                .is_true(), "Quote Document Should be visible"
            assert_that(self.sales_order_board_quote_details_page_tl.check_if_build_load_button_is_visible()) \
                .is_true(), "Build Load button Should be visible"

            assert_that(self.sales_order_board_quote_details_page_tl.get_customer_name()) \
                .is_equal_to(self.quoted_quote_data.get("customer")), "Customer Name should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_service_type()) \
                .is_equal_to(self.quoted_quote_data.get("serviceType")), "serviceType should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_shipment_type()) \
                .is_equal_to(self.quoted_quote_data.get("shipmentType")), "shipmentType should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_origin()) \
                .is_equal_to(self.quoted_quote_data.get("origin")), "origin should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_destination()) \
                .is_equal_to(self.quoted_quote_data.get("destination")), "destination should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_commodity_description()) \
                .is_equal_to(
                self.quoted_quote_data.get("commodityDescription")), "commodityDescription should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_pieces()) \
                .is_equal_to(
                self.quoted_quote_data.get("pieceCount")), "pieceCount should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_weight()) \
                .is_equal_to(self.quoted_quote_data.get("weight")), "weight should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_pallets()) \
                .is_equal_to(self.quoted_quote_data.get("palletCount")), "palletCount should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_handling_unit_type()) \
                .is_equal_to(self.quoted_quote_data.get("unitType")), "unitType should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_service()) \
                .is_equal_to(self.quoted_quote_data.get("service")), "service should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_equipment()) \
                .is_equal_to(self.quoted_quote_data.get("equipment")), "equipment should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_length()) \
                .is_equal_to(self.quoted_quote_data.get("length")), "length should match"

            assert_that(self.sales_order_board_quote_details_page_tl.get_actual_cost()) \
                .is_equal_to(self.quoted_quote_data.get("actualCost")), "actualCost should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_revenue()) \
                .is_equal_to(self.quoted_quote_data.get("revenue")), "revenue should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_max_buy()) \
                .is_equal_to(self.quoted_quote_data.get("maxBuy")), "maxBuy should match"
            assert_that(self.sales_order_board_quote_details_page_tl.get_target_cost()) \
                .is_equal_to(self.quoted_quote_data.get("targetCost")), "targetCost should match"

    @pytest.mark.testcase("TL-4802")
    @pytest.mark.testcase("TL-4862")
    @pytest.mark.testcase("TL-4856")
    @pytest.mark.testcase("TL-5097")
    def test_sob_user_can_create_new_load_if_all_required_fields_are_populated(self):
        self.sales_order_board_page.open_page_tl()

        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data, target_cost=600)
        self._open_quoted_tab_and_click_build_load(quote_bk=quote_bk)

        assert_that(self.create_quote_page.check_if_create_quote_page_is_opened()).described_as(
            "Create Quote Page should be visible") \
            .is_true()

        # Verifying Create Quote Data is correctly populated
        with soft_assertions():
            # Verifying Customer Section
            assert_that(self.create_quote_page.get_customer_search_box_value()) \
                .is_equal_to(self.quoted_quote_data.get("customer")), "Customer Name should match"
            assert_that(self.create_quote_page.get_service_type_dropdown_value()) \
                .is_equal_to(self.quoted_quote_data.get("serviceTypeShort")), "serviceType should match"
            assert_that(self.create_quote_page.get_selected_shipment_type()) \
                .is_equal_to(self.quoted_quote_data.get("shipmentTypeShort")), "shipmentType should match"
            assert_that(self.create_quote_page.get_service_dropdown_value()) \
                .is_equal_to(self.quoted_quote_data.get("service")), "service should match"
            assert_that(self.create_quote_page.get_selected_equipment_type()) \
                .is_equal_to(self.quoted_quote_data.get("equipment")), "equipment should match"
            assert_that(self.create_quote_page.get_selected_equipment_length()) \
                .is_equal_to(self.quoted_quote_data.get("equipmentLengthShort")), "equipment length should match"

            # Verifying Route Details Section
            assert_that(self.create_quote_route_details_page.get_origin_address_text()) \
                .is_equal_to(self.quoted_quote_data.get("originRouteDetails")), "origin should match"
            assert_that(self.create_quote_route_details_page.get_destination_address_text()) \
                .is_equal_to(self.quoted_quote_data.get("destinationRouteDetails")), "destination should match"

            # Verifying Origin & Destination Container

            self.create_quote_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
            self.create_quote_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)

            address1, address2, origin_city, origin_zip = self.create_quote_origin_page.get_address()

            assert_that(origin_city) \
                .is_equal_to(self.quoted_quote_data.get("originCity")), "origin address city should match"
            assert_that(origin_zip) \
                .is_equal_to(self.quoted_quote_data.get("originZip")), "origin address zip should match"

            address1, address2, destination_city, destination_zip = self.create_quote_destination_page.get_address()

            assert_that(destination_city).described_as("destination address city should match") \
                .is_equal_to(self.quoted_quote_data.get("destinationCity"))
            assert_that(destination_zip).described_as("destination address zip should match") \
                .is_equal_to(self.quoted_quote_data.get("destinationZip"))

            # Verifying Shipment section
            so_items = self.create_quote_sales_order_page.get_sale_item_rows()

            assert_that(so_items[0].get_commodity_text()) \
                .is_equal_to(
                self.quoted_quote_data.get("commodityDescription")), "commodityDescription should match"
            assert_that(self.create_quote_sales_order_page.get_total_shipment_value()) \
                .is_equal_to(
                self.quoted_quote_data.get("expectedTotalShipmentValue")), "expectedTotalShipmentValue should match"

            # Verifying Financials
            assert_that(self.create_quote_financials_page.get_actual_cost()) \
                .is_equal_to(self.quoted_quote_data.get("actualCost")), "actualCost should match"
            assert_that(self.create_quote_financials_page.get_revenue()) \
                .is_equal_to(self.quoted_quote_data.get("revenue")), "revenue should match"
            assert_that(self.create_quote_financials_page.get_max_buy()) \
                .is_equal_to(self.quoted_quote_data.get("maxBuy").replace('$', '')), "maxBuy should match"
            assert_that(self.create_quote_financials_page.get_target_cost()) \
                .is_equal_to(self.quoted_quote_data.get("targetCost").replace('$', '')), "targetCost should match"
            assert_that(self.create_quote_financials_page.get_margin()) \
                .is_equal_to(self.quoted_quote_data.get("margin")), "margin should match"
            assert_that(self.create_quote_financials_page.get_price_to_beat()) \
                .is_equal_to(self.quoted_quote_data.get("priceToBeat").replace('$', '')), "priceToBeat should match"
            assert_that(self.create_quote_financials_page.get_margin_percentage()) \
                .is_equal_to(self.quoted_quote_data.get("marginPercentage")), "marginPercentage should match"

        # Making some modifications to an order
        self.create_quote_page.select_equipment_type(self.quoted_quote_data.get("newEquipmentType"))
        self.create_quote_page.select_equipment_length(self.quoted_quote_data.get("newEquipmentLength"))

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.quoted_quote_data["additionalShippingItem"].get("commodityDescription")) \
            .with_piece_count(self.quoted_quote_data["additionalShippingItem"].get("pieceCount")) \
            .with_weight(self.quoted_quote_data["additionalShippingItem"].get("weight")) \
            .with_pallet_count(self.quoted_quote_data["additionalShippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        self.create_quote_shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        self.create_quote_shipping_items_page.click_add_to_order_button()

        with soft_assertions():
            assert_that(self.create_quote_page.get_selected_equipment_type()).described_as(
                "new equipment type should match") \
                .is_equal_to(self.quoted_quote_data.get("newEquipmentType"))
            assert_that(self.create_quote_page.get_selected_equipment_length()).described_as(
                "new equipment length should match") \
                .is_equal_to(self.quoted_quote_data.get("newEquipmentLength"))

            so_items = self.create_quote_sales_order_page.get_sale_item_rows()

            assert_that(len(so_items)).described_as("Two shipping items should be added").is_equal_to(2)
            assert_that(so_items[1].get_commodity_text()).is_equal_to(shipping_item.commodity_description)
            assert_that(self.create_quote_sales_order_page.get_total_shipment_value()).is_equal_to(
                self.quoted_quote_data.get("expectedTotalShipmentValue"))

        # TL-4862 Filling Origin & Destination data & building an order
        self._fill_origin_and_destination_data_and_build_order()

        with soft_assertions():
            assert_that(self.create_order_popup.is_popup_displayed()).described_as(
                "Order success message should be visible").is_true()
            assert_that(self.create_order_popup.is_go_to_sales_order_button_displayed()).described_as(
                "Go to sales order board button should be visible").is_true()
            assert_that(self.create_order_popup.is_go_to_bol_button_displayed()).described_as(
                "Go to bol button should be visible").is_true()

        # TL-4856 Go to Sales Order Board and verify
        self.create_order_popup.click_go_to_sales_order_button()
        assert_that(self.sales_order_board_page.is_sales_order_board_displayed()).described_as(
            "Sales Order Board Page Should be opened").is_true()

    @pytest.mark.testcase("TL-4857")
    @pytest.mark.testcase("TL-4860")
    def test_sob_user_can_navigate_to_created_sales_order_page_after_building_a_load(self):
        self.sales_order_board_page.open_page_tl()

        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data, target_cost=600)
        self._open_quoted_tab_and_click_build_load(quote_bk=quote_bk)

        assert_that(self.create_quote_page.check_if_create_quote_page_is_opened()).described_as(
            "Create Quote Page should be visible") \
            .is_true()

        # Filling Origin & Destination data & building an order
        self._fill_origin_and_destination_data_and_build_order()

        # Go to BOL and verify
        self.create_order_popup.click_go_to_bol()
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()

        assert_that(self.sales_order_page.is_page_displayed()).described_as(
            "Sales Order Page Should be opened").is_true()

        # TL-4860 Verify that status is set to available
        assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
            "TL-4860 Available order status").is_equal_to("AVAILABLE")

    @pytest.mark.testcase("TL-4858")
    def test_sob_user_cannot_create_new_load_if_all_required_fields_are_not_populated(self):
        self.sales_order_board_page.open_page_tl()

        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data, target_cost=600)
        self._open_quoted_tab_and_click_build_load(quote_bk=quote_bk)

        assert_that(self.create_quote_page.check_if_create_quote_page_is_opened()).described_as(
            "Create Quote Page should be visible") \
            .is_true()

        # Making some modifications to an order
        self.create_quote_page.select_equipment_type(self.quoted_quote_data.get("newEquipmentType2"))

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.quoted_quote_data["additionalShippingItem"].get("commodityDescription")) \
            .with_piece_count(self.quoted_quote_data["additionalShippingItem"].get("pieceCount")) \
            .with_weight(self.quoted_quote_data["additionalShippingItem"].get("weight")) \
            .with_pallet_count(self.quoted_quote_data["additionalShippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        self.create_quote_shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        self.create_quote_shipping_items_page.click_add_to_order_button()

        self.create_quote_page.click_build_order_footer_button()

        with soft_assertions():
            assert_that(self.create_order_popup.is_popup_displayed()).described_as(
                "Order success message should not be visible").is_false()
            assert_that(self.create_quote_page.get_selected_equipment_type()).described_as(
                "new equipment type should match") \
                .is_equal_to(self.quoted_quote_data.get("newEquipmentType2"))
            assert_that(self.create_quote_page.is_create_quote_container_header_highlighted()).described_as(
                "Create Quote container Header colour changed to red").is_true()
            assert_that(self.create_quote_page.verify_equipment_length_field_value("Required")).described_as(
                "Equipment type length field should be highlighted").is_true()

            # Required fields Validations Origin Container
            self.verify_origin_container_mandatory_fields()

            # Required fields Validations Destination Container
            self._verify_destination_container_mandatory_fields()

    @pytest.mark.testcase("TL-4859")
    def test_sob_user_cannot_create_new_load_if_customer_credit_check_fails(self):
        self.sales_order_board_page.open_page_tl()

        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data_credit_check_fails,
                                                    target_cost=600)
        self._open_quoted_tab_and_click_build_load(quote_bk=quote_bk)

        assert_that(self.create_quote_page.check_if_create_quote_page_is_opened()).described_as(
            "Create Quote Page should be visible") \
            .is_true()

        # Verifying revenue
        with soft_assertions():
            assert_that(self.create_quote_financials_page.get_revenue()) \
                .is_equal_to(self.quoted_quote_data_credit_check_fails.get("revenue")), "revenue should match"

        # Adding Shipping Item
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(
            self.quoted_quote_data_credit_check_fails["additionalShippingItem"].get("commodityDescription")) \
            .with_piece_count(self.quoted_quote_data_credit_check_fails["additionalShippingItem"].get("pieceCount")) \
            .with_weight(self.quoted_quote_data_credit_check_fails["additionalShippingItem"].get("weight")) \
            .with_pallet_count(self.quoted_quote_data_credit_check_fails["additionalShippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        self.create_quote_shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        self.create_quote_shipping_items_page.click_add_to_order_button()

        # Filling Origin & Destination data & building an order
        self._fill_origin_and_destination_data_and_build_order()

        with soft_assertions():
            assert_that(self.create_order_popup.is_popup_displayed()).described_as(
                "Order success message should not be visible").is_false()
            assert_that(self.create_order_popup.is_go_to_sales_order_button_displayed()).described_as(
                "Go to sales order board button should not be visible").is_false()
            assert_that(self.create_order_popup.is_go_to_bol_button_displayed()).described_as(
                "Go to bol button should not be visible").is_false()

        # TODO - Add Popup verification here

    @pytest.mark.jira("TL-7684")
    def test_sob_user_cannot_create_new_load_if_customer_credit_check_fails_verify_popup(self):
        # TODO - verify customer credit check popup in test above when TL-7684 defect will be fixed
        assert False

    @pytest.mark.testcase("TL-4861")
    def test_sob_shipment_status_is_pending_if_carrier_was_assigned_during_build_load(self):
        self.sales_order_board_page.open_page_tl()

        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data, target_cost=600)

        self._open_quoted_tab_and_click_build_load(quote_bk=quote_bk)

        assert_that(self.create_quote_page.check_if_create_quote_page_is_opened()).described_as(
            "Create Quote Page should be visible") \
            .is_true()

        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.quoted_quote_data["additionalShippingItem"].get("commodityDescription")) \
            .with_piece_count(self.quoted_quote_data["additionalShippingItem"].get("pieceCount")) \
            .with_weight(self.quoted_quote_data["additionalShippingItem"].get("weight")) \
            .with_pallet_count(self.quoted_quote_data["additionalShippingItem"].get("palletCount")) \
            .with_weight_units('lbs') \
            .with_dim_units('Inches') \
            .build()

        self.create_quote_shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        self.create_quote_shipping_items_page.click_add_to_order_button()

        carrier_name = self.quoted_quote_data.get("carrierNameToSelect")
        self._search_and_select_carrier(carrier_name=carrier_name)

        assert_that(self.create_quote_carrier_fulfillment_page.get_carrier_name()).described_as(
            "Assigned Carrier Name").is_equal_to(carrier_name)

        # Filling Origin & Destination data & building an order
        self._fill_origin_and_destination_data_and_build_order()

        # Go to BOL and verify
        self.create_order_popup.click_go_to_bol()
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()

        assert_that(self.sales_order_page.is_page_displayed()).described_as(
            "Sales Order Page Should be opened").is_true()

        # TL-4861 Verify that status is set to PENDING
        assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
            "TL-4861 Available order status").is_equal_to("PENDING")

    @pytest.mark.testcase("TL-6656")
    def test_sob_status_should_be_pending_after_build_load(self):
        self.sales_order_board_page.open_page_tl()

        quote_bk = self._create_quoted_quote_in_api(quote_api_data=self.create_tl_quote_api_data, target_cost=100)

        self._open_quoted_tab_and_click_build_load(quote_bk=quote_bk)

        assert_that(self.create_quote_page.check_if_create_quote_page_is_opened()).described_as(
            "TL-6656 Create Quote Page should be visible") \
            .is_true()

        carrier_name = self.quoted_quote_data.get("carrierNameToSelect")
        self._search_and_select_carrier(carrier_name=carrier_name)

        assert_that(self.create_quote_carrier_fulfillment_page.get_carrier_name()).described_as(
            "TL-6656 Assigned Carrier Name").is_equal_to(carrier_name)

        self._fill_origin_and_destination_data_and_build_order()

        self.create_order_popup.click_go_to_bol()
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_page.is_page_displayed()).described_as(
                "TL-6656 Sales Order Page Should be opened").is_true()
            assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
                "TL-6656 Available order status").is_equal_to("PENDING")

    def _create_quoted_quote_in_api(self, quote_api_data, target_cost):
        # Create a pending quote through API
        result = self.create_order_helpers_api.create_quote_through_api(quote_api_data['customerInfo'].get("name"),
                                                                        quote_api_data)

        quote_bk = result["quote_bk"]
        quote_details = self.order_api.get_quote_details(quote_bk=quote_bk)

        # Update quote through API and set it to quoted
        update_order_model = self.create_order_helpers_api.get_update_order_model_from_get_order_details_response(
            order_model=quote_details)
        update_order_model.set_target_cost(target_cost)
        update_order_model.add_documents(documents=self.create_tl_quote_documents_api_data)
        update_order_model.set_order_status(status_id=OrderStatusDict.order_statuses['QUOTED'])

        self.order_api.update_order(update_order_model=update_order_model)

        return quote_bk

    def _open_quoted_tab_and_click_build_load(self, quote_bk):
        self.sales_order_board_page.click_on_quoted_tab()
        self.sales_order_board_page.clear_filters()
        self.sales_order_board_page.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_quote_id_column_filter(value=quote_bk)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_page.open_quote_details(quote_number=quote_bk)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()
        self.sales_order_board_quote_details_page_tl.click_build_load()
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()

    def _fill_origin_and_destination_data_and_build_order(self):
        # TL-4862 Filling Origin & Destination data

        self.create_quote_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self.create_quote_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)

        self.create_quote_origin_page.enter_company_name(
            company_name=self.quoted_quote_data["originContainerData"].get("companyName"))
        self.create_quote_origin_page.enter_contact_name(
            contact_name=self.quoted_quote_data["originContainerData"].get("contactName"))
        self.create_quote_origin_page.enter_contact_phone(
            contact_phone=self.quoted_quote_data["originContainerData"].get("contactPhone"))
        self.create_quote_origin_page.type_to_address(
            addr1=self.quoted_quote_data["originContainerData"].get("address"))

        self.create_quote_destination_page.enter_company_name(
            company_name=self.quoted_quote_data["destinationContainerData"].get("companyName"))
        self.create_quote_destination_page.enter_contact_name(
            contact_name=self.quoted_quote_data["destinationContainerData"].get("contactName"))
        self.create_quote_destination_page.enter_contact_phone(
            contact_phone=self.quoted_quote_data["destinationContainerData"].get("contactPhone"))
        self.create_quote_destination_page.type_to_address(
            addr1=self.quoted_quote_data["destinationContainerData"].get("address"))

        # TL-4862 Building an order
        self.create_quote_page.click_build_order_footer_button()
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear()

    def verify_origin_container_mandatory_fields(self):
        self.create_quote_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)

        assert_that(self.validators.verify_text_match(
            self.create_quote_origin_page.get_place_holder_value_of_field_company_name_in_rl_container(),
            "Required")).described_as("Company Name field is mandatory in origin route location container.").is_true()

        assert_that(self.validators.verify_text_match(
            self.create_quote_origin_page.get_place_holder_value_of_field_contact_name_in_rl_container(),
            "Required")).described_as("Contact Name field is mandatory in origin route location container.").is_true()

        assert_that(self.validators.verify_text_match(
            self.create_quote_origin_page.get_place_holder_value_of_field_contact_phone_in_rl_container(),
            "Required")).described_as("Contact Phone field is mandatory in origin route location container.").is_true()

        assert_that(self.validators.verify_text_match(
            self.create_quote_origin_page.get_place_holder_value_of_field_address_line1_in_rl_container(),
            "Required")).described_as("Address Line1 field is mandatory in origin route location container.").is_true()

    def _verify_destination_container_mandatory_fields(self):
        self.create_quote_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)

        assert_that(self.validators.verify_text_match(
            self.create_quote_destination_page.get_place_holder_value_of_field_company_name_in_rl_container(),
            "Required")).described_as(
            "Company Name field is mandatory in destination route location container.").is_true()

        assert_that(self.validators.verify_text_match(
            self.create_quote_destination_page.get_place_holder_value_of_field_contact_name_in_rl_container(),
            "Required")).described_as(
            "Contact Name field is mandatory in destination route location container.").is_true()

        assert_that(self.validators.verify_text_match(
            self.create_quote_destination_page.get_place_holder_value_of_field_contact_phone_in_rl_container(),
            "Required")).described_as(
            "Contact Phone field is mandatory in destination route location container.").is_true()

        assert_that(self.validators.verify_text_match(
            self.create_quote_destination_page.get_place_holder_value_of_field_address_line1_in_rl_container(),
            "Required")).described_as(
            "Address Line1 field is mandatory in destination route location container.").is_true()

    def _search_and_select_carrier(self, carrier_name):
        self.create_quote_carrier_fulfillment_page.click_select_carrier_button()
        self.create_quote_carrier_fulfillment_page.search_and_select_carrier(search_string=carrier_name,
                                                                             carrier_name=carrier_name)
        self.create_quote_carrier_fulfillment_popup.click_save_and_assign_button()
