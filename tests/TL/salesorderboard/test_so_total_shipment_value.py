import pytest

import utilities.custom_logger as cl
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.createquote.create_quote_saved_order_popup_page import CreateQuoteSavedOrderPopupPageTL
from pages.TL.createquote.create_quote_shipping_items_page import CreateQuoteShippingItemsPage
from pages.TL.createquote.financials_page import FinancialsPage
from pages.TL.createquote.routelocations_destination_page import RouteLocationsDestinationPage
from pages.TL.createquote.routelocations_origin_page import RouteLocationsOriginPage
from pages.TL.createquote.sales_order_page import CreateQuoteSalesOrderPageTL
from pages.TL.createquote.tl_route_details_page import TLRouteDetailsPage
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL
from pages.basictypes.service_type import ServiceType
from pages.common.builders.shipping_item_builder import ShippingItemBuilder
from utilities.read_data import read_csv_data, read_json_file


@pytest.mark.usefixtures("oneTimeSetUpWithAPI")
class TestSOTotalShipmentValue:
    login_data = read_csv_data("login_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.so = SalesOrderPageTL(self.driver, self.log)

    def __create_order(self, total_shipment_value):
        # preconditions
        self.shipping_items_data = read_csv_data("shippingItems_for_tl_sales_order_tests.csv")
        self.route_details_data = read_csv_data("tl_route_details.csv")
        self.qop = CreateQuotePageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.qop.get_url_create_quote())
        self.quote_data = read_csv_data("quote_details.csv")

        self.qop.search_and_select_customer_by_index(self.quote_data[1]["customerName"])
        self.qop.select_service_type(service_type=ServiceType.TL)
        self.qop.wait_for_page_loader_to_show_and_disappear()

        self.test_data = read_json_file("tl_create_quote_origin_destintation.json")
        selected_company_origin = self.test_data["origin_data2"]["company"]
        selected_equipment_type = "Van"
        selected_equipment_length = "53"
        selected_shipment_type = "Spot Quote"
        self.qop.select_equipment_type(selected_equipment_type)
        self.qop.select_shipment_type(selected_shipment_type)
        self.qop.select_equipment_length(selected_equipment_length)

        # add route details
        origin_zip_code = self.route_details_data[0]["originZipCode"]
        destination_zip_code = self.route_details_data[0]["destinationZipCode"]
        tl_route_details_page = TLRouteDetailsPage(self.driver, self.log)

        tl_route_details_page.select_origin_by_zip_code_exact_match(origin_zip_code)
        tl_route_details_page.select_destination_by_zip_code_exact_match(destination_zip_code)

        self._rl_origin_page = RouteLocationsOriginPage(driver=self.driver, log=self.log)
        self._rl_origin_page.select_by_company(selected_company_origin)
        self._rl_destination_page = RouteLocationsDestinationPage(driver=self.driver, log=self.log)
        self._rl_destination_page.select_by_company(selected_company_origin)

        # add first shipping item
        row_id = 0
        shipping_item = ShippingItemBuilder() \
            .with_commodity_description(self.shipping_items_data[row_id]["commodity"]) \
            .with_piece_count(self.shipping_items_data[row_id]["piece"]) \
            .with_weight(self.shipping_items_data[row_id]["weight"]) \
            .with_weight_units(self.shipping_items_data[row_id]["UnitType"]) \
            .with_pallet_count(self.shipping_items_data[row_id]["unitcount"]) \
            .with_length(self.shipping_items_data[row_id]["length"]) \
            .with_width(self.shipping_items_data[row_id]["width"]) \
            .with_height(self.shipping_items_data[row_id]["height"]) \
            .with_dim_units(self.shipping_items_data[row_id]["Dim"]) \
            .build()

        shipping_items_page = CreateQuoteShippingItemsPage(self.driver, self.log)
        shipping_items_page.enter_shipping_item_data(shipping_item=shipping_item)
        shipping_items_page.click_add_to_order_button()

        sales_order_page = CreateQuoteSalesOrderPageTL(self.driver, self.log)
        sales_order_page.wait_for_sale_item_have_count_and_return(1, 30)
        sales_order_page.set_total_shipment_value(total_shipment_value)

        financials_page = FinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_charge_rows = financials_page.get_charge_table_rows()
        financials_charge_rows[0].set_revenue("0.01")
        financials_page.click_save()

        self.qop.click_create_order_footer_button()
        create_order_popup = CreateQuoteSavedOrderPopupPageTL(self.driver, self.log)
        create_order_popup.wait_for_create_order_popup_visible()
        create_order_popup.click_go_to_bol()
