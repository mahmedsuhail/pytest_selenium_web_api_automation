import pytest
from assertpy import assert_that, soft_assertions

import utilities.custom_logger as cl
from API.Models.save_tl_board_custom_column_filter_single_request_model import \
    SaveTruckloadBoardCustomColumnFilterSingleRequestModel
from API.Models.save_tl_board_custom_request_model import SaveTruckloadBoardCustomRequestModel
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestSOBBoardColumns:
    login_data = read_csv_data("login_details.csv")
    columns_names_to_verify = read_csv_data("salesorderboard_columns_to_show.csv")

    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        sob = SalesOrderBoardPageTL(self.driver, self.log)
        sob.open_page_tl()
        sob.wait_for_board_rows_to_be_loaded()

    @pytest.mark.testcase("TL-3136")
    @pytest.mark.testcase("TL-3139")
    @pytest.mark.testcase("TL-3143")
    @pytest.mark.testcase("TL-3147")
    @pytest.mark.testcase("TL-3148")
    @pytest.mark.testcase("TL-3149")
    @pytest.mark.slowtest
    def test_sob_popup_with_list_of_columns_select_deselect(self):
        self.__check_all_columns_on_all_boards_and_reload_page()
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)

        boards_names = self.sob.get_available_boards_names()
        for board in boards_names:
            self.sob.select_boards(table_name=board, wait_for_load=True)

            table = self.sob.get_table()
            columns = table.get_columns_sorted()

            filters = columns[0].get_all_available_columns_dropdown()

            assert_that(columns[0].is_popup_column_filter_visible()).described_as(
                "TL-3136 is column filter popup visible").is_true()

            columns[0].check_all_columns_in_filter_dropdown(filters)
            table.wait_for_board_loader_not_visible()
            filters_checked = sorted([item.name for item in filters if item.is_checked()])

            columns = table.get_columns_sorted()
            columns_names_before_uncheck = table.get_columns_names(columns)

            assert_that(filters_checked).described_as(f"TL-3139 selected columns on board {board}").is_equal_to(
                sorted(columns_names_before_uncheck))

            filters[0].uncheck()
            filters[1].uncheck()
            filters[2].uncheck()

            filter_name_uncheck_first = filters[0].name
            filter_name_uncheck_second = filters[1].name
            filter_name_uncheck_third = filters[2].name
            table.wait_for_board_loader_not_visible()

            columns[3].right_click_column_header()

            filters = columns[3].get_all_available_columns_dropdown()
            filters_checked = sorted([item.name for item in filters if item.is_checked()])

            columns = table.get_columns_sorted()
            columns_names_after_check = table.get_columns_names(columns)

            with soft_assertions():
                assert_that(filters_checked).described_as(f"TL-3143 deselected columns on board {board}").is_equal_to(
                    sorted(columns_names_after_check))
                assert_that(columns_names_after_check).described_as(
                    f"TL-3149 deselected columns are not on board {board}").does_not_contain(filter_name_uncheck_first)
                assert_that(columns_names_after_check).described_as(
                    f"TL-3149 deselected columns are not on board {board}").does_not_contain(filter_name_uncheck_second)
                assert_that(columns_names_after_check).described_as(
                    f"TL-3149 deselected columns are not on board {board}").does_not_contain(filter_name_uncheck_third)
            columns[3].close_column_popup()

            filters = columns[0].get_all_available_columns_dropdown()
            filters[0].check()
            new_column_name = filters[0].name

            columns = table.get_columns_sorted()
            columns_names_after = table.get_columns_names(columns)

            assert_that(columns_names_after).described_as(f"TL-3147 TL-3148 selected column is on board {board}") \
                .contains(new_column_name)

    @pytest.mark.testcase("TL-3144")
    @pytest.mark.testcase("TL-3145")
    @pytest.mark.testcase("TL-3178")
    @pytest.mark.slowtest
    def test_sob_popup_with_list_of_columns_is_the_same_for_all_columns_in_board(self):
        self.__check_all_columns_on_all_boards_and_reload_page()
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)

        boards_names = self.sob.get_available_boards_names()
        for board in boards_names:
            self.sob.select_boards(table_name=board, wait_for_load=True)
            table = self.sob.get_table()
            table.wait_for_board_loader_not_visible()
            columns = table.get_columns_sorted()
            table.scroll_to_div_test()

            columns_in_popup_first = [item.name for item in columns[0].get_all_available_columns_dropdown()]
            columns[0].right_click_column_header()

            with soft_assertions():
                for column in columns:
                    columns_in_popup_new = [item.name for item in column.get_all_available_columns_dropdown()]
                    scroll_visible = column.is_popup_column_filter_contain_scroll()
                    column.right_click_column_header()
                    assert_that(column.is_popup_column_filter_visible()).described_as(
                        ("TL-3145 popup not visible")).is_false()
                    assert_that(columns_in_popup_new).described_as(
                        "TL-3144 check if columns in popup are the same for all columns").is_equal_to(
                        columns_in_popup_first)
                    assert_that(scroll_visible).described_as("TL-3178 scroll is visible").is_true()

    @pytest.mark.testcase("TL-3155")
    @pytest.mark.jira("TL-7852")
    def test_sob_popup_all_changes_persist_after_refresh(self):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)

        boards_names = self.sob.get_available_boards_names()
        board = boards_names[0]
        self.sob.select_boards(table_name=board, wait_for_load=True)

        # --------- UNCHECK ---------------
        table = self.sob.get_table()
        columns = table.get_columns()

        filters = columns[0].get_all_available_columns_dropdown()

        first_filter_checked = next(item for item in filters if item.is_checked())
        first_filter_checked.uncheck()
        filter_name_removed = first_filter_checked.name

        columns_after_removed = table.get_columns()
        columns_names_after_removed = table.get_columns_names(columns_after_removed)
        filters_after_removed = columns_after_removed[0].get_all_available_columns_dropdown()
        filters_names_after_removed = sorted([item.name for item in filters_after_removed if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_removed).described_as(
                f"TL-3155 deselected column is not on board").does_not_contain(filter_name_removed)
            assert_that(sorted(columns_names_after_removed)).is_equal_to(filters_names_after_removed)

        self.sob.refresh_page()
        self.sob.wait_for_board_rows_to_be_loaded()
        table = self.sob.get_table()
        columns_after_removed_refresh = table.get_columns()
        columns_names_after_removed_refresh = table.get_columns_names(columns_after_removed_refresh)

        filters_after_removed_refresh = columns_after_removed_refresh[0].get_all_available_columns_dropdown()
        filters_names_after_removed_refresh = sorted(
            [item.name for item in filters_after_removed_refresh if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_removed).described_as(
                f"TL-3155 columns are the same after refresh on board {board}").is_equal_to(
                columns_names_after_removed_refresh)
            assert_that(filters_names_after_removed).described_as(
                f"TL-3155 popup filters are the same after refresh on board {board}").is_equal_to(
                filters_names_after_removed_refresh)

        # ---------- CHECK --------------

        first_filter_unchecked = next(item for item in filters_after_removed_refresh if not item.is_checked())
        first_filter_unchecked.check()
        filter_name_added = first_filter_unchecked.name
        table.wait_for_board_rows_to_be_loaded()

        columns_after_add = table.get_columns()
        columns_names_after_add = sorted(table.get_columns_names(columns_after_add))
        filters_after_add = columns_after_add[0].get_all_available_columns_dropdown()
        filters_names_after_add = sorted([item.name for item in filters_after_add if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_add).described_as(
                f"TL-3155 deselected column is not on board").contains(filter_name_added)

        self.sob.refresh_page()
        self.sob.wait_for_board_rows_to_be_loaded()
        table = self.sob.get_table()
        columns_after_add_refresh = table.get_columns()
        columns_names_after_add_refresh = sorted(table.get_columns_names(columns_after_add_refresh))

        filters_after_add_refresh = columns_after_add_refresh[0].get_all_available_columns_dropdown()
        filters_names_after_add_refresh = sorted(
            [item.name for item in filters_after_add_refresh if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_add).described_as(
                f"TL-3155 columns are the same after refresh on board {board}").is_equal_to(
                columns_names_after_add_refresh)
            assert_that(filters_names_after_add).described_as(
                f"TL-3155 popup filters are the same after refresh on board {board}").is_equal_to(
                filters_names_after_add_refresh)

    @pytest.mark.testcase("TL-3172")
    @pytest.mark.jira("TL-7852")
    def test_sob_popup_all_changes_persist_after_leave_page(self):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)

        boards_names = self.sob.get_available_boards_names()
        board_test = boards_names[0]
        self.__check_all_columns_on_selected_board(board_test)
        board_to_change = boards_names[2]
        self.sob.select_boards(table_name=board_test, wait_for_load=True)

        # --------- UNCHECK ---------------
        table = self.sob.get_table()
        columns = table.get_columns()

        filters = columns[0].get_all_available_columns_dropdown()

        first_filter_checked = next(item for item in filters if item.is_checked())
        first_filter_checked.uncheck()
        filter_name_removed = first_filter_checked.name

        columns_after_removed = table.get_columns()
        columns_names_after_removed = table.get_columns_names(columns_after_removed)
        filters_after_removed = columns_after_removed[0].get_all_available_columns_dropdown()
        filters_names_after_removed = sorted([item.name for item in filters_after_removed if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_removed).described_as(
                f"TL-3172 deselected column is not on board").does_not_contain(filter_name_removed)

        self.sob.select_boards(table_name=board_to_change, wait_for_load=True)
        self.sob.select_boards(table_name=board_test, wait_for_load=True)
        table = self.sob.get_table()
        columns_after_removed_refresh = table.get_columns()
        columns_names_after_removed_refresh = table.get_columns_names(columns_after_removed_refresh)

        filters_after_removed_refresh = columns_after_removed_refresh[0].get_all_available_columns_dropdown()
        filters_names_after_removed_refresh = sorted(
            [item.name for item in filters_after_removed_refresh if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_removed).described_as(
                f"TL-3172 columns are the same after refresh on board {board_test}").is_equal_to(
                columns_names_after_removed_refresh)
            assert_that(filters_names_after_removed).described_as(
                f"TL-3172 popup filters are the same after refresh on board {board_test}").is_equal_to(
                filters_names_after_removed_refresh)

        # ---------- CHECK --------------

        first_filter_unchecked = next(item for item in filters_after_removed_refresh if not item.is_checked())
        first_filter_unchecked.check()
        filter_name_added = first_filter_unchecked.name
        table.wait_for_board_rows_to_be_loaded()

        columns_after_add = table.get_columns()
        columns_names_after_add = sorted(table.get_columns_names(columns_after_add))
        filters_after_add = columns_after_add[0].get_all_available_columns_dropdown()
        filters_names_after_add = sorted([item.name for item in filters_after_add if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_add).described_as(
                f"TL-3172 deselected column is not on board").contains(filter_name_added)

        self.sob.select_boards(table_name=board_to_change, wait_for_load=True)
        self.sob.select_boards(table_name=board_test, wait_for_load=True)
        self.sob.wait_for_board_rows_to_be_loaded()
        table = self.sob.get_table()
        columns_after_add_refresh = table.get_columns()
        columns_names_after_add_refresh = sorted(table.get_columns_names(columns_after_add_refresh))

        filters_after_add_refresh = columns_after_add_refresh[0].get_all_available_columns_dropdown()
        filters_names_after_add_refresh = sorted(
            [item.name for item in filters_after_add_refresh if item.is_checked()])

        with soft_assertions():
            assert_that(columns_names_after_add).described_as(
                f"TL-3172 columns are the same after refresh on board {board_test}").is_equal_to(
                columns_names_after_add_refresh)
            assert_that(filters_names_after_add).described_as(
                f"TL-3172 popup filters are the same after refresh on board {board_test}").is_equal_to(
                filters_names_after_add_refresh)

    @pytest.mark.testcase("TL-3193")
    def test_sob_popup_at_least_one_column_is_required(self):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)

        boards_names = self.sob.get_available_boards_names()
        board = boards_names[0]
        self.__check_all_columns_on_selected_board(board)

        self.sob.select_boards(table_name=board, wait_for_load=True)

        table = self.sob.get_table()
        columns = table.get_columns()

        filters = columns[0].get_all_available_columns_dropdown()
        columns[0].uncheck_all_columns_in_filter_dropdown(filters)
        table.wait_for_board_loader_not_visible()
        columns[0].close_column_popup()

        columns = table.get_columns()
        filters_after = columns[0].get_all_available_columns_dropdown()
        filters_checked = sorted([item.name for item in filters_after if item.is_checked()])
        assert_that(len(filters_checked)).is_equal_to(1)

        self.__check_all_columns_on_selected_board(board)

    @pytest.mark.testcase("TL-3222")
    @pytest.mark.testcase("TL-3224")
    @pytest.mark.testcase("TL-3225")
    @pytest.mark.testcase("TL-3295")
    @pytest.mark.jira("TL-7852")
    def test_sob_adjustment_to_columns_persist_after_refresh(self):
        board_id = 2
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        index_column_from = 0
        index_column_to = 4
        resize_column_by_px = 30

        boards_names = self.sob.get_available_boards_names()
        board = boards_names[board_id]
        self.__check_all_columns_on_selected_board(board)
        self.sob.select_boards(table_name=board, wait_for_load=True)

        # ------------------ Change column order -------------------
        table = self.sob.get_table()
        columns = table.get_columns_sorted()

        columns_names_before = table.get_columns_names(columns)
        columns[index_column_from].grab_and_move_to_column(columns[index_column_to])

        table_after = self.sob.get_table()
        columns_after = table_after.get_columns_sorted()
        columns_names_after = table_after.get_columns_names(columns_after)

        assert_that(columns_names_after.index(columns_names_before[0])).described_as(
            "TL-3222 Column order changed after drag and drop").is_equal_to(index_column_to)

        self.sob.refresh_page()
        self.sob.wait_for_board_rows_to_be_loaded()

        table_after_refresh = self.sob.get_table()
        columns_after_refresh = table_after_refresh.get_columns_sorted()
        columns_names_after_refresh = table_after.get_columns_names(columns_after_refresh)

        assert_that(columns_names_after).described_as("TL-3225 Columns order is changed after refresh").is_equal_to(
            columns_names_after_refresh)

        # -------------------- RESIZE -------------------------
        column_width_before = columns_after_refresh[0].get_column_width()
        columns_after_refresh[0].retry_resize_column_width(resize_column_by_px, column_width_before)
        self.sob.wait_for_board_rows_to_be_loaded()

        assert_that(columns_after_refresh[0].get_column_width()).described_as(
            "TL-3295 column resized by value").is_equal_to(column_width_before + resize_column_by_px)

        self.sob.refresh_page()
        self.sob.wait_for_board_rows_to_be_loaded()

        table_after_resize_refresh = self.sob.get_table()
        columns_after_resize_refresh = table_after_resize_refresh.get_columns_sorted()

        assert_that(column_width_before + resize_column_by_px).described_as(
            "TL-3295 column width is the same after resize").is_equal_to(
            columns_after_resize_refresh[0].get_column_width())

        self.__check_all_columns_on_selected_board(board)

    @pytest.mark.testcase("TL-3222")
    @pytest.mark.testcase("TL-3224")
    @pytest.mark.testcase("TL-3228")
    @pytest.mark.testcase("TL-3296")
    def test_sob_adjustment_to_columns_persist_after_leave_page(self):
        board_id = 2
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        index_column_from = 0
        index_column_to = 4
        resize_column_by_px = 30

        boards_names = self.sob.get_available_boards_names()
        board_test = boards_names[board_id]
        self.__check_all_columns_on_selected_board(board_test)
        board_to_change = boards_names[2]

        self.sob.select_boards(table_name=board_test, wait_for_load=True)

        table = self.sob.get_table()
        columns = table.get_columns_sorted()

        columns_names_before = table.get_columns_names(columns)
        columns[index_column_from].grab_and_move_to_column(columns[index_column_to])

        table_after = self.sob.get_table()
        columns_after = table_after.get_columns_sorted()
        columns_names_after = table_after.get_columns_names(columns_after)

        assert_that(columns_names_after.index(columns_names_before[0])).described_as(
            "TL-3222 Column order changed after drag and drop").is_equal_to(index_column_to)

        self.sob.select_boards(table_name=board_to_change, wait_for_load=True)
        self.sob.select_boards(table_name=board_test, wait_for_load=True)
        self.sob.wait_for_board_rows_to_be_loaded()

        table_after_refresh = self.sob.get_table()
        columns_after_refresh = table_after_refresh.get_columns_sorted()
        columns_names_after_refresh = table_after.get_columns_names(columns_after_refresh)

        assert_that(columns_names_after).described_as("TL-3228 Columns order is changed after leave page").is_equal_to(
            columns_names_after_refresh)

        # -------------------- RESIZE -------------------------
        column_width_before = columns_after_refresh[0].get_column_width()
        columns_after_refresh[0].retry_resize_column_width(resize_column_by_px, column_width_before)
        self.sob.wait_for_board_rows_to_be_loaded()

        assert_that(columns_after_refresh[0].get_column_width()).described_as(
            "TL-3296 column resized by value").is_equal_to(column_width_before + resize_column_by_px)

        self.sob.select_boards(table_name=board_to_change, wait_for_load=True)
        self.sob.select_boards(table_name=board_test, wait_for_load=True)
        self.sob.wait_for_board_rows_to_be_loaded()

        table_after_resize_refresh = self.sob.get_table()
        columns_after_resize_refresh = table_after_resize_refresh.get_columns_sorted()

        assert_that(column_width_before + resize_column_by_px).described_as(
            "TL-3296 column width is the same after resize").is_equal_to(
            columns_after_resize_refresh[0].get_column_width())

        self.__check_all_columns_on_selected_board(board_test)

    @pytest.mark.testcase("TL-3230")
    @pytest.mark.testcase("TL-3297")
    @pytest.mark.jira("TL-8273")
    def test_sob_adjustment_to_columns_persist_after_logout(self):
        board_id = 2
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        index_column_from = 0
        index_column_to = 4
        resize_column_by_px = 30

        boards_names = self.sob.get_available_boards_names()
        board_test = boards_names[board_id]
        self.__check_all_columns_on_selected_board(board_test)
        self.sob.select_boards(table_name=board_test, wait_for_load=True)
        table = self.sob.get_table()
        columns = table.get_columns_sorted()

        columns_names_before = table.get_columns_names(columns)
        columns[index_column_from].grab_and_move_to_column(columns[index_column_to])

        table_after = self.sob.get_table()
        columns_after = table_after.get_columns_sorted()
        columns_names_after = table_after.get_columns_names(columns_after)

        column_width_before = columns_after[0].get_column_width()
        columns_after[0].retry_resize_column_width(resize_column_by_px, column_width_before)
        self.sob.wait_for_board_rows_to_be_loaded()

        with soft_assertions():
            assert_that(columns_names_after.index(columns_names_before[0])).described_as(
                "TL-3297 Column order changed after drag and drop").is_equal_to(index_column_to)
            assert_that(columns_after[0].get_column_width()).described_as(
                "TL-3297 column resized by value").is_equal_to(column_width_before + resize_column_by_px)

        logout_page = TMSLogoutPage(self.driver, self.log)
        logout_page.logout()

        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.waitToLoadLoginPage()
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()

        self.sob.open_page_tl()
        self.sob.wait_for_board_rows_to_be_loaded()
        self.sob.select_boards(table_name=board_test, wait_for_load=True)

        table_after_resize_refresh = self.sob.get_table()
        columns_after_resize_refresh = table_after_resize_refresh.get_columns_sorted()
        columns_names_after_refresh = table_after.get_columns_names(columns_after_resize_refresh)

        with soft_assertions():
            assert_that(columns_names_after).described_as(
                "TL-3230 Columns order is changed after logout/login").is_equal_to(columns_names_after_refresh)
            assert_that(column_width_before + resize_column_by_px).described_as(
                "TL-3297 column width is the same after logout/login").is_equal_to(
                columns_after_resize_refresh[0].get_column_width())

        self.__check_all_columns_on_selected_board(board_test)

    def __check_all_columns_on_all_boards_and_reload_page(self):
        boards_customization = self.order_api_tools.api.get_tl_boards_customization()
        self.__check_all_columns_on_all_boards_api(boards_customization)
        self.sob.open_page_tl()

    def __get_board_id_by_board_name(self, name):
        return self.lookup_tools.get_shipment_status_filter_by_name(name)

    def __check_all_columns_on_selected_board(self, board_name):
        boards_customization = self.order_api_tools.api.get_tl_boards_customization()
        board_id = self.__get_board_id_by_board_name(board_name)
        self.__check_all_columns_on_board_api(board_id, boards_customization)

    def __check_all_columns_on_all_boards_api(self, boards_customization_api):
        for board in boards_customization_api:
            self.__check_all_columns_on_board_api(board['tlBoardId'], boards_customization_api)

    def __check_all_columns_on_board_api(self, board_id, boards_customization_api):
        board_by_id = next(item for item in boards_customization_api if item['tlBoardId'] == board_id)
        column_settings = board_by_id['customColumnSettings']

        save_custom_model = SaveTruckloadBoardCustomRequestModel(board_id)
        for column in column_settings:
            setting = self.order_api_tools.get_custom_column_setting_to_be_saved(column, True)
            setting.model['width'] = 200
            save_custom_model.add_column_setting(setting)

        save_filter_model = SaveTruckloadBoardCustomColumnFilterSingleRequestModel(board_id)

        result = self.order_api_tools.api.save_truckload_board_customization(save_custom_model)
        result = self.order_api_tools.api.save_tl_custom_column_filters_single(save_filter_model)
        boards_customization = self.order_api_tools.api.get_tl_boards_customization()
