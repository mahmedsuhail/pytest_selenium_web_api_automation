import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from API.Helpers.create_order_helpers_api import CreateOrderHelpersAPI
from API.Types.order_statuses_dict import OrderStatusDict
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL, SalesOrderConfirmationPopup
from pages.TL.salesorder.so_carrier_fulfillment_page import SOCarrierFulfillmentPage
from pages.TL.salesorder.so_statuses_page import SOStatusesPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from utilities.read_data import read_csv_data, read_json_file


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login")
class TestSOShipmentStatusBookedDispatch:
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    carrier_data = read_json_file("tl_carrier_fulfillment_data.json")
    create_tl_order_api_data = read_json_file("API/order/create_tl_order_model.json")
    create_tl_order_item_api_data = read_json_file("API/order/create_tl_order_item_model.json")
    cancel_tl_order_api_data = read_json_file("API/order/cancel_tl_order_model.json")
    tl_order_details_api_carrier_data = read_json_file("API/order/tl_order_details_carrier_model.json")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_statuses_page = SOStatusesPage(self.driver, self.log)
        self.sales_order_page_confirmation_popup = SalesOrderConfirmationPopup(self.driver, self.log)
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_carrier_fulfillment_page = SOCarrierFulfillmentPage(self.driver, self.log)
        self.qop = CreateQuotePageTL(self.driver, self.log)
        self.create_order_helpers_api = CreateOrderHelpersAPI(api=self.api, logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login(self):
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.sales_order_board_page.get_url_sales_order_board())

    @pytest.mark.testcase("TL-8506")
    def test_so_user_can_change_from_booked_to_dispatch_and_back(self):
        bol_number = self.create_order_helpers_api.prepare_order_with_given_status_in_api(
            customer_name=self.quote_data[1]["customerName"], create_tl_order_api_data=self.create_tl_order_api_data,
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            tl_order_details_api_carrier_data=self.tl_order_details_api_carrier_data,
            status_id=OrderStatusDict.order_statuses["BOOKED"])

        self.sales_order_page.open_selected_order_tl(bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-8506 BOOKED order status").is_equal_to("BOOKED")
            assert_that(sorted(self.sales_order_statuses_page.get_order_status_dropdown_options())).described_as(
                "TL-8506 available statuses").is_equal_to(["DISPATCH"])

        self.sales_order_statuses_page.select_dispatch_status()
        self.qop.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-8506 DISPATCH order status").is_equal_to("DISPATCH")
            assert_that(sorted(self.sales_order_statuses_page.get_order_status_dropdown_options())).described_as(
                "TL-8506 available statuses").is_equal_to(["BOOKED"])

        self.sales_order_statuses_page.select_booked_status()
        self.qop.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-8506 BOOKED order status").is_equal_to("BOOKED")
            assert_that(sorted(self.sales_order_statuses_page.get_order_status_dropdown_options())).described_as(
                "TL-8506 available statuses").is_equal_to(["DISPATCH"])
