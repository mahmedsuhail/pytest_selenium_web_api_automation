import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from API.Helpers.create_order_helpers_api import CreateOrderHelpersAPI
from API.Models.tl_order_details_carrier_model import TLOrderDetailsCarrierModel
from API.Types.order_statuses_dict import OrderStatusDict
from API.api_carrier import APICarrier
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL, SalesOrderConfirmationPopup
from pages.TL.salesorder.so_carrier_fulfillment_page import SOCarrierFulfillmentPage
from pages.TL.salesorder.so_statuses_page import SOStatusesPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from utilities.read_data import read_csv_data, read_json_file


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login")
class TestSOShipmentStatusTests():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    carrier_data = read_json_file("tl_carrier_fulfillment_data.json")
    create_tl_order_api_data = read_json_file("API/order/create_tl_order_model.json")
    create_tl_order_item_api_data = read_json_file("API/order/create_tl_order_item_model.json")
    cancel_tl_order_api_data = read_json_file("API/order/cancel_tl_order_model.json")
    tl_order_details_api_carrier_data = read_json_file("API/order/tl_order_details_carrier_model.json")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.order_api = APIOrderAPI(self.api, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_statuses_page = SOStatusesPage(self.driver, self.log)
        self.sales_order_page_confirmation_popup = SalesOrderConfirmationPopup(self.driver, self.log)
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_carrier_fulfillment_page = SOCarrierFulfillmentPage(self.driver, self.log)
        self.qop = CreateQuotePageTL(self.driver, self.log)
        self.create_order_helpers_api = CreateOrderHelpersAPI(api=self.api, logger=self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login(self):
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.sales_order_board_page.get_url_sales_order_board())

    @pytest.mark.testcase("TL-2490")
    def test_so_user_cannot_update_canceled_status_manually(self):
        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api()

        # Cancel order through API
        cancel_order_model = CreateOrderHelpersAPI(self.api, self.log).get_cancel_order_model(customer_bk,
                                                                                              str(order_bk),
                                                                                              cancel_order_data=self.cancel_tl_order_api_data)
        self.cancel_order = self.order_api.cancel_order(cancel_order_model=cancel_order_model)

        self.sales_order_board_page.click_on_canceled_tab()
        self.sales_order_board_table.set_bol_column_filter(value=bol_number)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()

        assert self.sales_order_board_table.check_if_order_is_present(order_bol_number=bol_number)
        self.sales_order_board_table.open_order(order_bol_number=bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()
        self.sales_order_statuses_page.click_on_order_status_button()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
                "TL-2490 Cancelled status text").is_equal_to("CANCELED")
            assert_that(self.sales_order_statuses_page.check_if_order_status_dropdown_is_visible()).described_as(
                "TL-2490 Order status dropdown visibility").is_false()
            assert_that(len(self.sales_order_statuses_page.get_order_status_button_child_elements())).described_as(
                "TL-2490 Cancelled status cannot be changed").is_equal_to(0)

    @pytest.mark.testcase("TL-2498")
    def test_so_user_cannot_update_delivered_status_manually(self):
        self.sales_order_board_page.open_page_tl()

        bol_number = self._prepare_order_with_given_status_in_api(OrderStatusDict.order_statuses["DELIVERED"])

        self.sales_order_page.open_selected_order_tl(bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()
        self.sales_order_statuses_page.click_on_order_status_button()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
                "TL-2498 Delivered status text").is_equal_to("DELIVERED")
            assert_that(self.sales_order_statuses_page.check_if_order_status_dropdown_is_visible()).described_as(
                "TL-2498 Order status dropdown visibility").is_false()
            assert_that(len(self.sales_order_statuses_page.get_order_status_button_child_elements())).described_as(
                "TL-2498 Delivered status cannot be changed").is_equal_to(0)

    @pytest.mark.testcase("TL-5144")
    @pytest.mark.testcase("TL-8504")
    @pytest.mark.testcase("TL-8505")
    def test_so_available_to_customer_hold_status(self):
        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api()

        self.sales_order_board_page.click_on_available_tab()
        self.sales_order_board_table.set_bol_column_filter(value=bol_number)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded(timeout=20)

        assert_that(self.sales_order_board_table.check_if_order_is_present(order_bol_number=bol_number)).is_true()
        self.sales_order_board_table.open_order(order_bol_number=bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()

        # TL-5144

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-5144 Available order status").is_equal_to("AVAILABLE")
            assert_that(self.sales_order_statuses_page.get_order_status_dropdown_options()).described_as(
                "TL-5144, TL-8504 Available statuses").is_equal_to(["CUSTOMER HOLD"])

        self.sales_order_statuses_page.select_customer_hold_status()
        self.qop.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-5144 Customer Hold order status").is_equal_to("CUSTOMER HOLD")
            assert_that(self.sales_order_statuses_page.get_order_status_dropdown_options()).described_as(
                "TL-5144, TL-8505 Available statuses").is_equal_to(["AVAILABLE"])

    @pytest.mark.testcase("TL-5145")
    @pytest.mark.testcase("TL-8504")
    @pytest.mark.testcase("TL-8505")
    def test_so_customer_hold_status_to_available(self):
        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api()

        order_details = self.order_api.get_order_details(order_bk=order_bk)

        # Update order through API - set Customer Hold status
        update_order_model = CreateOrderHelpersAPI(self.api,
                                                   self.log).get_update_order_model_from_get_order_details_response(
            order_model=order_details)
        update_order_model.set_order_status(status_id=OrderStatusDict.order_statuses["CUSTOMER HOLD"])
        self.update_order = self.order_api.update_order(update_order_model=update_order_model)

        self.sales_order_page.open_selected_order_tl(bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()

        # TL-5145
        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-5144 Available order status").is_equal_to("CUSTOMER HOLD")
            assert_that(self.sales_order_statuses_page.get_order_status_dropdown_options()).described_as(
                "TL-5144, TL-8505 Available statuses").is_equal_to(["AVAILABLE"])

        self.sales_order_statuses_page.select_available_status()
        self.qop.wait_for_page_loader_to_show_and_disappear()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-5144 Available status").is_equal_to("AVAILABLE")
            assert_that(self.sales_order_statuses_page.get_order_status_dropdown_options()).described_as(
                "TL-5144, TL-8504 Available statuses").is_equal_to(["CUSTOMER HOLD"])

    @pytest.mark.testcase("TL-2792")
    @pytest.mark.testcase("TL-2790")
    @pytest.mark.testcase("TL-8254")
    def test_so_shipment_status_is_changed_to_pending_when_carrier_is_assigned(self):
        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api()

        self.sales_order_page.open_selected_order_tl(bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()

        # TL-2792
        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "TL-2792 Available order status").is_equal_to("AVAILABLE")
            assert_that(self.sales_order_statuses_page.get_order_status_dropdown_options()).described_as(
                "TL-2792, TL-8254 Available statuses").is_equal_to(["CUSTOMER HOLD"])

        carrier_name = self.carrier_data.get("carriers")[1].get("carrierName")
        self._search_and_select_carrier(carrier_name)
        self.qop.wait_for_page_loader_to_show_and_disappear()
        self.sales_order_page.wait_for_order_to_be_loaded()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
                "TL-2792 Order should be changed to Pending").is_equal_to("PENDING")
            assert_that(self.sales_order_carrier_fulfillment_page.check_if_carrier_is_assigned(
                carrier_name=carrier_name)).described_as(
                "TL-2792 Carrier should be assigned").is_true()
            assert_that(self.sales_order_statuses_page.check_if_order_status_dropdown_is_visible()).described_as(
                "TL-2790 Order status dropdown visibility").is_false()
            assert_that(len(self.sales_order_statuses_page.get_order_status_button_child_elements())).described_as(
                "TL-2790 Pending status cannot be changed").is_equal_to(0)

        self.sales_order_board_page.open_page_tl()
        self.sales_order_board_page.click_on_pending_tab()
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()
        self.sales_order_board_table.set_bol_column_filter(value=bol_number)
        self.sales_order_board_table.wait_for_board_rows_to_be_loaded()

        assert_that(self.sales_order_board_table.check_if_order_is_present(order_bol_number=bol_number))\
            .described_as("Check if order is present").is_true()
        self.sales_order_board_table.open_order(order_bol_number=bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
                "TL-2792 Order should be changed to Pending").is_equal_to("PENDING")
            assert_that(self.sales_order_carrier_fulfillment_page.check_if_carrier_is_assigned(
                carrier_name=carrier_name)).described_as(
                "TL-2792 Carrier should be assigned").is_true()
            assert_that(self.sales_order_statuses_page.check_if_order_status_dropdown_is_visible()).described_as(
                "TL-2790 Order status dropdown visibility").is_false()
            assert_that(len(self.sales_order_statuses_page.get_order_status_button_child_elements())).described_as(
                "TL-2790 Pending status cannot be changed").is_equal_to(0)

    @pytest.mark.testcase("TL-2790")
    def test_so_shipment_status_user_cannot_change_pending_status_manually(self):
        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api()

        order_details = self.order_api.get_order_details(order_bk=order_bk)

        # Assign Carrier through API - setting Pending status
        update_order_model = CreateOrderHelpersAPI(self.api,
                                                   self.log).get_update_order_model_from_get_order_details_response(
            order_model=order_details)
        update_order_model.add_carrier(carrier_data=TLOrderDetailsCarrierModel(
            carrier_data=self.tl_order_details_api_carrier_data))
        update_order_model.set_order_status(status_id=OrderStatusDict.order_statuses["PENDING"])

        self.carrier_api = APICarrier(self.api, self.log)
        self.update_order = self.carrier_api.assign_carrier(order_details_data=update_order_model)

        self.sales_order_page.open_selected_order_tl(bol_number)
        self.sales_order_page.wait_for_order_to_be_loaded()
        self.sales_order_statuses_page.click_on_order_status_button()

        with soft_assertions():
            assert_that(self.sales_order_statuses_page.get_order_status_button_text()).described_as(
                "TL-2790 Pending status text").is_equal_to("PENDING")
            assert_that(self.sales_order_statuses_page.check_if_order_status_dropdown_is_visible()).described_as(
                "TL-2790 Order status dropdown visibility").is_false()
            assert_that(len(self.sales_order_statuses_page.get_order_status_button_child_elements())).described_as(
                "TL-2790 Pending status cannot be changed").is_equal_to(0)

    def _create_order_through_api(self):
        bol_number, order_bk, customer_bk = self.create_order_helpers_api.create_order_through_api(
            customer_name=self.quote_data[1]["customerName"], create_tl_order_api_data=self.create_tl_order_api_data,
            create_tl_order_item_api_data=self.create_tl_order_item_api_data)

        return bol_number, order_bk, customer_bk

    def _prepare_order_with_given_status_in_api(self, status_id) -> int:
        bol_number = self.create_order_helpers_api.prepare_order_with_given_status_in_api(
            customer_name=self.quote_data[1]["customerName"], create_tl_order_api_data=self.create_tl_order_api_data,
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            tl_order_details_api_carrier_data=self.tl_order_details_api_carrier_data,
            status_id=status_id)

        return bol_number

    def _search_and_select_carrier(self, carrier_name):
        self.sales_order_carrier_fulfillment_page.click_select_carrier_button()
        self.sales_order_carrier_fulfillment_page.search_and_select_carrier(search_string=carrier_name,
                                                                            carrier_name=carrier_name)
        self.sales_order_carrier_fulfillment_page.click_save_and_assign_button()
