import pytest
from assertpy import assert_that, soft_assertions

import utilities.custom_logger as cl
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from utilities.read_data import read_csv_data
from utilities.util import Util


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestSOBBoardsManagement:
    login_data = read_csv_data("login_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.util = Util(self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        sob = SalesOrderBoardPageTL(self.driver, self.log)
        sob.open_page_tl()
        sob.wait_for_board_rows_to_be_loaded()

    @pytest.mark.testcase("TL-9924")
    def test_so_board_management_new_board_contains_rows(self):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        self.new_board_name = self.util.get_unique_name(5)

        self.create_board_page = self.sob.click_create_new_board()
        self.create_board_page.set_board_name(self.new_board_name)
        self.create_board_page.click_select_all_checkbox()
        self.create_board_page.click_save_and_close()

        self.sob.wait_for_page_loader_to_show_and_disappear()
        self.sob.wait_for_board_rows_to_be_loaded()
        boards_names = self.sob.get_available_boards_names()

        with soft_assertions():
            assert_that(boards_names).described_as("TL-9924 new board is visible on board list").contains(
                self.new_board_name)

        self.sob.select_boards(table_name=self.new_board_name, wait_for_load=True)
        table = self.sob.get_table()
        with soft_assertions():
            assert_that(len(table.get_rows())).described_as("TL-9924 new board contains rows").is_greater_than(2)

        self.sob.delete_custom_board(self.new_board_name)

    @pytest.mark.testcase("TL-6938")
    def test_so_board_correct_tabs_are_shown_on_tl_and_ltl(self):
        self.sob = SalesOrderBoardPageTL(self.driver, self.log)
        self.sob.wait_for_board_rows_to_be_loaded()
        boards_names_tl = self.sob.get_available_boards_names()

        with soft_assertions():
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Pending Quotes")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Quoted")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("EDI Tenders")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Booked")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Available")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Dispatched")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("In Transit")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Paperwork")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("All Shipments")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Canceled")
            boards_names_tl.remove("Pending Quotes")
            assert_that(boards_names_tl).described_as("TL board should contain").contains("Pending")

        self.sob.open_page_ltl()
        self.sob.wait_for_board_rows_to_be_loaded()
        boards_names_ltl = self.sob.get_available_boards_names()

        with soft_assertions():
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("All Orders")
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("Pending")
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("Dispatched")
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("In Transit")
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("Delivered")
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("Invoiced")
            assert_that(boards_names_ltl).described_as("LTL board should contain").contains("Quotes")
