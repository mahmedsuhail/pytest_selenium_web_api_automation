import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL
from pages.TL.salesorder.so_financials_page import SOFinancialsPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from utilities.read_data import read_csv_data


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login_and_go_to_tl")
class TestSOFinancials:
    login_data = read_csv_data("login_details.csv")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.so = SalesOrderPageTL(self.driver, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login_and_go_to_tl(self):
        sob = SalesOrderBoardPageTL(self.driver, self.log)

    @pytest.mark.testcase("TL-5209")
    def test_so_financials_user_narrow_line_select_by_search(self):
        filter_id = self.lookup_tools.get_shipment_status_filter_by_name('Booked')
        random_bol = self.order_api_tools.get_random_order_bol(filter_id)
        self.so.open_selected_order_tl(random_bol)
        self.so.wait_for_order_to_be_loaded()
        self.so.dismiss_shipment_warning_if_shows_up()
        financials_page = SOFinancialsPage(self.driver, self.log)
        financials_page.enable()
        financials_page.wait_for_add_charge_button_visible()
        financials_page.click_add_charge()

        financials_charge_rows = financials_page.get_charge_table_rows()
        last_row_id = len(financials_charge_rows) - 1

        values_one = financials_charge_rows[last_row_id].get_all_descriptions_with_search("B", True)
        values_two = financials_charge_rows[last_row_id].get_all_descriptions_with_search("BO", True)
        values_three = financials_charge_rows[last_row_id].get_all_descriptions_with_search("BOL", True)

        with soft_assertions():
            for value in values_one:
                assert_that(value.lower()).described_as("TL-5209").contains("b")
            for value in values_two:
                assert_that(value.lower()).described_as("TL-5209").contains("bo")
            for value in values_three:
                assert_that(value.lower()).described_as("TL-5209").contains("bol")

        financials_charge_rows[last_row_id].select_description_quick("BOL Correction")
        assert_that(financials_charge_rows[last_row_id].get_description()).is_equal_to("BOL Correction")
