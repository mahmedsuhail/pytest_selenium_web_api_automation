import pytest
from assertpy import soft_assertions, assert_that

import utilities.custom_logger as cl
from API.Helpers.create_order_helpers_api import CreateOrderHelpersAPI
from API.api_lookup import APILookup
from API.api_lookup_tools import APILookupTools
from API.api_order_api import APIOrderAPI
from API.api_order_tools import APIOrderTools
from pages.TL.createquote.create_quote_page_tl import CreateQuotePageTL
from pages.TL.salesorder.sales_order_page_tl import SalesOrderPageTL, SalesOrderConfirmationPopup
from pages.TL.salesorder.so_carrier_fulfillment_page import SOCarrierFulfillmentPage
from pages.TL.salesorder.so_network_info_page import SONetworkInfoPageTL
from pages.TL.salesorder.so_remove_carrier_popup_page_tl import SORemoveCarrierPopupPage
from pages.TL.salesorder.so_statuses_page import SOStatusesPage
from pages.TL.salesorderboard.sales_order_board_page_tl import SalesOrderBoardPageTL
from pages.TL.salesorderboard.sales_order_board_table_page_tl import SalesOrderBoardTablePageTL
from pages.home.TMSlogin_page import TMSLoginPage
from pages.home.TMSlogout_page import TMSLogoutPage
from utilities.read_data import read_csv_data, read_json_file


@pytest.mark.usefixtures("oneTimeSetUpWithAPI", "login")
class TestSOShipmentStatusTests():
    login_data = read_csv_data("login_details.csv")
    quote_data = read_csv_data("quote_details.csv")
    booked_by_data = read_json_file("TL/salesorder/test_so_booked_by_data.json")
    create_tl_order_api_data = read_json_file("API/order/create_tl_order_model.json")
    create_tl_order_item_api_data = read_json_file("API/order/create_tl_order_item_model.json")
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True)
    def class_set_up(self, oneTimeSetUpWithAPI):
        self.order_api = APIOrderAPI(self.api, self.log)
        self.lookup_tools = APILookupTools(APILookup(self.api, self.log))
        self.order_api_tools = APIOrderTools(APIOrderAPI(self.api, self.log))
        self.sales_order_page = SalesOrderPageTL(self.driver, self.log)
        self.sales_order_network_info_page = SONetworkInfoPageTL(self.driver, self.log)
        self.sales_order_statuses_page = SOStatusesPage(self.driver, self.log)
        self.sales_order_page_confirmation_popup = SalesOrderConfirmationPopup(self.driver, self.log)
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.sales_order_board_table = SalesOrderBoardTablePageTL(self.driver, self.log)
        self.sales_order_carrier_fulfillment_page = SOCarrierFulfillmentPage(self.driver, self.log)
        self.create_quote_page = CreateQuotePageTL(self.driver, self.log)
        self.remove_carrier_popup_page = SORemoveCarrierPopupPage(self.driver, self.log)
        self.create_order_helpers_api = CreateOrderHelpersAPI(api=self.api, logger=self.log)
        self.login_page = TMSLoginPage(self.driver, self.log)
        self.logout_page = TMSLogoutPage(self.driver, self.log)

    @pytest.fixture(scope="class")
    @pytest.mark.usefixtures("oneTimeSetUpWithAPI")
    def login(self):
        self.sales_order_board_page = SalesOrderBoardPageTL(self.driver, self.log)
        self.driver.get(self.api.base_url + self.sales_order_board_page.get_url_sales_order_board())

    @pytest.mark.testcase("TL-7499")
    def test_booked_by_should_be_correctly_displayed_one_user_scenario(self):
        expected_booked_by = self.booked_by_data.get("expectedBookedBy")

        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api(
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            create_tl_order_api_data=self.create_tl_order_api_data)

        self.sales_order_page.open_selected_order_tl(bol_id=bol_number)
        self.sales_order_page.wait_for_page_loader_to_show_and_disappear()

        # TL-7499
        with soft_assertions():
            assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
                "Booked By should be empty before carrier is assigned").is_equal_to("")
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "Order status should be available").is_equal_to("AVAILABLE")

        self._search_and_select_carrier(carrier_name=self.booked_by_data.get("carrierName"))
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self.sales_order_network_info_page.wait_for_booked_by_value(expected_value=expected_booked_by, timeout=3)

        assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
            "Booked By should have correct value when carrier is assigned").is_equal_to(expected_booked_by)

        self._remove_assigned_carrier(carrier_name=self.booked_by_data.get("carrierName"))
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self.sales_order_network_info_page.wait_for_booked_by_value(expected_value="", timeout=3)
        assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
            "Booked By should be removed when carrier is unassigned").is_equal_to("")

    @pytest.mark.testcase("TL-7499")
    def test_booked_by_should_be_correctly_displayed_multiple_users_carrier_assignment_scenario(self):
        expected_booked_by_2 = self.booked_by_data.get("expectedBookedBy2")

        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api(
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            create_tl_order_api_data=self.create_tl_order_api_data)

        self.sales_order_page.open_selected_order_tl(bol_id=bol_number)
        self.sales_order_page.wait_for_page_loader_to_show_and_disappear()

        # TL-7499
        with soft_assertions():
            assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
                "Booked By should be empty before carrier is assigned").is_equal_to("")
            assert_that(self.sales_order_statuses_page.get_current_order_status()).described_as(
                "Order status should be available").is_equal_to("AVAILABLE")

        # Logging in as different user and assigning carrier
        self._login_as_another_user()
        self.sales_order_page.open_selected_order_tl(bol_id=bol_number)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self._search_and_select_carrier(carrier_name=self.booked_by_data.get("carrierName"))
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self.sales_order_network_info_page.wait_for_booked_by_value(expected_value=expected_booked_by_2)

        assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
            "Booked By should have correct value when carrier is assigned by another user").is_equal_to(
            expected_booked_by_2)

    @pytest.mark.testcase("TL-7499")
    @pytest.mark.jira("TL-8594")
    def test_booked_by_should_be_correctly_displayed_multiple_users_carrier_removal_scenario(self):
        expected_booked_by = self.booked_by_data.get("expectedBookedBy")
        expected_booked_by_2 = self.booked_by_data.get("expectedBookedBy2")
        carrier_name = self.booked_by_data.get("carrierName")

        self.sales_order_board_page.open_page_tl()

        # Create order through API
        bol_number, order_bk, customer_bk = self._create_order_through_api(
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            create_tl_order_api_data=self.create_tl_order_api_data)

        self.sales_order_page.open_selected_order_tl(bol_id=bol_number)
        self.sales_order_page.wait_for_page_loader_to_show_and_disappear()

        # TL-7499
        with soft_assertions():
            assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
                "Booked By should be empty before carrier is assigned").is_equal_to("")
            assert_that(self.self.sales_order_statuses_page.get_current_order_status()).described_as(
                "Order status should be available").is_equal_to("AVAILABLE")

        self._search_and_select_carrier(carrier_name)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self.sales_order_network_info_page.wait_for_booked_by_value(expected_value=expected_booked_by)

        assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
            "Booked By should have correct value when carrier is assigned").is_equal_to(expected_booked_by)

        # Logging in as different user and reassigning carrier
        self._login_as_another_user()
        self.sales_order_page.open_selected_order_tl(bol_id=bol_number)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self._remove_assigned_carrier(carrier_name=carrier_name)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)

        self._search_and_select_carrier(carrier_name)
        self.create_quote_page.wait_for_page_loader_to_show_and_disappear(show_timeout=5, disappear_timeout=30)
        self.sales_order_network_info_page.wait_for_booked_by_value(expected_value=expected_booked_by_2)

        assert_that(self.sales_order_network_info_page.get_booked_by_field()).described_as(
            "Booked By should have correct value when carrier is assigned by another user").is_equal_to(
            expected_booked_by_2)

    def _create_order_through_api(self, create_tl_order_api_data, create_tl_order_item_api_data):
        bol_number, order_bk, customer_bk = self.create_order_helpers_api.create_order_through_api(
            customer_name=self.quote_data[1]["customerName"], create_tl_order_api_data=create_tl_order_api_data,
            create_tl_order_item_api_data=create_tl_order_item_api_data)

        return bol_number, order_bk, customer_bk

    def _prepare_order_with_given_status_in_api(self, status_id) -> int:
        bol_number = self.create_order_helpers_api.prepare_order_with_given_status_in_api(
            customer_name=self.quote_data[1]["customerName"], create_tl_order_api_data=self.create_tl_order_api_data,
            create_tl_order_item_api_data=self.create_tl_order_item_api_data,
            tl_order_details_api_carrier_data=self.tl_order_details_api_carrier_data,
            status_id=status_id)

        return bol_number

    def _search_and_select_carrier(self, carrier_name):
        self.sales_order_carrier_fulfillment_page.click_select_carrier_button()
        self.sales_order_carrier_fulfillment_page.search_and_select_carrier(search_string=carrier_name,
                                                                            carrier_name=carrier_name)
        self.sales_order_carrier_fulfillment_page.click_save_and_assign_button()

    def _remove_assigned_carrier(self, carrier_name):
        self.sales_order_page.click_remove_carrier_footer_button()
        self.remove_carrier_popup_page.click_select_cancellation_reason()
        self.remove_carrier_popup_page.select_breakdown_cancellation_reason()
        self.remove_carrier_popup_page.click_remove_carrier_button()
        assert_that(self.sales_order_carrier_fulfillment_page.check_if_carrier_is_assigned(
            carrier_name=carrier_name)).described_as("Carrier should be unassigned, after it is removed").is_false()

    def _login_as_another_user(self):
        self.logout_page.logout()
        self.logout_page.verifyLogoutSuccessful()
        self.login_page.login(userName=self.booked_by_data["secondUserLoginDetails"].get("username"),
                              password=self.booked_by_data["secondUserLoginDetails"].get("password"))
        self.login_page.verifyLoginSuccessful()
