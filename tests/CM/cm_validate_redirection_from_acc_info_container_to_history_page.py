import pytest
import unittest2

import utilities.custom_logger as cl
from pages.home.TMSlogin_page import TMSLoginPage
from pages.CM.customer_board_common_page import CustomerBoardCommonPage
from pages.CM.customer_profile_common_page import CustomerProfileCommonPage
from pages.CM.customer_history_common_page import CustomerHistoryCommonPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class NavigateToHistoryPageFromCustomerProfileTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).cm = CustomerBoardCommonPage(self.driver, self.log)
        type(self).cm_profile = CustomerProfileCommonPage(self.driver, self.log)
        type(self).cm_history = CustomerHistoryCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.login_data = read_csv_data("login_details.csv")

        self._login()
        self._navigation_from_cust_management_to_profile()

    def test_navigate_to_history_page_from_account_information_container(self):
        self.log.info("Inside test_navigate_to_history_page_from_account_information_container Method")
        acc_info_result = self.cm_profile.verify_account_information_labels()
        self.ts.mark(acc_info_result, "Validated Account Information Labels")
        view_history_button_result = self.cm_history.view_history_button_is_displayed()
        self.ts.mark(view_history_button_result, "Validated View History Button is present "
                                                 "in the account information container")
        self.cm_history.click_view_history_button_in_acc_info()
        self.cm.wait_for_cutomer_board_display()
        default_history_board_page_result = self.cm_history.default_history_board_validations()
        self.ts.mark(default_history_board_page_result, "Validated upon click on View History Button in Account Info "
                                                        "section it landed in default history page.")
        self.ts.mark_final("test_navigate_to_history_page_from_account_information_container", True,
                           "Validated Navigation from Account Information Container to History Page.")

    def _navigation_from_cust_management_to_profile(self):
        self.log.info("Inside _navigation_from_cust_management_to_profile method")
        self.cm.click_customer_board_icon()
        self.cm.wait_for_cutomer_board_display()
        self.cm.click_customer_view_button()
        self.cm_profile.wait_for_cutomer_profile_display()

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")
