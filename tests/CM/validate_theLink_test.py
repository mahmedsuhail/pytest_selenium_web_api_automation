import pytest
import unittest2
import utilities.custom_logger as cl
from pages.CM.customer_board_common_page import CustomerBoardCommonPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data


# This is a Temp class file later will remove or update this class
@pytest.mark.fixtures("oneTimeSetUp", "class_set_up")
class ValidateTheLinkTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).cm = CustomerBoardCommonPage(self.driver, self.log)
        self.login_data = read_csv_data("login_details.csv")

        self._login()

    def test_checkCM(self):
        self.cm.click_customer_board_icon()
        self.log.info("Validation completed")

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")
