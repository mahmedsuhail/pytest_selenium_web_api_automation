import pytest
import unittest2

import utilities.custom_logger as cl
from pages.CM.customer_profile_common_page import CustomerProfileCommonPage
from pages.home.TMSlogin_page import TMSLoginPage
from pages.CM.customer_board_common_page import CustomerBoardCommonPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class CustomerProfileTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).cm_board = CustomerBoardCommonPage(self.driver, self.log)
        type(self).cm_profile = CustomerProfileCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.login_data = read_csv_data("login_details.csv")

        self._login()

    def test_validate_account_information_on_cm_page(self):
        self._select_customer_and_click_view_button()
        self.log.info("Inside test_validate_account_information_on_board method")
        validated_res = self.cm_profile.verify_account_information_labels()
        self.ts.mark_final("test_validate_account_information_on_board", validated_res,
                           "Validated Account Information Labels")

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")

    def _select_customer_and_click_view_button(self):
        self.cm_board.click_customer_board_icon()
        self.cm_board.wait_for_cutomer_board_display()
        self.cm_board.select_timeframe("All")
        self.cm_board.wait_for_cutomer_board_display()
        self.cm_board.click_customer_view_button()
        self.cm_profile.wait_for_cutomer_profile_display()
