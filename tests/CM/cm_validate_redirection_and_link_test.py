import pytest
import unittest2
import utilities.custom_logger as cl
from pages.CM.customer_board_common_page import CustomerBoardCommonPage
from pages.CM.customer_profile_common_page import CustomerProfileCommonPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.fixtures("oneTimeSetUp", "class_set_up")
class CMValidateRedirectionAndLinkTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).cm = CustomerBoardCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).cm_profile = CustomerProfileCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.login_data = read_csv_data("login_details.csv")

        self._login()

    def test_validate_page_redirection_to_customer_management(self):
        self.log.info("Inside test_validate_page_redirection_to_customer_management Method")
        redirection_validation = self.cm.click_customer_board_icon()
        self.ts.mark(redirection_validation, "Validating the Redirection to the Customer Page")
        get_value = self.cm.get_customer_link_attribute_value("class")
        self.log.info("Printing get Value : " + str(get_value))
        self.ts.mark_final("test_validate_page_redirection_to_customer_management",
                           True if get_value == "active" else False,
                           "Validating the selected Customer Management Link should not able to click again.")

    def test_navigation_from_cust_management_to_profile(self):
        self.log.info("Inside test_navigation_from_cust_management_to_profile method")
        self.cm.click_customer_board_icon()
        self.cm.wait_for_cutomer_board_display()
        selected_time_frame_res = self.cm.select_timeframe("All")
        self.ts.mark(selected_time_frame_res, "Selected time frame is same as passed")
        self.cm.wait_for_cutomer_board_display()
        self.cm.click_customer_view_button()
        cust_profile_display_res = self.cm_profile.wait_for_cutomer_profile_display()
        self.ts.mark_final("test_navigation_from_cust_management_to_profile", cust_profile_display_res,
                           "Customer profile is displayed")

    def test_navigation_from_cust_profile_to_board(self):
        self.log.info("Inside test_navigation_from_cust_profile_to_board method")
        self.cm_profile.click_customer_board_button_from_profile()
        self.cm.wait_for_cutomer_board_display()
        cust_board_display_res = self.cm.verify_customer_board_display()
        self.ts.mark_final("test_navigation_from_cust_profile_to_board", cust_board_display_res,
                           "Customer Board is displayed")

    def _login(self):
        credential = self.login_data[0]
        self.lp.login(credential["userName"], credential["password"])
        self.lp.verifyLoginSuccessful()
        self.log.info("Login Completed")
