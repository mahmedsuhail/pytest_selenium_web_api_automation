import pytest
import unittest2
import utilities.custom_logger as cl
from pages.CM.customer_board_common_page import CustomerBoardCommonPage
from pages.CM.customer_profile_common_page import CustomerProfileCommonPage
from pages.home.TMSlogin_page import TMSLoginPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.fixtures("oneTimeSetUp", "class_set_up")
class CustomerBoardDetailsTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).cm = CustomerBoardCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        type(self).cm_profile = CustomerProfileCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.login_data = read_csv_data("login_details.csv")

        self._login()

    def test_customer_board_column_headers(self):
        self.log.info("Inside test_customer_board_column_headers method")
        self.cm.click_customer_board_icon()
        self.cm.wait_for_cutomer_board_display()
        verified_headers_result = self.cm.verify_customer_board_columns()
        self.ts.mark_final("test_customer_board_column_headers", verified_headers_result,
                           "Customer Headers are verified")

    def _login(self):
        credential = self.login_data[0]
        self.lp.login(credential["userName"], credential["password"])
        self.lp.verifyLoginSuccessful()
        self.log.info("Login Completed")
