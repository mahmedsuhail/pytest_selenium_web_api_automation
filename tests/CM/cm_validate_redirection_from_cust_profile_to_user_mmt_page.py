import pytest
import unittest2

import utilities.custom_logger as cl
from pages.home.TMSlogin_page import TMSLoginPage
from pages.CM.customer_board_common_page import CustomerBoardCommonPage
from pages.CM.customer_profile_common_page import CustomerProfileCommonPage
from pages.CM.customer_history_common_page import CustomerHistoryCommonPage
from utilities.read_data import read_csv_data
from utilities.reportteststatus import ReportTestStatus


@pytest.mark.usefixtures("oneTimeSetUp", "class_set_up")
class NavigateToUserManagementPageFromCustomerProfileTest(unittest2.TestCase):
    log = cl.test_logger(filename=__qualname__)
    address_data = read_csv_data("address_details.csv")

    @pytest.fixture(autouse=True, scope="class")
    def class_set_up(self, oneTimeSetUp):
        type(self).lp = TMSLoginPage(self.driver, self.log)
        type(self).cm = CustomerBoardCommonPage(self.driver, self.log)
        type(self).cm_profile = CustomerProfileCommonPage(self.driver, self.log)
        type(self).cm_history = CustomerHistoryCommonPage(self.driver, self.log)
        type(self).ts = ReportTestStatus(self.driver, self.log)
        self.login_data = read_csv_data("login_details.csv")

        self._login()
        self._navigation_from_cust_management_to_profile()

    def test_navigate_to_user_management_page_from_customer_profile(self):
        self.log.info("Inside test_navigate_to_user_management_page_from_customer_profile Method")
        self.cm_profile.click_view_or_manage_users_button_from_customer_profile()
        self.cm.wait_for_cutomer_board_display()
        default_customer_user_management_page_result = \
            self.cm_profile.default_customer_user_management_page_validations()
        self.ts.mark(default_customer_user_management_page_result,
                     "Validated default user management filters & page is displayed")
        self.ts.mark_final("test_navigate_to_user_management_page_from_customer_profile", True,
                           "Validated Navigation from Customer Profile to User Management Page.")

    def _navigation_from_cust_management_to_profile(self):
        self.log.info("Inside _navigation_from_cust_management_to_profile method")
        self.cm.click_customer_board_icon()
        self.cm.wait_for_cutomer_board_display()
        self.cm.click_customer_view_button()
        self.cm_profile.wait_for_cutomer_profile_display()

    def _login(self):
        credential = self.login_data[0]
        login_page = TMSLoginPage(self.driver, self.log)
        login_page.login(credential["userName"], credential["password"])
        login_page.verifyLoginSuccessful()
        self.log.info("Login Completed")
